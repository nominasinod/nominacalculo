/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhevacontestadas", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhevacontestadas.findAll", query = "SELECT t FROM Tsgrhevacontestadas t")})
public class Tsgrhevacontestadas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_evacontestada")
    private Integer codEvacontestada;
    @Basic(optional = false)
    @Column(name = "cod_total")
    private int codTotal;
    @Lob
    @Column(name = "bin_reporte")
    private byte[] binReporte;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEvacontestada", fetch = FetchType.LAZY)
    private List<Tsgrhrespuestaseva> tsgrhrespuestasevaList;
    @JoinColumn(name = "cod_evaluado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEvaluado;
    @JoinColumn(name = "cod_evaluador", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEvaluador;
    @JoinColumn(name = "cod_evaluacion", referencedColumnName = "cod_evaluacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhevaluaciones codEvaluacion;

    public Tsgrhevacontestadas() {
    }

    public Tsgrhevacontestadas(Integer codEvacontestada) {
        this.codEvacontestada = codEvacontestada;
    }

    public Tsgrhevacontestadas(Integer codEvacontestada, int codTotal) {
        this.codEvacontestada = codEvacontestada;
        this.codTotal = codTotal;
    }

    public Integer getCodEvacontestada() {
        return codEvacontestada;
    }

    public void setCodEvacontestada(Integer codEvacontestada) {
        this.codEvacontestada = codEvacontestada;
    }

    public int getCodTotal() {
        return codTotal;
    }

    public void setCodTotal(int codTotal) {
        this.codTotal = codTotal;
    }

    public byte[] getBinReporte() {
        return binReporte;
    }

    public void setBinReporte(byte[] binReporte) {
        this.binReporte = binReporte;
    }

    @XmlTransient
    public List<Tsgrhrespuestaseva> getTsgrhrespuestasevaList() {
        return tsgrhrespuestasevaList;
    }

    public void setTsgrhrespuestasevaList(List<Tsgrhrespuestaseva> tsgrhrespuestasevaList) {
        this.tsgrhrespuestasevaList = tsgrhrespuestasevaList;
    }

    public Tsgrhempleados getCodEvaluado() {
        return codEvaluado;
    }

    public void setCodEvaluado(Tsgrhempleados codEvaluado) {
        this.codEvaluado = codEvaluado;
    }

    public Tsgrhempleados getCodEvaluador() {
        return codEvaluador;
    }

    public void setCodEvaluador(Tsgrhempleados codEvaluador) {
        this.codEvaluador = codEvaluador;
    }

    public Tsgrhevaluaciones getCodEvaluacion() {
        return codEvaluacion;
    }

    public void setCodEvaluacion(Tsgrhevaluaciones codEvaluacion) {
        this.codEvaluacion = codEvaluacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEvacontestada != null ? codEvacontestada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhevacontestadas)) {
            return false;
        }
        Tsgrhevacontestadas other = (Tsgrhevacontestadas) object;
        if ((this.codEvacontestada == null && other.codEvacontestada != null) || (this.codEvacontestada != null && !this.codEvacontestada.equals(other.codEvacontestada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhevacontestadas[ codEvacontestada=" + codEvacontestada + " ]";
    }
    
}
