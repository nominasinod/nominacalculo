/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhfactoreseva", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhfactoreseva.findAll", query = "SELECT t FROM Tsgrhfactoreseva t")})
public class Tsgrhfactoreseva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_factor")
    private Integer codFactor;
    @Basic(optional = false)
    @Column(name = "des_nbfactor")
    private String desNbfactor;
    @Basic(optional = false)
    @Column(name = "cod_edoeliminar")
    private boolean codEdoeliminar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codFactor", fetch = FetchType.LAZY)
    private List<Tsgrhsubfactoreseva> tsgrhsubfactoresevaList;

    public Tsgrhfactoreseva() {
    }

    public Tsgrhfactoreseva(Integer codFactor) {
        this.codFactor = codFactor;
    }

    public Tsgrhfactoreseva(Integer codFactor, String desNbfactor, boolean codEdoeliminar) {
        this.codFactor = codFactor;
        this.desNbfactor = desNbfactor;
        this.codEdoeliminar = codEdoeliminar;
    }

    public Integer getCodFactor() {
        return codFactor;
    }

    public void setCodFactor(Integer codFactor) {
        this.codFactor = codFactor;
    }

    public String getDesNbfactor() {
        return desNbfactor;
    }

    public void setDesNbfactor(String desNbfactor) {
        this.desNbfactor = desNbfactor;
    }

    public boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    @XmlTransient
    public List<Tsgrhsubfactoreseva> getTsgrhsubfactoresevaList() {
        return tsgrhsubfactoresevaList;
    }

    public void setTsgrhsubfactoresevaList(List<Tsgrhsubfactoreseva> tsgrhsubfactoresevaList) {
        this.tsgrhsubfactoresevaList = tsgrhsubfactoresevaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFactor != null ? codFactor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhfactoreseva)) {
            return false;
        }
        Tsgrhfactoreseva other = (Tsgrhfactoreseva) object;
        if ((this.codFactor == null && other.codFactor != null) || (this.codFactor != null && !this.codFactor.equals(other.codFactor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhfactoreseva[ codFactor=" + codFactor + " ]";
    }
    
}
