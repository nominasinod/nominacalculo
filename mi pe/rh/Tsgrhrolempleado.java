/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhrolempleado", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhrolempleado.findAll", query = "SELECT t FROM Tsgrhrolempleado t")})
public class Tsgrhrolempleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_rolempleado")
    private Integer codRolempleado;
    @Basic(optional = false)
    @Column(name = "des_nbrol")
    private String desNbrol;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tsgrhrolempleado", fetch = FetchType.LAZY)
    private List<Tsgrhrelacionroles> tsgrhrelacionrolesList;

    public Tsgrhrolempleado() {
    }

    public Tsgrhrolempleado(Integer codRolempleado) {
        this.codRolempleado = codRolempleado;
    }

    public Tsgrhrolempleado(Integer codRolempleado, String desNbrol, Date audFeccreacion) {
        this.codRolempleado = codRolempleado;
        this.desNbrol = desNbrol;
        this.audFeccreacion = audFeccreacion;
    }

    public Integer getCodRolempleado() {
        return codRolempleado;
    }

    public void setCodRolempleado(Integer codRolempleado) {
        this.codRolempleado = codRolempleado;
    }

    public String getDesNbrol() {
        return desNbrol;
    }

    public void setDesNbrol(String desNbrol) {
        this.desNbrol = desNbrol;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    @XmlTransient
    public List<Tsgrhrelacionroles> getTsgrhrelacionrolesList() {
        return tsgrhrelacionrolesList;
    }

    public void setTsgrhrelacionrolesList(List<Tsgrhrelacionroles> tsgrhrelacionrolesList) {
        this.tsgrhrelacionrolesList = tsgrhrelacionrolesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRolempleado != null ? codRolempleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhrolempleado)) {
            return false;
        }
        Tsgrhrolempleado other = (Tsgrhrolempleado) object;
        if ((this.codRolempleado == null && other.codRolempleado != null) || (this.codRolempleado != null && !this.codRolempleado.equals(other.codRolempleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhrolempleado[ codRolempleado=" + codRolempleado + " ]";
    }
    
}
