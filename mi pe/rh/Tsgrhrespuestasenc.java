/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhrespuestasenc", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhrespuestasenc.findAll", query = "SELECT t FROM Tsgrhrespuestasenc t")})
public class Tsgrhrespuestasenc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_respuesta")
    private Integer codRespuesta;
    @Column(name = "cod_edoeliminar")
    private Boolean codEdoeliminar;
    @OneToMany(mappedBy = "codRespuesta", fetch = FetchType.LAZY)
    private List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList;
    @JoinColumn(name = "cod_catrespuesta", referencedColumnName = "cod_catrespuesta")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhcatrespuestas codCatrespuesta;
    @JoinColumn(name = "cod_pregunta", referencedColumnName = "cod_pregunta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhpreguntasenc codPregunta;

    public Tsgrhrespuestasenc() {
    }

    public Tsgrhrespuestasenc(Integer codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public Integer getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(Integer codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public Boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(Boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    @XmlTransient
    public List<TsgrhencuestaParticipantes> getTsgrhencuestaParticipantesList() {
        return tsgrhencuestaParticipantesList;
    }

    public void setTsgrhencuestaParticipantesList(List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList) {
        this.tsgrhencuestaParticipantesList = tsgrhencuestaParticipantesList;
    }

    public Tsgrhcatrespuestas getCodCatrespuesta() {
        return codCatrespuesta;
    }

    public void setCodCatrespuesta(Tsgrhcatrespuestas codCatrespuesta) {
        this.codCatrespuesta = codCatrespuesta;
    }

    public Tsgrhpreguntasenc getCodPregunta() {
        return codPregunta;
    }

    public void setCodPregunta(Tsgrhpreguntasenc codPregunta) {
        this.codPregunta = codPregunta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRespuesta != null ? codRespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhrespuestasenc)) {
            return false;
        }
        Tsgrhrespuestasenc other = (Tsgrhrespuestasenc) object;
        if ((this.codRespuesta == null && other.codRespuesta != null) || (this.codRespuesta != null && !this.codRespuesta.equals(other.codRespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhrespuestasenc[ codRespuesta=" + codRespuesta + " ]";
    }
    
}
