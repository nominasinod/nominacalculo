/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhclientes", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhclientes.findAll", query = "SELECT t FROM Tsgrhclientes t")})
public class Tsgrhclientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_cliente")
    private Integer codCliente;
    @Column(name = "des_nbcliente")
    private String desNbcliente;
    @Basic(optional = false)
    @Column(name = "des_direccioncte")
    private String desDireccioncte;
    @Basic(optional = false)
    @Column(name = "des_nbcontactocte")
    private String desNbcontactocte;
    @Basic(optional = false)
    @Column(name = "des_correocte")
    private String desCorreocte;
    @Column(name = "cod_telefonocte")
    private String codTelefonocte;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCliente", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList;

    public Tsgrhclientes() {
    }

    public Tsgrhclientes(Integer codCliente) {
        this.codCliente = codCliente;
    }

    public Tsgrhclientes(Integer codCliente, String desDireccioncte, String desNbcontactocte, String desCorreocte) {
        this.codCliente = codCliente;
        this.desDireccioncte = desDireccioncte;
        this.desNbcontactocte = desNbcontactocte;
        this.desCorreocte = desCorreocte;
    }

    public Integer getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(Integer codCliente) {
        this.codCliente = codCliente;
    }

    public String getDesNbcliente() {
        return desNbcliente;
    }

    public void setDesNbcliente(String desNbcliente) {
        this.desNbcliente = desNbcliente;
    }

    public String getDesDireccioncte() {
        return desDireccioncte;
    }

    public void setDesDireccioncte(String desDireccioncte) {
        this.desDireccioncte = desDireccioncte;
    }

    public String getDesNbcontactocte() {
        return desNbcontactocte;
    }

    public void setDesNbcontactocte(String desNbcontactocte) {
        this.desNbcontactocte = desNbcontactocte;
    }

    public String getDesCorreocte() {
        return desCorreocte;
    }

    public void setDesCorreocte(String desCorreocte) {
        this.desCorreocte = desCorreocte;
    }

    public String getCodTelefonocte() {
        return codTelefonocte;
    }

    public void setCodTelefonocte(String codTelefonocte) {
        this.codTelefonocte = codTelefonocte;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList() {
        return tsgrhcartaasignacionList;
    }

    public void setTsgrhcartaasignacionList(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList) {
        this.tsgrhcartaasignacionList = tsgrhcartaasignacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCliente != null ? codCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhclientes)) {
            return false;
        }
        Tsgrhclientes other = (Tsgrhclientes) object;
        if ((this.codCliente == null && other.codCliente != null) || (this.codCliente != null && !this.codCliente.equals(other.codCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhclientes[ codCliente=" + codCliente + " ]";
    }
    
}
