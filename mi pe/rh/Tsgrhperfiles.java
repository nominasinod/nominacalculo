/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhperfiles", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhperfiles.findAll", query = "SELECT t FROM Tsgrhperfiles t")})
public class Tsgrhperfiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_perfil")
    private Integer codPerfil;
    @Basic(optional = false)
    @Column(name = "des_perfil")
    private String desPerfil;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPerfil", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList;

    public Tsgrhperfiles() {
    }

    public Tsgrhperfiles(Integer codPerfil) {
        this.codPerfil = codPerfil;
    }

    public Tsgrhperfiles(Integer codPerfil, String desPerfil) {
        this.codPerfil = codPerfil;
        this.desPerfil = desPerfil;
    }

    public Integer getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(Integer codPerfil) {
        this.codPerfil = codPerfil;
    }

    public String getDesPerfil() {
        return desPerfil;
    }

    public void setDesPerfil(String desPerfil) {
        this.desPerfil = desPerfil;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList() {
        return tsgrhcartaasignacionList;
    }

    public void setTsgrhcartaasignacionList(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList) {
        this.tsgrhcartaasignacionList = tsgrhcartaasignacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPerfil != null ? codPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhperfiles)) {
            return false;
        }
        Tsgrhperfiles other = (Tsgrhperfiles) object;
        if ((this.codPerfil == null && other.codPerfil != null) || (this.codPerfil != null && !this.codPerfil.equals(other.codPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhperfiles[ codPerfil=" + codPerfil + " ]";
    }
    
}
