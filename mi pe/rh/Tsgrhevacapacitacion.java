/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhevacapacitacion", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhevacapacitacion.findAll", query = "SELECT t FROM Tsgrhevacapacitacion t")})
public class Tsgrhevacapacitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_evacapacitacion")
    private Integer codEvacapacitacion;
    @Basic(optional = false)
    @Column(name = "des_estado")
    private String desEstado;
    @Column(name = "des_evaluacion")
    private String desEvaluacion;
    @Basic(optional = false)
    @Column(name = "auf_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date aufFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;
    @JoinColumn(name = "cod_plancapacitacion", referencedColumnName = "cod_plancapacitacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhplancapacitacion codPlancapacitacion;

    public Tsgrhevacapacitacion() {
    }

    public Tsgrhevacapacitacion(Integer codEvacapacitacion) {
        this.codEvacapacitacion = codEvacapacitacion;
    }

    public Tsgrhevacapacitacion(Integer codEvacapacitacion, String desEstado, Date aufFeccreacion) {
        this.codEvacapacitacion = codEvacapacitacion;
        this.desEstado = desEstado;
        this.aufFeccreacion = aufFeccreacion;
    }

    public Integer getCodEvacapacitacion() {
        return codEvacapacitacion;
    }

    public void setCodEvacapacitacion(Integer codEvacapacitacion) {
        this.codEvacapacitacion = codEvacapacitacion;
    }

    public String getDesEstado() {
        return desEstado;
    }

    public void setDesEstado(String desEstado) {
        this.desEstado = desEstado;
    }

    public String getDesEvaluacion() {
        return desEvaluacion;
    }

    public void setDesEvaluacion(String desEvaluacion) {
        this.desEvaluacion = desEvaluacion;
    }

    public Date getAufFeccreacion() {
        return aufFeccreacion;
    }

    public void setAufFeccreacion(Date aufFeccreacion) {
        this.aufFeccreacion = aufFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhplancapacitacion getCodPlancapacitacion() {
        return codPlancapacitacion;
    }

    public void setCodPlancapacitacion(Tsgrhplancapacitacion codPlancapacitacion) {
        this.codPlancapacitacion = codPlancapacitacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEvacapacitacion != null ? codEvacapacitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhevacapacitacion)) {
            return false;
        }
        Tsgrhevacapacitacion other = (Tsgrhevacapacitacion) object;
        if ((this.codEvacapacitacion == null && other.codEvacapacitacion != null) || (this.codEvacapacitacion != null && !this.codEvacapacitacion.equals(other.codEvacapacitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhevacapacitacion[ codEvacapacitacion=" + codEvacapacitacion + " ]";
    }
    
}
