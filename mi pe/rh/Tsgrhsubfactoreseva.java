/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhsubfactoreseva", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhsubfactoreseva.findAll", query = "SELECT t FROM Tsgrhsubfactoreseva t")})
public class Tsgrhsubfactoreseva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_subfactor")
    private Integer codSubfactor;
    @Basic(optional = false)
    @Column(name = "des_nbsubfactor")
    private String desNbsubfactor;
    @Basic(optional = false)
    @Column(name = "cod_edoeliminar")
    private boolean codEdoeliminar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codSubfactor", fetch = FetchType.LAZY)
    private List<Tsgrhpreguntaseva> tsgrhpreguntasevaList;
    @JoinColumn(name = "cod_factor", referencedColumnName = "cod_factor")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhfactoreseva codFactor;

    public Tsgrhsubfactoreseva() {
    }

    public Tsgrhsubfactoreseva(Integer codSubfactor) {
        this.codSubfactor = codSubfactor;
    }

    public Tsgrhsubfactoreseva(Integer codSubfactor, String desNbsubfactor, boolean codEdoeliminar) {
        this.codSubfactor = codSubfactor;
        this.desNbsubfactor = desNbsubfactor;
        this.codEdoeliminar = codEdoeliminar;
    }

    public Integer getCodSubfactor() {
        return codSubfactor;
    }

    public void setCodSubfactor(Integer codSubfactor) {
        this.codSubfactor = codSubfactor;
    }

    public String getDesNbsubfactor() {
        return desNbsubfactor;
    }

    public void setDesNbsubfactor(String desNbsubfactor) {
        this.desNbsubfactor = desNbsubfactor;
    }

    public boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    @XmlTransient
    public List<Tsgrhpreguntaseva> getTsgrhpreguntasevaList() {
        return tsgrhpreguntasevaList;
    }

    public void setTsgrhpreguntasevaList(List<Tsgrhpreguntaseva> tsgrhpreguntasevaList) {
        this.tsgrhpreguntasevaList = tsgrhpreguntasevaList;
    }

    public Tsgrhfactoreseva getCodFactor() {
        return codFactor;
    }

    public void setCodFactor(Tsgrhfactoreseva codFactor) {
        this.codFactor = codFactor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSubfactor != null ? codSubfactor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhsubfactoreseva)) {
            return false;
        }
        Tsgrhsubfactoreseva other = (Tsgrhsubfactoreseva) object;
        if ((this.codSubfactor == null && other.codSubfactor != null) || (this.codSubfactor != null && !this.codSubfactor.equals(other.codSubfactor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhsubfactoreseva[ codSubfactor=" + codSubfactor + " ]";
    }
    
}
