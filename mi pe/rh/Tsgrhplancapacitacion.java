/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhplancapacitacion", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhplancapacitacion.findAll", query = "SELECT t FROM Tsgrhplancapacitacion t")})
public class Tsgrhplancapacitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_plancapacitacion")
    private Integer codPlancapacitacion;
    @Basic(optional = false)
    @Column(name = "des_nombre")
    private String desNombre;
    @Basic(optional = false)
    @Column(name = "des_criterios")
    private String desCriterios;
    @Basic(optional = false)
    @Column(name = "des_instructor")
    private String desInstructor;
    @Column(name = "des_comentarios")
    private String desComentarios;
    @Column(name = "des_evaluacion")
    private String desEvaluacion;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Basic(optional = false)
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tsgrhplancapacitacion", fetch = FetchType.LAZY)
    private List<Tsgrhrelacionroles> tsgrhrelacionrolesList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "codCapacitacion", fetch = FetchType.LAZY)
    private Tsgrhlogistica tsgrhlogistica;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @JoinColumn(name = "cod_estatus", referencedColumnName = "cod_estatus")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhestatuscapacitacion codEstatus;
    @JoinColumn(name = "cod_lugar", referencedColumnName = "cod_lugar")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhlugares codLugar;
    @JoinColumn(name = "cod_modo", referencedColumnName = "cod_modo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhmodo codModo;
    @JoinColumn(name = "cod_proceso", referencedColumnName = "cod_proceso")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhprocesos codProceso;
    @JoinColumn(name = "cod_proveedor", referencedColumnName = "cod_proveedor")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhproveedores codProveedor;
    @JoinColumn(name = "cod_tipocapacitacion", referencedColumnName = "cod_tipocapacitacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhtipocapacitacion codTipocapacitacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPlancapacitacion", fetch = FetchType.LAZY)
    private List<Tsgrhevacapacitacion> tsgrhevacapacitacionList;

    public Tsgrhplancapacitacion() {
    }

    public Tsgrhplancapacitacion(Integer codPlancapacitacion) {
        this.codPlancapacitacion = codPlancapacitacion;
    }

    public Tsgrhplancapacitacion(Integer codPlancapacitacion, String desNombre, String desCriterios, String desInstructor, Date audFeccreacion, Date audFecmodificacion) {
        this.codPlancapacitacion = codPlancapacitacion;
        this.desNombre = desNombre;
        this.desCriterios = desCriterios;
        this.desInstructor = desInstructor;
        this.audFeccreacion = audFeccreacion;
        this.audFecmodificacion = audFecmodificacion;
    }

    public Integer getCodPlancapacitacion() {
        return codPlancapacitacion;
    }

    public void setCodPlancapacitacion(Integer codPlancapacitacion) {
        this.codPlancapacitacion = codPlancapacitacion;
    }

    public String getDesNombre() {
        return desNombre;
    }

    public void setDesNombre(String desNombre) {
        this.desNombre = desNombre;
    }

    public String getDesCriterios() {
        return desCriterios;
    }

    public void setDesCriterios(String desCriterios) {
        this.desCriterios = desCriterios;
    }

    public String getDesInstructor() {
        return desInstructor;
    }

    public void setDesInstructor(String desInstructor) {
        this.desInstructor = desInstructor;
    }

    public String getDesComentarios() {
        return desComentarios;
    }

    public void setDesComentarios(String desComentarios) {
        this.desComentarios = desComentarios;
    }

    public String getDesEvaluacion() {
        return desEvaluacion;
    }

    public void setDesEvaluacion(String desEvaluacion) {
        this.desEvaluacion = desEvaluacion;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    @XmlTransient
    public List<Tsgrhrelacionroles> getTsgrhrelacionrolesList() {
        return tsgrhrelacionrolesList;
    }

    public void setTsgrhrelacionrolesList(List<Tsgrhrelacionroles> tsgrhrelacionrolesList) {
        this.tsgrhrelacionrolesList = tsgrhrelacionrolesList;
    }

    public Tsgrhlogistica getTsgrhlogistica() {
        return tsgrhlogistica;
    }

    public void setTsgrhlogistica(Tsgrhlogistica tsgrhlogistica) {
        this.tsgrhlogistica = tsgrhlogistica;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    public Tsgrhestatuscapacitacion getCodEstatus() {
        return codEstatus;
    }

    public void setCodEstatus(Tsgrhestatuscapacitacion codEstatus) {
        this.codEstatus = codEstatus;
    }

    public Tsgrhlugares getCodLugar() {
        return codLugar;
    }

    public void setCodLugar(Tsgrhlugares codLugar) {
        this.codLugar = codLugar;
    }

    public Tsgrhmodo getCodModo() {
        return codModo;
    }

    public void setCodModo(Tsgrhmodo codModo) {
        this.codModo = codModo;
    }

    public Tsgrhprocesos getCodProceso() {
        return codProceso;
    }

    public void setCodProceso(Tsgrhprocesos codProceso) {
        this.codProceso = codProceso;
    }

    public Tsgrhproveedores getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(Tsgrhproveedores codProveedor) {
        this.codProveedor = codProveedor;
    }

    public Tsgrhtipocapacitacion getCodTipocapacitacion() {
        return codTipocapacitacion;
    }

    public void setCodTipocapacitacion(Tsgrhtipocapacitacion codTipocapacitacion) {
        this.codTipocapacitacion = codTipocapacitacion;
    }

    @XmlTransient
    public List<Tsgrhevacapacitacion> getTsgrhevacapacitacionList() {
        return tsgrhevacapacitacionList;
    }

    public void setTsgrhevacapacitacionList(List<Tsgrhevacapacitacion> tsgrhevacapacitacionList) {
        this.tsgrhevacapacitacionList = tsgrhevacapacitacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPlancapacitacion != null ? codPlancapacitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhplancapacitacion)) {
            return false;
        }
        Tsgrhplancapacitacion other = (Tsgrhplancapacitacion) object;
        if ((this.codPlancapacitacion == null && other.codPlancapacitacion != null) || (this.codPlancapacitacion != null && !this.codPlancapacitacion.equals(other.codPlancapacitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhplancapacitacion[ codPlancapacitacion=" + codPlancapacitacion + " ]";
    }
    
}
