/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhpreguntaseva", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhpreguntaseva.findAll", query = "SELECT t FROM Tsgrhpreguntaseva t")})
public class Tsgrhpreguntaseva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_pregunta")
    private Integer codPregunta;
    @Basic(optional = false)
    @Column(name = "des_pregunta")
    private String desPregunta;
    @Basic(optional = false)
    @Column(name = "cod_edoeliminar")
    private boolean codEdoeliminar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPregunta", fetch = FetchType.LAZY)
    private List<Tsgrhrespuestaseva> tsgrhrespuestasevaList;
    @JoinColumn(name = "cod_evaluacion", referencedColumnName = "cod_evaluacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhevaluaciones codEvaluacion;
    @JoinColumn(name = "cod_subfactor", referencedColumnName = "cod_subfactor")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhsubfactoreseva codSubfactor;

    public Tsgrhpreguntaseva() {
    }

    public Tsgrhpreguntaseva(Integer codPregunta) {
        this.codPregunta = codPregunta;
    }

    public Tsgrhpreguntaseva(Integer codPregunta, String desPregunta, boolean codEdoeliminar) {
        this.codPregunta = codPregunta;
        this.desPregunta = desPregunta;
        this.codEdoeliminar = codEdoeliminar;
    }

    public Integer getCodPregunta() {
        return codPregunta;
    }

    public void setCodPregunta(Integer codPregunta) {
        this.codPregunta = codPregunta;
    }

    public String getDesPregunta() {
        return desPregunta;
    }

    public void setDesPregunta(String desPregunta) {
        this.desPregunta = desPregunta;
    }

    public boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    @XmlTransient
    public List<Tsgrhrespuestaseva> getTsgrhrespuestasevaList() {
        return tsgrhrespuestasevaList;
    }

    public void setTsgrhrespuestasevaList(List<Tsgrhrespuestaseva> tsgrhrespuestasevaList) {
        this.tsgrhrespuestasevaList = tsgrhrespuestasevaList;
    }

    public Tsgrhevaluaciones getCodEvaluacion() {
        return codEvaluacion;
    }

    public void setCodEvaluacion(Tsgrhevaluaciones codEvaluacion) {
        this.codEvaluacion = codEvaluacion;
    }

    public Tsgrhsubfactoreseva getCodSubfactor() {
        return codSubfactor;
    }

    public void setCodSubfactor(Tsgrhsubfactoreseva codSubfactor) {
        this.codSubfactor = codSubfactor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPregunta != null ? codPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhpreguntaseva)) {
            return false;
        }
        Tsgrhpreguntaseva other = (Tsgrhpreguntaseva) object;
        if ((this.codPregunta == null && other.codPregunta != null) || (this.codPregunta != null && !this.codPregunta.equals(other.codPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhpreguntaseva[ codPregunta=" + codPregunta + " ]";
    }
    
}
