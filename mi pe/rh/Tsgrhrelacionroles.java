/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhrelacionroles", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhrelacionroles.findAll", query = "SELECT t FROM Tsgrhrelacionroles t")})
public class Tsgrhrelacionroles implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TsgrhrelacionrolesPK tsgrhrelacionrolesPK;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @JoinColumn(name = "cod_plancapacitacion", referencedColumnName = "cod_plancapacitacion", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhplancapacitacion tsgrhplancapacitacion;
    @JoinColumn(name = "cod_rolempleado", referencedColumnName = "cod_rolempleado", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhrolempleado tsgrhrolempleado;

    public Tsgrhrelacionroles() {
    }

    public Tsgrhrelacionroles(TsgrhrelacionrolesPK tsgrhrelacionrolesPK) {
        this.tsgrhrelacionrolesPK = tsgrhrelacionrolesPK;
    }

    public Tsgrhrelacionroles(TsgrhrelacionrolesPK tsgrhrelacionrolesPK, Date audFeccreacion) {
        this.tsgrhrelacionrolesPK = tsgrhrelacionrolesPK;
        this.audFeccreacion = audFeccreacion;
    }

    public Tsgrhrelacionroles(int codPlancapacitacion, int codRolempleado) {
        this.tsgrhrelacionrolesPK = new TsgrhrelacionrolesPK(codPlancapacitacion, codRolempleado);
    }

    public TsgrhrelacionrolesPK getTsgrhrelacionrolesPK() {
        return tsgrhrelacionrolesPK;
    }

    public void setTsgrhrelacionrolesPK(TsgrhrelacionrolesPK tsgrhrelacionrolesPK) {
        this.tsgrhrelacionrolesPK = tsgrhrelacionrolesPK;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    public Tsgrhplancapacitacion getTsgrhplancapacitacion() {
        return tsgrhplancapacitacion;
    }

    public void setTsgrhplancapacitacion(Tsgrhplancapacitacion tsgrhplancapacitacion) {
        this.tsgrhplancapacitacion = tsgrhplancapacitacion;
    }

    public Tsgrhrolempleado getTsgrhrolempleado() {
        return tsgrhrolempleado;
    }

    public void setTsgrhrolempleado(Tsgrhrolempleado tsgrhrolempleado) {
        this.tsgrhrolempleado = tsgrhrolempleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tsgrhrelacionrolesPK != null ? tsgrhrelacionrolesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhrelacionroles)) {
            return false;
        }
        Tsgrhrelacionroles other = (Tsgrhrelacionroles) object;
        if ((this.tsgrhrelacionrolesPK == null && other.tsgrhrelacionrolesPK != null) || (this.tsgrhrelacionrolesPK != null && !this.tsgrhrelacionrolesPK.equals(other.tsgrhrelacionrolesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhrelacionroles[ tsgrhrelacionrolesPK=" + tsgrhrelacionrolesPK + " ]";
    }
    
}
