/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mipe
 */
@Embeddable
public class TsgrhrelacionrolesPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "cod_plancapacitacion")
    private int codPlancapacitacion;
    @Basic(optional = false)
    @Column(name = "cod_rolempleado")
    private int codRolempleado;

    public TsgrhrelacionrolesPK() {
    }

    public TsgrhrelacionrolesPK(int codPlancapacitacion, int codRolempleado) {
        this.codPlancapacitacion = codPlancapacitacion;
        this.codRolempleado = codRolempleado;
    }

    public int getCodPlancapacitacion() {
        return codPlancapacitacion;
    }

    public void setCodPlancapacitacion(int codPlancapacitacion) {
        this.codPlancapacitacion = codPlancapacitacion;
    }

    public int getCodRolempleado() {
        return codRolempleado;
    }

    public void setCodRolempleado(int codRolempleado) {
        this.codRolempleado = codRolempleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codPlancapacitacion;
        hash += (int) codRolempleado;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TsgrhrelacionrolesPK)) {
            return false;
        }
        TsgrhrelacionrolesPK other = (TsgrhrelacionrolesPK) object;
        if (this.codPlancapacitacion != other.codPlancapacitacion) {
            return false;
        }
        if (this.codRolempleado != other.codRolempleado) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.TsgrhrelacionrolesPK[ codPlancapacitacion=" + codPlancapacitacion + ", codRolempleado=" + codRolempleado + " ]";
    }
    
}
