/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhcontrataciones", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhcontrataciones.findAll", query = "SELECT t FROM Tsgrhcontrataciones t")})
public class Tsgrhcontrataciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_contratacion")
    private Integer codContratacion;
    @Basic(optional = false)
    @Column(name = "fec_inicio")
    @Temporal(TemporalType.DATE)
    private Date fecInicio;
    @Column(name = "fec_termino")
    @Temporal(TemporalType.DATE)
    private Date fecTermino;
    @Basic(optional = false)
    @Column(name = "des_esquema")
    private String desEsquema;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "cod_salarioestmin")
    private BigDecimal codSalarioestmin;
    @Column(name = "cod_salarioestmax")
    private BigDecimal codSalarioestmax;
    @Column(name = "tim_jornada")
    @Temporal(TemporalType.TIME)
    private Date timJornada;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;

    public Tsgrhcontrataciones() {
    }

    public Tsgrhcontrataciones(Integer codContratacion) {
        this.codContratacion = codContratacion;
    }

    public Tsgrhcontrataciones(Integer codContratacion, Date fecInicio, String desEsquema, BigDecimal codSalarioestmin, Date fecCreacion, Date fecModificacion) {
        this.codContratacion = codContratacion;
        this.fecInicio = fecInicio;
        this.desEsquema = desEsquema;
        this.codSalarioestmin = codSalarioestmin;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodContratacion() {
        return codContratacion;
    }

    public void setCodContratacion(Integer codContratacion) {
        this.codContratacion = codContratacion;
    }

    public Date getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Date fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Date getFecTermino() {
        return fecTermino;
    }

    public void setFecTermino(Date fecTermino) {
        this.fecTermino = fecTermino;
    }

    public String getDesEsquema() {
        return desEsquema;
    }

    public void setDesEsquema(String desEsquema) {
        this.desEsquema = desEsquema;
    }

    public BigDecimal getCodSalarioestmin() {
        return codSalarioestmin;
    }

    public void setCodSalarioestmin(BigDecimal codSalarioestmin) {
        this.codSalarioestmin = codSalarioestmin;
    }

    public BigDecimal getCodSalarioestmax() {
        return codSalarioestmax;
    }

    public void setCodSalarioestmax(BigDecimal codSalarioestmax) {
        this.codSalarioestmax = codSalarioestmax;
    }

    public Date getTimJornada() {
        return timJornada;
    }

    public void setTimJornada(Date timJornada) {
        this.timJornada = timJornada;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codContratacion != null ? codContratacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhcontrataciones)) {
            return false;
        }
        Tsgrhcontrataciones other = (Tsgrhcontrataciones) object;
        if ((this.codContratacion == null && other.codContratacion != null) || (this.codContratacion != null && !this.codContratacion.equals(other.codContratacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhcontrataciones[ codContratacion=" + codContratacion + " ]";
    }
    
}
