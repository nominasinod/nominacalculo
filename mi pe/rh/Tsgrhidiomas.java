/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhidiomas", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhidiomas.findAll", query = "SELECT t FROM Tsgrhidiomas t")})
public class Tsgrhidiomas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_idioma")
    private Integer codIdioma;
    @Basic(optional = false)
    @Column(name = "des_nbidioma")
    private String desNbidioma;
    @Column(name = "por_dominiooral")
    private Integer porDominiooral;
    @Column(name = "por_dominioescrito")
    private Integer porDominioescrito;
    @Column(name = "cod_empleado")
    private Integer codEmpleado;

    public Tsgrhidiomas() {
    }

    public Tsgrhidiomas(Integer codIdioma) {
        this.codIdioma = codIdioma;
    }

    public Tsgrhidiomas(Integer codIdioma, String desNbidioma) {
        this.codIdioma = codIdioma;
        this.desNbidioma = desNbidioma;
    }

    public Integer getCodIdioma() {
        return codIdioma;
    }

    public void setCodIdioma(Integer codIdioma) {
        this.codIdioma = codIdioma;
    }

    public String getDesNbidioma() {
        return desNbidioma;
    }

    public void setDesNbidioma(String desNbidioma) {
        this.desNbidioma = desNbidioma;
    }

    public Integer getPorDominiooral() {
        return porDominiooral;
    }

    public void setPorDominiooral(Integer porDominiooral) {
        this.porDominiooral = porDominiooral;
    }

    public Integer getPorDominioescrito() {
        return porDominioescrito;
    }

    public void setPorDominioescrito(Integer porDominioescrito) {
        this.porDominioescrito = porDominioescrito;
    }

    public Integer getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codIdioma != null ? codIdioma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhidiomas)) {
            return false;
        }
        Tsgrhidiomas other = (Tsgrhidiomas) object;
        if ((this.codIdioma == null && other.codIdioma != null) || (this.codIdioma != null && !this.codIdioma.equals(other.codIdioma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhidiomas[ codIdioma=" + codIdioma + " ]";
    }
    
}
