/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhexperienciaslaborales", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhexperienciaslaborales.findAll", query = "SELECT t FROM Tsgrhexperienciaslaborales t")})
public class Tsgrhexperienciaslaborales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_experiencia")
    private Integer codExperiencia;
    @Basic(optional = false)
    @Column(name = "des_nbempresa")
    private String desNbempresa;
    @Basic(optional = false)
    @Column(name = "des_nbpuesto")
    private String desNbpuesto;
    @Basic(optional = false)
    @Column(name = "fec_inicio")
    @Temporal(TemporalType.DATE)
    private Date fecInicio;
    @Basic(optional = false)
    @Column(name = "fec_termino")
    @Temporal(TemporalType.DATE)
    private Date fecTermino;
    @Basic(optional = false)
    @Column(name = "txt_actividades")
    private String txtActividades;
    @Column(name = "des_ubicacion")
    private String desUbicacion;
    @Column(name = "des_nbcliente")
    private String desNbcliente;
    @Column(name = "des_proyecto")
    private String desProyecto;
    @Column(name = "txt_logros")
    private String txtLogros;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;

    public Tsgrhexperienciaslaborales() {
    }

    public Tsgrhexperienciaslaborales(Integer codExperiencia) {
        this.codExperiencia = codExperiencia;
    }

    public Tsgrhexperienciaslaborales(Integer codExperiencia, String desNbempresa, String desNbpuesto, Date fecInicio, Date fecTermino, String txtActividades) {
        this.codExperiencia = codExperiencia;
        this.desNbempresa = desNbempresa;
        this.desNbpuesto = desNbpuesto;
        this.fecInicio = fecInicio;
        this.fecTermino = fecTermino;
        this.txtActividades = txtActividades;
    }

    public Integer getCodExperiencia() {
        return codExperiencia;
    }

    public void setCodExperiencia(Integer codExperiencia) {
        this.codExperiencia = codExperiencia;
    }

    public String getDesNbempresa() {
        return desNbempresa;
    }

    public void setDesNbempresa(String desNbempresa) {
        this.desNbempresa = desNbempresa;
    }

    public String getDesNbpuesto() {
        return desNbpuesto;
    }

    public void setDesNbpuesto(String desNbpuesto) {
        this.desNbpuesto = desNbpuesto;
    }

    public Date getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Date fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Date getFecTermino() {
        return fecTermino;
    }

    public void setFecTermino(Date fecTermino) {
        this.fecTermino = fecTermino;
    }

    public String getTxtActividades() {
        return txtActividades;
    }

    public void setTxtActividades(String txtActividades) {
        this.txtActividades = txtActividades;
    }

    public String getDesUbicacion() {
        return desUbicacion;
    }

    public void setDesUbicacion(String desUbicacion) {
        this.desUbicacion = desUbicacion;
    }

    public String getDesNbcliente() {
        return desNbcliente;
    }

    public void setDesNbcliente(String desNbcliente) {
        this.desNbcliente = desNbcliente;
    }

    public String getDesProyecto() {
        return desProyecto;
    }

    public void setDesProyecto(String desProyecto) {
        this.desProyecto = desProyecto;
    }

    public String getTxtLogros() {
        return txtLogros;
    }

    public void setTxtLogros(String txtLogros) {
        this.txtLogros = txtLogros;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codExperiencia != null ? codExperiencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhexperienciaslaborales)) {
            return false;
        }
        Tsgrhexperienciaslaborales other = (Tsgrhexperienciaslaborales) object;
        if ((this.codExperiencia == null && other.codExperiencia != null) || (this.codExperiencia != null && !this.codExperiencia.equals(other.codExperiencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhexperienciaslaborales[ codExperiencia=" + codExperiencia + " ]";
    }
    
}
