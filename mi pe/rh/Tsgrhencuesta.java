/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhencuesta", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhencuesta.findAll", query = "SELECT t FROM Tsgrhencuesta t")})
public class Tsgrhencuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_encuesta")
    private Integer codEncuesta;
    @Basic(optional = false)
    @Column(name = "des_nbencuesta")
    private String desNbencuesta;
    @Basic(optional = false)
    @Column(name = "cod_edoencuesta")
    private String codEdoencuesta;
    @Basic(optional = false)
    @Column(name = "fec_fechaencuesta")
    @Temporal(TemporalType.DATE)
    private Date fecFechaencuesta;
    @Basic(optional = false)
    @Column(name = "cod_lugar")
    private int codLugar;
    @Column(name = "tim_duracion")
    @Temporal(TemporalType.TIME)
    private Date timDuracion;
    @Column(name = "des_elementosvalidar")
    private String desElementosvalidar;
    @Column(name = "des_defectos")
    private String desDefectos;
    @Column(name = "des_introduccion")
    private String desIntroduccion;
    @Column(name = "cod_aceptado")
    private Boolean codAceptado;
    @Column(name = "cod_edoeliminar")
    private Boolean codEdoeliminar;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @Basic(optional = false)
    @Column(name = "cod_area")
    private int codArea;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEncuesta", fetch = FetchType.LAZY)
    private List<Tsgrhpreguntasenc> tsgrhpreguntasencList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEncuesta", fetch = FetchType.LAZY)
    private List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;

    public Tsgrhencuesta() {
    }

    public Tsgrhencuesta(Integer codEncuesta) {
        this.codEncuesta = codEncuesta;
    }

    public Tsgrhencuesta(Integer codEncuesta, String desNbencuesta, String codEdoencuesta, Date fecFechaencuesta, int codLugar, Date fecCreacion, Date fecModificacion, int codArea) {
        this.codEncuesta = codEncuesta;
        this.desNbencuesta = desNbencuesta;
        this.codEdoencuesta = codEdoencuesta;
        this.fecFechaencuesta = fecFechaencuesta;
        this.codLugar = codLugar;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
        this.codArea = codArea;
    }

    public Integer getCodEncuesta() {
        return codEncuesta;
    }

    public void setCodEncuesta(Integer codEncuesta) {
        this.codEncuesta = codEncuesta;
    }

    public String getDesNbencuesta() {
        return desNbencuesta;
    }

    public void setDesNbencuesta(String desNbencuesta) {
        this.desNbencuesta = desNbencuesta;
    }

    public String getCodEdoencuesta() {
        return codEdoencuesta;
    }

    public void setCodEdoencuesta(String codEdoencuesta) {
        this.codEdoencuesta = codEdoencuesta;
    }

    public Date getFecFechaencuesta() {
        return fecFechaencuesta;
    }

    public void setFecFechaencuesta(Date fecFechaencuesta) {
        this.fecFechaencuesta = fecFechaencuesta;
    }

    public int getCodLugar() {
        return codLugar;
    }

    public void setCodLugar(int codLugar) {
        this.codLugar = codLugar;
    }

    public Date getTimDuracion() {
        return timDuracion;
    }

    public void setTimDuracion(Date timDuracion) {
        this.timDuracion = timDuracion;
    }

    public String getDesElementosvalidar() {
        return desElementosvalidar;
    }

    public void setDesElementosvalidar(String desElementosvalidar) {
        this.desElementosvalidar = desElementosvalidar;
    }

    public String getDesDefectos() {
        return desDefectos;
    }

    public void setDesDefectos(String desDefectos) {
        this.desDefectos = desDefectos;
    }

    public String getDesIntroduccion() {
        return desIntroduccion;
    }

    public void setDesIntroduccion(String desIntroduccion) {
        this.desIntroduccion = desIntroduccion;
    }

    public Boolean getCodAceptado() {
        return codAceptado;
    }

    public void setCodAceptado(Boolean codAceptado) {
        this.codAceptado = codAceptado;
    }

    public Boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(Boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public int getCodArea() {
        return codArea;
    }

    public void setCodArea(int codArea) {
        this.codArea = codArea;
    }

    @XmlTransient
    public List<Tsgrhpreguntasenc> getTsgrhpreguntasencList() {
        return tsgrhpreguntasencList;
    }

    public void setTsgrhpreguntasencList(List<Tsgrhpreguntasenc> tsgrhpreguntasencList) {
        this.tsgrhpreguntasencList = tsgrhpreguntasencList;
    }

    @XmlTransient
    public List<TsgrhencuestaParticipantes> getTsgrhencuestaParticipantesList() {
        return tsgrhencuestaParticipantesList;
    }

    public void setTsgrhencuestaParticipantesList(List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList) {
        this.tsgrhencuestaParticipantesList = tsgrhencuestaParticipantesList;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEncuesta != null ? codEncuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhencuesta)) {
            return false;
        }
        Tsgrhencuesta other = (Tsgrhencuesta) object;
        if ((this.codEncuesta == null && other.codEncuesta != null) || (this.codEncuesta != null && !this.codEncuesta.equals(other.codEncuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhencuesta[ codEncuesta=" + codEncuesta + " ]";
    }
    
}
