/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhevaluaciones", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhevaluaciones.findAll", query = "SELECT t FROM Tsgrhevaluaciones t")})
public class Tsgrhevaluaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_evaluacion")
    private Integer codEvaluacion;
    @Basic(optional = false)
    @Column(name = "des_nbevaluacion")
    private String desNbevaluacion;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @Column(name = "des_edoevaluacion")
    private String desEdoevaluacion;
    @Column(name = "cod_edoeliminar")
    private Boolean codEdoeliminar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEvaluacion", fetch = FetchType.LAZY)
    private List<Tsgrhpreguntaseva> tsgrhpreguntasevaList;
    @OneToMany(mappedBy = "codEvaluacion", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEvaluacion", fetch = FetchType.LAZY)
    private List<Tsgrhevacontestadas> tsgrhevacontestadasList;

    public Tsgrhevaluaciones() {
    }

    public Tsgrhevaluaciones(Integer codEvaluacion) {
        this.codEvaluacion = codEvaluacion;
    }

    public Tsgrhevaluaciones(Integer codEvaluacion, String desNbevaluacion, Date fecCreacion, Date fecModificacion) {
        this.codEvaluacion = codEvaluacion;
        this.desNbevaluacion = desNbevaluacion;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodEvaluacion() {
        return codEvaluacion;
    }

    public void setCodEvaluacion(Integer codEvaluacion) {
        this.codEvaluacion = codEvaluacion;
    }

    public String getDesNbevaluacion() {
        return desNbevaluacion;
    }

    public void setDesNbevaluacion(String desNbevaluacion) {
        this.desNbevaluacion = desNbevaluacion;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public String getDesEdoevaluacion() {
        return desEdoevaluacion;
    }

    public void setDesEdoevaluacion(String desEdoevaluacion) {
        this.desEdoevaluacion = desEdoevaluacion;
    }

    public Boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(Boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    @XmlTransient
    public List<Tsgrhpreguntaseva> getTsgrhpreguntasevaList() {
        return tsgrhpreguntasevaList;
    }

    public void setTsgrhpreguntasevaList(List<Tsgrhpreguntaseva> tsgrhpreguntasevaList) {
        this.tsgrhpreguntasevaList = tsgrhpreguntasevaList;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList() {
        return tsgrhvalidaevaluaciondesList;
    }

    public void setTsgrhvalidaevaluaciondesList(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList) {
        this.tsgrhvalidaevaluaciondesList = tsgrhvalidaevaluaciondesList;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @XmlTransient
    public List<Tsgrhevacontestadas> getTsgrhevacontestadasList() {
        return tsgrhevacontestadasList;
    }

    public void setTsgrhevacontestadasList(List<Tsgrhevacontestadas> tsgrhevacontestadasList) {
        this.tsgrhevacontestadasList = tsgrhevacontestadasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEvaluacion != null ? codEvaluacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhevaluaciones)) {
            return false;
        }
        Tsgrhevaluaciones other = (Tsgrhevaluaciones) object;
        if ((this.codEvaluacion == null && other.codEvaluacion != null) || (this.codEvaluacion != null && !this.codEvaluacion.equals(other.codEvaluacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhevaluaciones[ codEvaluacion=" + codEvaluacion + " ]";
    }
    
}
