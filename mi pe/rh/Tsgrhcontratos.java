/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhcontratos", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhcontratos.findAll", query = "SELECT t FROM Tsgrhcontratos t")})
public class Tsgrhcontratos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_contrato")
    private Integer codContrato;
    @Basic(optional = false)
    @Column(name = "des_nbconsultor")
    private String desNbconsultor;
    @Basic(optional = false)
    @Column(name = "des_appaterno")
    private String desAppaterno;
    @Basic(optional = false)
    @Column(name = "des_apmaterno")
    private String desApmaterno;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;

    public Tsgrhcontratos() {
    }

    public Tsgrhcontratos(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public Tsgrhcontratos(Integer codContrato, String desNbconsultor, String desAppaterno, String desApmaterno, Date fecCreacion, Date fecModificacion) {
        this.codContrato = codContrato;
        this.desNbconsultor = desNbconsultor;
        this.desAppaterno = desAppaterno;
        this.desApmaterno = desApmaterno;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public String getDesNbconsultor() {
        return desNbconsultor;
    }

    public void setDesNbconsultor(String desNbconsultor) {
        this.desNbconsultor = desNbconsultor;
    }

    public String getDesAppaterno() {
        return desAppaterno;
    }

    public void setDesAppaterno(String desAppaterno) {
        this.desAppaterno = desAppaterno;
    }

    public String getDesApmaterno() {
        return desApmaterno;
    }

    public void setDesApmaterno(String desApmaterno) {
        this.desApmaterno = desApmaterno;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codContrato != null ? codContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhcontratos)) {
            return false;
        }
        Tsgrhcontratos other = (Tsgrhcontratos) object;
        if ((this.codContrato == null && other.codContrato != null) || (this.codContrato != null && !this.codContrato.equals(other.codContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhcontratos[ codContrato=" + codContrato + " ]";
    }
    
}
