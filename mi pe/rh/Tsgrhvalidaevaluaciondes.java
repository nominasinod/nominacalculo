/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhvalidaevaluaciondes", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhvalidaevaluaciondes.findAll", query = "SELECT t FROM Tsgrhvalidaevaluaciondes t")})
public class Tsgrhvalidaevaluaciondes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_validacion")
    private Integer codValidacion;
    @Basic(optional = false)
    @Column(name = "des_edoevaluacion")
    private String desEdoevaluacion;
    @Column(name = "fec_validacion")
    @Temporal(TemporalType.DATE)
    private Date fecValidacion;
    @Column(name = "cod_lugar")
    private Integer codLugar;
    @Column(name = "tim_duracion")
    @Temporal(TemporalType.TIME)
    private Date timDuracion;
    @Column(name = "des_defectosevalucacion")
    private String desDefectosevalucacion;
    @Column(name = "cod_edoeliminar")
    private Boolean codEdoeliminar;
    @JoinColumn(name = "cod_evaluado", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codEvaluado;
    @JoinColumn(name = "cod_evaluador", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codEvaluador;
    @JoinColumn(name = "cod_participante1", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante1;
    @JoinColumn(name = "cod_participante2", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante2;
    @JoinColumn(name = "cod_participante3", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante3;
    @JoinColumn(name = "cod_participante4", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante4;
    @JoinColumn(name = "cod_evaluacion", referencedColumnName = "cod_evaluacion")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhevaluaciones codEvaluacion;

    public Tsgrhvalidaevaluaciondes() {
    }

    public Tsgrhvalidaevaluaciondes(Integer codValidacion) {
        this.codValidacion = codValidacion;
    }

    public Tsgrhvalidaevaluaciondes(Integer codValidacion, String desEdoevaluacion) {
        this.codValidacion = codValidacion;
        this.desEdoevaluacion = desEdoevaluacion;
    }

    public Integer getCodValidacion() {
        return codValidacion;
    }

    public void setCodValidacion(Integer codValidacion) {
        this.codValidacion = codValidacion;
    }

    public String getDesEdoevaluacion() {
        return desEdoevaluacion;
    }

    public void setDesEdoevaluacion(String desEdoevaluacion) {
        this.desEdoevaluacion = desEdoevaluacion;
    }

    public Date getFecValidacion() {
        return fecValidacion;
    }

    public void setFecValidacion(Date fecValidacion) {
        this.fecValidacion = fecValidacion;
    }

    public Integer getCodLugar() {
        return codLugar;
    }

    public void setCodLugar(Integer codLugar) {
        this.codLugar = codLugar;
    }

    public Date getTimDuracion() {
        return timDuracion;
    }

    public void setTimDuracion(Date timDuracion) {
        this.timDuracion = timDuracion;
    }

    public String getDesDefectosevalucacion() {
        return desDefectosevalucacion;
    }

    public void setDesDefectosevalucacion(String desDefectosevalucacion) {
        this.desDefectosevalucacion = desDefectosevalucacion;
    }

    public Boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(Boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    public Tsgrhempleados getCodEvaluado() {
        return codEvaluado;
    }

    public void setCodEvaluado(Tsgrhempleados codEvaluado) {
        this.codEvaluado = codEvaluado;
    }

    public Tsgrhempleados getCodEvaluador() {
        return codEvaluador;
    }

    public void setCodEvaluador(Tsgrhempleados codEvaluador) {
        this.codEvaluador = codEvaluador;
    }

    public Tsgrhempleados getCodParticipante1() {
        return codParticipante1;
    }

    public void setCodParticipante1(Tsgrhempleados codParticipante1) {
        this.codParticipante1 = codParticipante1;
    }

    public Tsgrhempleados getCodParticipante2() {
        return codParticipante2;
    }

    public void setCodParticipante2(Tsgrhempleados codParticipante2) {
        this.codParticipante2 = codParticipante2;
    }

    public Tsgrhempleados getCodParticipante3() {
        return codParticipante3;
    }

    public void setCodParticipante3(Tsgrhempleados codParticipante3) {
        this.codParticipante3 = codParticipante3;
    }

    public Tsgrhempleados getCodParticipante4() {
        return codParticipante4;
    }

    public void setCodParticipante4(Tsgrhempleados codParticipante4) {
        this.codParticipante4 = codParticipante4;
    }

    public Tsgrhevaluaciones getCodEvaluacion() {
        return codEvaluacion;
    }

    public void setCodEvaluacion(Tsgrhevaluaciones codEvaluacion) {
        this.codEvaluacion = codEvaluacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codValidacion != null ? codValidacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhvalidaevaluaciondes)) {
            return false;
        }
        Tsgrhvalidaevaluaciondes other = (Tsgrhvalidaevaluaciondes) object;
        if ((this.codValidacion == null && other.codValidacion != null) || (this.codValidacion != null && !this.codValidacion.equals(other.codValidacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhvalidaevaluaciondes[ codValidacion=" + codValidacion + " ]";
    }
    
}
