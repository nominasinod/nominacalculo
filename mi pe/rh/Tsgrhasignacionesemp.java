/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhasignacionesemp", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhasignacionesemp.findAll", query = "SELECT t FROM Tsgrhasignacionesemp t")})
public class Tsgrhasignacionesemp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_asignacion")
    private Integer codAsignacion;
    @Column(name = "cod_modificadopor")
    private Integer codModificadopor;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_asignadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codAsignadopor;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;
    @JoinColumn(name = "cod_puesto", referencedColumnName = "cod_puesto")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhpuestos codPuesto;

    public Tsgrhasignacionesemp() {
    }

    public Tsgrhasignacionesemp(Integer codAsignacion) {
        this.codAsignacion = codAsignacion;
    }

    public Tsgrhasignacionesemp(Integer codAsignacion, String status, Date fecCreacion) {
        this.codAsignacion = codAsignacion;
        this.status = status;
        this.fecCreacion = fecCreacion;
    }

    public Integer getCodAsignacion() {
        return codAsignacion;
    }

    public void setCodAsignacion(Integer codAsignacion) {
        this.codAsignacion = codAsignacion;
    }

    public Integer getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Integer codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhempleados getCodAsignadopor() {
        return codAsignadopor;
    }

    public void setCodAsignadopor(Tsgrhempleados codAsignadopor) {
        this.codAsignadopor = codAsignadopor;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhpuestos getCodPuesto() {
        return codPuesto;
    }

    public void setCodPuesto(Tsgrhpuestos codPuesto) {
        this.codPuesto = codPuesto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAsignacion != null ? codAsignacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhasignacionesemp)) {
            return false;
        }
        Tsgrhasignacionesemp other = (Tsgrhasignacionesemp) object;
        if ((this.codAsignacion == null && other.codAsignacion != null) || (this.codAsignacion != null && !this.codAsignacion.equals(other.codAsignacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhasignacionesemp[ codAsignacion=" + codAsignacion + " ]";
    }
    
}
