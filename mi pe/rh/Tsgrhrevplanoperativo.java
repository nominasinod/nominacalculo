/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhrevplanoperativo", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhrevplanoperativo.findAll", query = "SELECT t FROM Tsgrhrevplanoperativo t")})
public class Tsgrhrevplanoperativo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_revplanoperativo")
    private Integer codRevplanoperativo;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "cod_lugar")
    private int codLugar;
    @Column(name = "tim_duracion")
    @Temporal(TemporalType.TIME)
    private Date timDuracion;
    @Column(name = "des_puntosatratar")
    private String desPuntosatratar;
    @Column(name = "des_acuerdosobtenidos")
    private String desAcuerdosobtenidos;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;
    @JoinColumn(name = "cod_participante1", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante1;
    @JoinColumn(name = "cod_participante2", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante2;
    @JoinColumn(name = "cod_participante3", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante3;
    @JoinColumn(name = "cod_participante4", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante4;
    @JoinColumn(name = "cod_participante5", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codParticipante5;
    @JoinColumn(name = "cod_planoperativo", referencedColumnName = "cod_planoperativo")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhplanoperativo codPlanoperativo;

    public Tsgrhrevplanoperativo() {
    }

    public Tsgrhrevplanoperativo(Integer codRevplanoperativo) {
        this.codRevplanoperativo = codRevplanoperativo;
    }

    public Tsgrhrevplanoperativo(Integer codRevplanoperativo, Date fecCreacion, int codLugar, Date fecModificacion) {
        this.codRevplanoperativo = codRevplanoperativo;
        this.fecCreacion = fecCreacion;
        this.codLugar = codLugar;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodRevplanoperativo() {
        return codRevplanoperativo;
    }

    public void setCodRevplanoperativo(Integer codRevplanoperativo) {
        this.codRevplanoperativo = codRevplanoperativo;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public int getCodLugar() {
        return codLugar;
    }

    public void setCodLugar(int codLugar) {
        this.codLugar = codLugar;
    }

    public Date getTimDuracion() {
        return timDuracion;
    }

    public void setTimDuracion(Date timDuracion) {
        this.timDuracion = timDuracion;
    }

    public String getDesPuntosatratar() {
        return desPuntosatratar;
    }

    public void setDesPuntosatratar(String desPuntosatratar) {
        this.desPuntosatratar = desPuntosatratar;
    }

    public String getDesAcuerdosobtenidos() {
        return desAcuerdosobtenidos;
    }

    public void setDesAcuerdosobtenidos(String desAcuerdosobtenidos) {
        this.desAcuerdosobtenidos = desAcuerdosobtenidos;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    public Tsgrhempleados getCodParticipante1() {
        return codParticipante1;
    }

    public void setCodParticipante1(Tsgrhempleados codParticipante1) {
        this.codParticipante1 = codParticipante1;
    }

    public Tsgrhempleados getCodParticipante2() {
        return codParticipante2;
    }

    public void setCodParticipante2(Tsgrhempleados codParticipante2) {
        this.codParticipante2 = codParticipante2;
    }

    public Tsgrhempleados getCodParticipante3() {
        return codParticipante3;
    }

    public void setCodParticipante3(Tsgrhempleados codParticipante3) {
        this.codParticipante3 = codParticipante3;
    }

    public Tsgrhempleados getCodParticipante4() {
        return codParticipante4;
    }

    public void setCodParticipante4(Tsgrhempleados codParticipante4) {
        this.codParticipante4 = codParticipante4;
    }

    public Tsgrhempleados getCodParticipante5() {
        return codParticipante5;
    }

    public void setCodParticipante5(Tsgrhempleados codParticipante5) {
        this.codParticipante5 = codParticipante5;
    }

    public Tsgrhplanoperativo getCodPlanoperativo() {
        return codPlanoperativo;
    }

    public void setCodPlanoperativo(Tsgrhplanoperativo codPlanoperativo) {
        this.codPlanoperativo = codPlanoperativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRevplanoperativo != null ? codRevplanoperativo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhrevplanoperativo)) {
            return false;
        }
        Tsgrhrevplanoperativo other = (Tsgrhrevplanoperativo) object;
        if ((this.codRevplanoperativo == null && other.codRevplanoperativo != null) || (this.codRevplanoperativo != null && !this.codRevplanoperativo.equals(other.codRevplanoperativo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhrevplanoperativo[ codRevplanoperativo=" + codRevplanoperativo + " ]";
    }
    
}
