/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhcapacitaciones", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhcapacitaciones.findAll", query = "SELECT t FROM Tsgrhcapacitaciones t")})
public class Tsgrhcapacitaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_capacitacion")
    private Integer codCapacitacion;
    @Basic(optional = false)
    @Column(name = "cod_tipocurso")
    private String codTipocurso;
    @Basic(optional = false)
    @Column(name = "des_nbcurso")
    private String desNbcurso;
    @Basic(optional = false)
    @Column(name = "des_organismo")
    private String desOrganismo;
    @Basic(optional = false)
    @Column(name = "fec_termino")
    @Temporal(TemporalType.DATE)
    private Date fecTermino;
    @Basic(optional = false)
    @Column(name = "des_duracion")
    private String desDuracion;
    @Lob
    @Column(name = "bin_documento")
    private byte[] binDocumento;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;

    public Tsgrhcapacitaciones() {
    }

    public Tsgrhcapacitaciones(Integer codCapacitacion) {
        this.codCapacitacion = codCapacitacion;
    }

    public Tsgrhcapacitaciones(Integer codCapacitacion, String codTipocurso, String desNbcurso, String desOrganismo, Date fecTermino, String desDuracion) {
        this.codCapacitacion = codCapacitacion;
        this.codTipocurso = codTipocurso;
        this.desNbcurso = desNbcurso;
        this.desOrganismo = desOrganismo;
        this.fecTermino = fecTermino;
        this.desDuracion = desDuracion;
    }

    public Integer getCodCapacitacion() {
        return codCapacitacion;
    }

    public void setCodCapacitacion(Integer codCapacitacion) {
        this.codCapacitacion = codCapacitacion;
    }

    public String getCodTipocurso() {
        return codTipocurso;
    }

    public void setCodTipocurso(String codTipocurso) {
        this.codTipocurso = codTipocurso;
    }

    public String getDesNbcurso() {
        return desNbcurso;
    }

    public void setDesNbcurso(String desNbcurso) {
        this.desNbcurso = desNbcurso;
    }

    public String getDesOrganismo() {
        return desOrganismo;
    }

    public void setDesOrganismo(String desOrganismo) {
        this.desOrganismo = desOrganismo;
    }

    public Date getFecTermino() {
        return fecTermino;
    }

    public void setFecTermino(Date fecTermino) {
        this.fecTermino = fecTermino;
    }

    public String getDesDuracion() {
        return desDuracion;
    }

    public void setDesDuracion(String desDuracion) {
        this.desDuracion = desDuracion;
    }

    public byte[] getBinDocumento() {
        return binDocumento;
    }

    public void setBinDocumento(byte[] binDocumento) {
        this.binDocumento = binDocumento;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCapacitacion != null ? codCapacitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhcapacitaciones)) {
            return false;
        }
        Tsgrhcapacitaciones other = (Tsgrhcapacitaciones) object;
        if ((this.codCapacitacion == null && other.codCapacitacion != null) || (this.codCapacitacion != null && !this.codCapacitacion.equals(other.codCapacitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhcapacitaciones[ codCapacitacion=" + codCapacitacion + " ]";
    }
    
}
