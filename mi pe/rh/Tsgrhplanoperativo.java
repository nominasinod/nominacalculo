/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhplanoperativo", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhplanoperativo.findAll", query = "SELECT t FROM Tsgrhplanoperativo t")})
public class Tsgrhplanoperativo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_planoperativo")
    private Integer codPlanoperativo;
    @Basic(optional = false)
    @Column(name = "des_nbplan")
    private String desNbplan;
    @Basic(optional = false)
    @Column(name = "cod_version")
    private String codVersion;
    @Basic(optional = false)
    @Column(name = "cod_anio")
    private int codAnio;
    @Basic(optional = false)
    @Column(name = "cod_estatus")
    private String codEstatus;
    @Lob
    @Column(name = "bin_planoperativo")
    private byte[] binPlanoperativo;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;
    @OneToMany(mappedBy = "codPlanoperativo", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList;

    public Tsgrhplanoperativo() {
    }

    public Tsgrhplanoperativo(Integer codPlanoperativo) {
        this.codPlanoperativo = codPlanoperativo;
    }

    public Tsgrhplanoperativo(Integer codPlanoperativo, String desNbplan, String codVersion, int codAnio, String codEstatus, Date fecCreacion, Date fecModificacion) {
        this.codPlanoperativo = codPlanoperativo;
        this.desNbplan = desNbplan;
        this.codVersion = codVersion;
        this.codAnio = codAnio;
        this.codEstatus = codEstatus;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodPlanoperativo() {
        return codPlanoperativo;
    }

    public void setCodPlanoperativo(Integer codPlanoperativo) {
        this.codPlanoperativo = codPlanoperativo;
    }

    public String getDesNbplan() {
        return desNbplan;
    }

    public void setDesNbplan(String desNbplan) {
        this.desNbplan = desNbplan;
    }

    public String getCodVersion() {
        return codVersion;
    }

    public void setCodVersion(String codVersion) {
        this.codVersion = codVersion;
    }

    public int getCodAnio() {
        return codAnio;
    }

    public void setCodAnio(int codAnio) {
        this.codAnio = codAnio;
    }

    public String getCodEstatus() {
        return codEstatus;
    }

    public void setCodEstatus(String codEstatus) {
        this.codEstatus = codEstatus;
    }

    public byte[] getBinPlanoperativo() {
        return binPlanoperativo;
    }

    public void setBinPlanoperativo(byte[] binPlanoperativo) {
        this.binPlanoperativo = binPlanoperativo;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList() {
        return tsgrhrevplanoperativoList;
    }

    public void setTsgrhrevplanoperativoList(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList) {
        this.tsgrhrevplanoperativoList = tsgrhrevplanoperativoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPlanoperativo != null ? codPlanoperativo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhplanoperativo)) {
            return false;
        }
        Tsgrhplanoperativo other = (Tsgrhplanoperativo) object;
        if ((this.codPlanoperativo == null && other.codPlanoperativo != null) || (this.codPlanoperativo != null && !this.codPlanoperativo.equals(other.codPlanoperativo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhplanoperativo[ codPlanoperativo=" + codPlanoperativo + " ]";
    }
    
}
