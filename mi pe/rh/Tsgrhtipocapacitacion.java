/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhtipocapacitacion", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhtipocapacitacion.findAll", query = "SELECT t FROM Tsgrhtipocapacitacion t")})
public class Tsgrhtipocapacitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_tipocapacitacion")
    private Integer codTipocapacitacion;
    @Basic(optional = false)
    @Column(name = "des_nbtipocapacitacion")
    private String desNbtipocapacitacion;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTipocapacitacion", fetch = FetchType.LAZY)
    private List<Tsgrhplancapacitacion> tsgrhplancapacitacionList;

    public Tsgrhtipocapacitacion() {
    }

    public Tsgrhtipocapacitacion(Integer codTipocapacitacion) {
        this.codTipocapacitacion = codTipocapacitacion;
    }

    public Tsgrhtipocapacitacion(Integer codTipocapacitacion, String desNbtipocapacitacion, Date audFeccreacion) {
        this.codTipocapacitacion = codTipocapacitacion;
        this.desNbtipocapacitacion = desNbtipocapacitacion;
        this.audFeccreacion = audFeccreacion;
    }

    public Integer getCodTipocapacitacion() {
        return codTipocapacitacion;
    }

    public void setCodTipocapacitacion(Integer codTipocapacitacion) {
        this.codTipocapacitacion = codTipocapacitacion;
    }

    public String getDesNbtipocapacitacion() {
        return desNbtipocapacitacion;
    }

    public void setDesNbtipocapacitacion(String desNbtipocapacitacion) {
        this.desNbtipocapacitacion = desNbtipocapacitacion;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    @XmlTransient
    public List<Tsgrhplancapacitacion> getTsgrhplancapacitacionList() {
        return tsgrhplancapacitacionList;
    }

    public void setTsgrhplancapacitacionList(List<Tsgrhplancapacitacion> tsgrhplancapacitacionList) {
        this.tsgrhplancapacitacionList = tsgrhplancapacitacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipocapacitacion != null ? codTipocapacitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhtipocapacitacion)) {
            return false;
        }
        Tsgrhtipocapacitacion other = (Tsgrhtipocapacitacion) object;
        if ((this.codTipocapacitacion == null && other.codTipocapacitacion != null) || (this.codTipocapacitacion != null && !this.codTipocapacitacion.equals(other.codTipocapacitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhtipocapacitacion[ codTipocapacitacion=" + codTipocapacitacion + " ]";
    }
    
}
