/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhprocesos", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhprocesos.findAll", query = "SELECT t FROM Tsgrhprocesos t")})
public class Tsgrhprocesos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_proceso")
    private Integer codProceso;
    @Basic(optional = false)
    @Column(name = "des_nbproceso")
    private String desNbproceso;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codProceso", fetch = FetchType.LAZY)
    private List<Tsgrhplancapacitacion> tsgrhplancapacitacionList;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;

    public Tsgrhprocesos() {
    }

    public Tsgrhprocesos(Integer codProceso) {
        this.codProceso = codProceso;
    }

    public Tsgrhprocesos(Integer codProceso, String desNbproceso, Date audFeccreacion) {
        this.codProceso = codProceso;
        this.desNbproceso = desNbproceso;
        this.audFeccreacion = audFeccreacion;
    }

    public Integer getCodProceso() {
        return codProceso;
    }

    public void setCodProceso(Integer codProceso) {
        this.codProceso = codProceso;
    }

    public String getDesNbproceso() {
        return desNbproceso;
    }

    public void setDesNbproceso(String desNbproceso) {
        this.desNbproceso = desNbproceso;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    @XmlTransient
    public List<Tsgrhplancapacitacion> getTsgrhplancapacitacionList() {
        return tsgrhplancapacitacionList;
    }

    public void setTsgrhplancapacitacionList(List<Tsgrhplancapacitacion> tsgrhplancapacitacionList) {
        this.tsgrhplancapacitacionList = tsgrhplancapacitacionList;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codProceso != null ? codProceso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhprocesos)) {
            return false;
        }
        Tsgrhprocesos other = (Tsgrhprocesos) object;
        if ((this.codProceso == null && other.codProceso != null) || (this.codProceso != null && !this.codProceso.equals(other.codProceso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhprocesos[ codProceso=" + codProceso + " ]";
    }
    
}
