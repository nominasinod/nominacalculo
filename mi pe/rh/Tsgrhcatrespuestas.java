/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhcatrespuestas", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhcatrespuestas.findAll", query = "SELECT t FROM Tsgrhcatrespuestas t")})
public class Tsgrhcatrespuestas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_catrespuesta")
    private Integer codCatrespuesta;
    @Basic(optional = false)
    @Column(name = "des_respuesta")
    private String desRespuesta;
    @Basic(optional = false)
    @Column(name = "cod_ponderacion")
    private int codPonderacion;
    @OneToMany(mappedBy = "codCatrespuesta", fetch = FetchType.LAZY)
    private List<Tsgrhrespuestasenc> tsgrhrespuestasencList;

    public Tsgrhcatrespuestas() {
    }

    public Tsgrhcatrespuestas(Integer codCatrespuesta) {
        this.codCatrespuesta = codCatrespuesta;
    }

    public Tsgrhcatrespuestas(Integer codCatrespuesta, String desRespuesta, int codPonderacion) {
        this.codCatrespuesta = codCatrespuesta;
        this.desRespuesta = desRespuesta;
        this.codPonderacion = codPonderacion;
    }

    public Integer getCodCatrespuesta() {
        return codCatrespuesta;
    }

    public void setCodCatrespuesta(Integer codCatrespuesta) {
        this.codCatrespuesta = codCatrespuesta;
    }

    public String getDesRespuesta() {
        return desRespuesta;
    }

    public void setDesRespuesta(String desRespuesta) {
        this.desRespuesta = desRespuesta;
    }

    public int getCodPonderacion() {
        return codPonderacion;
    }

    public void setCodPonderacion(int codPonderacion) {
        this.codPonderacion = codPonderacion;
    }

    @XmlTransient
    public List<Tsgrhrespuestasenc> getTsgrhrespuestasencList() {
        return tsgrhrespuestasencList;
    }

    public void setTsgrhrespuestasencList(List<Tsgrhrespuestasenc> tsgrhrespuestasencList) {
        this.tsgrhrespuestasencList = tsgrhrespuestasencList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCatrespuesta != null ? codCatrespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhcatrespuestas)) {
            return false;
        }
        Tsgrhcatrespuestas other = (Tsgrhcatrespuestas) object;
        if ((this.codCatrespuesta == null && other.codCatrespuesta != null) || (this.codCatrespuesta != null && !this.codCatrespuesta.equals(other.codCatrespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhcatrespuestas[ codCatrespuesta=" + codCatrespuesta + " ]";
    }
    
}
