/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhcartaasignacion", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhcartaasignacion.findAll", query = "SELECT t FROM Tsgrhcartaasignacion t")})
public class Tsgrhcartaasignacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_asignacion")
    private Integer codAsignacion;
    @Column(name = "des_actividades")
    private String desActividades;
    @Column(name = "des_lugarsalida")
    private String desLugarsalida;
    @Column(name = "des_lugarllegada")
    private String desLugarllegada;
    @Column(name = "fec_salida")
    @Temporal(TemporalType.DATE)
    private Date fecSalida;
    @Column(name = "fec_llegada")
    @Temporal(TemporalType.DATE)
    private Date fecLlegada;
    @Column(name = "cod_transporte")
    private String codTransporte;
    @Column(name = "des_lugarhopedaje")
    private String desLugarhopedaje;
    @Column(name = "fec_hospedaje")
    @Temporal(TemporalType.DATE)
    private Date fecHospedaje;
    @Column(name = "des_computadora")
    private String desComputadora;
    @Column(name = "cod_telefono")
    private String codTelefono;
    @Column(name = "des_accesorios")
    private String desAccesorios;
    @Column(name = "des_nbresponsable")
    private String desNbresponsable;
    @Column(name = "des_nbpuesto")
    private String desNbpuesto;
    @Column(name = "des_lugarresp")
    private String desLugarresp;
    @Column(name = "cod_telefonoresp")
    private String codTelefonoresp;
    @Column(name = "tim_horario")
    @Temporal(TemporalType.TIME)
    private Date timHorario;
    @Column(name = "fec_iniciocontra")
    @Temporal(TemporalType.DATE)
    private Date fecIniciocontra;
    @Column(name = "fec_terminocontra")
    @Temporal(TemporalType.DATE)
    private Date fecTerminocontra;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_sueldomensual")
    private BigDecimal impSueldomensual;
    @Column(name = "imp_nominaimss")
    private BigDecimal impNominaimss;
    @Column(name = "imp_honorarios")
    private BigDecimal impHonorarios;
    @Column(name = "imp_otros")
    private BigDecimal impOtros;
    @Column(name = "cod_rfc")
    private String codRfc;
    @Column(name = "des_razonsocial")
    private String desRazonsocial;
    @Column(name = "des_correo")
    private String desCorreo;
    @Column(name = "cod_cpostal")
    private Integer codCpostal;
    @Column(name = "des_direccionfact")
    private String desDireccionfact;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @JoinColumn(name = "cod_cliente", referencedColumnName = "cod_cliente")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhclientes codCliente;
    @JoinColumn(name = "cod_ape", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codApe;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;
    @JoinColumn(name = "cod_gpy", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codGpy;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;
    @JoinColumn(name = "cod_rhta", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codRhta;
    @JoinColumn(name = "cod_rys", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codRys;
    @JoinColumn(name = "cod_perfil", referencedColumnName = "cod_perfil")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhperfiles codPerfil;

    public Tsgrhcartaasignacion() {
    }

    public Tsgrhcartaasignacion(Integer codAsignacion) {
        this.codAsignacion = codAsignacion;
    }

    public Tsgrhcartaasignacion(Integer codAsignacion, Date fecCreacion, Date fecModificacion) {
        this.codAsignacion = codAsignacion;
        this.fecCreacion = fecCreacion;
        this.fecModificacion = fecModificacion;
    }

    public Integer getCodAsignacion() {
        return codAsignacion;
    }

    public void setCodAsignacion(Integer codAsignacion) {
        this.codAsignacion = codAsignacion;
    }

    public String getDesActividades() {
        return desActividades;
    }

    public void setDesActividades(String desActividades) {
        this.desActividades = desActividades;
    }

    public String getDesLugarsalida() {
        return desLugarsalida;
    }

    public void setDesLugarsalida(String desLugarsalida) {
        this.desLugarsalida = desLugarsalida;
    }

    public String getDesLugarllegada() {
        return desLugarllegada;
    }

    public void setDesLugarllegada(String desLugarllegada) {
        this.desLugarllegada = desLugarllegada;
    }

    public Date getFecSalida() {
        return fecSalida;
    }

    public void setFecSalida(Date fecSalida) {
        this.fecSalida = fecSalida;
    }

    public Date getFecLlegada() {
        return fecLlegada;
    }

    public void setFecLlegada(Date fecLlegada) {
        this.fecLlegada = fecLlegada;
    }

    public String getCodTransporte() {
        return codTransporte;
    }

    public void setCodTransporte(String codTransporte) {
        this.codTransporte = codTransporte;
    }

    public String getDesLugarhopedaje() {
        return desLugarhopedaje;
    }

    public void setDesLugarhopedaje(String desLugarhopedaje) {
        this.desLugarhopedaje = desLugarhopedaje;
    }

    public Date getFecHospedaje() {
        return fecHospedaje;
    }

    public void setFecHospedaje(Date fecHospedaje) {
        this.fecHospedaje = fecHospedaje;
    }

    public String getDesComputadora() {
        return desComputadora;
    }

    public void setDesComputadora(String desComputadora) {
        this.desComputadora = desComputadora;
    }

    public String getCodTelefono() {
        return codTelefono;
    }

    public void setCodTelefono(String codTelefono) {
        this.codTelefono = codTelefono;
    }

    public String getDesAccesorios() {
        return desAccesorios;
    }

    public void setDesAccesorios(String desAccesorios) {
        this.desAccesorios = desAccesorios;
    }

    public String getDesNbresponsable() {
        return desNbresponsable;
    }

    public void setDesNbresponsable(String desNbresponsable) {
        this.desNbresponsable = desNbresponsable;
    }

    public String getDesNbpuesto() {
        return desNbpuesto;
    }

    public void setDesNbpuesto(String desNbpuesto) {
        this.desNbpuesto = desNbpuesto;
    }

    public String getDesLugarresp() {
        return desLugarresp;
    }

    public void setDesLugarresp(String desLugarresp) {
        this.desLugarresp = desLugarresp;
    }

    public String getCodTelefonoresp() {
        return codTelefonoresp;
    }

    public void setCodTelefonoresp(String codTelefonoresp) {
        this.codTelefonoresp = codTelefonoresp;
    }

    public Date getTimHorario() {
        return timHorario;
    }

    public void setTimHorario(Date timHorario) {
        this.timHorario = timHorario;
    }

    public Date getFecIniciocontra() {
        return fecIniciocontra;
    }

    public void setFecIniciocontra(Date fecIniciocontra) {
        this.fecIniciocontra = fecIniciocontra;
    }

    public Date getFecTerminocontra() {
        return fecTerminocontra;
    }

    public void setFecTerminocontra(Date fecTerminocontra) {
        this.fecTerminocontra = fecTerminocontra;
    }

    public BigDecimal getImpSueldomensual() {
        return impSueldomensual;
    }

    public void setImpSueldomensual(BigDecimal impSueldomensual) {
        this.impSueldomensual = impSueldomensual;
    }

    public BigDecimal getImpNominaimss() {
        return impNominaimss;
    }

    public void setImpNominaimss(BigDecimal impNominaimss) {
        this.impNominaimss = impNominaimss;
    }

    public BigDecimal getImpHonorarios() {
        return impHonorarios;
    }

    public void setImpHonorarios(BigDecimal impHonorarios) {
        this.impHonorarios = impHonorarios;
    }

    public BigDecimal getImpOtros() {
        return impOtros;
    }

    public void setImpOtros(BigDecimal impOtros) {
        this.impOtros = impOtros;
    }

    public String getCodRfc() {
        return codRfc;
    }

    public void setCodRfc(String codRfc) {
        this.codRfc = codRfc;
    }

    public String getDesRazonsocial() {
        return desRazonsocial;
    }

    public void setDesRazonsocial(String desRazonsocial) {
        this.desRazonsocial = desRazonsocial;
    }

    public String getDesCorreo() {
        return desCorreo;
    }

    public void setDesCorreo(String desCorreo) {
        this.desCorreo = desCorreo;
    }

    public Integer getCodCpostal() {
        return codCpostal;
    }

    public void setCodCpostal(Integer codCpostal) {
        this.codCpostal = codCpostal;
    }

    public String getDesDireccionfact() {
        return desDireccionfact;
    }

    public void setDesDireccionfact(String desDireccionfact) {
        this.desDireccionfact = desDireccionfact;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Tsgrhclientes getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(Tsgrhclientes codCliente) {
        this.codCliente = codCliente;
    }

    public Tsgrhempleados getCodApe() {
        return codApe;
    }

    public void setCodApe(Tsgrhempleados codApe) {
        this.codApe = codApe;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhempleados getCodGpy() {
        return codGpy;
    }

    public void setCodGpy(Tsgrhempleados codGpy) {
        this.codGpy = codGpy;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    public Tsgrhempleados getCodRhta() {
        return codRhta;
    }

    public void setCodRhta(Tsgrhempleados codRhta) {
        this.codRhta = codRhta;
    }

    public Tsgrhempleados getCodRys() {
        return codRys;
    }

    public void setCodRys(Tsgrhempleados codRys) {
        this.codRys = codRys;
    }

    public Tsgrhperfiles getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(Tsgrhperfiles codPerfil) {
        this.codPerfil = codPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAsignacion != null ? codAsignacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhcartaasignacion)) {
            return false;
        }
        Tsgrhcartaasignacion other = (Tsgrhcartaasignacion) object;
        if ((this.codAsignacion == null && other.codAsignacion != null) || (this.codAsignacion != null && !this.codAsignacion.equals(other.codAsignacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhcartaasignacion[ codAsignacion=" + codAsignacion + " ]";
    }
    
}
