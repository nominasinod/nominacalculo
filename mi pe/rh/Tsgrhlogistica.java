/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhlogistica", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhlogistica.findAll", query = "SELECT t FROM Tsgrhlogistica t")})
public class Tsgrhlogistica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_logistica")
    private Integer codLogistica;
    @Basic(optional = false)
    @Column(name = "tim_horario")
    private int timHorario;
    @Column(name = "des_requerimientos")
    private String desRequerimientos;
    @Basic(optional = false)
    @Column(name = "fec_fecinicio")
    @Temporal(TemporalType.DATE)
    private Date fecFecinicio;
    @Basic(optional = false)
    @Column(name = "fec_termino")
    @Temporal(TemporalType.DATE)
    private Date fecTermino;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Basic(optional = false)
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "aud_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audCreadopor;
    @JoinColumn(name = "aud_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados audModificadopor;
    @JoinColumn(name = "cod_capacitacion", referencedColumnName = "cod_plancapacitacion")
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhplancapacitacion codCapacitacion;

    public Tsgrhlogistica() {
    }

    public Tsgrhlogistica(Integer codLogistica) {
        this.codLogistica = codLogistica;
    }

    public Tsgrhlogistica(Integer codLogistica, int timHorario, Date fecFecinicio, Date fecTermino, Date audFeccreacion, Date audFecmodificacion) {
        this.codLogistica = codLogistica;
        this.timHorario = timHorario;
        this.fecFecinicio = fecFecinicio;
        this.fecTermino = fecTermino;
        this.audFeccreacion = audFeccreacion;
        this.audFecmodificacion = audFecmodificacion;
    }

    public Integer getCodLogistica() {
        return codLogistica;
    }

    public void setCodLogistica(Integer codLogistica) {
        this.codLogistica = codLogistica;
    }

    public int getTimHorario() {
        return timHorario;
    }

    public void setTimHorario(int timHorario) {
        this.timHorario = timHorario;
    }

    public String getDesRequerimientos() {
        return desRequerimientos;
    }

    public void setDesRequerimientos(String desRequerimientos) {
        this.desRequerimientos = desRequerimientos;
    }

    public Date getFecFecinicio() {
        return fecFecinicio;
    }

    public void setFecFecinicio(Date fecFecinicio) {
        this.fecFecinicio = fecFecinicio;
    }

    public Date getFecTermino() {
        return fecTermino;
    }

    public void setFecTermino(Date fecTermino) {
        this.fecTermino = fecTermino;
    }

    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    public Tsgrhempleados getAudCreadopor() {
        return audCreadopor;
    }

    public void setAudCreadopor(Tsgrhempleados audCreadopor) {
        this.audCreadopor = audCreadopor;
    }

    public Tsgrhempleados getAudModificadopor() {
        return audModificadopor;
    }

    public void setAudModificadopor(Tsgrhempleados audModificadopor) {
        this.audModificadopor = audModificadopor;
    }

    public Tsgrhplancapacitacion getCodCapacitacion() {
        return codCapacitacion;
    }

    public void setCodCapacitacion(Tsgrhplancapacitacion codCapacitacion) {
        this.codCapacitacion = codCapacitacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codLogistica != null ? codLogistica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhlogistica)) {
            return false;
        }
        Tsgrhlogistica other = (Tsgrhlogistica) object;
        if ((this.codLogistica == null && other.codLogistica != null) || (this.codLogistica != null && !this.codLogistica.equals(other.codLogistica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhlogistica[ codLogistica=" + codLogistica + " ]";
    }
    
}
