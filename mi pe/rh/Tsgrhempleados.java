/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author José Antonio Zarco Robles, MBN
 */
@Entity
@Table(name = "tsgrhempleados", schema = "sgrh")
@XmlRootElement
public class Tsgrhempleados implements Serializable {

    @Basic(optional = false)
    @Column(name = "des_direccion")
    private String desDireccion;
    @Basic(optional = false)
    @Column(name = "fec_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fecNacimiento;
    @Basic(optional = false)
    @Column(name = "des_lugarnacimiento")
    private String desLugarnacimiento;
    @Basic(optional = false)
    @Column(name = "cod_edad")
    private int codEdad;
    @Basic(optional = false)
    @Column(name = "cod_tiposangre")
    private String codTiposangre;
    @Column(name = "cod_telefonocasa")
    private String codTelefonocasa;
    @Column(name = "cod_telefonocelular")
    private String codTelefonocelular;
    @Column(name = "cod_telemergencia")
    private String codTelemergencia;
    @Lob
    @Column(name = "bin_identificacion")
    private byte[] binIdentificacion;
    @Lob
    @Column(name = "bin_pasaporte")
    private byte[] binPasaporte;
    @Lob
    @Column(name = "bin_visa")
    private byte[] binVisa;
    @Column(name = "cod_licenciamanejo")
    private String codLicenciamanejo;
    @Basic(optional = false)
    @Column(name = "fec_ingreso")
    @Temporal(TemporalType.DATE)
    private Date fecIngreso;
    @Column(name = "cod_rfc")
    private String codRfc;
    @Column(name = "cod_nss")
    private String codNss;
    @Basic(optional = false)
    @Column(name = "cod_curp")
    private String codCurp;
    @Lob
    @Column(name = "bin_foto")
    private byte[] binFoto;
    @Column(name = "cod_tipofoto")
    private String codTipofoto;
    @Column(name = "cod_extensionfoto")
    private String codExtensionfoto;
    @Column(name = "cod_empleadoactivo")
    private Boolean codEmpleadoactivo;
    @Basic(optional = false)
    @Column(name = "cod_estatusempleado")
    private int codEstatusempleado;
    @Basic(optional = false)
    @Column(name = "cod_estadocivil")
    private int codEstadocivil;
    @Column(name = "cod_rol")
    private Integer codRol;
    @Column(name = "cod_diasvacaciones")
    private Integer codDiasvacaciones;
    @Column(name = "cod_sistemasuite")
    private Integer codSistemasuite;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhlugares> tsgrhlugaresList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhlugares> tsgrhlugaresList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhplanoperativo> tsgrhplanoperativoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhplanoperativo> tsgrhplanoperativoList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhtipocapacitacion> tsgrhtipocapacitacionList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhtipocapacitacion> tsgrhtipocapacitacionList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhproveedores> tsgrhproveedoresList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhproveedores> tsgrhproveedoresList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrolempleado> tsgrhrolempleadoList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrolempleado> tsgrhrolempleadoList1;
    @OneToMany(mappedBy = "codEvaluado", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList;
    @OneToMany(mappedBy = "codEvaluador", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codParticipante1", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList2;
    @OneToMany(mappedBy = "codParticipante2", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList3;
    @OneToMany(mappedBy = "codParticipante3", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList4;
    @OneToMany(mappedBy = "codParticipante4", fetch = FetchType.LAZY)
    private List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList5;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrelacionroles> tsgrhrelacionrolesList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrelacionroles> tsgrhrelacionrolesList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhexperienciaslaborales> tsgrhexperienciaslaboralesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhevaluaciones> tsgrhevaluacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhevaluaciones> tsgrhevaluacionesList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhescolaridad> tsgrhescolaridadList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhlogistica> tsgrhlogisticaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhlogistica> tsgrhlogisticaList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEvaluado", fetch = FetchType.LAZY)
    private List<Tsgrhevacontestadas> tsgrhevacontestadasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEvaluador", fetch = FetchType.LAZY)
    private List<Tsgrhevacontestadas> tsgrhevacontestadasList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhmodo> tsgrhmodoList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhmodo> tsgrhmodoList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhplancapacitacion> tsgrhplancapacitacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhplancapacitacion> tsgrhplancapacitacionList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList1;
    @OneToMany(mappedBy = "codParticipante1", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList2;
    @OneToMany(mappedBy = "codParticipante2", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList3;
    @OneToMany(mappedBy = "codParticipante3", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList4;
    @OneToMany(mappedBy = "codParticipante4", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList5;
    @OneToMany(mappedBy = "codParticipante5", fetch = FetchType.LAZY)
    private List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList6;
    @OneToMany(mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhempleados> tsgrhempleadosList;
    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codCreadopor;
    @OneToMany(mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhempleados> tsgrhempleadosList1;
    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhempleados codModificadopor;
    @JoinColumn(name = "cod_puesto", referencedColumnName = "cod_puesto")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhpuestos codPuesto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhprocesos> tsgrhprocesosList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhprocesos> tsgrhprocesosList1;
    @OneToMany(mappedBy = "codApe", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList2;
    @OneToMany(mappedBy = "codGpy", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList4;
    @OneToMany(mappedBy = "codRhta", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList5;
    @OneToMany(mappedBy = "codRys", fetch = FetchType.LAZY)
    private List<Tsgrhcartaasignacion> tsgrhcartaasignacionList6;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhestatuscapacitacion> tsgrhestatuscapacitacionList;
    @OneToMany(mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhestatuscapacitacion> tsgrhestatuscapacitacionList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhencuesta> tsgrhencuestaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhencuesta> tsgrhencuestaList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhcapacitaciones> tsgrhcapacitacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhevacapacitacion> tsgrhevacapacitacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "audModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhevacapacitacion> tsgrhevacapacitacionList1;
    @OneToMany(mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhevacapacitacion> tsgrhevacapacitacionList2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcontrataciones> tsgrhcontratacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcontrataciones> tsgrhcontratacionesList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCreadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcontratos> tsgrhcontratosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codModificadopor", fetch = FetchType.LAZY)
    private List<Tsgrhcontratos> tsgrhcontratosList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codAsignadopor", fetch = FetchType.LAZY)
    private List<Tsgrhasignacionesemp> tsgrhasignacionesempList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEmpleado", fetch = FetchType.LAZY)
    private List<Tsgrhasignacionesemp> tsgrhasignacionesempList1;

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_empleado")
    private Integer codEmpleado;
    @Basic(optional = false)
    @Column(name = "des_nombre")
    private String desNombre;
    @Column(name = "des_nombres")
    private String desNombres;
    @Basic(optional = false)
    @Column(name = "des_apepaterno")
    private String desApepaterno;
    @Column(name = "des_apematerno")
    private String desApematerno;
//    @Basic(optional = false)
//    @Column(name = "cod_tipofoto")
//    private String codTipofoto;
//    @Basic(optional = false)
//    @Column(name = "cod_extensionfoto")
//    private String codExtensionfoto;
//    @Basic(optional = false)
//    @Column(name = "cod_estatusempleado")
//    private int codEstatusempleado;
//    @Basic(optional = false)
//    @Column(name = "cod_estadocivil")
//    private int codEstadocivil;
//    @Basic(optional = false)
//    @Column(name = "cod_rol")
//    private int codRol;
//    @Basic(optional = false)
//    @Column(name = "cod_diasvacaciones")
//    private int codDiasvacaciones;
//    @Basic(optional = false)
//    @Column(name = "cod_sistemasuite")
//    private int codSistemasuite;
//    @Basic(optional = false)
//    @Column(name = "fec_creacion")
//    @Temporal(TemporalType.DATE)
//    private Date fecCreacion;
//    @Basic(optional = false)
//    @Column(name = "fec_modificacion")
//    @Temporal(TemporalType.DATE)
//    private Date fecModificacion;
//    @Basic(optional = false)
//    @Column(name = "cod_creadopor")
//    private int codCreadopor;
//    @Basic(optional = false)
//    @Column(name = "cod_modificadopor")
//    private int codModificadopor;
    @Column(name = "des_correo")
    private String desCorreo;
    @JoinColumn(name = "cod_area", referencedColumnName = "cod_area")
    @ManyToOne(optional = false)
    private Tsgrhareas codArea;

    public Tsgrhempleados() {
    }

    public Tsgrhempleados(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhempleados(Integer codEmpleado, String desNombre, String desApepaterno, String desDireccion, Date fecNacimiento, String desLugarnacimiento, String codTiposangre, Date fecIngreso, String codCurp, byte[] binFoto, String codTipofoto, String codExtensionfoto, int codEstatusempleado, int codEstadocivil, int codRol, int codDiasvacaciones, int codSistemasuite, Date fecCreacion, Date fecModificacion, int codCreadopor, int codModificadopor, String desCorreo) {
        this.codEmpleado = codEmpleado;
        this.desNombre = desNombre;
        this.desApepaterno = desApepaterno;
//        this.desDireccion = desDireccion;
//        this.fecNacimiento = fecNacimiento;
//        this.desLugarnacimiento = desLugarnacimiento;
//        this.codTiposangre = codTiposangre;
//        this.fecIngreso = fecIngreso;
//        this.codCurp = codCurp;
//        this.binFoto = binFoto;
//        this.codTipofoto = codTipofoto;
//        this.codExtensionfoto = codExtensionfoto;
//        this.codEstatusempleado = codEstatusempleado;
//        this.codEstadocivil = codEstadocivil;
//        this.codRol = codRol;
//        this.codDiasvacaciones = codDiasvacaciones;
//        this.codSistemasuite = codSistemasuite;
//        this.fecCreacion = fecCreacion;
//        this.fecModificacion = fecModificacion;
//        this.codCreadopor = codCreadopor;
//        this.codModificadopor = codModificadopor;
        this.desCorreo = desCorreo;
    }

    public Integer getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public String getDesNombre() {
        return desNombre;
    }

    public void setDesNombre(String desNombre) {
        this.desNombre = desNombre;
    }

    public String getDesNombres() {
        return desNombres;
    }

    public void setDesNombres(String desNombres) {
        this.desNombres = desNombres;
    }

    public String getDesApepaterno() {
        return desApepaterno;
    }

    public void setDesApepaterno(String desApepaterno) {
        this.desApepaterno = desApepaterno;
    }

    public String getDesApematerno() {
        return desApematerno;
    }

    public void setDesApematerno(String desApematerno) {
        this.desApematerno = desApematerno;
    }

//    public String getDesDireccion() {
//        return desDireccion;
//    }
//
//    public void setDesDireccion(String desDireccion) {
//        this.desDireccion = desDireccion;
//    }
//
//    public Date getFecNacimiento() {
//        return fecNacimiento;
//    }
//
//    public void setFecNacimiento(Date fecNacimiento) {
//        this.fecNacimiento = fecNacimiento;
//    }
//
//    public String getDesLugarnacimiento() {
//        return desLugarnacimiento;
//    }
//
//    public void setDesLugarnacimiento(String desLugarnacimiento) {
//        this.desLugarnacimiento = desLugarnacimiento;
//    }
//
//    public String getCodTiposangre() {
//        return codTiposangre;
//    }
//
//    public void setCodTiposangre(String codTiposangre) {
//        this.codTiposangre = codTiposangre;
//    }
//
//    public Date getFecIngreso() {
//        return fecIngreso;
//    }
//
//    public void setFecIngreso(Date fecIngreso) {
//        this.fecIngreso = fecIngreso;
//    }
//
//    public String getCodCurp() {
//        return codCurp;
//    }
//
//    public void setCodCurp(String codCurp) {
//        this.codCurp = codCurp;
//    }
//
    public byte[] getBinFoto() {
        return binFoto;
    }

    public void setBinFoto(byte[] binFoto) {
        this.binFoto = binFoto;
    }
//
//    public String getCodTipofoto() {
//        return codTipofoto;
//    }
//
//    public void setCodTipofoto(String codTipofoto) {
//        this.codTipofoto = codTipofoto;
//    }
//
//    public String getCodExtensionfoto() {
//        return codExtensionfoto;
//    }
//
//    public void setCodExtensionfoto(String codExtensionfoto) {
//        this.codExtensionfoto = codExtensionfoto;
//    }
//
//    public int getCodEstatusempleado() {
//        return codEstatusempleado;
//    }
//
//    public void setCodEstatusempleado(int codEstatusempleado) {
//        this.codEstatusempleado = codEstatusempleado;
//    }
//
//    public int getCodEstadocivil() {
//        return codEstadocivil;
//    }
//
//    public void setCodEstadocivil(int codEstadocivil) {
//        this.codEstadocivil = codEstadocivil;
//    }
//
//    public int getCodRol() {
//        return codRol;
//    }
//
//    public void setCodRol(int codRol) {
//        this.codRol = codRol;
//    }
//
//    public int getCodDiasvacaciones() {
//        return codDiasvacaciones;
//    }
//
//    public void setCodDiasvacaciones(int codDiasvacaciones) {
//        this.codDiasvacaciones = codDiasvacaciones;
//    }
//
//    public int getCodSistemasuite() {
//        return codSistemasuite;
//    }
//
//    public void setCodSistemasuite(int codSistemasuite) {
//        this.codSistemasuite = codSistemasuite;
//    }
//
//    public Date getFecCreacion() {
//        return fecCreacion;
//    }
//
//    public void setFecCreacion(Date fecCreacion) {
//        this.fecCreacion = fecCreacion;
//    }
//
//    public Date getFecModificacion() {
//        return fecModificacion;
//    }
//
//    public void setFecModificacion(Date fecModificacion) {
//        this.fecModificacion = fecModificacion;
//    }
//
//    public int getCodCreadopor() {
//        return codCreadopor;
//    }
//
//    public void setCodCreadopor(int codCreadopor) {
//        this.codCreadopor = codCreadopor;
//    }
//
//    public int getCodModificadopor() {
//        return codModificadopor;
//    }
//
//    public void setCodModificadopor(int codModificadopor) {
//        this.codModificadopor = codModificadopor;
//    }

    public String getDesCorreo() {
        return desCorreo;
    }

    public void setDesCorreo(String desCorreo) {
        this.desCorreo = desCorreo;
    }

    public Tsgrhareas getCodArea() {
        return codArea;
    }

    public void setCodArea(Tsgrhareas codArea) {
        this.codArea = codArea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEmpleado != null ? codEmpleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhempleados)) {
            return false;
        }
        Tsgrhempleados other = (Tsgrhempleados) object;
        if ((this.codEmpleado == null && other.codEmpleado != null) || (this.codEmpleado != null && !this.codEmpleado.equals(other.codEmpleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhempleados[ codEmpleado=" + codEmpleado + " ]";
    }

    public String getDesDireccion() {
        return desDireccion;
    }

    public void setDesDireccion(String desDireccion) {
        this.desDireccion = desDireccion;
    }

    public Date getFecNacimiento() {
        return fecNacimiento;
    }

    public void setFecNacimiento(Date fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    public String getDesLugarnacimiento() {
        return desLugarnacimiento;
    }

    public void setDesLugarnacimiento(String desLugarnacimiento) {
        this.desLugarnacimiento = desLugarnacimiento;
    }

    public int getCodEdad() {
        return codEdad;
    }

    public void setCodEdad(int codEdad) {
        this.codEdad = codEdad;
    }

    public String getCodTiposangre() {
        return codTiposangre;
    }

    public void setCodTiposangre(String codTiposangre) {
        this.codTiposangre = codTiposangre;
    }

    public String getCodTelefonocasa() {
        return codTelefonocasa;
    }

    public void setCodTelefonocasa(String codTelefonocasa) {
        this.codTelefonocasa = codTelefonocasa;
    }

    public String getCodTelefonocelular() {
        return codTelefonocelular;
    }

    public void setCodTelefonocelular(String codTelefonocelular) {
        this.codTelefonocelular = codTelefonocelular;
    }

    public String getCodTelemergencia() {
        return codTelemergencia;
    }

    public void setCodTelemergencia(String codTelemergencia) {
        this.codTelemergencia = codTelemergencia;
    }

    public byte[] getBinIdentificacion() {
        return binIdentificacion;
    }

    public void setBinIdentificacion(byte[] binIdentificacion) {
        this.binIdentificacion = binIdentificacion;
    }

    public byte[] getBinPasaporte() {
        return binPasaporte;
    }

    public void setBinPasaporte(byte[] binPasaporte) {
        this.binPasaporte = binPasaporte;
    }

    public byte[] getBinVisa() {
        return binVisa;
    }

    public void setBinVisa(byte[] binVisa) {
        this.binVisa = binVisa;
    }

    public String getCodLicenciamanejo() {
        return codLicenciamanejo;
    }

    public void setCodLicenciamanejo(String codLicenciamanejo) {
        this.codLicenciamanejo = codLicenciamanejo;
    }

    public Date getFecIngreso() {
        return fecIngreso;
    }

    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    public String getCodRfc() {
        return codRfc;
    }

    public void setCodRfc(String codRfc) {
        this.codRfc = codRfc;
    }

    public String getCodNss() {
        return codNss;
    }

    public void setCodNss(String codNss) {
        this.codNss = codNss;
    }

    public String getCodCurp() {
        return codCurp;
    }

    public void setCodCurp(String codCurp) {
        this.codCurp = codCurp;
    }

    public byte[] getBinFoto() {
        return binFoto;
    }

    public void setBinFoto(byte[] binFoto) {
        this.binFoto = binFoto;
    }

    public String getCodTipofoto() {
        return codTipofoto;
    }

    public void setCodTipofoto(String codTipofoto) {
        this.codTipofoto = codTipofoto;
    }

    public String getCodExtensionfoto() {
        return codExtensionfoto;
    }

    public void setCodExtensionfoto(String codExtensionfoto) {
        this.codExtensionfoto = codExtensionfoto;
    }

    public Boolean getCodEmpleadoactivo() {
        return codEmpleadoactivo;
    }

    public void setCodEmpleadoactivo(Boolean codEmpleadoactivo) {
        this.codEmpleadoactivo = codEmpleadoactivo;
    }

    public int getCodEstatusempleado() {
        return codEstatusempleado;
    }

    public void setCodEstatusempleado(int codEstatusempleado) {
        this.codEstatusempleado = codEstatusempleado;
    }

    public int getCodEstadocivil() {
        return codEstadocivil;
    }

    public void setCodEstadocivil(int codEstadocivil) {
        this.codEstadocivil = codEstadocivil;
    }

    public Integer getCodRol() {
        return codRol;
    }

    public void setCodRol(Integer codRol) {
        this.codRol = codRol;
    }

    public Integer getCodDiasvacaciones() {
        return codDiasvacaciones;
    }

    public void setCodDiasvacaciones(Integer codDiasvacaciones) {
        this.codDiasvacaciones = codDiasvacaciones;
    }

    public Integer getCodSistemasuite() {
        return codSistemasuite;
    }

    public void setCodSistemasuite(Integer codSistemasuite) {
        this.codSistemasuite = codSistemasuite;
    }

    public Date getFecCreacion() {
        return fecCreacion;
    }

    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    @XmlTransient
    public List<Tsgrhlugares> getTsgrhlugaresList() {
        return tsgrhlugaresList;
    }

    public void setTsgrhlugaresList(List<Tsgrhlugares> tsgrhlugaresList) {
        this.tsgrhlugaresList = tsgrhlugaresList;
    }

    @XmlTransient
    public List<Tsgrhlugares> getTsgrhlugaresList1() {
        return tsgrhlugaresList1;
    }

    public void setTsgrhlugaresList1(List<Tsgrhlugares> tsgrhlugaresList1) {
        this.tsgrhlugaresList1 = tsgrhlugaresList1;
    }

    @XmlTransient
    public List<Tsgrhplanoperativo> getTsgrhplanoperativoList() {
        return tsgrhplanoperativoList;
    }

    public void setTsgrhplanoperativoList(List<Tsgrhplanoperativo> tsgrhplanoperativoList) {
        this.tsgrhplanoperativoList = tsgrhplanoperativoList;
    }

    @XmlTransient
    public List<Tsgrhplanoperativo> getTsgrhplanoperativoList1() {
        return tsgrhplanoperativoList1;
    }

    public void setTsgrhplanoperativoList1(List<Tsgrhplanoperativo> tsgrhplanoperativoList1) {
        this.tsgrhplanoperativoList1 = tsgrhplanoperativoList1;
    }

    @XmlTransient
    public List<Tsgrhtipocapacitacion> getTsgrhtipocapacitacionList() {
        return tsgrhtipocapacitacionList;
    }

    public void setTsgrhtipocapacitacionList(List<Tsgrhtipocapacitacion> tsgrhtipocapacitacionList) {
        this.tsgrhtipocapacitacionList = tsgrhtipocapacitacionList;
    }

    @XmlTransient
    public List<Tsgrhtipocapacitacion> getTsgrhtipocapacitacionList1() {
        return tsgrhtipocapacitacionList1;
    }

    public void setTsgrhtipocapacitacionList1(List<Tsgrhtipocapacitacion> tsgrhtipocapacitacionList1) {
        this.tsgrhtipocapacitacionList1 = tsgrhtipocapacitacionList1;
    }

    @XmlTransient
    public List<Tsgrhproveedores> getTsgrhproveedoresList() {
        return tsgrhproveedoresList;
    }

    public void setTsgrhproveedoresList(List<Tsgrhproveedores> tsgrhproveedoresList) {
        this.tsgrhproveedoresList = tsgrhproveedoresList;
    }

    @XmlTransient
    public List<Tsgrhproveedores> getTsgrhproveedoresList1() {
        return tsgrhproveedoresList1;
    }

    public void setTsgrhproveedoresList1(List<Tsgrhproveedores> tsgrhproveedoresList1) {
        this.tsgrhproveedoresList1 = tsgrhproveedoresList1;
    }

    @XmlTransient
    public List<Tsgrhrolempleado> getTsgrhrolempleadoList() {
        return tsgrhrolempleadoList;
    }

    public void setTsgrhrolempleadoList(List<Tsgrhrolempleado> tsgrhrolempleadoList) {
        this.tsgrhrolempleadoList = tsgrhrolempleadoList;
    }

    @XmlTransient
    public List<Tsgrhrolempleado> getTsgrhrolempleadoList1() {
        return tsgrhrolempleadoList1;
    }

    public void setTsgrhrolempleadoList1(List<Tsgrhrolempleado> tsgrhrolempleadoList1) {
        this.tsgrhrolempleadoList1 = tsgrhrolempleadoList1;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList() {
        return tsgrhvalidaevaluaciondesList;
    }

    public void setTsgrhvalidaevaluaciondesList(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList) {
        this.tsgrhvalidaevaluaciondesList = tsgrhvalidaevaluaciondesList;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList1() {
        return tsgrhvalidaevaluaciondesList1;
    }

    public void setTsgrhvalidaevaluaciondesList1(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList1) {
        this.tsgrhvalidaevaluaciondesList1 = tsgrhvalidaevaluaciondesList1;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList2() {
        return tsgrhvalidaevaluaciondesList2;
    }

    public void setTsgrhvalidaevaluaciondesList2(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList2) {
        this.tsgrhvalidaevaluaciondesList2 = tsgrhvalidaevaluaciondesList2;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList3() {
        return tsgrhvalidaevaluaciondesList3;
    }

    public void setTsgrhvalidaevaluaciondesList3(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList3) {
        this.tsgrhvalidaevaluaciondesList3 = tsgrhvalidaevaluaciondesList3;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList4() {
        return tsgrhvalidaevaluaciondesList4;
    }

    public void setTsgrhvalidaevaluaciondesList4(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList4) {
        this.tsgrhvalidaevaluaciondesList4 = tsgrhvalidaevaluaciondesList4;
    }

    @XmlTransient
    public List<Tsgrhvalidaevaluaciondes> getTsgrhvalidaevaluaciondesList5() {
        return tsgrhvalidaevaluaciondesList5;
    }

    public void setTsgrhvalidaevaluaciondesList5(List<Tsgrhvalidaevaluaciondes> tsgrhvalidaevaluaciondesList5) {
        this.tsgrhvalidaevaluaciondesList5 = tsgrhvalidaevaluaciondesList5;
    }

    @XmlTransient
    public List<Tsgrhrelacionroles> getTsgrhrelacionrolesList() {
        return tsgrhrelacionrolesList;
    }

    public void setTsgrhrelacionrolesList(List<Tsgrhrelacionroles> tsgrhrelacionrolesList) {
        this.tsgrhrelacionrolesList = tsgrhrelacionrolesList;
    }

    @XmlTransient
    public List<Tsgrhrelacionroles> getTsgrhrelacionrolesList1() {
        return tsgrhrelacionrolesList1;
    }

    public void setTsgrhrelacionrolesList1(List<Tsgrhrelacionroles> tsgrhrelacionrolesList1) {
        this.tsgrhrelacionrolesList1 = tsgrhrelacionrolesList1;
    }

    @XmlTransient
    public List<Tsgrhexperienciaslaborales> getTsgrhexperienciaslaboralesList() {
        return tsgrhexperienciaslaboralesList;
    }

    public void setTsgrhexperienciaslaboralesList(List<Tsgrhexperienciaslaborales> tsgrhexperienciaslaboralesList) {
        this.tsgrhexperienciaslaboralesList = tsgrhexperienciaslaboralesList;
    }

    @XmlTransient
    public List<Tsgrhevaluaciones> getTsgrhevaluacionesList() {
        return tsgrhevaluacionesList;
    }

    public void setTsgrhevaluacionesList(List<Tsgrhevaluaciones> tsgrhevaluacionesList) {
        this.tsgrhevaluacionesList = tsgrhevaluacionesList;
    }

    @XmlTransient
    public List<Tsgrhevaluaciones> getTsgrhevaluacionesList1() {
        return tsgrhevaluacionesList1;
    }

    public void setTsgrhevaluacionesList1(List<Tsgrhevaluaciones> tsgrhevaluacionesList1) {
        this.tsgrhevaluacionesList1 = tsgrhevaluacionesList1;
    }

    @XmlTransient
    public List<Tsgrhescolaridad> getTsgrhescolaridadList() {
        return tsgrhescolaridadList;
    }

    public void setTsgrhescolaridadList(List<Tsgrhescolaridad> tsgrhescolaridadList) {
        this.tsgrhescolaridadList = tsgrhescolaridadList;
    }

    @XmlTransient
    public List<Tsgrhlogistica> getTsgrhlogisticaList() {
        return tsgrhlogisticaList;
    }

    public void setTsgrhlogisticaList(List<Tsgrhlogistica> tsgrhlogisticaList) {
        this.tsgrhlogisticaList = tsgrhlogisticaList;
    }

    @XmlTransient
    public List<Tsgrhlogistica> getTsgrhlogisticaList1() {
        return tsgrhlogisticaList1;
    }

    public void setTsgrhlogisticaList1(List<Tsgrhlogistica> tsgrhlogisticaList1) {
        this.tsgrhlogisticaList1 = tsgrhlogisticaList1;
    }

    @XmlTransient
    public List<Tsgrhevacontestadas> getTsgrhevacontestadasList() {
        return tsgrhevacontestadasList;
    }

    public void setTsgrhevacontestadasList(List<Tsgrhevacontestadas> tsgrhevacontestadasList) {
        this.tsgrhevacontestadasList = tsgrhevacontestadasList;
    }

    @XmlTransient
    public List<Tsgrhevacontestadas> getTsgrhevacontestadasList1() {
        return tsgrhevacontestadasList1;
    }

    public void setTsgrhevacontestadasList1(List<Tsgrhevacontestadas> tsgrhevacontestadasList1) {
        this.tsgrhevacontestadasList1 = tsgrhevacontestadasList1;
    }

    @XmlTransient
    public List<Tsgrhmodo> getTsgrhmodoList() {
        return tsgrhmodoList;
    }

    public void setTsgrhmodoList(List<Tsgrhmodo> tsgrhmodoList) {
        this.tsgrhmodoList = tsgrhmodoList;
    }

    @XmlTransient
    public List<Tsgrhmodo> getTsgrhmodoList1() {
        return tsgrhmodoList1;
    }

    public void setTsgrhmodoList1(List<Tsgrhmodo> tsgrhmodoList1) {
        this.tsgrhmodoList1 = tsgrhmodoList1;
    }

    @XmlTransient
    public List<Tsgrhplancapacitacion> getTsgrhplancapacitacionList() {
        return tsgrhplancapacitacionList;
    }

    public void setTsgrhplancapacitacionList(List<Tsgrhplancapacitacion> tsgrhplancapacitacionList) {
        this.tsgrhplancapacitacionList = tsgrhplancapacitacionList;
    }

    @XmlTransient
    public List<Tsgrhplancapacitacion> getTsgrhplancapacitacionList1() {
        return tsgrhplancapacitacionList1;
    }

    public void setTsgrhplancapacitacionList1(List<Tsgrhplancapacitacion> tsgrhplancapacitacionList1) {
        this.tsgrhplancapacitacionList1 = tsgrhplancapacitacionList1;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList() {
        return tsgrhrevplanoperativoList;
    }

    public void setTsgrhrevplanoperativoList(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList) {
        this.tsgrhrevplanoperativoList = tsgrhrevplanoperativoList;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList1() {
        return tsgrhrevplanoperativoList1;
    }

    public void setTsgrhrevplanoperativoList1(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList1) {
        this.tsgrhrevplanoperativoList1 = tsgrhrevplanoperativoList1;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList2() {
        return tsgrhrevplanoperativoList2;
    }

    public void setTsgrhrevplanoperativoList2(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList2) {
        this.tsgrhrevplanoperativoList2 = tsgrhrevplanoperativoList2;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList3() {
        return tsgrhrevplanoperativoList3;
    }

    public void setTsgrhrevplanoperativoList3(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList3) {
        this.tsgrhrevplanoperativoList3 = tsgrhrevplanoperativoList3;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList4() {
        return tsgrhrevplanoperativoList4;
    }

    public void setTsgrhrevplanoperativoList4(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList4) {
        this.tsgrhrevplanoperativoList4 = tsgrhrevplanoperativoList4;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList5() {
        return tsgrhrevplanoperativoList5;
    }

    public void setTsgrhrevplanoperativoList5(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList5) {
        this.tsgrhrevplanoperativoList5 = tsgrhrevplanoperativoList5;
    }

    @XmlTransient
    public List<Tsgrhrevplanoperativo> getTsgrhrevplanoperativoList6() {
        return tsgrhrevplanoperativoList6;
    }

    public void setTsgrhrevplanoperativoList6(List<Tsgrhrevplanoperativo> tsgrhrevplanoperativoList6) {
        this.tsgrhrevplanoperativoList6 = tsgrhrevplanoperativoList6;
    }

    @XmlTransient
    public List<Tsgrhempleados> getTsgrhempleadosList() {
        return tsgrhempleadosList;
    }

    public void setTsgrhempleadosList(List<Tsgrhempleados> tsgrhempleadosList) {
        this.tsgrhempleadosList = tsgrhempleadosList;
    }

    public Tsgrhempleados getCodCreadopor() {
        return codCreadopor;
    }

    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
        this.codCreadopor = codCreadopor;
    }

    @XmlTransient
    public List<Tsgrhempleados> getTsgrhempleadosList1() {
        return tsgrhempleadosList1;
    }

    public void setTsgrhempleadosList1(List<Tsgrhempleados> tsgrhempleadosList1) {
        this.tsgrhempleadosList1 = tsgrhempleadosList1;
    }

    public Tsgrhempleados getCodModificadopor() {
        return codModificadopor;
    }

    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
        this.codModificadopor = codModificadopor;
    }

    public Tsgrhpuestos getCodPuesto() {
        return codPuesto;
    }

    public void setCodPuesto(Tsgrhpuestos codPuesto) {
        this.codPuesto = codPuesto;
    }

    @XmlTransient
    public List<Tsgrhprocesos> getTsgrhprocesosList() {
        return tsgrhprocesosList;
    }

    public void setTsgrhprocesosList(List<Tsgrhprocesos> tsgrhprocesosList) {
        this.tsgrhprocesosList = tsgrhprocesosList;
    }

    @XmlTransient
    public List<Tsgrhprocesos> getTsgrhprocesosList1() {
        return tsgrhprocesosList1;
    }

    public void setTsgrhprocesosList1(List<Tsgrhprocesos> tsgrhprocesosList1) {
        this.tsgrhprocesosList1 = tsgrhprocesosList1;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList() {
        return tsgrhcartaasignacionList;
    }

    public void setTsgrhcartaasignacionList(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList) {
        this.tsgrhcartaasignacionList = tsgrhcartaasignacionList;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList1() {
        return tsgrhcartaasignacionList1;
    }

    public void setTsgrhcartaasignacionList1(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList1) {
        this.tsgrhcartaasignacionList1 = tsgrhcartaasignacionList1;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList2() {
        return tsgrhcartaasignacionList2;
    }

    public void setTsgrhcartaasignacionList2(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList2) {
        this.tsgrhcartaasignacionList2 = tsgrhcartaasignacionList2;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList3() {
        return tsgrhcartaasignacionList3;
    }

    public void setTsgrhcartaasignacionList3(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList3) {
        this.tsgrhcartaasignacionList3 = tsgrhcartaasignacionList3;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList4() {
        return tsgrhcartaasignacionList4;
    }

    public void setTsgrhcartaasignacionList4(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList4) {
        this.tsgrhcartaasignacionList4 = tsgrhcartaasignacionList4;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList5() {
        return tsgrhcartaasignacionList5;
    }

    public void setTsgrhcartaasignacionList5(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList5) {
        this.tsgrhcartaasignacionList5 = tsgrhcartaasignacionList5;
    }

    @XmlTransient
    public List<Tsgrhcartaasignacion> getTsgrhcartaasignacionList6() {
        return tsgrhcartaasignacionList6;
    }

    public void setTsgrhcartaasignacionList6(List<Tsgrhcartaasignacion> tsgrhcartaasignacionList6) {
        this.tsgrhcartaasignacionList6 = tsgrhcartaasignacionList6;
    }

    @XmlTransient
    public List<TsgrhencuestaParticipantes> getTsgrhencuestaParticipantesList() {
        return tsgrhencuestaParticipantesList;
    }

    public void setTsgrhencuestaParticipantesList(List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList) {
        this.tsgrhencuestaParticipantesList = tsgrhencuestaParticipantesList;
    }

    @XmlTransient
    public List<Tsgrhestatuscapacitacion> getTsgrhestatuscapacitacionList() {
        return tsgrhestatuscapacitacionList;
    }

    public void setTsgrhestatuscapacitacionList(List<Tsgrhestatuscapacitacion> tsgrhestatuscapacitacionList) {
        this.tsgrhestatuscapacitacionList = tsgrhestatuscapacitacionList;
    }

    @XmlTransient
    public List<Tsgrhestatuscapacitacion> getTsgrhestatuscapacitacionList1() {
        return tsgrhestatuscapacitacionList1;
    }

    public void setTsgrhestatuscapacitacionList1(List<Tsgrhestatuscapacitacion> tsgrhestatuscapacitacionList1) {
        this.tsgrhestatuscapacitacionList1 = tsgrhestatuscapacitacionList1;
    }

    @XmlTransient
    public List<Tsgrhencuesta> getTsgrhencuestaList() {
        return tsgrhencuestaList;
    }

    public void setTsgrhencuestaList(List<Tsgrhencuesta> tsgrhencuestaList) {
        this.tsgrhencuestaList = tsgrhencuestaList;
    }

    @XmlTransient
    public List<Tsgrhencuesta> getTsgrhencuestaList1() {
        return tsgrhencuestaList1;
    }

    public void setTsgrhencuestaList1(List<Tsgrhencuesta> tsgrhencuestaList1) {
        this.tsgrhencuestaList1 = tsgrhencuestaList1;
    }

    @XmlTransient
    public List<Tsgrhcapacitaciones> getTsgrhcapacitacionesList() {
        return tsgrhcapacitacionesList;
    }

    public void setTsgrhcapacitacionesList(List<Tsgrhcapacitaciones> tsgrhcapacitacionesList) {
        this.tsgrhcapacitacionesList = tsgrhcapacitacionesList;
    }

    @XmlTransient
    public List<Tsgrhevacapacitacion> getTsgrhevacapacitacionList() {
        return tsgrhevacapacitacionList;
    }

    public void setTsgrhevacapacitacionList(List<Tsgrhevacapacitacion> tsgrhevacapacitacionList) {
        this.tsgrhevacapacitacionList = tsgrhevacapacitacionList;
    }

    @XmlTransient
    public List<Tsgrhevacapacitacion> getTsgrhevacapacitacionList1() {
        return tsgrhevacapacitacionList1;
    }

    public void setTsgrhevacapacitacionList1(List<Tsgrhevacapacitacion> tsgrhevacapacitacionList1) {
        this.tsgrhevacapacitacionList1 = tsgrhevacapacitacionList1;
    }

    @XmlTransient
    public List<Tsgrhevacapacitacion> getTsgrhevacapacitacionList2() {
        return tsgrhevacapacitacionList2;
    }

    public void setTsgrhevacapacitacionList2(List<Tsgrhevacapacitacion> tsgrhevacapacitacionList2) {
        this.tsgrhevacapacitacionList2 = tsgrhevacapacitacionList2;
    }

    @XmlTransient
    public List<Tsgrhcontrataciones> getTsgrhcontratacionesList() {
        return tsgrhcontratacionesList;
    }

    public void setTsgrhcontratacionesList(List<Tsgrhcontrataciones> tsgrhcontratacionesList) {
        this.tsgrhcontratacionesList = tsgrhcontratacionesList;
    }

    @XmlTransient
    public List<Tsgrhcontrataciones> getTsgrhcontratacionesList1() {
        return tsgrhcontratacionesList1;
    }

    public void setTsgrhcontratacionesList1(List<Tsgrhcontrataciones> tsgrhcontratacionesList1) {
        this.tsgrhcontratacionesList1 = tsgrhcontratacionesList1;
    }

    @XmlTransient
    public List<Tsgrhcontratos> getTsgrhcontratosList() {
        return tsgrhcontratosList;
    }

    public void setTsgrhcontratosList(List<Tsgrhcontratos> tsgrhcontratosList) {
        this.tsgrhcontratosList = tsgrhcontratosList;
    }

    @XmlTransient
    public List<Tsgrhcontratos> getTsgrhcontratosList1() {
        return tsgrhcontratosList1;
    }

    public void setTsgrhcontratosList1(List<Tsgrhcontratos> tsgrhcontratosList1) {
        this.tsgrhcontratosList1 = tsgrhcontratosList1;
    }

    @XmlTransient
    public List<Tsgrhasignacionesemp> getTsgrhasignacionesempList() {
        return tsgrhasignacionesempList;
    }

    public void setTsgrhasignacionesempList(List<Tsgrhasignacionesemp> tsgrhasignacionesempList) {
        this.tsgrhasignacionesempList = tsgrhasignacionesempList;
    }

    @XmlTransient
    public List<Tsgrhasignacionesemp> getTsgrhasignacionesempList1() {
        return tsgrhasignacionesempList1;
    }

    public void setTsgrhasignacionesempList1(List<Tsgrhasignacionesemp> tsgrhasignacionesempList1) {
        this.tsgrhasignacionesempList1 = tsgrhasignacionesempList1;
    }

}
