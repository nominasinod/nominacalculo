/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhpreguntasenc", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhpreguntasenc.findAll", query = "SELECT t FROM Tsgrhpreguntasenc t")})
public class Tsgrhpreguntasenc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_pregunta")
    private Integer codPregunta;
    @Basic(optional = false)
    @Column(name = "des_pregunta")
    private String desPregunta;
    @Column(name = "cod_tipopregunta")
    private Boolean codTipopregunta;
    @Column(name = "cod_edoeliminar")
    private Boolean codEdoeliminar;
    @JoinColumn(name = "cod_encuesta", referencedColumnName = "cod_encuesta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhencuesta codEncuesta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPregunta", fetch = FetchType.LAZY)
    private List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPregunta", fetch = FetchType.LAZY)
    private List<Tsgrhrespuestasenc> tsgrhrespuestasencList;

    public Tsgrhpreguntasenc() {
    }

    public Tsgrhpreguntasenc(Integer codPregunta) {
        this.codPregunta = codPregunta;
    }

    public Tsgrhpreguntasenc(Integer codPregunta, String desPregunta) {
        this.codPregunta = codPregunta;
        this.desPregunta = desPregunta;
    }

    public Integer getCodPregunta() {
        return codPregunta;
    }

    public void setCodPregunta(Integer codPregunta) {
        this.codPregunta = codPregunta;
    }

    public String getDesPregunta() {
        return desPregunta;
    }

    public void setDesPregunta(String desPregunta) {
        this.desPregunta = desPregunta;
    }

    public Boolean getCodTipopregunta() {
        return codTipopregunta;
    }

    public void setCodTipopregunta(Boolean codTipopregunta) {
        this.codTipopregunta = codTipopregunta;
    }

    public Boolean getCodEdoeliminar() {
        return codEdoeliminar;
    }

    public void setCodEdoeliminar(Boolean codEdoeliminar) {
        this.codEdoeliminar = codEdoeliminar;
    }

    public Tsgrhencuesta getCodEncuesta() {
        return codEncuesta;
    }

    public void setCodEncuesta(Tsgrhencuesta codEncuesta) {
        this.codEncuesta = codEncuesta;
    }

    @XmlTransient
    public List<TsgrhencuestaParticipantes> getTsgrhencuestaParticipantesList() {
        return tsgrhencuestaParticipantesList;
    }

    public void setTsgrhencuestaParticipantesList(List<TsgrhencuestaParticipantes> tsgrhencuestaParticipantesList) {
        this.tsgrhencuestaParticipantesList = tsgrhencuestaParticipantesList;
    }

    @XmlTransient
    public List<Tsgrhrespuestasenc> getTsgrhrespuestasencList() {
        return tsgrhrespuestasencList;
    }

    public void setTsgrhrespuestasencList(List<Tsgrhrespuestasenc> tsgrhrespuestasencList) {
        this.tsgrhrespuestasencList = tsgrhrespuestasencList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPregunta != null ? codPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhpreguntasenc)) {
            return false;
        }
        Tsgrhpreguntasenc other = (Tsgrhpreguntasenc) object;
        if ((this.codPregunta == null && other.codPregunta != null) || (this.codPregunta != null && !this.codPregunta.equals(other.codPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhpreguntasenc[ codPregunta=" + codPregunta + " ]";
    }
    
}
