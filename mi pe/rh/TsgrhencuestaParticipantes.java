/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhencuesta_participantes", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TsgrhencuestaParticipantes.findAll", query = "SELECT t FROM TsgrhencuestaParticipantes t")})
public class TsgrhencuestaParticipantes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_participantenc")
    private Integer codParticipantenc;
    @Column(name = "respuesta_abierta")
    private String respuestaAbierta;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;
    @JoinColumn(name = "cod_encuesta", referencedColumnName = "cod_encuesta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhencuesta codEncuesta;
    @JoinColumn(name = "cod_pregunta", referencedColumnName = "cod_pregunta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhpreguntasenc codPregunta;
    @JoinColumn(name = "cod_respuesta", referencedColumnName = "cod_respuesta")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tsgrhrespuestasenc codRespuesta;

    public TsgrhencuestaParticipantes() {
    }

    public TsgrhencuestaParticipantes(Integer codParticipantenc) {
        this.codParticipantenc = codParticipantenc;
    }

    public Integer getCodParticipantenc() {
        return codParticipantenc;
    }

    public void setCodParticipantenc(Integer codParticipantenc) {
        this.codParticipantenc = codParticipantenc;
    }

    public String getRespuestaAbierta() {
        return respuestaAbierta;
    }

    public void setRespuestaAbierta(String respuestaAbierta) {
        this.respuestaAbierta = respuestaAbierta;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public Tsgrhencuesta getCodEncuesta() {
        return codEncuesta;
    }

    public void setCodEncuesta(Tsgrhencuesta codEncuesta) {
        this.codEncuesta = codEncuesta;
    }

    public Tsgrhpreguntasenc getCodPregunta() {
        return codPregunta;
    }

    public void setCodPregunta(Tsgrhpreguntasenc codPregunta) {
        this.codPregunta = codPregunta;
    }

    public Tsgrhrespuestasenc getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(Tsgrhrespuestasenc codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codParticipantenc != null ? codParticipantenc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TsgrhencuestaParticipantes)) {
            return false;
        }
        TsgrhencuestaParticipantes other = (TsgrhencuestaParticipantes) object;
        if ((this.codParticipantenc == null && other.codParticipantenc != null) || (this.codParticipantenc != null && !this.codParticipantenc.equals(other.codParticipantenc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.TsgrhencuestaParticipantes[ codParticipantenc=" + codParticipantenc + " ]";
    }
    
}
