/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhrespuestaseva", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhrespuestaseva.findAll", query = "SELECT t FROM Tsgrhrespuestaseva t")})
public class Tsgrhrespuestaseva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_respuesta")
    private Integer codRespuesta;
    @Basic(optional = false)
    @Column(name = "des_respuesta")
    private String desRespuesta;
    @JoinColumn(name = "cod_evacontestada", referencedColumnName = "cod_evacontestada")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhevacontestadas codEvacontestada;
    @JoinColumn(name = "cod_pregunta", referencedColumnName = "cod_pregunta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhpreguntaseva codPregunta;

    public Tsgrhrespuestaseva() {
    }

    public Tsgrhrespuestaseva(Integer codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public Tsgrhrespuestaseva(Integer codRespuesta, String desRespuesta) {
        this.codRespuesta = codRespuesta;
        this.desRespuesta = desRespuesta;
    }

    public Integer getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(Integer codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getDesRespuesta() {
        return desRespuesta;
    }

    public void setDesRespuesta(String desRespuesta) {
        this.desRespuesta = desRespuesta;
    }

    public Tsgrhevacontestadas getCodEvacontestada() {
        return codEvacontestada;
    }

    public void setCodEvacontestada(Tsgrhevacontestadas codEvacontestada) {
        this.codEvacontestada = codEvacontestada;
    }

    public Tsgrhpreguntaseva getCodPregunta() {
        return codPregunta;
    }

    public void setCodPregunta(Tsgrhpreguntaseva codPregunta) {
        this.codPregunta = codPregunta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRespuesta != null ? codRespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhrespuestaseva)) {
            return false;
        }
        Tsgrhrespuestaseva other = (Tsgrhrespuestaseva) object;
        if ((this.codRespuesta == null && other.codRespuesta != null) || (this.codRespuesta != null && !this.codRespuesta.equals(other.codRespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhrespuestaseva[ codRespuesta=" + codRespuesta + " ]";
    }
    
}
