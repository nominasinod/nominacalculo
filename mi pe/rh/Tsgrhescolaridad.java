/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mipe
 */
@Entity
@Table(name = "tsgrhescolaridad", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhescolaridad.findAll", query = "SELECT t FROM Tsgrhescolaridad t")})
public class Tsgrhescolaridad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgrh")})
    @GeneratedValue(generator = "argId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_escolaridad")
    private Integer codEscolaridad;
    @Basic(optional = false)
    @Column(name = "des_nbinstitucion")
    private String desNbinstitucion;
    @Basic(optional = false)
    @Column(name = "des_nivelestudios")
    private String desNivelestudios;
    @Basic(optional = false)
    @Column(name = "cod_titulo")
    private boolean codTitulo;
    @Basic(optional = false)
    @Column(name = "fec_inicio")
    @Temporal(TemporalType.DATE)
    private Date fecInicio;
    @Basic(optional = false)
    @Column(name = "fec_termino")
    @Temporal(TemporalType.DATE)
    private Date fecTermino;
    @Lob
    @Column(name = "bin_titulo")
    private byte[] binTitulo;
    @JoinColumn(name = "cod_empleado", referencedColumnName = "cod_empleado")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tsgrhempleados codEmpleado;

    public Tsgrhescolaridad() {
    }

    public Tsgrhescolaridad(Integer codEscolaridad) {
        this.codEscolaridad = codEscolaridad;
    }

    public Tsgrhescolaridad(Integer codEscolaridad, String desNbinstitucion, String desNivelestudios, boolean codTitulo, Date fecInicio, Date fecTermino) {
        this.codEscolaridad = codEscolaridad;
        this.desNbinstitucion = desNbinstitucion;
        this.desNivelestudios = desNivelestudios;
        this.codTitulo = codTitulo;
        this.fecInicio = fecInicio;
        this.fecTermino = fecTermino;
    }

    public Integer getCodEscolaridad() {
        return codEscolaridad;
    }

    public void setCodEscolaridad(Integer codEscolaridad) {
        this.codEscolaridad = codEscolaridad;
    }

    public String getDesNbinstitucion() {
        return desNbinstitucion;
    }

    public void setDesNbinstitucion(String desNbinstitucion) {
        this.desNbinstitucion = desNbinstitucion;
    }

    public String getDesNivelestudios() {
        return desNivelestudios;
    }

    public void setDesNivelestudios(String desNivelestudios) {
        this.desNivelestudios = desNivelestudios;
    }

    public boolean getCodTitulo() {
        return codTitulo;
    }

    public void setCodTitulo(boolean codTitulo) {
        this.codTitulo = codTitulo;
    }

    public Date getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Date fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Date getFecTermino() {
        return fecTermino;
    }

    public void setFecTermino(Date fecTermino) {
        this.fecTermino = fecTermino;
    }

    public byte[] getBinTitulo() {
        return binTitulo;
    }

    public void setBinTitulo(byte[] binTitulo) {
        this.binTitulo = binTitulo;
    }

    public Tsgrhempleados getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(Tsgrhempleados codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEscolaridad != null ? codEscolaridad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhescolaridad)) {
            return false;
        }
        Tsgrhescolaridad other = (Tsgrhescolaridad) object;
        if ((this.codEscolaridad == null && other.codEscolaridad != null) || (this.codEscolaridad != null && !this.codEscolaridad.equals(other.codEscolaridad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhescolaridad[ codEscolaridad=" + codEscolaridad + " ]";
    }
    
}
