DROP FUNCTION sgnom.prueba();
CREATE OR REPLACE FUNCTION sgnom.prueba() RETURNS TABLE(despuesto character varying, nomcompleto text, desnbarea  character varying)
    LANGUAGE plpgsql
    AS $$

BEGIN 
RETURN QUERY 

SELECT puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, 
sgrh.tsgrhempleados AS rh_emp, 
sgrh.tsgrhpuestos AS puesto, 
sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
--AND rh_emp.cod_empleadoactivo = 't'
--AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
--AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;


END;
$$;

------------------------------------------------------------------------------------

Select sgnom.prueba();
Select * from sgnom.prueba();


SELECT 
puesto.des_puesto AS puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nombre,
	area.des_nbarea AS areas
FROM 
sgrh.tsgrhareas AS area, 
sgrh.tsgrhempleados AS rh_emp, 
sgrh.tsgrhpuestos AS puesto, 
sgnom.tsgnomempleados AS nom_emp
WHERE 
area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
--AND rh_emp.cod_empleadoactivo = 't'
--AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
--AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION sgnom.buscar_detalle_empleados(
	)
    RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$BODY$;
