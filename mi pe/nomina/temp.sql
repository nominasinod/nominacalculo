-------------------------------------------------------
tsgnomempleados
-------------------------------------------------------
SELECT 	cod_empleadoid, bol_estatus, 
	imp_sueldoimss, imp_honorarios, (imp_sueldoimss + imp_honorarios) as sueldo_total
FROM  sgnom.tsgnomempleados
WHERE bol_estatus = 't'
--AND imp_sueldoimss > 500
ORDER BY 3 DESC;

-------------------------------------------------------
tsgnomquincena
-------------------------------------------------------
INSERT INTO sgnom.tsgnomquincena(
fec_inicio, fec_fin
	VALUES (SELECT date_trunc('MONTH',Timestamp '2014-06-19')::DATE, SELECT date_trunc('MONTH',Timestamp '2014-06-19')::DATE + 14);

-------------------------------------------------------

-------------------------------------------------------
SELECT sum(imp_honorarios) FROM sgnom.tsgnomempleados WHERE bol_estatus = 't'
BETWEEN (SELECT date_trunc('*MONTH*',now())::DATE) AND now();

SELECT sum(imp_sueldoimss) FROM sgnom.tsgnomempleados WHERE bol_estatus='t' 
BETWEEN (SELECT date_trunc('MONTH','2014-06-19 13:26:58.484661-05')::DATE) AND now();

SELECT sum(imp_sueldoimss) total_imss FROM sgnom.tsgnomempleados WHERE bol_estatus='t' ;

SELECT (imp_sueldoimss + imp_honorarios) sueldo_total FROM sgnom.tsgnomempleados WHERE bol_estatus='t' ;

-------------------------------------------------------
calcular inicio de 1er quincena mes
-------------------------------------------------------
SELECT date_trunc('MONTH',Timestamp '2014-06-19')::DATE;

-------------------------------------------------------
calcular fin de 1er quincena mes
-------------------------------------------------------
SELECT date_trunc('MONTH',Timestamp '2014-06-19')::DATE + 14;

-------------------------------------------------------
calcular inicio de 2da quincena mes
-------------------------------------------------------
SELECT date_trunc('MONTH',Timestamp '2014-06-19')::DATE + 15;

-------------------------------------------------------
calcular fin de 2da quincena mes
-------------------------------------------------------
SELECT (date_trunc('MONTH',Timestamp '2016-08-29')::DATE -1) +'1 month'::interval;

select (date_trunc('month', now()) + interval '1 month') - interval '1 day';

-------------------------------------------------------
Genera fechas cada 14 dias
-------------------------------------------------------
select quincena::date 
into table fecha
from generate_series('2016-01-01', '2019-06-30', '14 day'::interval) quincena


--select 'abc' ~* '^[[:xdigit:]]+$'; 
--SELECT REGEXP_REPLACE('89ABC12345xyz','[[:digit:]]','','g');
--SELECT REGEXP_REPLACE('ABC12345xyz','[[:alpha:]]','.','g');
--SELECT REGEXP_REPLACE('John A Doe','(.*) (.*) (.*)','\1 \3, \1'); 
--select current_date, current_date - interval '15 day' as hace_10_dias;



--SELECT date_trunc('MONTH', current_date)::DATE;

--SELECT date_trunc('minute', TIMESTAMP '2017-03-17 02:09:30');