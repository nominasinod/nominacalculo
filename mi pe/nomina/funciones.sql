CREATE OR REPLACE FUNCTION sgnom.suma(num1 int, num2 int) RETURNS int
as
$$

SELECT num1 + num2;

$$
Language SQL


CREATE VIEW 