CREATE OR REPLACE FUNCTION 
sgnom.creaNomina(nombre character varying, quincena integer, tipo integer,
estatus integer, codcreado integer) 
RETURNS boolean AS $$
 DECLARE
 ultimoid integer;
 empleados integer[];
 rec RECORD;
 BEGIN
   --SELECT cod_empleadoid INTO empleados FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't';
   INSERT INTO sgnom.tsgnomcabecera(cod_nbnomina, fec_creacion, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_feccreacion)
			VALUES (nombre, CURRENT_DATE, quincena, tipo, estatus, codcreado, CURRENT_DATE) 
	RETURNING cod_cabeceraid INTO ultimoid;
	
	FOR rec IN SELECT cod_empleadoid 
	FROM sgnom.tsgnomempleados 
	WHERE tsgnomempleados.bol_estatus = 't'
	LOOP
		INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp)
		VALUES (rec.cod_empleadoid, ultimoid, 't');
	END LOOP;
    RETURN true;
END;
$$ LANGUAGE plpgsql;;

SELECT sgnom.creanomina('prueba',1,1,1,10);