---------------------------------------------
--crea nomina funcion
--quincena abierta cod_estatusnomina 
---------------------------------------------
CREATE FUNCTION sgnom.creaNomina(nombre character varying, quincena integer, tipo integer,
estatus integer, codcreado integer) RETURNS boolean AS $$
 DECLARE
 ultimoid integer;
 empleados integer[];
 rec RECORD;
 BEGIN
   --SELECT cod_empleadoid INTO empleados FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't';
    INSERT INTO sgnom.tsgnomcabecera(cod_nbnomina, fec_creacion, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_feccreacion)
            VALUES (nombre, CURRENT_DATE, quincena, tipo, estatus, codcreado, CURRENT_DATE) 
    RETURNING cod_cabeceraid INTO ultimoid;
    IF found THEN
            RETURN true;
    END IF;
    FOR rec IN SELECT cod_empleadoid FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't'
    LOOP
        INSERT INTO sgnom.tsgnomempquincena(
        cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp)
        VALUES (rec.cod_empleadoid, ultimoid, 't');
        IF found THEN
            RETURN true;
        END IF;
    END LOOP;
    EXCEPTION WHEN unique_violation THEN
            -- Do nothing, and loop to try the UPDATE again.
    RETURN false;
END;
$$ LANGUAGE plpgsql;;

--SELECT sgnom.creanomina('cabecera x32',1,1,1,10)

---------------------------------------------
--crea nomina sequencias
---------------------------------------------

-- SEQUENCE: sgnom.seq_cabecera

DROP SEQUENCE sgnom.seq_cabecera;

CREATE OR REPLACE SEQUENCE sgnom.seq_cabecera
    INCREMENT 1
    START 3
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE sgnom.seq_cabecera
    OWNER TO suite;

GRANT UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;

GRANT SELECT, USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;
--nextval('sgnom.seq_cabecera'::regclass)


-- SEQUENCE: sgnom.seq_empquincena

 DROP SEQUENCE sgnom.seq_empquincena;

CREATE SEQUENCE sgnom.seq_empquincena
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE sgnom.seq_empquincena
    OWNER TO suite;

GRANT UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;

GRANT SELECT, USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;
--nextval('sgnom.seq_empquincena'::regclass)