----------------------------------------------
Tipos de nomina
----------------------------------------------
INSERT	INTO	sgnom.tsgnomtiponomina(	
	cod_tiponominaid,	cod_nomina,	bol_estatus)
VALUES
	(1,	'abierta',	true),
	(2,	'calculada',	true),
	(3,	'revision',	true),
	(4,	'validada',	true),
	(5,	'cerrada',	true);

----------------------------------------------

----------------------------------------------
SELECT *
FROM
sgnom.tsgnomempquincena
WHERE cod_cabeceraid_fk = 2
AND bol_estatusemp = '1';

----------------------------------------------
Llenan cabecera --PENDIENTE
----------------------------------------------
Select sum(imp_totpercepcion ), sum (imp_totdeduccion)
FROM
sgnom.tsgnomempquincena
WHERE cod_cabeceraid_fk = 2
AND bol_estatusemp = '1';

----------------------------------------------
Consulta y resta imp_totalemp
----------------------------------------------
SELECT (imp_totpercepcion - imp_totdeduccion) as algo
FROM
sgnom.tsgnomcabecera

---------------------------------------------
crea nomina
----------------------------------------------
CREATE OR REPLACE FUNCTION 
sgnom.creaNomina(nombre character varying, creacion date, quincena integer, tipo integer,
estatus integer, codcreado integer, feccreado date) 
RETURNS boolean AS $$
 DECLARE
 ultimoid integer;
 empleados integer[];
 rec RECORD;
 BEGIN
   --SELECT cod_empleadoid INTO empleados FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't';
   INSERT INTO sgnom.tsgnomcabecera(cod_nbnomina, fec_creacion, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_feccreacion)
			VALUES (nombre, creacion, quincena, tipo, estatus, codcreado, feccreado) 
	RETURNING cod_cabeceraid INTO ultimoid;
	
	FOR rec IN SELECT cod_empleadoid 
	FROM sgnom.tsgnomempleados 
	WHERE tsgnomempleados.bol_estatus = 't'
	LOOP
		INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp)
		VALUES (rec.cod_empleadoid, ultimoid, 't');
	END LOOP;
    RETURN true;
END;
$$ LANGUAGE plpgsql;;

SELECT sgnom.creanomina('cabecera x','2019-01-01',1,1,1,10,'2019-01-01');

----------------------------------------------
FUNCIONA
----------------------------------------------
CREATE OR REPLACE FUNCTION 
sgnom.creaNomina(nombre character varying, quincena integer, tipo integer,
estatus integer, codcreado integer) 
RETURNS boolean AS $$
 DECLARE
 ultimoid integer;
 empleados integer[];
 rec RECORD;
 BEGIN
   --SELECT cod_empleadoid INTO empleados FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't';
   INSERT INTO sgnom.tsgnomcabecera(cod_nbnomina, fec_creacion, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_feccreacion)
			VALUES (nombre, CURRENT_DATE, quincena, tipo, estatus, codcreado, CURRENT_DATE) 
	RETURNING cod_cabeceraid INTO ultimoid;
	
	FOR rec IN SELECT cod_empleadoid FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't'
	LOOP
		INSERT INTO sgnom.tsgnomempquincena(cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp)
		VALUES (rec.cod_empleadoid, ultimoid, 't');
	END LOOP;
    RETURN true;
END;
$$ LANGUAGE plpgsql;;

SELECT sgnom.creanomina('prueba',1,1,1,10);
----------------------------------------------

----------------------------------------------
-- SEQUENCE: sgnom.seq_cabecera

-- DROP SEQUENCE sgnom.seq_cabecera;

CREATE OR REPLACE SEQUENCE sgnom.seq_cabecera
    INCREMENT 1
    START 3
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE sgnom.seq_cabecera
    OWNER TO suite;

GRANT UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;

GRANT SELECT, USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;
--nextval('sgnom.seq_cabecera'::regclass)


-- SEQUENCE: sgnom.seq_empquincena

-- DROP SEQUENCE sgnom.seq_empquincena;

CREATE SEQUENCE sgnom.seq_empquincena
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE sgnom.seq_empquincena
    OWNER TO suite;

GRANT UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;

GRANT SELECT, USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;
--nextval('sgnom.seq_empquincena'::regclass)

----------------------------------------------

----------------------------------------------