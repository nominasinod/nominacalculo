--tablas
sgnom.tsgnomejercicio


sgnom.tsgnomestatusnom


sgnom.tsgnomtiponomina


sgnom.tsgnomquincena


sgnom.tsgnomcabecera


--Quincena

INSERT INTO sgnom.tsgnomejercicio(
	cod_ejercicioid, cnu_valorejercicio, bol_estatus)
	VALUES (1, 1970, true);
	
SELECT * FROM sgnom.tsgnomejercicio;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomestatusnom(
	cod_estatusnomid, cod_estatusnomina, bol_estatus)
	VALUES (1, 'validada', true);

SELECT * FROM sgnom.tsgnomestatusnom;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomtiponomina(
	cod_tiponominaid, cod_nomina, bol_estatus)
	VALUES (1, 'pruebas', true);
	
SELECT * FROM sgnom.tsgnomtiponomina;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomquincena(
    cod_quincenaid, des_quincena, 
	fec_inicio, fec_fin, fec_pago, fec_dispersion,
    cnu_numquincena, cod_ejercicioid_fk, bol_estatus)
    VALUES (1, 'test', 
	'2018-01-01', '2018-01-01', '2018-01-01', '2018-01-01',
    1, 1, true);
	
SELECT * FROM sgnom.tsgnomquincena;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomcabecera(
	cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, 
	imp_totpercepcion, imp_totdeduccion, imp_totalemp, 
	cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, 
	aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion,
	cnu_totalemp)
	VALUES (1, 1, '2018-12-12', '2018-12-12', '2018-12-12', 
			123, 321, 1234, 
			1, 1, 1, 
			10,10,'2018-12-12', '2018-12-12', 10);
			
SELECT * FROM sgnom.tsgnomcabecera;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomempleados(
	cod_empleadoid, fec_ingreso, fec_salida, 
	bol_estatus, cod_empleado_fk, imp_sueldoimss, 
	imp_honorarios, imp_finiquito, cod_tipoimss, 
	cod_tipohonorarios, cod_banco, cod_sucursal, 
	cod_cuenta, cod_clave, txt_descripcionbaja, 
	aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, 
	aud_fecmodificacion)
	VALUES (1, '2019-05-07', '2019-05-10', 
			true, 10, 123, 
			213, 123, '1', 
			'o', 123, 321, 
			12345, 12345, 'baja', 
			10, '2190-12-21', 10, 
			'1990-12-21');
			
SELECT * FROM sgnom.tsgnomempleados;
-------------------------------------------------------------------------

INSERT INTO sgnom.tsgnomempquincena(
	cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, 
	imp_totpercepcion, imp_totdeduccion, imp_totalemp, 
	bol_estatusemp)
	VALUES (1, 1, 1,
			213.12, 123.321, 2134.32,
			true);
			
SELECT * FROM sgnom.tsgnomempquincena;
-------------------------------------------------------------------------