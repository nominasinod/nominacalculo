CREATE USER suite WITH
  LOGIN
  SUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  REPLICATION;

COMMENT ON ROLE suite IS 'Usuario principal de la base de datos de la suite de desarrollo.';
--------------------------------
--CREATE TABLESPACE TBS_suite
--  OWNER suite
--  LOCATION 'C:\TBS_SUITE';
  -------------------------------

CREATE DATABASE suite
    WITH
    OWNER = suite
    ENCODING = 'UTF8'
    --LC_COLLATE = 'Spanish_Mexico.1252'
    --LC_CTYPE = 'Spanish_Mexico.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
------------------------------------
--si llega a marcar error de region ejecutar la sentencia comentada
--CREATE DATABASE suite
--    WITH
--    OWNER = suite
--    ENCODING = 'UTF8'
--    TABLESPACE = pg_default
--    CONNECTION LIMIT = -1;
--------------------------------

ALTER DEFAULT PRIVILEGES
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT ALL ON TABLES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT USAGE, SELECT ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT USAGE, SELECT ON SEQUENCES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO PUBLIC;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO PUBLIC;

CREATE SCHEMA sgrt
    AUTHORIZATION suite;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

GRANT ALL ON SCHEMA sgrt TO suite;

SET ROLE TO suite;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';




--####################  SCHEMA   ###################
--
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: suite
--

--CREATE SCHEMA sgco;


--ALTER SCHEMA sgco OWNER TO suite;

--
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: suite
--

--COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';
----------------------------------------------------------------------------------

CREATE SCHEMA sgco
    AUTHORIZATION suite;

COMMENT ON SCHEMA sgco
    IS 'Sistema de Gestion de Conocimiento de la Organizacion.';

GRANT ALL ON SCHEMA sgco TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;
-----------------------------
--secuencias
-----------------------------
CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO suite;

--
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO suite;

--
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;


--####################  TABLAS   ###################

--
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO suite;

--
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO suite;

--
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: suite
--
CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO suite;


--###############	VALORES DE SECUENCIA   ###################
--
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);



--###############	RESTRICCIONES   ###################
--
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
  ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);

--
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);



--###############	LLAVES PRIMARIAS   ###################
--
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);

--
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);

--
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);




CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);

ALTER TYPE sgrt.destinatario OWNER TO suite;

--
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);

ALTER TYPE sgrt.edoticket OWNER TO suite;

--
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);

ALTER TYPE sgrt.encriptacion OWNER TO suite;

--
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);

ALTER TYPE sgrt.estatus OWNER TO suite;

--
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);

ALTER TYPE sgrt.estatus_compromiso OWNER TO suite;

--
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);

ALTER TYPE sgrt.modulo OWNER TO suite;

--
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);

ALTER TYPE sgrt.origencontac OWNER TO suite;

--
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);

ALTER TYPE sgrt.prioridad OWNER TO suite;

--
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);

ALTER TYPE sgrt.protocolo OWNER TO suite;

--
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);

ALTER TYPE sgrt.tipo OWNER TO suite;

--
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: suite
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);

ALTER TYPE sgrt.tipo_compromiso OWNER TO suite;


CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_agenda OWNER TO suite;

--
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_archivo OWNER TO suite;

--
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_asistente OWNER TO suite;

--
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_attach OWNER TO suite;

--
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_categoriafaq OWNER TO suite;

--
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


ALTER TABLE sgrt.seq_chat OWNER TO suite;

--
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ciudad OWNER TO suite;

--
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsagenda OWNER TO suite;

--
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsreunion OWNER TO suite;

--
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_compromiso OWNER TO suite;

--
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_contacto OWNER TO suite;

--
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_correo OWNER TO suite;

--
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_depto OWNER TO suite;

--
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_edoacuerdo OWNER TO suite;

--
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_elemento OWNER TO suite;

--
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_estadorep OWNER TO suite;

--
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_faq OWNER TO suite;

--
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_grupo OWNER TO suite;

--
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_invitado OWNER TO suite;

--
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_lugar OWNER TO suite;

--
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_mensaje OWNER TO suite;

--
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_nota OWNER TO suite;

--
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_plantillacorreo OWNER TO suite;

--
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_prioridad OWNER TO suite;

--
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_resp OWNER TO suite;

--
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuesta OWNER TO suite;

--
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_reunion OWNER TO suite;

--
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_servicio OWNER TO suite;

--
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_solicitud OWNER TO suite;

--
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ticket OWNER TO suite;

--
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: suite
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_topico OWNER TO suite;



--####################  TABLAS   ###################
--
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtagenda OWNER TO suite;

--
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtarchivos OWNER TO suite;

--
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


ALTER TABLE sgrt.tsgrtasistentes OWNER TO suite;

--
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


ALTER TABLE sgrt.tsgrtattchticket OWNER TO suite;

--
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtayudatopico OWNER TO suite;

--
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcategoriafaq OWNER TO suite;

--
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


ALTER TABLE sgrt.tsgrtchat OWNER TO suite;

--
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


ALTER TABLE sgrt.tsgrtciudades OWNER TO suite;

--
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosagenda OWNER TO suite;

--
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosreunion OWNER TO suite;

--
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


ALTER TABLE sgrt.tsgrtcompromisos OWNER TO suite;

--
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcorreo OWNER TO suite;

--
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtdatossolicitud OWNER TO suite;

--
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtdepartamento OWNER TO suite;

--
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtedosolicitudes OWNER TO suite;

--
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


ALTER TABLE sgrt.tsgrtelementos OWNER TO suite;

--
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


ALTER TABLE sgrt.tsgrtestados OWNER TO suite;

--
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtfaq OWNER TO suite;

--
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtgrupo OWNER TO suite;

--
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


ALTER TABLE sgrt.tsgrtinvitados OWNER TO suite;

--
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


ALTER TABLE sgrt.tsgrtlugares OWNER TO suite;

--
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtmsjticket OWNER TO suite;

--
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtnota OWNER TO suite;

--
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


ALTER TABLE sgrt.tsgrtplantillacorreos OWNER TO suite;

--
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtprioridad OWNER TO suite;

--
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtresppredefinida OWNER TO suite;

--
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtrespuesta OWNER TO suite;

--
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


ALTER TABLE sgrt.tsgrtreuniones OWNER TO suite;

--
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


ALTER TABLE sgrt.tsgrtservicios OWNER TO suite;

--
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


ALTER TABLE sgrt.tsgrtsolicitudservicios OWNER TO suite;

--
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: suite
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


ALTER TABLE sgrt.tsgrtticket OWNER TO suite;


--###############	VALORES DE SECUENCIA   ###################
--
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: suite
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);



--###############	RESTRICCIONES   ###################
--
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);

--
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);

--
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);

--
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);

--
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);

--
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);

--
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);

--
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);



--###############	LLAVES PRIMARIAS   ###################
--
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);

--
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);

--
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);

--
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);

--
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);

--
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);

--
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);

--
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);

--
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: suite
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);



--####################  INDICES   ###################
--
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: suite
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


CREATE SCHEMA sgrh
    AUTHORIZATION suite;

COMMENT ON SCHEMA sgrh
    IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';

GRANT ALL ON SCHEMA sgrh TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrh
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrh
GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrh
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrh
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

SET ROLE TO suite;

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);

ALTER TYPE sgrh.edo_encuesta OWNER TO suite;


--####################  SECUENCIAS  ###################
--
CREATE SEQUENCE sgrh.seq_plancapacitacion
  START WITH 1

  INCREMENT BY 1

  NO MINVALUE

  NO MAXVALUE

  CACHE 1;

CREATE SEQUENCE sgrh.seq_tiposcapacitaciones
  START WITH 1

  INCREMENT BY 1

  NO MINVALUE

  NO MAXVALUE

  CACHE 1;

CREATE SEQUENCE sgrh.seq_proceso
  START WITH 1

  INCREMENT BY 1

  NO MINVALUE

  NO MAXVALUE

  CACHE 1;


-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO suite;

--
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO suite;

--
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO suite;

--
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO postgres;

--
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO postgres;

--
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO suite;

--
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO suite;

--
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO suite;

--
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO suite;

--
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO suite;

--
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO suite;

--
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO suite;

--
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO suite;

--
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO suite;

--
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO suite;

--
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO suite;

--
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO suite;

--
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO suite;

--
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO suite;

--
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO suite;

--
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO suite;

--
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO suite;

--
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO suite;

--
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO suite;

--
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO suite;

--
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO suite;



--####################  TABLAS   ###################
--

CREATE TABLE sgrh.tsgrhplancapacitacion
(
    Cod_plancapacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_plancapacitacion'),
    Des_nombre character varying (50) NOT NULL,
    Des_tipoplancapa character varying (10),
    Cod_capacitacion integer,
    Des_criterios character varying (200),
    Des_roles character varying (200),
    Cod_proceso integer,
    Des_instructor character varying (50),
    Des_proveedor character varying (50),
  Fec_fecha date,
  Des_lugar character varying (50),
  Cnu_numeroasistentes integer,
  Des_estado character varying(10) default '--',
  Des_defecto_nombre character varying (200),
  Des_defecto_tipo character varying (200),
  Des_defecto_capacitacion character varying (200),
  Des_defecto_criterios character varying (200),
  Des_defecto_roles character varying (200),
  Des_defecto_proceso character varying (200),
  Des_defecto_instructor character varying (200),
  Des_defecto_provedor character varying (200),
  Des_defecto_logistica character varying (200),
  Des_defecto_calendario character varying (200),
  fec_creacion date NOT NULL,
    fec_modificacion date,
    cod_creadopor integer not null,
    cod_modificadopor integer,

     CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (Cod_plancapacitacion)
);



CREATE TABLE sgrh.tsgrhtiposcapacitaciones
(
    Cod_capacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_tiposcapacitaciones'::regclass),
    Des_capacitacion character varying (30) NOT NULL,
     CONSTRAINT tsgrhtiposcapacitaciones_pkey PRIMARY KEY (Cod_capacitacion)
);

CREATE TABLE sgrh.tsgrhprocesocapacitacion
(
    Cod_proceso integer NOT NULL DEFAULT nextval('sgrh.seq_proceso'::regclass),
    Des_proceso character varying (30) NOT NULL,
     CONSTRAINT tsgrhproceso_pkey PRIMARY KEY (Cod_proceso)
);

-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer
);


ALTER TABLE sgrh.tsgrhareas OWNER TO suite;

--
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO suite;

--
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO suite;

--
-- Name: tsgrhcatencuestaparticipantes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcatencuestaparticipantes (
    cod_cat_participante integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_participante integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatencuestaparticipantes OWNER TO suite;

--
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO suite;

--
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO suite;

--
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO suite;

--
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO suite;

--
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO suite;

--
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO suite;

--
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO suite;

--
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO suite;

--
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO suite;

--
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO suite;

--
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO suite;

--
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO suite;

--
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO suite;

--
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO suite;

--
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO suite;

--
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO suite;

--
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO suite;

--
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO suite;

--
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO suite;

--
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO suite;

--
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO suite;

--
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO suite;

--
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO postgres;

--
-- Name: tsgrhevacapacitacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacapacitacionesemp (
    cod_evaluacioncap integer NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhevacapacitacionesemp OWNER TO postgres;




--###############	VALORES DE SECUENCIA   ###################
--
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_area', 5, true);


--
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 9, true);


--
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 8, true);


--
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 1, false);


--
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 13, true);


--
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 7, true);


--
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 1, false);


--
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 1, false);


--
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 106, true);


--
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 4, true);


--
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 411, true);


--
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);



--###############	RESTRICCIONES   ###################

--
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);

--
-- TOC entry 3207 (class 2606 OID 74724)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);



--###############	LLAVES PRIMARIAS   ###################
--
-- Name: tsgrhcatencuestaparticipantes tsgrh_cat_encuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
    ADD CONSTRAINT tsgrh_cat_encuesta_participantes_pkey PRIMARY KEY (cod_cat_participante);


--
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);

--
-- TOC entry 3207 (class 2606 OID 82929)
-- Name: tsgrhevacapacitacionesemp pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evaluacioncap);

    CREATE SCHEMA sisat
  	    AUTHORIZATION suite;

  	COMMENT ON SCHEMA sisat
  	    IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';

  	ALTER DEFAULT PRIVILEGES IN SCHEMA sisat
  	GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

  	ALTER DEFAULT PRIVILEGES IN SCHEMA sisat
  	GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

  	ALTER DEFAULT PRIVILEGES IN SCHEMA sisat
  	GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

  	ALTER DEFAULT PRIVILEGES IN SCHEMA sisat
  	GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

  	GRANT ALL ON SCHEMA sisat TO suite;

  	SET ROLE TO suite;


  	CREATE SEQUENCE sisat.seq_aceptaciones
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_aceptaciones OWNER TO suite;

  	--
  	-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_asignaciones
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_asignaciones OWNER TO suite;

  	--
  	-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_candidatos
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_candidatos OWNER TO suite;

  	--
  	-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_cartaasignaciones
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_cartaasignaciones OWNER TO suite;

  	--
  	-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_cotizaciones
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_cotizaciones OWNER TO suite;

  	--
  	-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_cursos
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_cursos OWNER TO suite;

  	--
  	-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_entrevistas
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_entrevistas OWNER TO suite;

  	--
  	-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_envios
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_envios OWNER TO suite;

  	--
  	-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_escolaridad
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_escolaridad OWNER TO suite;

  	--
  	-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_experiencias
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_experiencias OWNER TO suite;

  	--
  	-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_firmas
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_firmas OWNER TO suite;

  	--
  	-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_habilidades
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_habilidades OWNER TO suite;

  	--
  	-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_idiomas
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_idiomas OWNER TO suite;

  	--
  	-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_ordenservicios
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_ordenservicios OWNER TO suite;

  	--
  	-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_prospectos
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_prospectos OWNER TO suite;

  	--
  	-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_proyectos
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_proyectos OWNER TO suite;

  	--
  	-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: suite
  	--

  	CREATE SEQUENCE sisat.seq_vacantes
  	    START WITH 1
  	    INCREMENT BY 1
  	    NO MINVALUE
  	    NO MAXVALUE
  	    CACHE 1;


  	ALTER TABLE sisat.seq_vacantes OWNER TO suite;



  	--####################  TABLAS   ###################
  	--
  	-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatasignaciones (
  	    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    cod_perfil integer NOT NULL,
  	    cod_cliente integer NOT NULL,
  	    des_correocte character varying(40) NOT NULL,
  	    cod_telefonocte character varying(16) NOT NULL,
  	    des_direccioncte character varying(200),
  	    cod_empleado integer,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatasignaciones OWNER TO suite;

  	--
  	-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatcandidatos (
  	    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
  	    des_nombre character varying(50) NOT NULL,
  	    cod_perfil integer NOT NULL,
  	    imp_sueldo numeric(6,2),
  	    imp_sueldodia numeric(6,2),
  	    imp_nominaimss numeric(6,2),
  	    imp_honorarios numeric(6,2),
  	    imp_cargasocial numeric(6,2),
  	    imp_prestaciones numeric(6,2),
  	    imp_viaticos numeric(6,2),
  	    imp_subtotalcandidato numeric(6,2),
  	    imp_costoadmin numeric(6,2),
  	    cnu_financiamiento smallint,
  	    imp_isr numeric(6,2),
  	    imp_financiamiento numeric(6,2),
  	    imp_adicionales numeric(6,2),
  	    imp_subtotaladmin1 numeric(6,2),
  	    imp_comisiones numeric(6,2),
  	    imp_otrosgastos numeric(6,2),
  	    imp_subtotaladmin2 numeric(6,2),
  	    imp_total numeric(6,2),
  	    imp_iva numeric(6,2),
  	    por_utilidad numeric(4,2),
  	    imp_utilidad numeric(6,2),
  	    imp_tarifa numeric(6,2)
  	);


  	ALTER TABLE sisat.tsisatcandidatos OWNER TO suite;

  	--
  	-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatcartaaceptacion (
  	    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
  	    des_objetivo character varying(200) NOT NULL,
  	    txt_oferta text,
  	    des_esquema character varying(30) NOT NULL,
  	    tim_jornada time without time zone,
  	    txt_especificaciones text,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO suite;

  	--
  	-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatcartaasignacion (
  	    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    cod_perfil integer NOT NULL,
  	    des_actividades character varying(200),
  	    des_lugarsalida character varying(100),
  	    des_lugarllegada character varying(100),
  	    fec_salida date,
  	    fec_llegada date,
  	    cod_transporte character varying(20),
  	    des_lugarhopedaje character varying(60),
  	    fec_hospedaje date,
  	    des_computadora character varying(150),
  	    cod_telefono character varying(16),
  	    des_accesorios character varying(150),
  	    des_nbresponsable character varying(50),
  	    des_nbpuesto character varying(50),
  	    des_lugarresp character varying(100),
  	    cod_telefonoresp character varying(16),
  	    tim_horario time without time zone,
  	    fec_iniciocontra date,
  	    fec_terminocontra date,
  	    imp_sueldomensual numeric(6,2),
  	    imp_nominaimss numeric(6,2),
  	    imp_honorarios numeric(6,2),
  	    imp_otros numeric(6,2),
  	    cod_rfc character varying(13),
  	    des_razonsocial character varying(45),
  	    des_correo character varying(50),
  	    cod_cpostal integer,
  	    des_direccionfact character varying(200),
  	    cod_cliente integer NOT NULL,
  	    cod_gpy integer,
  	    cod_rhta integer,
  	    cod_ape integer,
  	    cod_rys integer,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatcartaasignacion OWNER TO suite;

  	--
  	-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatcotizaciones (
  	    cod_cotizacion integer DEFAULT nextval('sisat.seq_cotizaciones'::regclass) NOT NULL,
  	    des_nbciudad character varying(20),
  	    des_nbestado character varying(20),
  	    fec_fecha date,
  	    des_nbcontacto character varying(50),
  	    cod_puesto integer,
  	    des_compania character varying(50),
  	    des_nbservicio character varying(50),
  	    cnu_cantidad smallint,
  	    txt_concepto text,
  	    imp_inversionhr numeric(6,2),
  	    txt_condicionescomer text,
  	    des_nbatentamente character varying(60),
  	    des_correoatentamente character varying(50),
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatcotizaciones OWNER TO suite;

  	--
  	-- Name: tsisatcursosycerticados; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatcursosycerticados (
  	    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    des_curso character varying(100) NOT NULL,
  	    des_institucion character varying(70) NOT NULL,
  	    fec_termino date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatcursosycerticados OWNER TO suite;

  	--
  	-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatentrevistas (
  	    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
  	    des_nbentrevistador character varying(90) NOT NULL,
  	    des_puesto character varying(50) NOT NULL,
  	    des_correoent character varying(40) NOT NULL,
  	    cod_telefonoent character varying(16) NOT NULL,
  	    des_direccionent character varying(200),
  	    tim_horarioent time without time zone,
  	    fec_fechaent date NOT NULL,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatentrevistas OWNER TO suite;

  	--
  	-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatenviocorreos (
  	    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
  	    des_destinatario character varying(90) NOT NULL,
  	    des_asunto character varying(50) NOT NULL,
  	    des_mensaje character varying(200) NOT NULL,
  	    bin_adjunto bytea,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatenviocorreos OWNER TO suite;

  	--
  	-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatescolaridad (
  	    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    des_escolaridad character varying(45) NOT NULL,
  	    des_escuela character varying(70) NOT NULL,
  	    fec_inicio date NOT NULL,
  	    fec_termino date NOT NULL,
  	    cod_estatus character varying(20)
  	);


  	ALTER TABLE sisat.tsisatescolaridad OWNER TO suite;

  	--
  	-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatexperienciaslaborales (
  	    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    des_empresa character varying(50) NOT NULL,
  	    des_puesto character varying(40) NOT NULL,
  	    fec_inicio date,
  	    fec_termino date,
  	    des_ubicacion character varying(70),
  	    txt_funciones text,
  	    des_nbcliente character varying(70),
  	    des_proyecto character varying(70),
  	    txt_logros character varying(300)
  	);


  	ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO suite;

  	--
  	-- Name: tsisatfirmas; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatfirmas (
  	    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
  	    cod_solicita integer,
  	    cod_puestosolicita integer,
  	    cod_autoriza integer,
  	    cod_puestoautoriza integer,
  	    cod_contratacion integer NOT NULL,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatfirmas OWNER TO suite;

  	--
  	-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisathabilidades (
  	    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    des_habilidad1 character varying(50),
  	    des_habilidad2 character varying(50),
  	    des_habilidad3 character varying(50),
  	    des_habilidad4 character varying(50),
  	    des_habilidad5 character varying(50),
  	    des_habilidad6 character varying(50),
  	    des_habilidad7 character varying(50),
  	    des_habilidad8 character varying(50),
  	    des_habilidad9 character varying(50),
  	    des_habilidad10 character varying(50),
  	    des_habilidad11 character varying(50),
  	    des_habilidad12 character varying(50),
  	    des_habilidad13 character varying(50),
  	    des_habilidad14 character varying(50),
  	    des_habilidad15 character varying(50),
  	    des_habilidad16 character varying(50),
  	    des_habilidad17 character varying(50),
  	    des_habilidad18 character varying(50),
  	    des_habilidad19 character varying(50),
  	    des_habilidad20 character varying(50),
  	    des_habilidad21 character varying(50),
  	    des_habilidad22 character varying(50),
  	    des_habilidad23 character varying(50),
  	    des_habilidad24 character varying(50),
  	    des_habilidad25 character varying(50),
  	    des_habilidad26 character varying(50),
  	    des_habilidad27 character varying(50),
  	    des_habilidad28 character varying(50),
  	    des_habilidad29 character varying(50),
  	    des_habilidad30 character varying(50),
  	    des_dominio1 integer DEFAULT 0,
  	    des_dominio2 integer DEFAULT 0,
  	    des_dominio3 integer DEFAULT 0,
  	    des_dominio4 integer DEFAULT 0,
  	    des_dominio5 integer DEFAULT 0,
  	    des_dominio6 integer DEFAULT 0,
  	    des_dominio7 integer DEFAULT 0,
  	    des_dominio8 integer DEFAULT 0,
  	    des_dominio9 integer DEFAULT 0,
  	    des_dominio10 integer DEFAULT 0,
  	    des_dominio11 integer DEFAULT 0,
  	    des_dominio12 integer DEFAULT 0,
  	    des_dominio13 integer DEFAULT 0,
  	    des_dominio14 integer DEFAULT 0,
  	    des_dominio15 integer DEFAULT 0,
  	    des_dominio16 integer DEFAULT 0,
  	    des_dominio17 integer DEFAULT 0,
  	    des_dominio18 integer DEFAULT 0,
  	    des_dominio19 integer DEFAULT 0,
  	    des_dominio20 integer DEFAULT 0,
  	    des_dominio21 integer DEFAULT 0,
  	    des_dominio22 integer DEFAULT 0,
  	    des_dominio23 integer DEFAULT 0,
  	    des_dominio24 integer DEFAULT 0,
  	    des_dominio25 integer DEFAULT 0,
  	    des_dominio26 integer DEFAULT 0,
  	    des_dominio27 integer DEFAULT 0,
  	    des_dominio28 integer DEFAULT 0,
  	    des_dominio29 integer DEFAULT 0,
  	    des_dominio30 integer DEFAULT 0
  	);


  	ALTER TABLE sisat.tsisathabilidades OWNER TO suite;

  	--
  	-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatidiomas (
  	    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    cod_nbidioma character varying(20) NOT NULL,
  	    cod_nivel character varying(20) NOT NULL,
  	    des_certificado character varying(40)
  	);


  	ALTER TABLE sisat.tsisatidiomas OWNER TO suite;

  	--
  	-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatordenservicio (
  	    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
  	    cod_estadorep integer NOT NULL,
  	    cod_ciudad integer NOT NULL,
  	    fec_fecha date NOT NULL,
  	    des_nbcontacto character varying(50),
  	    cod_puesto integer,
  	    des_nbcompania character varying(50),
  	    des_nbservicio character varying(60),
  	    cnu_cantidad smallint,
  	    txt_concepto text,
  	    imp_inversionhr numeric(6,2),
  	    txt_condicionescomer text,
  	    des_ubcnconsultor character varying(100),
  	    fec_finservicio date,
  	    cod_gpy integer NOT NULL,
  	    des_correogpy character varying(50) NOT NULL,
  	    cod_cliente integer NOT NULL,
  	    des_correoclte character varying(50) NOT NULL,
  	    des_empresaclte character varying(50),
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatordenservicio OWNER TO suite;

  	--
  	-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatprospectos (
  	    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
  	    des_nombre character varying(45) NOT NULL,
  	    des_nombres character varying(60),
  	    des_appaterno character varying(40) NOT NULL,
  	    des_apmaterno character varying(40),
  	    des_lugarnacimiento character varying(50) NOT NULL,
  	    fec_nacimiento date NOT NULL,
  	    cod_edad integer NOT NULL,
  	    cod_edocivil character varying(15) NOT NULL,
  	    des_nbpadre character varying(70),
  	    des_nbmadre character varying(70),
  	    cod_numhermanos integer,
  	    des_nbcalle character varying(60),
  	    cod_numcasa integer,
  	    des_colonia character varying(60),
  	    des_localidad character varying(60),
  	    des_municipio character varying(60),
  	    des_estado character varying(60),
  	    cod_cpostal integer,
  	    cod_tiposangre character varying(5),
  	    des_emailmbn character varying(40),
  	    des_emailpersonal character varying(40),
  	    des_pasatiempo character varying(200),
  	    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
  	    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
  	    cod_rfc character varying(13),
  	    cod_nss character varying(20),
  	    cod_curp character varying(18) NOT NULL,
  	    des_nacionalidad character varying(30),
  	    cod_administrador integer,
  	    fec_fechacoment date,
  	    txt_comentarios text,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatprospectos OWNER TO suite;

  	--
  	-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatproyectos (
  	    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
  	    cod_prospecto integer NOT NULL,
  	    cod_perfil integer NOT NULL,
  	    des_nbcliente character varying(50) NOT NULL,
  	    des_nbresponsable character varying(50) NOT NULL,
  	    des_correo character varying(50) NOT NULL,
  	    cod_telefono character varying(16) NOT NULL,
  	    des_direccion character varying(200) NOT NULL,
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatproyectos OWNER TO suite;

  	--
  	-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: suite
  	--

  	CREATE TABLE sisat.tsisatvacantes (
  	    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
  	    des_rqvacante character varying(200) NOT NULL,
  	    cnu_anexperiencia smallint,
  	    txt_experiencia text,
  	    des_escolaridad character varying(50),
  	    txt_herramientas text,
  	    txt_habilidades text,
  	    des_lugartrabajo character varying(100),
  	    imp_sueldo numeric(6,2),
  	    cod_creadopor integer NOT NULL,
  	    cod_modificadopor integer NOT NULL,
  	    fec_creacion date NOT NULL,
  	    fec_modificacion date NOT NULL
  	);


  	ALTER TABLE sisat.tsisatvacantes OWNER TO suite;



  	--###############	VALORES DE SECUENCIA   ###################
  	--
  	-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


  	--
  	-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


  	--
  	-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


  	--
  	-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 1, false);


  	--
  	-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


  	--
  	-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


  	--
  	-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


  	--
  	-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


  	--
  	-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


  	--
  	-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


  	--
  	-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_firmas', 1, false);


  	--
  	-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


  	--
  	-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


  	--
  	-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_ordenservicios', 1, false);


  	--
  	-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


  	--
  	-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


  	--
  	-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: suite
  	--

  	SELECT pg_catalog.setval('sisat.seq_vacantes', 1, false);


  	--###############	RESTRICCIONES   ###################



  	--###############	LLAVES PRIMARIAS   ###################
  	--
  	-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatasignaciones
  	    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


  	--
  	-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatcandidatos
  	    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


  	--
  	-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatcartaaceptacion
  	    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


  	--
  	-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatcartaasignacion
  	    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


  	--
  	-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatcotizaciones
  	    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


  	--
  	-- Name: tsisatcursosycerticados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatcursosycerticados
  	    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


  	--
  	-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatentrevistas
  	    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


  	--
  	-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatenviocorreos
  	    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


  	--
  	-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatescolaridad
  	    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


  	--
  	-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
  	    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


  	--
  	-- Name: tsisatfirmas tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatfirmas
  	    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


  	--
  	-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisathabilidades
  	    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


  	--
  	-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatidiomas
  	    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


  	--
  	-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatordenservicio
  	    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


  	--
  	-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatprospectos
  	    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


  	--
  	-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatproyectos
  	    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


  	--
  	-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: suite
  	--

  	ALTER TABLE ONLY sisat.tsisatvacantes
  	    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);

    ALTER TABLE ONLY sgco.tsgcousuarios
    	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);
---------------------------------------------------------------
---------------------------------------------------------------

	--
	-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhempleados
	    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhpuestos
	    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
	    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


	--
	-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhempleados
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcontratos
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcontrataciones
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhencuesta
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhevaluaciones
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhplanoperativo
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhescolaridad
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcatencuestaparticipantes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_participante) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
	    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcatencuestaparticipantes fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
	    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
	    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
	    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhevacontestadas
	    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhevacontestadas
	    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhevacontestadas
	    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
	    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhencuesta fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhencuesta
	    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhempleados
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcontratos
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcontrataciones
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhencuesta
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhevaluaciones
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhplanoperativo
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
	    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
	    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
	    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
	    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhempleados fk_cod_puesto; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhempleados
	    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
	    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhareas fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhareas
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhempleados fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhempleados
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
	--

	ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
	    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- TOC entry 3208 (class 2606 OID 74735)
	-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
	--

	ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
	    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


	--
	-- TOC entry 3209 (class 2606 OID 74725)
	-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
	--

	ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
	    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


	--
	-- TOC entry 3210 (class 2606 OID 74730)
	-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
	--

	ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
	    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


	--
	-- TOC entry 3208 (class 2606 OID 82935)
	-- Name: tsgrhevacapacitacionesemp fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
	--

	ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
	    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


	--
	-- TOC entry 3209 (class 2606 OID 82930)
	-- Name: tsgrhevacapacitacionesemp fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
	--

	ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
	    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);

----------------------------------------------------------------------

	ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
	    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtfaq fk_cod_categoria; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtfaq
	    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcompromisos fk_cod_chat; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcompromisos
	    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);


	--
	-- Name: tsgrtlugares fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtlugares
	    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdepartamento fk_cod_correo; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdepartamento
	    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcategoriafaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcorreo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcorreo
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdepartamento fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdepartamento
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtfaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtfaq
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtgrupo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtgrupo
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtayudatopico fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtayudatopico
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtnota fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtnota
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtplantillacorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtprioridad fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtprioridad
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtresppredefinida fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtresppredefinida
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtattchticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtattchticket
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtmsjticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtmsjticket
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtreuniones fk_cod_creadorreunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtreuniones
	    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcorreo fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcorreo
	    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtayudatopico fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtayudatopico
	    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtresppredefinida fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtresppredefinida
	    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdatossolicitud fk_cod_edosolicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
	    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdatossolicitud fk_cod_elemento; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
	    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtasistentes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtasistentes
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtnota fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtnota
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtsolicitudservicios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtinvitados fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtinvitados
	    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtasistentes fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtasistentes
	    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcomentariosreunion fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
	    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcomentariosagenda fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
	    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtreuniones fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtreuniones
	    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_mensaje; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdepartamento fk_cod_plantillacorreo; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdepartamento
	    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtayudatopico fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtayudatopico
	    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtreuniones fk_cod_responsable; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtreuniones
	    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcompromisos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcompromisos
	    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtarchivos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtarchivos
	    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtinvitados fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtinvitados
	    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcomentariosreunion fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
	    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtagenda fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtagenda
	    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtreuniones fk_cod_reunionanterior; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtreuniones
	    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtsolicitudservicios fk_cod_servicio; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
	    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcorreo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcorreo
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdepartamento fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdepartamento
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtgrupo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtgrupo
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtayudatopico fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtayudatopico
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtnota fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtnota
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtplantillacorreos fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtprioridad fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtprioridad
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtresppredefinida fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtresppredefinida
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtattchticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtattchticket
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtmsjticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtmsjticket
	    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdatossolicitud fk_cod_solicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
	    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtnota fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtnota
	    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtsolicitudservicios fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
	    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtattchticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtattchticket
	    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtmsjticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtmsjticket
	    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_topico; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcorreo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcorreo
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtdepartamento fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtdepartamento
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtfaq fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtfaq
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtgrupo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtgrupo
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtayudatopico fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtayudatopico
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtnota fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtnota
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtplantillacorreos fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtprioridad fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtprioridad
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtrespuesta fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtrespuesta
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtresppredefinida fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtresppredefinida
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtticket
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtattchticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtattchticket
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtmsjticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtmsjticket
	    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtcorreo fk_cod_usuario; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtcorreo
	    ADD CONSTRAINT fk_cod_usuario FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsgrtciudades fk_estadorep; Type: FK CONSTRAINT; Schema: sgrt; Owner: suite
	--

	ALTER TABLE ONLY sgrt.tsgrtciudades
	    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;
------------------------------------------------------------


	ALTER TABLE ONLY sisat.tsisatprospectos
	    ADD CONSTRAINT fk_cod_administrador FOREIGN KEY (cod_administrador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_autoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_autoriza FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatasignaciones
	    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sgrh.tsgrhcontrataciones(cod_contratacion) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatvacantes
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaaceptacion
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcotizaciones
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatenviocorreos
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatasignaciones
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatentrevistas
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatprospectos
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatproyectos
	    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatvacantes
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaaceptacion
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcotizaciones
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatenviocorreos
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatasignaciones
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatentrevistas
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatprospectos
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatproyectos
	    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcandidatos
	    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatasignaciones
	    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatproyectos
	    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatasignaciones
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatproyectos
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatescolaridad
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcursosycerticados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcursosycerticados
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatidiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatidiomas
	    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcotizaciones
	    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatordenservicio fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatordenservicio
	    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_puestoautoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_puestoautoriza FOREIGN KEY (cod_puestoautoriza) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_puestosolicita; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_puestosolicita FOREIGN KEY (cod_puestosolicita) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatcartaasignacion
	    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


	--
	-- Name: tsisatfirmas fk_cod_solicita; Type: FK CONSTRAINT; Schema: sisat; Owner: suite
	--

	ALTER TABLE ONLY sisat.tsisatfirmas
	    ADD CONSTRAINT fk_cod_solicita FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;
	---------------------------------------------


	CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
	    LANGUAGE sql
	    AS $_$
	            SELECT * FROM crosstab(
	                'SELECT p.des_pregunta AS rowid,
	                        cr.cod_ponderacion as attribute,
	                        cr.des_respuesta as value
	                FROM sgrh.tsgrhpreguntasenc p
	                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta
	                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta
	                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta
	                WHERE e.cod_encuesta = ' || $1
				)
	            AS (
	                pregunta VARCHAR(200),
	                resp1 VARCHAR(200),
	                resp2 VARCHAR(200),
	                resp3 VARCHAR(200),
	                resp4 VARCHAR(200),
	                resp5 VARCHAR(200)
	            );
	    $_$;

	ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO postgres;

	--
	-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: postgres
	--

	CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
	    LANGUAGE plpgsql
	    AS $$
	declare begin
		new.fec_modificacion:=current_date;
		return new;

	end;
	$$;

	ALTER FUNCTION sgrh.factualizarfecha() OWNER TO postgres;
	CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();

	--------------------------------------------------------------------------------------------------
	CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
	    LANGUAGE sql
	    AS $$
	SELECT
	des_nombre as nombre_asistente,
	CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
	des_empresa) as area_asistente
	FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
	$$;


	ALTER FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) OWNER TO suite;

	--
	-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
	    LANGUAGE plpgsql
	    AS $$

	BEGIN
	RETURN QUERY
	select
	CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
	com.des_descripcion,
	com.cod_estatus,
	CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
	CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
	from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

	END;
	$$;


	ALTER FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) OWNER TO suite;

	--
	-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
	    LANGUAGE sql
	    AS $$
	SELECT
	cod_area,
	cod_acronimo as des_nbarea,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtreuniones reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_responsable=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE area.cod_area=a.cod_area and
	reu.fec_fecha >= cast(fecha_inicio as date)
	AND reu.fec_fecha <=  cast(fecha_fin as date)
	) as INTEGER) AS cantidad_minutas
	FROM sgrh.tsgrhareas a
	$$;


	ALTER FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) OWNER TO suite;

	--
	-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
	    LANGUAGE sql
	    AS $$
	  select
	  reunion.cod_reunion,
	  reunion.des_nombre,
	  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
	  reunion.cod_lugar,
	  CAST(reunion.tim_hora as text),
	  lugar.des_nombre,
	  lugar.cod_ciudad,
	ciudad.des_nbciudad,
	ciudad.cod_estadorep,
	estado.des_nbestado,
	estado.cod_estadorep
	from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
	inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
	inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
	where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

	$$;


	ALTER FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) OWNER TO suite;

	--
	-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
	    LANGUAGE sql
	    AS $$
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Terminado' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Terminado' AND
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date)
	)as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	UNION
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Pendiente' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Pendiente' and
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	$$;


	ALTER FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) OWNER TO suite;

	--
	-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
	    LANGUAGE sql
	    AS $$
	SELECT
	emp.cod_empleado,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
	com.des_descripcion,
	cast(com.cod_estatus as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
	WHERE com.fec_compromiso=cast(fechaCompromiso as date);
	$$;


	ALTER FUNCTION sgrt.compromisos_dia(fechacompromiso text) OWNER TO suite;

	--
	-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
	    LANGUAGE sql
	    AS $$
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Validador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_validador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Verificador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_verificador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Ejecutor' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_ejecutor
	$$;


	ALTER FUNCTION sgrt.compromisos_generales() OWNER TO suite;

	--
	-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: suite
	--

	CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
	    LANGUAGE plpgsql
	    AS $$
	DECLARE
	    var_r record;
	BEGIN
	   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
	     LOOP
	              RETURN QUERY
			select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
			(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
			(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
			(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
			CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
			sgrt.tsgrtreuniones reunion,
			sgrh.tsgrhempleados empleado
			WHERE reunion.cod_responsable=empleado.cod_empleado and
			cod_reunion=var_r.cod_reunion) c;
	            END LOOP;
	END; $$;


	ALTER FUNCTION sgrt.reporte_por_tema(reunionid integer) OWNER TO suite;

	------------------------------------------------------------------------------


	CREATE SCHEMA sgnom
	    AUTHORIZATION postgres;

	GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


	COMMENT ON SCHEMA sgnom
	    IS 'Sistema de Gestion de Nomina.';
	ALTER DEFAULT PRIVILEGES IN SCHEMA sgnom
	GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

	ALTER DEFAULT PRIVILEGES IN SCHEMA sgnom
	GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

	ALTER DEFAULT PRIVILEGES IN SCHEMA sgnom
	GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

	ALTER DEFAULT PRIVILEGES IN SCHEMA sgnom
	GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

	create table "sgnom"."tsgnomalguinaldo" (
	"cod_aguinaldoid" int4 not null,
	"imp_aguinaldo" numeric(10,2) not null,
	"cod_tipoaguinaldo" char(1) collate "default" not null,
	"cod_empquincenaid_fk" int4 not null,
	"bol_estatus" bool not null,
	"xml_desgloce" xml,
	constraint "nom_aguinaldo_id" primary key ("cod_aguinaldoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomalguinaldo" is 'cod_tipoaguinaldo

	(

	i = imss,

	h = honorarios

	)';
	alter table "sgnom"."tsgnomalguinaldo" owner to "suite";

	create table "sgnom"."tsgnomargumento" (
	"cod_argumentoid" int4 not null,
	"cod_nbargumento" varchar(30) collate "default" not null,
	"cod_clavearg" varchar(5) collate "default" not null,
	"imp_valorconst" numeric(10,2),
	"des_funcionbd" varchar(60) collate "default",
	"bol_estatus" bool not null,
	"txt_descripcion" text collate "default",
	"aud_ucrea" int4 not null,
	"aud_uactualizacion" int4,
	"aud_feccrea" date not null,
	"aud_fecactualizacion" date,
	constraint "nom_cat_argumento_id" primary key ("cod_argumentoid")
	)
	without oids;
	alter table "sgnom"."tsgnomargumento" owner to "suite";

	create table "sgnom"."tsgnombitacora" (
	"cod_bitacoraid" int4 not null,
	"xml_bitacora" xml not null,
	"cod_tablaid_fk" int4 not null,
	constraint "nom_bitacora_id" primary key ("cod_bitacoraid")
	)
	without oids;
	alter table "sgnom"."tsgnombitacora" owner to "suite";

	create table "sgnom"."tsgnomcabecera" (
	"cod_cabeceraid" int4 not null,
	"cod_nbnomina" varchar(40) collate "default" not null,
	"fec_creacion" date not null,
	"fec_ejecucion" date not null,
	"fec_cierre" date not null,
	"imp_totpercepcion" numeric(10,2) not null,
	"imp_totdeduccion" numeric(10,2) not null,
	"imp_totalemp" numeric(10,2) not null,
	"cod_quincenaid_fk" int4 not null,
	"cod_tiponominaid_fk" int4 not null,
	"cod_estatusnomid_fk" int4 not null,
	"aud_ucrea" int4 not null,
	"aud_uactualiza" int4,
	"aud_feccrea" date not null,
	"aud_fecactualiza" date,
	constraint "nom_cabecera_id" primary key ("cod_cabeceraid")
	)
	without oids;
	alter table "sgnom"."tsgnomcabecera" owner to "suite";

	create table "sgnom"."tsgnomcabeceraht" (
	"cod_cabeceraid" int4 not null,
	"cod_nbnomina" varchar(40) collate "default" not null,
	"fec_creacion" date not null,
	"fec_ejecucion" date not null,
	"fec_cierre" date not null,
	"imp_totpercepcion" numeric(10,2) not null,
	"imp_totdeduccion" numeric(10,2) not null,
	"imp_totalemp" numeric(10,2) not null,
	"cod_quincenaid_fk" int4 not null,
	"cod_tiponominaid_fk" int4 not null,
	"cod_estatusnomid_fk" int4 not null,
	"aud_ucrea" int4 not null,
	"aud_uactualiza" int4,
	"aud_feccrea" date not null,
	"aud_fecactualiza" date,
	constraint "nom_cabecera_copia_id" primary key ("cod_cabeceraid")
	)
	without oids;
	alter table "sgnom"."tsgnomcabeceraht" owner to "suite";

	create table "sgnom"."tsgnomcalculo" (
	"cod_calculoid" int4 not null,
	"cod_tpcalculo" varchar(25) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_tipo_calculo_id" primary key ("cod_calculoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomcalculo" is 'bol_estatus

	(

	activo, inactivo

	)';
	alter table "sgnom"."tsgnomcalculo" owner to "suite";

	create table "sgnom"."tsgnomcatincidencia" (
	"cod_catincidenciaid" int4 not null,
	"cod_claveincidencia" varchar(5) collate "default",
	"cod_nbincidencia" varchar(20) collate "default",
	"cod_perfilincidencia" varchar(25) collate "default",
	"bol_estatus" bool,
	"cod_tipoincidencia" char(1) collate "default",
	constraint "cat_incidencia_id" primary key ("cod_catincidenciaid")
	)
	without oids;
	comment on table "sgnom"."tsgnomcatincidencia" is 'cod_tipoincidencia

	(

	1 = horas

	2 = dias

	3 = actividad

	)';
	alter table "sgnom"."tsgnomcatincidencia" owner to "suite";

	create table "sgnom"."tsgnomclasificador" (
	"cod_clasificadorid" int4 not null,
	"cod_tpclasificador" varchar(20) collate "default",
	"bol_estatus" bool,
	constraint "nom_cat_tipo_clasificador_id" primary key ("cod_clasificadorid")
	)
	without oids;
	alter table "sgnom"."tsgnomclasificador" owner to "suite";

	create table "sgnom"."tsgnomcncptoquinc" (
	"cod_cncptoquincid" int4 not null,
	"cod_empquincenaid_fk" int4 not null,
	"cod_conceptoid_fk" int4 not null,
	"imp_concepto" numeric(10,2) not null,
	"imp_gravado" numeric(10,2),
	"imp_exento" numeric(10,2),
	"xml_desgloce" xml,
	constraint "nom_conceptos_quincena_id" primary key ("cod_cncptoquincid")
	)
	without oids;
	alter table "sgnom"."tsgnomcncptoquinc" owner to "suite";

	create table "sgnom"."tsgnomcncptoquincht" (
	"cod_cncptoquinchtid" int4 not null,
	"cod_empquincenaid_fk" int4 not null,
	"cod_conceptoid_fk" int4 not null,
	"imp_concepto" numeric(10,2) not null,
	"imp_gravado" numeric(10,2),
	"imp_exento" numeric(10,2),
	"xml_desgloce" xml,
	constraint "nom_conceptos_quincena_copia_id" primary key ("cod_cncptoquinchtid")
	)
	without oids;
	alter table "sgnom"."tsgnomcncptoquincht" owner to "suite";

	create table "sgnom"."tsgnomconcepto" (
	"cod_conceptoid" int4 not null,
	"cod_nbconcepto" varchar(20) collate "default" not null,
	"cod_claveconcepto" varchar(4) collate "default" not null,
	"cnu_prioricalculo" int4 not null,
	"cnu_articulo" int4 not null,
	"bol_estatus" bool not null,
	"cod_formulaid_fk" int4 not null,
	"cod_tipoconceptoid_fk" int4 not null,
	"cod_calculoid_fk" int4 not null,
	"cod_conceptosatid_fk" int4 not null,
	"cod_frecuenciapago" varchar(20) collate "default" not null,
	"cod_partidaprep" int4 not null,
	"cnu_cuentacontable" int4 not null,
	"cod_gravado" char(1) collate "default" not null,
	"cod_excento" char(1) collate "default" not null,
	"bol_retroactividad" bool not null,
	"cnu_topeex" int4 not null,
	"cod_clasificadorid_fk" int4 not null,
	"aud_ualta" int4 not null,
	"aud_uactualizacion" int4,
	"aud_feccrea" date not null,
	"aud_fecactualizacion" date,
	constraint "nom_cat_concepto_id" primary key ("cod_conceptoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomconcepto" is 'bol_estatus

	(

	activo, inactivo

	)



	gravado

	(

	1 = dias,

	2 = porcentaje,

	3 = no aplica

	)



	excento

	(

	activo, inactivo

	)





	validar tope_ex';
	alter table "sgnom"."tsgnomconcepto" owner to "suite";

	create table "sgnom"."tsgnomconceptosat" (
	"cod_conceptosatid" int4 not null,
	"des_conceptosat" varchar(51) collate "default" not null,
	"des_descconcepto" varchar(51) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_concepto_sat_id" primary key ("cod_conceptosatid")
	)
	without oids;
	comment on table "sgnom"."tsgnomconceptosat" is 'bol_estatus

	(

	activo, inactivo

	)';
	alter table "sgnom"."tsgnomconceptosat" owner to "suite";

	create table "sgnom"."tsgnomconfpago" (
	"cod_confpagoid" int4 not null,
	"bol_pagoempleado" bool,
	"bol_pagorh" bool,
	"bol_pagofinanzas" bool,
	"cod_empquincenaid_fk" int4,
	constraint "nom_conf_pago_pkey" primary key ("cod_confpagoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomconfpago" is 'bol_pagoempleado

	(

	confirmado, pendiente

	)



	pago_rh

	(

	autorizado, pendiente

	)



	pago_fnzas

	(

	autorizado, pendiente

	)';
	alter table "sgnom"."tsgnomconfpago" owner to "suite";

	create table "sgnom"."tsgnomejercicio" (
	"cod_ejercicioid" int4 not null,
	"cnu_valorejercicio" int4 not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_ejercicio_id" primary key ("cod_ejercicioid")
	)
	without oids;
	alter table "sgnom"."tsgnomejercicio" owner to "suite";

	create table "sgnom"."tsgnomempleados" (
	"cod_empleadoid" int4 not null,
	"fec_ingreso" date not null,
	"fec_salida" date,
	"bol_estatus" bool not null,
	"cod_empleado_fk" int4 not null,
	"imp_sueldoimss" numeric(10,2),
	"imp_honorarios" numeric(10,2),
	"cod_tipoimss" char(1) collate "default",
	"cod_tipohonorarios" char(1) collate "default",
	"cod_banco" varchar(50) collate "default",
	"cod_sucursal" int4,
	"cod_cuenta" int4,
	"cod_clabe" int4,
	"txt_descripcionbaja" text collate "default",
	"aud_codcreadopor" int4 not null,
	"aud_feccreacion" date not null,
	"aud_codmodificadopor" int4,
	"aud_fecmodificacion" date,
	constraint "nom_empleado_id" primary key ("cod_empleadoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomempleados" is 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';
	alter table "sgnom"."tsgnomempleados" owner to "suite";

	create table "sgnom"."tsgnomempquincena" (
	"cod_empquincenaid" int4 not null,
	"cod_empleadoid_fk" int4 not null,
	"cod_cabeceraid_fk" int4 not null,
	"imp_totpercepcion" numeric(10,2) not null,
	"imp_totdeduccion" numeric(10,2) not null,
	"imp_totalemp" numeric(10,2) not null,
	"bol_estatusemp" bool not null,
	constraint "nom_empleado_quincena_id" primary key ("cod_empquincenaid")
	)
	without oids;
	comment on table "sgnom"."tsgnomempquincena" is 'bol_estatusemp

	(

	activo, inactivo

	)';
	alter table "sgnom"."tsgnomempquincena" owner to "suite";

	create table "sgnom"."tsgnomempquincenaht" (
	"cod_empquincenahtid" int4 not null,
	"cod_empleadoid_fk" int4 not null,
	"cod_cabeceraid_fk" int4 not null,
	"imp_totpercepcion" numeric(10,2) not null,
	"imp_totdeduccion" numeric(10,2) not null,
	"imp_totalemp" numeric(10,2) not null,
	"bol_estatusemp" bool not null,
	constraint "nom_empleado_quincena_copia_id" primary key ("cod_empquincenahtid")
	)
	without oids;
	alter table "sgnom"."tsgnomempquincenaht" owner to "suite";

	create table "sgnom"."tsgnomestatusnom" (
	"cod_estatusnomid" int4 not null,
	"cod_estatusnomina" varchar(15) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_estatus_nomina_id" primary key ("cod_estatusnomid")
	)
	without oids;
	comment on table "sgnom"."tsgnomestatusnom" is 'cod_estatusnomid

	estatus

	(

	abierta,

	calculada,

	revision,

	validada,

	cerrada

	)



	nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente

	hasta ser validada';
	alter table "sgnom"."tsgnomestatusnom" owner to "suite";

	create table "sgnom"."tsgnomformula" (
	"cod_formulaid" int4 not null,
	"des_nbformula" varchar(60) collate "default" not null,
	"des_formula" varchar(250) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_formula_id" primary key ("cod_formulaid")
	)
	without oids;
	comment on table "sgnom"."tsgnomformula" is 'bol_estatus

	(

	activo, inactivo

	)';
	alter table "sgnom"."tsgnomformula" owner to "suite";

	create table "sgnom"."tsgnomfuncion" (
	"cod_funcionid" int4 not null,
	"cod_nbfuncion" varchar(15) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_funcion_id" primary key ("cod_funcionid")
	)
	without oids;
	alter table "sgnom"."tsgnomfuncion" owner to "suite";

	create table "sgnom"."tsgnomhisttabla" (
	"cod_tablaid" int4 not null,
	"cod_nbtabla" varchar(18) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_tabla_id" primary key ("cod_tablaid")
	)
	without oids;
	alter table "sgnom"."tsgnomhisttabla" owner to "suite";

	create table "sgnom"."tsgnomincidencia" (
	"cod_incidenciaid" int4 not null,
	"cod_catincidenciaid_fk" int4 not null,
	"cnu_cantidad" int2,
	"des_actividad" varchar(100) collate "default",
	"txt_comentarios" text collate "default",
	"cod_empreporta_fk" int4,
	"cod_empautoriza_fk" int4,
	"imp_monto" numeric(10,2),
	"xml_detcantidad" xml,
	"bol_estatus" bool,
	"cod_quincenaid_fk" int4 not null,
	"bol_validacion" bool,
	"aud_feccrea" date not null,
	"aud_fecvalidacion" date,
	constraint "nom_incidencia_id" primary key ("cod_incidenciaid")
	)
	without oids;
	comment on table "sgnom"."tsgnomincidencia" is 'bol_estatus: (activo, inactivo) (1, 0)

	validacion: (validar, denegar) (1, 0)



	';
	alter table "sgnom"."tsgnomincidencia" owner to "suite";

	create table "sgnom"."tsgnommanterceros" (
	"cod_mantercerosid" int4 not null,
	"cod_conceptoid_fk" int4,
	"imp_monto" numeric(10,2),
	"cod_quincenainicio_fk" int4,
	"cod_quincenafin_fk" int4,
	"cod_empleadoid_fk" int4,
	"cod_frecuenciapago" varchar(20) collate "default",
	"bol_estatus" bool,
	"aud_ucrea" int4 not null,
	"aud_uactualizacion" int4,
	"aud_feccrea" date not null,
	"aud_fecactualizacion" varchar(255),
	constraint "nom_manuales_terceros_pkey" primary key ("cod_mantercerosid")
	)
	without oids;
	alter table "sgnom"."tsgnommanterceros" owner to "suite";

	create table "sgnom"."tsgnomquincena" (
	"cod_quincenaid" int4 not null,
	"des_quincena" varchar(70) collate "default" not null,
	"fec_inicio" date not null,
	"fec_fin" date not null,
	"fec_pago" date not null,
	"fec_dispersion" date not null,
	"cnu_numquincena" int4 not null,
	"cod_ejercicioid_fk" int4 not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_quincena_id" primary key ("cod_quincenaid")
	)
	without oids;
	alter table "sgnom"."tsgnomquincena" owner to "suite";

	create table "sgnom"."tsgnomtipoconcepto" (
	"cod_tipoconceptoid" int4 not null,
	"cod_tipoconcepto" varchar(25) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_tipo_conepto_id" primary key ("cod_tipoconceptoid")
	)
	without oids;
	comment on table "sgnom"."tsgnomtipoconcepto" is 'bol_estatus

	(

	activo, inactivo

	)';
	alter table "sgnom"."tsgnomtipoconcepto" owner to "suite";

	create table "sgnom"."tsgnomtiponomina" (
	"cod_tiponominaid" int4 not null,
	"cod_nomina" varchar(30) collate "default" not null,
	"bol_estatus" bool not null,
	constraint "nom_cat_tipo_nomina_id" primary key ("cod_tiponominaid")
	)
	without oids;
	comment on table "sgnom"."tsgnomtiponomina" is 'bol_estatus (activa, inactiva)';
	alter table "sgnom"."tsgnomtiponomina" owner to "suite";


	alter table "sgnom"."tsgnomalguinaldo" add constraint "nom_empleados_quincena_fk_aguinaldos" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnombitacora" add constraint "nom_cat_tabla_id_fk_nom_cat_tablas" foreign key ("cod_tablaid_fk") references "sgnom"."tsgnomhisttabla" ("cod_tablaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_estatus_nomina_id_fk_cabeceras" foreign key ("cod_estatusnomid_fk") references "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_quincena_id_fk_cabeceras" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_tipo_nomina_id_fk_cabeceras" foreign key ("cod_tiponominaid_fk") references "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_estatus_nomina_id_copia_fk_cabeceras" foreign key ("cod_estatusnomid_fk") references "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_quincena_id_copia_fk_cabeceras" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_tipo_nomina_id_copia_fk_cabeceras" foreign key ("cod_tiponominaid_fk") references "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcncptoquinc" add constraint "concepto_quincena_id_fk_conceptos_quincena" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcncptoquinc" add constraint "empleado_concepto_id_fk_conceptos_quincena" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcncptoquincht" add constraint "concepto_quincena_id_copia_fk_conceptos_quincena" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomcncptoquincht" add constraint "empleado_concepto_id_copia_fk_conceptos_quincena" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconcepto" add constraint "nom_cat_clasificador_id_fk_cat_conceptos" foreign key ("cod_clasificadorid_fk") references "sgnom"."tsgnomclasificador" ("cod_clasificadorid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconcepto" add constraint "nom_concepto_sat_id_fk_cat_conceptos" foreign key ("cod_conceptosatid_fk") references "sgnom"."tsgnomconceptosat" ("cod_conceptosatid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconcepto" add constraint "nom_formula_id_fk_cat_conceptos" foreign key ("cod_formulaid_fk") references "sgnom"."tsgnomformula" ("cod_formulaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconcepto" add constraint "nom_tipo_calculo_id_fk_cat_conceptos" foreign key ("cod_calculoid_fk") references "sgnom"."tsgnomcalculo" ("cod_calculoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconcepto" add constraint "nom_tipo_concepto_id_fk_cat_conceptos" foreign key ("cod_tipoconceptoid_fk") references "sgnom"."tsgnomtipoconcepto" ("cod_tipoconceptoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomconfpago" add constraint "nom_empleados_quincena_fk_conf_pago" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomempquincena" add constraint "nom_cabecera_id_fk_empleados_quincena" foreign key ("cod_cabeceraid_fk") references "sgnom"."tsgnomcabecera" ("cod_cabeceraid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomempquincena" add constraint "nom_empleado_quincena_id_fk_empleados_quincena" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomempquincenaht" add constraint "nom_cabecera_id_fk_empleados_quincena_copia" foreign key ("cod_cabeceraid_fk") references "sgnom"."tsgnomcabecera" ("cod_cabeceraid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomempquincenaht" add constraint "nom_empleado_quincena_id_fk_empleados_quincena_copia" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomincidencia" add constraint "nom_cat_incidencia_id_fk_incidencias" foreign key ("cod_catincidenciaid_fk") references "sgnom"."tsgnomcatincidencia" ("cod_catincidenciaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomincidencia" add constraint "nom_cat_quincena_id_fk_incidencias" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomincidencia" add constraint "nom_emp_autoriza_fk_incidencias" foreign key ("cod_empautoriza_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnomincidencia" add constraint "nom_emp_reporta_fk_incidencias" foreign key ("cod_empreporta_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
	alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_conceptos_fk_manuales_terceros" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update no action;
	alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_quincenas_fk_manuales_terceros_fin" foreign key ("cod_quincenafin_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update no action;
	alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_quincenas_fk_manuales_terceros_inicio" foreign key ("cod_quincenainicio_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update no action;
	alter table "sgnom"."tsgnommanterceros" add constraint "nom_empleados_fk_manuales_terceros" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update no action;
	alter table "sgnom"."tsgnomquincena" add constraint "nom_cat_ejercicio_id_fk_cat_quincenas" foreign key ("cod_ejercicioid_fk") references "sgnom"."tsgnomejercicio" ("cod_ejercicioid") on delete no action on update cascade;
