--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-1.pgdg19.04+1)
-- Dumped by pg_dump version 11.3 (Ubuntu 11.3-1.pgdg19.04+1)

-- Started on 2019-06-13 14:12:06 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE suite;
--
-- TOC entry 4707 (class 1262 OID 24323)
-- Name: suite; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE suite WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_MX.UTF-8' LC_CTYPE = 'es_MX.UTF-8';


ALTER DATABASE suite OWNER TO postgres;

\connect suite

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 24364)
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgco;


ALTER SCHEMA sgco OWNER TO postgres;

--
-- TOC entry 4708 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--
-- TOC entry 11 (class 2615 OID 26396)
-- Name: sgnom; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgnom;


ALTER SCHEMA sgnom OWNER TO postgres;

--
-- TOC entry 4710 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA sgnom; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgnom IS 'Sistema de Gestion de Nomina.';


--
-- TOC entry 12 (class 2615 OID 26710)
-- Name: sgrh; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrh;


ALTER SCHEMA sgrh OWNER TO postgres;

--
-- TOC entry 4712 (class 0 OID 0)
-- Dependencies: 12
-- Name: SCHEMA sgrh; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgrh IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';


--
-- TOC entry 9 (class 2615 OID 24338)
-- Name: sgrt; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrt;


ALTER SCHEMA sgrt OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 25190)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO postgres;

--
-- TOC entry 4713 (class 0 OID 0)
-- Dependencies: 13
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 2 (class 3079 OID 24343)
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- TOC entry 4714 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';


--
-- TOC entry 1159 (class 1247 OID 26712)
-- Name: edo_encuesta; Type: TYPE; Schema: sgrh; Owner: postgres
--

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);


ALTER TYPE sgrh.edo_encuesta OWNER TO postgres;

--
-- TOC entry 1162 (class 1247 OID 24397)
-- Name: destinatario; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);


ALTER TYPE sgrt.destinatario OWNER TO postgres;

--
-- TOC entry 832 (class 1247 OID 24406)
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);


ALTER TYPE sgrt.edoticket OWNER TO postgres;

--
-- TOC entry 835 (class 1247 OID 24412)
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);


ALTER TYPE sgrt.encriptacion OWNER TO postgres;

--
-- TOC entry 838 (class 1247 OID 24418)
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);


ALTER TYPE sgrt.estatus OWNER TO postgres;

--
-- TOC entry 841 (class 1247 OID 24424)
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);


ALTER TYPE sgrt.estatus_compromiso OWNER TO postgres;

--
-- TOC entry 854 (class 1247 OID 24430)
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);


ALTER TYPE sgrt.modulo OWNER TO postgres;

--
-- TOC entry 855 (class 1247 OID 24436)
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);


ALTER TYPE sgrt.origencontac OWNER TO postgres;

--
-- TOC entry 858 (class 1247 OID 24448)
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);


ALTER TYPE sgrt.prioridad OWNER TO postgres;

--
-- TOC entry 861 (class 1247 OID 24456)
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);


ALTER TYPE sgrt.protocolo OWNER TO postgres;

--
-- TOC entry 864 (class 1247 OID 24462)
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);


ALTER TYPE sgrt.tipo OWNER TO postgres;

--
-- TOC entry 867 (class 1247 OID 24474)
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);


ALTER TYPE sgrt.tipo_compromiso OWNER TO postgres;

--
-- TOC entry 436 (class 1255 OID 28608)
-- Name: buscar_detalle_empleados(); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.buscar_detalle_empleados() RETURNS TABLE(codempleado integer, despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY 

SELECT rh_emp.cod_empleado, puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
AND rh_emp.cod_empleadoactivo = 't'
AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;

END;
$$;


ALTER FUNCTION sgnom.buscar_detalle_empleados() OWNER TO postgres;

--
-- TOC entry 419 (class 1255 OID 28061)
-- Name: prueba(); Type: FUNCTION; Schema: sgnom; Owner: postgres
--

CREATE FUNCTION sgnom.prueba() RETURNS TABLE(despuesto character varying, nomcompleto text, desnbarea character varying)
    LANGUAGE plpgsql
    AS $$

BEGIN 
RETURN QUERY 

SELECT puesto.des_puesto, 
	CONCAT(rh_emp.des_nombre, ' ', rh_emp.des_nombres, ' ', rh_emp.des_apepaterno, ' ', rh_emp.des_apematerno) AS nom_completo,
	area.des_nbarea
FROM sgrh.tsgrhareas AS area, sgrh.tsgrhempleados AS rh_emp, sgrh.tsgrhpuestos AS puesto, sgnom.tsgnomempleados AS nom_emp
WHERE area.cod_area = puesto.cod_area
AND rh_emp.cod_area = area.cod_area
AND rh_emp.cod_puesto = puesto.cod_puesto
--AND rh_emp.cod_empleadoactivo = 't'
--AND rh_emp.cod_empleado = nom_emp.cod_empleadoid
--AND nom_emp.bol_estatus = 't'
ORDER BY 3, 1, 2;


END;
$$;


ALTER FUNCTION sgnom.prueba() OWNER TO postgres;

--
-- TOC entry 437 (class 1255 OID 26721)
-- Name: crosstab_report_encuesta(integer); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
    LANGUAGE sql
    AS $_$        


            SELECT * FROM crosstab(


                'SELECT p.des_pregunta AS rowid, 


                        cr.cod_ponderacion as attribute, 


                        cr.des_respuesta as value


                FROM sgrh.tsgrhpreguntasenc p


                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta


                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta


                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta


                WHERE e.cod_encuesta = ' || $1


			) 


            AS (


                pregunta VARCHAR(200), 


                resp1 VARCHAR(200), 


                resp2 VARCHAR(200), 


                resp3 VARCHAR(200), 


                resp4 VARCHAR(200), 


                resp5 VARCHAR(200)


            );


    $_$;


ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO postgres;

--
-- TOC entry 438 (class 1255 OID 26722)
-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
    LANGUAGE plpgsql
    AS $$


declare begin


	new.fec_modificacion:=current_date;


	return new;





end;


$$;


ALTER FUNCTION sgrh.factualizarfecha() OWNER TO postgres;

--
-- TOC entry 439 (class 1255 OID 26388)
-- Name: buscar_asistentes_minuta(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
    LANGUAGE sql
    AS $$
	SELECT
	des_nombre as nombre_asistente,
	CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
	des_empresa) as area_asistente
	FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
	$$;


ALTER FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) OWNER TO postgres;

--
-- TOC entry 440 (class 1255 OID 26389)
-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS $$

	BEGIN
	RETURN QUERY
	select
	CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
	com.des_descripcion,
	com.cod_estatus,
	CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
	CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
	(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
	from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

	END;
	$$;


ALTER FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) OWNER TO postgres;

--
-- TOC entry 432 (class 1255 OID 26390)
-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
    LANGUAGE sql
    AS $$
	SELECT
	cod_area,
	cod_acronimo as des_nbarea,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtreuniones reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_responsable=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE area.cod_area=a.cod_area and
	reu.fec_fecha >= cast(fecha_inicio as date)
	AND reu.fec_fecha <=  cast(fecha_fin as date)
	) as INTEGER) AS cantidad_minutas
	FROM sgrh.tsgrhareas a
	$$;


ALTER FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 433 (class 1255 OID 26391)
-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
	  select
	  reunion.cod_reunion,
	  reunion.des_nombre,
	  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
	  reunion.cod_lugar,
	  CAST(reunion.tim_hora as text),
	  lugar.des_nombre,
	  lugar.cod_ciudad,
	ciudad.des_nbciudad,
	ciudad.cod_estadorep,
	estado.des_nbestado,
	estado.cod_estadorep
	from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
	inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
	inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
	where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

	$$;


ALTER FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) OWNER TO postgres;

--
-- TOC entry 434 (class 1255 OID 26392)
-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
    LANGUAGE sql
    AS $$
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Terminado' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Terminado' AND
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date)
	)as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	UNION
	select
	cod_area,
	cod_acronimo as des_nbarea,
	CAST('Pendiente' as text) as tipo,
	cast((SELECT
	COUNT(*)
	FROM sgrt.tsgrtcompromisos reu
	INNER JOIN sgrh.tsgrhempleados emp
	ON reu.cod_ejecutor=emp.cod_empleado
	INNER JOIN sgrh.tsgrhareas area
	ON emp.cod_area=area.cod_area
	WHERE reu.cod_estatus='Pendiente' and
	area.cod_area=a.cod_area AND
	reu.fec_compromiso >= cast(fecha_inicio as date)
	AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
	FROM
	sgrh.tsgrhareas a
	$$;


ALTER FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 441 (class 1255 OID 26393)
-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
    LANGUAGE sql
    AS $$
	SELECT
	emp.cod_empleado,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
	com.des_descripcion,
	cast(com.cod_estatus as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
	WHERE com.fec_compromiso=cast(fechaCompromiso as date);
	$$;


ALTER FUNCTION sgrt.compromisos_dia(fechacompromiso text) OWNER TO postgres;

--
-- TOC entry 435 (class 1255 OID 26394)
-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
    LANGUAGE sql
    AS $$
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Validador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_validador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Verificador' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_verificador
	UNION
	SELECT
	emp.cod_empleado,
	reunion.cod_reunion,
	CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
	CAST('Ejecutor' as text) as rol,
	cast(area.cod_acronimo as text) as area,
	cast(com.des_descripcion as text) as descripcion,
	cast(reunion.des_nombre as text) as minuta,
	cast(com.cod_estatus as text)as estatus,
	cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
	cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
	cast((select count(the_day) from
	    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
	cast((select count(the_day) from
	    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
	where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
	FROM sgrt.tsgrtcompromisos com
	LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
	LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
	LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
	WHERE emp.cod_empleado=com.cod_ejecutor
	$$;


ALTER FUNCTION sgrt.compromisos_generales() OWNER TO postgres;

--
-- TOC entry 442 (class 1255 OID 26395)
-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	    var_r record;
	BEGIN
	   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
	     LOOP
	              RETURN QUERY
			select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
			(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
			(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
			(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
			CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
			sgrt.tsgrtreuniones reunion,
			sgrh.tsgrhempleados empleado
			WHERE reunion.cod_responsable=empleado.cod_empleado and
			cod_reunion=var_r.cod_reunion) c;
	            END LOOP;
	END; $$;


ALTER FUNCTION sgrt.reporte_por_tema(reunionid integer) OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 24369)
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24371)
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 24373)
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 208 (class 1259 OID 24375)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 24378)
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24381)
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 26401)
-- Name: tsgnomalguinaldo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomalguinaldo (
    cod_aguinaldoid integer NOT NULL,
    imp_aguinaldo numeric(10,2) NOT NULL,
    cod_tipoaguinaldo character(1) NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL,
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomalguinaldo OWNER TO suite;

--
-- TOC entry 4726 (class 0 OID 0)
-- Dependencies: 307
-- Name: TABLE tsgnomalguinaldo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomalguinaldo IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';


--
-- TOC entry 308 (class 1259 OID 26409)
-- Name: tsgnomargumento; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomargumento (
    cod_argumentoid integer NOT NULL,
    cod_nbargumento character varying(30) NOT NULL,
    cod_clavearg character varying(5) NOT NULL,
    imp_valorconst numeric(10,2),
    des_funcionbd character varying(60),
    bol_estatus boolean NOT NULL,
    txt_descripcion text NOT NULL,
    cod_tipoargumento character(1),
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomargumento OWNER TO suite;

--
-- TOC entry 309 (class 1259 OID 26417)
-- Name: tsgnombitacora; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnombitacora (
    cod_bitacoraid integer NOT NULL,
    xml_bitacora xml NOT NULL,
    cod_tablaid_fk integer NOT NULL
);


ALTER TABLE sgnom.tsgnombitacora OWNER TO suite;

--
-- TOC entry 310 (class 1259 OID 26425)
-- Name: tsgnomcabecera; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabecera (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    cnu_totalemp integer
);


ALTER TABLE sgnom.tsgnomcabecera OWNER TO suite;

--
-- TOC entry 311 (class 1259 OID 26430)
-- Name: tsgnomcabeceraht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcabeceraht (
    cod_cabeceraid integer NOT NULL,
    cod_nbnomina character varying(40) NOT NULL,
    fec_creacion date NOT NULL,
    fec_ejecucion date,
    fec_cierre date,
    imp_totpercepcion numeric(10,2),
    imp_totdeduccion numeric(10,2),
    imp_totalemp numeric(10,2),
    cod_quincenaid_fk integer NOT NULL,
    cod_tiponominaid_fk integer NOT NULL,
    cod_estatusnomid_fk integer NOT NULL,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    cnu_totalemp integer
);


ALTER TABLE sgnom.tsgnomcabeceraht OWNER TO suite;

--
-- TOC entry 312 (class 1259 OID 26435)
-- Name: tsgnomcalculo; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcalculo (
    cod_calculoid integer NOT NULL,
    cod_tpcalculo character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomcalculo OWNER TO suite;

--
-- TOC entry 4732 (class 0 OID 0)
-- Dependencies: 312
-- Name: TABLE tsgnomcalculo; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcalculo IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 313 (class 1259 OID 26440)
-- Name: tsgnomcatincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcatincidencia (
    cod_catincidenciaid integer NOT NULL,
    cod_claveincidencia character varying(5),
    cod_nbincidencia character varying(20),
    cod_perfilincidencia character varying(25),
    bol_estatus boolean,
    cod_tipoincidencia character(1),
    imp_monto numeric(10,2)
);


ALTER TABLE sgnom.tsgnomcatincidencia OWNER TO suite;

--
-- TOC entry 4734 (class 0 OID 0)
-- Dependencies: 313
-- Name: TABLE tsgnomcatincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomcatincidencia IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';


--
-- TOC entry 314 (class 1259 OID 26445)
-- Name: tsgnomclasificador; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomclasificador (
    cod_clasificadorid integer NOT NULL,
    cod_tpclasificador character varying(20),
    bol_estatus boolean
);


ALTER TABLE sgnom.tsgnomclasificador OWNER TO suite;

--
-- TOC entry 315 (class 1259 OID 26450)
-- Name: tsgnomcncptoquinc; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquinc (
    cod_cncptoquincid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquinc OWNER TO suite;

--
-- TOC entry 316 (class 1259 OID 26458)
-- Name: tsgnomcncptoquincht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomcncptoquincht (
    cod_cncptoquinchtid integer NOT NULL,
    cod_empquincenaid_fk integer NOT NULL,
    cod_conceptoid_fk integer NOT NULL,
    imp_concepto numeric(10,2) NOT NULL,
    imp_gravado numeric(10,2),
    imp_exento numeric(10,2),
    xml_desgloce xml
);


ALTER TABLE sgnom.tsgnomcncptoquincht OWNER TO suite;

--
-- TOC entry 317 (class 1259 OID 26466)
-- Name: tsgnomconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconcepto (
    cod_conceptoid integer NOT NULL,
    cod_nbconcepto character varying(20) NOT NULL,
    cod_claveconcepto character varying(4) NOT NULL,
    cnu_prioricalculo integer NOT NULL,
    cnu_articulo integer NOT NULL,
    bol_estatus boolean NOT NULL,
    cod_formulaid_fk integer,
    cod_tipoconceptoid_fk integer NOT NULL,
    cod_calculoid_fk integer NOT NULL,
    cod_conceptosatid_fk integer NOT NULL,
    cod_frecuenciapago character varying(20) NOT NULL,
    cod_partidaprep integer NOT NULL,
    cnu_cuentacontable integer NOT NULL,
    cod_gravado character(1),
    cod_excento character(1),
    bol_retroactividad boolean NOT NULL,
    cnu_topeex integer,
    cod_clasificadorid_fk integer NOT NULL,
    bol_aplicaisn boolean,
    cod_tiponominaid_fk integer,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomconcepto OWNER TO suite;

--
-- TOC entry 4739 (class 0 OID 0)
-- Dependencies: 317
-- Name: TABLE tsgnomconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconcepto IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';


--
-- TOC entry 318 (class 1259 OID 26471)
-- Name: tsgnomconceptosat; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconceptosat (
    cod_conceptosatid integer NOT NULL,
    des_conceptosat character varying(51) NOT NULL,
    des_descconcepto character varying(51) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomconceptosat OWNER TO suite;

--
-- TOC entry 4741 (class 0 OID 0)
-- Dependencies: 318
-- Name: TABLE tsgnomconceptosat; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconceptosat IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 319 (class 1259 OID 26476)
-- Name: tsgnomconfpago; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomconfpago (
    cod_confpagoid integer NOT NULL,
    bol_pagoempleado boolean,
    bol_pagorh boolean,
    bol_pagofinanzas boolean,
    cod_empquincenaid_fk integer
);


ALTER TABLE sgnom.tsgnomconfpago OWNER TO suite;

--
-- TOC entry 4743 (class 0 OID 0)
-- Dependencies: 319
-- Name: TABLE tsgnomconfpago; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomconfpago IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';


--
-- TOC entry 320 (class 1259 OID 26481)
-- Name: tsgnomejercicio; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomejercicio (
    cod_ejercicioid integer NOT NULL,
    cnu_valorejercicio integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomejercicio OWNER TO suite;

--
-- TOC entry 321 (class 1259 OID 26486)
-- Name: tsgnomempleados; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempleados (
    cod_empleadoid integer NOT NULL,
    fec_ingreso date NOT NULL,
    fec_salida date,
    bol_estatus boolean NOT NULL,
    cod_empleado_fk integer NOT NULL,
    imp_sueldoimss numeric(10,2),
    imp_honorarios numeric(10,2),
    cod_tipoimss character(1),
    cod_tipohonorarios character(1),
    cod_banco character varying(50),
    cod_sucursal integer,
    cod_cuenta character varying(20),
    txt_descripcionbaja text,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date,
    imp_finiquito numeric(10,2),
    cod_clave character varying(18)
);


ALTER TABLE sgnom.tsgnomempleados OWNER TO suite;

--
-- TOC entry 4746 (class 0 OID 0)
-- Dependencies: 321
-- Name: TABLE tsgnomempleados; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempleados IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';


--
-- TOC entry 322 (class 1259 OID 26494)
-- Name: tsgnomempquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincena (
    cod_empquincenaid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincena OWNER TO suite;

--
-- TOC entry 4748 (class 0 OID 0)
-- Dependencies: 322
-- Name: TABLE tsgnomempquincena; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomempquincena IS 'bol_estatusemp

(

activo, inactivo

)';


--
-- TOC entry 323 (class 1259 OID 26499)
-- Name: tsgnomempquincenaht; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomempquincenaht (
    cod_empquincenahtid integer NOT NULL,
    cod_empleadoid_fk integer NOT NULL,
    cod_cabeceraid_fk integer NOT NULL,
    imp_totpercepcion numeric(10,2) NOT NULL,
    imp_totdeduccion numeric(10,2) NOT NULL,
    imp_totalemp numeric(10,2) NOT NULL,
    bol_estatusemp boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomempquincenaht OWNER TO suite;

--
-- TOC entry 324 (class 1259 OID 26504)
-- Name: tsgnomestatusnom; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomestatusnom (
    cod_estatusnomid integer NOT NULL,
    cod_estatusnomina character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomestatusnom OWNER TO suite;

--
-- TOC entry 4751 (class 0 OID 0)
-- Dependencies: 324
-- Name: TABLE tsgnomestatusnom; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomestatusnom IS 'cod_estatusnomid 

estatus

(

abierta, 

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';


--
-- TOC entry 325 (class 1259 OID 26509)
-- Name: tsgnomformula; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomformula (
    cod_formulaid integer NOT NULL,
    des_nbformula character varying(60) NOT NULL,
    des_formula character varying(250) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomformula OWNER TO suite;

--
-- TOC entry 4753 (class 0 OID 0)
-- Dependencies: 325
-- Name: TABLE tsgnomformula; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomformula IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 326 (class 1259 OID 26514)
-- Name: tsgnomfuncion; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomfuncion (
    cod_funcionid integer NOT NULL,
    cod_nbfuncion character varying(15) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomfuncion OWNER TO suite;

--
-- TOC entry 327 (class 1259 OID 26519)
-- Name: tsgnomhisttabla; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomhisttabla (
    cod_tablaid integer NOT NULL,
    cod_nbtabla character varying(18) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomhisttabla OWNER TO suite;

--
-- TOC entry 328 (class 1259 OID 26524)
-- Name: tsgnomincidencia; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomincidencia (
    cod_incidenciaid integer NOT NULL,
    cod_catincidenciaid_fk integer NOT NULL,
    cnu_cantidad smallint,
    des_actividad character varying(100),
    txt_comentarios text,
    cod_empreporta_fk integer,
    cod_empautoriza_fk integer,
    imp_monto numeric(10,2),
    xml_detcantidad xml,
    bol_estatus boolean,
    cod_quincenaid_fk integer NOT NULL,
    bol_validacion boolean,
    fec_validacion date,
    aud_codcreadopor integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_codmodificadopor integer,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnomincidencia OWNER TO suite;

--
-- TOC entry 4757 (class 0 OID 0)
-- Dependencies: 328
-- Name: TABLE tsgnomincidencia; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomincidencia IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';


--
-- TOC entry 329 (class 1259 OID 26532)
-- Name: tsgnommanterceros; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnommanterceros (
    cod_mantercerosid integer NOT NULL,
    cod_conceptoid_fk integer,
    imp_monto numeric(10,2),
    cod_quincenainicio_fk integer,
    cod_quincenafin_fk integer,
    cod_empleadoid_fk integer,
    cod_frecuenciapago character varying(20),
    bol_estatus boolean,
    aud_codcreadopor integer NOT NULL,
    aud_codmodificadopor integer,
    aud_fecreacion date NOT NULL,
    aud_fecmodificacion date
);


ALTER TABLE sgnom.tsgnommanterceros OWNER TO suite;

--
-- TOC entry 330 (class 1259 OID 26537)
-- Name: tsgnomquincena; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomquincena (
    cod_quincenaid integer NOT NULL,
    des_quincena character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_fin date NOT NULL,
    fec_pago date NOT NULL,
    fec_dispersion date NOT NULL,
    cnu_numquincena integer NOT NULL,
    cod_ejercicioid_fk integer NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomquincena OWNER TO suite;

--
-- TOC entry 331 (class 1259 OID 26542)
-- Name: tsgnomtipoconcepto; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtipoconcepto (
    cod_tipoconceptoid integer NOT NULL,
    cod_tipoconcepto character varying(25) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtipoconcepto OWNER TO suite;

--
-- TOC entry 4761 (class 0 OID 0)
-- Dependencies: 331
-- Name: TABLE tsgnomtipoconcepto; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtipoconcepto IS 'bol_estatus

(

activo, inactivo

)';


--
-- TOC entry 332 (class 1259 OID 26547)
-- Name: tsgnomtiponomina; Type: TABLE; Schema: sgnom; Owner: suite
--

CREATE TABLE sgnom.tsgnomtiponomina (
    cod_tiponominaid integer NOT NULL,
    cod_nomina character varying(30) NOT NULL,
    bol_estatus boolean NOT NULL
);


ALTER TABLE sgnom.tsgnomtiponomina OWNER TO suite;

--
-- TOC entry 4763 (class 0 OID 0)
-- Dependencies: 332
-- Name: TABLE tsgnomtiponomina; Type: COMMENT; Schema: sgnom; Owner: suite
--

COMMENT ON TABLE sgnom.tsgnomtiponomina IS 'bol_estatus (activa, inactiva)';


--
-- TOC entry 333 (class 1259 OID 26723)
-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 26725)
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 26727)
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 26729)
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 26731)
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 26733)
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 26735)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 26737)
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 26739)
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 26741)
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 26743)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 26745)
-- Name: seq_estatus; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_estatus
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_estatus OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 26747)
-- Name: seq_evacapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacapacitacion OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 26749)
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 26751)
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 26753)
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 26755)
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO postgres;

--
-- TOC entry 350 (class 1259 OID 26757)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 26759)
-- Name: seq_logistica; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_logistica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_logistica OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 26761)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_lugar OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 26763)
-- Name: seq_modo; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_modo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_modo OWNER TO postgres;

--
-- TOC entry 354 (class 1259 OID 26765)
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 26767)
-- Name: seq_plancapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_plancapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_plancapacitacion OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 26769)
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 26771)
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 26773)
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 26775)
-- Name: seq_proceso; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proceso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proceso OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 26777)
-- Name: seq_proveedor; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proveedor
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proveedor OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 26779)
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO postgres;

--
-- TOC entry 362 (class 1259 OID 26781)
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 26783)
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 26785)
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 26787)
-- Name: seq_rolempleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_rolempleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_rolempleado OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 26789)
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO postgres;

--
-- TOC entry 367 (class 1259 OID 26791)
-- Name: seq_tipocapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_tipocapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_tipocapacitacion OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 26793)
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO postgres;

--
-- TOC entry 369 (class 1259 OID 26795)
-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer,
    cod_creadopor integer,
    cod_modificadopor integer,
    fec_creacion date,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhareas OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 26799)
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 26802)
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 26809)
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 26818)
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 26822)
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO postgres;

--
-- TOC entry 375 (class 1259 OID 26827)
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 26833)
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO postgres;

--
-- TOC entry 377 (class 1259 OID 26839)
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO postgres;

--
-- TOC entry 378 (class 1259 OID 26848)
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 26858)
-- Name: tsgrhencuesta_participantes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta_participantes (
    cod_participantenc integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_respuesta integer,
    respuesta_abierta text
);


ALTER TABLE sgrh.tsgrhencuesta_participantes OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 26865)
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO postgres;

--
-- TOC entry 381 (class 1259 OID 26872)
-- Name: tsgrhestatuscapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhestatuscapacitacion (
    cod_estatus integer DEFAULT nextval('sgrh.seq_estatus'::regclass) NOT NULL,
    des_estatus character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhestatuscapacitacion OWNER TO postgres;

--
-- TOC entry 382 (class 1259 OID 26876)
-- Name: tsgrhevacapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacapacitacion (
    cod_evacapacitacion integer DEFAULT nextval('sgrh.seq_evacapacitacion'::regclass) NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    auf_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhevacapacitacion OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 26880)
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 26887)
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 26895)
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO postgres;

--
-- TOC entry 386 (class 1259 OID 26902)
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 26906)
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 26910)
-- Name: tsgrhlogistica; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlogistica (
    cod_logistica integer DEFAULT nextval('sgrh.seq_logistica'::regclass) NOT NULL,
    tim_horario integer NOT NULL,
    des_requerimientos character varying(200),
    fec_fecinicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_capacitacion integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhlogistica OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 26914)
-- Name: tsgrhlugares; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlugares (
    cod_lugar integer DEFAULT nextval('sgrh.seq_lugar'::regclass) NOT NULL,
    des_lugar character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhlugares OWNER TO postgres;

--
-- TOC entry 390 (class 1259 OID 26918)
-- Name: tsgrhmodo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhmodo (
    cod_modo integer DEFAULT nextval('sgrh.seq_modo'::regclass) NOT NULL,
    des_nbmodo character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhmodo OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 26922)
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 26926)
-- Name: tsgrhplancapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplancapacitacion (
    cod_plancapacitacion integer DEFAULT nextval('sgrh.seq_plancapacitacion'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_modo integer NOT NULL,
    cod_tipocapacitacion integer NOT NULL,
    des_criterios character varying(200) NOT NULL,
    cod_proceso integer NOT NULL,
    des_instructor character varying(50) NOT NULL,
    cod_proveedor integer NOT NULL,
    cod_estatus integer NOT NULL,
    des_comentarios character varying(200),
    des_evaluacion character varying(50),
    cod_lugar integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhplancapacitacion OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 26933)
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 26942)
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 26946)
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 26950)
-- Name: tsgrhprocesos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhprocesos (
    cod_proceso integer DEFAULT nextval('sgrh.seq_proceso'::regclass) NOT NULL,
    des_nbproceso character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhprocesos OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 26954)
-- Name: tsgrhproveedores; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhproveedores (
    cod_proveedor integer DEFAULT nextval('sgrh.seq_proveedor'::regclass) NOT NULL,
    des_nbproveedor character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhproveedores OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 26958)
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL,
    cod_acronimo character varying(5)
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 26962)
-- Name: tsgrhrelacionroles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrelacionroles (
    cod_plancapacitacion integer NOT NULL,
    cod_rolempleado integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrelacionroles OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 26965)
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 26969)
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 26973)
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO postgres;

--
-- TOC entry 403 (class 1259 OID 26982)
-- Name: tsgrhrolempleado; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrolempleado (
    cod_rolempleado integer DEFAULT nextval('sgrh.seq_rolempleado'::regclass) NOT NULL,
    des_nbrol character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrolempleado OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 26986)
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 26990)
-- Name: tsgrhtipocapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhtipocapacitacion (
    cod_tipocapacitacion integer DEFAULT nextval('sgrh.seq_tipocapacitacion'::regclass) NOT NULL,
    des_nbtipocapacitacion character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhtipocapacitacion OWNER TO postgres;

--
-- TOC entry 406 (class 1259 OID 26994)
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 24479)
-- Name: seq_agenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_agenda OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 24481)
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_archivo OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 24483)
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_asistente OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24485)
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_attach OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 24487)
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_categoriafaq OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 24489)
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


ALTER TABLE sgrt.seq_chat OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 24491)
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ciudad OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 24493)
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsagenda OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 24495)
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsreunion OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 24497)
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_compromiso OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 24499)
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_contacto OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 24501)
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_correo OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 24503)
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_depto OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 24505)
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_edoacuerdo OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 24507)
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_elemento OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 24509)
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_estadorep OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 24511)
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_faq OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 24513)
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_grupo OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 24515)
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_invitado OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 24517)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_lugar OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 24519)
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_mensaje OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 24521)
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_nota OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 24523)
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_plantillacorreo OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 24525)
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_prioridad OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 24527)
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_resp OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 24529)
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuesta OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 27002)
-- Name: seq_respuestas_participantes; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuestas_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuestas_participantes OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 24531)
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_reunion OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 24533)
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_servicio OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 24535)
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_solicitud OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 24537)
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ticket OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 24539)
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_topico OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 24541)
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtagenda OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 24546)
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtarchivos OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 24553)
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


ALTER TABLE sgrt.tsgrtasistentes OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 24557)
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


ALTER TABLE sgrt.tsgrtattchticket OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 24564)
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtayudatopico OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 24568)
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcategoriafaq OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 24575)
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


ALTER TABLE sgrt.tsgrtchat OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 24582)
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


ALTER TABLE sgrt.tsgrtciudades OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 24586)
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosagenda OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 24593)
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosreunion OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 24600)
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


ALTER TABLE sgrt.tsgrtcompromisos OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 24606)
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcorreo OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 24612)
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtdatossolicitud OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 24615)
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtdepartamento OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 24624)
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtedosolicitudes OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 24627)
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


ALTER TABLE sgrt.tsgrtelementos OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 24631)
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


ALTER TABLE sgrt.tsgrtestados OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 24635)
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtfaq OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 24642)
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtgrupo OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 24647)
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


ALTER TABLE sgrt.tsgrtinvitados OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 24654)
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


ALTER TABLE sgrt.tsgrtlugares OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 24658)
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtmsjticket OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 24668)
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtnota OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 24676)
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


ALTER TABLE sgrt.tsgrtplantillacorreos OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 24684)
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtprioridad OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 24688)
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtresppredefinida OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 24695)
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtrespuesta OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 24702)
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


ALTER TABLE sgrt.tsgrtreuniones OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 24709)
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


ALTER TABLE sgrt.tsgrtservicios OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 24713)
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


ALTER TABLE sgrt.tsgrtsolicitudservicios OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 24717)
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


ALTER TABLE sgrt.tsgrtticket OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 25195)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 25197)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 25199)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 25201)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 25203)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 25205)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 25207)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 25209)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 25211)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 25213)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 25215)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 25217)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 25219)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 25221)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 25223)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 25225)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 25227)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 25229)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 25233)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2)
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 25237)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 25244)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 25251)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer DEFAULT nextval('sisat.seq_cotizaciones'::regclass) NOT NULL,
    des_nbciudad character varying(20),
    des_nbestado character varying(20),
    fec_fecha date,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_compania character varying(50),
    des_nbservicio character varying(50),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_nbatentamente character varying(60),
    des_correoatentamente character varying(50),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 25258)
-- Name: tsisatcursosycerticados; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcursosycerticados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycerticados OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 25262)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 25266)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 25273)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 25277)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 25284)
-- Name: tsisatfirmas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatfirmas (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_contratacion integer NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatfirmas OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 25288)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad1 character varying(50),
    des_habilidad2 character varying(50),
    des_habilidad3 character varying(50),
    des_habilidad4 character varying(50),
    des_habilidad5 character varying(50),
    des_habilidad6 character varying(50),
    des_habilidad7 character varying(50),
    des_habilidad8 character varying(50),
    des_habilidad9 character varying(50),
    des_habilidad10 character varying(50),
    des_habilidad11 character varying(50),
    des_habilidad12 character varying(50),
    des_habilidad13 character varying(50),
    des_habilidad14 character varying(50),
    des_habilidad15 character varying(50),
    des_habilidad16 character varying(50),
    des_habilidad17 character varying(50),
    des_habilidad18 character varying(50),
    des_habilidad19 character varying(50),
    des_habilidad20 character varying(50),
    des_habilidad21 character varying(50),
    des_habilidad22 character varying(50),
    des_habilidad23 character varying(50),
    des_habilidad24 character varying(50),
    des_habilidad25 character varying(50),
    des_habilidad26 character varying(50),
    des_habilidad27 character varying(50),
    des_habilidad28 character varying(50),
    des_habilidad29 character varying(50),
    des_habilidad30 character varying(50),
    des_dominio1 integer DEFAULT 0,
    des_dominio2 integer DEFAULT 0,
    des_dominio3 integer DEFAULT 0,
    des_dominio4 integer DEFAULT 0,
    des_dominio5 integer DEFAULT 0,
    des_dominio6 integer DEFAULT 0,
    des_dominio7 integer DEFAULT 0,
    des_dominio8 integer DEFAULT 0,
    des_dominio9 integer DEFAULT 0,
    des_dominio10 integer DEFAULT 0,
    des_dominio11 integer DEFAULT 0,
    des_dominio12 integer DEFAULT 0,
    des_dominio13 integer DEFAULT 0,
    des_dominio14 integer DEFAULT 0,
    des_dominio15 integer DEFAULT 0,
    des_dominio16 integer DEFAULT 0,
    des_dominio17 integer DEFAULT 0,
    des_dominio18 integer DEFAULT 0,
    des_dominio19 integer DEFAULT 0,
    des_dominio20 integer DEFAULT 0,
    des_dominio21 integer DEFAULT 0,
    des_dominio22 integer DEFAULT 0,
    des_dominio23 integer DEFAULT 0,
    des_dominio24 integer DEFAULT 0,
    des_dominio25 integer DEFAULT 0,
    des_dominio26 integer DEFAULT 0,
    des_dominio27 integer DEFAULT 0,
    des_dominio28 integer DEFAULT 0,
    des_dominio29 integer DEFAULT 0,
    des_dominio30 integer DEFAULT 0
);


ALTER TABLE sisat.tsisathabilidades OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 25325)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_nbidioma character varying(20) NOT NULL,
    cod_nivel character varying(20) NOT NULL,
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatidiomas OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 25329)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_nbcompania character varying(50),
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    des_correogpy character varying(50) NOT NULL,
    cod_cliente integer NOT NULL,
    des_correoclte character varying(50) NOT NULL,
    des_empresaclte character varying(50),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 25336)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatprospectos OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 25345)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatproyectos OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 25349)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatvacantes OWNER TO postgres;

--
-- TOC entry 4502 (class 0 OID 24375)
-- Dependencies: 208
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'Sistema de Recursos Humanos y Ambiente de Trabajo', 'SGRHAT');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'Sistema de Integración', 'SISAT');


--
-- TOC entry 4503 (class 0 OID 24378)
-- Dependencies: 209
-- Data for Name: tsgcotipousuario; Type: TABLE DATA; Schema: sgco; Owner: postgres
--



--
-- TOC entry 4504 (class 0 OID 24381)
-- Dependencies: 210
-- Data for Name: tsgcousuarios; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (2, 11, 'adrian.suarez@gmail.com', '123456789', 'adrian_suarez', '123456789');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (3, 12, 'carlos.antonio@gmail.com', 'abcdefg', 'carlos_antonio', 'abcdefg');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (4, 13, 'angel.roano@gmail.com', '123456', 'angel_roano', '123456');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (1, 10, 'mateorj96@gmail.com', '143100365m@teorj96', 'mateorj96', 'root');


--
-- TOC entry 4601 (class 0 OID 26401)
-- Dependencies: 307
-- Data for Name: tsgnomalguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4602 (class 0 OID 26409)
-- Dependencies: 308
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4603 (class 0 OID 26417)
-- Dependencies: 309
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4604 (class 0 OID 26425)
-- Dependencies: 310
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, cnu_totalemp) VALUES (1, '1', '2018-12-12', '2018-12-12', '2018-12-12', 123.00, 321.00, 1234.00, 1, 1, 1, 10, 10, '2018-12-12', '2018-12-12', 10);
INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, cnu_totalemp) VALUES (2, '1', '2018-12-12', '2018-12-12', '2018-12-12', 123.00, 321.00, 1234.00, 1, 1, 1, 10, 10, '2018-12-12', '2018-12-12', NULL);


--
-- TOC entry 4605 (class 0 OID 26430)
-- Dependencies: 311
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4606 (class 0 OID 26435)
-- Dependencies: 312
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4607 (class 0 OID 26440)
-- Dependencies: 313
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4608 (class 0 OID 26445)
-- Dependencies: 314
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4609 (class 0 OID 26450)
-- Dependencies: 315
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4610 (class 0 OID 26458)
-- Dependencies: 316
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4611 (class 0 OID 26466)
-- Dependencies: 317
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4612 (class 0 OID 26471)
-- Dependencies: 318
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4613 (class 0 OID 26476)
-- Dependencies: 319
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4614 (class 0 OID 26481)
-- Dependencies: 320
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (1, 1970, true);
INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (2, 1970, true);


--
-- TOC entry 4615 (class 0 OID 26486)
-- Dependencies: 321
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (1, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (2, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (3, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (10, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (11, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (12, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (13, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (14, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (15, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (16, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (17, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (18, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (19, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (20, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, txt_descripcionbaja, aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion, imp_finiquito, cod_clave) VALUES (21, '2019-05-07', '2019-05-10', true, 10, 123.00, 213.00, '1', 'o', '123', 321, '12345', 'baja', 10, '2190-12-21', 10, '1990-12-21', 123.00, '12345');


--
-- TOC entry 4616 (class 0 OID 26494)
-- Dependencies: 322
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (1, 1, 1, 213.12, 123.32, 2134.32, true);
INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (2, 1, 1, 213.12, 123.32, 2134.32, true);


--
-- TOC entry 4617 (class 0 OID 26499)
-- Dependencies: 323
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4618 (class 0 OID 26504)
-- Dependencies: 324
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (1, 'validada', true);
INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (2, 'validada', true);


--
-- TOC entry 4619 (class 0 OID 26509)
-- Dependencies: 325
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4620 (class 0 OID 26514)
-- Dependencies: 326
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4621 (class 0 OID 26519)
-- Dependencies: 327
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4622 (class 0 OID 26524)
-- Dependencies: 328
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4623 (class 0 OID 26532)
-- Dependencies: 329
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4624 (class 0 OID 26537)
-- Dependencies: 330
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (1, 'test', '2018-01-01', '2018-01-01', '2018-01-01', '2018-01-01', 1, 1, true);
INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (5, 'test', '2018-01-01', '2018-01-01', '2018-01-01', '2018-01-01', 1, 1, true);


--
-- TOC entry 4625 (class 0 OID 26542)
-- Dependencies: 331
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 4626 (class 0 OID 26547)
-- Dependencies: 332
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (1, 'pruebas', true);
INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (2, 'pruebas', true);


--
-- TOC entry 4663 (class 0 OID 26795)
-- Dependencies: 369
-- Data for Name: tsgrhareas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (2, 'FABRICA DE SOFTWARE', 'FS', true, 1, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (3, 'DISEÑO', 'DS', true, 1, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (4, 'SOPORTE TECNICO', 'ST', true, 1, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (1, 'BASE DE DATOS', 'DB', true, 2, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (5, 'RECURSOS HUMANOS', 'RH', true, 1, NULL, NULL, NULL, NULL);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion) VALUES (6, 'Marketing', 'mrg', true, 2, NULL, NULL, NULL, NULL);


--
-- TOC entry 4664 (class 0 OID 26799)
-- Dependencies: 370
-- Data for Name: tsgrhasignacionesemp; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4665 (class 0 OID 26802)
-- Dependencies: 371
-- Data for Name: tsgrhcapacitaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4666 (class 0 OID 26809)
-- Dependencies: 372
-- Data for Name: tsgrhcartaasignacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4667 (class 0 OID 26818)
-- Dependencies: 373
-- Data for Name: tsgrhcatrespuestas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (1, 'Nunca', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (2, 'Algunas veces', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (3, 'Regular', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (4, 'Con frecuencia', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (5, 'Siempre', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (6, 'Muy malo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (7, 'Malo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (8, 'Bueno', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (9, 'Muy bueno', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (10, 'Muy bajo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (11, 'Bajo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (12, 'Alto', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (13, 'Muy alto', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (14, 'Muy incómodo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (15, 'Incómodo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (16, 'Soportable', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (17, 'Confortable', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (18, 'Muy confortable', 5);


--
-- TOC entry 4668 (class 0 OID 26822)
-- Dependencies: 374
-- Data for Name: tsgrhclientes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (1, 'Pemex', 'Tamaulipas', 'Samuel Velzaco', 'samuel-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (2, 'Cable vision', 'Mexico', 'Alfredo Gomez', 'alfgom-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (3, 'Telecom', 'Mexico', 'Miguel Romero', 'romero@gmail.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (4, 'Bancomer', 'Mexico', 'Jose Mauro', 'jose@bbva.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (5, 'Nuevo', 'conocido', 'josue', 'dfd2@nuevo.com', '556322');


--
-- TOC entry 4669 (class 0 OID 26827)
-- Dependencies: 375
-- Data for Name: tsgrhcontrataciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4670 (class 0 OID 26833)
-- Dependencies: 376
-- Data for Name: tsgrhcontratos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4671 (class 0 OID 26839)
-- Dependencies: 377
-- Data for Name: tsgrhempleados; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (10, 'MATEO', NULL, 'RODRIGUEZ', 'JUAREZ', 'DOMICILIO CONOCIDO', '1996-04-09', 'MECATLAN, VER.', 23, 'mateorj96@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROJM960409HPZDAT22', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', NULL, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (11, 'ADRIAN', NULL, 'SUAREZ', 'DE LA CRUZ', 'DOMICILIO CONOCIDO', '1996-06-14', 'AHUACATLAN, PUE.', 23, 'adrian.suarezc@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'SDCA960614HPZDBT23', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (12, 'CARLOS', NULL, 'ANTONIO', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1996-03-12', 'AHUACATLAN, PUE.', 23, 'trinidad.carlos@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ANTC960312HPZDCT24', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (13, 'ANGEL', 'ANTONIO', 'ROANO', 'ALVARADO', 'DOMICILIO CONOCIDO', '1995-02-19', 'TETELA, PUE.', 23, 'angel.antonio.roa@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROAA950219HPZDDT25', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (14, 'JUAN', '', 'MARQUEZ', 'SAVEDO', 'DOMICILIO CONOCIDO', '1995-02-12', 'APIZACO, TLAX', 23, 'maito1.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MASJ950212HPZDET26', NULL, '', '', true, 1, 0, NULL, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (15, 'ANTONIO', '', 'HERRERA', 'CHAVEZ', 'DOMICILIO CONOCIDO', '1995-02-20', 'TLAXCALA, TLAX', 23, 'maito2.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'HECA950220HPZDFT27', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (16, 'JAVIER', '', 'CHICHARITO', 'HERNANDEZ', 'DOMICILIO CONOCIDO', '1995-02-21', 'AHUCATLAN, PUE.', 23, 'maito3.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'CHHJ950221HPZDGT28', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (17, 'MANUEL', '', 'GONZALEZ', 'PEREZ', 'DOMICILIO CONOCIDO', '1995-02-22', 'TEPANGO PUE.', 23, 'maito4.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOPM950222HPZDHT29', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (18, 'SERGIO', '', 'RAMOS', 'FLORES', 'DOMICILIO CONOCIDO', '1995-03-01', 'ZAPOTITLAN, PUE.', 23, 'maito5.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'RAFS950301HPZDIT30', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (19, 'CARINE', '', 'BENZEMA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-02', 'AQUIXTLA, PUE.', 23, 'maito6.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'BEAC950302HPZDJT31', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (20, 'CAROLINA', '', 'JIMENEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '1995-03-03', 'ZACATLAN, PUE.', 23, 'maito7.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'JIVC950303HPZDKT32', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (21, 'VERONICA', '', 'SANCHEZ', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-04', 'CHIGNAHUAPAN, PUE.', 23, 'maito8.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'SATV950304HPZDLT33', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (22, 'HEIDY', '', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-05', 'TLAXCO, TLAX.', 23, 'maito9.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TRMH950305HPZDMT34', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (23, 'JESUS', 'MIGUEL', 'VELAZCO', 'MARQUEZ', 'DOMICILIO CONOCIDO', '1995-03-06', 'POZA RICA, VER.', 23, 'maito10.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MVMJ950306HPZDNT35', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (24, 'RAUL', '', 'ESPINOZA', 'MARTINEZ', 'DOMICILIO CONOCIDO', '1995-03-07', 'XALAPA, VER.', 23, 'maito11.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESMR950307HPZDOT36', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (25, 'JOSE', 'EDUARDO', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-08', 'COAHUILA, COAH.', 23, 'maito12.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ETMJ950308HPZDPT37', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (26, 'SARAHI', '', 'GONZALEZ', 'SUAREZ', 'DOMICILIO CONOCIDO', '1995-03-09', 'HUACHINANGO, PUE.', 23, 'maito13.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOSS950309HPZDQT38', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (27, 'FEDERICO', '', 'GUZMAN', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-10', 'TULANCINGO, HID.', 23, 'maito14.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GUTF950310HPZDRT39', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (28, 'JOSE', 'IVAN', 'VACILIO', 'SANCHEZ', 'DOMICILIO CONOCIDO', '1995-03-11', 'XOXONANCATLA, PUE.', 23, 'maito15.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'IVSJ950311HPZDTT40', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (29, 'ERICA', '', 'ESPINOZA', 'CANDELARIA', 'DOMICILIO CONOCIDO', '1995-03-12', 'XICOTEPEC, PUE.', 23, 'maito16.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESCE950312HPZDUT41', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (30, 'ROBERTO', '', 'ORTEGA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-13', 'JICOLAPA, PUE.', 23, 'maito17.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ORAR950313HPZDVT42', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (31, 'CESARIO', '', 'TELLEZ', 'REYES', 'DOMICILIO CONOCIDO', '1995-03-14', 'SANTA INES, PUE.', 23, 'maito18.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TERC950314HPZDWT43', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (32, 'CARLOS', 'EFREN', 'SANCHEZ', 'JUAN', 'DOMICILIO CONOCIDO', '1995-03-15', 'CHOLULA, PUE.', 23, 'maito19.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESJC950315HPZDXT44', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (33, 'JOSE', '', 'DE LOS SANTOS', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-16', 'DF, DF.', 23, 'maito20.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'DETJ950316HPZDYT45', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (34, 'FIDEL', '', 'SANCHEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '0022-01-17', 'ZACATELCO, TLAX.', 0, 'maito21.example@gmail.com', 'A-', '', '757581', '', NULL, NULL, NULL, '', '0012-08-19', 'dfgt45', '', 'SAVF950317HPZDZT46', NULL, '', '', true, 1, 1, 2, 5, NULL, NULL, '0012-08-19', '2019-04-11', 10, 11, 5);


--
-- TOC entry 4672 (class 0 OID 26848)
-- Dependencies: 378
-- Data for Name: tsgrhencuesta; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (5, 'ENCUESTA 00000001', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (6, 'ENCUESTA 00000002', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (8, 'ENCUESTA 00000004', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (9, 'ENCUESTA 00000005', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (10, 'ENCUESTA 00000006', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (11, 'ENCUESTA 00000007', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (12, 'ENCUESTA 00000008', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (13, 'ENCUESTA 00000009', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (14, 'ENCUESTA 00000010', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (15, 'ENCUESTA 00000011', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (16, 'ENCUESTA 00000012', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (7, 'ENCUESTA 00000003', 'Aceptado', '2018-12-08', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2019-02-15', 1);


--
-- TOC entry 4673 (class 0 OID 26858)
-- Dependencies: 379
-- Data for Name: tsgrhencuesta_participantes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (1, 29, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (2, 29, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (3, 29, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (4, 29, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (5, 29, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (6, 29, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (7, 29, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (8, 29, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (9, 29, 5, 90, 516, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (10, 29, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (11, 29, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (12, 29, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (13, 29, 5, 94, 528, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (14, 29, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (15, 29, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (16, 29, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (17, 29, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (18, 29, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (19, 29, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (20, 29, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (21, 29, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (22, 29, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (23, 29, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (24, 30, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (25, 30, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (26, 30, 5, 84, 489, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (27, 30, 5, 85, 494, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (28, 30, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (29, 30, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (30, 30, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (31, 30, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (32, 30, 5, 90, 515, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (33, 30, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (34, 30, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (35, 30, 5, 93, 522, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (36, 30, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (37, 30, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (38, 30, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (39, 30, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (40, 30, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (41, 30, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (42, 30, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (43, 30, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (44, 30, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (45, 30, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (46, 30, 5, 106, 573, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (47, 31, 5, 82, 479, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (48, 31, 5, 83, 484, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (49, 31, 5, 84, 487, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (50, 31, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (51, 31, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (52, 31, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (53, 31, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (54, 31, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (55, 31, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (56, 31, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (57, 31, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (58, 31, 5, 93, 524, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (59, 31, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (60, 31, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (61, 31, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (62, 31, 5, 98, 537, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (63, 31, 5, 99, 544, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (64, 31, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (65, 31, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (66, 31, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (67, 31, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (68, 31, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (69, 31, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (70, 32, 5, 82, 478, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (71, 32, 5, 83, 483, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (72, 32, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (73, 32, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (74, 32, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (75, 32, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (76, 32, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (77, 32, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (78, 32, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (79, 32, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (80, 32, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (81, 32, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (82, 32, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (83, 32, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (84, 32, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (85, 32, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (86, 32, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (87, 32, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (88, 32, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (89, 32, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (90, 32, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (91, 32, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (92, 32, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (93, 33, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (94, 33, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (95, 33, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (96, 33, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (97, 33, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (98, 33, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (99, 33, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (100, 33, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (101, 33, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (102, 33, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (103, 33, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (104, 33, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (105, 33, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (106, 33, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (107, 33, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (108, 33, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (109, 33, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (110, 33, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (111, 33, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (112, 33, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (113, 33, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (114, 33, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (115, 33, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (116, 34, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (117, 34, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (118, 34, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (119, 34, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (120, 34, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (121, 34, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (122, 34, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (123, 34, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (124, 34, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (125, 34, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (126, 34, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (127, 34, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (128, 34, 5, 94, 527, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (129, 34, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (130, 34, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (131, 34, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (132, 34, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (133, 34, 5, 100, 549, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (134, 34, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (135, 34, 5, 102, 559, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (136, 34, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (137, 34, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (138, 34, 5, 106, 575, NULL);


--
-- TOC entry 4674 (class 0 OID 26865)
-- Dependencies: 380
-- Data for Name: tsgrhescolaridad; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4675 (class 0 OID 26872)
-- Dependencies: 381
-- Data for Name: tsgrhestatuscapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'No aprobado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'En revisión', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Pendiente', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Detenido', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Aprobado', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4676 (class 0 OID 26876)
-- Dependencies: 382
-- Data for Name: tsgrhevacapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4677 (class 0 OID 26880)
-- Dependencies: 383
-- Data for Name: tsgrhevacontestadas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4678 (class 0 OID 26887)
-- Dependencies: 384
-- Data for Name: tsgrhevaluaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4679 (class 0 OID 26895)
-- Dependencies: 385
-- Data for Name: tsgrhexperienciaslaborales; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4680 (class 0 OID 26902)
-- Dependencies: 386
-- Data for Name: tsgrhfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4681 (class 0 OID 26906)
-- Dependencies: 387
-- Data for Name: tsgrhidiomas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (1, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (2, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (3, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (4, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (5, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (6, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (7, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (8, 'ingles', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (9, 'Ingles', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (10, 'Frances', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (11, 'Fran', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (12, 'Fran', 20, 20, NULL);


--
-- TOC entry 4682 (class 0 OID 26910)
-- Dependencies: 388
-- Data for Name: tsgrhlogistica; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4683 (class 0 OID 26914)
-- Dependencies: 389
-- Data for Name: tsgrhlugares; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4684 (class 0 OID 26918)
-- Dependencies: 390
-- Data for Name: tsgrhmodo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Interno', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Externo', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4685 (class 0 OID 26922)
-- Dependencies: 391
-- Data for Name: tsgrhperfiles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (1, 'Administrador');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (2, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (3, 'Prueba');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (4, '.NET, java scrip');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (5, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (6, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (7, 'luness');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (8, 'nuevo');


--
-- TOC entry 4686 (class 0 OID 26926)
-- Dependencies: 392
-- Data for Name: tsgrhplancapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4687 (class 0 OID 26933)
-- Dependencies: 393
-- Data for Name: tsgrhplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4688 (class 0 OID 26942)
-- Dependencies: 394
-- Data for Name: tsgrhpreguntasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (107, 'En su centro de trabajo las oportunidades de desarrollo laboral solo las reciben unas cuantas personas privilegiadas', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (109, 'En  su centro de trabajo se cuenta con programas de capacitación en materia de igualdad laboral y no discriminación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (111, 'En su centro de trabajo para lograr la contratación, una promoción o un ascenso cuentan más las recomendaciones que los conocimientos y capacidades de la persona.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (113, 'En su centro de trabajo la competencia por mejores puestos, condiciones laborales o salariales es justa y equitativa.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (115, 'En su centro de trabajo se cuenta con un sistema de evaluación de desempeño del personal.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (117, 'Usted siente que se le trata con respeto en su trabajo actual.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (119, 'En su centro de trabajo todas las personas que laboran obtienen un trato digno y decente.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (126, 'En su centro de trabajo existen campañas de difusión internas de promoción de la igualdad laboral y no discriminación.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (137, 'La organización cuenta con planes y acciones específicos destinados a mejorar mi trabajo.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (141, 'El nivel de compromiso por apoyar el trabajo de los demás en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (143, 'Mi jefe me respalda frente a sus superiores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (145, 'Participo de las actividades culturales y recreacionales que la organización realiza.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (147, 'Mi jefe me brinda la retroalimentación necesaria para reforzar mis puntos débiles según la evaluación de desempeño.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (149, 'Los jefes reconocen y valoran mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (151, 'La distribución de la carga de trabajo que tiene mi área es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (153, '¿Cómo calificaría su nivel de satisfacción con el trabajo que realiza en la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (155, 'Te agradeceremos nos hagas llegar algunos comentarios acerca de aspectos que ayudarían a mejorar nuestro ambiente de trabajo.', true, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (157, 'Usted tiene el suficiente tiempo para realizar su trabajo habitual:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (159, '¿Considera que recibe una justa retribución económica por las labores desempeñadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (161, '¿Cómo calificaría su nivel de satisfacción por trabajar en la organización?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (121, 'En su centro de trabajo, en general hay personas que discriminan, tratan mal o le faltan el respeto a sus compañeras/os, colegas o subordinadas/os.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (124, 'En su centro de trabajo  las y los superiores reciben un trato mucho más respetuoso que subordinados(as) y personal administrativo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (128, 'En su centro de trabajo las cargas de trabajo se distribuyen de acuerdo a la responsabilidad del cargo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (130, 'En mi oficina se fomenta y desarrolla el trabajo en equipo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (132, 'Existe comunicación dentro de mi grupo de trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (134, 'Siento que no me alcanza el tiempo para completar mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (136, 'La relación entre compañeros de trabajo en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (138, 'La organización otorga buenos y equitativos beneficios a los trabajadores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (140, 'Las remuneraciones están al nivel de los sueldos de mis colegas en el mercado', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (95, 'Soy responsable del trabajo que realizo', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (94, 'Mi superior me motiva a cumplir con mi trabajo de la manera que yo considere mejor.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (93, 'Considero que necesito capacitación en alguna área de mi interés y que forma parte importante de mi desarrollo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (142, 'Siento apoyo en mi jefe cuando me encuentro en dificultades', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (91, '¿Cree que su trabajo es compatible con los objetivos de la empresa?', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (90, 'Cuento con los materiales y equipos necesarios para realizar mi trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (89, 'Está conforme con la limpieza, higiene y salubridad en su lugar de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (88, 'Si  hay  un  nuevo Plan  Estratégico, estoy dispuesto a servir de voluntario para iniciar los cambios.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (87, 'En esta Institución, la gente planifica cuidadosamente antes de tomar acción.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (144, 'Mi jefe me da autonomía para tomar las decisiones necesarias para el cumplimiento de mis responsabilidades.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (86, 'Yo aporto al proceso de planificación en mi área de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (83, 'En mi organización está claramente definida su Misión y Visión.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (146, 'Mi jefe me proporciona información suficiente, adecuada para realizar bien mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (148, 'El nivel de recursos (materiales, equipos e infraestructura) con los que cuento para realizar bien mi trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (150, 'Mi remuneración, comparada con lo que otros ganan y hacen en la organización, está acorde con las responsabilidades de mi cargo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (152, '¿Cómo calificaría su nivel de satisfacción por pertenecer a la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (154, '¿Cómo calificaría su nivel de identificación con la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (102, 'Siento que formo parte de un equipo que trabaja hacia una meta común', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (101, 'En mi grupo de trabajo, solucionar el problema es más importante que encontrar algún culpable.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (100, 'Mis compañeros y yo trabajamos juntos de manera efectiva', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (99, 'El horario de trabajo me permite atender mis necesidades personales', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (98, 'Me siento comprometido para alcanzar las metas establecidas.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (156, 'En relación a las condiciones físicas de su puesto de trabajo (iluminación, temperatura, etc.)  usted considera que éste es:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (158, '¿Está usted de acuerdo en cómo está gestionado el departamento en el que trabaja respecto a las metas que éste tiene encomendadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (82, 'Me siento muy satisfecho con mi ambiente de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (160, 'Considera que su remuneración está por encima de la media en su entorno social, fuera de la empresa?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (92, 'Considero que me pagan lo justo por mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (97, 'Conozco las exigencias de mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (108, 'En su centro de trabajo mujeres y hombres tienen por igual oportunidades de ascenso y capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (110, 'En los últimos 12 meses usted ha participado  en programas de capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (112, 'En su centro de trabajo se ha despedido a alguna mujer por embarazo u orillado a renunciar al regresar de su licencia de maternidad.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (114, 'En su centro de trabajo mujeres y hombres tienen las mismas oportunidades para ocupar puestos de decisión.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (116, 'En los últimos 12 meses le han realizado una evaluación de desempeño.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (118, 'En su centro de trabajo quienes realizan tareas personales para las y los jefes logran privilegios.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (120, 'En su centro de trabajo las valoraciones que se realizan a sus actividades dependen más de la calidad y responsabilidad que de cualquier otra cuestión personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (122, 'En su centro de trabajo debido a sus características personales hay personas que sufren un trato inferior o de burla.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (125, 'En su centro de trabajo las y los superiores están abiertos a la comunicación con el personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (127, 'En su centro de trabajo las funciones y tareas se transmiten de manera clara y precisa.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (85, 'Existe un plan para lograr los  objetivos de la organización.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (84, 'La  dirección manifiesta sus objetivos de tal forma que se crea un sentido común de misión e identidad entre sus miembros.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (129, 'Si manifiesto mi preocupación sobre algún asunto relacionado con la igualdad de género o prácticas discriminatorias, se le da seguimiento', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (131, 'Para el desempeño de mis labores mi ambiente de trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (133, 'Existe comunicación fluida entre mi Región y la sede central.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (135, 'Los jefes en la organización se preocupan por mantener elevado el nivel de motivación del personal', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (139, 'En la organización las funciones están claramente definidas', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (205, 'preguntaejemplo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (106, 'Hay evidencia de que mi jefe me apoya utilizando mis ideas o propuestas para mejorar el trabajo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (105, 'Tengo mucho trabajo y poco tiempo para realizarlo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (104, 'Mi superior inmediato toma acciones que refuerzan el objetivo común de la Institución.', false, true, 5);


--
-- TOC entry 4689 (class 0 OID 26946)
-- Dependencies: 395
-- Data for Name: tsgrhpreguntaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4690 (class 0 OID 26950)
-- Dependencies: 396
-- Data for Name: tsgrhprocesos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Plan Capacitacion GN', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Plan Capacitacion GPR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Plan Capacitacion GR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Plan Capacitacion APE', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4691 (class 0 OID 26954)
-- Dependencies: 397
-- Data for Name: tsgrhproveedores; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4692 (class 0 OID 26958)
-- Dependencies: 398
-- Data for Name: tsgrhpuestos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (1, 'DISEÑO Y MANTENIMIENTO DE BASE DE DATOS', 1, 'DBA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (2, 'DESARROLLO BACKEND DE SOFTWARE COMERCIALIZABLE', 2, 'BACK');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (3, 'DISEÑO FRONTEND DE APLICATIVOS COMERCIALIZABLES', 3, 'FRONT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (4, 'SOLUCION DE PROBLEMAS CON EL APLICATIVO DESARROLLADO Y MANTENIMIENTO DE CODIGO FUENTE', 4, 'QA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (5, 'RESPONSABLE DE CAPACITACIÓN', 5, 'RC');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (6, 'RESPONSABLE DE RECURSOS HUMANOS Y AMBIENTE DE TRABAJO', 5, 'RRHAT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (7, 'RESPOSABLE DE GESTIÓN DE RECURSOS ', 4, 'rdg');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (9, 'Soporte', 1, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (8, 'Soporte', 6, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (11, 'Prueba', 4, 'stp');


--
-- TOC entry 4693 (class 0 OID 26962)
-- Dependencies: 399
-- Data for Name: tsgrhrelacionroles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4694 (class 0 OID 26965)
-- Dependencies: 400
-- Data for Name: tsgrhrespuestasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (475, 1, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (476, 2, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (477, 3, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (478, 4, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (479, 5, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (480, 1, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (481, 2, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (482, 3, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (483, 4, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (484, 5, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (485, 1, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (486, 2, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (487, 3, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (488, 4, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (489, 5, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (490, 1, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (491, 2, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (492, 3, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (493, 4, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (494, 5, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (495, 1, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (496, 2, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (497, 3, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (498, 4, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (499, 5, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (500, 1, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (501, 2, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (502, 3, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (503, 4, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (504, 5, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (505, 1, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (506, 2, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (507, 3, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (508, 4, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (509, 5, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (510, 1, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (511, 2, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (512, 3, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (513, 4, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (514, 5, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (515, 1, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (516, 2, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (517, 3, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (518, 4, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (519, 5, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (520, 1, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (521, 2, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (522, 3, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (523, 4, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (524, 5, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (525, 1, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (526, 2, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (527, 3, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (528, 4, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (529, 5, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (530, 1, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (531, 2, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (532, 3, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (533, 4, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (534, 5, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (535, 1, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (536, 2, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (537, 3, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (538, 4, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (539, 5, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (540, 1, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (541, 2, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (542, 3, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (543, 4, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (544, 5, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (545, 1, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (546, 2, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (547, 3, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (548, 4, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (549, 5, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (550, 1, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (551, 2, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (552, 3, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (553, 4, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (554, 5, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (555, 1, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (556, 2, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (557, 3, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (558, 4, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (559, 5, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (561, 1, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (562, 2, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (563, 3, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (564, 4, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (565, 5, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (566, 1, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (567, 2, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (568, 3, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (569, 4, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (570, 5, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (571, 1, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (572, 2, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (573, 3, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (574, 4, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (575, 5, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (576, 1, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (577, 2, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (578, 3, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (579, 4, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (580, 5, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (581, 1, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (582, 2, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (583, 3, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (584, 4, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (585, 5, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (586, 1, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (587, 2, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (588, 3, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (589, 4, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (590, 5, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (591, 1, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (592, 2, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (593, 3, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (594, 4, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (595, 5, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (596, 1, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (597, 2, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (598, 3, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (599, 4, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (600, 5, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (601, 1, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (602, 2, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (603, 3, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (604, 4, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (605, 5, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (606, 1, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (607, 2, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (608, 3, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (609, 4, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (610, 5, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (611, 1, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (612, 2, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (613, 3, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (614, 4, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (615, 5, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (616, 1, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (617, 2, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (618, 3, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (619, 4, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (620, 5, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (621, 1, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (622, 2, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (623, 3, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (624, 4, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (625, 5, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (626, 1, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (627, 2, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (628, 3, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (629, 4, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (630, 5, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (631, 1, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (632, 2, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (633, 3, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (634, 4, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (635, 5, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (637, 1, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (638, 2, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (639, 3, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (640, 4, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (641, 5, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (642, 1, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (643, 2, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (644, 3, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (645, 4, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (646, 5, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (647, 1, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (648, 2, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (649, 3, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (650, 4, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (651, 5, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (652, 1, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (653, 2, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (654, 3, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (655, 4, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (656, 5, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (657, 1, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (658, 2, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (659, 3, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (660, 4, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (661, 5, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (662, 1, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (663, 2, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (664, 3, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (665, 4, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (666, 5, 129, false);


--
-- TOC entry 4695 (class 0 OID 26969)
-- Dependencies: 401
-- Data for Name: tsgrhrespuestaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4696 (class 0 OID 26973)
-- Dependencies: 402
-- Data for Name: tsgrhrevplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4697 (class 0 OID 26982)
-- Dependencies: 403
-- Data for Name: tsgrhrolempleado; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4698 (class 0 OID 26986)
-- Dependencies: 404
-- Data for Name: tsgrhsubfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4699 (class 0 OID 26990)
-- Dependencies: 405
-- Data for Name: tsgrhtipocapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Curso', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Taller', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Seminario', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Autoestudio', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Mentoreo', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (6, 'Diplomado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (7, 'Congreso', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4700 (class 0 OID 26994)
-- Dependencies: 406
-- Data for Name: tsgrhvalidaevaluaciondes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4536 (class 0 OID 24541)
-- Dependencies: 242
-- Data for Name: tsgrtagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4537 (class 0 OID 24546)
-- Dependencies: 243
-- Data for Name: tsgrtarchivos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4538 (class 0 OID 24553)
-- Dependencies: 244
-- Data for Name: tsgrtasistentes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4539 (class 0 OID 24557)
-- Dependencies: 245
-- Data for Name: tsgrtattchticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4540 (class 0 OID 24564)
-- Dependencies: 246
-- Data for Name: tsgrtayudatopico; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4541 (class 0 OID 24568)
-- Dependencies: 247
-- Data for Name: tsgrtcategoriafaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4542 (class 0 OID 24575)
-- Dependencies: 248
-- Data for Name: tsgrtchat; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4543 (class 0 OID 24582)
-- Dependencies: 249
-- Data for Name: tsgrtciudades; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4544 (class 0 OID 24586)
-- Dependencies: 250
-- Data for Name: tsgrtcomentariosagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4545 (class 0 OID 24593)
-- Dependencies: 251
-- Data for Name: tsgrtcomentariosreunion; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4546 (class 0 OID 24600)
-- Dependencies: 252
-- Data for Name: tsgrtcompromisos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4547 (class 0 OID 24606)
-- Dependencies: 253
-- Data for Name: tsgrtcorreo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4548 (class 0 OID 24612)
-- Dependencies: 254
-- Data for Name: tsgrtdatossolicitud; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4549 (class 0 OID 24615)
-- Dependencies: 255
-- Data for Name: tsgrtdepartamento; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4550 (class 0 OID 24624)
-- Dependencies: 256
-- Data for Name: tsgrtedosolicitudes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4551 (class 0 OID 24627)
-- Dependencies: 257
-- Data for Name: tsgrtelementos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4552 (class 0 OID 24631)
-- Dependencies: 258
-- Data for Name: tsgrtestados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--

INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (1, 'PUEBLA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (2, 'TLAXCALA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (3, 'VERACRUZ');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (4, 'DISTRITO FEDERAL');


--
-- TOC entry 4553 (class 0 OID 24635)
-- Dependencies: 259
-- Data for Name: tsgrtfaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4554 (class 0 OID 24642)
-- Dependencies: 260
-- Data for Name: tsgrtgrupo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4555 (class 0 OID 24647)
-- Dependencies: 261
-- Data for Name: tsgrtinvitados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4556 (class 0 OID 24654)
-- Dependencies: 262
-- Data for Name: tsgrtlugares; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4557 (class 0 OID 24658)
-- Dependencies: 263
-- Data for Name: tsgrtmsjticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4558 (class 0 OID 24668)
-- Dependencies: 264
-- Data for Name: tsgrtnota; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4559 (class 0 OID 24676)
-- Dependencies: 265
-- Data for Name: tsgrtplantillacorreos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4560 (class 0 OID 24684)
-- Dependencies: 266
-- Data for Name: tsgrtprioridad; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4561 (class 0 OID 24688)
-- Dependencies: 267
-- Data for Name: tsgrtresppredefinida; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4562 (class 0 OID 24695)
-- Dependencies: 268
-- Data for Name: tsgrtrespuesta; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4563 (class 0 OID 24702)
-- Dependencies: 269
-- Data for Name: tsgrtreuniones; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4564 (class 0 OID 24709)
-- Dependencies: 270
-- Data for Name: tsgrtservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4565 (class 0 OID 24713)
-- Dependencies: 271
-- Data for Name: tsgrtsolicitudservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4566 (class 0 OID 24717)
-- Dependencies: 272
-- Data for Name: tsgrtticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4584 (class 0 OID 25229)
-- Dependencies: 290
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4585 (class 0 OID 25233)
-- Dependencies: 291
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4586 (class 0 OID 25237)
-- Dependencies: 292
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4587 (class 0 OID 25244)
-- Dependencies: 293
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4588 (class 0 OID 25251)
-- Dependencies: 294
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4589 (class 0 OID 25258)
-- Dependencies: 295
-- Data for Name: tsisatcursosycerticados; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4590 (class 0 OID 25262)
-- Dependencies: 296
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4591 (class 0 OID 25266)
-- Dependencies: 297
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4592 (class 0 OID 25273)
-- Dependencies: 298
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4593 (class 0 OID 25277)
-- Dependencies: 299
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4594 (class 0 OID 25284)
-- Dependencies: 300
-- Data for Name: tsisatfirmas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4595 (class 0 OID 25288)
-- Dependencies: 301
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4596 (class 0 OID 25325)
-- Dependencies: 302
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4597 (class 0 OID 25329)
-- Dependencies: 303
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4598 (class 0 OID 25336)
-- Dependencies: 304
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4599 (class 0 OID 25345)
-- Dependencies: 305
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4600 (class 0 OID 25349)
-- Dependencies: 306
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4874 (class 0 OID 0)
-- Dependencies: 205
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- TOC entry 4875 (class 0 OID 0)
-- Dependencies: 206
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- TOC entry 4876 (class 0 OID 0)
-- Dependencies: 207
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);


--
-- TOC entry 4877 (class 0 OID 0)
-- Dependencies: 333
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_area', 6, true);


--
-- TOC entry 4878 (class 0 OID 0)
-- Dependencies: 334
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- TOC entry 4879 (class 0 OID 0)
-- Dependencies: 335
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- TOC entry 4880 (class 0 OID 0)
-- Dependencies: 336
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 138, true);


--
-- TOC entry 4881 (class 0 OID 0)
-- Dependencies: 337
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 200, false);


--
-- TOC entry 4882 (class 0 OID 0)
-- Dependencies: 338
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 5, true);


--
-- TOC entry 4883 (class 0 OID 0)
-- Dependencies: 339
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- TOC entry 4884 (class 0 OID 0)
-- Dependencies: 340
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- TOC entry 4885 (class 0 OID 0)
-- Dependencies: 341
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 13, true);


--
-- TOC entry 4886 (class 0 OID 0)
-- Dependencies: 342
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 17, true);


--
-- TOC entry 4887 (class 0 OID 0)
-- Dependencies: 343
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- TOC entry 4888 (class 0 OID 0)
-- Dependencies: 344
-- Name: seq_estatus; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_estatus', 5, true);


--
-- TOC entry 4889 (class 0 OID 0)
-- Dependencies: 345
-- Name: seq_evacapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacapacitacion', 1, false);


--
-- TOC entry 4890 (class 0 OID 0)
-- Dependencies: 346
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- TOC entry 4891 (class 0 OID 0)
-- Dependencies: 347
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- TOC entry 4892 (class 0 OID 0)
-- Dependencies: 348
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- TOC entry 4893 (class 0 OID 0)
-- Dependencies: 349
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- TOC entry 4894 (class 0 OID 0)
-- Dependencies: 350
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 12, true);


--
-- TOC entry 4895 (class 0 OID 0)
-- Dependencies: 351
-- Name: seq_logistica; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_logistica', 1, false);


--
-- TOC entry 4896 (class 0 OID 0)
-- Dependencies: 352
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_lugar', 1, false);


--
-- TOC entry 4897 (class 0 OID 0)
-- Dependencies: 353
-- Name: seq_modo; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_modo', 2, true);


--
-- TOC entry 4898 (class 0 OID 0)
-- Dependencies: 354
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 8, true);


--
-- TOC entry 4899 (class 0 OID 0)
-- Dependencies: 355
-- Name: seq_plancapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_plancapacitacion', 1, false);


--
-- TOC entry 4900 (class 0 OID 0)
-- Dependencies: 356
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- TOC entry 4901 (class 0 OID 0)
-- Dependencies: 357
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 205, true);


--
-- TOC entry 4902 (class 0 OID 0)
-- Dependencies: 358
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- TOC entry 4903 (class 0 OID 0)
-- Dependencies: 359
-- Name: seq_proceso; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proceso', 4, true);


--
-- TOC entry 4904 (class 0 OID 0)
-- Dependencies: 360
-- Name: seq_proveedor; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proveedor', 1, false);


--
-- TOC entry 4905 (class 0 OID 0)
-- Dependencies: 361
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 11, true);


--
-- TOC entry 4906 (class 0 OID 0)
-- Dependencies: 362
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 500, false);


--
-- TOC entry 4907 (class 0 OID 0)
-- Dependencies: 363
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- TOC entry 4908 (class 0 OID 0)
-- Dependencies: 364
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- TOC entry 4909 (class 0 OID 0)
-- Dependencies: 365
-- Name: seq_rolempleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_rolempleado', 1, false);


--
-- TOC entry 4910 (class 0 OID 0)
-- Dependencies: 366
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- TOC entry 4911 (class 0 OID 0)
-- Dependencies: 367
-- Name: seq_tipocapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_tipocapacitacion', 7, true);


--
-- TOC entry 4912 (class 0 OID 0)
-- Dependencies: 368
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);


--
-- TOC entry 4913 (class 0 OID 0)
-- Dependencies: 211
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- TOC entry 4914 (class 0 OID 0)
-- Dependencies: 212
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- TOC entry 4915 (class 0 OID 0)
-- Dependencies: 213
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- TOC entry 4916 (class 0 OID 0)
-- Dependencies: 214
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- TOC entry 4917 (class 0 OID 0)
-- Dependencies: 215
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- TOC entry 4918 (class 0 OID 0)
-- Dependencies: 216
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- TOC entry 4919 (class 0 OID 0)
-- Dependencies: 217
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- TOC entry 4920 (class 0 OID 0)
-- Dependencies: 218
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- TOC entry 4921 (class 0 OID 0)
-- Dependencies: 219
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- TOC entry 4922 (class 0 OID 0)
-- Dependencies: 220
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- TOC entry 4923 (class 0 OID 0)
-- Dependencies: 221
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- TOC entry 4924 (class 0 OID 0)
-- Dependencies: 222
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- TOC entry 4925 (class 0 OID 0)
-- Dependencies: 223
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- TOC entry 4926 (class 0 OID 0)
-- Dependencies: 224
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- TOC entry 4927 (class 0 OID 0)
-- Dependencies: 225
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- TOC entry 4928 (class 0 OID 0)
-- Dependencies: 226
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- TOC entry 4929 (class 0 OID 0)
-- Dependencies: 227
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- TOC entry 4930 (class 0 OID 0)
-- Dependencies: 228
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- TOC entry 4931 (class 0 OID 0)
-- Dependencies: 229
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- TOC entry 4932 (class 0 OID 0)
-- Dependencies: 230
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- TOC entry 4933 (class 0 OID 0)
-- Dependencies: 231
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- TOC entry 4934 (class 0 OID 0)
-- Dependencies: 232
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- TOC entry 4935 (class 0 OID 0)
-- Dependencies: 233
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- TOC entry 4936 (class 0 OID 0)
-- Dependencies: 234
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- TOC entry 4937 (class 0 OID 0)
-- Dependencies: 235
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- TOC entry 4938 (class 0 OID 0)
-- Dependencies: 236
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- TOC entry 4939 (class 0 OID 0)
-- Dependencies: 407
-- Name: seq_respuestas_participantes; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuestas_participantes', 1, false);


--
-- TOC entry 4940 (class 0 OID 0)
-- Dependencies: 237
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- TOC entry 4941 (class 0 OID 0)
-- Dependencies: 238
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- TOC entry 4942 (class 0 OID 0)
-- Dependencies: 239
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- TOC entry 4943 (class 0 OID 0)
-- Dependencies: 240
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- TOC entry 4944 (class 0 OID 0)
-- Dependencies: 241
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);


--
-- TOC entry 4945 (class 0 OID 0)
-- Dependencies: 273
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 4946 (class 0 OID 0)
-- Dependencies: 274
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 4947 (class 0 OID 0)
-- Dependencies: 275
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 4948 (class 0 OID 0)
-- Dependencies: 276
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 1, false);


--
-- TOC entry 4949 (class 0 OID 0)
-- Dependencies: 277
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 4950 (class 0 OID 0)
-- Dependencies: 278
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 4951 (class 0 OID 0)
-- Dependencies: 279
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 4952 (class 0 OID 0)
-- Dependencies: 280
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 4953 (class 0 OID 0)
-- Dependencies: 281
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 4954 (class 0 OID 0)
-- Dependencies: 282
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 4955 (class 0 OID 0)
-- Dependencies: 283
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_firmas', 1, false);


--
-- TOC entry 4956 (class 0 OID 0)
-- Dependencies: 284
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 4957 (class 0 OID 0)
-- Dependencies: 285
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 4958 (class 0 OID 0)
-- Dependencies: 286
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 1, false);


--
-- TOC entry 4959 (class 0 OID 0)
-- Dependencies: 287
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 4960 (class 0 OID 0)
-- Dependencies: 288
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 4961 (class 0 OID 0)
-- Dependencies: 289
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 1, false);


--
-- TOC entry 3778 (class 2606 OID 24385)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3782 (class 2606 OID 24387)
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);


--
-- TOC entry 3786 (class 2606 OID 24389)
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);


--
-- TOC entry 3780 (class 2606 OID 24391)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3784 (class 2606 OID 24393)
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);


--
-- TOC entry 3788 (class 2606 OID 24395)
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);


--
-- TOC entry 3982 (class 2606 OID 26444)
-- Name: tsgnomcatincidencia cat_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcatincidencia
    ADD CONSTRAINT cat_incidencia_id PRIMARY KEY (cod_catincidenciaid);


--
-- TOC entry 3970 (class 2606 OID 26408)
-- Name: tsgnomalguinaldo nom_aguinaldo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_aguinaldo_id PRIMARY KEY (cod_aguinaldoid);


--
-- TOC entry 3974 (class 2606 OID 26424)
-- Name: tsgnombitacora nom_bitacora_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_bitacora_id PRIMARY KEY (cod_bitacoraid);


--
-- TOC entry 3978 (class 2606 OID 26434)
-- Name: tsgnomcabeceraht nom_cabecera_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cabecera_copia_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3976 (class 2606 OID 26429)
-- Name: tsgnomcabecera nom_cabecera_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cabecera_id PRIMARY KEY (cod_cabeceraid);


--
-- TOC entry 3972 (class 2606 OID 26416)
-- Name: tsgnomargumento nom_cat_argumento_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomargumento
    ADD CONSTRAINT nom_cat_argumento_id PRIMARY KEY (cod_argumentoid);


--
-- TOC entry 3990 (class 2606 OID 26470)
-- Name: tsgnomconcepto nom_cat_concepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_concepto_id PRIMARY KEY (cod_conceptoid);


--
-- TOC entry 3992 (class 2606 OID 26475)
-- Name: tsgnomconceptosat nom_cat_concepto_sat_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconceptosat
    ADD CONSTRAINT nom_cat_concepto_sat_id PRIMARY KEY (cod_conceptosatid);


--
-- TOC entry 3996 (class 2606 OID 26485)
-- Name: tsgnomejercicio nom_cat_ejercicio_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomejercicio
    ADD CONSTRAINT nom_cat_ejercicio_id PRIMARY KEY (cod_ejercicioid);


--
-- TOC entry 4004 (class 2606 OID 26508)
-- Name: tsgnomestatusnom nom_cat_estatus_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomestatusnom
    ADD CONSTRAINT nom_cat_estatus_nomina_id PRIMARY KEY (cod_estatusnomid);


--
-- TOC entry 4006 (class 2606 OID 26513)
-- Name: tsgnomformula nom_cat_formula_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomformula
    ADD CONSTRAINT nom_cat_formula_id PRIMARY KEY (cod_formulaid);


--
-- TOC entry 4008 (class 2606 OID 26518)
-- Name: tsgnomfuncion nom_cat_funcion_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomfuncion
    ADD CONSTRAINT nom_cat_funcion_id PRIMARY KEY (cod_funcionid);


--
-- TOC entry 4016 (class 2606 OID 26541)
-- Name: tsgnomquincena nom_cat_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_quincena_id PRIMARY KEY (cod_quincenaid);


--
-- TOC entry 4010 (class 2606 OID 26523)
-- Name: tsgnomhisttabla nom_cat_tabla_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomhisttabla
    ADD CONSTRAINT nom_cat_tabla_id PRIMARY KEY (cod_tablaid);


--
-- TOC entry 3980 (class 2606 OID 26439)
-- Name: tsgnomcalculo nom_cat_tipo_calculo_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcalculo
    ADD CONSTRAINT nom_cat_tipo_calculo_id PRIMARY KEY (cod_calculoid);


--
-- TOC entry 3984 (class 2606 OID 26449)
-- Name: tsgnomclasificador nom_cat_tipo_clasificador_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomclasificador
    ADD CONSTRAINT nom_cat_tipo_clasificador_id PRIMARY KEY (cod_clasificadorid);


--
-- TOC entry 4018 (class 2606 OID 26546)
-- Name: tsgnomtipoconcepto nom_cat_tipo_conepto_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtipoconcepto
    ADD CONSTRAINT nom_cat_tipo_conepto_id PRIMARY KEY (cod_tipoconceptoid);


--
-- TOC entry 4020 (class 2606 OID 26551)
-- Name: tsgnomtiponomina nom_cat_tipo_nomina_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomtiponomina
    ADD CONSTRAINT nom_cat_tipo_nomina_id PRIMARY KEY (cod_tiponominaid);


--
-- TOC entry 3988 (class 2606 OID 26465)
-- Name: tsgnomcncptoquincht nom_conceptos_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT nom_conceptos_quincena_copia_id PRIMARY KEY (cod_cncptoquinchtid);


--
-- TOC entry 3986 (class 2606 OID 26457)
-- Name: tsgnomcncptoquinc nom_conceptos_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT nom_conceptos_quincena_id PRIMARY KEY (cod_cncptoquincid);


--
-- TOC entry 3994 (class 2606 OID 26480)
-- Name: tsgnomconfpago nom_conf_pago_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_conf_pago_pkey PRIMARY KEY (cod_confpagoid);


--
-- TOC entry 3998 (class 2606 OID 26493)
-- Name: tsgnomempleados nom_empleado_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT nom_empleado_id PRIMARY KEY (cod_empleadoid);


--
-- TOC entry 4002 (class 2606 OID 26503)
-- Name: tsgnomempquincenaht nom_empleado_quincena_copia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_copia_id PRIMARY KEY (cod_empquincenahtid);


--
-- TOC entry 4000 (class 2606 OID 26498)
-- Name: tsgnomempquincena nom_empleado_quincena_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id PRIMARY KEY (cod_empquincenaid);


--
-- TOC entry 4012 (class 2606 OID 26531)
-- Name: tsgnomincidencia nom_incidencia_id; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_incidencia_id PRIMARY KEY (cod_incidenciaid);


--
-- TOC entry 4014 (class 2606 OID 26536)
-- Name: tsgnommanterceros nom_manuales_terceros_pkey; Type: CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_manuales_terceros_pkey PRIMARY KEY (cod_mantercerosid);


--
-- TOC entry 4030 (class 2606 OID 27005)
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);


--
-- TOC entry 4068 (class 2606 OID 27007)
-- Name: tsgrhmodo cod_capacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_capacitacion_pk PRIMARY KEY (cod_modo);


--
-- TOC entry 4048 (class 2606 OID 27009)
-- Name: tsgrhestatuscapacitacion cod_estatus_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_estatus_pk PRIMARY KEY (cod_estatus);


--
-- TOC entry 4066 (class 2606 OID 27011)
-- Name: tsgrhlugares cod_lugar_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_lugar_pk PRIMARY KEY (cod_lugar);


--
-- TOC entry 4080 (class 2606 OID 27013)
-- Name: tsgrhprocesos cod_procesos_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_procesos_pk PRIMARY KEY (cod_proceso);


--
-- TOC entry 4082 (class 2606 OID 27015)
-- Name: tsgrhproveedores cod_proveedor_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_proveedor_pk PRIMARY KEY (cod_proveedor);


--
-- TOC entry 4086 (class 2606 OID 27017)
-- Name: tsgrhrelacionroles cod_relacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_relacion_pk PRIMARY KEY (cod_plancapacitacion, cod_rolempleado);


--
-- TOC entry 4094 (class 2606 OID 27019)
-- Name: tsgrhrolempleado cod_rolempleado_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_rolempleado_pk PRIMARY KEY (cod_rolempleado);


--
-- TOC entry 4098 (class 2606 OID 27021)
-- Name: tsgrhtipocapacitacion cod_tipocapacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_tipocapacitacion_pk PRIMARY KEY (cod_tipocapacitacion);


--
-- TOC entry 4024 (class 2606 OID 27023)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4050 (class 2606 OID 27025)
-- Name: tsgrhevacapacitacion pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evacapacitacion);


--
-- TOC entry 4022 (class 2606 OID 27027)
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- TOC entry 4026 (class 2606 OID 27029)
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- TOC entry 4028 (class 2606 OID 27031)
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 4032 (class 2606 OID 27033)
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- TOC entry 4034 (class 2606 OID 27035)
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- TOC entry 4036 (class 2606 OID 27037)
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 4038 (class 2606 OID 27039)
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- TOC entry 4040 (class 2606 OID 27041)
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- TOC entry 4044 (class 2606 OID 27043)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_pkey PRIMARY KEY (cod_participantenc);


--
-- TOC entry 4042 (class 2606 OID 27045)
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- TOC entry 4046 (class 2606 OID 27047)
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 4052 (class 2606 OID 27049)
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- TOC entry 4054 (class 2606 OID 27051)
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- TOC entry 4056 (class 2606 OID 27053)
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 4058 (class 2606 OID 27055)
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- TOC entry 4060 (class 2606 OID 27057)
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 4070 (class 2606 OID 27059)
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- TOC entry 4072 (class 2606 OID 27061)
-- Name: tsgrhplancapacitacion tsgrhplancapacitacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (cod_plancapacitacion);


--
-- TOC entry 4074 (class 2606 OID 27063)
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- TOC entry 4076 (class 2606 OID 27065)
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 4078 (class 2606 OID 27067)
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 4084 (class 2606 OID 27069)
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- TOC entry 4062 (class 2606 OID 27071)
-- Name: tsgrhlogistica tsgrhreglogistica_cod_capacitacion_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_cod_capacitacion_key UNIQUE (cod_capacitacion);


--
-- TOC entry 4064 (class 2606 OID 27073)
-- Name: tsgrhlogistica tsgrhreglogistica_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_pkey PRIMARY KEY (cod_logistica);


--
-- TOC entry 4088 (class 2606 OID 27075)
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 4090 (class 2606 OID 27077)
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 4092 (class 2606 OID 27079)
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- TOC entry 4096 (class 2606 OID 27081)
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- TOC entry 4100 (class 2606 OID 27083)
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);


--
-- TOC entry 3790 (class 2606 OID 24732)
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- TOC entry 3794 (class 2606 OID 24734)
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- TOC entry 3800 (class 2606 OID 24736)
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- TOC entry 3804 (class 2606 OID 24738)
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- TOC entry 3865 (class 2606 OID 24740)
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3816 (class 2606 OID 24742)
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3824 (class 2606 OID 24744)
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- TOC entry 3837 (class 2606 OID 24746)
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- TOC entry 3843 (class 2606 OID 24748)
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- TOC entry 3847 (class 2606 OID 24750)
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- TOC entry 3924 (class 2606 OID 24752)
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- TOC entry 3853 (class 2606 OID 24754)
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- TOC entry 3857 (class 2606 OID 24756)
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- TOC entry 3926 (class 2606 OID 24758)
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- TOC entry 3861 (class 2606 OID 24760)
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- TOC entry 3867 (class 2606 OID 24762)
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- TOC entry 3871 (class 2606 OID 24764)
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- TOC entry 3875 (class 2606 OID 24766)
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- TOC entry 3880 (class 2606 OID 24768)
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- TOC entry 3884 (class 2606 OID 24770)
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- TOC entry 3888 (class 2606 OID 24772)
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- TOC entry 3892 (class 2606 OID 24774)
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- TOC entry 3896 (class 2606 OID 24776)
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- TOC entry 3908 (class 2606 OID 24778)
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- TOC entry 3902 (class 2606 OID 24780)
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- TOC entry 3796 (class 2606 OID 24782)
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- TOC entry 3912 (class 2606 OID 24784)
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- TOC entry 3916 (class 2606 OID 24786)
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- TOC entry 3920 (class 2606 OID 24788)
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- TOC entry 3928 (class 2606 OID 24790)
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3806 (class 2606 OID 24792)
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- TOC entry 3810 (class 2606 OID 24794)
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);


--
-- TOC entry 3792 (class 2606 OID 24812)
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- TOC entry 3798 (class 2606 OID 24814)
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- TOC entry 3802 (class 2606 OID 24816)
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- TOC entry 3808 (class 2606 OID 24818)
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- TOC entry 3812 (class 2606 OID 24798)
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);


--
-- TOC entry 3814 (class 2606 OID 24820)
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- TOC entry 3818 (class 2606 OID 24822)
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- TOC entry 3820 (class 2606 OID 24824)
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- TOC entry 3822 (class 2606 OID 24826)
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- TOC entry 3827 (class 2606 OID 24828)
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- TOC entry 3830 (class 2606 OID 24830)
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- TOC entry 3833 (class 2606 OID 24832)
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);


--
-- TOC entry 3839 (class 2606 OID 24796)
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3841 (class 2606 OID 24856)
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- TOC entry 3845 (class 2606 OID 24858)
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);


--
-- TOC entry 3849 (class 2606 OID 24800)
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3851 (class 2606 OID 24834)
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- TOC entry 3855 (class 2606 OID 24836)
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- TOC entry 3859 (class 2606 OID 24838)
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- TOC entry 3863 (class 2606 OID 24840)
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- TOC entry 3869 (class 2606 OID 24842)
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- TOC entry 3873 (class 2606 OID 24844)
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 3878 (class 2606 OID 24846)
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- TOC entry 3882 (class 2606 OID 24848)
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- TOC entry 3886 (class 2606 OID 24850)
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- TOC entry 3890 (class 2606 OID 24852)
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- TOC entry 3894 (class 2606 OID 24854)
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);


--
-- TOC entry 3898 (class 2606 OID 24802)
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3900 (class 2606 OID 24860)
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- TOC entry 3904 (class 2606 OID 24804)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);


--
-- TOC entry 3906 (class 2606 OID 24862)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3910 (class 2606 OID 24864)
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3914 (class 2606 OID 24866)
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);


--
-- TOC entry 3918 (class 2606 OID 24868)
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);


--
-- TOC entry 3922 (class 2606 OID 24870)
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);


--
-- TOC entry 3930 (class 2606 OID 24806)
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3932 (class 2606 OID 24808)
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);


--
-- TOC entry 3934 (class 2606 OID 24872)
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);


--
-- TOC entry 3835 (class 2606 OID 24810)
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);


--
-- TOC entry 3936 (class 2606 OID 25357)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3938 (class 2606 OID 25359)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 3940 (class 2606 OID 25361)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 3942 (class 2606 OID 25363)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3944 (class 2606 OID 25365)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 3946 (class 2606 OID 25367)
-- Name: tsisatcursosycerticados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcursosycerticados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 3948 (class 2606 OID 25369)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 3950 (class 2606 OID 25371)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 3952 (class 2606 OID 25373)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3954 (class 2606 OID 25375)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3956 (class 2606 OID 25377)
-- Name: tsisatfirmas tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 3958 (class 2606 OID 25379)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 3960 (class 2606 OID 25381)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3962 (class 2606 OID 25383)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 3964 (class 2606 OID 25385)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 3966 (class 2606 OID 25387)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 3968 (class 2606 OID 25389)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3831 (class 1259 OID 24873)
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- TOC entry 3876 (class 1259 OID 24874)
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- TOC entry 3828 (class 1259 OID 24875)
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- TOC entry 3825 (class 1259 OID 24876)
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


--
-- TOC entry 4377 (class 2620 OID 27084)
-- Name: tsgrhencuesta tg_actualizarfecha; Type: TRIGGER; Schema: sgrh; Owner: postgres
--

CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();


--
-- TOC entry 4101 (class 2606 OID 27085)
-- Name: tsgcousuarios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4249 (class 2606 OID 26602)
-- Name: tsgnomcncptoquincht concepto_quincena_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT concepto_quincena_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4247 (class 2606 OID 26592)
-- Name: tsgnomcncptoquinc concepto_quincena_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT concepto_quincena_id_fk_conceptos_quincena FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4250 (class 2606 OID 26607)
-- Name: tsgnomcncptoquincht empleado_concepto_id_copia_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquincht
    ADD CONSTRAINT empleado_concepto_id_copia_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4248 (class 2606 OID 26597)
-- Name: tsgnomcncptoquinc empleado_concepto_id_fk_conceptos_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcncptoquinc
    ADD CONSTRAINT empleado_concepto_id_fk_conceptos_quincena FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4263 (class 2606 OID 26642)
-- Name: tsgnomempquincena nom_cabecera_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4265 (class 2606 OID 26652)
-- Name: tsgnomempquincenaht nom_cabecera_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_cabecera_id_fk_empleados_quincena_copia FOREIGN KEY (cod_cabeceraid_fk) REFERENCES sgnom.tsgnomcabecera(cod_cabeceraid) ON UPDATE CASCADE;


--
-- TOC entry 4254 (class 2606 OID 26612)
-- Name: tsgnomconcepto nom_cat_clasificador_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_cat_clasificador_id_fk_cat_conceptos FOREIGN KEY (cod_clasificadorid_fk) REFERENCES sgnom.tsgnomclasificador(cod_clasificadorid) ON UPDATE CASCADE;


--
-- TOC entry 4275 (class 2606 OID 26682)
-- Name: tsgnommanterceros nom_cat_conceptos_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_conceptos_fk_manuales_terceros FOREIGN KEY (cod_conceptoid_fk) REFERENCES sgnom.tsgnomconcepto(cod_conceptoid);


--
-- TOC entry 4279 (class 2606 OID 26702)
-- Name: tsgnomquincena nom_cat_ejercicio_id_fk_cat_quincenas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomquincena
    ADD CONSTRAINT nom_cat_ejercicio_id_fk_cat_quincenas FOREIGN KEY (cod_ejercicioid_fk) REFERENCES sgnom.tsgnomejercicio(cod_ejercicioid) ON UPDATE CASCADE;


--
-- TOC entry 4244 (class 2606 OID 26577)
-- Name: tsgnomcabeceraht nom_cat_estatus_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_estatus_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4239 (class 2606 OID 26562)
-- Name: tsgnomcabecera nom_cat_estatus_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_estatus_nomina_id_fk_cabeceras FOREIGN KEY (cod_estatusnomid_fk) REFERENCES sgnom.tsgnomestatusnom(cod_estatusnomid) ON UPDATE CASCADE;


--
-- TOC entry 4269 (class 2606 OID 26662)
-- Name: tsgnomincidencia nom_cat_incidencia_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_incidencia_id_fk_incidencias FOREIGN KEY (cod_catincidenciaid_fk) REFERENCES sgnom.tsgnomcatincidencia(cod_catincidenciaid) ON UPDATE CASCADE;


--
-- TOC entry 4245 (class 2606 OID 26582)
-- Name: tsgnomcabeceraht nom_cat_quincena_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_quincena_id_copia_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4240 (class 2606 OID 26567)
-- Name: tsgnomcabecera nom_cat_quincena_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_quincena_id_fk_cabeceras FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4270 (class 2606 OID 26667)
-- Name: tsgnomincidencia nom_cat_quincena_id_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_cat_quincena_id_fk_incidencias FOREIGN KEY (cod_quincenaid_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4276 (class 2606 OID 26687)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_fin; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_fin FOREIGN KEY (cod_quincenafin_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid);


--
-- TOC entry 4277 (class 2606 OID 26692)
-- Name: tsgnommanterceros nom_cat_quincenas_fk_manuales_terceros_inicio; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_cat_quincenas_fk_manuales_terceros_inicio FOREIGN KEY (cod_quincenainicio_fk) REFERENCES sgnom.tsgnomquincena(cod_quincenaid);


--
-- TOC entry 4236 (class 2606 OID 26557)
-- Name: tsgnombitacora nom_cat_tabla_id_fk_nom_cat_tablas; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnombitacora
    ADD CONSTRAINT nom_cat_tabla_id_fk_nom_cat_tablas FOREIGN KEY (cod_tablaid_fk) REFERENCES sgnom.tsgnomhisttabla(cod_tablaid) ON UPDATE CASCADE;


--
-- TOC entry 4246 (class 2606 OID 26587)
-- Name: tsgnomcabeceraht nom_cat_tipo_nomina_id_copia_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT nom_cat_tipo_nomina_id_copia_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4241 (class 2606 OID 26572)
-- Name: tsgnomcabecera nom_cat_tipo_nomina_id_fk_cabeceras; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT nom_cat_tipo_nomina_id_fk_cabeceras FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4255 (class 2606 OID 26617)
-- Name: tsgnomconcepto nom_concepto_sat_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_concepto_sat_id_fk_cat_conceptos FOREIGN KEY (cod_conceptosatid_fk) REFERENCES sgnom.tsgnomconceptosat(cod_conceptosatid) ON UPDATE CASCADE;


--
-- TOC entry 4271 (class 2606 OID 26672)
-- Name: tsgnomincidencia nom_emp_autoriza_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_autoriza_fk_incidencias FOREIGN KEY (cod_empautoriza_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4272 (class 2606 OID 26677)
-- Name: tsgnomincidencia nom_emp_reporta_fk_incidencias; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT nom_emp_reporta_fk_incidencias FOREIGN KEY (cod_empreporta_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4264 (class 2606 OID 26647)
-- Name: tsgnomempquincena nom_empleado_quincena_id_fk_empleados_quincena; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincena
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4266 (class 2606 OID 26657)
-- Name: tsgnomempquincenaht nom_empleado_quincena_id_fk_empleados_quincena_copia; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempquincenaht
    ADD CONSTRAINT nom_empleado_quincena_id_fk_empleados_quincena_copia FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid) ON UPDATE CASCADE;


--
-- TOC entry 4278 (class 2606 OID 26697)
-- Name: tsgnommanterceros nom_empleados_fk_manuales_terceros; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT nom_empleados_fk_manuales_terceros FOREIGN KEY (cod_empleadoid_fk) REFERENCES sgnom.tsgnomempleados(cod_empleadoid);


--
-- TOC entry 4235 (class 2606 OID 26552)
-- Name: tsgnomalguinaldo nom_empleados_quincena_fk_aguinaldos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomalguinaldo
    ADD CONSTRAINT nom_empleados_quincena_fk_aguinaldos FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4259 (class 2606 OID 26637)
-- Name: tsgnomconfpago nom_empleados_quincena_fk_conf_pago; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconfpago
    ADD CONSTRAINT nom_empleados_quincena_fk_conf_pago FOREIGN KEY (cod_empquincenaid_fk) REFERENCES sgnom.tsgnomempquincena(cod_empquincenaid) ON UPDATE CASCADE;


--
-- TOC entry 4256 (class 2606 OID 26622)
-- Name: tsgnomconcepto nom_formula_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_formula_id_fk_cat_conceptos FOREIGN KEY (cod_formulaid_fk) REFERENCES sgnom.tsgnomformula(cod_formulaid) ON UPDATE CASCADE;


--
-- TOC entry 4257 (class 2606 OID 26627)
-- Name: tsgnomconcepto nom_tipo_calculo_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_calculo_id_fk_cat_conceptos FOREIGN KEY (cod_calculoid_fk) REFERENCES sgnom.tsgnomcalculo(cod_calculoid) ON UPDATE CASCADE;


--
-- TOC entry 4258 (class 2606 OID 26632)
-- Name: tsgnomconcepto nom_tipo_concepto_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_concepto_id_fk_cat_conceptos FOREIGN KEY (cod_tipoconceptoid_fk) REFERENCES sgnom.tsgnomtipoconcepto(cod_tipoconceptoid) ON UPDATE CASCADE;


--
-- TOC entry 4251 (class 2606 OID 28001)
-- Name: tsgnomconcepto nom_tipo_nomina_id_fk_cat_conceptos; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT nom_tipo_nomina_id_fk_cat_conceptos FOREIGN KEY (cod_tiponominaid_fk) REFERENCES sgnom.tsgnomtiponomina(cod_tiponominaid) ON UPDATE CASCADE;


--
-- TOC entry 4242 (class 2606 OID 27991)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4243 (class 2606 OID 27996)
-- Name: tsgnomcabeceraht tsgrh_empleados_id_copia_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabeceraht
    ADD CONSTRAINT tsgrh_empleados_id_copia_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4237 (class 2606 OID 27981)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4238 (class 2606 OID 27986)
-- Name: tsgnomcabecera tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomcabecera
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4260 (class 2606 OID 28026)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_cabeceras_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_cabeceras_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4252 (class 2606 OID 28006)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4253 (class 2606 OID 28011)
-- Name: tsgnomconcepto tsgrh_empleados_id_fk_cat_conceptos_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomconcepto
    ADD CONSTRAINT tsgrh_empleados_id_fk_cat_conceptos_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4261 (class 2606 OID 28016)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados FOREIGN KEY (cod_empleado_fk) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4262 (class 2606 OID 28021)
-- Name: tsgnomempleados tsgrh_empleados_id_fk_empleados_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomempleados
    ADD CONSTRAINT tsgrh_empleados_id_fk_empleados_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4268 (class 2606 OID 28036)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4267 (class 2606 OID 28031)
-- Name: tsgnomincidencia tsgrh_empleados_id_fk_incidencias_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnomincidencia
    ADD CONSTRAINT tsgrh_empleados_id_fk_incidencias_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4273 (class 2606 OID 28041)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_c; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_c FOREIGN KEY (aud_codcreadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4274 (class 2606 OID 28046)
-- Name: tsgnommanterceros tsgrh_empleados_id_fk_manuales_terceros_m; Type: FK CONSTRAINT; Schema: sgnom; Owner: suite
--

ALTER TABLE ONLY sgnom.tsgnommanterceros
    ADD CONSTRAINT tsgrh_empleados_id_fk_manuales_terceros_m FOREIGN KEY (aud_codmodificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4322 (class 2606 OID 27090)
-- Name: tsgrhlogistica cod_capacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_capacitacion_fk FOREIGN KEY (cod_capacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4327 (class 2606 OID 27095)
-- Name: tsgrhmodo cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4364 (class 2606 OID 27100)
-- Name: tsgrhrolempleado cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4342 (class 2606 OID 27105)
-- Name: tsgrhprocesos cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4344 (class 2606 OID 27110)
-- Name: tsgrhproveedores cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4325 (class 2606 OID 27115)
-- Name: tsgrhlugares cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4310 (class 2606 OID 27120)
-- Name: tsgrhestatuscapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4367 (class 2606 OID 27125)
-- Name: tsgrhtipocapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4329 (class 2606 OID 27130)
-- Name: tsgrhplancapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4323 (class 2606 OID 27135)
-- Name: tsgrhlogistica cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4347 (class 2606 OID 27140)
-- Name: tsgrhrelacionroles cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4312 (class 2606 OID 27145)
-- Name: tsgrhevacapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4328 (class 2606 OID 27150)
-- Name: tsgrhmodo cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4365 (class 2606 OID 27155)
-- Name: tsgrhrolempleado cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4343 (class 2606 OID 27160)
-- Name: tsgrhprocesos cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4345 (class 2606 OID 27165)
-- Name: tsgrhproveedores cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4326 (class 2606 OID 27170)
-- Name: tsgrhlugares cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4311 (class 2606 OID 27175)
-- Name: tsgrhestatuscapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4368 (class 2606 OID 27180)
-- Name: tsgrhtipocapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4330 (class 2606 OID 27185)
-- Name: tsgrhplancapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4324 (class 2606 OID 27190)
-- Name: tsgrhlogistica cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4348 (class 2606 OID 27195)
-- Name: tsgrhrelacionroles cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4313 (class 2606 OID 27200)
-- Name: tsgrhevacapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4349 (class 2606 OID 27205)
-- Name: tsgrhrelacionroles cod_plancapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_plancapacitacion_fk FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4350 (class 2606 OID 27210)
-- Name: tsgrhrelacionroles cod_rolempleado_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_rolempleado_fk FOREIGN KEY (cod_rolempleado) REFERENCES sgrh.tsgrhrolempleado(cod_rolempleado);


--
-- TOC entry 4285 (class 2606 OID 27215)
-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4298 (class 2606 OID 27220)
-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4346 (class 2606 OID 27225)
-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4351 (class 2606 OID 27230)
-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


--
-- TOC entry 4286 (class 2606 OID 27235)
-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4299 (class 2606 OID 27240)
-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4296 (class 2606 OID 27245)
-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4294 (class 2606 OID 27250)
-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4303 (class 2606 OID 27255)
-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4319 (class 2606 OID 27260)
-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4337 (class 2606 OID 27265)
-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4355 (class 2606 OID 27270)
-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4287 (class 2606 OID 27275)
-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4321 (class 2606 OID 27280)
-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4309 (class 2606 OID 27285)
-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4284 (class 2606 OID 27290)
-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4288 (class 2606 OID 27295)
-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4339 (class 2606 OID 27300)
-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4353 (class 2606 OID 27305)
-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4340 (class 2606 OID 27310)
-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4316 (class 2606 OID 27315)
-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4369 (class 2606 OID 27320)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4317 (class 2606 OID 27325)
-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4370 (class 2606 OID 27330)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4318 (class 2606 OID 27335)
-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4371 (class 2606 OID 27340)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4366 (class 2606 OID 27345)
-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4289 (class 2606 OID 27350)
-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4372 (class 2606 OID 27360)
-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4356 (class 2606 OID 27365)
-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4300 (class 2606 OID 27370)
-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4297 (class 2606 OID 27375)
-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4295 (class 2606 OID 27380)
-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4304 (class 2606 OID 27385)
-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4320 (class 2606 OID 27390)
-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4338 (class 2606 OID 27395)
-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4357 (class 2606 OID 27400)
-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4290 (class 2606 OID 27405)
-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4373 (class 2606 OID 27410)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4358 (class 2606 OID 27415)
-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4374 (class 2606 OID 27420)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4359 (class 2606 OID 27425)
-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4375 (class 2606 OID 27430)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4360 (class 2606 OID 27435)
-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4376 (class 2606 OID 27440)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4361 (class 2606 OID 27445)
-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4362 (class 2606 OID 27450)
-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4291 (class 2606 OID 27455)
-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4363 (class 2606 OID 27460)
-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4352 (class 2606 OID 27465)
-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4354 (class 2606 OID 27470)
-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4301 (class 2606 OID 27475)
-- Name: tsgrhempleados fk_cod_puesto; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4292 (class 2606 OID 27480)
-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4293 (class 2606 OID 27485)
-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4280 (class 2606 OID 27490)
-- Name: tsgrhareas fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4302 (class 2606 OID 27495)
-- Name: tsgrhempleados fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4341 (class 2606 OID 27500)
-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4281 (class 2606 OID 27505)
-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4314 (class 2606 OID 27510)
-- Name: tsgrhevacapacitacion fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4282 (class 2606 OID 27515)
-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4283 (class 2606 OID 27520)
-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4315 (class 2606 OID 27525)
-- Name: tsgrhevacapacitacion fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4331 (class 2606 OID 27530)
-- Name: tsgrhplancapacitacion plancap_tipocapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancap_tipocapacitacion_fk FOREIGN KEY (cod_tipocapacitacion) REFERENCES sgrh.tsgrhtipocapacitacion(cod_tipocapacitacion);


--
-- TOC entry 4332 (class 2606 OID 27535)
-- Name: tsgrhplancapacitacion plancapacitacion_estatus_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_estatus_fk FOREIGN KEY (cod_estatus) REFERENCES sgrh.tsgrhestatuscapacitacion(cod_estatus);


--
-- TOC entry 4333 (class 2606 OID 27540)
-- Name: tsgrhplancapacitacion plancapacitacion_lugar_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_lugar_fk FOREIGN KEY (cod_lugar) REFERENCES sgrh.tsgrhlugares(cod_lugar);


--
-- TOC entry 4334 (class 2606 OID 27545)
-- Name: tsgrhplancapacitacion plancapacitacion_modo_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_modo_fk FOREIGN KEY (cod_modo) REFERENCES sgrh.tsgrhmodo(cod_modo);


--
-- TOC entry 4335 (class 2606 OID 27550)
-- Name: tsgrhplancapacitacion plancapacitacion_proceso_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proceso_fk FOREIGN KEY (cod_proceso) REFERENCES sgrh.tsgrhprocesos(cod_proceso);


--
-- TOC entry 4336 (class 2606 OID 27555)
-- Name: tsgrhplancapacitacion plancapacitacion_proveedor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proveedor_fk FOREIGN KEY (cod_proveedor) REFERENCES sgrh.tsgrhproveedores(cod_proveedor);


--
-- TOC entry 4305 (class 2606 OID 27560)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_empleado_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_empleado_fkey FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4306 (class 2606 OID 27565)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_encuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_encuesta_fkey FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta);


--
-- TOC entry 4307 (class 2606 OID 27570)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_pregunta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_pregunta_fkey FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta);


--
-- TOC entry 4308 (class 2606 OID 27575)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_respuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_respuesta_fkey FOREIGN KEY (cod_respuesta) REFERENCES sgrh.tsgrhrespuestasenc(cod_respuesta);


--
-- TOC entry 4117 (class 2606 OID 25720)
-- Name: tsgrtcomentariosagenda fk_cod_agenda; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4136 (class 2606 OID 25725)
-- Name: tsgrtfaq fk_cod_categoria; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4121 (class 2606 OID 25730)
-- Name: tsgrtcompromisos fk_cod_chat; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);


--
-- TOC entry 4144 (class 2606 OID 25735)
-- Name: tsgrtlugares fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4131 (class 2606 OID 25740)
-- Name: tsgrtdepartamento fk_cod_correo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4115 (class 2606 OID 27580)
-- Name: tsgrtcategoriafaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4126 (class 2606 OID 27585)
-- Name: tsgrtcorreo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4134 (class 2606 OID 27590)
-- Name: tsgrtdepartamento fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4137 (class 2606 OID 27595)
-- Name: tsgrtfaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4140 (class 2606 OID 27600)
-- Name: tsgrtgrupo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4113 (class 2606 OID 27605)
-- Name: tsgrtayudatopico fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4151 (class 2606 OID 27610)
-- Name: tsgrtnota fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4155 (class 2606 OID 27615)
-- Name: tsgrtplantillacorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4158 (class 2606 OID 27620)
-- Name: tsgrtprioridad fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4167 (class 2606 OID 27625)
-- Name: tsgrtrespuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4162 (class 2606 OID 27630)
-- Name: tsgrtresppredefinida fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4181 (class 2606 OID 27635)
-- Name: tsgrtticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4108 (class 2606 OID 27640)
-- Name: tsgrtattchticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4147 (class 2606 OID 27645)
-- Name: tsgrtmsjticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4172 (class 2606 OID 27650)
-- Name: tsgrtreuniones fk_cod_creadorreunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4123 (class 2606 OID 25820)
-- Name: tsgrtcorreo fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4110 (class 2606 OID 25825)
-- Name: tsgrtayudatopico fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4160 (class 2606 OID 25830)
-- Name: tsgrtresppredefinida fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4177 (class 2606 OID 25835)
-- Name: tsgrtticket fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4128 (class 2606 OID 25840)
-- Name: tsgrtdatossolicitud fk_cod_edosolicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4129 (class 2606 OID 25845)
-- Name: tsgrtdatossolicitud fk_cod_elemento; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4105 (class 2606 OID 27655)
-- Name: tsgrtasistentes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4152 (class 2606 OID 27660)
-- Name: tsgrtnota fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4168 (class 2606 OID 27665)
-- Name: tsgrtrespuesta fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4176 (class 2606 OID 27670)
-- Name: tsgrtsolicitudservicios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4182 (class 2606 OID 27675)
-- Name: tsgrtticket fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4143 (class 2606 OID 27680)
-- Name: tsgrtinvitados fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4104 (class 2606 OID 25880)
-- Name: tsgrtasistentes fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4119 (class 2606 OID 25885)
-- Name: tsgrtcomentariosreunion fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4118 (class 2606 OID 25890)
-- Name: tsgrtcomentariosagenda fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4170 (class 2606 OID 25895)
-- Name: tsgrtreuniones fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4164 (class 2606 OID 25900)
-- Name: tsgrtrespuesta fk_cod_mensaje; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4132 (class 2606 OID 25905)
-- Name: tsgrtdepartamento fk_cod_plantillacorreo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4111 (class 2606 OID 25910)
-- Name: tsgrtayudatopico fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4178 (class 2606 OID 25915)
-- Name: tsgrtticket fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4173 (class 2606 OID 27685)
-- Name: tsgrtreuniones fk_cod_responsable; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4122 (class 2606 OID 25925)
-- Name: tsgrtcompromisos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4103 (class 2606 OID 25930)
-- Name: tsgrtarchivos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4142 (class 2606 OID 25935)
-- Name: tsgrtinvitados fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4120 (class 2606 OID 25940)
-- Name: tsgrtcomentariosreunion fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4102 (class 2606 OID 25945)
-- Name: tsgrtagenda fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4171 (class 2606 OID 25950)
-- Name: tsgrtreuniones fk_cod_reunionanterior; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4174 (class 2606 OID 25955)
-- Name: tsgrtsolicitudservicios fk_cod_servicio; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4124 (class 2606 OID 25960)
-- Name: tsgrtcorreo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4133 (class 2606 OID 25965)
-- Name: tsgrtdepartamento fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4139 (class 2606 OID 25970)
-- Name: tsgrtgrupo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4112 (class 2606 OID 25975)
-- Name: tsgrtayudatopico fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4149 (class 2606 OID 25980)
-- Name: tsgrtnota fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4154 (class 2606 OID 25985)
-- Name: tsgrtplantillacorreos fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4157 (class 2606 OID 25990)
-- Name: tsgrtprioridad fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4165 (class 2606 OID 25995)
-- Name: tsgrtrespuesta fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4161 (class 2606 OID 26000)
-- Name: tsgrtresppredefinida fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4179 (class 2606 OID 26005)
-- Name: tsgrtticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4106 (class 2606 OID 26010)
-- Name: tsgrtattchticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4145 (class 2606 OID 26015)
-- Name: tsgrtmsjticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4130 (class 2606 OID 26020)
-- Name: tsgrtdatossolicitud fk_cod_solicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4150 (class 2606 OID 26025)
-- Name: tsgrtnota fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4166 (class 2606 OID 26030)
-- Name: tsgrtrespuesta fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4175 (class 2606 OID 26035)
-- Name: tsgrtsolicitudservicios fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4107 (class 2606 OID 26040)
-- Name: tsgrtattchticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4146 (class 2606 OID 26045)
-- Name: tsgrtmsjticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4180 (class 2606 OID 26050)
-- Name: tsgrtticket fk_cod_topico; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4127 (class 2606 OID 27690)
-- Name: tsgrtcorreo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4135 (class 2606 OID 27695)
-- Name: tsgrtdepartamento fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4138 (class 2606 OID 27700)
-- Name: tsgrtfaq fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4141 (class 2606 OID 27705)
-- Name: tsgrtgrupo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4114 (class 2606 OID 27710)
-- Name: tsgrtayudatopico fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4153 (class 2606 OID 27715)
-- Name: tsgrtnota fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4156 (class 2606 OID 27720)
-- Name: tsgrtplantillacorreos fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4159 (class 2606 OID 27725)
-- Name: tsgrtprioridad fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4169 (class 2606 OID 27730)
-- Name: tsgrtrespuesta fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4163 (class 2606 OID 27735)
-- Name: tsgrtresppredefinida fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4183 (class 2606 OID 27740)
-- Name: tsgrtticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4109 (class 2606 OID 27745)
-- Name: tsgrtattchticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4148 (class 2606 OID 27750)
-- Name: tsgrtmsjticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4125 (class 2606 OID 26120)
-- Name: tsgrtcorreo fk_cod_usuario; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_usuario FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4116 (class 2606 OID 26125)
-- Name: tsgrtciudades fk_estadorep; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4226 (class 2606 OID 27755)
-- Name: tsisatprospectos fk_cod_administrador; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_administrador FOREIGN KEY (cod_administrador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4193 (class 2606 OID 27760)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4211 (class 2606 OID 27765)
-- Name: tsisatfirmas fk_cod_autoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_autoriza FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4219 (class 2606 OID 26145)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4194 (class 2606 OID 27770)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4221 (class 2606 OID 27775)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4185 (class 2606 OID 27780)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4212 (class 2606 OID 27785)
-- Name: tsisatfirmas fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sgrh.tsgrhcontrataciones(cod_contratacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4233 (class 2606 OID 27790)
-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4190 (class 2606 OID 27795)
-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4195 (class 2606 OID 27800)
-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4213 (class 2606 OID 27805)
-- Name: tsisatfirmas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4201 (class 2606 OID 27810)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4222 (class 2606 OID 27815)
-- Name: tsisatordenservicio fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4207 (class 2606 OID 27820)
-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4186 (class 2606 OID 27825)
-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4205 (class 2606 OID 27830)
-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4227 (class 2606 OID 27835)
-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4230 (class 2606 OID 27840)
-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4220 (class 2606 OID 26225)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4196 (class 2606 OID 27845)
-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4223 (class 2606 OID 27850)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4234 (class 2606 OID 27855)
-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4191 (class 2606 OID 27860)
-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4197 (class 2606 OID 27865)
-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4214 (class 2606 OID 27870)
-- Name: tsisatfirmas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4202 (class 2606 OID 27875)
-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4224 (class 2606 OID 27880)
-- Name: tsisatordenservicio fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4208 (class 2606 OID 27885)
-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4187 (class 2606 OID 27890)
-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4206 (class 2606 OID 27895)
-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4228 (class 2606 OID 27900)
-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4231 (class 2606 OID 27905)
-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4198 (class 2606 OID 27910)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4189 (class 2606 OID 27915)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4188 (class 2606 OID 27920)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4232 (class 2606 OID 27925)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4192 (class 2606 OID 26315)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4184 (class 2606 OID 26320)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4229 (class 2606 OID 26325)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4210 (class 2606 OID 26330)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4209 (class 2606 OID 26335)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4204 (class 2606 OID 26340)
-- Name: tsisatcursosycerticados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcursosycerticados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4218 (class 2606 OID 26345)
-- Name: tsisatidiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4203 (class 2606 OID 27930)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4225 (class 2606 OID 27935)
-- Name: tsisatordenservicio fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4215 (class 2606 OID 27940)
-- Name: tsisatfirmas fk_cod_puestoautoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_puestoautoriza FOREIGN KEY (cod_puestoautoriza) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4216 (class 2606 OID 27945)
-- Name: tsisatfirmas fk_cod_puestosolicita; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_puestosolicita FOREIGN KEY (cod_puestosolicita) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4199 (class 2606 OID 27950)
-- Name: tsisatcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4200 (class 2606 OID 27955)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4217 (class 2606 OID 27960)
-- Name: tsisatfirmas fk_cod_solicita; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_solicita FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4709 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA sgco; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA sgco FROM postgres;
GRANT ALL ON SCHEMA sgco TO postgres WITH GRANT OPTION;


--
-- TOC entry 4711 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA sgnom; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA sgnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4715 (class 0 OID 0)
-- Dependencies: 1159
-- Name: TYPE edo_encuesta; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TYPE sgrh.edo_encuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4716 (class 0 OID 0)
-- Dependencies: 436
-- Name: FUNCTION buscar_detalle_empleados(); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.buscar_detalle_empleados() TO suite WITH GRANT OPTION;


--
-- TOC entry 4717 (class 0 OID 0)
-- Dependencies: 419
-- Name: FUNCTION prueba(); Type: ACL; Schema: sgnom; Owner: postgres
--

GRANT ALL ON FUNCTION sgnom.prueba() TO suite WITH GRANT OPTION;


--
-- TOC entry 4718 (class 0 OID 0)
-- Dependencies: 437
-- Name: FUNCTION crosstab_report_encuesta(integer); Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON FUNCTION sgrh.crosstab_report_encuesta(integer) TO suite WITH GRANT OPTION;


--
-- TOC entry 4719 (class 0 OID 0)
-- Dependencies: 438
-- Name: FUNCTION factualizarfecha(); Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON FUNCTION sgrh.factualizarfecha() TO suite WITH GRANT OPTION;


--
-- TOC entry 4720 (class 0 OID 0)
-- Dependencies: 205
-- Name: SEQUENCE seq_sistema; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_sistema FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_sistema TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_sistema TO postgres WITH GRANT OPTION;


--
-- TOC entry 4721 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEQUENCE seq_tipousuario; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_tipousuario FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_tipousuario TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_tipousuario TO postgres WITH GRANT OPTION;


--
-- TOC entry 4722 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEQUENCE seq_usuarios; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON SEQUENCE sgco.seq_usuarios FROM postgres;
GRANT UPDATE ON SEQUENCE sgco.seq_usuarios TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_usuarios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4723 (class 0 OID 0)
-- Dependencies: 208
-- Name: TABLE tsgcosistemas; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcosistemas FROM postgres;
GRANT ALL ON TABLE sgco.tsgcosistemas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4724 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE tsgcotipousuario; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcotipousuario FROM postgres;
GRANT ALL ON TABLE sgco.tsgcotipousuario TO postgres WITH GRANT OPTION;


--
-- TOC entry 4725 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE tsgcousuarios; Type: ACL; Schema: sgco; Owner: postgres
--

REVOKE ALL ON TABLE sgco.tsgcousuarios FROM postgres;
GRANT ALL ON TABLE sgco.tsgcousuarios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4727 (class 0 OID 0)
-- Dependencies: 307
-- Name: TABLE tsgnomalguinaldo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomalguinaldo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomalguinaldo TO suite WITH GRANT OPTION;


--
-- TOC entry 4728 (class 0 OID 0)
-- Dependencies: 308
-- Name: TABLE tsgnomargumento; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomargumento FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomargumento TO suite WITH GRANT OPTION;


--
-- TOC entry 4729 (class 0 OID 0)
-- Dependencies: 309
-- Name: TABLE tsgnombitacora; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnombitacora FROM suite;
GRANT ALL ON TABLE sgnom.tsgnombitacora TO suite WITH GRANT OPTION;


--
-- TOC entry 4730 (class 0 OID 0)
-- Dependencies: 310
-- Name: TABLE tsgnomcabecera; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabecera FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabecera TO suite WITH GRANT OPTION;


--
-- TOC entry 4731 (class 0 OID 0)
-- Dependencies: 311
-- Name: TABLE tsgnomcabeceraht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcabeceraht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcabeceraht TO suite WITH GRANT OPTION;


--
-- TOC entry 4733 (class 0 OID 0)
-- Dependencies: 312
-- Name: TABLE tsgnomcalculo; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcalculo FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcalculo TO suite WITH GRANT OPTION;


--
-- TOC entry 4735 (class 0 OID 0)
-- Dependencies: 313
-- Name: TABLE tsgnomcatincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcatincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcatincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4736 (class 0 OID 0)
-- Dependencies: 314
-- Name: TABLE tsgnomclasificador; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomclasificador FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomclasificador TO suite WITH GRANT OPTION;


--
-- TOC entry 4737 (class 0 OID 0)
-- Dependencies: 315
-- Name: TABLE tsgnomcncptoquinc; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquinc FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquinc TO suite WITH GRANT OPTION;


--
-- TOC entry 4738 (class 0 OID 0)
-- Dependencies: 316
-- Name: TABLE tsgnomcncptoquincht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomcncptoquincht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomcncptoquincht TO suite WITH GRANT OPTION;


--
-- TOC entry 4740 (class 0 OID 0)
-- Dependencies: 317
-- Name: TABLE tsgnomconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4742 (class 0 OID 0)
-- Dependencies: 318
-- Name: TABLE tsgnomconceptosat; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconceptosat FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconceptosat TO suite WITH GRANT OPTION;


--
-- TOC entry 4744 (class 0 OID 0)
-- Dependencies: 319
-- Name: TABLE tsgnomconfpago; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomconfpago FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomconfpago TO suite WITH GRANT OPTION;


--
-- TOC entry 4745 (class 0 OID 0)
-- Dependencies: 320
-- Name: TABLE tsgnomejercicio; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomejercicio FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomejercicio TO suite WITH GRANT OPTION;


--
-- TOC entry 4747 (class 0 OID 0)
-- Dependencies: 321
-- Name: TABLE tsgnomempleados; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempleados FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 4749 (class 0 OID 0)
-- Dependencies: 322
-- Name: TABLE tsgnomempquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4750 (class 0 OID 0)
-- Dependencies: 323
-- Name: TABLE tsgnomempquincenaht; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomempquincenaht FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomempquincenaht TO suite WITH GRANT OPTION;


--
-- TOC entry 4752 (class 0 OID 0)
-- Dependencies: 324
-- Name: TABLE tsgnomestatusnom; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomestatusnom FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomestatusnom TO suite WITH GRANT OPTION;


--
-- TOC entry 4754 (class 0 OID 0)
-- Dependencies: 325
-- Name: TABLE tsgnomformula; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomformula FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomformula TO suite WITH GRANT OPTION;


--
-- TOC entry 4755 (class 0 OID 0)
-- Dependencies: 326
-- Name: TABLE tsgnomfuncion; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomfuncion FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomfuncion TO suite WITH GRANT OPTION;


--
-- TOC entry 4756 (class 0 OID 0)
-- Dependencies: 327
-- Name: TABLE tsgnomhisttabla; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomhisttabla FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomhisttabla TO suite WITH GRANT OPTION;


--
-- TOC entry 4758 (class 0 OID 0)
-- Dependencies: 328
-- Name: TABLE tsgnomincidencia; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomincidencia FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomincidencia TO suite WITH GRANT OPTION;


--
-- TOC entry 4759 (class 0 OID 0)
-- Dependencies: 329
-- Name: TABLE tsgnommanterceros; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnommanterceros FROM suite;
GRANT ALL ON TABLE sgnom.tsgnommanterceros TO suite WITH GRANT OPTION;


--
-- TOC entry 4760 (class 0 OID 0)
-- Dependencies: 330
-- Name: TABLE tsgnomquincena; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomquincena FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomquincena TO suite WITH GRANT OPTION;


--
-- TOC entry 4762 (class 0 OID 0)
-- Dependencies: 331
-- Name: TABLE tsgnomtipoconcepto; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtipoconcepto FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtipoconcepto TO suite WITH GRANT OPTION;


--
-- TOC entry 4764 (class 0 OID 0)
-- Dependencies: 332
-- Name: TABLE tsgnomtiponomina; Type: ACL; Schema: sgnom; Owner: suite
--

REVOKE ALL ON TABLE sgnom.tsgnomtiponomina FROM suite;
GRANT ALL ON TABLE sgnom.tsgnomtiponomina TO suite WITH GRANT OPTION;


--
-- TOC entry 4765 (class 0 OID 0)
-- Dependencies: 333
-- Name: SEQUENCE seq_area; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_area TO suite WITH GRANT OPTION;


--
-- TOC entry 4766 (class 0 OID 0)
-- Dependencies: 334
-- Name: SEQUENCE seq_capacitaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_capacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4767 (class 0 OID 0)
-- Dependencies: 335
-- Name: SEQUENCE seq_cartaasignacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4768 (class 0 OID 0)
-- Dependencies: 336
-- Name: SEQUENCE seq_cat_encuesta_participantes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cat_encuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4769 (class 0 OID 0)
-- Dependencies: 337
-- Name: SEQUENCE seq_catrespuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_catrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4770 (class 0 OID 0)
-- Dependencies: 338
-- Name: SEQUENCE seq_clientes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_clientes TO suite WITH GRANT OPTION;


--
-- TOC entry 4771 (class 0 OID 0)
-- Dependencies: 339
-- Name: SEQUENCE seq_contrataciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4772 (class 0 OID 0)
-- Dependencies: 340
-- Name: SEQUENCE seq_contratos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_contratos TO suite WITH GRANT OPTION;


--
-- TOC entry 4773 (class 0 OID 0)
-- Dependencies: 341
-- Name: SEQUENCE seq_empleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_empleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4774 (class 0 OID 0)
-- Dependencies: 342
-- Name: SEQUENCE seq_encuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_encuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4775 (class 0 OID 0)
-- Dependencies: 343
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_escolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4776 (class 0 OID 0)
-- Dependencies: 344
-- Name: SEQUENCE seq_estatus; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_estatus TO suite WITH GRANT OPTION;


--
-- TOC entry 4777 (class 0 OID 0)
-- Dependencies: 345
-- Name: SEQUENCE seq_evacapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4778 (class 0 OID 0)
-- Dependencies: 346
-- Name: SEQUENCE seq_evacontestadas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 4779 (class 0 OID 0)
-- Dependencies: 347
-- Name: SEQUENCE seq_evaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_evaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4780 (class 0 OID 0)
-- Dependencies: 348
-- Name: SEQUENCE seq_experiencialab; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_experiencialab TO suite WITH GRANT OPTION;


--
-- TOC entry 4781 (class 0 OID 0)
-- Dependencies: 349
-- Name: SEQUENCE seq_factoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_factoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4782 (class 0 OID 0)
-- Dependencies: 350
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_idiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4783 (class 0 OID 0)
-- Dependencies: 351
-- Name: SEQUENCE seq_logistica; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_logistica TO suite WITH GRANT OPTION;


--
-- TOC entry 4784 (class 0 OID 0)
-- Dependencies: 352
-- Name: SEQUENCE seq_lugar; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_lugar TO suite WITH GRANT OPTION;


--
-- TOC entry 4785 (class 0 OID 0)
-- Dependencies: 353
-- Name: SEQUENCE seq_modo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_modo TO suite WITH GRANT OPTION;


--
-- TOC entry 4786 (class 0 OID 0)
-- Dependencies: 354
-- Name: SEQUENCE seq_perfiles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_perfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 4787 (class 0 OID 0)
-- Dependencies: 355
-- Name: SEQUENCE seq_plancapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_plancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4788 (class 0 OID 0)
-- Dependencies: 356
-- Name: SEQUENCE seq_planesoperativos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_planesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 4789 (class 0 OID 0)
-- Dependencies: 357
-- Name: SEQUENCE seq_preguntasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4790 (class 0 OID 0)
-- Dependencies: 358
-- Name: SEQUENCE seq_preguntaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_preguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4791 (class 0 OID 0)
-- Dependencies: 359
-- Name: SEQUENCE seq_proceso; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proceso TO suite WITH GRANT OPTION;


--
-- TOC entry 4792 (class 0 OID 0)
-- Dependencies: 360
-- Name: SEQUENCE seq_proveedor; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_proveedor TO suite WITH GRANT OPTION;


--
-- TOC entry 4793 (class 0 OID 0)
-- Dependencies: 361
-- Name: SEQUENCE seq_puestos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_puestos TO suite WITH GRANT OPTION;


--
-- TOC entry 4794 (class 0 OID 0)
-- Dependencies: 362
-- Name: SEQUENCE seq_respuestasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4795 (class 0 OID 0)
-- Dependencies: 363
-- Name: SEQUENCE seq_respuestaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_respuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4796 (class 0 OID 0)
-- Dependencies: 364
-- Name: SEQUENCE seq_revplanesoperativos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_revplanesoperativos TO suite WITH GRANT OPTION;


--
-- TOC entry 4797 (class 0 OID 0)
-- Dependencies: 365
-- Name: SEQUENCE seq_rolempleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_rolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4798 (class 0 OID 0)
-- Dependencies: 366
-- Name: SEQUENCE seq_subfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_subfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4799 (class 0 OID 0)
-- Dependencies: 367
-- Name: SEQUENCE seq_tipocapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_tipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4800 (class 0 OID 0)
-- Dependencies: 368
-- Name: SEQUENCE seq_validaevaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_validaevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4801 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgrhareas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhareas TO suite WITH GRANT OPTION;


--
-- TOC entry 4802 (class 0 OID 0)
-- Dependencies: 370
-- Name: TABLE tsgrhasignacionesemp; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhasignacionesemp TO suite WITH GRANT OPTION;


--
-- TOC entry 4803 (class 0 OID 0)
-- Dependencies: 371
-- Name: TABLE tsgrhcapacitaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcapacitaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4804 (class 0 OID 0)
-- Dependencies: 372
-- Name: TABLE tsgrhcartaasignacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcartaasignacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4805 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE tsgrhcatrespuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcatrespuestas TO suite WITH GRANT OPTION;


--
-- TOC entry 4806 (class 0 OID 0)
-- Dependencies: 374
-- Name: TABLE tsgrhclientes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhclientes TO suite WITH GRANT OPTION;


--
-- TOC entry 4807 (class 0 OID 0)
-- Dependencies: 375
-- Name: TABLE tsgrhcontrataciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcontrataciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4808 (class 0 OID 0)
-- Dependencies: 376
-- Name: TABLE tsgrhcontratos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhcontratos TO suite WITH GRANT OPTION;


--
-- TOC entry 4809 (class 0 OID 0)
-- Dependencies: 377
-- Name: TABLE tsgrhempleados; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhempleados TO suite WITH GRANT OPTION;


--
-- TOC entry 4810 (class 0 OID 0)
-- Dependencies: 378
-- Name: TABLE tsgrhencuesta; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhencuesta TO suite WITH GRANT OPTION;


--
-- TOC entry 4811 (class 0 OID 0)
-- Dependencies: 379
-- Name: TABLE tsgrhencuesta_participantes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhencuesta_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4812 (class 0 OID 0)
-- Dependencies: 380
-- Name: TABLE tsgrhescolaridad; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhescolaridad TO suite WITH GRANT OPTION;


--
-- TOC entry 4813 (class 0 OID 0)
-- Dependencies: 381
-- Name: TABLE tsgrhestatuscapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhestatuscapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4814 (class 0 OID 0)
-- Dependencies: 382
-- Name: TABLE tsgrhevacapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevacapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4815 (class 0 OID 0)
-- Dependencies: 383
-- Name: TABLE tsgrhevacontestadas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevacontestadas TO suite WITH GRANT OPTION;


--
-- TOC entry 4816 (class 0 OID 0)
-- Dependencies: 384
-- Name: TABLE tsgrhevaluaciones; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevaluaciones TO suite WITH GRANT OPTION;


--
-- TOC entry 4817 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE tsgrhexperienciaslaborales; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhexperienciaslaborales TO suite WITH GRANT OPTION;


--
-- TOC entry 4818 (class 0 OID 0)
-- Dependencies: 386
-- Name: TABLE tsgrhfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4819 (class 0 OID 0)
-- Dependencies: 387
-- Name: TABLE tsgrhidiomas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhidiomas TO suite WITH GRANT OPTION;


--
-- TOC entry 4820 (class 0 OID 0)
-- Dependencies: 388
-- Name: TABLE tsgrhlogistica; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhlogistica TO suite WITH GRANT OPTION;


--
-- TOC entry 4821 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE tsgrhlugares; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhlugares TO suite WITH GRANT OPTION;


--
-- TOC entry 4822 (class 0 OID 0)
-- Dependencies: 390
-- Name: TABLE tsgrhmodo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhmodo TO suite WITH GRANT OPTION;


--
-- TOC entry 4823 (class 0 OID 0)
-- Dependencies: 391
-- Name: TABLE tsgrhperfiles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhperfiles TO suite WITH GRANT OPTION;


--
-- TOC entry 4824 (class 0 OID 0)
-- Dependencies: 392
-- Name: TABLE tsgrhplancapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhplancapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4825 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE tsgrhplanoperativo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 4826 (class 0 OID 0)
-- Dependencies: 394
-- Name: TABLE tsgrhpreguntasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpreguntasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4827 (class 0 OID 0)
-- Dependencies: 395
-- Name: TABLE tsgrhpreguntaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpreguntaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4828 (class 0 OID 0)
-- Dependencies: 396
-- Name: TABLE tsgrhprocesos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhprocesos TO suite WITH GRANT OPTION;


--
-- TOC entry 4829 (class 0 OID 0)
-- Dependencies: 397
-- Name: TABLE tsgrhproveedores; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhproveedores TO suite WITH GRANT OPTION;


--
-- TOC entry 4830 (class 0 OID 0)
-- Dependencies: 398
-- Name: TABLE tsgrhpuestos; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhpuestos TO suite WITH GRANT OPTION;


--
-- TOC entry 4831 (class 0 OID 0)
-- Dependencies: 399
-- Name: TABLE tsgrhrelacionroles; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrelacionroles TO suite WITH GRANT OPTION;


--
-- TOC entry 4832 (class 0 OID 0)
-- Dependencies: 400
-- Name: TABLE tsgrhrespuestasenc; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrespuestasenc TO suite WITH GRANT OPTION;


--
-- TOC entry 4833 (class 0 OID 0)
-- Dependencies: 401
-- Name: TABLE tsgrhrespuestaseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrespuestaseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4834 (class 0 OID 0)
-- Dependencies: 402
-- Name: TABLE tsgrhrevplanoperativo; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrevplanoperativo TO suite WITH GRANT OPTION;


--
-- TOC entry 4835 (class 0 OID 0)
-- Dependencies: 403
-- Name: TABLE tsgrhrolempleado; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhrolempleado TO suite WITH GRANT OPTION;


--
-- TOC entry 4836 (class 0 OID 0)
-- Dependencies: 404
-- Name: TABLE tsgrhsubfactoreseva; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhsubfactoreseva TO suite WITH GRANT OPTION;


--
-- TOC entry 4837 (class 0 OID 0)
-- Dependencies: 405
-- Name: TABLE tsgrhtipocapacitacion; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhtipocapacitacion TO suite WITH GRANT OPTION;


--
-- TOC entry 4838 (class 0 OID 0)
-- Dependencies: 406
-- Name: TABLE tsgrhvalidaevaluaciondes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhvalidaevaluaciondes TO suite WITH GRANT OPTION;


--
-- TOC entry 4839 (class 0 OID 0)
-- Dependencies: 407
-- Name: SEQUENCE seq_respuestas_participantes; Type: ACL; Schema: sgrt; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrt.seq_respuestas_participantes TO suite WITH GRANT OPTION;


--
-- TOC entry 4840 (class 0 OID 0)
-- Dependencies: 273
-- Name: SEQUENCE seq_aceptaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_aceptaciones FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_aceptaciones TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_aceptaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4841 (class 0 OID 0)
-- Dependencies: 274
-- Name: SEQUENCE seq_asignaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_asignaciones FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_asignaciones TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_asignaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4842 (class 0 OID 0)
-- Dependencies: 275
-- Name: SEQUENCE seq_candidatos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_candidatos FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_candidatos TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_candidatos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4843 (class 0 OID 0)
-- Dependencies: 276
-- Name: SEQUENCE seq_cartaasignaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_cartaasignaciones FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_cartaasignaciones TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cartaasignaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4844 (class 0 OID 0)
-- Dependencies: 277
-- Name: SEQUENCE seq_cotizaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_cotizaciones FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_cotizaciones TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cotizaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4845 (class 0 OID 0)
-- Dependencies: 278
-- Name: SEQUENCE seq_cursos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_cursos FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_cursos TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_cursos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4846 (class 0 OID 0)
-- Dependencies: 279
-- Name: SEQUENCE seq_entrevistas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_entrevistas FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_entrevistas TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_entrevistas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4847 (class 0 OID 0)
-- Dependencies: 280
-- Name: SEQUENCE seq_envios; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_envios FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_envios TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_envios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4848 (class 0 OID 0)
-- Dependencies: 281
-- Name: SEQUENCE seq_escolaridad; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_escolaridad FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_escolaridad TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_escolaridad TO postgres WITH GRANT OPTION;


--
-- TOC entry 4849 (class 0 OID 0)
-- Dependencies: 282
-- Name: SEQUENCE seq_experiencias; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_experiencias FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_experiencias TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_experiencias TO postgres WITH GRANT OPTION;


--
-- TOC entry 4850 (class 0 OID 0)
-- Dependencies: 283
-- Name: SEQUENCE seq_firmas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_firmas FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_firmas TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_firmas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4851 (class 0 OID 0)
-- Dependencies: 284
-- Name: SEQUENCE seq_habilidades; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_habilidades FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_habilidades TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_habilidades TO postgres WITH GRANT OPTION;


--
-- TOC entry 4852 (class 0 OID 0)
-- Dependencies: 285
-- Name: SEQUENCE seq_idiomas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_idiomas FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_idiomas TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_idiomas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4853 (class 0 OID 0)
-- Dependencies: 286
-- Name: SEQUENCE seq_ordenservicios; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_ordenservicios FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_ordenservicios TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_ordenservicios TO postgres WITH GRANT OPTION;


--
-- TOC entry 4854 (class 0 OID 0)
-- Dependencies: 287
-- Name: SEQUENCE seq_prospectos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_prospectos FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_prospectos TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_prospectos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4855 (class 0 OID 0)
-- Dependencies: 288
-- Name: SEQUENCE seq_proyectos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_proyectos FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_proyectos TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_proyectos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4856 (class 0 OID 0)
-- Dependencies: 289
-- Name: SEQUENCE seq_vacantes; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON SEQUENCE sisat.seq_vacantes FROM postgres;
GRANT UPDATE ON SEQUENCE sisat.seq_vacantes TO postgres;
GRANT SELECT,USAGE ON SEQUENCE sisat.seq_vacantes TO postgres WITH GRANT OPTION;


--
-- TOC entry 4857 (class 0 OID 0)
-- Dependencies: 290
-- Name: TABLE tsisatasignaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatasignaciones FROM postgres;
GRANT ALL ON TABLE sisat.tsisatasignaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4858 (class 0 OID 0)
-- Dependencies: 291
-- Name: TABLE tsisatcandidatos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatcandidatos FROM postgres;
GRANT ALL ON TABLE sisat.tsisatcandidatos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4859 (class 0 OID 0)
-- Dependencies: 292
-- Name: TABLE tsisatcartaaceptacion; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatcartaaceptacion FROM postgres;
GRANT ALL ON TABLE sisat.tsisatcartaaceptacion TO postgres WITH GRANT OPTION;


--
-- TOC entry 4860 (class 0 OID 0)
-- Dependencies: 293
-- Name: TABLE tsisatcartaasignacion; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatcartaasignacion FROM postgres;
GRANT ALL ON TABLE sisat.tsisatcartaasignacion TO postgres WITH GRANT OPTION;


--
-- TOC entry 4861 (class 0 OID 0)
-- Dependencies: 294
-- Name: TABLE tsisatcotizaciones; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatcotizaciones FROM postgres;
GRANT ALL ON TABLE sisat.tsisatcotizaciones TO postgres WITH GRANT OPTION;


--
-- TOC entry 4862 (class 0 OID 0)
-- Dependencies: 295
-- Name: TABLE tsisatcursosycerticados; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatcursosycerticados FROM postgres;
GRANT ALL ON TABLE sisat.tsisatcursosycerticados TO postgres WITH GRANT OPTION;


--
-- TOC entry 4863 (class 0 OID 0)
-- Dependencies: 296
-- Name: TABLE tsisatentrevistas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatentrevistas FROM postgres;
GRANT ALL ON TABLE sisat.tsisatentrevistas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4864 (class 0 OID 0)
-- Dependencies: 297
-- Name: TABLE tsisatenviocorreos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatenviocorreos FROM postgres;
GRANT ALL ON TABLE sisat.tsisatenviocorreos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4865 (class 0 OID 0)
-- Dependencies: 298
-- Name: TABLE tsisatescolaridad; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatescolaridad FROM postgres;
GRANT ALL ON TABLE sisat.tsisatescolaridad TO postgres WITH GRANT OPTION;


--
-- TOC entry 4866 (class 0 OID 0)
-- Dependencies: 299
-- Name: TABLE tsisatexperienciaslaborales; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatexperienciaslaborales FROM postgres;
GRANT ALL ON TABLE sisat.tsisatexperienciaslaborales TO postgres WITH GRANT OPTION;


--
-- TOC entry 4867 (class 0 OID 0)
-- Dependencies: 300
-- Name: TABLE tsisatfirmas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatfirmas FROM postgres;
GRANT ALL ON TABLE sisat.tsisatfirmas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4868 (class 0 OID 0)
-- Dependencies: 301
-- Name: TABLE tsisathabilidades; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisathabilidades FROM postgres;
GRANT ALL ON TABLE sisat.tsisathabilidades TO postgres WITH GRANT OPTION;


--
-- TOC entry 4869 (class 0 OID 0)
-- Dependencies: 302
-- Name: TABLE tsisatidiomas; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatidiomas FROM postgres;
GRANT ALL ON TABLE sisat.tsisatidiomas TO postgres WITH GRANT OPTION;


--
-- TOC entry 4870 (class 0 OID 0)
-- Dependencies: 303
-- Name: TABLE tsisatordenservicio; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatordenservicio FROM postgres;
GRANT ALL ON TABLE sisat.tsisatordenservicio TO postgres WITH GRANT OPTION;


--
-- TOC entry 4871 (class 0 OID 0)
-- Dependencies: 304
-- Name: TABLE tsisatprospectos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatprospectos FROM postgres;
GRANT ALL ON TABLE sisat.tsisatprospectos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4872 (class 0 OID 0)
-- Dependencies: 305
-- Name: TABLE tsisatproyectos; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatproyectos FROM postgres;
GRANT ALL ON TABLE sisat.tsisatproyectos TO postgres WITH GRANT OPTION;


--
-- TOC entry 4873 (class 0 OID 0)
-- Dependencies: 306
-- Name: TABLE tsisatvacantes; Type: ACL; Schema: sisat; Owner: postgres
--

REVOKE ALL ON TABLE sisat.tsisatvacantes FROM postgres;
GRANT ALL ON TABLE sisat.tsisatvacantes TO postgres WITH GRANT OPTION;


--
-- TOC entry 2437 (class 826 OID 24366)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2439 (class 826 OID 24368)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2438 (class 826 OID 24367)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2436 (class 826 OID 24365)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgco; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgco GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2445 (class 826 OID 26398)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2447 (class 826 OID 26400)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2446 (class 826 OID 26399)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2444 (class 826 OID 26397)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgnom; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sgnom GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2433 (class 826 OID 24340)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2435 (class 826 OID 24342)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TYPES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2434 (class 826 OID 24341)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON FUNCTIONS  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2432 (class 826 OID 24339)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgrt; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrt GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2441 (class 826 OID 25192)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON SEQUENCES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2443 (class 826 OID 25194)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TYPES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2442 (class 826 OID 25193)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON FUNCTIONS  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2440 (class 826 OID 25191)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sisat; Owner: suite
--

ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat REVOKE ALL ON TABLES  FROM suite;
ALTER DEFAULT PRIVILEGES FOR ROLE suite IN SCHEMA sisat GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2429 (class 826 OID 24335)
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2431 (class 826 OID 24337)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- TOC entry 2430 (class 826 OID 24336)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- TOC entry 2428 (class 826 OID 24334)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;


-- Completed on 2019-06-13 14:12:07 CDT

--
-- PostgreSQL database dump complete
--

