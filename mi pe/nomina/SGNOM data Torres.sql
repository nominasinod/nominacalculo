--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

-- Started on 2019-06-13 13:04:57 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3617 (class 0 OID 20057)
-- Dependencies: 382
-- Data for Name: tsgnomalguinaldo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3618 (class 0 OID 20065)
-- Dependencies: 383
-- Data for Name: tsgnomargumento; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3619 (class 0 OID 20073)
-- Dependencies: 384
-- Data for Name: tsgnombitacora; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3620 (class 0 OID 20081)
-- Dependencies: 385
-- Data for Name: tsgnomcabecera; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomcabecera (cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre, imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_creadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion, cnu_totalemp) VALUES (1, '1', '2018-12-12', '2018-12-12', '2018-12-12', 123.00, 321.00, 1234.00, 1, 1, 1, 1, 1, '2018-12-12', '2018-12-12', NULL);


--
-- TOC entry 3621 (class 0 OID 20086)
-- Dependencies: 386
-- Data for Name: tsgnomcabeceraht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3622 (class 0 OID 20091)
-- Dependencies: 387
-- Data for Name: tsgnomcalculo; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3623 (class 0 OID 20096)
-- Dependencies: 388
-- Data for Name: tsgnomcatincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3624 (class 0 OID 20101)
-- Dependencies: 389
-- Data for Name: tsgnomclasificador; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3625 (class 0 OID 20106)
-- Dependencies: 390
-- Data for Name: tsgnomcncptoquinc; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3626 (class 0 OID 20114)
-- Dependencies: 391
-- Data for Name: tsgnomcncptoquincht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3627 (class 0 OID 20122)
-- Dependencies: 392
-- Data for Name: tsgnomconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3628 (class 0 OID 20127)
-- Dependencies: 393
-- Data for Name: tsgnomconceptosat; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3629 (class 0 OID 20132)
-- Dependencies: 394
-- Data for Name: tsgnomconfpago; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3630 (class 0 OID 20137)
-- Dependencies: 395
-- Data for Name: tsgnomejercicio; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomejercicio (cod_ejercicioid, cnu_valorejercicio, bol_estatus) VALUES (1, 1970, true);


--
-- TOC entry 3631 (class 0 OID 20142)
-- Dependencies: 396
-- Data for Name: tsgnomempleados; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (1, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (10, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (11, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (12, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (13, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (14, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (15, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (16, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (17, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (18, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (19, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (20, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');
INSERT INTO sgnom.tsgnomempleados (cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk, imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios, cod_banco, cod_sucursal, cod_cuenta, cod_clabe, txt_descripcionbaja, aud_codcreadopor, aud_feccreacioncion, aud_codmodificadopor, aud_fecmodificacion) VALUES (21, '2019-05-07', '2019-05-10', true, 1, 123.00, 213.00, 123.00, '1', 'o', '123', 321, 12345, '12345', 'baja', 1, '2190-12-21', 1, '1990-12-21');


--
-- TOC entry 3632 (class 0 OID 20150)
-- Dependencies: 397
-- Data for Name: tsgnomempquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomempquincena (cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion, imp_totdeduccion, imp_totalemp, bol_estatusemp) VALUES (1, 1, 1, 213.12, 123.32, 2134.32, true);


--
-- TOC entry 3633 (class 0 OID 20155)
-- Dependencies: 398
-- Data for Name: tsgnomempquincenaht; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3634 (class 0 OID 20160)
-- Dependencies: 399
-- Data for Name: tsgnomestatusnom; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomestatusnom (cod_estatusnomid, cod_estatusnomina, bol_estatus) VALUES (1, 'validada', true);


--
-- TOC entry 3635 (class 0 OID 20165)
-- Dependencies: 400
-- Data for Name: tsgnomformula; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3636 (class 0 OID 20170)
-- Dependencies: 401
-- Data for Name: tsgnomfuncion; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3637 (class 0 OID 20175)
-- Dependencies: 402
-- Data for Name: tsgnomhisttabla; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3638 (class 0 OID 20180)
-- Dependencies: 403
-- Data for Name: tsgnomincidencia; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3639 (class 0 OID 20188)
-- Dependencies: 404
-- Data for Name: tsgnommanterceros; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3640 (class 0 OID 20193)
-- Dependencies: 405
-- Data for Name: tsgnomquincena; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomquincena (cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago, fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) VALUES (1, 'test', '2018-01-01', '2018-01-01', '2018-01-01', '2018-01-01', 1, 1, true);


--
-- TOC entry 3641 (class 0 OID 20198)
-- Dependencies: 406
-- Data for Name: tsgnomtipoconcepto; Type: TABLE DATA; Schema: sgnom; Owner: suite
--



--
-- TOC entry 3642 (class 0 OID 20203)
-- Dependencies: 407
-- Data for Name: tsgnomtiponomina; Type: TABLE DATA; Schema: sgnom; Owner: suite
--

INSERT INTO sgnom.tsgnomtiponomina (cod_tiponominaid, cod_nomina, bol_estatus) VALUES (1, 'pruebas', true);


-- Completed on 2019-06-13 13:04:58 CDT

--
-- PostgreSQL database dump complete
--