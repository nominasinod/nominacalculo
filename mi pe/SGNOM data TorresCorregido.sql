INSERT INTO sgnom.tsgnomcabecera 
(cod_cabeceraid, cod_nbnomina, fec_creacion, fec_ejecucion, fec_cierre,
 imp_totpercepcion, imp_totdeduccion, imp_totalemp, cod_quincenaid_fk, cod_tiponominaid_fk,
  cod_estatusnomid_fk, aud_codcreadopor, aud_codmodificadopor, aud_feccreacion, aud_fecmodificacion,
   cnu_totalemp) 
VALUES (2, '1', '2018-12-12', '2018-12-12', '2018-12-12',
 123.00, 321.00, 1234.00, 1, 1,
  1, 10, 10, '2018-12-12', '2018-12-12',
   NULL);

INSERT INTO sgnom.tsgnomejercicio 
(cod_ejercicioid, cnu_valorejercicio, bol_estatus) 
VALUES (2, 1970, true);

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (3, '2019-05-07', '2019-05-10', true, 10, 
	123.00, 213.00, 123.00, '1', 'o', 
	'123', 321, 12345, '12345', 'baja', 
	10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (10, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (11, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (12, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (13, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (14, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (15, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (16, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (17, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (18, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (19, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (20, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');

INSERT INTO sgnom.tsgnomempleados
(cod_empleadoid, fec_ingreso, fec_salida, bol_estatus, cod_empleado_fk,
 imp_sueldoimss, imp_honorarios, imp_finiquito, cod_tipoimss, cod_tipohonorarios,
  cod_banco, cod_sucursal, cod_cuenta, cod_clave, txt_descripcionbaja,
   aud_codcreadopor, aud_feccreacion, aud_codmodificadopor, aud_fecmodificacion) 
VALUES (21, '2019-05-07', '2019-05-10', true, 10,
 123.00, 213.00, 123.00, '1', 'o',
  '123', 321, 12345, '12345', 'baja',
   10, '2190-12-21', 10, '1990-12-21');


INSERT INTO sgnom.tsgnomempquincena 
(cod_empquincenaid, cod_empleadoid_fk, cod_cabeceraid_fk, imp_totpercepcion,
 imp_totdeduccion, imp_totalemp, bol_estatusemp) 
VALUES (2, 1, 1, 213.12,
 123.32, 2134.32, true);

INSERT INTO sgnom.tsgnomestatusnom 
(cod_estatusnomid, cod_estatusnomina, bol_estatus) 
VALUES (2, 'validada', true);

INSERT INTO sgnom.tsgnomquincena 
(cod_quincenaid, des_quincena, fec_inicio, fec_fin, fec_pago,
 fec_dispersion, cnu_numquincena, cod_ejercicioid_fk, bol_estatus) 
VALUES (5, 'test', '2018-01-01', '2018-01-01', '2018-01-01',
	'2018-01-01', 1, 1, true);

INSERT INTO sgnom.tsgnomtiponomina 
(cod_tiponominaid, cod_nomina, bol_estatus) 
VALUES (2, 'pruebas', true);
