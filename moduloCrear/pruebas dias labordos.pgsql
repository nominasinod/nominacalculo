SELECT * FROM "sgnom"."tsgnomcncptoquinc" LIMIT 1000;

SELECT * FROM sgnom.tsgnomempquincena LIMIT 1000;

SELECT * -- fec_ingreso, fec_salida 
FROM sgnom.tsgnomempleados
WHERE cod_empleadoid = 1;

SELECT FECHA_INICIO_QUINCENA,
       FECHA_FIN_QUINCENA
FROM RH_NOMN_CAT_QUINCENAS
WHERE ID_QUINCENA=
        (SELECT ID_QUINCENA
         FROM RH_NOMN_CAT_QUINCENAS Q
         INNER JOIN RH_NOMN_CAT_EJERCICIOS E ON Q.ID_EJERCICIO_FK =E.ID_EJERCICIO

         WHERE Q.NUMERO_QUINCENA= prmNumQuincenaCalculo
             AND E.VALOR =
                 (SELECT MAX(EJC.VALOR)
                  FROM RH_NOMN_CAT_EJERCICIOS EJC
                  INNER JOIN RH_NOMN_CAT_QUINCENAS CQ ON EJC.ID_EJERCICIO = CQ.ID_EJERCICIO_FK
                  INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB ON CQ.ID_QUINCENA = CAB.ID_QUINCENA_FK) );


SELECT fec_inicio, fec_fin
FROM sgnom.tsgnomquincena
WHERE cod_quincenaid = 
            (SELECT cod_quincenaid
            FROM sgnom.tsgnomquincena Q 
            INNER JOIN sgnom.tsgnomejercicio E ON Q.cod_ejercicioid_fk = E.cod_ejercicioid
            WHERE cod_quincenaid = 13
            AND E.cnu_valorejercicio = (
                SELECT MAX(cnu_valorejercicio)
                FROM sgnom.tsgnomejercicio EJC 
                INNER JOIN sgnom.tsgnomquincena CQ ON EJC.cod_ejercicioid = CQ.cod_ejercicioid_fk
                INNER JOIN sgnom.tsgnomcabecera CAB ON CQ.cod_quincenaid = CAB.cod_quincenaid_fk))

SELECT fec_inicio, fec_fin
FROM sgnom.tsgnomquincena
WHERE cod_quincenaid =
            (SELECT cod_quincenaid 
            FROM sgnom.tsgnomquincena 
            WHERE cod_quincenaid = 13 
            AND cod_ejercicioid_fk = (SELECT cod_ejercicioid
                                        FROM sgnom.tsgnomejercicio
                                        WHERE cnu_valorejercicio = (
                                                SELECT date_part('year',now()))))

