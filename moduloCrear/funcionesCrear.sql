-- SEQUENCE: sgnom.seq_cabecera
 -- DROP SEQUENCE sgnom.seq_cabecera;

CREATE SEQUENCE sgnom.seq_cabecera INCREMENT 1
START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;


ALTER SEQUENCE sgnom.seq_cabecera OWNER TO suite;

GRANT
UPDATE ON SEQUENCE sgnom.seq_cabecera TO suite;

GRANT
SELECT, USAGE ON SEQUENCE sgnom.seq_cabecera TO suite WITH GRANT OPTION;

--nextval('sgnom.seq_cabecera'::regclass)
 -- SEQUENCE: sgnom.seq_empquincena
 -- DROP SEQUENCE sgnom.seq_empquincena;

CREATE SEQUENCE sgnom.seq_empquincena INCREMENT 1
START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;


ALTER SEQUENCE sgnom.seq_empquincena OWNER TO suite;

GRANT
UPDATE ON SEQUENCE sgnom.seq_empquincena TO suite;

GRANT
SELECT, USAGE ON SEQUENCE sgnom.seq_empquincena TO suite WITH GRANT OPTION;

--nextval('sgnom.seq_empquincena'::regclass)
 ---------------------------------------
--actualiza importe total en la cabecera
---------------------------------

CREATE FUNCTION sgnom.totalimpcab(id integer) RETURNS boolean AS $$
DECLARE
 total numeric(10,2);
 BEGIN
   SELECT (imp_totpercepcion - imp_totdeduccion)
   INTO total
   FROM sgnom.tsgnomcabecera
   WHERE cod_cabeceraid = id;
  UPDATE sgnom.tsgnomcabecera SET imp_totalemp=total
   WHERE cod_cabeceraid = id;
RETURN true;
END;
$$ LANGUAGE plpgsql;

;

--SELECT sgnom.totalimpcab(1)
 ----------------------------------------------
--crea nomina
----------------------------------------------

CREATE FUNCTION sgnom.creaNomina(nombre character varying, creacion date, quincena integer, tipo integer, estatus integer, codcreado integer, feccreado date) RETURNS boolean AS $$
 DECLARE
 ultimoid integer;
 rec RECORD;
 BEGIN
    INSERT INTO sgnom.tsgnomcabecera(cod_nbnomina, fec_creacion, cod_quincenaid_fk, cod_tiponominaid_fk, cod_estatusnomid_fk, aud_codcreadopor, aud_feccreacion)
			VALUES (nombre, creacion, quincena, tipo, estatus, codcreado, feccreado)
	RETURNING cod_cabeceraid INTO ultimoid;
	IF found THEN
            RETURN true;
	END IF;
	FOR rec IN SELECT cod_empleadoid FROM sgnom.tsgnomempleados WHERE tsgnomempleados.bol_estatus = 't'
	LOOP
		INSERT INTO sgnom.tsgnomempquincena(
		cod_empleadoid_fk, cod_cabeceraid_fk, bol_estatusemp,
        imp_totpercepcion, imp_totdeduccion, imp_totalemp)
		VALUES (rec.cod_empleadoid, ultimoid, 't',
        0,0,0);
		IF found THEN
            RETURN true;
		END IF ;
	END LOOP;
	EXCEPTION WHEN unique_violation THEN
            -- Do nothing, and loop to try the UPDATE again.
    RETURN false;
END;
$$ LANGUAGE plpgsql;

;

--SELECT sgnom.creanomina('cabecera x32','2019-01-01',1,1,1,10,'2019-01-01')

SELECT cod_empleadoid
FROM sgnom.tsgnomempleados
WHERE tsgnomempleados.bol_estatus = 't'
    SELECT tsgnomquincena.fec_inicio
    FROM sgnom.tsgnomquincena,
         sgnom.tsgnomcabecera WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
    AND tsgnomcabecera.cod_cabeceraid = 1
    SELECT tsgnomquincena.fec_fin
    FROM sgnom.tsgnomquincena,
         sgnom.tsgnomcabecera WHERE tsgnomquincena.cod_quincenaid = tsgnomcabecera.cod_quincenaid_fk
    AND tsgnomcabecera.cod_cabeceraid = 1