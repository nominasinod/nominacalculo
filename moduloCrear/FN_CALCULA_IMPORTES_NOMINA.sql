	  CREATE OR REPLACE FUNCTION sgnom.FN_CALCULA_IMPORTES_NOMINA(
	idNomina INTEGER,
    vEmpleados VARCHAR,
    todos      NUMERIC) 
    RETURNS VARCHAR AS $$
    DECLARE
        vTotalPercepcion          NUMERIC(10,2):=0;
        vTotalDeduccion           NUMERIC(10,2):=0;
        vImporteTotal             NUMERIC(10,2):=0;
        vNumRegistros             INTEGER:=0;
        vNumRegistros2            INTEGER:=0;
        vBandera                  INTEGER;
        vIdEmpleadoNomina         INTEGER;
        vTotalPercepcionC         NUMERIC(10,2):=0;
        vTotalDeduccionC          NUMERIC(10,2):=0;
        vImporteTotalC            NUMERIC(10,2):=0;
        vNumeroEmpleados          INTEGER;
        vNomEmpleado              NUMERIC(10,2);
        vTotalEmpleadosNomina     INTEGER;
        vTotalEmpleadosCalculados INTEGER;
        ERR_CODE       INTEGER;
        ERR_MSG        VARCHAR(250);
        a_count INTEGER;
        b_count INTEGER;
        --CURSORES

        C5 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            ORDER BY EQ.cod_empquincenaid;

        C6 CURSOR FOR
            SELECT EQ.cod_empquincenaid
            FROM sgnom.tsgnomempquincena EQ
            WHERE EQ.cod_cabeceraid_fk = idNomina
            AND EQ.bol_estatusemp = 'f'
            ORDER BY EQ.cod_empquincenaid;

    BEGIN
        OPEN C5;
        RAISE INFO 'abre cursor C1';
        LOOP 
            FETCH C5 INTO vIdEmpleadoNomina;
            EXIT WHEN not found;
            RAISE INFO 'empleado %, idNomina %', vIdEmpleadoNomina,idNomina;
            --Se realiza la suma de los conceptos que son percepciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalPercepcion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empquincenaid = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalPercepcion %', vTotalPercepcion;
            IF(vTotalPercepcion        IS NOT NULL) THEN
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            ELSIF(vTotalPercepcion     IS NULL) THEN
                vTotalPercepcion         :=0;
                vTotalPercepcionC        :=vTotalPercepcionC+vTotalPercepcion;
            END IF;
            RAISE INFO 'vTotalPercepcionC %', vTotalPercepcionC;
            --Se realiza la suma de los conceptos que son deducciones
            SELECT SUM(CQ.imp_concepto) INTO vTotalDeduccion
            FROM sgnom.tsgnomcncptoquinc CQ, sgnom.tsgnomconcepto CO, sgnom.tsgnomempquincena EQ
            WHERE CO.cod_tipoconceptoid_fk = 2
            AND EQ.cod_cabeceraid_fk = idNomina
            AND EQ.cod_empleadoid_fk = vIdEmpleadoNomina
            AND EQ.cod_empquincenaid = CQ.cod_empquincenaid_fk
            AND CO.cod_conceptoid = CQ.cod_conceptoid_fk;
            RAISE INFO 'vTotalDeduccion %', vTotalDeduccion;
            IF(vTotalDeduccion         IS NOT NULL) THEN
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            ELSIF (vTotalDeduccion     IS NULL) THEN
                vTotalDeduccion          :=0;
                vTotalDeduccionC         :=vTotalDeduccionC+vTotalDeduccion;
            END IF;
            RAISE INFO 'vTotalDeduccionC %', vTotalDeduccionC;
            IF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NOT NULL) THEN
                SELECT (vTotalPercepcion - vTotalDeduccion) INTO vImporteTotal ;
            ELSIF(vTotalPercepcion IS NULL AND vTotalDeduccion IS NOT NULL) THEN
                vImporteTotal        :=vTotalDeduccion;
            ELSIF(vTotalPercepcion IS NOT NULL AND vTotalDeduccion IS NULL) THEN
                vImporteTotal        :=vTotalPercepcion;
            END IF;
            
            IF(vImporteTotal IS NOT NULL) THEN
                vImporteTotalC :=vImporteTotalC+vImporteTotal;
            ELSE
                vImporteTotalC:=0;
            END IF;
            --Se actualizan los importes de cada empleado asi como el estatus
            UPDATE sgnom.tsgnomempquincena
            SET imp_totpercepcion     =vTotalPercepcion,
                imp_totdeduccion        =vTotalDeduccion,
                imp_totalemp          =vImporteTotal
                --bol_estatusemp ='B'
            WHERE cod_empquincenaid = vIdEmpleadoNomina
            AND cod_cabeceraid_fk = idNomina;
            GET DIAGNOSTICS a_count = ROW_COUNT;
			RAISE INFO 'a_count %', a_count;
            IF (a_count>0) THEN 
                vNumRegistros := vNumRegistros + 1 ;
				RAISE INFO 'vNumRegistros %', vNumRegistros;
            END IF;
            --COMMIT;
        END LOOP;
        CLOSE C5;
        UPDATE sgnom.tsgnomcabecera
        SET imp_totpercepcion = vTotalPercepcionC,
        imp_totdeduccion = vTotalDeduccionC,
        imp_totalemp = vImporteTotalC,
        cod_estatusnomid_fk = 2
        WHERE cod_cabeceraid = idNomina
        AND (cod_estatusnomid_fk=1 OR cod_estatusnomid_fk=2);
        GET DIAGNOSTICS b_count = ROW_COUNT;
        IF (b_count >0) THEN
            vNumRegistros2      := vNumRegistros2 + 1 ;
        END IF; 

        IF (vNumRegistros > 0 AND vNumRegistros2 > 0) THEN
            vBandera := 1;
        ELSIF (vNumRegistros = 0 OR vNumRegistros2 = 0) THEN
            vBandera := 0;
        ELSE
            vBandera:=2;
        END IF;
    RETURN vBandera;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RAISE EXCEPTION '% exp no data found', vIdEmpleado;
			RETURN NULL;
    END;
$$ LANGUAGE plpgsql;