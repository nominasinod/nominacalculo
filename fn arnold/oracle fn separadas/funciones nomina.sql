
CREATE FUNCTION sgnom.FN_CALCULA_DIAS_LABORADOS(
    empid integer,
    quincenaid integer) RETURNS integer AS $$
    DECLARE
    --rtSueldoBase integer;
    vFormula CHARACTER VARYING; --valor de la formula
    vArgumento CHARACTER VARYING; --valor del argumento
    vNombreArgumento CHARACTER VARYING; --valor del argumento
    vNumeroArgumentos INTEGER; --numero de argumentos

    CURSOR C_formula FOR
        SELECT des_formula
        FROM sgnom.tsgnomformula
        WHERE cod_formulaid = 1;

    BEGIN
        OPEN C_formula;

        FETCH C_formula INTO vFormula;

        vNumeroArgumentos := SELECT COUNT(*)
                            FROM regexp_matches(
                                                (SELECT des_formula
                                                FROM sgnom.tsgnomformula
                                                WHERE cod_formulaid = 1), ':[^#0-9/*+$%-:]+', 'g');
        FOR i IN 1 .. vNumeroArgumentos
        LOOP
            --Se obtiene el nombre del argumento
            SELECT cod_nbargumento INTO vNombreArgumento
            FROM sgnom.tsgnomargumento
            WHERE ':' || cod_clavearg = vArgumento;

            --Se obtiene la funci�n del argumento

            SELECT  INTO vFuncionOracle
            FROM sgnom.tsgnomargumento
            WHERE ':' || cod_clavearg = vArgumento;

        END LOOP;

    END;
$$ LANGUAGE plpgsql;

;

