--------------------------------------------------------
--  DDL for Function FN_CALCULA_CONCEPTOS_NOMINA
--------------------------------------------------------

CREATE OR REPLACE EDITIONABLE FUNCTION "USR_SERP_QA"."FN_CALCULA_CONCEPTOS_NOMINA" ( 
    vIdNomina NUMBER, vEmpleados VARCHAR2 , todos NUMBER) 
    RETURN NUMBER AS /******************************************************************************
  NAME:       FN_CALCULA_CONCEPTOS_NOMINA
  PURPOSE:    Funci�n que realiza el c�lculo de una n�mina ordinaria.
  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        13/03/2017                   1. se creo funcion
  ******************************************************************************/ PRAGMA AUTONOMOUS_TRANSACTION;

total NUMBER;
vIdNomEmpPla01 NUMBER;
vIdConEmpPla01 NUMBER;
vIdConcepto01fk NUMBER;
vNumRegistros NUMBER:=0;
vBandera NUMBER;
vFormula VARCHAR2(150);
vArgumento VARCHAR2(100);
vNumeroArgumentos NUMBER;
vFuncionOracle VARCHAR2(100);
vDiasLaborados NUMBER;
vCadenaEjecutar VARCHAR2(500);
vFormulaEjecutar VARCHAR2(500);
vTabulador NUMBER:=0;
vIdEmpleado NUMBER;
vIdPlaza NUMBER;
vNumeroQuincena NUMBER;
vSueldoBase NUMBER:=0;
vConstante NUMBER;
vFormula2 VARCHAR2(150);
vImporteActualizado NUMBER;
vCadenaImporte VARCHAR2(250);
vXML VARCHAR2(4000);
vNombreConcepto VARCHAR2(150);
vValorArgumento NUMBER;
vNombreArgumento VARCHAR2(100);
vCompensasionG NUMBER:=0;
vNumeroEmpleados NUMBER;
vNomEmpleado NUMBER;
vBanderaImporte NUMBER:=0;
vIdManTer NUMBER;
vPorcentajeManTer NUMBER;
vClaveConcepto VARCHAR2(20);
vClaveConcepto2 VARCHAR2(20);
vApoyoVehicular NUMBER;
vAportacionSSIGF NUMBER;
vPrimaVacacional NUMBER;
vFormulaCICAS VARCHAR2(150);
vFormulaCICAC VARCHAR2(150);
vNumeroArgumentosCAS NUMBER;
vSueldoDiario NUMBER:=0;
vExisteAguinaldo NUMBER;
vIdAguinaldo NUMBER;
vIdQuincena NUMBER;
vFaltasSueldoEst NUMBER:=0;
vFaltasSueldoEv NUMBER:=0;
vFaltasSueldoEstAA NUMBER:=0;
vFaltasCompEst NUMBER:=0;
vFaltasCompEv NUMBER:=0;
vFaltasCompEstAA NUMBER:=0;
vISRSueldo NUMBER;
vISRCompGaran NUMBER;
vISRSSI NUMBER;
vISRApoyoVehic NUMBER;
vISRPrimaVac NUMBER;
vValorQuinquenio NUMBER:=0;
vSueldoCompactado NUMBER:=0;
vSueldoBimestral NUMBER:=0;
vQuinquenioBimestral NUMBER:=0;
vSumaSRCEAYV NUMBER;
vSueldoBimestralEV NUMBER;
vQuinquenioBimestralEV NUMBER;
vSumaSRCEAYVEV NUMBER;
vSueldoCompactadoEV NUMBER;
vPensionAlimenticia NUMBER;
vDespensa NUMBER:=0;
xmlDesglose VARCHAR2(3000);
cursorPrimaVacacional SYS_REFCURSOR;
--xmlApoyoVehicular VARCHAR2(3000);
cursorApoyoVehicular SYS_REFCURSOR;
-- xmlISRSSI VARCHAR2(3000);
cursorISRSSI SYS_REFCURSOR;
vSumaSARP NUMBER;
vSumaFOVP NUMBER;
vSumaSRCEAYVPAT NUMBER;
vPorcentajePension NUMBER;
vISREstDesemp NUMBER;
vISRHonorarios NUMBER;
vValorEstimulo NUMBER;
vNumeroFaltas NUMBER;
vDiasLicencia NUMBER;
ERR_CODE INTEGER;
ERR_MSG VARCHAR2(250);
vAhorroSolidario NUMBER;
DecimaImpar NUMBER;
vNumeroQuincenaBIM NUMBER;
vTipoNominaBIM NUMBER;
vEjercicioBIM NUMBER;
vBanderaSegBaja NUMBER;
vValISREstDesempA NUMBER;
vExisteAnt NUMBER;
vSueldoAnt NUMBER;
vCompAnt NUMBER;
vDiasLabAnterior NUMBER;
vFechaNomina NUMBER;
vImpLicMedSueldo NUMBER;
vBanderaEmpNeg NUMBER;
--Se crean los cursores necesarios para obtener informacion que se usara en diferentes loops
CURSOR C1 IS
    SELECT NNEP.ID_NOMEMPPLA01,
        NNEP.ID_EMPLEADO_FK,
        NNEP.ID_PLAZA_FK,
        Q.NUMERO_QUINCENA,
        Q.ID_QUINCENA
    FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01 NNEP
    INNER JOIN RH_NOMN_CABECERAS_NOMINAS NCN 
        ON NCN.ID_NOMINA = NNEP.ID_NOMINA01_FK
    INNER JOIN RH_NOMN_CAT_QUINCENAS Q 
        ON Q.ID_QUINCENA = NCN.ID_QUINCENA_FK
    WHERE NNEP.ID_NOMINA01_FK = vIdNomina
    AND ESTATUS_EMPLEADO_NOMINA!='D'
    ORDER BY NNEP.ID_NOMEMPPLA01;

CURSOR C3 IS
    SELECT NCEM.ID_CONEMPPLA01,
        NCEM.ID_CONCEPTO01_FK,
        CO.NOMBRE_CONCEPTO,
        CO.CLAVE_CONCEPTO
    FROM RH_NOMN_CONPTOS_EMPLE_PLAZA_01 NCEM
    INNER JOIN RH_NOMN_CAT_CONCEPTOS CO 
        ON CO.ID_CONCEPTO = NCEM.ID_CONCEPTO01_FK
    WHERE CO.ID_TIPO_CALCULO_FK = 3
        AND NCEM.ID_NOMEMPPLA01_FK = vIdNomEmpPla01
    ORDER BY CO.ID_TIPO_CONCEPTO_FK,
            CO.PRIORIDAD_CALCULO;

CURSOR C4 IS
    SELECT FORMULA
    FROM RH_NOMN_CAT_FORMULAS
    WHERE ID_FORMULA =
            (SELECT ID_FORMULA_FK
            FROM RH_NOMN_CAT_CONCEPTOS
            WHERE ID_CONCEPTO = vIdConcepto01fk );

CURSOR C5 IS
    SELECT MT.ID_MANUALES_TERCEROS
    FROM RH_NOMN_MANUALES_TERCEROS MT
    WHERE MT.ID_EMPLEADO =
            (SELECT NEP.ID_EMPLEADO_FK
            FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01 NEP
            WHERE NEP.ID_NOMEMPPLA01=vIdNomEmpPla01 )
        AND MT.ID_CONCEPTO = vIdConcepto01fk;

CURSOR C10 IS
    SELECT FORMULA
    FROM RH_NOMN_CAT_FORMULAS
    WHERE ID_FORMULA =
            (SELECT ID_FORMULA_FK
            FROM RH_NOMN_CAT_CONCEPTOS
            WHERE CLAVE_CONCEPTO = 'CICAS' );


CURSOR C11 IS
    SELECT FORMULA
    FROM RH_NOMN_CAT_FORMULAS
    WHERE ID_FORMULA =
            (SELECT ID_FORMULA_FK
            FROM RH_NOMN_CAT_CONCEPTOS
            WHERE CLAVE_CONCEPTO = 'CICAC' );

BEGIN EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPAJUSTEBIMESTRAL'; 
--Si la variable todos es igual a 1 se recorreran todos los registros
SELECT NUMERO_QUINCENA INTO vNumeroQuincenaBIM
FROM RH_NOMN_CAT_QUINCENAS
WHERE ID_QUINCENA=
        (SELECT ID_QUINCENA_FK
         FROM RH_NOMN_CABECERAS_NOMINAS
         WHERE ID_NOMINA=vIdNomina);

SELECT ID_TIPO_NOMINA_FK INTO vTipoNominaBIM
FROM RH_NOMN_CABECERAS_NOMINAS
WHERE ID_NOMINA=vIdNomina;

SELECT ID_EJERCICIO_FK INTO vEjercicioBIM
FROM RH_NOMN_CABECERAS_NOMINAS
WHERE ID_NOMINA=vIdNomina;

IF (todos=1) THEN 
    IF(vNumeroQuincenaBIM=4
                     OR vNumeroQuincenaBIM=8
                     OR vNumeroQuincenaBIM=12
                     OR vNumeroQuincenaBIM=16
                     OR vNumeroQuincenaBIM=20
                     OR vNumeroQuincenaBIM=24)THEN
        FOR i IN (vNumeroQuincenaBIM-3)..(vNumeroQuincenaBIM-1) 
        LOOP
            INSERT INTO TMPAJUSTEBIMESTRAL (ID_NOMINA,ID_QUINCENA_FK,NUMERO_QUINCENA,
            ID_EMPLEADO_FK, ID_CONCEPTO_FK,IMPORTE_CONCEPTO,IMPORTE_GRAVADO,IMPORTE_EXENTO)
            SELECT CAB.ID_NOMINA,
                CAB.ID_QUINCENA_FK,
                CQ.NUMERO_QUINCENA,
                NEP.ID_EMPLEADO_FK,
                CEP.ID_CONCEPTO_FK,
                CEP.IMPORTE_CONCEPTO,
                CEP.IMPORTE_GRAVADO,
                CEP.IMPORTE_EXENTO
            FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
            INNER JOIN RH_NOMN_CAT_CONCEPTOS CO ON CEP.ID_CONCEPTO_FK = CO.ID_CONCEPTO
            INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
            INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
            INNER JOIN RH_NOMN_CAT_QUINCENAS CQ ON CAB.ID_QUINCENA_FK = CQ.ID_QUINCENA
            WHERE CAB.ID_NOMINA =
                    (SELECT ID_NOMINA
                    FROM RH_NOMN_CABECERAS_NOMINAS
                    WHERE ID_QUINCENA_FK=
                            (SELECT ID_QUINCENA
                            FROM RH_NOMN_CAT_QUINCENAS
                            WHERE NUMERO_QUINCENA = i
                                AND ID_EJERCICIO_FK=vEjercicioBIM)
                        AND ID_TIPO_NOMINA_FK=vTipoNominaBIM)
                AND CO.CLAVE_CONCEPTO IN ('07','07E','A1','A2','A3','A4','A5',
                                        'A1E','A2E','A3E','A4E','A5E','102',
                                        '10E','SARP','SARPE','102P',
                                        '102PE','FOVP','FOVPE');
        END LOOP;
    END IF;
    OPEN C1;
    LOOP FETCH C1 INTO vIdNomEmpPla01,
                   vIdEmpleado,
                   vIdPlaza,
                   vNumeroQuincena,
                   vIdQuincena;
        EXIT WHEN C1%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(vIdEmpleado);
        OPEN C10;
        LOOP FETCH C10 INTO vFormula;
            EXIT WHEN C10%NOTFOUND;
            vFormula2 :=vFormula;
            -- DBMS_OUTPUT.PUT_LINE(vFormula);
            vNumeroArgumentos:= regexp_count(vFormula,':');
            FOR vCnt IN 1 .. vNumeroArgumentos 
            LOOP --Se obitnene los argumentos de la formula
                SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) 
                INTO vArgumento FROM dual;
                --Se obtiene el nombre del argumento
                SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
                FROM RH_NOMN_CAT_ARGUMENTOS
                WHERE CLAVE_ARGUMENTO = vArgumento;
                --Se obtiene la funci�n del argumento
                SELECT FUNCION_ORACLE INTO vFuncionOracle
                FROM RH_NOMN_CAT_ARGUMENTOS
                WHERE CLAVE_ARGUMENTO = vArgumento;

                IF (vFuncionOracle IS NOT NULL) THEN 
                    IF (vFuncionOracle = 'FN_CALCULA_SUELDO_DIARIO') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoDiario;
                        vValorArgumento :=vSueldoDiario;
                    ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
                        vValorArgumento :=vDiasLaborados;
                    ELSIF (vFuncionOracle = 'FN_OBTEN_NUMERO_FALTAS') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vNumeroFaltas;
                        vValorArgumento :=vNumeroFaltas;
                    ELSIF (vFuncionOracle = 'FN_OBTEN_DIAS_LICENCIA') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLicencia;
                        vValorArgumento :=vDiasLicencia;
                    END IF;
                ELSE --Si el argumento no tiene funci�n tiene una constante y aqui se obtiene
                    SELECT VALOR_CONSTANTE INTO vConstante
                    FROM RH_NOMN_CAT_ARGUMENTOS
                    WHERE CLAVE_ARGUMENTO = vArgumento;
                    vValorArgumento :=vConstante;
                END IF;
                --Se reemplaza el nombre del argumento de la formula por el valor numerico obtenido anteriormente
                SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
                FROM
                    (SELECT FORMULA
                    FROM RH_NOMN_CAT_FORMULAS
                    WHERE ID_FORMULA =
                            (SELECT ID_FORMULA_FK
                            FROM RH_NOMN_CAT_CONCEPTOS
                            WHERE CLAVE_CONCEPTO = 'CICAS' ) );
            END LOOP;
            vCadenaImporte:='SELECT '|| vFormula2 || ' FROM DUAL';
            -- DBMS_OUTPUT.PUT_LINE(vFormula2);
            EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
            SELECT COUNT(ID_AGUINALDO) INTO vExisteAguinaldo
            FROM RH_NOMN_AGUINALDOS
            WHERE ID_EMPLEADO_FK=vIdEmpleado
                AND ID_PLAZA_FK =vIdPlaza
                AND ID_QUINCENA_FK =vIdQuincena
                AND TIPO_AGUINALDO =0;
            IF(vExisteAguinaldo =0)THEN
                INSERT INTO RH_NOMN_AGUINALDOS ( ID_AGUINALDO, ID_EMPLEADO_FK, ID_PLAZA_FK, ID_QUINCENA_FK, IMPORTE_QUINCENAL, FECHA_DE_CREACION, TIPO_AGUINALDO, DIAS_AGUINALDO )
                VALUES ( SEC_AGUINALDOS.NEXTVAL,
                        vIdEmpleado,
                        vIdPlaza,
                        vIdQuincena,
                        vImporteActualizado,
                        SYSDATE,
                        0, (vDiasLaborados-vNumeroFaltas-vDiasLicencia) );
            ELSE
                SELECT ID_AGUINALDO INTO vIdAguinaldo
                FROM RH_NOMN_AGUINALDOS
                WHERE ID_EMPLEADO_FK=vIdEmpleado
                    AND ID_PLAZA_FK =vIdPlaza
                    AND ID_QUINCENA_FK =vIdQuincena
                    AND TIPO_AGUINALDO =0;
                UPDATE RH_NOMN_AGUINALDOS
                SET IMPORTE_QUINCENAL =vImporteActualizado,
                    DIAS_AGUINALDO = (vDiasLaborados-vNumeroFaltas-vDiasLicencia)
                WHERE ID_AGUINALDO =vIdAguinaldo;
            END IF;
            COMMIT;
        END LOOP;
        CLOSE C10;
        OPEN C11;
        LOOP FETCH C11 INTO vFormula;
            EXIT WHEN C11%NOTFOUND;
            vFormula2 :=vFormula;
            vNumeroArgumentos:= regexp_count(vFormula,':');
            FOR vCnt IN 1 .. vNumeroArgumentos 
            LOOP --Se obitnene los argumentos de la formula
                SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) INTO vArgumento FROM dual;
                
                --Se obtiene el nombre del argumento
                SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
                FROM RH_NOMN_CAT_ARGUMENTOS
                WHERE CLAVE_ARGUMENTO = vArgumento;
                --Se obtiene la funci�n del argumento
                SELECT FUNCION_ORACLE INTO vFuncionOracle
                FROM RH_NOMN_CAT_ARGUMENTOS
                WHERE CLAVE_ARGUMENTO = vArgumento;

                IF (vFuncionOracle IS NOT NULL) THEN 
                    IF (vFuncionOracle = 'FN_CALCULA_COMP_GARAN_DIARIA') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoDiario;
                        vValorArgumento :=vSueldoDiario;
                    ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
                        vValorArgumento :=vDiasLaborados;
                    ELSIF (vFuncionOracle = 'FN_OBTEN_NUMERO_FALTAS') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vNumeroFaltas;
                        vValorArgumento :=vNumeroFaltas;
                    ELSIF (vFuncionOracle = 'FN_OBTEN_DIAS_LICENCIA') THEN 
                        vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLicencia;
                        vValorArgumento :=vDiasLicencia;
                    END IF;
                ELSE --Si el argumento no tiene funci�n tiene una constante y aqui se obtiene
                    SELECT VALOR_CONSTANTE INTO vConstante
                    FROM RH_NOMN_CAT_ARGUMENTOS
                    WHERE CLAVE_ARGUMENTO = vArgumento;
                    vValorArgumento :=vConstante;
                END IF;
                --Se reemplaza el nombre del argumento de la formula por el valor numerico obtenido anteriormente
                SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
                FROM
                    (SELECT FORMULA
                    FROM RH_NOMN_CAT_FORMULAS
                    WHERE ID_FORMULA =
                            (SELECT ID_FORMULA_FK
                            FROM RH_NOMN_CAT_CONCEPTOS
                            WHERE CLAVE_CONCEPTO = 'CICAC' ) );
            END LOOP;
            vCadenaImporte:='SELECT '|| vFormula2 || ' FROM DUAL';
            EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
            SELECT COUNT(ID_AGUINALDO) INTO vExisteAguinaldo
            FROM RH_NOMN_AGUINALDOS
            WHERE ID_EMPLEADO_FK=vIdEmpleado
                AND ID_PLAZA_FK =vIdPlaza
                AND ID_QUINCENA_FK =vIdQuincena
                AND TIPO_AGUINALDO =1;
            IF(vExisteAguinaldo =0)THEN
                INSERT INTO RH_NOMN_AGUINALDOS ( ID_AGUINALDO, ID_EMPLEADO_FK, ID_PLAZA_FK, ID_QUINCENA_FK, IMPORTE_QUINCENAL, FECHA_DE_CREACION, TIPO_AGUINALDO, DIAS_AGUINALDO )
                VALUES ( SEC_AGUINALDOS.NEXTVAL,
                        vIdEmpleado,
                        vIdPlaza,
                        vIdQuincena,
                        vImporteActualizado,
                        SYSDATE,
                        1, (vDiasLaborados-vNumeroFaltas-vDiasLicencia) );
            ELSE
                SELECT ID_AGUINALDO INTO vIdAguinaldo
                FROM RH_NOMN_AGUINALDOS
                WHERE ID_EMPLEADO_FK=vIdEmpleado
                    AND ID_PLAZA_FK =vIdPlaza
                    AND ID_QUINCENA_FK =vIdQuincena
                    AND TIPO_AGUINALDO =1;
                UPDATE RH_NOMN_AGUINALDOS
                SET IMPORTE_QUINCENAL =vImporteActualizado,
                    DIAS_AGUINALDO=(vDiasLaborados-vNumeroFaltas-vDiasLicencia)
                WHERE ID_AGUINALDO =vIdAguinaldo;
            END IF;
            COMMIT;
        END LOOP;
        CLOSE C11;
        OPEN C3;
        LOOP FETCH C3 INTO vIdConEmpPla01,
                   vIdConcepto01fk,
                   vNombreConcepto,
                   vClaveConcepto ;

            EXIT WHEN C3%NOTFOUND;
            OPEN C4;
            LOOP FETCH C4 INTO vFormula;
                EXIT WHEN C4%NOTFOUND;
                vFormula2 :=vFormula;
                --Se inicia construccion de XML
                vXML :='<?xml version="1.0" encoding="ISO8859_1"?>                
                <mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                  
                <claveConcepto>' || vClaveConcepto || '</claveConcepto>                  
                <nombreConcepto>' || vNombreConcepto || '</nombreConcepto>';
                IF(vClaveConcepto ='102')THEN 
                    IF(vNumeroQuincena=4
                                 OR vNumeroQuincena=8
                                 OR vNumeroQuincena=12
                                 OR vNumeroQuincena=16
                                 OR vNumeroQuincena=20
                                 OR vNumeroQuincena=24)THEN 
                        vFormula :='((:SUELDOCOMPBIM+:VALQUINQUENIOBIM)*:SRCEAYV)-:SUMASRCEAYV';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='10E')THEN 
                    IF(vNumeroQuincena=4
                                 OR vNumeroQuincena=8
                                 OR vNumeroQuincena=12
                                 OR vNumeroQuincena=16
                                 OR vNumeroQuincena=20
                                 OR vNumeroQuincena=24)THEN 
                        vFormula :='((:SUELDOCOMPBIMEV+:VALQUINQUENIOBIMEV)*:SRCEAYV)-:SUMASRCEAYVEV';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='SARP')THEN 
                    IF(vNumeroQuincena=4
                                  OR vNumeroQuincena=8
                                  OR vNumeroQuincena=12
                                  OR vNumeroQuincena=16
                                  OR vNumeroQuincena=20
                                  OR vNumeroQuincena=24)THEN 
                        vFormula :='(:SUELDOCOMPBIM*:SAR)-:SUMASARP';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='102P')THEN 
                    IF(vNumeroQuincena=4
                                  OR vNumeroQuincena=8
                                  OR vNumeroQuincena=12
                                  OR vNumeroQuincena=16
                                  OR vNumeroQuincena=20
                                  OR vNumeroQuincena=24)THEN 
                        vFormula :='(:SUELDOCOMPBIM*:SRCEAYVPAT)-:SUMASRCEAYVPAT';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='FOVP')THEN 
                    IF(vNumeroQuincena=4
                                  OR vNumeroQuincena=8
                                  OR vNumeroQuincena=12
                                  OR vNumeroQuincena=16
                                  OR vNumeroQuincena=20
                                  OR vNumeroQuincena=24)THEN 
                        vFormula :='(:SUELDOCOMPBIM*:FDOVIV)-:SUMAFOVP';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='SARPE')THEN 
                    IF(vNumeroQuincena=4
                                   OR vNumeroQuincena=8
                                   OR vNumeroQuincena=12
                                   OR vNumeroQuincena=16
                                   OR vNumeroQuincena=20
                                   OR vNumeroQuincena=24)THEN 
                        vFormula :='(:SUELDOCOMPBIMEV*:SAR)-:SUMASARP';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='102PE')THEN 
                    IF(vNumeroQuincena=4
                                   OR vNumeroQuincena=8
                                   OR vNumeroQuincena=12
                                   OR vNumeroQuincena=16
                                   OR vNumeroQuincena=20
                                   OR vNumeroQuincena=24)THEN 
                        vFormula :='(:SUELDOCOMPBIMEV*:SRCEAYVPAT)-:SUMASRCEAYVPAT';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                IF(vClaveConcepto ='FOVPE')THE
                    IF(vNumeroQuincena=4
                                   OR vNumeroQuincena=8
                                   OR vNumeroQuincena=12
                                   OR vNumeroQuincena=16
                                   OR vNumeroQuincena=20
                                   OR vNumeroQuincena=24)THEN
                        vFormula :='(:SUELDOCOMPBIMEV*:FDOVIV)-:SUMAFOVP';
                        vFormula2 :=vFormula;
                    END IF;
                END IF;
                vXML :=vXML ||'<formula>' || vFormula || '</formula>                  
                <argumentos>';
                --Se obtiene el numero de argumentos que contiene la formula
                vNumeroArgumentos:= regexp_count(vFormula,':');
                FOR vCnt IN 1 .. vNumeroArgumentos 
                LOOP --Se obitnene los argumentos de la formula
                    SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) INTO vArgumento FROM dual;
                    --Se obtiene el nombre del argumento
                    SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
                    FROM RH_NOMN_CAT_ARGUMENTOS
                    WHERE CLAVE_ARGUMENTO = vArgumento;
                    --Se obtiene la funci�n del argumento
                    SELECT FUNCION_ORACLE INTO vFuncionOracle
                    FROM RH_NOMN_CAT_ARGUMENTOS
                    WHERE CLAVE_ARGUMENTO = vArgumento;
                    --Se calcula la funci�n del argumento
                    IF (vFuncionOracle IS NOT NULL) THEN 
                        IF (vFuncionOracle = 'FN_CALCULA_SUELDO_BASE') THEN 
                            vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBase;
                            vValorArgumento :=vSueldoBase;
                        ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN 
                            vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
                            vValorArgumento :=vDiasLaborados;
                        ELSIF (vFuncionOracle ='FN_CALCULA_COMPENSACION_GARAN') THEN 
                            vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vCompensasionG;
                            vValorArgumento :=vCompensasionG;
                        ELSIF (vFuncionOracle = 'FN_CALCULA_DESPENSA') THEN 
                            vFormulaEjecutar :=vFuncionOracle||'('||vDiasLaborados||','||vIdNomina||','||vIdEmpleado||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vDespensa;
                            vValorArgumento :=vDespensa;
                        ELSIF(vFuncionOracle='FN_OBTEN_PORCENTAJE_MAN_TER') THEN 
                            OPEN C5;
                            LOOP FETCH C5 INTO vIdManTer;
                                EXIT WHEN C5%NOTFOUND;
                                vFormulaEjecutar:=vFuncionOracle||'('||vIdManTer||')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                                EXECUTE IMMEDIATE vCadenaEjecutar INTO vPorcentajeManTer;
                                vValorArgumento:=vPorcentajeManTer;
                            END LOOP;
                            CLOSE C5;
                        ELSIF(vFuncionOracle='FN_CALCULA_APOYO_VEHICULAR')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vSueldoBase||','||vCompensasionG||','||vDespensa||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorApoyoVehicular;
                            LOOP FETCH cursorApoyoVehicular INTO vApoyoVehicular,xmlDesglose;
                                EXIT WHEN cursorApoyoVehicular%NOTFOUND;
                                vValorArgumento :=vApoyoVehicular;
                            END LOOP;
                        ELSIF(vFuncionOracle='FN_CALCULA_APORTACION_SSI_GF')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vSueldoBase||','||vCompensasionG||','||vIdNomina||','||vDespensa||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vAportacionSSIGF;
                            vValorArgumento :=vAportacionSSIGF;
                        ELSIF(vFuncionOracle='FN_CALCULA_PRIMA_VACACIONAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorPrimaVacacional;
                            LOOP FETCH cursorPrimaVacacional INTO vPrimaVacacional,xmlDesglose;
                                EXIT WHEN cursorPrimaVacacional%NOTFOUND;
                                vValorArgumento :=vPrimaVacacional;
                            END LOOP;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SUELDOB_EST')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEst;
                            vValorArgumento :=vFaltasSueldoEst;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SUELDOB_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEv;
                            vValorArgumento :=vFaltasSueldoEv;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SB_EST_A')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEstAA;
                            vValorArgumento :=vFaltasSueldoEstAA;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_COMPGA_EST')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEst;
                            vValorArgumento :=vFaltasCompEst;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_COMPGA_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEv;
                            vValorArgumento :=vFaltasCompEv;
                        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_CG_EST_A')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEstAA;
                            vValorArgumento :=vFaltasCompEstAA;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_SUELDO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vSueldoBase||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRSueldo;
                            vValorArgumento :=vISRSueldo;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_COMP_GARAN')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vSueldoBase||','||vCompensasionG||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRCompGaran;
                            vValorArgumento :=vISRCompGaran-vISRSueldo;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_SSI')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','||vIdEmpleado||','||vNumeroQuincena||','||vDiasLaborados||','||vIdNomina||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorISRSSI;
                            LOOP FETCH cursorISRSSI INTO vISRSSI, xmlDesglose;
                                EXIT WHEN cursorISRSSI%NOTFOUND;
                                vValorArgumento :=vISRSSI;
                            END LOOP;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_APOYO_VEHIC')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','||vIdEmpleado||','||vNumeroQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRApoyoVehic;
                        vValorArgumento :=vISRApoyoVehic;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_PRIMA_VAC')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vIdNomEmpPla01||','||vSueldoBase||','||vCompensasionG||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRPrimaVac;
                        vValorArgumento :=vISRPrimaVac;
                        ELSIF(vFuncionOracle='FN_CALCULA_VALOR_QUINQUENIO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorQuinquenio;
                        vValorArgumento :=vValorQuinquenio;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_COMPACTADO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','''||vClaveConcepto||''')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoCompactado;
                        vValorArgumento :=vSueldoCompactado;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_BIMESTRAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','''||vClaveConcepto||''')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBimestral;
                        vValorArgumento :=vSueldoBimestral;
                        ELSIF(vFuncionOracle='FN_OBTEN_QUINQUENIO_BIMESTRAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vQuinquenioBimestral;
                        vValorArgumento :=vQuinquenioBimestral;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYV;
                        vValorArgumento :=vSumaSRCEAYV;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_BIMESTRAL_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','''||vClaveConcepto||''')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBimestralEV;
                        vValorArgumento :=vSueldoBimestralEV;
                        ELSIF(vFuncionOracle='FN_OBTEN_QUINQUENIO_BIM_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vQuinquenioBimestralEV;
                        vValorArgumento :=vQuinquenioBimestralEV;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYV_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYVEV;
                        vValorArgumento :=vSumaSRCEAYVEV;
                        ELSIF(vFuncionOracle='FN_CALCULA_PENSION_ALIMEN')THEN vFormulaEjecutar :=vFuncionOracle||'('||vSueldoCompactado||','||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','||vDespensa||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vPensionAlimenticia;
                        vValorArgumento :=vPensionAlimenticia;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_COMP_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','''||vClaveConcepto||''')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoCompactado;
                        vValorArgumento :=vSueldoCompactado;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SARP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSARP;
                        vValorArgumento :=vSumaSARP;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_FOVP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaFOVP;
                        vValorArgumento :=vSumaFOVP;
                        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYVPAT')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYVPAT;
                        vValorArgumento :=vSumaSRCEAYVPAT;
                        ELSIF(vFuncionOracle='FN_OBTEN_PCT_PENSION')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vPorcentajePension;
                        vValorArgumento :=vPorcentajePension;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_EST_DESEMP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vDespensa||','||vDiasLaborados||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||','||vNumeroQuincena||','''||vClaveConcepto||''')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISREstDesemp;
                        vValorArgumento :=vISREstDesemp;
                        ELSIF(vFuncionOracle='FN_OBTEN_ISR_01F')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValISREstDesempA;
                        vValorArgumento :=vValISREstDesempA;
                        ELSIF(vFuncionOracle='FN_CALCULA_ISR_HONORARIOS')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vDiasLaborados||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRHonorarios;
                        vValorArgumento :=vISRHonorarios;
                        ELSIF(vFuncionOracle='FN_DESC_LIC_MED')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||')';
                        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaEjecutar INTO vImpLicMedSueldo;
                        vValorArgumento :=vImpLicMedSueldo;
                        ELSIF(vFuncionOracle='FN_VALOR_ESTIMULO_DES')THEN 
                            IF(vClaveConcepto='62F')THEN vClaveConcepto2:='78A';
                                vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto2||''')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                                EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
                                vValorArgumento :=vValorEstimulo;
                            ELSIF(vClaveConcepto='62G')THEN 
                                vClaveConcepto2:='78B';
                                vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto2||''')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                                EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
                                vValorArgumento :=vValorEstimulo;
                            ELSE 
                                vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto||''')';
                                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                                EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
                                vValorArgumento :=vValorEstimulo;
                            END IF;
                        ELSIF(vFuncionOracle='FN_CALCULA_AHORRO_SOLIDARIO')THEN 
                            vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaEjecutar INTO vAhorroSolidario;
                            vValorArgumento :=vAhorroSolidario;
                        END IF;
                    ELSE --Si el argumento no tiene funci�n tiene una constante y aqui se obtiene
                        SELECT VALOR_CONSTANTE INTO vConstante
                        FROM RH_NOMN_CAT_ARGUMENTOS
                        WHERE CLAVE_ARGUMENTO = vArgumento;
                        vValorArgumento :=vConstante;
                    END IF;
                    IF(vValorArgumento IS NULL) THEN 
                        vValorArgumento :=0;
                    END IF;
                    --Se reemplaza el nombre del argumento de la formula por el valor numerico obtenido anteriormente
                    SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
                    FROM
                        (SELECT FORMULA
                        FROM RH_NOMN_CAT_FORMULAS
                        WHERE ID_FORMULA =
                                (SELECT ID_FORMULA_FK
                                FROM RH_NOMN_CAT_CONCEPTOS
                                WHERE ID_CONCEPTO = vIdConcepto01fk ) );
                    --Se continua la construccion del XML agregando cada argumento
                    vXML:= vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                            
                    <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
                END LOOP;
                SELECT DISTINCT COUNT(AG.ID_AGUINALDO) INTO vDiasLabAnterior
                FROM RH_NOMN_AGUINALDOS AG
                WHERE AG.ID_QUINCENA_FK=(vIdQuincena-1)
                    AND AG.ID_EMPLEADO_FK=vIdEmpleado;
                IF(vDiasLabAnterior>0)THEN
                    SELECT DISTINCT AG.DIAS_AGUINALDO INTO vDiasLabAnterior
                    FROM RH_NOMN_AGUINALDOS AG
                    WHERE AG.ID_QUINCENA_FK=(vIdQuincena-1)
                        AND AG.ID_EMPLEADO_FK=vIdEmpleado;
                ELSE 
                    vDiasLabAnterior:=0;
                END IF;
                /*
                                --LA FECHA 16/11/17 DETERMINA EL FIN DE LA MIGRACI�N Y PERMITE QUE SE APLIQUEN LAS REGLAS DE NEGOCIO
                                SELECT COUNT(FECHA_INICIO_QUINCENA) INTO vFechaNomina  FROM RH_NOMN_CAT_QUINCENAS
                    WHERE ID_QUINCENA =(SELECT ID_QUINCENA_FK FROM RH_NOMN_CABECERAS_NOMINAS WHERE ID_NOMINA=vIdNomina)
                    AND FECHA_INICIO_QUINCENA>TO_DATE('16/11/2017','DD/MM/YY');
                            IF(vClaveConcepto='07' AND ( MOD(vNumeroQuincena,2) = 0) AND vDiasLaborados=15 AND vDiasLabAnterior=15 AND vFechaNomina =1)THEN

                        SELECT COUNT(CEP.ID_CONEMPPLA) INTO vExisteAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                        ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                        INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                        ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                        WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                        AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                        AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                        WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);
                        -- DBMS_OUTPUT.PUT_LINE('vExisteAnt '||vExisteAnt);
                        IF(vExisteAnt>0)THEN
                        SELECT CEP.IMPORTE_CONCEPTO INTO vSueldoAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                        ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                        INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                        ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                        WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                        AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                        AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                        WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                        vImporteActualizado:=vSueldoBase-vSueldoAnt;

                        --DBMS_OUTPUT.PUT_LINE('vSueldoBase '||vSueldoBase||' vSueldoAnt '||vSueldoAnt||' vImporteActualizado '||vImporteActualizado);

                        ELSE

                            vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                            EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                        END IF;

                    ELSIF(vClaveConcepto='06' AND ( MOD(vNumeroQuincena,2) = 0) AND vDiasLaborados=15 AND vDiasLabAnterior=15 AND vFechaNomina =1)THEN

                        SELECT COUNT(CEP.ID_CONEMPPLA) INTO vExisteAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                        ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                        INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                        ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                        WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                        AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                        AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                        WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                        IF(vExisteAnt>0)THEN
                        SELECT CEP.IMPORTE_CONCEPTO INTO vCompAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                        ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                        INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                        ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                        WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                        AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                        AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                        WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                        vImporteActualizado:=vCompensasionG-vCompAnt;

                        ELSE

                                vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                                EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                            END IF;

                        ELSE
                        vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                        END IF;



                        /* IF(vClaveConcepto='07' OR vClaveConcepto='06')THEN
                        vCadenaImporte:='SELECT TRUNC('|| vFormula2 || ', 2)FROM DUAL';
                        ELSE
                        vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';

                        END IF;
                        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;

                            IF(vClaveConcepto='07')THEN
                        -- DBMS_OUTPUT.PUT_LINE(vCadenaImporte);
                        --DBMS_OUTPUT.PUT_LINE(vSueldoBase);
                        SELECT MOD(
                        (SELECT SUBSTR(
                        vSueldoBase,(
                        SELECT INSTR(TO_CHAR(vSueldoBase),'.', 1, 1)+1  FROM DUAL)
                        ) FROM DUAL),2) INTO DecimaImpar FROM DUAL;

                                IF DecimaImpar= 1 THEN
                                -- DBMS_OUTPUT.PUT_LINE('DecimaImpar '||DecimaImpar);
                                IF MOD(vDiasLaborados,2) = 1 THEN
                                -- DBMS_OUTPUT.PUT_LINE('vDiasLaborados '||vDiasLaborados);
                                -- DBMS_OUTPUT.PUT_LINE('vNumeroQuincena '||vNumeroQuincena);
                                IF MOD(vNumeroQuincena,2) = 1 THEN
                        vImporteActualizado:=vImporteActualizado+.01;
                        --DBMS_OUTPUT.PUT_LINE('ES impar '||vImporteActualizado);
                            END IF;
                            END IF;
                            END IF;

                            END IF;

                            IF(vClaveConcepto='06')THEN
                                -- DBMS_OUTPUT.PUT_LINE(vCadenaImporte);
                                --  DBMS_OUTPUT.PUT_LINE(vCompensasionG);
                                SELECT MOD(
                                (SELECT SUBSTR(
                                vCompensasionG,(
                                SELECT INSTR(TO_CHAR(vCompensasionG),'.', 1, 1)+1  FROM DUAL)
                                ) FROM DUAL),2) INTO DecimaImpar FROM DUAL;

                                IF DecimaImpar= 1 THEN
                                --  DBMS_OUTPUT.PUT_LINE('DecimaImpar '||DecimaImpar);
                                IF MOD(vDiasLaborados,2) = 1 THEN
                                --  DBMS_OUTPUT.PUT_LINE('vDiasLaborados '||vDiasLaborados);
                                -- DBMS_OUTPUT.PUT_LINE('vNumeroQuincena '||vNumeroQuincena);
                                IF MOD(vNumeroQuincena,2) = 1 THEN
                        vImporteActualizado:=vImporteActualizado+.01;
                        -- DBMS_OUTPUT.PUT_LINE('ES impar '||vImporteActualizado);
                            END IF;
                            END IF;
                            END IF;

                            END IF;*/

                IF(vClaveConcepto='07'
                                OR vClaveConcepto='06'
                                OR vClaveConcepto='07E'
                                OR vClaveConcepto='06E')THEN 
                    vCadenaImporte:='SELECT CEIL(ROUND(('|| vFormula2 || '),2) * 100)/100 FROM DUAL';
                ELSE 
                    vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                END IF;
                EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                vXML :=vXML || '</argumentos>';
                IF(xmlDesglose IS NOT NULL) THEN 
                    vXML := vXML || xmlDesglose;
                    xmlDesglose :=NULL;
                END IF;
                --Se agrega el importe total al XML
                vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
                --Se guarda el importe del concepto y el XML generado
                UPDATE RH_NOMN_CONPTOS_EMPLE_PLAZA_01
                SET IMPORTE_CONCEPTO = vImporteActualizado,
                    XML_ARGUMENTO_VALOR = vXML
                WHERE ID_CONEMPPLA01 =vIdConEmpPla01;
                IF (SQL%ROWCOUNT >0) THEN 
                    vNumRegistros := vNumRegistros + 1 ;
                END IF;
                COMMIT; 
            END LOOP;
            CLOSE C4;
        END LOOP;
        CLOSE C3;
        vSueldoBase :=0;
        vDiasLaborados :=0;
        vCompensasionG :=0;
        vDespensa :=0;
        vPorcentajeManTer :=0;
        vApoyoVehicular :=0;
        vAportacionSSIGF :=0;
        vPrimaVacacional :=0;
        vFaltasSueldoEst :=0;
        vFaltasSueldoEv :=0;
        vFaltasSueldoEstAA :=0;
        vFaltasCompEst :=0;
        vFaltasCompEv :=0;
        vFaltasCompEstAA :=0;
        vISRSueldo :=0;
        vISRCompGaran :=0;
        vISRSSI :=0;
        vISRApoyoVehic :=0;
        vISRPrimaVac :=0;
        vValorQuinquenio :=0;
        vSueldoCompactado :=0;
        vSueldoBimestral :=0;
        vQuinquenioBimestral :=0;
        vSumaSRCEAYV :=0;
        vSueldoBimestralEV :=0;
        vQuinquenioBimestralEV:=0;
        vSumaSRCEAYVEV :=0;
        vPensionAlimenticia :=0;
        vSueldoCompactado :=0;
        vSumaSARP :=0;
        vSumaFOVP :=0;
        vSumaSRCEAYVPAT :=0;
        vSumaSRCEAYVPAT :=0;
        vPorcentajePension :=0;
        vISREstDesemp :=0;
        vISRHonorarios :=0;
        vValorEstimulo :=0;
        vAhorroSolidario :=0;
        vConstante :=0;
        vImpLicMedSueldo :=0;
    END LOOP;
    CLOSE C1;
    ----------------------------------------------------
    ----------------------------------------------------
    --Se llama la funcion que calcula los importes de cada concepto
    SELECT FN_CALCULA_IMPORTES_NOMINA(vIdNomina,NULL,1) INTO vBanderaImporte FROM DUAL;
    ----------------------------------------------------------------------------------
ELSE --Se obtiene en numero de empleados de los que se calcularan conceptos
    vNumeroEmpleados:= regexp_count(vEmpleados,':');
    FOR vCntE IN 1 .. vNumeroEmpleados 
    LOOP --Se obtiene el id de RH_NOMN_NOMINA_EMPLE_PLAZA_01 enviado en la variable vEmpleados
        SELECT regexp_substr(vEmpleados,'[#0-9]+',1,vCntE) INTO vNomEmpleado FROM dual;
        vIdNomEmpPla01:=vNomEmpleado;
        --Se obtienen los datos del empleado
        SELECT ID_EMPLEADO_FK INTO vIdEmpleado
        FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01
        WHERE ID_NOMEMPPLA01 = vNomEmpleado
        ORDER BY ID_NOMEMPPLA01;

        SELECT ID_PLAZA_FK INTO vIdPlaza
        FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01
        WHERE ID_NOMEMPPLA01 = vNomEmpleado
        ORDER BY ID_NOMEMPPLA01;

        SELECT Q.NUMERO_QUINCENA INTO vNumeroQuincena
        FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01 NNEP
        INNER JOIN RH_NOMN_CABECERAS_NOMINAS NCN 
            ON NCN.ID_NOMINA = NNEP.ID_NOMINA01_FK
        INNER JOIN RH_NOMN_CAT_QUINCENAS Q 
            ON Q.ID_QUINCENA = NCN.ID_QUINCENA_FK
        WHERE ID_NOMEMPPLA01 = vNomEmpleado
        ORDER BY ID_NOMEMPPLA01;

        SELECT Q.ID_QUINCENA INTO vIdQuincena
        FROM RH_NOMN_NOMINAS_EMPLE_PLAZA_01 NNEP
        INNER JOIN RH_NOMN_CABECERAS_NOMINAS NCN ON NCN.ID_NOMINA = NNEP.ID_NOMINA01_FK
        INNER JOIN RH_NOMN_CAT_QUINCENAS Q ON Q.ID_QUINCENA = NCN.ID_QUINCENA_FK
        WHERE ID_NOMEMPPLA01 = vNomEmpleado
        ORDER BY ID_NOMEMPPLA01;

        IF(vNumeroQuincenaBIM=4
            OR vNumeroQuincenaBIM=8
            OR vNumeroQuincenaBIM=12
            OR vNumeroQuincenaBIM=16
            OR vNumeroQuincenaBIM=20
            OR vNumeroQuincenaBIM=24) THEN
            FOR i IN (vNumeroQuincenaBIM-3)..(vNumeroQuincenaBIM-1) 
            LOOP
                INSERT INTO TMPAJUSTEBIMESTRAL (ID_NOMINA,ID_QUINCENA_FK,NUMERO_QUINCENA,ID_EMPLEADO_FK, ID_CONCEPTO_FK,IMPORTE_CONCEPTO,IMPORTE_GRAVADO,IMPORTE_EXENTO)
                SELECT CAB.ID_NOMINA,
                    CAB.ID_QUINCENA_FK,
                    CQ.NUMERO_QUINCENA,
                    NEP.ID_EMPLEADO_FK,
                    CEP.ID_CONCEPTO_FK,
                    CEP.IMPORTE_CONCEPTO,
                    CEP.IMPORTE_GRAVADO,
                    CEP.IMPORTE_EXENTO
                FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                INNER JOIN RH_NOMN_CAT_CONCEPTOS CO ON CEP.ID_CONCEPTO_FK = CO.ID_CONCEPTO
                INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                INNER JOIN RH_NOMN_CAT_QUINCENAS CQ ON CAB.ID_QUINCENA_FK = CQ.ID_QUINCENA
                WHERE CAB.ID_NOMINA =
                        (SELECT ID_NOMINA
                        FROM RH_NOMN_CABECERAS_NOMINAS
                        WHERE ID_QUINCENA_FK=
                                (SELECT ID_QUINCENA
                                FROM RH_NOMN_CAT_QUINCENAS
                                WHERE NUMERO_QUINCENA = i
                                    AND ID_EJERCICIO_FK=vEjercicioBIM)
                            AND ID_TIPO_NOMINA_FK=vTipoNominaBIM)
                    AND CO.CLAVE_CONCEPTO IN ('07','07E','A1','A2','A3','A4','A5',
                                            'A1E','A2E','A3E','A4E','A5E','102','10E',
                                            'SARP','SARPE','102P','102PE','FOVP','FOVPE')
                    AND NEP.ID_EMPLEADO_FK=vIdEmpleado;
            END LOOP;
        END IF;
        OPEN C10;
        LOOP FETCH C10 INTO vFormula;
        EXIT WHEN C10%NOTFOUND;
            vFormula2 :=vFormula;
            vNumeroArgumentos:= regexp_count(vFormula,':');
            FOR vCnt IN 1 .. vNumeroArgumentos LOOP --Se obitnene los argumentos de la formula
            SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) INTO vArgumento
            FROM dual;
            --Se obtiene el nombre del argumento
            SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
            FROM RH_NOMN_CAT_ARGUMENTOS
            WHERE CLAVE_ARGUMENTO = vArgumento;
            --Se obtiene la funci�n del argumento
            SELECT FUNCION_ORACLE INTO vFuncionOracle
            FROM RH_NOMN_CAT_ARGUMENTOS
            WHERE CLAVE_ARGUMENTO = vArgumento;

            IF (vFuncionOracle IS NOT NULL) THEN IF (vFuncionOracle = 'FN_CALCULA_SUELDO_DIARIO') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoDiario;
                vValorArgumento :=vSueldoDiario;
                ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
                vValorArgumento :=vDiasLaborados;
                ELSIF (vFuncionOracle = 'FN_OBTEN_NUMERO_FALTAS') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                EXECUTE IMMEDIATE vCadenaEjecutar INTO vNumeroFaltas;
                vValorArgumento :=vNumeroFaltas;
                ELSIF (vFuncionOracle = 'FN_OBTEN_DIAS_LICENCIA') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
                vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
                EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLicencia;
                vValorArgumento :=vDiasLicencia;
                END IF;
            ELSE --Si el argumento no tiene funci�n tiene una constante y aqui se obtiene
                SELECT VALOR_CONSTANTE INTO vConstante
                FROM RH_NOMN_CAT_ARGUMENTOS
                WHERE CLAVE_ARGUMENTO = vArgumento;
                vValorArgumento :=vConstante;
            END IF;
            --Se reemplaza el nombre del argumento de la formula por el valor numerico obtenido anteriormente
            SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
            FROM
                (SELECT FORMULA
                FROM RH_NOMN_CAT_FORMULAS
                WHERE ID_FORMULA =
                        (SELECT ID_FORMULA_FK
                        FROM RH_NOMN_CAT_CONCEPTOS
                    WHERE CLAVE_CONCEPTO = 'CICAS' ) );
        END LOOP;
        vCadenaImporte:='SELECT '|| vFormula2 || ' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
        SELECT COUNT(ID_AGUINALDO) INTO vExisteAguinaldo
        FROM RH_NOMN_AGUINALDOS
        WHERE ID_EMPLEADO_FK=vIdEmpleado
            AND ID_PLAZA_FK =vIdPlaza
            AND ID_QUINCENA_FK =vIdQuincena
            AND TIPO_AGUINALDO =0;
        IF(vExisteAguinaldo =0)THEN
            INSERT INTO RH_NOMN_AGUINALDOS ( ID_AGUINALDO, ID_EMPLEADO_FK, ID_PLAZA_FK, ID_QUINCENA_FK, IMPORTE_QUINCENAL, FECHA_DE_CREACION, TIPO_AGUINALDO, DIAS_AGUINALDO )
            VALUES ( SEC_AGUINALDOS.NEXTVAL,
                    vIdEmpleado,
                    vIdPlaza,
                    vIdQuincena,
                    vImporteActualizado,
                    SYSDATE,
                    0, (vDiasLaborados-vNumeroFaltas-vDiasLicencia) );
        ELSE
            SELECT ID_AGUINALDO INTO vIdAguinaldo
            FROM RH_NOMN_AGUINALDOS
            WHERE ID_EMPLEADO_FK=vIdEmpleado
                AND ID_PLAZA_FK =vIdPlaza
                AND ID_QUINCENA_FK =vIdQuincena
                AND TIPO_AGUINALDO =0;
            UPDATE RH_NOMN_AGUINALDOS
            SET IMPORTE_QUINCENAL =vImporteActualizado,
                DIAS_AGUINALDO = (vDiasLaborados-vNumeroFaltas-vDiasLicencia)
            WHERE ID_AGUINALDO =vIdAguinaldo;
        END IF;
        COMMIT;
        END LOOP;
        CLOSE C10;
        OPEN C11;
        LOOP FETCH C11 INTO vFormula;
        EXIT WHEN C11%NOTFOUND;
        vFormula2 :=vFormula;
        vNumeroArgumentos:= regexp_count(vFormula,':');
        FOR vCnt IN 1 .. vNumeroArgumentos LOOP --Se obitnene los argumentos de la formula
        SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) INTO vArgumento
        FROM dual;
        --Se obtiene el nombre del argumento
        SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
        FROM RH_NOMN_CAT_ARGUMENTOS
        WHERE CLAVE_ARGUMENTO = vArgumento;
        --Se obtiene la funci�n del argumento
        SELECT FUNCION_ORACLE INTO vFuncionOracle
        FROM RH_NOMN_CAT_ARGUMENTOS
        WHERE CLAVE_ARGUMENTO = vArgumento;
        IF (vFuncionOracle IS NOT NULL) THEN IF (vFuncionOracle = 'FN_CALCULA_COMP_GARAN_DIARIA') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoDiario;
        vValorArgumento :=vSueldoDiario;
        ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
        vValorArgumento :=vDiasLaborados;
        ELSIF (vFuncionOracle = 'FN_OBTEN_NUMERO_FALTAS') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vNumeroFaltas;
        vValorArgumento :=vNumeroFaltas;
        ELSIF (vFuncionOracle = 'FN_OBTEN_DIAS_LICENCIA') THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLicencia;
        vValorArgumento :=vDiasLicencia;
        END IF;
        ELSE --Si el argumento no tiene funci�n tiene una constante y aqui se obtiene
        SELECT VALOR_CONSTANTE INTO vConstante
        FROM RH_NOMN_CAT_ARGUMENTOS
        WHERE CLAVE_ARGUMENTO = vArgumento;
        vValorArgumento :=vConstante;
        END IF;
        --Se reemplaza el nombre del argumento de la formula por el valor numerico obtenido anteriormente
        SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
        FROM
            (SELECT FORMULA
            FROM RH_NOMN_CAT_FORMULAS
            WHERE ID_FORMULA =
                    (SELECT ID_FORMULA_FK
                    FROM RH_NOMN_CAT_CONCEPTOS
                    WHERE CLAVE_CONCEPTO = 'CICAC' ) );

        END LOOP;
        vCadenaImporte:='SELECT '|| vFormula2 || ' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
        SELECT COUNT(ID_AGUINALDO) INTO vExisteAguinaldo
        FROM RH_NOMN_AGUINALDOS
        WHERE ID_EMPLEADO_FK=vIdEmpleado
            AND ID_PLAZA_FK =vIdPlaza
            AND ID_QUINCENA_FK =vIdQuincena
            AND TIPO_AGUINALDO =1;
        IF(vExisteAguinaldo =0)THEN
        INSERT INTO RH_NOMN_AGUINALDOS ( ID_AGUINALDO, ID_EMPLEADO_FK, ID_PLAZA_FK, ID_QUINCENA_FK, IMPORTE_QUINCENAL, FECHA_DE_CREACION, TIPO_AGUINALDO, DIAS_AGUINALDO )
        VALUES ( SEC_AGUINALDOS.NEXTVAL,
                vIdEmpleado,
                vIdPlaza,
                vIdQuincena,
                vImporteActualizado,
                SYSDATE,
                1, (vDiasLaborados-vNumeroFaltas-vDiasLicencia) );

        ELSE
        SELECT ID_AGUINALDO INTO vIdAguinaldo
        FROM RH_NOMN_AGUINALDOS
        WHERE ID_EMPLEADO_FK=vIdEmpleado
            AND ID_PLAZA_FK =vIdPlaza
            AND ID_QUINCENA_FK =vIdQuincena
            AND TIPO_AGUINALDO =1;


        UPDATE RH_NOMN_AGUINALDOS
        SET IMPORTE_QUINCENAL =vImporteActualizado,
            DIAS_AGUINALDO = (vDiasLaborados-vNumeroFaltas-vDiasLicencia)
        WHERE ID_AGUINALDO =vIdAguinaldo;

        END IF;
        COMMIT;
        END LOOP;
        CLOSE C11;
        OPEN C3;
        LOOP FETCH C3 INTO vIdConEmpPla01,
                        vIdConcepto01fk,
                        vNombreConcepto,
                        vClaveConcepto ;

        EXIT WHEN C3%NOTFOUND;
        OPEN C4;
        LOOP FETCH C4 INTO vFormula;
        EXIT WHEN C4%NOTFOUND;
        vFormula2 :=vFormula;
        --Se inicia construccion de XML
        vXML :='<?xml version="1.0" encoding="ISO8859_1"?><mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>                
        <claveConcepto>' || vClaveConcepto || '</claveConcepto>                
        <nombreConcepto>' || vNombreConcepto || '</nombreConcepto> ';

        IF(vClaveConcepto ='102')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='((:SUELDOCOMPBIM+:VALQUINQUENIOBIM)*:SRCEAYV)-:SUMASRCEAYV';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='10E')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='((:SUELDOCOMPBIMEV+:VALQUINQUENIOBIMEV)*:SRCEAYV)-:SUMASRCEAYVEV';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='SARP')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIM*:SAR)-:SUMASARP';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='102P')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIM*:SRCEAYVPAT)-:SUMASRCEAYVPAT';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='FOVP')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIM*:FDOVIV)-:SUMAFOVP';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='SARPE')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIMEV*:SAR)-:SUMASARP';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='102PE')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIMEV*:SRCEAYVPAT)-:SUMASRCEAYVPAT';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        IF(vClaveConcepto ='FOVPE')THEN IF(vNumeroQuincena=4
                                        OR vNumeroQuincena=8
                                        OR vNumeroQuincena=14
                                        OR vNumeroQuincena=16
                                        OR vNumeroQuincena=20
                                        OR vNumeroQuincena=24)THEN vFormula :='(:SUELDOCOMPBIMEV*:FDOVIV)-:SUMAFOVP';

        vFormula2 :=vFormula;

        END IF;

        END IF;

        vXML :=vXML ||'<formula>' || vFormula || '</formula>                
        <argumentos>';
        --Se obtiene el numero de argumentos de la formula de cada concepto
        vNumeroArgumentos:= regexp_count(vFormula,':');
        FOR vCnt IN 1 .. vNumeroArgumentos LOOP --Se obtienen los argumentos de la formula
        SELECT regexp_substr(vFormula,':[^#0-9/*+$%-:]+',1,vCnt) INTO vArgumento
        FROM dual;
        --Se obtiene el nombre del argumento
        SELECT NOMBRE_ARGUMENTO INTO vNombreArgumento
        FROM RH_NOMN_CAT_ARGUMENTOS
        WHERE CLAVE_ARGUMENTO = vArgumento;
        --Se obtiene la funcion del argumento
        SELECT FUNCION_ORACLE INTO vFuncionOracle
        FROM RH_NOMN_CAT_ARGUMENTOS
        WHERE CLAVE_ARGUMENTO = vArgumento;
        IF (vFuncionOracle IS NOT NULL) THEN IF (vFuncionOracle = 'FN_CALCULA_SUELDO_BASE') THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBase;
        vValorArgumento :=vSueldoBase;
        ELSIF (vFuncionOracle = 'FN_CALCULA_DIAS_LABORADOS') THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDiasLaborados;
        vValorArgumento :=vDiasLaborados;
        ELSIF (vFuncionOracle ='FN_CALCULA_COMPENSACION_GARAN') THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vCompensasionG;
        vValorArgumento :=vCompensasionG;
        ELSIF (vFuncionOracle = 'FN_CALCULA_DESPENSA') THEN vFormulaEjecutar :=vFuncionOracle||'('||vDiasLaborados||','||vIdNomina||','||vIdEmpleado||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vDespensa;
        vValorArgumento :=vDespensa;
        ELSIF(vFuncionOracle='FN_OBTEN_PORCENTAJE_MAN_TER') THEN 
        OPEN C5;
        LOOP FETCH C5 INTO vIdManTer;
            EXIT WHEN C5%NOTFOUND;
            vFormulaEjecutar:=vFuncionOracle||'('||vIdManTer||')';
            vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
            EXECUTE IMMEDIATE vCadenaEjecutar INTO vPorcentajeManTer;
            vValorArgumento:=vPorcentajeManTer;
        END LOOP;
        CLOSE C5;
        ELSIF(vFuncionOracle='FN_CALCULA_APOYO_VEHICULAR')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vSueldoBase||','||vCompensasionG||','||vDespensa||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorApoyoVehicular;
        LOOP FETCH cursorApoyoVehicular INTO vApoyoVehicular,
                                           xmlDesglose;
        EXIT WHEN cursorApoyoVehicular%NOTFOUND;
        vValorArgumento :=vApoyoVehicular;
        END LOOP;
        ELSIF(vFuncionOracle='FN_CALCULA_APORTACION_SSI_GF')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vSueldoBase||','||vCompensasionG||','||vIdNomina||','||vDespensa||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vAportacionSSIGF;
        vValorArgumento :=vAportacionSSIGF;
        ELSIF(vFuncionOracle='FN_CALCULA_PRIMA_VACACIONAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorPrimaVacacional;
        LOOP FETCH cursorPrimaVacacional INTO vPrimaVacacional,
                                           xmlDesglose;
        EXIT WHEN cursorPrimaVacacional%NOTFOUND;
        vValorArgumento :=vPrimaVacacional;
        END LOOP;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SUELDOB_EST')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEst;
        vValorArgumento :=vFaltasSueldoEst;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SUELDOB_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEv;
        vValorArgumento :=vFaltasSueldoEv;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_SB_EST_A')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasSueldoEstAA;
        vValorArgumento :=vFaltasSueldoEstAA;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_COMPGA_EST')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEst;
        vValorArgumento :=vFaltasCompEst;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_COMPGA_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEv;
        vValorArgumento :=vFaltasCompEv;
        ELSIF(vFuncionOracle='FN_CALCULA_FALTAS_CG_EST_A')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vIdQuincena||','||vSueldoBase||','||vCompensasionG||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vFaltasCompEstAA;
        vValorArgumento :=vFaltasCompEstAA;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_SUELDO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vSueldoBase||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRSueldo;
        vValorArgumento :=vISRSueldo;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_COMP_GARAN')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vSueldoBase||','||vCompensasionG||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRCompGaran;
        vValorArgumento :=vISRCompGaran-vISRSueldo;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_SSI')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','||vIdEmpleado||','||vNumeroQuincena||','||vDiasLaborados||','||vIdNomina||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO cursorISRSSI;
        LOOP FETCH cursorISRSSI INTO vISRSSI,
                                    xmlDesglose;
        EXIT WHEN cursorISRSSI%NOTFOUND;
        vValorArgumento :=vISRSSI;
        END LOOP;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_APOYO_VEHIC')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','||vIdEmpleado||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRApoyoVehic;
        vValorArgumento :=vISRApoyoVehic;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_PRIMA_VAC')THEN vFormulaEjecutar :=vFuncionOracle||'('||vTabulador||','||vIdEmpleado||','||vIdPlaza||','||vNumeroQuincena||','||vIdNomEmpPla01||','||vSueldoBase||','||vCompensasionG||','||vDespensa||','||vIdQuincena||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRPrimaVac;
        vValorArgumento :=vISRPrimaVac;
        ELSIF(vFuncionOracle='FN_CALCULA_VALOR_QUINQUENIO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorQuinquenio;
        vValorArgumento :=vValorQuinquenio;
        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_COMPACTADO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoCompactado;
        vValorArgumento :=vSueldoCompactado;
        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_BIMESTRAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBimestral;
        vValorArgumento :=vSueldoBimestral;
        ELSIF(vFuncionOracle='FN_OBTEN_QUINQUENIO_BIMESTRAL')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vQuinquenioBimestral;
        vValorArgumento :=vQuinquenioBimestral;
        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYV;
        vValorArgumento :=vSumaSRCEAYV;
        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_BIMESTRAL_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoBimestralEV;
        vValorArgumento :=vSueldoBimestralEV;
        ELSIF(vFuncionOracle='FN_OBTEN_QUINQUENIO_BIM_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vQuinquenioBimestralEV;
        vValorArgumento :=vQuinquenioBimestralEV;
        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYV_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYVEV;
        vValorArgumento :=vSumaSRCEAYVEV;
        ELSIF(vFuncionOracle='FN_CALCULA_PENSION_ALIMEN')THEN vFormulaEjecutar :=vFuncionOracle||'('||vSueldoCompactado||','||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||','||vDespensa||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vPensionAlimenticia;
        vValorArgumento :=vPensionAlimenticia;
        ELSIF(vFuncionOracle='FN_OBTEN_SUELDO_COMP_EV')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSueldoCompactado;
        vValorArgumento :=vSueldoCompactado;
        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SARP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSARP;
        vValorArgumento :=vSumaSARP;
        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_FOVP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaFOVP;
        vValorArgumento :=vSumaFOVP;
        ELSIF(vFuncionOracle='FN_OBTEN_SUMA_SRCEAYVPAT')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vNumeroQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vSumaSRCEAYVPAT;
        vValorArgumento :=vSumaSRCEAYVPAT;
        ELSIF(vFuncionOracle='FN_OBTEN_PCT_PENSION')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vPorcentajePension;
        vValorArgumento :=vPorcentajePension;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_EST_DESEMP')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vDespensa||','||vDiasLaborados||','||vFaltasSueldoEst||','||vFaltasSueldoEv||','||vFaltasSueldoEstAA||','||vFaltasCompEst||','||vFaltasCompEv||','||vFaltasCompEstAA||','||vNumeroQuincena||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISREstDesemp;
        vValorArgumento :=vISREstDesemp;
        ELSIF(vFuncionOracle='FN_OBTEN_ISR_01F')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValISREstDesempA;
        vValorArgumento :=vValISREstDesempA;
        ELSIF(vFuncionOracle='FN_CALCULA_ISR_HONORARIOS')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||','||vDiasLaborados||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vISRHonorarios;
        vValorArgumento :=vISRHonorarios;
        ELSIF(vFuncionOracle='FN_DESC_LIC_MED')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vImpLicMedSueldo;
        vValorArgumento :=vImpLicMedSueldo;
        ELSIF(vFuncionOracle='FN_VALOR_ESTIMULO_DES')THEN IF(vClaveConcepto='62F')THEN vClaveConcepto2:='78A';
        vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto2||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
        vValorArgumento :=vValorEstimulo;
        ELSIF(vClaveConcepto='62G')THEN vClaveConcepto2:='78B';
        vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto2||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
        vValorArgumento :=vValorEstimulo;
        ELSE vFormulaEjecutar :=vFuncionOracle||'('||vIdNomEmpPla01||','''||vClaveConcepto||''')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vValorEstimulo;
        vValorArgumento :=vValorEstimulo;
        END IF;
        ELSIF(vFuncionOracle='FN_CALCULA_AHORRO_SOLIDARIO')THEN vFormulaEjecutar :=vFuncionOracle||'('||vIdEmpleado||','||vIdQuincena||')';
        vCadenaEjecutar :='select ' || vFormulaEjecutar ||' FROM DUAL';
        EXECUTE IMMEDIATE vCadenaEjecutar INTO vAhorroSolidario;
        vValorArgumento :=vAhorroSolidario;
        END IF;
        ELSE --Si el argumento no tiene una formula debe tener una constante y aqui se obtiene
            SELECT VALOR_CONSTANTE INTO vConstante
            FROM RH_NOMN_CAT_ARGUMENTOS
            WHERE CLAVE_ARGUMENTO = vArgumento;
            vValorArgumento :=vConstante;
        END IF;

        --Se remplaca en nombre del argumento por el valor numerico obtenido anteriormente

        SELECT REPLACE(vFormula2, vArgumento,vValorArgumento) INTO vFormula2
        FROM
            (SELECT FORMULA
            FROM RH_NOMN_CAT_FORMULAS
            WHERE ID_FORMULA =
                    (SELECT ID_FORMULA_FK
                    FROM RH_NOMN_CAT_CONCEPTOS
                    WHERE ID_CONCEPTO = vIdConcepto01fk ) );

        --Se continua la creacion del XML, agregando los argumentos y sus importes
        vXML:=vXML || '<mx.org.ift.serp.conceptoBindingXml.Argumento><nombre>' || vArgumento || '</nombre>                          
        <valor>' || vValorArgumento || '</valor> <descripcion>' || vNombreArgumento || '</descripcion> </mx.org.ift.serp.conceptoBindingXml.Argumento>';
        END LOOP;
        SELECT DISTINCT COUNT(AG.ID_AGUINALDO) INTO vDiasLabAnterior
        FROM RH_NOMN_AGUINALDOS AG
        WHERE AG.ID_QUINCENA_FK=(vIdQuincena-1)
            AND AG.ID_EMPLEADO_FK=vIdEmpleado;

        IF(vDiasLabAnterior>0)THEN
        SELECT DISTINCT AG.DIAS_AGUINALDO INTO vDiasLabAnterior
        FROM RH_NOMN_AGUINALDOS AG
        WHERE AG.ID_QUINCENA_FK=(vIdQuincena-1)
            AND AG.ID_EMPLEADO_FK=vIdEmpleado;

        ELSE vDiasLabAnterior:=0;

        END IF;

        /*
                --LA FECHA 16/11/17 DETERMINA EL FIN DE LA MIGRACI�N Y PERMITE QUE SE APLIQUEN LAS REGLAS DE NEGOCIO
                    SELECT COUNT(FECHA_INICIO_QUINCENA) INTO vFechaNomina  FROM RH_NOMN_CAT_QUINCENAS
        WHERE ID_QUINCENA =(SELECT ID_QUINCENA_FK FROM RH_NOMN_CABECERAS_NOMINAS WHERE ID_NOMINA=vIdNomina)
        AND FECHA_INICIO_QUINCENA>TO_DATE('16/11/2017','DD/MM/YY');
                IF(vClaveConcepto='07' AND ( MOD(vNumeroQuincena,2) = 0) AND vDiasLaborados=15 AND vDiasLabAnterior=15 AND vFechaNomina =1)THEN

                    SELECT COUNT(CEP.ID_CONEMPPLA) INTO vExisteAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                    ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                    ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                    WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                    AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                    AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                    WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);
                        DBMS_OUTPUT.PUT_LINE('vExisteAnt '||vExisteAnt);
                    IF(vExisteAnt>0)THEN
                    SELECT CEP.IMPORTE_CONCEPTO INTO vSueldoAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                    ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                    ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                    WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                    AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                    AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                    WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                    vImporteActualizado:=vSueldoBase-vSueldoAnt;

                    DBMS_OUTPUT.PUT_LINE('vSueldoBase '||vSueldoBase||' vSueldoAnt '||vSueldoAnt||' vImporteActualizado '||vImporteActualizado);

                    ELSE

                        vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                        DBMS_OUTPUT.PUT_LINE('vSueldoBase vImporteActualizado'||vImporteActualizado);
                    END IF;


                ELSIF(vClaveConcepto='06' AND ( MOD(vNumeroQuincena,2) = 0) AND vDiasLaborados=15 AND vDiasLabAnterior=15 AND vFechaNomina =1)THEN

                    SELECT COUNT(CEP.ID_CONEMPPLA) INTO vExisteAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                    ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                    ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                    WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                    AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                    AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                    WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                    IF(vExisteAnt>0)THEN
                    SELECT CEP.IMPORTE_CONCEPTO INTO vCompAnt FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
                    ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB
                    ON NEP.ID_NOMINA_FK = CAB.ID_NOMINA
                    WHERE CEP.ID_CONCEPTO_FK= vIdConcepto01fk
                    AND NEP.ID_EMPLEADO_FK = vIdEmpleado
                    AND CAB.ID_NOMINA = (SELECT CAB2.ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS CAB2
                    WHERE CAB2.ID_QUINCENA_FK=(vIdQuincena-1) AND CAB2.ID_TIPO_NOMINA_FK=vTipoNominaBIM);

                    vImporteActualizado:=vCompensasionG-vCompAnt;

                    ELSE

                        vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                    END IF;

                ELSE
                vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
                --DBMS_OUTPUT.PUT_LINE('vCadenaImporte    '||vCadenaImporte);
                EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
                END IF;



                /*    IF(vClaveConcepto='07')THEN
                DBMS_OUTPUT.PUT_LINE(vCadenaImporte);
                DBMS_OUTPUT.PUT_LINE(vSueldoBase);
                SELECT MOD(
                (SELECT SUBSTR(
                vSueldoBase,(
                SELECT INSTR(TO_CHAR(vSueldoBase),'.', 1, 1)+1  FROM DUAL)
                ) FROM DUAL),2) INTO DecimaImpar FROM DUAL;

                IF DecimaImpar= 1 THEN
                DBMS_OUTPUT.PUT_LINE('DecimaImpar '||DecimaImpar);
                IF MOD(vDiasLaborados,2) = 1 THEN
                DBMS_OUTPUT.PUT_LINE('vDiasLaborados '||vDiasLaborados);
                DBMS_OUTPUT.PUT_LINE('vNumeroQuincena '||vNumeroQuincena);
                IF MOD(vNumeroQuincena,2) = 1 THEN
        vImporteActualizado:=vImporteActualizado+.01;
        DBMS_OUTPUT.PUT_LINE('ES impar '||vImporteActualizado);
            END IF;
            END IF;

            END IF;

            END IF;

            IF(vClaveConcepto='06')THEN
            DBMS_OUTPUT.PUT_LINE('trunc '||vImporteActualizado);
                DBMS_OUTPUT.PUT_LINE(vCadenaImporte);
                DBMS_OUTPUT.PUT_LINE(vCompensasionG);
                SELECT MOD(
                (SELECT SUBSTR(
                vCompensasionG,(
                SELECT INSTR(TO_CHAR(vCompensasionG),'.', 1, 1)+1  FROM DUAL)
                ) FROM DUAL),2) INTO DecimaImpar FROM DUAL;

                IF DecimaImpar= 1 THEN
                DBMS_OUTPUT.PUT_LINE('DecimaImpar '||DecimaImpar);
                IF MOD(vDiasLaborados,2) = 1 THEN
                DBMS_OUTPUT.PUT_LINE('vDiasLaborados '||vDiasLaborados);
                DBMS_OUTPUT.PUT_LINE('vNumeroQuincena '||vNumeroQuincena);
                IF MOD(vNumeroQuincena,2) = 1 THEN
        vImporteActualizado:=vImporteActualizado+.01;
        DBMS_OUTPUT.PUT_LINE('ES impar '||vImporteActualizado);
            END IF;
            END IF;
            ELSE
            DBMS_OUTPUT.PUT_LINE('ES par '||vImporteActualizado);
            END IF;

            END IF;
                */ IF(vClaveConcepto='07'
                        OR vClaveConcepto='06'
                        OR vClaveConcepto='07E'
                        OR vClaveConcepto='06E')THEN vCadenaImporte:='SELECT CEIL(ROUND(('|| vFormula2 || '),2) * 100)/100 FROM DUAL';

        ELSE vCadenaImporte:='SELECT ROUND('|| vFormula2 || ', 2)FROM DUAL';
        END IF;
        EXECUTE IMMEDIATE vCadenaImporte INTO vImporteActualizado;
        vXML :=vXML || '</argumentos>';
        IF(xmlDesglose IS NOT NULL) THEN vXML := vXML || xmlDesglose;
        xmlDesglose :=NULL;
        END IF;
        --Se agrega el importe total al XML
        vXML:=vXML || '<importe>' || vImporteActualizado || '</importe> </mx.org.ift.serp.conceptoBindingXml.DetallesConcepto>  ';
        --Se guarda el importe y el XML
        UPDATE RH_NOMN_CONPTOS_EMPLE_PLAZA_01
        SET IMPORTE_CONCEPTO = vImporteActualizado,
            XML_ARGUMENTO_VALOR = vXML
        WHERE ID_CONEMPPLA01 =vIdConEmpPla01;
        IF (SQL%ROWCOUNT >0) THEN vNumRegistros := vNumRegistros + 1 ;
        END IF;
        COMMIT;
        END LOOP;
        CLOSE C4;
        END LOOP;
        CLOSE C3;
        vSueldoBase :=0;
        vDiasLaborados :=0;
        vCompensasionG :=0;
        vDespensa :=0;
        vPorcentajeManTer :=0;
        vApoyoVehicular :=0;
        vAportacionSSIGF :=0;
        vPrimaVacacional :=0;
        vFaltasSueldoEst :=0;
        vFaltasSueldoEv :=0;
        vFaltasSueldoEstAA :=0;
        vFaltasCompEst :=0;
        vFaltasCompEv :=0;
        vFaltasCompEstAA :=0;
        vISRSueldo :=0;
        vISRCompGaran :=0;
        vISRSSI :=0;
        vISRApoyoVehic :=0;
        vISRPrimaVac :=0;
        vValorQuinquenio :=0;
        vSueldoCompactado :=0;
        vSueldoBimestral :=0;
        vQuinquenioBimestral :=0;
        vSumaSRCEAYV :=0;
        vSueldoBimestralEV :=0;
        vQuinquenioBimestralEV:=0;
        vSumaSRCEAYVEV :=0;
        vPensionAlimenticia :=0;
        vSueldoCompactado :=0;
        vSumaSARP :=0;
        vSumaFOVP :=0;
        vSumaSRCEAYVPAT :=0;
        vSumaSRCEAYVPAT :=0;
        vPorcentajePension :=0;
        vISREstDesemp :=0;
        vISRHonorarios :=0;
        vValorEstimulo :=0;
        vAhorroSolidario :=0;
        vConstante :=0;
        vImpLicMedSueldo :=0;
    END LOOP;
    --Se llama la funcion que calcula los importes de la nomina
    SELECT FN_CALCULA_IMPORTES_NOMINA(vIdNomina,vEmpleados,0) INTO vBanderaImporte
    FROM DUAL;
END IF;
--Se llama la funcion que recalcula los seguros proporcionales por baja
SELECT FN_CALCULA_SEGUROS_BAJA(vIdNomina) INTO vBanderaSegBaja
FROM DUAL;
--Se identifican los empleados con montos negativos para poder ser eliminados
SELECT FN_EMPLEADOS_NEGATIVOS_PREV(vIdNomina) INTO vBanderaEmpNeg
FROM DUAL;

IF vNumRegistros > 0 THEN 
    vBandera := 1;
ELSIF vNumRegistros = 0 THEN 
    vBandera := 0;
ELSE 
    vBandera:=2;
END IF;
RETURN vBandera;

--
/*
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN NULL;
WHEN OTHERS THEN
  err_code := SQLCODE;
  err_msg  := SUBSTR(SQLERRM, 1, 200);
  SPDERRORES('FN_CALCULA_CONCEPTOS_NOMINA',err_code,err_msg,vIdEmpleado);
  RETURN NULL;
  --*/