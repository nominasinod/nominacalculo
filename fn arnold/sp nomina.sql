--------------------------------------------------------
-- Archivo creado  - lunes-marzo-26-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure BAJA_CAT_ISR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."BAJA_CAT_ISR" 
IS 
BEGIN

UPDATE RH_NOMN_CAT_ISR_MENSUAL SET ACTIVO='I'
WHERE FECHA_FIN = TO_CHAR(SYSDATE,'DD/MM/YYYY');
COMMIT;

UPDATE RH_NOMN_CAT_ISR_ANUAL SET ACTIVO='I'
WHERE FECHA_FIN = TO_CHAR(SYSDATE,'DD/MM/YYYY');
COMMIT;

UPDATE RH_NOMN_CAT_SUBSIDIO_EMP_MENS SET ACTIVO='I'
WHERE FECHA_FIN = TO_CHAR(SYSDATE,'DD/MM/YYYY');
COMMIT;

END BAJA_CAT_ISR;

/
--------------------------------------------------------
--  DDL for Procedure CALCULA_PROPORCIONALIDAD_SGMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."CALCULA_PROPORCIONALIDAD_SGMM" (FECHA_REGISTRO IN DATE,TIPONOMINA IN NUMBER,IMPORTE IN NUMBER, PRIMERPAGO OUT NUMBER)IS

DIASPROP NUMBER;
ESTATUSNOMINA NUMBER;

BEGIN

SELECT Q.FECHA_FIN_QUINCENA - FECHA_REGISTRO  INTO DIASPROP
FROM RH_NOMN_CAT_QUINCENAS Q
WHERE TRUNC(SYSDATE) BETWEEN Q.FECHA_INICIO_QUINCENA AND Q.FECHA_FIN_QUINCENA
;


BEGIN
SELECT 
               QN.ID_ESTATUS_NOMINA_FK
               INTO ESTATUSNOMINA
        FROM RH_NOMN_CABECERAS_NOMINAS QN
        WHERE ID_TIPO_NOMINA_FK = TIPONOMINA
        AND ID_QUINCENA_FK = (SELECT Q.ID_QUINCENA
                           FROM RH_NOMN_CAT_QUINCENAS Q
                           WHERE  FECHA_REGISTRO  -- TRUNC(SYSDATE) 
                           BETWEEN Q.FECHA_INICIO_QUINCENA AND Q.FECHA_FIN_QUINCENA)
        ;
EXCEPTION WHEN NO_DATA_FOUND THEN
   ESTATUSNOMINA:= -1;        
END;

IF DIASPROP > 0 THEN
    IF ESTATUSNOMINA <> 3 OR ESTATUSNOMINA = -1 THEN
        PRIMERPAGO := (DIASPROP+1) * (IMPORTE/15);
    ELSE
        PRIMERPAGO := ((DIASPROP+1) * (IMPORTE/15)) + IMPORTE;
    END IF;
ELSE
    PRIMERPAGO := IMPORTE;
END IF;

--DBMS_OUTPUT.PUT_LINE('DIAS PROPORCIONALES: '||DIASPROP);
--DBMS_OUTPUT.PUT_LINE('ESTATUS NOMINA: '||ESTATUSNOMINA);
--DBMS_OUTPUT.PUT_LINE('PRIMER PAGO: '||PRIMERPAGO);


END CALCULA_PROPORCIONALIDAD_SGMM;

/
--------------------------------------------------------
--  DDL for Procedure FONAC_QUIN_BY_ID_CICLO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."FONAC_QUIN_BY_ID_CICLO" (
    idCiclo    IN INTEGER,   
    res OUT SYS_REFCURSOR)
IS

dtFICiclo date;
dtFFCiclo date;
cicloIdQuinIni integer;
cicloIdQuinFin integer;

qry1 varchar2(1500);

begin
  
  select to_date(VIGENCIA_INICIO, 'dd/MM/yy')  , to_date(VIGENCIA_FIN, 'dd/MM/yy') 
  into  dtFICiclo , dtFFCiclo
  from rh_nomn_fonac_ciclos 
  WHERE ID_FONAC_CICLO = idCiclo;
  
  DBMS_OUTPUT.PUT_LINE(' --1--> ' || dtFICiclo || ' ' ||  dtFFCiclo);
  
  select id_quincena 
  into cicloIdQuinIni
  from rh_nomn_cat_quincenas 
      where dtFICiclo
      between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA;
      
      
  DBMS_OUTPUT.PUT_LINE(' --cicloIdQuinIni--> ' || cicloIdQuinIni );
  
  select id_quincena 
  into cicloIdQuinFin
  from rh_nomn_cat_quincenas 
      where dtFFCiclo
      between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA;
  
  DBMS_OUTPUT.PUT_LINE(' --cicloIdQuinFin--> ' || cicloIdQuinFin );
  
  qry1 := '
  
  select q.id_quincena, ( e.VALOR || '' '' || q.DESCRIPCION_QUINCENA ) as descQ
  from rh_nomn_cat_quincenas  q
  inner join rh_nomn_cat_ejercicios e on q.ID_EJERCICIO_FK = e.id_ejercicio
  where q.id_quincena >= ' || cicloIdQuinIni || ' and q.id_quincena <= ' || cicloIdQuinFin || '
  ORDER BY q.id_quincena ASC'
  ;
        
  
  open res for qry1;
     

end;

/
--------------------------------------------------------
--  DDL for Procedure REP_SEG_RET_CONC
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REP_SEG_RET_CONC" (MES IN VARCHAR2,DATOS OUT SYS_REFCURSOR) IS

Q1 NUMBER;
Q2 NUMBER;
IDQUINCENAS VARCHAR2(50);

vQUERY VARCHAR2(5000);

BEGIN

IF MES IS NOT NULL THEN


SELECT Q_1, Q_2 INTO Q1,Q2 
FROM 
(
    SELECT 
        MAX(DECODE((SELECT MOD(NUMERO_QUINCENA,2) FROM DUAL),0,NUMERO_QUINCENA)) Q_1,
        MAX(DECODE((SELECT MOD(NUMERO_QUINCENA,2) FROM DUAL),1,NUMERO_QUINCENA)) Q_2
    FROM RH_NOMN_CAT_QUINCENAS QUI
    INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
    WHERE TO_CHAR(QUI.FECHA_INICIO_QUINCENA,'MM') = MES
    AND QUI.ID_EJERCICIO_FK = (
                               SELECT ID_EJERCICIO
                               FROM RH_NOMN_CAT_EJERCICIOS
                               WHERE VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
                               )
)                               ;

IDQUINCENAS:=Q1||' Q1'||', '||Q2||' Q2';

            vQUERY:='SELECT  CLAVE_ADSCRIPCION,
                    NVL(ES_Q1IMP_TRAB,0) ES_Q1IMP_TRAB,
                    NVL(EV_Q1IMP_TRAB,0) EV_Q1IMP_TRAB,
                    NVL(ES_Q1IMP_DEP,0)  ES_Q1IMP_DEP,
                    NVL(EV_Q1IMP_DEP,0)  EV_Q1IMP_DEP,
                    NVL(ES_Q2IMP_TRAB,0) ES_Q2IMP_TRAB,
                    NVL(EV_Q2IMP_TRAB,0) EV_Q2IMP_TRAB,
                    NVL(ES_Q2IMP_DEP,0)  ES_Q2IMP_DEP,
                    NVL(EV_Q2IMP_DEP,0)  EV_Q2IMP_DEP,
                    NVL(ES_Q1IMP_TRAB,0) + NVL(ES_Q2IMP_TRAB,0) ES_IMP_TRAB,
                    NVL(EV_Q1IMP_TRAB,0) + NVL(EV_Q2IMP_TRAB,0) EV_IMP_TRAB,
                    NVL(ES_Q1IMP_DEP,0)  + NVL(ES_Q2IMP_DEP,0)  ES_IMP_DEP,
                    NVL(EV_Q1IMP_DEP,0)  + NVL(EV_Q2IMP_DEP,0)  EV_IMP_DEP




            FROM 
            (
                SELECT  *
                FROM 
                (
                    SELECT  ADS.CLAVE_ADSCRIPCION,
                            UPPER(TIPNOM.DESCRIPCION) DESCRIPCION,
                            QUI2.NUMERO_QUINCENA,
                            CONEMPPLA.IMPORTE_CONCEPTO IMP_TRAB,
                            DECODE( (SELECT MOD(QUI2.NUMERO_QUINCENA,2) FROM DUAL),0,SEGRET.IMPORTE_PAR_DEP,1,SEGRET.IMPORTE_NON_DEP) IMP_DEP
                    FROM RH_NOMN_CABECERAS_NOMINAS CAB
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON NOMEMPPLA.ID_NOMINA_FK = CAB.ID_NOMINA
                    INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CAT_CONCEPTOS       CPTS      ON CPTS.ID_CONCEPTO =  CONEMPPLA.ID_CONCEPTO_FK
                    INNER JOIN RH_NOMN_CAT_QUINCENAS       QUI2      ON QUI2.ID_QUINCENA = CAB.ID_QUINCENA_FK
                    INNER JOIN RH_NOMN_SEGURO_RETIRO       SEGRET    ON SEGRET.ID_EMPLEADO_FK = NOMEMPPLA.ID_EMPLEADO_FK
                    INNER JOIN RH_NOMN_EMPLEADOS           EMP       ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
                    INNER JOIN RH_NOMN_PLAZAS              PLZ       ON PLZ.ID_PLAZA = EMP.ID_PLAZA
                    INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNOM ON TIPNOM.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
                    INNER JOIN RH_NOMN_ADSCRIPCIONES       ADS       ON ADS.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
                    WHERE CAB.ID_QUINCENA_FK IN (
                                                 SELECT ID_QUINCENA
                                                 FROM RH_NOMN_CAT_QUINCENAS QUI
                                                 INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
                                                 WHERE TO_CHAR(QUI.FECHA_INICIO_QUINCENA,''MM'') = '||MES||
                                                 ' AND QUI.ID_EJERCICIO_FK = (
                                                                            SELECT ID_EJERCICIO
                                                                            FROM RH_NOMN_CAT_EJERCICIOS
                                                                            WHERE VALOR = ( SELECT TO_CHAR(SYSDATE,''YYYY'') FROM DUAL)
                                                                           )
                                                )
                    AND CAB.ID_TIPO_NOMINA_FK IN (1,2)
                    AND CAB.ID_ESTATUS_NOMINA_FK = 3
                    AND CPTS.ID_CONCEPTO IN (39,79)
                )PIVOT (
                            SUM(IMP_TRAB)IMP_TRAB, SUM(IMP_DEP) IMP_DEP FOR NUMERO_QUINCENA IN ('||IDQUINCENAS||')
                        )
                )PIVOT (
                            SUM(Q1_IMP_TRAB)Q1IMP_TRAB, 
                            SUM(Q1_IMP_DEP) Q1IMP_DEP,
                            SUM(Q2_IMP_TRAB)Q2IMP_TRAB, 
                            SUM(Q2_IMP_DEP) Q2IMP_DEP 
                            FOR DESCRIPCION IN (''ESTRUCTURA'' ES,''EVENTUAL'' EV)
                       )
                ORDER BY CLAVE_ADSCRIPCION';


                OPEN DATOS FOR vQUERY;
ELSE 
    OPEN DATOS FOR
            SELECT  0 CLAVE_ADSCRIPCION,
                    0 ES_Q1IMP_TRAB,
                    0 EV_Q1IMP_TRAB,
                    0 ES_Q1IMP_DEP,
                    0 EV_Q1IMP_DEP,

                    0 ES_Q2IMP_TRAB,
                    0 EV_Q2IMP_TRAB,
                    0 ES_Q2IMP_DEP,
                    0 EV_Q2IMP_DEP,

                    0 ES_IMP_TRAB,
                    0 EV_IMP_TRAB,
                    0 ES_IMP_DEP,
                    0 EV_IMP_DEP
            FROM DUAL;

END IF;                

END REP_SEG_RET_CONC;

/
--------------------------------------------------------
--  DDL for Procedure REP_SEGVID_AJUSTE_PRIMA_BASICA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REP_SEGVID_AJUSTE_PRIMA_BASICA" (IDTRIMESTRE IN NUMBER, DATOS OUT SYS_REFCURSOR) 


AS 

  vQUERY VARCHAR2(4000);

  tablatmp        VARCHAR2(300) ; --Para generar el nombre de la tabla temporar para crear
  existTable      integer := 0;
  ERR_CODE          INTEGER;
  ERR_MSG           VARCHAR2(250);
  fechaHasta      VARCHAR2(25);
  flagInserta integer := 0;
  estatusEmp  VARCHAR2(25);

  descTipoMov varchar2(35);
  cntMovs integer;
  cntEmps integer;
   
  idQuincenaIni integer;
  idQuincenaFin integer;
  
  numQuinIniCalc integer;
  numQuinFinCalc integer;
  
  numQuinAplicar integer;
  
  fechaTrimestreINI date;
  fechaTrimestreFIN date;
  
  
  nivelTab  varchar2(50);
  nivelTabNvo varchar2(50);
  sdoBase NUMBER(8,2);
  compGarantizada NUMBER(8,2);
  descUniAdmin varchar2(300);
  nivTabHom varchar2(5);
  
  porcPrimaBasica numeric(8,2) := 1.46;
  
   CURSOR   cursorAltas  IS

        SELECT distinct 
            hismovs.id_historico_movimiento,   
            
            hiscamps.VALOR_BUSQUEDA,
            
            hismovs.fecha,
            hismovs.id_accion_fk, 
                                 
            
            ca.descripcion,            
            
            catcamps.id_tipos_movimiento
            
        FROM       RH_NOMN_BIT_HISTORICOS_MOVS HISMOVS 
        INNER JOIN RH_NOMN_CAT_ACCIONES CA                ON hismovs.id_accion_fk             = CA.id_accion        
        INNER JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS HISCAMPS ON hismovs.id_historico_movimiento  = hiscamps.id_historico_movimiento_fk
        INNER JOIN RH_NOMN_CAT_CAMPOS CATCAMPS            ON hiscamps.id_campo_afectado_fk    = catcamps.id_campo
        WHERE 
              trunc(HISMOVS.FECHA) BETWEEN trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN)
          AND CA.ID_ACCION  in (7,9,14, 11,12,13, 10)   
          --AND HISMOVS.ID_EJERCICIO_FK = 5
          AND HISCAMPS.VALOR_BUSQUEDA != '0'
          --AND   catcamps.id_tipos_movimiento in ()
        ORDER BY HISCAMPS.VALOR_BUSQUEDA, hismovs.id_historico_movimiento;
        
    ca_VALOR_BUSQUEDA VARCHAR2(50 BYTE);
    bhm_FECHA_MOV date;
    bhm_ID_ACCION_FK NUMBER;
    ca_id_historico_movimiento NUMBER;   
    ca_descripcion VARCHAR2(50 BYTE);    
    ca_id_tipos_movimiento NUMBER;
    
    bhm_FECHA_FIN DATE;
    bhm_FECHA_INI DATE;
    
    emp_ID_EMPLEADO NUMBER;
    emp_ID_DATOS_PERSONALES NUMBER;
    emp_ID_PLAZA NUMBER;
    emp_NUMERO_EMPLEADO VARCHAR2(30 BYTE);    
    emp_ID_ESTATUS_EMPLEADO NUMBER;
    
    dp_NOMBRE VARCHAR2(100 BYTE);
    dp_A_PATERNO VARCHAR2(100 BYTE);
    dp_A_MATERNO VARCHAR2(100 BYTE);
    dp_CURP VARCHAR2(20 BYTE);
    dp_RFC VARCHAR2(20 BYTE);
    dp_fecha_nacimiento date;
    dp_sexo NUMBER;
    dp_descSexo varchar2(3);
    
    CURSOR cursorLicencias  IS
      select  
        ID_LICENCIA_MEDICA,
        ID_EMPLEADO_FK,
        FECHA_INICIO,
        FECHA_FIN    
      from 
        RH_NOMN_LICENCIAS_MEDICAS
      where
            trunc(FECHA_INICIO) BETWEEN trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN)
         OR trunc(FECHA_FIN) between    trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN)
        ;
        
      C2_ID_LIC_MED INTEGER;
      C2_ID_EMPLEADO_FK INTEGER;
      C2_FECHA_INI DATE;
      C2_FECHA_FIN  DATE;
    
    
BEGIN

----------------------------------------------------------------------
--  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
----------------------------------------------------------------------

tablatmp  := 'TMP_REP_SIGEVI' || '_'|| TO_CHAR(SYSDATE,'ddmmyy_hhmmss');

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp;
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        --DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
   
     EXECUTE IMMEDIATE 'CREATE TABLE ' || tablatmp || '    
         (  
          AP                VARCHAR2(200),
          AM                VARCHAR2(200),
          NOMBRE            VARCHAR2(200),
          RFC               VARCHAR2(200),
          CURP              VARCHAR2(200),
          FECHA_NAC         VARCHAR2(200),
          SEXO              VARCHAR2(200),
          NIV_TAB           VARCHAR2(200),
          NIV_TAB_HOM       VARCHAR2(200),
          NIV_TAB_NVO       VARCHAR2(200),
          UNID_ADM          VARCHAR2(200),
          PERC_ORD_BTA_MENS VARCHAR2(200),
          COMP_MENS_GAR     VARCHAR2(200),
          PRIMA_MENS_RISGO  VARCHAR2(200),
          PERC_BTA_MENS     VARCHAR2(200),
          COBERTURA_MOVMTO  VARCHAR2(200),
          TIPO_MOVMTO       VARCHAR2(200),
          FECHA_INI_MOVNTO  VARCHAR2(200),
          FECHA_FIN_MOVMTO  VARCHAR2(200),
          NUM_QUIN_APLICAR  VARCHAR2(200),
          PORC_PRIMA_BASICA VARCHAR2(200),
          NUMMESES_POTSUMASE VARCHAR2(200),
          PORC_PRIMA_POT    VARCHAR2(200),
          IMPTE_PRIMBASAJSTE VARCHAR2(200),
          IMPTE_PRIMPOTAJSTE VARCHAR2(200),
          IMPTE_PRIMTOTAJSTE VARCHAR2(200),
          DICE              VARCHAR2(200),
          DEBE              VARCHAR2(200)
       )';
 
-----------------------------------------------------------
--- I D   Q U I N C E N A   D E L   E J E R C I C I O
-----------------------------------------------------------
  
  IF(IDTRIMESTRE = 1)THEN      idQuincenaIni := 1;      idQuincenaFin := 6;  
  ELSE 
  IF (IDTRIMESTRE = 2)THEN     idQuincenaIni := 7;      idQuincenaFin := 12;  
  ELSE 
  IF (IDTRIMESTRE = 3)THEN     idQuincenaIni := 13;     idQuincenaFin := 18;
  ELSE 
  IF (IDTRIMESTRE = 4)THEN     idQuincenaIni := 19;     idQuincenaFin := 24;  
  END IF;   END IF;  END IF;  END IF;
  
  --DBMS_OUTPUT.PUT_LINE(' idQuincenaIni-->' || idQuincenaIni || '    '  || idQuincenafin );    

-----------------------------------------------------------
--- F E C H A   I N I C I A L   D E L   T R I M E S T R E
-----------------------------------------------------------
  select  trunc(cq.FECHA_INICIO_QUINCENA )
  INTO    fechaTrimestreINI
  from    RH_NOMN_CAT_QUINCENAS cq
  where 
          cq.ACTIVO = 1
  and     cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor =  (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual) )
  and     cq.NUMERO_QUINCENA = idQuincenaIni;
  
-----------------------------------------------------------
--- F E C H A   F I N A L   D E L   T R I M E S T R E
-----------------------------------------------------------  
  select  trunc(cq.FECHA_FIN_QUINCENA)
  INTO    fechaTrimestreFIN
  from    RH_NOMN_CAT_QUINCENAS cq
  where 
          cq.ACTIVO = 1
  and     cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor = (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual)  )
  and     cq.NUMERO_QUINCENA = idQuincenaFin;


  --DBMS_OUTPUT.PUT_LINE(' fechaTrimestreINI-->' || fechaTrimestreINI || '    '  || fechaTrimestrefin );
  
  
---------------------------------------------------------------------------------------
---  O B T E N E M O S    A L T A S    B A J A S    L I C E N C I A   M E D I C A
---------------------------------------------------------------------------------------

  OPEN cursorAltas;
    LOOP
      FETCH cursorAltas INTO
          ca_id_historico_movimiento,   
          ca_VALOR_BUSQUEDA,
          bhm_FECHA_MOV,
          bhm_ID_ACCION_FK,
          
          ca_descripcion,
          ca_id_tipos_movimiento
          
      ;
      EXIT WHEN cursorAltas%NOTFOUND;
      
      
    --DBMS_OUTPUT.PUT_LINE('---------------------------- bhm_ID_ACCION_FK-->' || ca_VALOR_BUSQUEDA ||  '       ' || bhm_ID_ACCION_FK);
      fechaHasta := 'NA';
      nivelTabNvo := '';
      descTipoMov := '';
      numQuinAplicar := 0;
      flagInserta := 0;
      
     -------------------------------------------------------------
     --- N � M E R O   D E   Q U I N C E N A S   A P L I C A R
     -------------------------------------------------------------
     
     -- A L T A   E M P L E A D O --  
     
        if ( bhm_ID_ACCION_FK = 7 OR bhm_ID_ACCION_FK = 9 OR bhm_ID_ACCION_FK =  14 )  then
        
            --DBMS_OUTPUT.PUT_LINE('-----------A L T A ----------');
            
            descTipoMov :=  'ALTA';    
            nivelTabNvo := '';
            estatusEmp := '1,2';
            flagInserta := 1;
            
                        
            select  NUMERO_QUINCENA
            into    numQuinIniCalc 
            from    RH_NOMN_CAT_QUINCENAS
            where   bhm_FECHA_MOV BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;
            
        -- OBTENER numQuinFinCalc con los mov de baja y si no hay con le fecha fin del trimestre
            
          select case 
             when exists( select bhm.FECHA						
                          from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
                          inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
                          where 
                              bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
                            and bhm.ID_ACCION_FK IN (10, 11,12, 13) 
                            and trunc(bhm.FECHA) between  trunc(bhm_FECHA_MOV) and trunc(fechaTrimestreFIN)
                            and rownum = 1
                          --ORDER BY bhm.ID_ACCION_FK asc
                        )
             then 	 (  select bhm.FECHA						
                        from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
                        inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
                        where 
                            bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
                          and bhm.ID_ACCION_FK IN (10, 11,12, 13) 
                          and trunc(bhm.FECHA) between  trunc(bhm_FECHA_MOV) and trunc(fechaTrimestreFIN)
                          and rownum = 1
                        --ORDER BY bhm.ID_ACCION_FK asc
                 )
             else null
          end  into bhm_FECHA_FIN
          from dual;
            
            if ( bhm_FECHA_FIN IS NOT NULL ) then            
                
                select to_char(bhm_FECHA_FIN, 'yyyyMMdd'  ) into   fechaHasta from dual;
                
                select  NUMERO_QUINCENA
                into    numQuinFinCalc 
                from    RH_NOMN_CAT_QUINCENAS
                where   trunc(bhm_FECHA_FIN) BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;                
            
            else
                 numQuinFinCalc := idQuincenaFin ;  
            end if;
            
            
    -- C A M B I O   P U E S T O  --   
        ELSE
        if (  bhm_ID_ACCION_FK = 10 ) then
            
            descTipoMov :=  'CAMBIO DE NIVEL';            
            nivelTabNvo := nivelTab;
            estatusEmp := '4';
            
            select  NUMERO_QUINCENA
            into    numQuinIniCalc 
            from    RH_NOMN_CAT_QUINCENAS
            where   bhm_FECHA_MOV BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;
            
        -- OBTENER numQuinIniCalc con los mov de ALTA y si no hay con le fecha INICIAL del trimestre
            
            select count (bhm.FECHA)
            into cntMovs
            from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
            inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
            where 
                  bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
              and bhm.ID_ACCION_FK IN (7,9,14, 10) 
              and trunc(bhm.FECHA) between  trunc(fechaTrimestreINI) and (trunc(bhm_FECHA_MOV)-1);
              
           if(cntMovs > 0)then
           
                select bhm.FECHA 
                into   bhm_FECHA_INI
                from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
                inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
                where 
                      bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
                  and bhm.ID_ACCION_FK IN (7,9,14, 10) 
                  and trunc(bhm.FECHA) between  trunc(fechaTrimestreINI) and (trunc(bhm_FECHA_MOV)-1)
                  and rownum = 1
                ORDER BY bhm.ID_ACCION_FK DESC;
                
                select  NUMERO_QUINCENA
                into    numQuinIniCalc 
                from    RH_NOMN_CAT_QUINCENAS
                where   bhm_FECHA_INI BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;                                
                
           else
                numQuinIniCalc := idQuincenaIni ; 
           end if;
           
           
           
            flagInserta := 1;
                       
     
     -- B A J A    E M P L E A D O
        ELSE
        if (  bhm_ID_ACCION_FK = 11 OR  bhm_ID_ACCION_FK = 12 OR bhm_ID_ACCION_FK = 13 ) then
            --DBMS_OUTPUT.PUT_LINE('----------- B A J A ----------');
            flagInserta := 1;            
            descTipoMov :=  'BAJA';
            nivelTabNvo := '';
            estatusEmp := '1,2';
            
            select  NUMERO_QUINCENA
            into    numQuinFinCalc 
            from    RH_NOMN_CAT_QUINCENAS
            where   bhm_FECHA_MOV BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;
            
        -- OBTENER numQuinIniCalc con los mov de ALTA o CAMBIO DE PUESTO  y si no hay con le fecha INICIO del trimestre
            
            select count (bhm.FECHA)
            into cntMovs
            from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
            inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
            where 
                  bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
              and bhm.ID_ACCION_FK IN (7,9,14,10) 
              and trunc(bhm.FECHA) between  trunc(fechaTrimestreINI) and trunc(bhm_FECHA_MOV);
              
            if(cntMovs > 0)then
           
                select bhm.FECHA
                into   bhm_FECHA_INI
                from   RH_NOMN_BIT_HISTORICOS_CAMPOS bhc
                inner join RH_NOMN_BIT_HISTORICOS_MOVS   bhm on  bhc.ID_HISTORICO_MOVIMIENTO_FK = bhm.ID_HISTORICO_MOVIMIENTO            
                where 
                      bhc.VALOR_BUSQUEDA = ca_VALOR_BUSQUEDA
                  and bhm.ID_ACCION_FK IN (7,9,14,10) 
                  and trunc(bhm.FECHA) between  trunc(fechaTrimestreINI) and trunc(bhm_FECHA_MOV)
                  and rownum = 1
                ORDER BY bhm.ID_ACCION_FK DESC;     
                
                select  NUMERO_QUINCENA
                into    numQuinIniCalc 
                from    RH_NOMN_CAT_QUINCENAS
                where   bhm_FECHA_INI BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;                
                
           else
                numQuinIniCalc := idQuincenaIni ; 
           end if;
                    
            
        ELSE
          flagInserta :=0;
        end if;end if;end if;     
        
        numQuinAplicar := (numQuinFinCalc - numQuinIniCalc) + 1;

 
    
    
       if( flagInserta = 1)then
       
          -------------------------------------------------------------------------------
          -- D A T O S   G E N E R A L E S   D E L   E M P L E A D O 
          -------------------------------------------------------------------------------
          EXECUTE IMMEDIATE '
            select         
                  count (emp.ID_EMPLEADO)                    
                  from
                      RH_NOMN_EMPLEADOS emp 
                  inner join 
                      RH_NOMN_DATOS_PERSONALES dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
                  where                                    
                        emp.NUMERO_EMPLEADO = ' || ca_VALOR_BUSQUEDA || '
                    and emp.ID_ESTATUS_EMPLEADO IN ( ' || estatusEmp || ')
                    and rownum = 1
                  ORDER BY  emp.ID_EMPLEADO desc
                  ' INTO cntEmps;
                  
                  
            if(  cntEmps  > 0 )then
              
              execute immediate '
                    select         
                        emp.ID_EMPLEADO,
                        emp.ID_DATOS_PERSONALES,
                        emp.ID_PLAZA,
                        emp.NUMERO_EMPLEADO,
                        emp.ID_ESTATUS_EMPLEADO,
                        
                        dp.NOMBRE as nombre,
                        dp.A_PATERNO as apPat,
                        dp.A_MATERNO as apMat,
                        dp.CURP,
                        dp.RFC,            
                        dp.FECHA_NACIMIENTO,
                        dp.ID_GENERO 
                    
                    from
                        RH_NOMN_EMPLEADOS emp 
                    inner join 
                        RH_NOMN_DATOS_PERSONALES dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
                    where                                    
                          emp.NUMERO_EMPLEADO = ' || ca_VALOR_BUSQUEDA || '
                      and emp.ID_ESTATUS_EMPLEADO in ( ' || estatusEmp ||')
                      and rownum = 1
                    ORDER BY  emp.ID_EMPLEADO desc'
                    into 
                        emp_ID_EMPLEADO,
                        emp_ID_DATOS_PERSONALES,
                        emp_ID_PLAZA,
                        emp_NUMERO_EMPLEADO,   
                        emp_ID_ESTATUS_EMPLEADO,
                        
                        dp_NOMBRE ,
                        dp_A_PATERNO ,
                        dp_A_MATERNO ,
                        dp_CURP,
                        dp_RFC,
                        dp_fecha_nacimiento,
                        dp_sexo 
                    ;                
                          
                    dp_descSexo :=  'F'; 
                      if (dp_sexo = 1)then  dp_descSexo :=  'M';   end if;
                          
                ---  N I V E L   T A B U L A R
                    select 
                      gpoJer.CLAVE_JERARQUICA || grdsJer.GRADO || catNiv.NIVEL as nivelTabulador,
                      tab.SUELDO_BASE,
                      tab.COMPENSACION_GARANTIZADA,
                      gpoJer.SGMM,
                      catUAdmin.DESCRIPCION
                    into 
                      nivelTab,
                      sdoBase,
                      compGarantizada,
                      nivTabHom,
                      descUniAdmin
                    from 
                      RH_NOMN_EMPLEADOS emp
                    inner join 
                      RH_NOMN_PLAZAS plz ON plz.ID_PLAZA = emp.ID_PLAZA
                    inner join 
                      RH_NOMN_TABULADORES tab ON plz.ID_TABULADOR_FK = tab.ID_TABULADOR
                    inner join RH_NOMN_CAT_GRUPOS_JERARQUICOS gpoJer on tab.ID_GRUPO_JERARQUICO_FK = gpoJer.ID_GRUPO_JERARQUICO
                    inner join RH_NOMN_CAT_GRADOS_JERARQUICOS grdsJer on grdsJer.ID_GRADO_JERARQUICO = tab.ID_GRADO_JERARQUICO_FK
                    inner join RH_NOMN_CAT_NIVELES_TABULADOR catNiv on catNiv.ID_NIVEL_TABULADOR = tab.ID_NIVEL_TABULADOR_FK
                    inner join RH_NOMN_ADSCRIPCIONES ads ON plz.ID_ADSCRIPCION_FK = ads.ID_ADSCRIPCION
                    inner join RH_NOMN_CAT_UNIDADES_ADMINS catUAdmin on catUAdmin.ID_UNIADM = ads.ID_UNIADM_FK
                    
                    WHERE emp.ID_EMPLEADO = emp_ID_EMPLEADO;      
            
              ---------------------------------------------   
              -- INSERTAMOS EN TABLA REGISTRO EMPLEADO
              --------------------------------------------- 
              --DBMS_OUTPUT.PUT_LINE('----------- INSERTA----------');
             
                 EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' 
                 || ' ''' ||     dp_A_PATERNO                               --1          
                 || ''',''' ||   dp_A_MATERNO                               --2
                 || ''',''' ||   dp_NOMBRE                                  --3 
                 || ''',''' ||   dp_RFC                                     --4
                 || ''',''' ||   dp_CURP                                    --5
                 || ''',''' ||   TO_CHAR(dp_fecha_nacimiento, 'yyyyMMdd')   --6
                 || ''',''' ||   dp_descSexo                                --7    
                    
                 || ''',''' ||   nivelTab                                   --8  
                 || ''',''' ||   nivTabHom                                  --9
                 || ''',''' ||   nivelTabNvo                                  --10  
                 || ''',''' ||   descUniAdmin                               --11
                 
                 || ''',''' ||   round(( sdoBase + compGarantizada ),2)     --12
                 || ''',''' ||   compGarantizada                            --13
                 || ''',''' ||   NULL                                       --14 SE DEBE OBTENER DEL REPORTE 1.4.1
                 || ''',''' ||   round(( sdoBase + compGarantizada ),2)     --15
                 || ''',''' ||   'B�sica'                                   --16
                 || ''',''' ||   descTipoMov                                --17
                 || ''',''' ||   to_char(bhm_FECHA_MOV, 'yyyyMMdd')         --18
                 || ''',''' ||   fechaHasta --'NA-FECHAS EN CASO ALTA'      --19  
                 || ''',''' ||   numQuinAplicar                             --20
                 || ''',''' ||   porcPrimaBasica                            --21
                 
                 || ''',''' ||   NULL                                       --22 N/A YA QUE ES SOLO CUANTO POTENCIA EL TRABAJOR
                 || ''',''' ||   NULL                                       --23 N/A YA QUE ES SOLO CUANTO POTENCIA EL TRABAJOR
                 || ''',''' ||   round(( (sdoBase + compGarantizada) * porcPrimaBasica * numQuinAplicar ),2)      --24
                 || ''',''' ||   round(( (sdoBase + compGarantizada) * 1 * numQuinAplicar ),2)                    --25    
                 || ''',''' ||   ( round(( (sdoBase + compGarantizada) * porcPrimaBasica * numQuinAplicar ),2)   +    round(( (sdoBase + compGarantizada) * 1 * numQuinAplicar ),2)   ) --26
                 || ''',''' ||   'NA'                                    --27
                 || ''',''' ||   'NA'                                    --28  
                 || ''' )'; 
              end if;  
               
           end if;   
        
    END LOOP;  
    CLOSE cursorAltas;
    
    
    --////////////////////////////////////////////////////////////////////////////
    --////////////////////////////////////////////////////////////////////////////
    --////////////////////////////////////////////////////////////////////////////
    --////////////////////////////////////////////////////////////////////////////
    
---------------------------------------------------------------------------------------
---  O B T E N E M O S    L I C E N C I A S   M � D I C A S 
---------------------------------------------------------------------------------------

  OPEN cursorLicencias;
    LOOP
      FETCH cursorLicencias INTO
          C2_ID_LIC_MED,
          C2_ID_EMPLEADO_FK,
          C2_FECHA_INI,
          C2_FECHA_FIN 
          
      ;
      EXIT WHEN cursorLicencias%NOTFOUND;
      
      
    --DBMS_OUTPUT.PUT_LINE('---------------------------- C2_ID_LIC_MED-->' || C2_ID_LIC_MED ||  '       ' || C2_ID_EMPLEADO_FK ||  '    '|| C2_FECHA_INI ||  '     ' || C2_FECHA_FIN);
      fechaHasta := 'NA';
      descTipoMov := 'LICENCIA MEDICA';
      nivelTabNvo := '';
      numQuinAplicar := 0;
      flagInserta := 0;
      
      -------------------------------------------------------------------------------
      -- D A T O S   G E N E R A L E S   D E L   E M P L E A D O 
      -------------------------------------------------------------------------------
      
          
          execute immediate '
                select         
                    emp.ID_EMPLEADO,
                    emp.ID_DATOS_PERSONALES,
                    emp.ID_PLAZA,
                    emp.NUMERO_EMPLEADO,
                    emp.ID_ESTATUS_EMPLEADO,
                    
                    dp.NOMBRE as nombre,
                    dp.A_PATERNO as apPat,
                    dp.A_MATERNO as apMat,
                    dp.CURP,
                    dp.RFC,            
                    dp.FECHA_NACIMIENTO,
                    dp.ID_GENERO 
                
                from
                    RH_NOMN_EMPLEADOS emp 
                inner join 
                    RH_NOMN_DATOS_PERSONALES dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
                where                                    
                      emp.ID_EMPLEADO = ' || C2_ID_EMPLEADO_FK 
                
                into 
                    emp_ID_EMPLEADO,
                    emp_ID_DATOS_PERSONALES,
                    emp_ID_PLAZA,
                    emp_NUMERO_EMPLEADO,   
                    emp_ID_ESTATUS_EMPLEADO,
                    
                    dp_NOMBRE ,
                    dp_A_PATERNO ,
                    dp_A_MATERNO ,
                    dp_CURP,
                    dp_RFC,
                    dp_fecha_nacimiento,
                    dp_sexo 
                ;                
                      
                dp_descSexo :=  'F'; 
                  if (dp_sexo = 1)then  dp_descSexo :=  'M';   end if;
                      
            ---  N I V E L   T A B U L A R
                select 
                  gpoJer.CLAVE_JERARQUICA || grdsJer.GRADO || catNiv.NIVEL as nivelTabulador,
                  tab.SUELDO_BASE,
                  tab.COMPENSACION_GARANTIZADA,
                  gpoJer.SGMM,
                  catUAdmin.DESCRIPCION
                into 
                  nivelTab,
                  sdoBase,
                  compGarantizada,
                  nivTabHom,
                  descUniAdmin
                from 
                  RH_NOMN_EMPLEADOS emp
                inner join 
                  RH_NOMN_PLAZAS plz ON plz.ID_PLAZA = emp.ID_PLAZA
                inner join 
                  RH_NOMN_TABULADORES tab ON plz.ID_TABULADOR_FK = tab.ID_TABULADOR
                inner join RH_NOMN_CAT_GRUPOS_JERARQUICOS gpoJer on tab.ID_GRUPO_JERARQUICO_FK = gpoJer.ID_GRUPO_JERARQUICO
                inner join RH_NOMN_CAT_GRADOS_JERARQUICOS grdsJer on grdsJer.ID_GRADO_JERARQUICO = tab.ID_GRADO_JERARQUICO_FK
                inner join RH_NOMN_CAT_NIVELES_TABULADOR catNiv on catNiv.ID_NIVEL_TABULADOR = tab.ID_NIVEL_TABULADOR_FK
                inner join RH_NOMN_ADSCRIPCIONES ads ON plz.ID_ADSCRIPCION_FK = ads.ID_ADSCRIPCION
                inner join RH_NOMN_CAT_UNIDADES_ADMINS catUAdmin on catUAdmin.ID_UNIADM = ads.ID_UNIADM_FK
                
                WHERE emp.ID_EMPLEADO = emp_ID_EMPLEADO;   
           
     -------------------------------------------------------------
     --- N � M E R O   D E   Q U I N C E N A S   A P L I C A R
     -------------------------------------------------------------
     
     -- I N I C I O   D E   L I C E N C I A --  
     
        if ( TRUNC(C2_FECHA_INI) BETWEEN  trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN))  then
        
            --DBMS_OUTPUT.PUT_LINE('-----------  I N I C I O   L I C  ----------');
             flagInserta := 1;            
                        
            select  NUMERO_QUINCENA
            into    numQuinIniCalc 
            from    RH_NOMN_CAT_QUINCENAS
            where   TRUNC(C2_FECHA_INI) BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;
            
        -- OBTENER numQuinFinCalc con la FECHA FIN de la licencia, si no hay se toma el NUMQUINC del FIN trimestre
        
            if( (C2_FECHA_FIN IS NOT NULL) AND ( C2_FECHA_FIN BETWEEN  trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN) ) )THEN
                    
                  select to_char(C2_FECHA_FIN, 'yyyyMMdd'  ) into   fechaHasta from dual;
                  
                  select  NUMERO_QUINCENA
                  into    numQuinFinCalc 
                  from    RH_NOMN_CAT_QUINCENAS
                  where   trunc(C2_FECHA_FIN) BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;                
              
            else
                 numQuinFinCalc := idQuincenaFin ;  
            end if;
        
        
        else if(TRUNC(C2_FECHA_FIN) BETWEEN  trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN)) THEN
          --DBMS_OUTPUT.PUT_LINE('-----------  F I N   L I C  ----------');
          flagInserta := 1;
          
          select  NUMERO_QUINCENA
            into    numQuinFinCalc 
            from    RH_NOMN_CAT_QUINCENAS
            where   TRUNC(C2_FECHA_FIN) BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;
            
            
            -- OBTENER numQuinINICalc con la FECHA INI de la licencia, si no hay se toma el NUMQUINC del INI trimestre
            
            IF((C2_FECHA_INI IS NOT NULL) AND ( C2_FECHA_INI BETWEEN  trunc(fechaTrimestreINI) and trunc(fechaTrimestreFIN) ) )THEN
              select  NUMERO_QUINCENA
              into    numQuinIniCalc 
              from    RH_NOMN_CAT_QUINCENAS
              where   trunc(C2_FECHA_INI) BETWEEN trunc(FECHA_INICIO_QUINCENA) and trunc(FECHA_FIN_QUINCENA) and rownum = 1;                
          
            ELSE
              numQuinIniCalc := idQuincenaIni ;  
            END IF;
            
        end if;
        end if;
        
        numQuinAplicar := (numQuinFinCalc - numQuinIniCalc) + 1;

 
    
    
       if( flagInserta = 1)then
       
             
            
              ---------------------------------------------   
              -- INSERTAMOS EN TABLA REGISTRO EMPLEADO
              --------------------------------------------- 
              --DBMS_OUTPUT.PUT_LINE('----------- INSERTA----------');
             
                 EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' 
                 || ' ''' ||     dp_A_PATERNO                               --1          
                 || ''',''' ||   dp_A_MATERNO                               --2
                 || ''',''' ||   dp_NOMBRE                                  --3 
                 || ''',''' ||   dp_RFC                                     --4
                 || ''',''' ||   dp_CURP                                    --5
                 || ''',''' ||   TO_CHAR(dp_fecha_nacimiento, 'yyyyMMdd')   --6
                 || ''',''' ||   dp_descSexo                                --7    
                    
                 || ''',''' ||   nivelTab                                   --8  
                 || ''',''' ||   nivTabHom                                  --9
                 || ''',''' ||   nivelTabNvo                                   --10  
                 || ''',''' ||   descUniAdmin                               --11
                 
                 || ''',''' ||   round(( sdoBase + compGarantizada ),2)     --12
                 || ''',''' ||   compGarantizada                            --13
                 || ''',''' ||   NULL                                       --14 SE DEBE OBTENER DEL REPORTE 1.4.1
                 || ''',''' ||   round(( sdoBase + compGarantizada ),2)     --15
                 || ''',''' ||   'B�sica'                                   --16
                 || ''',''' ||   descTipoMov                                --17
                 || ''',''' ||   to_char(bhm_FECHA_MOV, 'yyyyMMdd')         --18
                 || ''',''' ||   fechaHasta --'NA-FECHAS EN CASO ALTA'      --19  
                 || ''',''' ||   numQuinAplicar                             --20
                 || ''',''' ||   porcPrimaBasica                            --21
                 
                 || ''',''' ||   NULL                                       --22 N/A YA QUE ES SOLO CUANTO POTENCIA EL TRABAJOR
                 || ''',''' ||   NULL                                       --23 N/A YA QUE ES SOLO CUANTO POTENCIA EL TRABAJOR
                 || ''',''' ||   round(( (sdoBase + compGarantizada) * porcPrimaBasica * numQuinAplicar ),2)      --24
                 || ''',''' ||   round(( (sdoBase + compGarantizada) * 1 * numQuinAplicar ),2)                    --25    
                 || ''',''' ||   ( round(( (sdoBase + compGarantizada) * porcPrimaBasica * numQuinAplicar ),2)   +    round(( (sdoBase + compGarantizada) * 1 * numQuinAplicar ),2)   ) --26
                 || ''',''' ||   'NA'                                    --27
                 || ''',''' ||   'NA'                                    --28  
                 || ''' )'; 
             
               
           end if;   
        
    END LOOP;  
    CLOSE cursorLicencias;
    
    vQUERY := 'select  
                * 
              from ' || tablatmp || '                 
                order by ap,am, nombre, FECHA_INI_MOVNTO, TIPO_MOVMTO';
    
    OPEN DATOS FOR vQUERY;
    
    EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
    
      
  
EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp 
      AND OWNER = 'SERP';
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
  DATOS := null;    
  
  SPDERRORES('REPSEGVID_AJUSTEPRIMBAS',err_code,err_msg,''); 


  
END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_FONAC_CONCENTRADO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_FONAC_CONCENTRADO" (
                                                          idCiclo IN INTEGER,                                                          
                                                          REPORTE OUT SYS_REFCURSOR) 
IS
operA number(12,2);
qryTbl varchar2(4500);
 ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
  
  tablatmp        VARCHAR2(300) ; --Para generar el nombre de la tabla temporar para crear
  existTable      integer := 0;
cntPorcRend integer;
C1 SYS_REFCURSOR;
C2 SYS_REFCURSOR;
C3 SYS_REFCURSOR;
qry1 varchar(3000);
qryPens varchar2(2000);
nivelTab varchar2(50);

ciclo_desc VARCHAR2(100 BYTE);
ciclo_contrato VARCHAR2(20 BYTE);
ciclo_vig_ini VARCHAR2(10 BYTE);
ciclo_vig_fin VARCHAR2(10 BYTE);

ciclo_idQuinIni integer;
ciclo_idQuinFin Integer;

cntReg integer := 1;
c1_ID_EMPLEADO_PADRON NUMBER(38,0);
c1_ID_FONAC_CICLO NUMBER(38,0);
c1_ID_CICLO_PERIODO  NUMBER(38,0);
c1_ID_PERIODO_INSCRIPCION NUMBER(38,0);
c1_ID_EMPLEADO NUMBER(38,0);
c1_alta_Fonac date;
c1_idBajaFonac NUMBER(38,0);
c1_sumPorcPensTrab number(10,2);

mtoAportEmp NUMBER(10,2);
mtoAportGobFedCap NUMBER(10,2);
mtoAportGobFedNoCap NUMBER(10,2);
sldoBase NUMBER(8,2);
compGarantizada NUMBER(8,2);
sumPorcPensionados NUMBER(10,2);
porcPensionado NUMBER(10,2);

cadenaPensDato1 varchar(4500);
cadenaPorcPensionado varchar2(4500);
strCadenaPens varchar2(4500);
strCadenaPens2 varchar2(4500);
porcPensEmpleado number(10,2);


cntDetBaja integer;
c2_idQuin NUMBER;
c2_fechaIni DATE;
c2_fechaFin DATE;
c2_idEjer NUMBER;
c2_numQuin NUMBER(2,0);

c3_ID_TIPO_DESCUENTO NUMBER;
c3_ID_DATOS_PERSONALES NUMBER; 
c3_ID_QUINCENA_INICIO NUMBER;
c3_ID_QUINCENA_FIN NUMBER;
c3_VALOR_PENSION NUMBER;
c3_PENS_NOMBRE varchar2(150);
c3_sumPorcPens number(10,2);

mtoPens number(10,2);

v_nombre varchar2(100); v_apellidoP varchar2(100); v_apellidoM varchar2(170);
descMotBaja VARCHAR2(50 BYTE);
fechaBaja date;

cntQuincenas integer :=0;

empleQuinIni integer;
empleQuinFin integer;

mtoActual numeric(9,2);
strCadena varchar2(4500);

peIns_quinIni integer; peIns_quinFin  integer;

sumAportTrab numeric(10,2);
sumGobFedCap numeric(10,2);
sumGobFedNoCap numeric(10,2);

rendPorc numeric(10,2); rendMes varchar2(20); rendAnio integer;
rendObt numeric(10,2);

cntPensiones integer;
idRegPadre integer;

BEGIN

tablatmp  := 'TMP_FONAC_R'  || '_'|| TO_CHAR(SYSDATE,'ddmmyy_hhmmss');

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp;
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        --DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
    
    ----------------------------------------------------------------------
    --  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
    ----------------------------------------------------------------------
     EXECUTE IMMEDIATE 'CREATE TABLE ' || tablatmp || '    
          (  
          IDCONS INTEGER,
          NIVEL_TAB VARCHAR2(200),
          NOMBRE VARCHAR2(200),
          INGRESO_FONAC DATE,
          RETRO NUMERIC(10,2),
          MOT_BAJA VARCHAR2(300),
          FECHA_BAJA DATE,
          PORC_TRAB NUMERIC(10,2),
          PORC_PENS NUMERIC(10,2),
          NOMBRE_PENS VARCHAR2(200),
          
          APORT_TRAB_1        NUMERIC(10,2),
          APOR_GOB_NO_CAP_1   NUMERIC(10,2),
          APOR_GOB_CAP_1      NUMERIC(10,2),
          TOTAL_1              NUMERIC(10,2),

          APORT_TRAB_2        NUMERIC(10,2),
          APOR_GOB_NO_CAP_2   NUMERIC(10,2),
          APOR_GOB_CAP_2      NUMERIC(10,2),
          TOTAL_2              NUMERIC(10,2),

          APORT_TRAB_3        NUMERIC(10,2),
          APOR_GOB_NO_CAP_3   NUMERIC(10,2),
          APOR_GOB_CAP_3      NUMERIC(10,2),
          TOTAL_3              NUMERIC(10,2),  
          
          APORT_TRAB_4        NUMERIC(10,2),
          APOR_GOB_NO_CAP_4   NUMERIC(10,2),
          APOR_GOB_CAP_4      NUMERIC(10,2),
          TOTAL_4             NUMERIC(10,2),
          
          APORT_TRAB_5        NUMERIC(10,2),
          APOR_GOB_NO_CAP_5   NUMERIC(10,2),
          APOR_GOB_CAP_5      NUMERIC(10,2),
          TOTAL_5              NUMERIC(10,2),
          
          APORT_TRAB_6        NUMERIC(10,2),
          APOR_GOB_NO_CAP_6   NUMERIC(10,2),
          APOR_GOB_CAP_6      NUMERIC(10,2),
          TOTAL_6             NUMERIC(10,2),
          
          APORT_TRAB_7        NUMERIC(10,2),
          APOR_GOB_NO_CAP_7   NUMERIC(10,2),
          APOR_GOB_CAP_7      NUMERIC(10,2),
          TOTAL_7             NUMERIC(10,2),
          
          APORT_TRAB_8        NUMERIC(10,2),
          APOR_GOB_NO_CAP_8   NUMERIC(10,2),
          APOR_GOB_CAP_8      NUMERIC(10,2),
          TOTAL_8             NUMERIC(10,2),
          
          APORT_TRAB_9       NUMERIC(10,2),
          APOR_GOB_NO_CAP_9  NUMERIC(10,2),
          APOR_GOB_CAP_9      NUMERIC(10,2),
          TOTAL_9             NUMERIC(10,2),
          
          APORT_TRAB_10        NUMERIC(10,2),
          APOR_GOB_NO_CAP_10   NUMERIC(10,2),
          APOR_GOB_CAP_10      NUMERIC(10,2),
          TOTAL_10             NUMERIC(10,2),
          
          APORT_TRAB_11        NUMERIC(10,2),
          APOR_GOB_NO_CAP_11   NUMERIC(10,2),
          APOR_GOB_CAP_11      NUMERIC(10,2),
          TOTAL_11             NUMERIC(10,2),
          
          APORT_TRAB_12        NUMERIC(10,2),
          APOR_GOB_NO_CAP_12   NUMERIC(10,2),
          APOR_GOB_CAP_12      NUMERIC(10,2),
          TOTAL_12             NUMERIC(10,2),
          
          APORT_TRAB_13       NUMERIC(10,2),
          APOR_GOB_NO_CAP_13   NUMERIC(10,2),
          APOR_GOB_CAP_13      NUMERIC(10,2),
          TOTAL_13             NUMERIC(10,2),
          
          APORT_TRAB_14        NUMERIC(10,2),
          APOR_GOB_NO_CAP_14   NUMERIC(10,2),
          APOR_GOB_CAP_14      NUMERIC(10,2),
          TOTAL_14             NUMERIC(10,2),
          
          APORT_TRAB_15        NUMERIC(10,2),
          APOR_GOB_NO_CAP_15   NUMERIC(10,2),
          APOR_GOB_CAP_15      NUMERIC(10,2),
          TOTAL_15             NUMERIC(10,2),
          
          APORT_TRAB_16        NUMERIC(10,2),
          APOR_GOB_NO_CAP_16   NUMERIC(10,2),
          APOR_GOB_CAP_16      NUMERIC(10,2),
          TOTAL_16             NUMERIC(10,2),
          
          APORT_TRAB_17        NUMERIC(10,2),
          APOR_GOB_NO_CAP_17   NUMERIC(10,2),
          APOR_GOB_CAP_17      NUMERIC(10,2),
          TOTAL_17             NUMERIC(10,2),
          
         
          
          APORT_TRAB_18        NUMERIC(10,2),
          APOR_GOB_NO_CAP_18   NUMERIC(10,2),
          APOR_GOB_CAP_18      NUMERIC(10,2),
          TOTAL_18             NUMERIC(10,2),
          
          APORT_TRAB_19       NUMERIC(10,2),
          APOR_GOB_NO_CAP_19   NUMERIC(10,2),
          APOR_GOB_CAP_19      NUMERIC(10,2),
          TOTAL_19             NUMERIC(10,2),
          
          APORT_TRAB_20        NUMERIC(10,2),
          APOR_GOB_NO_CAP_20   NUMERIC(10,2),
          APOR_GOB_CAP_20      NUMERIC(10,2),
          TOTAL_20             NUMERIC(10,2),
          
          APORT_TRAB_21       NUMERIC(10,2),
          APOR_GOB_NO_CAP_21   NUMERIC(10,2),
          APOR_GOB_CAP_21      NUMERIC(10,2),
          TOTAL_21             NUMERIC(10,2),
          
          APORT_TRAB_22        NUMERIC(10,2),
          APOR_GOB_NO_CAP_22   NUMERIC(10,2),
          APOR_GOB_CAP_22      NUMERIC(10,2),
          TOTAL_22             NUMERIC(10,2),
          
          APORT_TRAB_23       NUMERIC(10,2),
          APOR_GOB_NO_CAP_23   NUMERIC(10,2),
          APOR_GOB_CAP_23      NUMERIC(10,2),
          TOTAL_23             NUMERIC(10,2),
          
          APORT_TRAB_24        NUMERIC(10,2),
          APOR_GOB_NO_CAP_24   NUMERIC(10,2),
          APOR_GOB_CAP_24      NUMERIC(10,2),
          TOTAL_24             NUMERIC(10,2),          
          
          
          
          SUM_APORT_TRAB       NUMERIC(10,2),
          SUM_APOR_GOB_NO_CAP   NUMERIC(10,2),
          SUM_APOR_GOB_CAP     NUMERIC(10,2),
          SUM_TOTAL             NUMERIC(10,2),
          
          MES_FACT VARCHAR2(1250),
          CANT     NUMERIC(10,2),
          
          TOT_APORT NUMERIC(10,2),
          REND_OBT  NUMERIC(10,2),
          TOTAL_REND NUMERIC(10,2),
          
          PENS_GOB_NO_CAP_TRAB NUMERIC(10,2),
          PENS_GOB_NO_CAP_PENS NUMERIC(10,2),
          REND_TRAB             NUMERIC(10,2),
          REND_PENS             NUMERIC(10,2),
          
          CHEQUE_TRANS_TRAB NUMERIC(10,2),
          CHEQUE_TRANS_PENS NUMERIC(10,2),
          VALIDADOR         NUMERIC(10,2)
          
       )';
     
      

-- btenemos datos del ciclo, quincena inicio y quincena 
  select 
      DESCRIPCION_CICLO,
      CONTRATO,
      TO_DATE (VIGENCIA_INICIO, 'dd/MM/yy') as VIGENCIA_INICIO,
      TO_DATE (VIGENCIA_FIN, 'dd/MM/yy') as VIGENCIA_FIN
  into
      ciclo_desc,
      ciclo_contrato,
      ciclo_vig_ini,
      ciclo_vig_fin
  from 
      rh_nomn_fonac_ciclos
  where 
      ID_FONAC_CICLO = idCiclo;
      
-- Obtenemos los idQuincena de kla fecha inicio y fin del ciclo, estos se usar�n com ranngo de quincenas a recorrer
  select ID_QUINCENA into ciclo_idQuinIni from RH_NOMN_CAT_QUINCENAS where ciclo_vig_ini between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA and ACTIVO = 1;
  select ID_QUINCENA into ciclo_idQuinFin from RH_NOMN_CAT_QUINCENAS where ciclo_vig_fin between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA and ACTIVO = 1;
  
qry1 := ' select 
            
            aso.ID_EMPLEADO_PADRON,
            aso.ID_FONAC_CICLO,
            aso.ID_CICLO_PERIODO,
            aso.ID_PERIODO_INSCRIPCION,
            
            ep.id_empleado,
            to_date(TO_CHAR(ep.FECHA_INSCRIPCION_ALTA, ''dd/MM/yyyy''), ''dd/MM/yyyy'') as FECHA_INSCRIPCION_ALTA,
            ep.ID_BAJA_FONAC,
            
            apor.APORTACION_EMPLEADO,
            apor.APORTACION_GOB_FED_CAP,
            apor.APORTACION_GOB_FED_NO_CAP,
            
            tab.SUELDO_BASE,
            tab.COMPENSACION_GARANTIZADA,
            
            nvl((SELECT  SUM(CASE 
                                        WHEN p.ID_TIPO_DESCUENTO = 1 THEN ( (p.VALOR_PENSION*100)/(tab.SUELDO_BASE+tab.COMPENSACION_GARANTIZADA))
                                        WHEN p.ID_TIPO_DESCUENTO = 2 THEN (  p.VALOR_PENSION)                     
                                        WHEN p.ID_TIPO_DESCUENTO = 3 THEN (  p.VALOR_PENSION) 
                                        END )
                                from RH_NOMN_PENSIONES p
                                where p.id_empleado = ep.id_empleado
                                ), 0) as sumPorcPens
            
          from 
            rh_nomn_fonac_aso_emp_pad_cic aso
          inner join rh_nomn_fonac_empleados_padron ep on aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON  
          inner join rh_nomn_fonac_aportaciones apor on ep.ID_FONAC_APORTACION = apor.ID_FONAC_APORTACION
          inner join rh_nomn_empleados emp on ep.ID_EMPLEADO = emp.ID_EMPLEADO
          inner join rh_nomn_plazas pl on pl.ID_PLAZA = emp.ID_PLAZA
          inner join rh_nomn_tabuladores tab on pl.ID_TABULADOR_FK = tab.ID_TABULADOR
          where
            aso.ID_FONAC_CICLO = ' || idCiclo || ' 
            order by aso.ID_EMPLEADO_PADRON';
            
          


OPEN C1 FOR qry1;
      LOOP  
        FETCH C1 INTO 
            
            c1_ID_EMPLEADO_PADRON,
            c1_ID_FONAC_CICLO,
            c1_ID_CICLO_PERIODO,
            c1_ID_PERIODO_INSCRIPCION,
            c1_id_empleado,
            c1_alta_Fonac,
            c1_idBajaFonac,
            mtoAportEmp,
            mtoAportGobFedCap,
            mtoAportGobFedNoCap,
            sldoBase,
            compGarantizada,
            c1_sumPorcPensTrab;
        EXIT
            WHEN C1%NOTFOUND;  
            
            --DBMS_OUTPUT.PUT_LINE('    ---- c1_ID_EMPLEADO_PADRON ----> ' || c1_ID_EMPLEADO_PADRON || '  ' || c1_id_empleado);
            
            sumAportTrab   := 0.0; 
            sumGobFedCap   := 0.0 ;
            sumGobFedNoCap := 0.0;
            strCadenaPens := '';
            
            -- Obtenemos datos del empleado
            select            
              dp.nombre,  dp.A_PATERNO,   dp.A_MATERNO 
            into v_nombre, v_apellidoP, v_apellidoM 
            from rh_nomn_empleados emp 
            inner join rh_nomn_datos_personales dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES 
            where id_empleado = c1_id_empleado;
            
            -- NIVEL TABULAR
            
              
              select 
                /*emp.ID_PLAZA,  
                plz.ID_TABULADOR_FK,
                tab.ID_GRUPO_JERARQUICO_FK,
                tab.ID_GRADO_JERARQUICO_FK,
                tab.ID_NIVEL_TABULADOR_FK,*/
                
                gpoJer.CLAVE_JERARQUICA || grdsJer.GRADO || catNiv.NIVEL as nivelTabulador
              into 
                nivelTab
              from 
                RH_NOMN_EMPLEADOS emp
              inner join 
                RH_NOMN_PLAZAS plz ON plz.ID_PLAZA = emp.ID_PLAZA
              inner join 
                RH_NOMN_TABULADORES tab ON plz.ID_TABULADOR_FK = tab.ID_TABULADOR
              inner join RH_NOMN_CAT_GRUPOS_JERARQUICOS gpoJer on tab.ID_GRUPO_JERARQUICO_FK = gpoJer.ID_GRUPO_JERARQUICO
              inner join RH_NOMN_CAT_GRADOS_JERARQUICOS grdsJer on grdsJer.ID_GRADO_JERARQUICO = tab.ID_GRADO_JERARQUICO_FK
              inner join RH_NOMN_CAT_NIVELES_TABULADOR catNiv on catNiv.ID_NIVEL_TABULADOR = tab.ID_NIVEL_TABULADOR_FK
              
              WHERE emp.ID_EMPLEADO = c1_id_empleado;
              

            
            -- obtenemos IDquincena alta del empleado
                select id_quincena into empleQuinIni from rh_nomn_cat_quincenas where activo = 1 and c1_alta_Fonac between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA ;
            
            -- OBTENEMOS idQuincenaBaja, FECHA Y MOTIVO DE BAJA, MES Y CANTIDAD SI ES QUE TIENE
              descMotBaja := '';
              fechaBaja := null;
              rendPorc := 0.0; rendMes := ''; rendAnio := null;
              empleQuinFin := ciclo_idQuinFin;
              
              
              if( c1_idBajaFonac is not null )then
              
                select DESCRIPCION_MOTIVO_BAJA  into descMotBaja 
                from RH_NOMN_CAT_MOTIVOS_BAJA where ID_MOTIVO_BAJA = (select ID_MOTIVO_BAJA_FK from rh_nomn_fonac_det_baja where ID_BAJA_FONAC = c1_idBajaFonac);
                
                
                select count(db.FECHA_INSCRIPCION_BAJA) 
                into cntDetBaja
                from rh_nomn_fonac_det_baja db                 
                where db.ID_BAJA_FONAC = c1_idBajaFonac;
                
                 if(cntDetBaja >0)then 
                 
                    select to_date( to_char(db.FECHA_INSCRIPCION_BAJA, 'dd/MM/yyyy'), 'dd/MM/yyyy')   into fechaBaja from rh_nomn_fonac_det_baja db where db.ID_BAJA_FONAC = c1_idBajaFonac;
                 
                    select count(db.porcentaje_rendimiento)into cntPorcRend from rh_nomn_fonac_det_baja db where db.ID_BAJA_FONAC = c1_idBajaFonac;
                    if(cntPorcRend > 0)then
                        select                       
                          nvl(db.porcentaje_rendimiento, 0.0) as porc,
                          nvl(case when ren.id_mes = 1 then 'Ene' when ren.id_mes = 2 then 'Feb' when ren.id_mes = 3 then 'Mar' when ren.id_mes = 4 then 'Abr' when ren.id_mes = 5 then 'May' when ren.id_mes = 6 then 'Jun' when ren.id_mes = 7 then 'Jul' when ren.id_mes = 8 then 'Ago' when ren.id_mes = 9 then 'Sep' when ren.id_mes = 10 then 'Oct' when ren.id_mes = 11 then 'Nov'  else 'Dic' end , 'pend cheque') as mes,
                          nvl(ej.valor, '') as val
                        into  rendPorc, rendMes, rendAnio
                        from rh_nomn_fonac_det_baja db 
                        inner join rh_nomn_fonac_rendimientos ren on db.ID_RENDIMIENTO = ren.ID_RENDIMIENTO
                        inner join rh_nomn_cat_ejercicios ej on ren.id_ejercicio = ej.id_ejercicio
                        where db.ID_BAJA_FONAC = c1_idBajaFonac;
                    end if;
                    
                    select id_quincena into empleQuinFin from rh_nomn_cat_quincenas where activo = 1 and fechaBaja between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA ;
                  
                 end if;
              end if;
          
          -- obtenemos quinenas de los peridos de inscripcion
            select ID_QUINCENA_INICIO, ID_QUINCENA_FIN into peIns_quinIni, peIns_quinFin from RH_NOMN_FONAC_PE_INSCRIPCIONES where ID_PERIODO_INSCRIPCION = c1_ID_PERIODO_INSCRIPCION;
              
           -- obtenenmos montos de las aportaciones
              mtoActual := mtoAportEmp;
              
              strCadena :=  cntReg ||  ',''' ||  nivelTab || ''',''' || v_apellidoP || ' ' ||  v_apellidoM || ' ' || v_nombre || ''',''' || c1_alta_Fonac || ''', 0.0, ''' || descMotBaja || ''', ''' || fechaBaja || ''', null, null, null,';
              cadenaPensDato1 := '''' ||  v_apellidoP || ' ' ||  v_apellidoM || ' ' || v_nombre || ''',''' || c1_alta_Fonac || ''', 0.0, ''' || descMotBaja || ''', ''' || fechaBaja || ''', ';
              
            OPEN C2 FOR 
              SELECT 
                ID_QUINCENA,                
                FECHA_INICIO_QUINCENA,
                FECHA_FIN_QUINCENA,                
                  ID_EJERCICIO_FK,
                NUMERO_QUINCENA
              FROM 
                RH_NOMN_CAT_QUINCENAS
              WHERE 
                ID_QUINCENA >= ciclo_idQuinIni and ID_QUINCENA <= ciclo_idQuinFin 
                and activo = 1 
              order by id_quincena;            
            LOOP
              FETCH C2 INTO
                c2_idQuin,
                c2_fechaIni,
                c2_fechaFin,
                c2_idEjer,
                c2_numQuin;              
               EXIT
               WHEN C2%NOTFOUND;
               
                  
                  -- validamos si en la quincena actual el empleado esta dado de alta
                    if( c2_idQuin >= empleQuinIni AND c2_idQuin <= empleQuinFin )then
                    
                      -- Vaidamos si tiene que aplicarse retroactividad por alta en la segunda quincena
                        if( c2_idQuin  = peIns_quinFin and   empleQuinIni = peIns_quinFin ) then
                          mtoActual := mtoActual * 2;
                        else
                          mtoActual := mtoAportEmp;
                        end if;
                     
                      
                        strCadena     := strCadena     || mtoActual || ',' || (mtoActual * mtoAportGobFedCap ) ||  ',' || (mtoActual * mtoAportGobFedNoCap ) || ', ' ||( mtoActual + (mtoActual * mtoAportGobFedCap ) +  (mtoActual * mtoAportGobFedNoCap ) )  || ' ,' ;
                        strCadenaPens := strCadenaPens || mtoActual || ',' || (mtoActual * mtoAportGobFedCap ) ||  ',' || (mtoActual * mtoAportGobFedNoCap ) || ', ' ||( mtoActual + (mtoActual * mtoAportGobFedCap ) +  (mtoActual * mtoAportGobFedNoCap ) )  || ' ,' ;
                        
                        
                        
                       -- Sumamos los montos para los totales
                        sumAportTrab   := sumAportTrab   +  mtoActual; 
                        sumGobFedCap   := sumGobFedCap   + (mtoActual * mtoAportGobFedCap ) ;
                        sumGobFedNoCap := sumGobFedNoCap + (mtoActual * mtoAportGobFedNoCap );
                      
                    else
                      strCadena := strCadena || '0.0, 0.0 , 0.0, 0.0, ';
                      strCadenaPens := strCadenaPens || '0.0, 0.0 , 0.0, 0.0, '; 
                    end if;            
                              
            END LOOP;
            
            -- Concatenamos la Suma de los montos 
               
               strCadena := strCadena || sumAportTrab  || ',' || sumGobFedNoCap || ',' || sumGobFedCap ||  ',' || (sumAportTrab + sumGobFedNoCap + sumGobFedCap)  || ','''  || rendMes || '-'  || rendAnio || ''','  || rendPorc  || ',' ||  (sumAportTrab + sumGobFedNoCap) || ',' || ((sumAportTrab + sumGobFedNoCap) * rendPorc) || ',' || ( (sumAportTrab + sumGobFedNoCap) + ((sumAportTrab + sumGobFedNoCap) * rendPorc)  ) || ',';
               strCadena := strCadena || (c1_sumPorcPensTrab * sumGobFedNoCap) ||  ',0.0, ' || ( c1_sumPorcPensTrab * ((sumAportTrab + sumGobFedNoCap) * rendPorc) )  || ',0.0,' || ( sumAportTrab + (c1_sumPorcPensTrab * sumGobFedNoCap) +  ( c1_sumPorcPensTrab * ((sumAportTrab + sumGobFedNoCap) * rendPorc) ) ) || ',0.0,0.0' ;
               
               strCadenaPens2 :=  sumAportTrab  || ',' || sumGobFedNoCap || ',' || sumGobFedCap ||  ',' || (sumAportTrab + sumGobFedNoCap + sumGobFedCap)  || ','''  || rendMes || '-'  || rendAnio || ''','  || rendPorc  || ',' ||  (sumAportTrab + sumGobFedNoCap) || ',' || ((sumAportTrab + sumGobFedNoCap) * rendPorc) || ',' || ( (sumAportTrab + sumGobFedNoCap) + ((sumAportTrab + sumGobFedNoCap) * rendPorc)  ) || ',';
               
              
               
            ---------------------------------------------   
            -- INSERTAMOS EN TABLA REGISTRO EMPLEADO
            ---------------------------------------------
                
                EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' || strCadena || ')'; 
                
                
                
                
                execute immediate 'update '||tablatmp||' set validador = ( select ( (CHEQUE_TRANS_TRAB + CHEQUE_TRANS_PENS) - total_rend ) as sumer from '||tablatmp||' where idcons = ' || cntReg || ')  where idcons = ' || cntReg ;
                
                DBMS_OUTPUT.PUT_LINE(' cntReg->' || cntReg);
                idRegPadre := cntReg;
                cntReg := cntReg + 1;
            
            
            ---------------------------------------------   
            -- xxPENSIONADOS
            ---------------------------------------------
            
              


                porcPensEmpleado   :=  0.0;
                sumPorcPensionados := 0.0;
                
                select count(ID_PENSION) into cntPensiones from rh_nomn_pensiones where id_empleado = c1_id_empleado;
                
                
                if( cntPensiones > 0)then
                
                    --DBMS_OUTPUT.PUT_LINE('        empleQuinIni-->  ' || empleQuinIni);  
                
                    open  c3 for
                          select 
                            pen.ID_TIPO_DESCUENTO, pen.ID_DATOS_PERSONALES, pen.ID_QUINCENA_INICIO ,pen.ID_QUINCENA_FIN, pen.VALOR_PENSION,
                            dp.NOMBRE ||  ' '  || A_PATERNO || ' ' || A_MATERNO as nombre, 
                            nvl((SELECT  SUM(CASE 
                                        WHEN p.ID_TIPO_DESCUENTO = 1 THEN ( (p.VALOR_PENSION*100)/(sldoBase+compGarantizada))
                                        WHEN p.ID_TIPO_DESCUENTO = 2 THEN (  p.VALOR_PENSION)                     
                                        WHEN p.ID_TIPO_DESCUENTO = 3 THEN (  p.VALOR_PENSION) 
                                        END )
                                from RH_NOMN_PENSIONES p
                                where p.id_empleado = c1_id_empleado
                            ), 0) as sumPorcPens
                            
                            
                          from rh_nomn_pensiones pen
                          inner join rh_nomn_datos_personales dp on pen.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
                          where 
                                pen.id_empleado =  c1_id_empleado  
                                and pen.ID_QUINCENA_INICIO >= empleQuinIni;
                    LOOP
                      FETCH C3 INTO
                          c3_ID_TIPO_DESCUENTO, 
                          c3_ID_DATOS_PERSONALES, 
                          c3_ID_QUINCENA_INICIO ,
                          c3_ID_QUINCENA_FIN,
                          c3_VALOR_PENSION,
                          c3_PENS_NOMBRE,
                          c3_sumPorcPens;            
                       EXIT
                       WHEN C3%NOTFOUND; 
                       
                        cadenaPorcPensionado := '';
                          
                          porcPensEmpleado := 100;
                          porcPensionado := 0;

                        porcPensEmpleado := porcPensEmpleado - c3_sumPorcPens;
 
                       
                         if(c3_ID_TIPO_DESCUENTO = 1 )then

                            porcPensionado := (c3_VALOR_PENSION*100)/(sldoBase+compGarantizada);
                            
                          else if(c3_ID_TIPO_DESCUENTO = 2 or c3_ID_TIPO_DESCUENTO = 3 )then
                              
                            porcPensionado :=  c3_VALOR_PENSION ;  

                          end if;                                
                          end if;
                         
                         execute immediate 'select rend_obt from  ' || tablatmp || ' where IDcons = ' || idRegPadre into rendObt;  
                         
                         --DBMS_OUTPUT.PUT_LINE('        porcPensEmpleado-->  ' || porcPensEmpleado);  
                         --DBMS_OUTPUT.PUT_LINE('        rendObt-->  ' || rendObt);  
                         
                         cadenaPorcPensionado := cntReg || ',' || cadenaPensDato1 || porcPensEmpleado ||  ',' || porcPensionado ||  ','''  || c3_PENS_NOMBRE  || ''',' ||  strCadenaPens || strCadenaPens2 || '0.0,' || (porcPensionado * ( sumGobFedNoCap)) || ',0.0,' || (rendObt * porcPensionado) || ',0.0,'  ||( (rendObt * porcPensionado) + (porcPensionado * ( sumGobFedNoCap)) ) || ',0.0';
                         
                         --DBMS_OUTPUT.PUT_LINE('        cadenaPorcPensionado-->  ' || cadenaPorcPensionado);  
                          
                         EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' || cadenaPorcPensionado || ')'; 
                          cntReg := cntReg + 1;
                          
                    END LOOP;
                  
                  
                  
                end if;
            
      END LOOP;      

 qry1 := 'select * from '  || tablatmp;

open 
REPORTE for qry1;
            

END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_FONAC_PLAZAPRES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_FONAC_PLAZAPRES" (
                                                          idCiclo IN INTEGER,
                                                          idPeriodo in integer,
                                             REPORTE OUT SYS_REFCURSOR)
IS 

qry varchar2(4000);
 C1 SYS_REFCURSOR;
 qry1 varchar(3000);

  prm_ID_FONAC_CICLO NUMBER(38,0);
  prm_CICLO NUMBER(38,0);
  prm_DESCRIPCION_CICLO VARCHAR2(100 BYTE);
  prm_CONTRATO VARCHAR2(20 BYTE);
  prm_VIGENCIA_INICIO VARCHAR2(10 BYTE);
  prm_VIGENCIA_FIN VARCHAR2(10 BYTE);
  prm_ID_ESTATUS_FONAC_FK NUMBER(38,0);
  prm_FACT_PER_ORDINARIO NUMBER;
  prm_FACT_PER_EXTRAORDINARIO NUMBER;
  
  day_fi varchar2(5);
  num_mes_fi varchar2(3);
  month_fi varchar2(11);
  trim_month_fi varchar2(11);
  anio_fi varchar2(5);

  day_ff varchar2(5);
  num_mes_ff varchar2(3);
  month_ff varchar2(11);
  trim_month_ff varchar2(11);
  anio_ff varchar2(5);
  
  day_act varchar(3);
  month_act varchar(3);
  year_act varchar(3);
  
  C1_ID_EMPLEADO_PADRON  INTEGER;
  C1_FECHA_INSCRIPCION_ALTA  DATE;
  C1_ID_QUINCENA_FIN integer;
  C1_ID_QUINCENA_INICIO integer;
  C1_APORTACION_EMPLEADO NUMBER(10,2);
  C1_APORTACION_GOB_FED_CAP NUMBER(10,2);
  C1_APORTACION_GOB_FED_NO_CAP NUMBER(10,2);
  
  cntTrabInsc integer :=0;
  mtoAportInicial integer := 100;
  
  idQuincAlta integer;
  
  sumAportQuincTrab  NUMBER(10,2) := 0;
  sumAportQuincGobFedNC NUMBER(10,2) := 0;
  sumTotAport NUMBER(10,2) := 0;
  
  
BEGIN
---------------------------  
-- OBTENEMOS EL CICLO
---------------------------    
  select  
    ID_FONAC_CICLO,
    CICLO,
    DESCRIPCION_CICLO,
    CONTRATO,
    VIGENCIA_INICIO,
    VIGENCIA_FIN,
    ID_ESTATUS_FONAC_FK,
    FACT_PER_ORDINARIO,
    FACT_PER_EXTRAORDINARIO
  into
    prm_ID_FONAC_CICLO,
    prm_CICLO,
    prm_DESCRIPCION_CICLO,
    prm_CONTRATO,
    prm_VIGENCIA_INICIO,
    prm_VIGENCIA_FIN,
    prm_ID_ESTATUS_FONAC_FK,
    prm_FACT_PER_ORDINARIO,
    prm_FACT_PER_EXTRAORDINARIO
  from 
    rh_nomn_fonac_ciclos
  where id_fonac_ciclo =  idCiclo;
  
  if(prm_FACT_PER_ORDINARIO is null ) then
      prm_FACT_PER_ORDINARIO :=0;
  end if;
  
  if(prm_FACT_PER_EXTRAORDINARIO is null)then
    prm_FACT_PER_EXTRAORDINARIO:= 0;
  end if;  

---------------------------  
-- FECHA ACTUAL
---------------------------   
  -- DIA ACT
    SELECT TO_CHAR(SYSDATE,'dd') into day_act FROM DUAL; 
  -- MES ACT
    SELECT TO_CHAR(SYSDATE,'MM') into month_act FROM DUAL;   
  -- DIA ACT
    SELECT TO_CHAR(SYSDATE,'yy') into year_act FROM DUAL; 

---------------------------  
-- DESC FECHA INICIO CICLO
---------------------------    
  -- d�a
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'dd') into day_fi FROM DUAL; 
  -- mes
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'MM' ) into num_mes_fi FROM DUAL; 
    select to_char(to_date(to_char(num_mes_fi),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH') into month_fi from dual; 
    SELECT TRIM(month_fi) into trim_month_fi FROM DUAL; 
    select upper(trim_month_fi) into month_fi from dual;
  -- a�o
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'yy') into anio_fi FROM DUAL; 

---------------------------  
-- DESC FECHA FIN CICLO
---------------------------    
  -- d�a
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'dd') into day_ff FROM DUAL; 
  -- mes
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'MM' ) into num_mes_ff FROM DUAL; 
    select to_char(to_date(to_char(num_mes_ff),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH') into month_ff from dual; 
    SELECT TRIM(month_ff) into trim_month_ff FROM DUAL; 
    select upper(trim_month_ff) into month_ff from dual;
  -- a�o
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'yy') into anio_ff FROM DUAL; 

----------------------------
--- trabajadores inscritos
-----------------------------
  
  --- obtenemos cada registro, lo recorremos por cada uno btenemos su aportaci�n quincenal 
  --- y vamos sumando 
  
  qry1 := 'select 
            ep.ID_EMPLEADO_PADRON,
            
            ep.FECHA_INSCRIPCION_ALTA,
            
            pei.ID_QUINCENA_FIN,
            pei.ID_QUINCENA_INICIO,
            
            fa.APORTACION_EMPLEADO,
            fa.APORTACION_GOB_FED_CAP,
            fa.APORTACION_GOB_FED_NO_CAP
        
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_pe_inscripciones pei on pei.ID_CICLO_PERIODO = aso.ID_CICLO_PERIODO
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO  and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO    
        inner join 
            RH_NOMN_FONAC_APORTACIONES fa on fa.ID_FONAC_APORTACION = ep.ID_FONAC_APORTACION    
        where 
              aso.ID_FONAC_CICLO = ' || idCiclo || ' and cp.PERIODO_ORDINARIO = ' || idPeriodo || '
             and ep.id_baja_fonac is null
        order by 
            aso.id_empleado_padron
        ' ;
        
--DBMS_OUTPUT.PUT_LINE(' ------------qry--> ' || qry1);         

  OPEN C1 FOR qry1;
      LOOP  
      FETCH C1 INTO
        C1_ID_EMPLEADO_PADRON,
        C1_FECHA_INSCRIPCION_ALTA,
        C1_ID_QUINCENA_FIN,
        C1_ID_QUINCENA_INICIO,
        C1_APORTACION_EMPLEADO,
        C1_APORTACION_GOB_FED_CAP,
        C1_APORTACION_GOB_FED_NO_CAP
        ;
        EXIT
            WHEN C1%NOTFOUND; 
              -- se cuentan cuantos empleados recorremos
                 cntTrabInsc := cntTrabInsc +1;
              -- por cada empleado obtenemos si hay que aplicar retroactividad al inscribirse en el segundo periodo (quincena)
              -- esto para que se le cobre los del primero.
              -- obtenemos idQuincena d alta con la fecha de alta
                select ID_QUINCENA into idQuincAlta from rh_nomn_cat_quincenas where C1_FECHA_INSCRIPCION_ALTA between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA;
                 if(idQuincAlta = C1_ID_QUINCENA_FIN )then
                    -- el empleado se dio de alta en el segundo periodo(quincena) de inscripcion
                      C1_APORTACION_EMPLEADO := C1_APORTACION_EMPLEADO * 2;                       
                 end if;
                 
                 sumAportQuincTrab   := sumAportQuincTrab   + C1_APORTACION_EMPLEADO;
                 sumAportQuincGobFedNC := sumAportQuincGobFedNC + (C1_APORTACION_EMPLEADO * C1_APORTACION_GOB_FED_NO_CAP );
                 
      END LOOP;
      
        
        sumTotAport :=  (sumAportQuincTrab + sumAportQuincGobFedNC);
      
        mtoAportInicial  := cntTrabInsc * mtoAportInicial;
        
      
      
  
------------------------
--- CONCATENAMOS TODO
------------------------  
  qry :=  'select ' || 
            day_act || ' as diaAct, ' || month_act || ' as mesAct, ' || year_act  || ' as anioAct,' ||
            
            '  ''PERIODO ORDINARIO DEL ' || day_fi ||  ' DE ' || month_fi || ' DE 20' ||  anio_fi || '  AL ' || day_ff ||  ' DE ' || month_ff || ' DE 20' ||  anio_ff || ''' as descFechaCiclo, '||
            
            prm_ID_FONAC_CICLO || ' as idFonac,' ||
            
            prm_CICLO || ' as int_idCiclo,' ||
             ' ''' || prm_DESCRIPCION_CICLO || ''' as descCiclo,' ||
             ' ''' || prm_CONTRATO || ''' as contrato,' ||
             ' ''' || prm_VIGENCIA_INICIO || ''' as vigInicio,' ||
             ' ''' || prm_VIGENCIA_FIN || ''' as vigFin,' ||
            prm_FACT_PER_ORDINARIO || ' as factPerOrd,' ||
            prm_FACT_PER_EXTRAORDINARIO  || ' as factPerExt, '||
            cntTrabInsc || ' as trabInsc, ' ||
            mtoAportInicial || ' as aportInicial, '||
            sumAportQuincTrab || ' as aportQuincTrab, ' ||
            sumAportQuincGobFedNC || ' as aportQuincGobFed, ' ||
            sumTotAport || ' as sumTotAport,
            '' DEL ' || day_fi ||  ' DE ' || month_fi || ' DE 20' ||  anio_fi || '  AL ' || day_ff ||  ' DE ' || month_ff || ' DE 20' ||  anio_ff || ''' as descFechaCiclo2 
          from dual';
          
          
          DBMS_OUTPUT.PUT_LINE(' ------------qry--> ' || qry); 
       
        OPEN REPORTE FOR qry;
            

END REPORT_FONAC_PLAZAPRES;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_FONAC_QUINC_APORT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_FONAC_QUINC_APORT" (
                                                          idCiclo IN INTEGER,
                                                          idQuincena in integer,
                                                          idPeriodo in integer,
                                             REPORTE OUT SYS_REFCURSOR)
IS 

 qry varchar2(4000);
 C1 SYS_REFCURSOR;
 qry1 varchar(3000);
 
 -- DATOS DEL CICLO
  intCiclo integer;
  cntTotInscritos integer;
  cntTotBajas integer;
  cntBajasQuin integer;

 
  prm_ID_FONAC_CICLO NUMBER(38,0);
  prm_CICLO NUMBER(38,0);
  prm_DESCRIPCION_CICLO VARCHAR2(100 BYTE);
  prm_CONTRATO VARCHAR2(20 BYTE);
  prm_VIGENCIA_INICIO VARCHAR2(10 BYTE);
  prm_VIGENCIA_FIN VARCHAR2(10 BYTE);
  prm_ID_ESTATUS_FONAC_FK NUMBER(38,0);
  prm_FACT_PER_ORDINARIO NUMBER;
  prm_FACT_PER_EXTRAORDINARIO NUMBER;
 
-- FECHAS 
  day_fi varchar2(5);
  num_mes_fi varchar2(3);
  month_fi varchar2(11);
  trim_month_fi varchar2(11);
  anio_fi varchar2(5);

  day_ff varchar2(5);
  num_mes_ff varchar2(3);
  month_ff varchar2(11);
  trim_month_ff varchar2(11);
  anio_ff varchar2(5);
  
  day_act varchar(3);
  month_act varchar(3);
  year_act varchar(3);
 
-- DATOS QUINCENAS
  QI_ID_QUINCENA INTEGER; 
  QI_ID_EJERCICIO_FK  INTEGER;
  QI_NUMERO_QUINCENA INTEGER;
  
  QA_ID_QUINCENA INTEGER; 
  QA_ID_EJERCICIO_FK  INTEGER;
  QA_NUMERO_QUINCENA INTEGER;
  QA_FECHA_INICIO_QUINCENA DATE;
  QA_FECHA_FIN_QUINCENA DATE;
 
-- DATOS EMPLEADO PADRON 
  C1_ID_EMPLEADO_PADRON  INTEGER;
  C1_FECHA_INSCRIPCION_ALTA  DATE;
  C1_ID_QUINCENA_FIN integer;
  C1_ID_QUINCENA_INICIO integer;
  C1_APORTACION_EMPLEADO NUMBER(10,2);
  C1_APORTACION_GOB_FED_CAP NUMBER(10,2);
  C1_APORTACION_GOB_FED_NO_CAP NUMBER(10,2);
  c1_PERIODO_ORDINARIO integer;
 
 -- DATOS CALCULOS
  cntTrabInsc integer :=0;
  mtoAportInicial integer := 100;
  
  idQuincAlta integer;
  
  sumAportQuincTrab  NUMBER(10,2) := 0;
  sumAportQuincGobFedC NUMBER(10,2) := 0;
  sumAportQuincGobFedNC NUMBER(10,2) := 0;
  sumTotAport NUMBER(10,2) := 0;
  
  
  
BEGIN
---------------------------  
-- OBTENEMOS EL CICLO
---------------------------    
  select  
    ID_FONAC_CICLO,
    CICLO,
    DESCRIPCION_CICLO,
    CONTRATO,
    VIGENCIA_INICIO,
    VIGENCIA_FIN,
    ID_ESTATUS_FONAC_FK,
    FACT_PER_ORDINARIO,
    FACT_PER_EXTRAORDINARIO
  into
    prm_ID_FONAC_CICLO,
    prm_CICLO,
    prm_DESCRIPCION_CICLO,
    prm_CONTRATO,
    prm_VIGENCIA_INICIO,
    prm_VIGENCIA_FIN,
    prm_ID_ESTATUS_FONAC_FK,
    prm_FACT_PER_ORDINARIO,
    prm_FACT_PER_EXTRAORDINARIO
  from 
    rh_nomn_fonac_ciclos
  where id_fonac_ciclo =  idCiclo;
  
  if(prm_FACT_PER_ORDINARIO is null ) then
      prm_FACT_PER_ORDINARIO :=0;
  end if;
  
  if(prm_FACT_PER_EXTRAORDINARIO is null)then
    prm_FACT_PER_EXTRAORDINARIO:= 0;
  end if;  

---------------------------  
-- CICLO ACTUAL
---------------------------   
  
  
  
  -- Obtenemos los datos de la quincena al que corresponde el ciclito 1 del ciclo
      select ID_QUINCENA,  ID_EJERCICIO_FK, NUMERO_QUINCENA into QI_ID_QUINCENA, QI_ID_EJERCICIO_FK, QI_NUMERO_QUINCENA from rh_nomn_cat_quincenas where activo = 1 and fecha_inicio_quincena = to_date(prm_VIGENCIA_INICIO, 'dd/MM/yy');

  -- obtenemos el n�mero de quincena de la que se esta cosultando
      select ID_QUINCENA,  ID_EJERCICIO_FK, NUMERO_QUINCENA, FECHA_INICIO_QUINCENA, FECHA_FIN_QUINCENA  into QA_ID_QUINCENA, QA_ID_EJERCICIO_FK, QA_NUMERO_QUINCENA, QA_FECHA_INICIO_QUINCENA, QA_FECHA_FIN_QUINCENA  from rh_nomn_cat_quincenas where ID_QUINCENA = idQuincena;
      
  -- validamos si estan en el mismo a�o con el id_ejercicio
    
     if( QI_ID_EJERCICIO_FK = QA_ID_EJERCICIO_FK)then
        intCiclo := ( (QA_NUMERO_QUINCENA +1) - QI_NUMERO_QUINCENA  )  ;      
        --DBMS_OUTPUT.PUT_LINE(' -----actual anio --> ' || intCiclo ); 
     else
        intCiclo := ( QI_NUMERO_QUINCENA - QA_NUMERO_QUINCENA ) ;
       -- DBMS_OUTPUT.PUT_LINE(' -----ant  --> ' || (25 - QI_NUMERO_QUINCENA ) ); 
       -- DBMS_OUTPUT.PUT_LINE(' -----anrt --> ' || ( QA_NUMERO_QUINCENA) ); 
        
     end if;
  
---------------------------  
-- FECHA ACTUAL
---------------------------   
  -- DIA ACT
    SELECT TO_CHAR(SYSDATE,'dd') into day_act FROM DUAL; 
  -- MES ACT
    SELECT TO_CHAR(SYSDATE,'MM') into month_act FROM DUAL;   
  -- DIA ACT
    SELECT TO_CHAR(SYSDATE,'yy') into year_act FROM DUAL; 

---------------------------  
-- DESC FECHA INICIO CICLO
---------------------------    
  -- d�a
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'dd') into day_fi FROM DUAL; 
  -- mes
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'MM' ) into num_mes_fi FROM DUAL; 
    select to_char(to_date(to_char(num_mes_fi),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH') into month_fi from dual; 
    SELECT TRIM(month_fi) into trim_month_fi FROM DUAL; 
    select upper(trim_month_fi) into month_fi from dual;
  -- a�o
    SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'yy') into anio_fi FROM DUAL; 

---------------------------  
-- DESC FECHA FIN CICLO
---------------------------    
  -- d�a
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'dd') into day_ff FROM DUAL; 
  -- mes
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'MM' ) into num_mes_ff FROM DUAL; 
    select to_char(to_date(to_char(num_mes_ff),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH') into month_ff from dual; 
    SELECT TRIM(month_ff) into trim_month_ff FROM DUAL; 
    select upper(trim_month_ff) into month_ff from dual;
  -- a�o
    SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'yy') into anio_ff FROM DUAL; 
    

  
------------------------
--- obtenemos total inscritos, bajas en la quincena y bajas acumuladas
------------------------  
  --total inscritos
    select 
      count(aso.id_empleado_padron) 
    into
      cntTotInscritos
    from 
      rh_nomn_fonac_aso_emp_pad_cic aso
    inner join
      rh_nomn_fonac_empleados_padron ep on aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
    inner join 
      rh_nomn_fonac_ciclos_periodos cp on cp.ID_CICLO_PERIODO =  aso.ID_CICLO_PERIODO    
    where 
      aso.id_fonac_ciclo = idCiclo and cp.PERIODO_ORDINARIO = idPeriodo;
      
 -- bajas totales
    select 
      count(aso.id_empleado_padron) 
    into
      cntTotBajas
    from 
      rh_nomn_fonac_aso_emp_pad_cic aso
    inner join
      rh_nomn_fonac_empleados_padron ep on aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
    inner join 
      rh_nomn_fonac_ciclos_periodos cp on cp.ID_CICLO_PERIODO =  aso.ID_CICLO_PERIODO    
    where 
      aso.id_fonac_ciclo = idCiclo and cp.PERIODO_ORDINARIO = idPeriodo and  ep.ID_BAJA_FONAC is not null;
      
  -- bajas Por quincena
    select 
      count(aso.id_empleado_padron) 
    into
      cntBajasQuin
    from 
      rh_nomn_fonac_aso_emp_pad_cic aso
    inner join
      rh_nomn_fonac_empleados_padron ep on aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
    inner join 
      rh_nomn_fonac_ciclos_periodos cp on cp.ID_CICLO_PERIODO =  aso.ID_CICLO_PERIODO    
    inner join 
      rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC
    where 
      aso.id_fonac_ciclo = idCiclo and cp.PERIODO_ORDINARIO = idPeriodo and  ep.ID_BAJA_FONAC is not null
      and FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA and QA_FECHA_FIN_QUINCENA;
      

----------------------------
--- trabajadores inscritos
-----------------------------
  
  --- obtenemos cada registro, lo recorremos por cada uno btenemos su aportaci�n quincenal 
  --- y vamos sumando
  
  cntTrabInsc := 0;
  
  qry1 := 'select 
            ep.ID_EMPLEADO_PADRON,
            
            ep.FECHA_INSCRIPCION_ALTA,
            
            pei.ID_QUINCENA_FIN,
            pei.ID_QUINCENA_INICIO,
            
            fa.APORTACION_EMPLEADO,
            fa.APORTACION_GOB_FED_CAP,
            fa.APORTACION_GOB_FED_NO_CAP,
            
            cp.PERIODO_ORDINARIO
        
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_pe_inscripciones pei on pei.ID_CICLO_PERIODO = aso.ID_CICLO_PERIODO
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO  
        inner join 
            RH_NOMN_FONAC_APORTACIONES fa on fa.ID_FONAC_APORTACION = ep.ID_FONAC_APORTACION    
        where 
            aso.ID_FONAC_CICLO = ' || idCiclo || ' and cp.PERIODO_ORDINARIO = ' || idPeriodo || '
             and ep.id_baja_fonac is null
        order by 
            aso.id_empleado_padron
        ' ;

  OPEN C1 FOR qry1;
      LOOP  
      FETCH C1 INTO
        C1_ID_EMPLEADO_PADRON,
        C1_FECHA_INSCRIPCION_ALTA,
        C1_ID_QUINCENA_FIN,
        C1_ID_QUINCENA_INICIO,
        C1_APORTACION_EMPLEADO,
        C1_APORTACION_GOB_FED_CAP,
        C1_APORTACION_GOB_FED_NO_CAP,
        c1_PERIODO_ORDINARIO
        ;
        EXIT
            WHEN C1%NOTFOUND; 
              if(c1_PERIODO_ORDINARIO = idPeriodo)then
                -- se cuentan cuantos empleados recorremos
                   cntTrabInsc := cntTrabInsc +1;
                -- por cada empleado obtenemos si hay que aplicar retroactividad al inscribirse en el segundo periodo (quincena)
                -- esto para que se le cobre los del primero.
                -- obtenemos idQuincena d alta con la fecha de alta
                  select ID_QUINCENA into idQuincAlta from rh_nomn_cat_quincenas where C1_FECHA_INSCRIPCION_ALTA between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA;
                   if(idQuincAlta = C1_ID_QUINCENA_FIN )then
                      -- el empleado se dio de alta en el segundo periodo(quincena) de inscripcion
                        C1_APORTACION_EMPLEADO := C1_APORTACION_EMPLEADO * 2;                       
                   end if;
                   
                   sumAportQuincTrab   := sumAportQuincTrab   + C1_APORTACION_EMPLEADO;                 
                   sumAportQuincGobFedNC := sumAportQuincGobFedNC + (C1_APORTACION_EMPLEADO * C1_APORTACION_GOB_FED_NO_CAP );
                   sumAportQuincGobFedC  := sumAportQuincGobFedC + (C1_APORTACION_EMPLEADO * C1_APORTACION_GOB_FED_CAP );
              end if;       
                 
      END LOOP;
      
        
        sumTotAport :=  (sumAportQuincTrab + sumAportQuincGobFedNC + sumAportQuincGobFedC);
      
        mtoAportInicial  := cntTrabInsc * mtoAportInicial;
        

      
------------------------
--- CONCATENAMOS TODO
------------------------  
  qry :=  'select ' || 
            cntTotInscritos || ' as cntTotInsc, ' || 
            cntTotBajas     || ' as cntTotBajas, ' || 
            cntBajasQuin || ' as cntBajasQuin, ' || 
            QA_NUMERO_QUINCENA || ' as qnaAct, ' ||
            intCiclo || ' as cicloAct, ' ||
            '''20' || anio_fi || '-20' || anio_ff || ''' as aniosCiclo,' ||
            '''INICIO ' || prm_DESCRIPCION_CICLO || ' DE FONAC'' as desc_ciclo_enc, ' || 
            day_act || ' as diaAct, ' || month_act || ' as mesAct, 20' || year_act  || ' as anioAct,' ||
            '  ''PERIODO DEL ' || day_fi ||  ' DE ' || month_fi || ' DE 20' ||  anio_fi || '  AL ' || day_ff ||  ' DE ' || month_ff || ' DE 20' ||  anio_ff || ''' as descFechaCiclo, '||
            prm_ID_FONAC_CICLO || ' as idFonac,' ||
            prm_CICLO || ' as idCiclo,' ||
             ' ''' || prm_DESCRIPCION_CICLO || ''' as descCiclo,' ||
             ' ''' || prm_CONTRATO || ''' as contrato,' ||
             ' ''' || prm_VIGENCIA_INICIO || ''' as vigInicio,' ||
             ' ''' || prm_VIGENCIA_FIN || ''' as vigFin,' ||
            prm_FACT_PER_ORDINARIO || ' as factPerOrd,' ||
            prm_FACT_PER_EXTRAORDINARIO  || ' as factPerExt, '||
            cntTrabInsc || ' as trabInsc, ' ||
            mtoAportInicial || ' as aportInicial, '||
            sumAportQuincTrab || ' as aportQuincTrab, ' ||
            sumAportQuincGobFedNC || ' as aportQuincGobFedNC, ' ||
            sumAportQuincGobFedC || ' as aportQuincGobFedC, ' ||
            sumTotAport || ' as sumTotAport             
          from dual';
          
          
          DBMS_OUTPUT.PUT_LINE(' ------------qry--> ' || qry); 
       
        OPEN REPORTE FOR qry;
            


END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_FONAC_QUINC_DESINCOR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_FONAC_QUINC_DESINCOR" (
    idCiclo    IN INTEGER,
    idQuincena IN INTEGER,
    idPeriodo  IN INTEGER,
    REPORTE OUT SYS_REFCURSOR)
IS

  cntMot1 integer;
  cntMot2 integer;
  cntMot3 integer;
  cntMot4 integer;
  cntMot5 integer;
  cntMot7 integer;
  contTotal integer;

  qry VARCHAR2(4000);
  C1 SYS_REFCURSOR;
  qry1 VARCHAR(3000);
  -- DATOS DEL CICLO
  intCiclo                INTEGER;
  cntTotInscritos         INTEGER;
  cntTotBajas             INTEGER;
  cntBajasQuin            INTEGER;
  prm_ID_FONAC_CICLO      NUMBER(38,0);
  prm_CICLO               NUMBER(38,0);
  prm_DESCRIPCION_CICLO   VARCHAR2(100 BYTE);
  prm_CONTRATO            VARCHAR2(20 BYTE);
  prm_VIGENCIA_INICIO     VARCHAR2(10 BYTE);
  prm_VIGENCIA_FIN        VARCHAR2(10 BYTE);
  prm_ID_ESTATUS_FONAC_FK NUMBER(38,0);
  -- FECHAS
  day_fi        VARCHAR2(5);
  num_mes_fi    VARCHAR2(3);
  month_fi      VARCHAR2(11);
  trim_month_fi VARCHAR2(11);
  anio_fi       VARCHAR2(5);
  day_ff        VARCHAR2(5);
  num_mes_ff    VARCHAR2(3);
  month_ff      VARCHAR2(11);
  trim_month_ff VARCHAR2(11);
  anio_ff       VARCHAR2(5);
  day_act       VARCHAR(3);
  month_act     VARCHAR(3);
  year_act      VARCHAR(3);
  
  dayI_qa VARCHAR(3);
  dayF_qa VARCHAR(3);
  num_mes_QA  VARCHAR(3);
  month_QA VARCHAR(12);
  trim_month_QA VARCHAR(11);
  anio_QA  VARCHAR(6);
  
  
  -- DATOS QUINCENAS
  QI_ID_QUINCENA           INTEGER;
  QI_ID_EJERCICIO_FK       INTEGER;
  QI_NUMERO_QUINCENA       INTEGER;
  QA_ID_QUINCENA           INTEGER;
  QA_ID_EJERCICIO_FK       INTEGER;
  QA_NUMERO_QUINCENA       INTEGER;
  QA_FECHA_INICIO_QUINCENA DATE;
  QA_FECHA_FIN_QUINCENA    DATE;
  -- DATOS EMPLEADO PADRON
  C1_ID_EMPLEADO_PADRON     INTEGER;
  C1_FECHA_INSCRIPCION_ALTA DATE;
  c1_PERIODO_ORDINARIO      INTEGER;
  -- DATOS CALCULOS
  cntTrabInsc INTEGER :=0;
BEGIN
  ---------------------------
  -- OBTENEMOS EL CICLO
  ---------------------------
  SELECT ID_FONAC_CICLO,
    CICLO,
    DESCRIPCION_CICLO,
    CONTRATO,
    VIGENCIA_INICIO,
    VIGENCIA_FIN,
    ID_ESTATUS_FONAC_FK
    
  INTO 
    prm_ID_FONAC_CICLO,
    prm_CICLO,
    prm_DESCRIPCION_CICLO,
    prm_CONTRATO,
    prm_VIGENCIA_INICIO,
    prm_VIGENCIA_FIN,
    prm_ID_ESTATUS_FONAC_FK
  FROM rh_nomn_fonac_ciclos
  WHERE id_fonac_ciclo       = idCiclo;
  
  
  ---------------------------
  -- DESC FECHA INICIO CICLO
  ---------------------------
  -- d�a
      SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'dd')
      INTO day_fi
      FROM DUAL;
  -- mes
      SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'MM' )
      INTO num_mes_fi
      FROM DUAL;
      SELECT TO_CHAR(to_date(TO_CHAR(num_mes_fi),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH')
      INTO month_fi
      FROM dual;
      SELECT TRIM(month_fi) INTO trim_month_fi FROM DUAL;
      SELECT upper(trim_month_fi) INTO month_fi FROM dual;
      -- a�o
      SELECT TO_CHAR(to_date(prm_VIGENCIA_INICIO, 'dd/MM/YYYY'),'yy')
      INTO anio_fi
      FROM DUAL;
  ---------------------------
  -- DESC FECHA FIN CICLO
  ---------------------------
      -- d�a
        SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'dd')
        INTO day_ff
        FROM DUAL;
      -- mes
        SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'MM' )
        INTO num_mes_ff
        FROM DUAL;
        SELECT TO_CHAR(to_date(TO_CHAR(num_mes_ff),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH')
        INTO month_ff
        FROM dual;
        SELECT TRIM(month_ff) INTO trim_month_ff FROM DUAL;
        SELECT upper(trim_month_ff) INTO month_ff FROM dual;  
      -- a�o
        SELECT TO_CHAR(to_date(prm_VIGENCIA_FIN, 'dd/MM/YYYY'),'yy')
        INTO anio_ff
        FROM DUAL;
      
  ------------------------
  --- obtenemos las desc de la quincena
  ------------------------
  
    -- obtenemos el n�mero de quincena de la que se esta cosultando
      select ID_QUINCENA,  ID_EJERCICIO_FK, NUMERO_QUINCENA, FECHA_INICIO_QUINCENA, FECHA_FIN_QUINCENA  
      into QA_ID_QUINCENA, QA_ID_EJERCICIO_FK, QA_NUMERO_QUINCENA, QA_FECHA_INICIO_QUINCENA, QA_FECHA_FIN_QUINCENA  
      from rh_nomn_cat_quincenas
      where ID_QUINCENA = idQuincena;
      
    -- d�a
        SELECT TO_CHAR(QA_FECHA_INICIO_QUINCENA,'dd')
        INTO dayI_qa
        FROM DUAL;
        
        SELECT TO_CHAR(QA_FECHA_FIN_QUINCENA,'dd')
        INTO dayF_qa
        FROM DUAL;
    -- mes
        SELECT TO_CHAR(QA_FECHA_INICIO_QUINCENA,'MM' )
        INTO num_mes_QA
        FROM DUAL;
        
        SELECT TO_CHAR(to_date(TO_CHAR(num_mes_QA),'mm'),'Month','NLS_DATE_LANGUAGE = SPANISH')
        INTO month_QA
        FROM dual;
        
        SELECT TRIM(month_QA) INTO trim_month_QA FROM DUAL;
        
        SELECT upper(trim_month_QA) INTO month_QA FROM dual;  
        
      -- a�o
        SELECT TO_CHAR(to_date(QA_FECHA_INICIO_QUINCENA, 'dd/MM/YYYY'),'yy')
        INTO anio_QA
        FROM DUAL;
      
  ------------------------
  --- obtenemos total inscritos, bajas en la quincena y bajas acumuladas
  ------------------------
  --total inscritos
        SELECT COUNT(aso.id_empleado_padron)
        INTO cntTotInscritos
        FROM rh_nomn_fonac_aso_emp_pad_cic aso
        INNER JOIN rh_nomn_fonac_empleados_padron ep
        ON aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
        INNER JOIN rh_nomn_fonac_ciclos_periodos cp
        ON cp.ID_CICLO_PERIODO   = aso.ID_CICLO_PERIODO
        WHERE aso.id_fonac_ciclo = idCiclo
        AND cp.PERIODO_ORDINARIO = idPeriodo;
      -- bajas totales
        SELECT COUNT(aso.id_empleado_padron)
        INTO cntTotBajas
        FROM rh_nomn_fonac_aso_emp_pad_cic aso
        INNER JOIN rh_nomn_fonac_empleados_padron ep
        ON aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
        INNER JOIN rh_nomn_fonac_ciclos_periodos cp
        ON cp.ID_CICLO_PERIODO   = aso.ID_CICLO_PERIODO
        WHERE aso.id_fonac_ciclo = idCiclo
        AND cp.PERIODO_ORDINARIO = idPeriodo
        AND ep.ID_BAJA_FONAC    IS NOT NULL;
  -- bajas Por quincena
      SELECT COUNT(aso.id_empleado_padron)
      INTO cntBajasQuin
      FROM rh_nomn_fonac_aso_emp_pad_cic aso
      INNER JOIN rh_nomn_fonac_empleados_padron ep
      ON aso.ID_EMPLEADO_PADRON = ep.ID_EMPLEADO_PADRON
      INNER JOIN rh_nomn_fonac_ciclos_periodos cp
      ON cp.ID_CICLO_PERIODO = aso.ID_CICLO_PERIODO
      INNER JOIN rh_nomn_fonac_det_baja db
      ON db.ID_BAJA_FONAC      = ep.ID_BAJA_FONAC
      WHERE aso.id_fonac_ciclo = idCiclo
      AND cp.PERIODO_ORDINARIO = idPeriodo
      AND ep.ID_BAJA_FONAC    IS NOT NULL
      AND FECHA_INSCRIPCION_BAJA BETWEEN QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA;
  ----------------------------
  --- obtenemos los empleados con baja en la quincena, divididos por el motivo de baja
  -----------------------------
    -- RENUNCIA
        select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot1
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 1
        order by 
            aso.id_empleado_padron;
     
     -- FALLECIMIENTO, INCAPACIDAD O INVALIDEZ       
       select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot2
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 2
        order by 
            aso.id_empleado_padron;
            
     -- VOLUNTAD EXPRESA
       select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot3
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 3
        order by 
            aso.id_empleado_padron;      
            
    -- PROMOCI�N A MANDO MEDIO O SUPERIOR
       select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot4
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 4
        order by 
            aso.id_empleado_padron;  
            
     -- LICENCIA SIN GOCE DE SUELDO
       select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot5
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 5
        order by 
            aso.id_empleado_padron; 
      
      -- otra
       select 
            count(db.ID_MOTIVO_BAJA_FK) 
        into
            cntMot7 
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO 
        inner join 
            rh_nomn_fonac_det_baja db on db.ID_BAJA_FONAC = ep.ID_BAJA_FONAC        
        where 
            aso.ID_FONAC_CICLO        = idCiclo
            and cp.PERIODO_ORDINARIO  = idPeriodo
            and ep.id_baja_fonac is not null
            and db.FECHA_INSCRIPCION_BAJA between QA_FECHA_INICIO_QUINCENA AND QA_FECHA_FIN_QUINCENA
            and db.ID_MOTIVO_BAJA_FK  = 7
        order by 
            aso.id_empleado_padron; 
            
  --DBMS_OUTPUT.PUT_LINE(' -----contadores--> ' || cntMot1 || ' 2-> ' || cntMot2 || ' 3-> ' || cntMot3 || ' 4->' || cntMot4 || ' 5->' || cntMot5 || ' 7->' || cntMot7);    
            
        contTotal :=  cntMot1 + cntMot2 + cntMot3 +cntMot4 + cntMot5 + cntMot7;  
  ------------------------
  --- CONCATENAMOS TODO
  ------------------------
  qry := 'select ' || 
          contTotal || ' as tot, '||
          cntMot1 || ' as mot1, ' || cntMot2 || ' as mot2, ' || cntMot3 || ' as mot3, ' || cntMot4 || ' as mot4, ' || cntMot5 || ' as mot5, ' || cntMot7  || ' as mot7, ' || 
           ' ''PERIODO: DEL  ' || dayI_qa || ' AL ' || dayF_qa || ' DE ' || month_QA || ' DE 20'  || anio_QA || ''' as desc_Periodo,' ||
          cntTotInscritos || ' as cntTotInsc, ' || cntTotBajas || ' as cntTotBajas, ' || cntBajasQuin || ' as cntBajasQuin, ' || 
          QA_NUMERO_QUINCENA || ' as qnaAct, ' || '''20' || anio_fi || '-20' || anio_ff || ''' as aniosCiclo,' || '''PARA EL  ' ||
          prm_DESCRIPCION_CICLO || ' CICLO DEL FONAC'' as desc_ciclo_enc, ' || '  ''PERIODO DEL ' || day_fi || ' DE ' || month_fi || ' DE 20' || anio_fi || '  AL ' || day_ff || ' DE ' || month_ff || ' DE 20' || anio_ff || ''' as descFechaCiclo, '|| prm_ID_FONAC_CICLO || ' as idFonac,' || prm_CICLO || ' as idCiclo,' || ' ''' || prm_DESCRIPCION_CICLO || ''' as descCiclo,' || ' ''' || prm_CONTRATO || ''' as contrato,' || ' ''' || prm_VIGENCIA_INICIO || ''' as vigInicio,' || ' ''' || prm_VIGENCIA_FIN || ''' as vigFin,' ||
          cntTrabInsc || ' as trabInsc                       
        from dual';
  DBMS_OUTPUT.PUT_LINE(' ------------qry--> ' || qry);
  OPEN REPORTE FOR qry;
END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_FONAC_TRANS_ELECTR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_FONAC_TRANS_ELECTR" (
                                                          idCiclo IN INTEGER,
                                                          REPORTE OUT SYS_REFCURSOR)
IS 


  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
cntImpAdic integer;
cntClabeBenef integer;
qry2 varchar2(4000);
 C1 SYS_REFCURSOR;
 C2 SYS_REFCURSOR;
 qry1 varchar(3000);
 cntBancoDesc integer;
 beneficiario varchar2(300);
 clabeBenef varchar2(20 byte);
 c1_PERIODO_ORDINARIO VARCHAR2(100 BYTE);
 
  prm_ID_FONAC_CICLO NUMBER(38,0);
  prm_CICLO NUMBER(38,0);
  prm_DESCRIPCION_CICLO VARCHAR2(100 BYTE);
  prm_CONTRATO VARCHAR2(20 BYTE);
  prm_VIGENCIA_INICIO VARCHAR2(10 BYTE);
  prm_VIGENCIA_FIN VARCHAR2(10 BYTE);
  prm_ID_ESTATUS_FONAC_FK NUMBER(38,0);
  prm_FACT_PER_ORDINARIO NUMBER;
  prm_FACT_PER_EXTRAORDINARIO NUMBER;
  
  C1_ID_EMPLEADO_PADRON  INTEGER;
  C1_FECHA_INSCRIPCION_ALTA  DATE;
  C1_ID_QUINCENA_FIN integer;
  C1_ID_QUINCENA_INICIO integer;
  C1_APORTACION_EMPLEADO NUMBER(10,2);
  C1_APORTACION_GOB_FED_CAP NUMBER(10,2);
  C1_APORTACION_GOB_FED_NO_CAP NUMBER(10,2);
  c1_id_fonac_aportacion NUMBER(38,0);
  c1_ID_EMPLEADO integer;
  c1_ID_QUINC_CICLO_INICIO NUMBER(38,0);
  c1_ID_QUINC_CICLO_FIN NUMBER(38,0);
  cntRetro integer;
  cntQuincRetro integer;
  fechaRetro date;
  mtoAdicional NUMBER(38,0);
  idQuincAdic integer;
  idQuincRetro integer;
  sumMtoRetro  NUMBER(38,0);
  bancoDesc varchar2(100);
  tablatmp varchar2(50);
  
 
  c2_ID_QUINCENA NUMBER;                       
  c2_FECHA_INICIO_QUINCENA DATE;
  c2_FECHA_FIN_QUINCENA DATE;          
  c2_ID_EJERCICIO_FK NUMBER;
  c2_NUMERO_QUINCENA NUMBER(2,0);
  
  mtoAportAct number(8,2);
  sumTotMtoAportTrab number(8,2);
  sumTotMtoGobNoCap number(8,2);
  sumTotMtoGobCap number(8,2);
  sumTotal number(8,2);
  
  
  existTable INTEGER;
  
BEGIN

tablatmp  := 'TMP_REPORT_TRANS_ELEC';

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp;
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
    
  ----------------------------------------------------------------------
  --  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
  ----------------------------------------------------------------------
     
       EXECUTE IMMEDIATE 'CREATE TABLE '||tablatmp||' 
       (  
          BANCO VARCHAR2(250),
          CLABE VARCHAR2(25),
          IMPTE NUMERIC(9,2),
          BENEFICIARIO VARCHAR2(100),
          REFERENCIA VARCHAR2(4)
       )';  

---------------------------  
-- OBTENEMOS EL CICLO
---------------------------    
  select  
    ID_FONAC_CICLO,
    CICLO,
    DESCRIPCION_CICLO,
    CONTRATO,
    VIGENCIA_INICIO,
    VIGENCIA_FIN,
    ID_ESTATUS_FONAC_FK,
    FACT_PER_ORDINARIO,
    FACT_PER_EXTRAORDINARIO
  into
    prm_ID_FONAC_CICLO,
    prm_CICLO,
    prm_DESCRIPCION_CICLO,
    prm_CONTRATO,
    prm_VIGENCIA_INICIO,
    prm_VIGENCIA_FIN,
    prm_ID_ESTATUS_FONAC_FK,
    prm_FACT_PER_ORDINARIO,
    prm_FACT_PER_EXTRAORDINARIO
  from 
    rh_nomn_fonac_ciclos
  where id_fonac_ciclo =  idCiclo;
  
  if(prm_FACT_PER_ORDINARIO is null ) then
      prm_FACT_PER_ORDINARIO :=0;
  end if;
  
  if(prm_FACT_PER_EXTRAORDINARIO is null)then
    prm_FACT_PER_EXTRAORDINARIO:= 0;
  end if;  

----------------------------
--- trabajadores inscritos
-----------------------------
  
  --- obtenemos cada registro, lo recorremos por cada uno btenemos su aportaci�n quincenal 
  --- y vamos sumando 
  
  qry1 := 'select 
            ep.ID_EMPLEADO_PADRON,
            
            ep.FECHA_INSCRIPCION_ALTA,
            
            pei.ID_QUINCENA_FIN,
            pei.ID_QUINCENA_INICIO,
            
            fa.APORTACION_EMPLEADO,
            fa.APORTACION_GOB_FED_CAP,
            fa.APORTACION_GOB_FED_NO_CAP, 
            fa.id_fonac_aportacion,
            ep.ID_EMPLEADO,
            
            cp.PERIODO_ORDINARIO,
            cp.ID_QUINCENA_INICIO,
            cp.ID_QUINCENA_FIN
            
        
        from 
            rh_nomn_fonac_aso_emp_pad_cic aso    
        inner join 
            rh_nomn_fonac_empleados_padron ep on ep.ID_EMPLEADO_PADRON = aso.ID_EMPLEADO_PADRON
        inner join 
            rh_nomn_fonac_pe_inscripciones pei on pei.ID_CICLO_PERIODO = aso.ID_CICLO_PERIODO
        inner join 
            rh_nomn_fonac_ciclos_periodos cp on cp.ID_FONAC_CICLO = aso.ID_FONAC_CICLO  and aso.ID_CICLO_PERIODO = cp.ID_CICLO_PERIODO    
        inner join 
            RH_NOMN_FONAC_APORTACIONES fa on fa.ID_FONAC_APORTACION = ep.ID_FONAC_APORTACION  
        where 
              aso.ID_FONAC_CICLO = ' || idCiclo || '
             and ep.id_baja_fonac is null
        order by 
            aso.id_empleado_padron
        ' ;
        
        --DBMS_OUTPUT.PUT_LINE(' qry1-->' || qry1);

  OPEN C1 FOR qry1;
    LOOP  
      FETCH C1 INTO
        C1_ID_EMPLEADO_PADRON,
        C1_FECHA_INSCRIPCION_ALTA,
        C1_ID_QUINCENA_FIN,
        C1_ID_QUINCENA_INICIO,
        C1_APORTACION_EMPLEADO,
        C1_APORTACION_GOB_FED_CAP,
        C1_APORTACION_GOB_FED_NO_CAP,
        c1_id_fonac_aportacion,
        c1_ID_EMPLEADO,
        c1_PERIODO_ORDINARIO,
        c1_ID_QUINC_CICLO_INICIO,
        c1_ID_QUINC_CICLO_FIN
        ;
        EXIT
            WHEN C1%NOTFOUND; 
              
              --DBMS_OUTPUT.PUT_LINE(' ---------------------------------------------------------------------');
              --DBMS_OUTPUT.PUT_LINE(' ---------------------------------------' || C1_ID_EMPLEADO_PADRON ); 
              
              -- BANCO / CTA CLABE / NOMBRE /IFT
              
              select a_paterno  || ' ' ||  a_materno || ' ' ||  nombre as beneficiario
              into beneficiario
              from rh_nomn_datos_personales
              where ID_DATOS_PERSONALES = (select ID_DATOS_PERSONALES from rh_nomn_empleados where id_empleado =  c1_ID_EMPLEADO);
              
              select count(DESCRIPCION_BANCO) into cntBancoDesc from rh_nomn_cat_bancos where id_banco = ( select id_banco from rh_nomn_datos_bancarios where id_datos_bancarios = (select ID_DATOS_BANCARIOS from rh_nomn_empleados where id_empleado =  c1_ID_EMPLEADO) );
                
              if(cntBancoDesc = 0 or cntBancoDesc is null)then
                  bancoDesc := '';
              else
                  select DESCRIPCION_BANCO into bancoDesc from rh_nomn_cat_bancos where id_banco = ( select id_banco from rh_nomn_datos_bancarios where id_datos_bancarios = (select ID_DATOS_BANCARIOS from rh_nomn_empleados where id_empleado =  c1_ID_EMPLEADO) );
              end if;    
                            
              select count(CLABE) into   cntClabeBenef from   rh_nomn_datos_bancarios where  ID_DATOS_BANCARIOS = (select ID_DATOS_BANCARIOS from rh_nomn_empleados where id_empleado = c1_ID_EMPLEADO);
              
              if(cntClabeBenef = 0 or cntClabeBenef is null )then
                clabeBenef := '';
              else
                select CLABE
                into   clabeBenef
                from   rh_nomn_datos_bancarios
                where  ID_DATOS_BANCARIOS = (select ID_DATOS_BANCARIOS from rh_nomn_empleados where id_empleado = c1_ID_EMPLEADO);
              end if;
            
            
            -------------------------------------------------------------
            -- APORTACIONES CON RENDIMIENTOS DEL EMPLEADO EN EL CICLO  
            -------------------------------------------------------------
              select round (
                    ( count (ID_QUINCENA)  * C1_APORTACION_EMPLEADO ) +
                    ( count (ID_QUINCENA)  * C1_APORTACION_EMPLEADO )   , 2)                    
              into sumTotal
              from rh_nomn_cat_quincenas
              where  ACTIVO = 1 and id_quincena >=   C1_ID_QUINCENA_INICIO  and ID_QUINCENA <= c1_ID_QUINC_CICLO_FIN ;
              
           ----------------------------------------------------------------------
            -- OBTENEMOS EL MONTO TOTAL DE LA RETROACTIVIDAD SI ES QUE HAY UNA
           ---------------------------------------------------------------------
            
            sumMtoRetro := 0.0;
            select count(ID_FONAC_APORTACION) into cntRetro from rh_nomn_fonac_aporta_retro where ID_FONAC_APORTACION = c1_ID_FONAC_APORTACION;
            
            
             if(cntRetro > 0)then
                select fecha_inicio into fechaRetro from rh_nomn_fonac_aporta_retro  where ID_FONAC_APORTACION = c1_ID_FONAC_APORTACION ;
                DBMS_OUTPUT.PUT_LINE(' fechaRetro---> ' || fechaRetro);  
                
                select ID_QUINCENA
                  into idQuincRetro
                  from rh_nomn_cat_quincenas 
                  where 
                      ACTIVO = 1 
                      and fechaRetro between FECHA_INICIO_QUINCENA and FECHA_FIN_QUINCENA;
                      
                mtoAdicional := 0.0;
                sumMtoRetro := 0.0;
                
                select count(importe_adicional) into cntImpAdic from RH_NOMN_FONAC_PAGO_ADICIONAL where id_quincena > idQuincRetro and id_empleado_padron = C1_ID_EMPLEADO_PADRON; 
                if(cntImpAdic > 0)then
                  select importe_adicional, ID_QUINCENA into mtoAdicional, idQuincAdic from RH_NOMN_FONAC_PAGO_ADICIONAL where id_quincena > idQuincRetro and id_empleado_padron = C1_ID_EMPLEADO_PADRON;
                    
                    select ( count(id_quincena) * mtoAdicional)
                    into sumMtoRetro 
                    from rh_nomn_cat_quincenas 
                    where 
                        ID_QUINCENA >= idQuincRetro
                          
                     and ID_QUINCENA <= idQuincAdic ;
                end if;
                 
                 DBMS_OUTPUT.PUT_LINE(' sumMtoRetro---> ' || sumMtoRetro);
                 
                   
             end if;
             
             sumTotal := sumTotal + sumMtoRetro;
         
              EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (''' || bancoDesc || ''', ''' || clabeBenef || ''',' || sumTotal || ' , '''  ||  beneficiario || ''', ''IFT'' )';
              
    END LOOP;
    CLOSE C1;
  
------------------------
--- CONCATENAMOS TODO
------------------------  
  qry1 :=  ' select * from ' || tablatmp;

  OPEN REPORTE FOR qry1;
  
   OPEN REPORTE FOR qry1;
   
    EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
    
    
EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp 
      AND OWNER = 'SERP';
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
  reporte := null;  
      
            

END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_SEGVID_AJUSTE_PRIMAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_SEGVID_AJUSTE_PRIMAS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) 


AS 
 
  vQuincenaReporta    VARCHAR2(100);
  vNumeroPoliza       VARCHAR2(50);
  nivelTab varchar2(50);
  sdoBase NUMBER(8,2);
  compGarantizada NUMBER(8,2);
  descUniAdmin varchar2(300);
  nivTabHom varchar2(5);
  
  primaMensRiesgo numeric(8,2) := 0;
  porcPrimaBasica numeric(8,2) := 1.46;
  
   CURSOR   cursorAltas  IS

        select 
            emp.FECHA_DE_INGRESO,
            emp.ID_EMPLEADO,
            emp.ID_DATOS_PERSONALES,
            emp.ID_PLAZA,
            emp.NUMERO_EMPLEADO,
            emp.FECHA_BAJA,
            
            dp.NOMBRE as nombre,
            dp.A_PATERNO as apPat,
            dp.A_MATERNO as apMat,
            dp.CURP,
            dp.RFC,
            to_char(dp.FECHA_NACIMIENTO, 'yyyyMMdd') as fecha_nacimiento,
            case when dp.ID_GENERO = 1 then 'M' ELSE 'F' END as sexo
          
          
            
          from  rh_nomn_empleados emp
          inner join RH_NOMN_DATOS_PERSONALES dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
          where trunc(emp.FECHA_DE_INGRESO) 
                between 
                   (  select trunc(cq.FECHA_INICIO_QUINCENA )
                    from RH_NOMN_CAT_QUINCENAS cq
                    where 
                          cq.ACTIVO = 1
                    and    cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor = (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual))
                    and   cq.NUMERO_QUINCENA = IDQUINCENA)
                AND 
                  (  select trunc(cq.FECHA_FIN_QUINCENA)
                    from RH_NOMN_CAT_QUINCENAS cq
                    where 
                          cq.ACTIVO = 1
                    and    cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor = (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual))
                    and   cq.NUMERO_QUINCENA = IDQUINCENA)
                    
        ORDER BY emp.NUMERO_EMPLEADO                                               
          ;
          
    emp_FECHA_DE_INGRESO date;
    emp_ID_EMPLEADO NUMBER;
    emp_ID_DATOS_PERSONALES NUMBER;
    emp_ID_PLAZA NUMBER;
    emp_NUMERO_EMPLEADO VARCHAR2(30 BYTE);
    emp_FECHA_BAJA DATE;
    
    dp_NOMBRE VARCHAR2(100 BYTE);
    dp_A_PATERNO VARCHAR2(100 BYTE);
    dp_A_MATERNO VARCHAR2(100 BYTE);
    dp_CURP VARCHAR2(20 BYTE);
    dp_RFC VARCHAR2(20 BYTE);
    dp_fecha_nacimiento date;
    dp_sexo VARCHAR2(2 BYTE);
    
    
BEGIN



  -- Obtener el n�mero de poliza actual, de la prestaci�n de Seguro de Vida.
  SELECT 
    POL.NUMERO_POLIZA
  INTO
    vNumeroPoliza
  FROM 
    RH_NOMN_POLIZAS POL
    INNER JOIN RH_NOMN_CAT_PRESTACIONES PRE ON PRE.ID_PRESTACION = POL.ID_PRESTACION
  WHERE 
    PRE.DESC_PRESTACION LIKE '%VIDA%'
  ;  

  -- Quincena en la que se hace el reporte.
  SELECT
    MAX(DECODE((SELECT QUI2.NUMERO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI2 WHERE QUI2.ID_QUINCENA = IDQUINCENA), 
    1, 'PRIMERA QNA DE ENERO',
    2, 'SEGUNDA QNA DE ENERO',
    3, 'PRIMERA QNA DE FEBRERO',
    4, 'SEGUNDA QNA DE FEBRERO',
    5, 'PRIMERA QNA DE MARZO',
    6, 'SEGUNDA QNA DE MARZO',
    7, 'PRIMERA QNA DE ABRIL',
    8, 'SEGUNDA QNA DE ABRIL',
    9, 'PRIMERA QNA DE MAYO',
    10, 'SEGUNDA QNA DE MAYO',
    11, 'PRIMERA QNA DE JUNIO',
    12, 'SEGUNDA QNA DE JUNIO',
    13, 'PRIMERA QNA DE JULIO',
    14, 'SEGUNDA QNA DE JULIO',
    15, 'PRIMERA QNA DE AGOSTO',
    16, 'SEGUNDA QNA DE AGOSTO',
    17, 'PRIMERA QNA DE SEPTIEMBRE',
    18, 'SEGUNDA QNA DE SEPTIEMBRE',
    19, 'PRIMERA QNA DE OCTUBRE',
    20, 'SEGUNDA QNA DE OCTUBRE',
    21, 'PRIMERA QNA DE NOVIEMBRE',
    22, 'SEGUNDA QNA DE NOVIEMBRE',
    23, 'PRIMERA QNA DE DICIEMBRE',
    24, 'SEGUNDA QNA DE DICIEMBRE')) QUINCENA_REPORTA
  INTO
    vQuincenaReporta
  FROM
    RH_NOMN_CAT_QUINCENAS
  ;  

  OPEN cursorAltas;
    LOOP
      FETCH cursorAltas INTO
          
          emp_FECHA_DE_INGRESO,
          emp_ID_EMPLEADO,
          emp_ID_DATOS_PERSONALES,
          emp_ID_PLAZA,
          emp_NUMERO_EMPLEADO,
          emp_FECHA_BAJA,
          
          dp_NOMBRE ,
          dp_A_PATERNO,
          dp_A_MATERNO ,
          dp_CURP ,
          dp_RFC,
          dp_fecha_nacimiento ,
          dp_sexo 
          
      ;
      EXIT WHEN cursorAltas%NOTFOUND;
                
      ---  N I V E L   T A B U L A R
          select 
            gpoJer.CLAVE_JERARQUICA || grdsJer.GRADO || catNiv.NIVEL as nivelTabulador,
            tab.SUELDO_BASE,
            tab.COMPENSACION_GARANTIZADA,
            gpoJer.SGMM,
            catUAdmin.DESCRIPCION
          into 
            nivelTab,
            sdoBase,
            compGarantizada,
            nivTabHom,
            descUniAdmin
          from 
            RH_NOMN_EMPLEADOS emp
          inner join 
            RH_NOMN_PLAZAS plz ON plz.ID_PLAZA = emp.ID_PLAZA
          inner join 
            RH_NOMN_TABULADORES tab ON plz.ID_TABULADOR_FK = tab.ID_TABULADOR
          inner join RH_NOMN_CAT_GRUPOS_JERARQUICOS gpoJer on tab.ID_GRUPO_JERARQUICO_FK = gpoJer.ID_GRUPO_JERARQUICO
          inner join RH_NOMN_CAT_GRADOS_JERARQUICOS grdsJer on grdsJer.ID_GRADO_JERARQUICO = tab.ID_GRADO_JERARQUICO_FK
          inner join RH_NOMN_CAT_NIVELES_TABULADOR catNiv on catNiv.ID_NIVEL_TABULADOR = tab.ID_NIVEL_TABULADOR_FK
          inner join RH_NOMN_ADSCRIPCIONES ads ON plz.ID_ADSCRIPCION_FK = ads.ID_ADSCRIPCION
          inner join RH_NOMN_CAT_UNIDADES_ADMINS catUAdmin on catUAdmin.ID_UNIADM = ads.ID_UNIADM_FK
          
          WHERE emp.ID_EMPLEADO = emp_ID_EMPLEADO;
          
         
        
    END LOOP;  
    CLOSE cursorAltas;
  
  
  
  
  


  
END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_SEGVID_AJUSTEPRIMASBAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_SEGVID_AJUSTEPRIMASBAS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) 


AS 
 
  vQuincenaReporta    VARCHAR2(100);
  vNumeroPoliza       VARCHAR2(50);
  nivelTab varchar2(50);
  sdoBase NUMBER(8,2);
  compGarantizada NUMBER(8,2);
  descUniAdmin varchar2(300);
  nivTabHom varchar2(5);
  
  primaMensRiesgo numeric(8,2) := 0;
  porcPrimaBasica numeric(8,2) := 1.46;
  
   CURSOR   cursorAltas  IS

        select 
            emp.FECHA_DE_INGRESO,
            emp.ID_EMPLEADO,
            emp.ID_DATOS_PERSONALES,
            emp.ID_PLAZA,
            emp.NUMERO_EMPLEADO,
            emp.FECHA_BAJA,
            
            dp.NOMBRE as nombre,
            dp.A_PATERNO as apPat,
            dp.A_MATERNO as apMat,
            dp.CURP,
            dp.RFC,
            to_char(dp.FECHA_NACIMIENTO, 'yyyyMMdd') as fecha_nacimiento,
            case when dp.ID_GENERO = 1 then 'M' ELSE 'F' END as sexo
          
          
            
          from  rh_nomn_empleados emp
          inner join RH_NOMN_DATOS_PERSONALES dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES
          where trunc(emp.FECHA_DE_INGRESO) 
                between 
                   (  select trunc(cq.FECHA_INICIO_QUINCENA )
                    from RH_NOMN_CAT_QUINCENAS cq
                    where 
                          cq.ACTIVO = 1
                    and    cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor = (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual))
                    and   cq.NUMERO_QUINCENA = IDQUINCENA)
                AND 
                  (  select trunc(cq.FECHA_FIN_QUINCENA)
                    from RH_NOMN_CAT_QUINCENAS cq
                    where 
                          cq.ACTIVO = 1
                    and    cq.ID_EJERCICIO_FK in ( select id_ejercicio  from RH_NOMN_CAT_EJERCICIOS ej where ej.valor = (select EXTRACT(YEAR FROM trunc(sysdate) ) from dual))
                    and   cq.NUMERO_QUINCENA = IDQUINCENA)
                    
        ORDER BY emp.NUMERO_EMPLEADO                                               
          ;
          
    emp_FECHA_DE_INGRESO date;
    emp_ID_EMPLEADO NUMBER;
    emp_ID_DATOS_PERSONALES NUMBER;
    emp_ID_PLAZA NUMBER;
    emp_NUMERO_EMPLEADO VARCHAR2(30 BYTE);
    emp_FECHA_BAJA DATE;
    
    dp_NOMBRE VARCHAR2(100 BYTE);
    dp_A_PATERNO VARCHAR2(100 BYTE);
    dp_A_MATERNO VARCHAR2(100 BYTE);
    dp_CURP VARCHAR2(20 BYTE);
    dp_RFC VARCHAR2(20 BYTE);
    dp_fecha_nacimiento date;
    dp_sexo VARCHAR2(2 BYTE);
    
    
BEGIN



  -- Obtener el n�mero de poliza actual, de la prestaci�n de Seguro de Vida.
  SELECT 
    POL.NUMERO_POLIZA
  INTO
    vNumeroPoliza
  FROM 
    RH_NOMN_POLIZAS POL
    INNER JOIN RH_NOMN_CAT_PRESTACIONES PRE ON PRE.ID_PRESTACION = POL.ID_PRESTACION
  WHERE 
    PRE.DESC_PRESTACION LIKE '%VIDA%'
  ;  

  -- Quincena en la que se hace el reporte.
  SELECT
    MAX(DECODE((SELECT QUI2.NUMERO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI2 WHERE QUI2.ID_QUINCENA = IDQUINCENA), 
    1, 'PRIMERA QNA DE ENERO',
    2, 'SEGUNDA QNA DE ENERO',
    3, 'PRIMERA QNA DE FEBRERO',
    4, 'SEGUNDA QNA DE FEBRERO',
    5, 'PRIMERA QNA DE MARZO',
    6, 'SEGUNDA QNA DE MARZO',
    7, 'PRIMERA QNA DE ABRIL',
    8, 'SEGUNDA QNA DE ABRIL',
    9, 'PRIMERA QNA DE MAYO',
    10, 'SEGUNDA QNA DE MAYO',
    11, 'PRIMERA QNA DE JUNIO',
    12, 'SEGUNDA QNA DE JUNIO',
    13, 'PRIMERA QNA DE JULIO',
    14, 'SEGUNDA QNA DE JULIO',
    15, 'PRIMERA QNA DE AGOSTO',
    16, 'SEGUNDA QNA DE AGOSTO',
    17, 'PRIMERA QNA DE SEPTIEMBRE',
    18, 'SEGUNDA QNA DE SEPTIEMBRE',
    19, 'PRIMERA QNA DE OCTUBRE',
    20, 'SEGUNDA QNA DE OCTUBRE',
    21, 'PRIMERA QNA DE NOVIEMBRE',
    22, 'SEGUNDA QNA DE NOVIEMBRE',
    23, 'PRIMERA QNA DE DICIEMBRE',
    24, 'SEGUNDA QNA DE DICIEMBRE')) QUINCENA_REPORTA
  INTO
    vQuincenaReporta
  FROM
    RH_NOMN_CAT_QUINCENAS
  ;  

  OPEN cursorAltas;
    LOOP
      FETCH cursorAltas INTO
          
          emp_FECHA_DE_INGRESO,
          emp_ID_EMPLEADO,
          emp_ID_DATOS_PERSONALES,
          emp_ID_PLAZA,
          emp_NUMERO_EMPLEADO,
          emp_FECHA_BAJA,
          
          dp_NOMBRE ,
          dp_A_PATERNO,
          dp_A_MATERNO ,
          dp_CURP ,
          dp_RFC,
          dp_fecha_nacimiento ,
          dp_sexo 
          
      ;
      EXIT WHEN cursorAltas%NOTFOUND;
                
      ---  N I V E L   T A B U L A R
          select 
            gpoJer.CLAVE_JERARQUICA || grdsJer.GRADO || catNiv.NIVEL as nivelTabulador,
            tab.SUELDO_BASE,
            tab.COMPENSACION_GARANTIZADA,
            gpoJer.SGMM,
            catUAdmin.DESCRIPCION
          into 
            nivelTab,
            sdoBase,
            compGarantizada,
            nivTabHom,
            descUniAdmin
          from 
            RH_NOMN_EMPLEADOS emp
          inner join 
            RH_NOMN_PLAZAS plz ON plz.ID_PLAZA = emp.ID_PLAZA
          inner join 
            RH_NOMN_TABULADORES tab ON plz.ID_TABULADOR_FK = tab.ID_TABULADOR
          inner join RH_NOMN_CAT_GRUPOS_JERARQUICOS gpoJer on tab.ID_GRUPO_JERARQUICO_FK = gpoJer.ID_GRUPO_JERARQUICO
          inner join RH_NOMN_CAT_GRADOS_JERARQUICOS grdsJer on grdsJer.ID_GRADO_JERARQUICO = tab.ID_GRADO_JERARQUICO_FK
          inner join RH_NOMN_CAT_NIVELES_TABULADOR catNiv on catNiv.ID_NIVEL_TABULADOR = tab.ID_NIVEL_TABULADOR_FK
          inner join RH_NOMN_ADSCRIPCIONES ads ON plz.ID_ADSCRIPCION_FK = ads.ID_ADSCRIPCION
          inner join RH_NOMN_CAT_UNIDADES_ADMINS catUAdmin on catUAdmin.ID_UNIADM = ads.ID_UNIADM_FK
          
          WHERE emp.ID_EMPLEADO = emp_ID_EMPLEADO;
          
         
        
    END LOOP;  
    CLOSE cursorAltas;
  
  
  
  
  


  
END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORT_SRC_ALTAS_BAJAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORT_SRC_ALTAS_BAJAS" (
                                                          idQuincIni IN INTEGER,
                                                          idQuincFin IN INTEGER,
                                                          accion in INTEGER,
                                                          REPORTE OUT SYS_REFCURSOR)
IS 


  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);

  C1 SYS_REFCURSOR;
  C2 SYS_REFCURSOR;
  qry1 varchar(4500);
 
 fechaIniQuin1 date;
 fechaFinQuin1 date;
 
 fechaIniQuin2 date;
 fechaFinQuin2 date;
 
 varAccion varchar2(150);
 srcFecha varchar2(150);
 
BEGIN

  select FECHA_INICIO_QUINCENA, FECHA_PAGO_QUINCENA into fechaIniQuin1, fechaFinQuin1 from rh_nomn_cat_quincenas where id_quincena = idQuincIni;
  select FECHA_INICIO_QUINCENA, FECHA_PAGO_QUINCENA    into  fechaIniQuin2, fechaFinQuin2 from rh_nomn_cat_quincenas where id_quincena = idQuincFin;

  varAccion := 'ALTA';  
  srcFecha := ' src.fecha_inicio '; 
 

  if(accion = 2)then
    varAccion := 'BAJA';
    srcFecha  := ' src.fecha_fin '; 
  end if;
  
  if (accion < 3)then
    qry1 := '
      select
            rownum,
            dp.nombre,  dp.A_PATERNO,   dp.A_MATERNO , 
            to_char(' || srcFecha || ', ''dd/MM/yyyy'') as fecha_mov,
            GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL CLAVE_PUESTO,            
            niv.DESCRIPCION_NIVEL_POTENCIA_SRC as DESCRIPCION_NIVEL_POTENCIA_SRC,
            ''' || varAccion || ''' as accion,
            
            seguro.IMPORTE_PRIMER_PAGO,
            seguro.IMPORTE,
            
            case 
              when ' || srcFecha || ' between ''' || fechaIniQuin1 || ''' and ''' || fechaFinQuin1 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else 0.0 end as pgo1,
              
          case 
              when ' || srcFecha || ' between ''' || fechaIniQuin2 || ''' and ''' || fechaFinQuin2 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else seguro.IMPORTE end as pgo2,
              
           (case 
              when ' || srcFecha || ' between ''' || fechaIniQuin1 || ''' and ''' || fechaFinQuin1 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else 0.0 end) 
           +
           (case 
              when ' || srcFecha || ' between ''' || fechaIniQuin2 || ''' and ''' || fechaFinQuin2 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else seguro.IMPORTE end)
           as totEmp
            
           
      from rh_nomn_seguros seguro 
      inner join rh_nomn_seg_resp_civi src on src.id_seguro=seguro.id_seguro
      inner join RH_NOMN_NIVELES_POTENCIA_SRC niv on src.ID_NIVEL_POTENCIA_SRC=niv.ID_NIVEL_POTENCIA_SRC
      inner join RH_NOMN_CONF_PERIODOS_SRC conf on src.ID_CONF_PERIODOS_SRC= conf.ID_CONF_PERIODOS_SRC
      inner join rh_nomn_empleados emp on emp.ID_EMPLEADO = seguro.id_empleado_fk
      inner join rh_nomn_datos_personales dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES 
      inner join rh_nomn_plazas pl on pl.ID_PLAZA = emp.ID_PLAZA
      INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = pl.ID_TABULADOR_FK
      INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK       
      
      where ' || srcFecha || '  between ''' || fechaIniQuin1 || ''' and ''' || fechaFinQuin2 || '''';
    
    
      --DBMS_OUTPUT.put_line ('qry1--->' || qry1);
     
     OPEN REPORTE FOR qry1;

  else
  
     qry1 := '
      select
            rownum,
            dp.nombre,  dp.A_PATERNO,   dp.A_MATERNO , 
            to_char(' || srcFecha || ', ''dd/MM/yyyy'') as fecha_mov,
            GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL CLAVE_PUESTO,            
            niv.DESCRIPCION_NIVEL_POTENCIA_SRC as DESCRIPCION_NIVEL_POTENCIA_SRC,
            ''' || varAccion || ''' as accion,
            
            seguro.IMPORTE_PRIMER_PAGO,
            seguro.IMPORTE,
            
            case 
              when ' || srcFecha || ' between ''' || fechaIniQuin1 || ''' and ''' || fechaFinQuin1 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else 0.0 end as pgo1,
              
          case 
              when ' || srcFecha || ' between ''' || fechaIniQuin2 || ''' and ''' || fechaFinQuin2 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else seguro.IMPORTE end as pgo2,
              
           (case 
              when ' || srcFecha || ' between ''' || fechaIniQuin1 || ''' and ''' || fechaFinQuin1 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else 0.0 end) 
           +
           (case 
              when ' || srcFecha || ' between ''' || fechaIniQuin2 || ''' and ''' || fechaFinQuin2 || ''' then seguro.IMPORTE_PRIMER_PAGO
              else seguro.IMPORTE end)
           as totEmp
            
           
      from rh_nomn_seguros seguro 
      inner join rh_nomn_seg_resp_civi src on src.id_seguro=seguro.id_seguro
      inner join RH_NOMN_NIVELES_POTENCIA_SRC niv on src.ID_NIVEL_POTENCIA_SRC=niv.ID_NIVEL_POTENCIA_SRC
      inner join RH_NOMN_CONF_PERIODOS_SRC conf on src.ID_CONF_PERIODOS_SRC= conf.ID_CONF_PERIODOS_SRC
      inner join rh_nomn_empleados emp on emp.ID_EMPLEADO = seguro.id_empleado_fk
      inner join rh_nomn_datos_personales dp on emp.ID_DATOS_PERSONALES = dp.ID_DATOS_PERSONALES 
      inner join rh_nomn_plazas pl on pl.ID_PLAZA = emp.ID_PLAZA
      INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = pl.ID_TABULADOR_FK
      INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK       
      
      where emp.id_empleado = 000';
    
  
    OPEN REPORTE FOR qry1 ;
    --REPORTE := null;
  end if;
   

       

END ;

/
--------------------------------------------------------
--  DDL for Procedure REPORTE_ALTA_ISSSTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTE_ALTA_ISSSTE" (IDEMPLEADO IN NUMBER,DATOS OUT SYS_REFCURSOR) IS

BEGIN

    OPEN DATOS FOR
        SELECT  
                EM.NUMERO_EMPLEADO,
                UPPER(DAT.CURP) CURP,
                UPPER(DAT.RFC) RFC,
                EM.NSS,
                UPPER(ENT.DESCRIPCION_ENTIDAD_FEDERATIVA) ENTIDAD_NACIMIENTO,
                DECODE(UPPER(ESTCIVIL.DESCRIPCION_ESTADO_CIVIL),'CASADO (A)',2,1) ESTADO_CIVIL,
                UPPER(SUBSTR(GENERO.DESCRIPCION_GENERO,0,1)) GENERO,
                UPPER(DAT.A_PATERNO) A_PATERNO,
                UPPER(DAT.A_MATERNO) A_MATERNO,
                UPPER(DAT.NOMBRE) NOMBRE,
                UPPER(DIR.CALLE) CALLE,
                UPPER(DIR.NUM_EXTERIOR)NUM_EXTERIOR ,
                UPPER(DIR.NUM_INTERIOR)NUM_INTERIOR,
                UPPER(COLONIA.DESCRIPCION_COLONIA) COLONIA,
                UPPER(MUNI.DESCRIPCION_MUNICIPIO) MUNICIPIO,
                UPPER(ENT.DESCRIPCION_ENTIDAD_FEDERATIVA) DIR_ENTIDAD_FEDERATIVA ,
                CP.DESCRIPCION_CODIGO_POSTAL CP,
                TO_CHAR(DECODE(EM.ID_CAUSA_BAJA,NULL,EM.FECHA_DE_INGRESO,EM.FECHA_BAJA),'DD') DIA,
                TO_CHAR(DECODE(EM.ID_CAUSA_BAJA,NULL,EM.FECHA_DE_INGRESO,EM.FECHA_BAJA),'MM') MES,
                TO_CHAR(DECODE(EM.ID_CAUSA_BAJA,NULL,EM.FECHA_DE_INGRESO,EM.FECHA_BAJA),'YY') ANIO,
                DECODE(UPPER(NOMB.DESCRIPCION),'ESTRUCTURA',20,'EVENTUAL',30) NOMBRAMINETO,
                TAB.SUELDO_BASE,
                TAB.COMPENSACION_GARANTIZADA,
                GRU.CLAVE_JERARQUICA||GRA.GRADO||NIV.NIVEL CLAVE_TAB,
                EM.ID_CAUSA_BAJA
        FROM RH_NOMN_EMPLEADOS EM
        LEFT JOIN RH_NOMN_DATOS_PERSONALES DAT       ON DAT.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
        LEFT JOIN RH_NOMN_CAT_ESTADOS_CIVIL ESTCIVIL ON ESTCIVIL.ID_ESTADO_CIVIL = DAT.ID_ESTADO_CIVIL
        LEFT JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENT ON ENT.ID_ENTIDAD_FEDERATIVA = DAT.ID_ENTIDAD_NACIMIENTO
        LEFT JOIN RH_NOMN_CAT_GENEROS  GENERO        ON GENERO.ID_GENERO = DAT.ID_GENERO
        LEFT JOIN RH_NOMN_DIRECCIONES  DIR           ON DIR.ID_DIRECCION = DAT.ID_DIRECCION
        LEFT JOIN RH_NOMN_CAT_COLONIAS COLONIA       ON COLONIA.ID_COLONIA = DIR.ID_COLONIA
        LEFT JOIN RH_NOMN_CAT_MUNICIPIOS  MUNI       ON MUNI.ID_MUNICIPIO = COLONIA.ID_MUNICIPIO
        LEFT JOIN RH_NOMN_CAT_CODIGO_POSTAL CP       ON CP.ID_CODIGO_POSTAL = COLONIA.ID_CODIGO_POSTAL
        LEFT JOIN RH_NOMN_PLAZAS PLZ                 ON PLZ.ID_PLAZA = EM.ID_PLAZA
        LEFT JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOMB ON NOMB.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
        LEFT JOIN RH_NOMN_TABULADORES            TAB  ON TAB.ID_TABULADOR = PLZ.ID_TABULADOR_FK
        LEFT JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NIV ON NIV.ID_NIVEL_TABULADOR =  tab.ID_NIVEL_TABULADOR_FK
        WHERE EM.ID_EMPLEADO = IDEMPLEADO

;
END REPORTE_ALTA_ISSSTE;

/
--------------------------------------------------------
--  DDL for Procedure REPORTE_LIC_MEDICAS_XPERIODO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTE_LIC_MEDICAS_XPERIODO" (FECHAINICIO IN DATE, FECHAFIN IN DATE,DATOS OUT SYS_REFCURSOR)IS

BEGIN

OPEN DATOS FOR
SELECT  EM.NUMERO_EMPLEADO,
        DAT.A_PATERNO||' '||A_MATERNO||' '||NOMBRE NOMBRE,
        ADS.DESCRIPCION ADSCRIPCION,
        LIC.TIPO_LICENCIA,
        LIC.DIAGNOSTICO,
        LIC.FECHA_INICIO,
        LIC.FECHA_FIN   
FROM RH_NOMN_EMPLEADOS EM
LEFT JOIN RH_NOMN_DATOS_PERSONALES  DAT  ON DAT.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
LEFT JOIN RH_NOMN_PLAZAS            PLZ  ON PLZ.ID_PLAZA            = EM.ID_PLAZA
LEFT JOIN RH_NOMN_ADSCRIPCIONES     ADS  ON ADS.ID_ADSCRIPCION      = PLZ.ID_ADSCRIPCION_FK
LEFT JOIN RH_NOMN_LICENCIAS_MEDICAS LIC  ON LIC.ID_EMPLEADO_FK      = EM.ID_EMPLEADO
WHERE LIC.FECHA_INICIO BETWEEN FECHAINICIO AND FECHAFIN
ORDER BY LIC.FECHA_INICIO
;

END REPORTE_LIC_MEDICAS_XPERIODO;

/
--------------------------------------------------------
--  DDL for Procedure REPORTEALTASBAJASSGMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTEALTASBAJASSGMM" (fechaInicio DATE, fechaFin DATE,
                                             REPORTE OUT SYS_REFCURSOR)
IS 

vIdSGMM   NUMBER;
vIdSGMMTitular   NUMBER;
vIdExtSGMMDescendiente  NUMBER;
vIdExtSGMMAscendiente  NUMBER;

vAPTitular  VARCHAR2(40);
vAMTitular  VARCHAR2(40);
vNOMTitular  VARCHAR2(40);
vFNTitular  DATE;
vRFCTitular VARCHAR2(20);
vCURPTitular  VARCHAR2(20);
vSexoTitular  VARCHAR2(3);
vCodEntFedTitular NUMBER;
vCodMunTitular  NUMBER;
vNivTabTitular  VARCHAR2(3);
vPercepTitular  NUMBER;
vEventual VARCHAR2(3);

vAPAsegurado  VARCHAR2(40);
vAMAsegurado VARCHAR2(40);
vNOMAsegurado  VARCHAR2(40);
vFNAsegurado  DATE;
vCURPAsegurado  VARCHAR2(20);
vSexoAsegurado  VARCHAR2(3);
vFAntAsegurado  DATE;
vTipoAsegurado  VARCHAR2(2);
vFColAsegurado  DATE;
vAltaBaja VARCHAR2(5);
vFMovAsegurado  DATE;

vSumaAsegBasica NUMBER;
vPrimaBasica NUMBER;
vSumaAsegPotenciada NUMBER;
vPrimaPotenciada NUMBER;


Tabla VARCHAR2(10000);
EXISTETABLA NUMBER;

CURSOR SGMM IS 
SELECT SGMM.ID_SGMM FROM RH_NOMN_SGMM SGMM WHERE SGMM.FECHA_ALTA_IFT BETWEEN fechaInicio AND fechaFin ORDER BY SGMM.FECHA_ALTA_IFT;

CURSOR TITULAR IS
SELECT SGMM.ID_SGMM, DP.A_PATERNO , DP.A_MATERNO , 
DP.NOMBRE,DP.FECHA_NACIMIENTO ,DP.RFC ,
DP.CURP ,DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),ENTFED.ID_ENTIDAD_FEDERATIVA ,
MUN.ID_MUNICIPIO,NTAB.NIVEL ,
TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA ,
DECODE(PL.ID_TIPO_NOMBRAM_FK,2,'SI','NO')
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_PLAZAS PL
ON EMP.ID_PLAZA = PL.ID_PLAZA
INNER JOIN RH_NOMN_TABULADORES TAB
ON PL.ID_TABULADOR_FK = TAB.ID_TABULADOR
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NTAB
ON TAB.ID_NIVEL_TABULADOR_FK = NTAB.ID_NIVEL_TABULADOR
INNER JOIN RH_NOMN_DIRECCIONES DIR
ON DP.ID_DIRECCION = DIR.ID_DIRECCION
INNER JOIN RH_NOMN_CAT_COLONIAS COLN
ON DIR.ID_COLONIA = COLN.ID_COLONIA
INNER JOIN RH_NOMN_CAT_MUNICIPIOS MUN
ON COLN.ID_MUNICIPIO = MUN.ID_MUNICIPIO
INNER JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED
ON MUN.ID_ENTIDAD_FEDERATIVA = ENTFED.ID_ENTIDAD_FEDERATIVA
WHERE SGMM.ID_SGMM =vIdSGMM;

CURSOR DESCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR ASCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR TASEGURADO IS 
SELECT DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
SGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(SEG.ACTIVO,'A','ALTA','I','BAJA'),
SGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR SPTITULAR IS
SELECT 
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular;

CURSOR SPDESCENDIENTES IS 
SELECT
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1 
ON EXTSGMM.ID_BENEFICIARIO_POLIZA =  BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GJ
ON BP.ID_GRPOS_JER_FK = GJ.ID_GRPOS_JER
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE
BP1.ID_BENEFICIARIO_POLIZA=EXTSGMM.ID_BENEFICIARIO_POLIZA
AND BP1.ID_SUMA_ASEGURADA_SGMM=SUMASG.ID_SUMA_ASEGURADA_SGMM
AND BP1.ID_GRPOS_JER_FK=GJ.ID_GRPOS_JER
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMDescendiente;

CURSOR SPASCENDIENTES IS 
SELECT 
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0),'B',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0))
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
INNER JOIN RH_NOMN_BASICA_POT_EDADES RBP
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = RBP.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_MONTO_ASC MASC
ON RBP.ID_BASICA_POT_EDAD = MASC.ID_BASICA_POT_EDAD
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
WHERE
RBP.ID_BENEFICIARIO_POLIZA = EXTSGMM.ID_BENEFICIARIO_POLIZA
AND RBP.ACTIVO=1
AND(SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) >= RBP.EDAD_MINIMA
AND (SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) <= RBP.EDAD_MAXIMA
AND MASC.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
AND MASC.ID_BASICA_POT_EDAD = RBP.ID_BASICA_POT_EDAD
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMAscendiente;


BEGIN 

 EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPAltasBajasSGMM';    





OPEN SGMM;
LOOP
FETCH SGMM INTO vIdSGMM;
EXIT WHEN SGMM%NOTFOUND;

OPEN TITULAR;
LOOP
FETCH TITULAR INTO vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual;
EXIT WHEN TITULAR%NOTFOUND;

OPEN TASEGURADO;
LOOP
FETCH TASEGURADO INTO vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN TASEGURADO%NOTFOUND;

OPEN SPTITULAR;
LOOP
FETCH SPTITULAR INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPTITULAR%NOTFOUND; 

INSERT INTO TMPAltasBajasSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada);

END LOOP;
CLOSE SPTITULAR;

END LOOP;
CLOSE TASEGURADO;

OPEN DESCENDIENTES;
LOOP
FETCH DESCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMDescendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN DESCENDIENTES%NOTFOUND;

OPEN SPDESCENDIENTES;
LOOP
FETCH SPDESCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPDESCENDIENTES%NOTFOUND;

/*EXECUTE IMMEDIATE 'INSERT INTO TMPAltasBajasSGMM VALUES('||vIdSGMMTitular||','||vAPTitular||','||vAMTitular||','
||vNOMTitular||','||vFNTitular||','||vRFCTitular||','||vCURPTitular||','
||vSexoTitular||','||vCodEntFedTitular||','||vCodMunTitular||','||vNivTabTitular||','
||vPercepTitular||','||vEventual||','||vAPAsegurado||','||vAMAsegurado||','
||vNOMAsegurado||','||vFNAsegurado||','||vCURPAsegurado||','||vSexoAsegurado||','
||vFAntAsegurado||','||vTipoAsegurado||','||vFColAsegurado||','||vAltaBaja||','||vFMovAsegurado||','
||vSumaAsegBasica||','||vPrimaBasica||','||vSumaAsegPotenciada||','||vPrimaPotenciada||')';*/
INSERT INTO TMPAltasBajasSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada);

END LOOP;
CLOSE SPDESCENDIENTES;



END LOOP;
CLOSE DESCENDIENTES;


OPEN ASCENDIENTES;
LOOP
FETCH ASCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMAscendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN ASCENDIENTES%NOTFOUND;

OPEN SPASCENDIENTES;
LOOP
FETCH SPASCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPASCENDIENTES%NOTFOUND;

/*EXECUTE IMMEDIATE 'INSERT INTO TMPAltasBajasSGMM VALUES('||vIdSGMMTitular||','||vAPTitular||','||vAMTitular||','
||vNOMTitular||','||vFNTitular||','||vRFCTitular||','||vCURPTitular||','
||vSexoTitular||','||vCodEntFedTitular||','||vCodMunTitular||','||vNivTabTitular||','
||vPercepTitular||','||vEventual||','||vAPAsegurado||','||vAMAsegurado||','
||vNOMAsegurado||','||vFNAsegurado||','||vCURPAsegurado||','||vSexoAsegurado||','
||vFAntAsegurado||','||vTipoAsegurado||','||vFColAsegurado||','||vAltaBaja||','||vFMovAsegurado||','
||vSumaAsegBasica||','||vPrimaBasica||','||vSumaAsegPotenciada||','||vPrimaPotenciada||')';*/
INSERT INTO TMPAltasBajasSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada);

END LOOP;
CLOSE SPASCENDIENTES;

END LOOP;
CLOSE ASCENDIENTES;


END LOOP;
CLOSE TITULAR;
END LOOP;
CLOSE SGMM;


OPEN REPORTE FOR
SELECT 
vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual,
vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada FROM TMPAltasBajasSGMM;




END REPORTEALTASBAJASSGMM;

/
--------------------------------------------------------
--  DDL for Procedure REPORTEPOTENCIACIONSGMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTEPOTENCIACIONSGMM" (fechaInicio DATE, fechaFin DATE,
                                             REPORTE OUT SYS_REFCURSOR)
IS 

vIdSGMM   NUMBER;
vIdSGMMTitular   NUMBER;
vIdExtSGMMDescendiente  NUMBER;
vIdExtSGMMAscendiente  NUMBER;

vAPTitular  VARCHAR2(40);
vAMTitular  VARCHAR2(40);
vNOMTitular  VARCHAR2(40);
vFNTitular  DATE;
vRFCTitular VARCHAR2(20);
vCURPTitular  VARCHAR2(20);
vSexoTitular  VARCHAR2(3);
vCodEntFedTitular NUMBER;
vCodMunTitular  NUMBER;
vNivTabTitular  VARCHAR2(3);
vPercepTitular  NUMBER;
vEventual VARCHAR2(3);

vAPAsegurado  VARCHAR2(40);
vAMAsegurado VARCHAR2(40);
vNOMAsegurado  VARCHAR2(40);
vFNAsegurado  DATE;
vCURPAsegurado  VARCHAR2(20);
vSexoAsegurado  VARCHAR2(3);
vFAntAsegurado  DATE;
vTipoAsegurado  VARCHAR2(2);
vFColAsegurado  DATE;
vAltaBaja VARCHAR2(5);
vFMovAsegurado  DATE;

vSumaAsegBasica NUMBER;
vPrimaBasica NUMBER;
vSumaAsegPotenciada NUMBER;
vPrimaPotenciada NUMBER;

vSumaAseguradaTotal NUMBER;
vPrimaPotenciadaTotal  NUMBER;
vIdGrupJer  NUMBER;
vIdSumAseg  NUMBER;
vIdTipoAsegurado  NUMBER;
vExisteSumA   NUMBER;

Tabla VARCHAR2(10000);
EXISTETABLA NUMBER;

CURSOR SGMM IS 
SELECT SGMM.ID_SGMM  FROM RH_NOMN_SGMM SGMM 
WHERE 
 SGMM.FECHA_ALTA_IFT 
BETWEEN fechaInicio AND fechaFin 
AND SGMM.COBERTURA='P'
ORDER BY SGMM.FECHA_ALTA_IFT;

CURSOR SGMM1 IS 
SELECT SGMM.ID_SGMM  FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
WHERE 
 SGMM.FECHA_ALTA_IFT 
BETWEEN fechaInicio AND fechaFin 
AND  EXTSGMM.ID_BENEFICIARIO_POLIZA=4
ORDER BY SGMM.FECHA_ALTA_IFT;


CURSOR TITULAR IS
SELECT SGMM.ID_SGMM, DP.A_PATERNO , DP.A_MATERNO , 
DP.NOMBRE,DP.FECHA_NACIMIENTO ,DP.RFC ,
DP.CURP ,DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),ENTFED.ID_ENTIDAD_FEDERATIVA ,
MUN.ID_MUNICIPIO,NTAB.NIVEL ,
TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA ,
DECODE(PL.ID_TIPO_NOMBRAM_FK,2,'SI','NO')
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_PLAZAS PL
ON EMP.ID_PLAZA = PL.ID_PLAZA
INNER JOIN RH_NOMN_TABULADORES TAB
ON PL.ID_TABULADOR_FK = TAB.ID_TABULADOR
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NTAB
ON TAB.ID_NIVEL_TABULADOR_FK = NTAB.ID_NIVEL_TABULADOR
INNER JOIN RH_NOMN_DIRECCIONES DIR
ON DP.ID_DIRECCION = DIR.ID_DIRECCION
INNER JOIN RH_NOMN_CAT_COLONIAS COLN
ON DIR.ID_COLONIA = COLN.ID_COLONIA
INNER JOIN RH_NOMN_CAT_MUNICIPIOS MUN
ON COLN.ID_MUNICIPIO = MUN.ID_MUNICIPIO
INNER JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED
ON MUN.ID_ENTIDAD_FEDERATIVA = ENTFED.ID_ENTIDAD_FEDERATIVA
WHERE SGMM.ID_SGMM =vIdSGMM;

CURSOR DESCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA ,BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR ASCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA , BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR TASEGURADO IS 
SELECT DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
SGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA ,BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(SEG.ACTIVO,'A','ALTA','I','BAJA'),
SGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR SPTITULAR IS
SELECT 
DECODE(SGMM.COBERTURA,'B',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular),'P',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular)),
DECODE(SGMM.COBERTURA,'B',BP.MONTO_BASICO,'P',BP.MONTO_BASICO),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'P',BP.MONTO_POTENCIADO,'B',0),
BP.ID_GRPOS_JER_FK
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular;

CURSOR SPDESCENDIENTES IS 
SELECT
DECODE(SGMM.COBERTURA,'B',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular),'P',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular)),
DECODE(SGMM.COBERTURA,'B',BP1.MONTO_BASICO,'P',BP1.MONTO_BASICO),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',BP.MONTO_POTENCIADO,'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1 
ON EXTSGMM.ID_BENEFICIARIO_POLIZA =  BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GJ
ON BP.ID_GRPOS_JER_FK = GJ.ID_GRPOS_JER
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE
BP1.ID_BENEFICIARIO_POLIZA=EXTSGMM.ID_BENEFICIARIO_POLIZA
AND BP1.ID_SUMA_ASEGURADA_SGMM=SUMASG.ID_SUMA_ASEGURADA_SGMM
AND BP1.ID_GRPOS_JER_FK=GJ.ID_GRPOS_JER
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMDescendiente;

CURSOR SPASCENDIENTES IS 
SELECT 
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,MASC.MONTO,0),'B',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,MASC.MONTO,0))
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
INNER JOIN RH_NOMN_BASICA_POT_EDADES RBP
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = RBP.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_MONTO_ASC MASC
ON RBP.ID_BASICA_POT_EDAD = MASC.ID_BASICA_POT_EDAD
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
WHERE
RBP.ID_BENEFICIARIO_POLIZA = EXTSGMM.ID_BENEFICIARIO_POLIZA
AND RBP.ACTIVO=1
AND(SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) >= RBP.EDAD_MINIMA
AND (SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) <= RBP.EDAD_MAXIMA
AND MASC.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
AND MASC.ID_BASICA_POT_EDAD = RBP.ID_BASICA_POT_EDAD
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMAscendiente;


BEGIN 

 EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPPotenciadoSGMM';    





OPEN SGMM;
LOOP
FETCH SGMM INTO vIdSGMM;
EXIT WHEN SGMM%NOTFOUND;

OPEN TITULAR;
LOOP
FETCH TITULAR INTO vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual;
EXIT WHEN TITULAR%NOTFOUND;

OPEN TASEGURADO;
LOOP
FETCH TASEGURADO INTO vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN TASEGURADO%NOTFOUND;

OPEN SPTITULAR;
LOOP
FETCH SPTITULAR INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vIdGrupJer;
EXIT WHEN SPTITULAR%NOTFOUND; 

vSumaAseguradaTotal:=vSumaAsegBasica+vSumaAsegPotenciada;
DBMS_OUTPUT.PUT_LINE('vIdSGMMTitular '||vIdSGMMTitular);
DBMS_OUTPUT.PUT_LINE('vIdGrupJer '||vIdGrupJer);
DBMS_OUTPUT.PUT_LINE('vSumaAsegBasica '||vSumaAsegBasica);
DBMS_OUTPUT.PUT_LINE('vSumaAsegPotenciada '||vSumaAsegPotenciada);
DBMS_OUTPUT.PUT_LINE('vSumaAseguradaTotal '||vSumaAseguradaTotal);
SELECT COUNT(ID_SUMA_ASEGURADA_SGMM) INTO vExisteSumA FROM RH_NOMN_SUMA_ASEGURADA_SGMM 
WHERE DESCRIPCION_SUMA_ASEGURADA = vSumaAseguradaTotal;
IF(vExisteSumA>0)THEN
SELECT ID_SUMA_ASEGURADA_SGMM INTO vIdSumAseg FROM RH_NOMN_SUMA_ASEGURADA_SGMM 
WHERE DESCRIPCION_SUMA_ASEGURADA = vSumaAseguradaTotal;
ELSE
SELECT ID_SUMA_ASEGURADA_SGMM INTO vIdSumAseg FROM RH_NOMN_SUMA_ASEGURADA_SGMM 
WHERE DESCRIPCION_SUMA_ASEGURADA = vSumaAsegPotenciada;
END IF;
SELECT MONTO_POTENCIADO INTO vPrimaPotenciadaTotal FROM RH_NOMN_BASICA_POTENCIADO 
WHERE ID_SUMA_ASEGURADA_SGMM = vIdSumAseg AND ID_GRPOS_JER_FK= vIdGrupJer AND ID_BENEFICIARIO_POLIZA=vIdTipoAsegurado;



INSERT INTO TMPPotenciadoSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vSumaAseguradaTotal,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vSumaAseguradaTotal||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPTITULAR;

END LOOP;
CLOSE TASEGURADO;

OPEN DESCENDIENTES;
LOOP
FETCH DESCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMDescendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN DESCENDIENTES%NOTFOUND;

OPEN SPDESCENDIENTES;
LOOP
FETCH SPDESCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPDESCENDIENTES%NOTFOUND;

SELECT MONTO_POTENCIADO INTO vPrimaPotenciadaTotal FROM RH_NOMN_BASICA_POTENCIADO 
WHERE ID_SUMA_ASEGURADA_SGMM = vIdSumAseg AND ID_GRPOS_JER_FK= vIdGrupJer AND ID_BENEFICIARIO_POLIZA=vIdTipoAsegurado;


INSERT INTO TMPPotenciadoSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vSumaAseguradaTotal,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vSumaAseguradaTotal||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPDESCENDIENTES;



END LOOP;
CLOSE DESCENDIENTES;


OPEN ASCENDIENTES;
LOOP
FETCH ASCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMAscendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN ASCENDIENTES%NOTFOUND;

OPEN SPASCENDIENTES;
LOOP
FETCH SPASCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPASCENDIENTES%NOTFOUND;

SELECT MONTO_POTENCIADO INTO vPrimaPotenciadaTotal FROM RH_NOMN_BASICA_POTENCIADO 
WHERE ID_SUMA_ASEGURADA_SGMM = vIdSumAseg AND ID_GRPOS_JER_FK= vIdGrupJer AND ID_BENEFICIARIO_POLIZA=vIdTipoAsegurado;


INSERT INTO TMPPotenciadoSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vSumaAseguradaTotal,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE('ASCENDIENTE '||vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vSumaAseguradaTotal||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPASCENDIENTES;

END LOOP;
CLOSE ASCENDIENTES;


END LOOP;
CLOSE TITULAR;
END LOOP;
CLOSE SGMM;


------------------------------------------------------------------------------------------------------

OPEN SGMM1;
LOOP
FETCH SGMM1 INTO vIdSGMM;
EXIT WHEN SGMM1%NOTFOUND;

OPEN TITULAR;
LOOP
FETCH TITULAR INTO vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual;
EXIT WHEN TITULAR%NOTFOUND;

OPEN ASCENDIENTES;
LOOP
FETCH ASCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMAscendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN ASCENDIENTES%NOTFOUND;

OPEN SPASCENDIENTES;
LOOP
FETCH SPASCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPASCENDIENTES%NOTFOUND;

SELECT MONTO_POTENCIADO INTO vPrimaPotenciadaTotal FROM RH_NOMN_BASICA_POTENCIADO 
WHERE ID_SUMA_ASEGURADA_SGMM = vIdSumAseg AND ID_GRPOS_JER_FK= vIdGrupJer AND ID_BENEFICIARIO_POLIZA=vIdTipoAsegurado;


INSERT INTO TMPPotenciadoSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vSumaAseguradaTotal,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE('ASCENDIENTE '||vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vSumaAseguradaTotal||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPASCENDIENTES;

END LOOP;
CLOSE ASCENDIENTES;


END LOOP;
CLOSE TITULAR;
END LOOP;
CLOSE SGMM1;



OPEN REPORTE FOR
SELECT 
vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual,
vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vSumaAseguradaTotal,vPrimaPotenciadaTotal FROM TMPPotenciadoSGMM;




END REPORTEPOTENCIACIONSGMM;

/
--------------------------------------------------------
--  DDL for Procedure REPORTEPRESTAMOSCP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTEPRESTAMOSCP" (fechaInicio DATE, 
                                             REPORTE OUT SYS_REFCURSOR)
IS 

vQuincena VARCHAR2(10);
vIdEmpleado NUMBER;
vIdNomEmp NUMBER;
VNumEmp NUMBER;
vNumPagos NUMBER;
vNumPagado  NUMBER;

CURSOR C1 IS 
SELECT EJC.VALOR||'-'||CQ.NUMERO_QUINCENA, NEP.ID_EMPLEADO_FK, CEP.ID_NOMEMPPLA_FK,EMP.NUMERO_EMPLEADO,ROUND(MONTHS_BETWEEN
       (TO_DATE(MT.FECHA_FIN,'DD/MM/YY'),
        TO_DATE(MT.FECHA_INICIO,'DD/MM/YY') )*2)
FROM  RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
INNER JOIN RH_NOMN_EMPLEADOS EMP ON NEP.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_MANUALES_TERCEROS MT ON NEP.ID_EMPLEADO_FK = MT.ID_EMPLEADO
INNER JOIN RH_NOMN_CAT_CONCEPTOS CO ON MT.ID_CONCEPTO = CO.ID_CONCEPTO
INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB ON  NEP.ID_NOMINA_FK = CAB.ID_NOMINA
INNER JOIN RH_NOMN_CAT_QUINCENAS CQ ON CAB.ID_QUINCENA_FK = CQ.ID_QUINCENA
INNER JOIN RH_NOMN_CAT_EJERCICIOS EJC ON CQ.ID_EJERCICIO_FK = EJC.ID_EJERCICIO
INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CEP ON NEP.ID_NOMEMPPLA = CEP.ID_NOMEMPPLA_FK
WHERE CEP.ID_CONCEPTO_FK = CO.ID_CONCEPTO
AND CEP.ID_CONCEPTO_FK = MT.ID_CONCEPTO
AND (CO.CLAVE_CONCEPTO = '03' OR CO.CLAVE_CONCEPTO = '03E')
AND CQ.FECHA_INICIO_QUINCENA>=fechaInicio
ORDER BY TO_NUMBER(EMP.NUMERO_EMPLEADO),EJC.VALOR,CQ.NUMERO_QUINCENA;

BEGIN

 EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPPRESTAMOSCP';  

OPEN C1;
LOOP
FETCH C1 INTO vQuincena,vIdEmpleado,vIdNomEmp,VNumEmp,vNumPagos;
EXIT WHEN C1%NOTFOUND;

SELECT COUNT(CEP.ID_CONEMPPLA) INTO vNumPagado FROM  RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
INNER JOIN RH_NOMN_MANUALES_TERCEROS MT ON NEP.ID_EMPLEADO_FK = MT.ID_EMPLEADO
INNER JOIN RH_NOMN_CAT_CONCEPTOS CO ON MT.ID_CONCEPTO = CO.ID_CONCEPTO
INNER JOIN RH_NOMN_CABECERAS_NOMINAS CAB ON  NEP.ID_NOMINA_FK = CAB.ID_NOMINA
INNER JOIN RH_NOMN_CAT_QUINCENAS CQ ON CAB.ID_QUINCENA_FK = CQ.ID_QUINCENA
INNER JOIN RH_NOMN_CAT_EJERCICIOS EJC ON CQ.ID_EJERCICIO_FK = EJC.ID_EJERCICIO
INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CEP ON NEP.ID_NOMEMPPLA = CEP.ID_NOMEMPPLA_FK
WHERE CEP.ID_CONCEPTO_FK = CO.ID_CONCEPTO
AND CEP.ID_CONCEPTO_FK = MT.ID_CONCEPTO
AND (CO.CLAVE_CONCEPTO = '03' OR CO.CLAVE_CONCEPTO = '03E')
AND NEP.ID_EMPLEADO_FK = vIdEMpleado AND NEP.ID_NOMEMPPLA<=vIdNomEmp;

INSERT INTO TMPPRESTAMOSCP VALUES(vQuincena,vIdEmpleado,vIdNomEmp,VNumEmp,vNumPagos,vNumPagado);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vQuincena||' '||vIdEmpleado||' '||vIdNomEmp||' '||VNumEmp||' '||vNumPagos||' '||vNumPagado);

END LOOP;
CLOSE C1;

OPEN REPORTE FOR
SELECT vQuincena,vIdEmpleado,vIdNomEmp,VNumEmp,vNumPagos,vNumPagado FROM TMPPRESTAMOSCP ORDER BY VNUMEMP;

END;

/
--------------------------------------------------------
--  DDL for Procedure REPORTEPROMOCIONESSGMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTEPROMOCIONESSGMM" (fechaInicio DATE, fechaFin DATE,
                                             REPORTE OUT SYS_REFCURSOR)
IS 


vIdSGMM   NUMBER;
vIdEmpleadoSeg  NUMBER;
vNumEmpSeg    NUMBER;
vIdExtSGMMDescendiente  NUMBER;
vIdExtSGMMAscendiente  NUMBER;


vIdSGMMTitular   NUMBER;
vIdSGMMTitularANT   NUMBER;
vIdSGMMANT  NUMBER;
vIdExtSGMMDescendienteANT  NUMBER;
vIdExtSGMMAscendienteANT  NUMBER;

vAPTitular  VARCHAR2(40);
vAMTitular  VARCHAR2(40);
vNOMTitular  VARCHAR2(40);
vFNTitular  DATE;
vRFCTitular VARCHAR2(20);
vCURPTitular  VARCHAR2(20);
vSexoTitular  VARCHAR2(3);
vCodEntFedTitular NUMBER;
vCodMunTitular  NUMBER;
vNivTabTitular  VARCHAR2(3);
vPercepTitular  NUMBER;
vEventual VARCHAR2(3);


vNivTabTitularANT  VARCHAR2(3);
vPercepTitularANT  NUMBER;
vEventualANT VARCHAR2(3);
vFBajaTitularANT  DATE;

vAPAsegurado  VARCHAR2(40);
vAMAsegurado VARCHAR2(40);
vNOMAsegurado  VARCHAR2(40);
vFNAsegurado  DATE;
vCURPAsegurado  VARCHAR2(20);
vSexoAsegurado  VARCHAR2(3);
vFAntAsegurado  DATE;
vTipoAsegurado  VARCHAR2(2);
vFColAsegurado  DATE;
vAltaBaja VARCHAR2(5);
vFMovAsegurado  DATE;

vSumaAsegBasica NUMBER;
vPrimaBasica NUMBER;
vSumaAsegPotenciada NUMBER;
vPrimaPotenciada NUMBER;

vFAntAseguradoANT  DATE;
vTipoAseguradoANT  VARCHAR2(2);
vFColAseguradoANT  DATE;
vAltaBajaANT VARCHAR2(5);
vFMovAseguradoANT  DATE;


vSumaAsegBasicaANT NUMBER;
vPrimaBasicaANT NUMBER;
vSumaAsegPotenciadaANT NUMBER;
vPrimaPotenciadaANT NUMBER;


CURSOR NTITULAR IS SELECT SGMM.ID_SGMM ,SEG.ID_EMPLEADO_FK, EMP.NUMERO_EMPLEADO
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP 
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
WHERE SGMM.FECHA_ALTA_IFT BETWEEN fechaInicio AND fechaFin
ORDER BY SEG.ID_EMPLEADO_FK DESC;

CURSOR IDEMP IS SELECT  MAX(SGMM.ID_SGMM)
FROM RH_NOMN_EMPLEADOS EMP
INNER JOIN RH_NOMN_SEGUROS SEG
ON EMP.ID_EMPLEADO = SEG.ID_EMPLEADO_FK
INNER JOIN RH_NOMN_SGMM SGMM
ON SEG.ID_SEGURO = SGMM.ID_SEGURO
WHERE  EMP.NUMERO_EMPLEADO = vNumEmpSeg
AND EMP.ID_EMPLEADO < vIdEmpleadoSeg;

CURSOR TITULAR IS
SELECT SGMM.ID_SGMM, DP.A_PATERNO , DP.A_MATERNO , 
DP.NOMBRE,DP.FECHA_NACIMIENTO ,DP.RFC ,
DP.CURP ,DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),ENTFED.ID_ENTIDAD_FEDERATIVA ,
MUN.ID_MUNICIPIO,NTAB.NIVEL AS NTAB_ACTUAL,
TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA ,
DECODE(PL.ID_TIPO_NOMBRAM_FK,2,'SI','NO')
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_PLAZAS PL
ON EMP.ID_PLAZA = PL.ID_PLAZA
INNER JOIN RH_NOMN_TABULADORES TAB
ON PL.ID_TABULADOR_FK = TAB.ID_TABULADOR
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NTAB
ON TAB.ID_NIVEL_TABULADOR_FK = NTAB.ID_NIVEL_TABULADOR
INNER JOIN RH_NOMN_DIRECCIONES DIR
ON DP.ID_DIRECCION = DIR.ID_DIRECCION
INNER JOIN RH_NOMN_CAT_COLONIAS COLN
ON DIR.ID_COLONIA = COLN.ID_COLONIA
INNER JOIN RH_NOMN_CAT_MUNICIPIOS MUN
ON COLN.ID_MUNICIPIO = MUN.ID_MUNICIPIO
INNER JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED
ON MUN.ID_ENTIDAD_FEDERATIVA = ENTFED.ID_ENTIDAD_FEDERATIVA
WHERE SGMM.ID_SGMM =vIdSGMM;


CURSOR TITULARANT IS
SELECT SGMM.ID_SGMM,NTAB.NIVEL AS NTAB_ANTERIOR,
TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA ,
DECODE(PL.ID_TIPO_NOMBRAM_FK,2,'SI','NO'),EMP.FECHA_BAJA
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_PLAZAS PL
ON EMP.ID_PLAZA = PL.ID_PLAZA
INNER JOIN RH_NOMN_TABULADORES TAB
ON PL.ID_TABULADOR_FK = TAB.ID_TABULADOR
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NTAB
ON TAB.ID_NIVEL_TABULADOR_FK = NTAB.ID_NIVEL_TABULADOR
INNER JOIN RH_NOMN_DIRECCIONES DIR
ON DP.ID_DIRECCION = DIR.ID_DIRECCION
INNER JOIN RH_NOMN_CAT_COLONIAS COLN
ON DIR.ID_COLONIA = COLN.ID_COLONIA
INNER JOIN RH_NOMN_CAT_MUNICIPIOS MUN
ON COLN.ID_MUNICIPIO = MUN.ID_MUNICIPIO
INNER JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED
ON MUN.ID_ENTIDAD_FEDERATIVA = ENTFED.ID_ENTIDAD_FEDERATIVA
WHERE SGMM.ID_SGMM =vIdSGMMANT;

CURSOR TASEGURADO IS 
SELECT DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
SGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(SEG.ACTIVO,'A','ALTA','I','BAJA'),
SGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR SPTITULAR IS
SELECT 
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular;



CURSOR SPTITULARANT IS
SELECT 
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-SGMM.FECHA_ALTA_IFT,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EMP.FECHA_BAJA,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitularANT;


CURSOR DESCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMM;

CURSOR SPDESCENDIENTES IS 
SELECT
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1 
ON EXTSGMM.ID_BENEFICIARIO_POLIZA =  BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GJ
ON BP.ID_GRPOS_JER_FK = GJ.ID_GRPOS_JER
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE
BP1.ID_BENEFICIARIO_POLIZA=EXTSGMM.ID_BENEFICIARIO_POLIZA
AND BP1.ID_SUMA_ASEGURADA_SGMM=SUMASG.ID_SUMA_ASEGURADA_SGMM
AND BP1.ID_GRPOS_JER_FK=GJ.ID_GRPOS_JER
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMDescendiente;



CURSOR DESCENDIENTESANT IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMANT;




CURSOR SPDESCENDIENTESANT IS 
SELECT
DECODE(SGMM.COBERTURA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'B',DECODE(SEG.ACTIVO,'A',ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP1.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_BASICO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2))),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(SEG.ACTIVO,'A',ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((BP.MONTO_POTENCIADO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1 
ON EXTSGMM.ID_BENEFICIARIO_POLIZA =  BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GJ
ON BP.ID_GRPOS_JER_FK = GJ.ID_GRPOS_JER
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE
BP1.ID_BENEFICIARIO_POLIZA=EXTSGMM.ID_BENEFICIARIO_POLIZA
AND BP1.ID_SUMA_ASEGURADA_SGMM=SUMASG.ID_SUMA_ASEGURADA_SGMM
AND BP1.ID_GRPOS_JER_FK=GJ.ID_GRPOS_JER
AND SGMM.ID_SGMM=vIdSGMMANT
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMDescendienteANT;


CURSOR ASCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMM;


CURSOR SPASCENDIENTES IS 
SELECT 
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0),'B',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0))
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
INNER JOIN RH_NOMN_BASICA_POT_EDADES RBP
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = RBP.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_MONTO_ASC MASC
ON RBP.ID_BASICA_POT_EDAD = MASC.ID_BASICA_POT_EDAD
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
WHERE
RBP.ID_BENEFICIARIO_POLIZA = EXTSGMM.ID_BENEFICIARIO_POLIZA
AND RBP.ACTIVO=1
AND(SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) >= RBP.EDAD_MINIMA
AND (SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) <= RBP.EDAD_MAXIMA
AND MASC.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
AND MASC.ID_BASICA_POT_EDAD = RBP.ID_BASICA_POT_EDAD
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMAscendiente;


CURSOR ASCENDIENTESANT IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM, 
EXTSGMM.FECHA_ANTIGUEDAD, BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMANT;


CURSOR SPASCENDIENTESANT IS 
SELECT 
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0),'B',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,DECODE(SEG.ACTIVO,'A',ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2),ROUND(((MASC.MONTO/15)*ROUND(fechaFin-EXTSGMM.FECHA_ANTIGUEDAD,0)),2)),0))
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
INNER JOIN RH_NOMN_BASICA_POT_EDADES RBP
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = RBP.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_MONTO_ASC MASC
ON RBP.ID_BASICA_POT_EDAD = MASC.ID_BASICA_POT_EDAD
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
WHERE
RBP.ID_BENEFICIARIO_POLIZA = EXTSGMM.ID_BENEFICIARIO_POLIZA
AND RBP.ACTIVO=1
AND(SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) >= RBP.EDAD_MINIMA
AND (SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) <= RBP.EDAD_MAXIMA
AND MASC.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
AND MASC.ID_BASICA_POT_EDAD = RBP.ID_BASICA_POT_EDAD
AND SGMM.ID_SGMM=vIdSGMMTitularANT
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMAscendienteANT;


BEGIN

 EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPPromocionesSGMM';    


OPEN NTITULAR;
LOOP
FETCH NTITULAR INTO vIdSGMM,vIdEmpleadoSeg, vNumEmpSeg;
EXIT WHEN NTITULAR%NOTFOUND;

OPEN TITULAR;
LOOP
FETCH TITULAR INTO vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual;
EXIT WHEN TITULAR%NOTFOUND;

OPEN IDEMP;
LOOP
FETCH IDEMP INTO vIdSGMMANT;
EXIT WHEN IDEMP%NOTFOUND;

OPEN TITULARANT;
LOOP
FETCH TITULARANT INTO vIdSGMMTitularANT,vNivTabTitularANT,vPercepTitularANT,vEventualANT,vFBajaTitularANT;
EXIT WHEN TITULARANT%NOTFOUND;


OPEN TASEGURADO;
LOOP
FETCH TASEGURADO INTO vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN TASEGURADO%NOTFOUND;

OPEN SPTITULAR;
LOOP
FETCH SPTITULAR INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPTITULAR%NOTFOUND; 



OPEN SPTITULARANT;
LOOP
FETCH SPTITULARANT INTO vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT;
EXIT WHEN SPTITULARANT%NOTFOUND; 
INSERT INTO TMPPromocionesSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,
vFNTitular,vRFCTitular,vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vNivTabTitularANT,vPercepTitularANT,vEventualANT,vFBajaTitularANT,vAPAsegurado,
vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vFAntAseguradoANT,vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,
vFMovAseguradoANT,vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT);
COMMIT;

DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vNivTabTitularANT||' '||
vPercepTitular||' '||vPercepTitularANT||' '||vEventual||' '||vFBajaTitularANT||' '||vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||
vTipoAseguradoANT||' '||vFColAseguradoANT||' '||vAltaBajaANT||' '||vFMovAseguradoANT||' '||vSumaAsegBasicaANT||' '||vPrimaBasicaANT||' '||
vSumaAsegPotenciadaANT||' '||vPrimaPotenciadaANT);

END LOOP;
CLOSE SPTITULARANT;

END LOOP;
CLOSE SPTITULAR;

END LOOP;
CLOSE TASEGURADO;

OPEN DESCENDIENTES;
LOOP
FETCH DESCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMDescendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN DESCENDIENTES%NOTFOUND;

OPEN SPDESCENDIENTES;
LOOP
FETCH SPDESCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPDESCENDIENTES%NOTFOUND;


OPEN DESCENDIENTESANT;
LOOP
FETCH DESCENDIENTESANT INTO vIdSGMMTitularANT,vIdExtSGMMDescendienteANT,vFAntAseguradoANT,
vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,vFMovAseguradoANT;
EXIT WHEN DESCENDIENTESANT%NOTFOUND;

OPEN SPDESCENDIENTESANT;
LOOP
FETCH SPDESCENDIENTESANT INTO vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT;
EXIT WHEN SPDESCENDIENTESANT%NOTFOUND;
INSERT INTO TMPPromocionesSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,
vFNTitular,vRFCTitular,vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vNivTabTitularANT,vPercepTitularANT,vEventualANT,vFBajaTitularANT,vAPAsegurado,
vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vFAntAseguradoANT,vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,
vFMovAseguradoANT,vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vNivTabTitularANT||' '||
vPercepTitular||' '||vPercepTitularANT||' '||vEventual||' '||vFBajaTitularANT||' '||vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||
vTipoAseguradoANT||' '||vFColAseguradoANT||' '||vAltaBajaANT||' '||vFMovAseguradoANT||' '||vSumaAsegBasicaANT||' '||vPrimaBasicaANT||' '||
vSumaAsegPotenciadaANT||' '||vPrimaPotenciadaANT);
END LOOP;
CLOSE SPDESCENDIENTESANT;



END LOOP;
CLOSE DESCENDIENTESANT;

END LOOP;
CLOSE SPDESCENDIENTES;



END LOOP;
CLOSE DESCENDIENTES;


OPEN ASCENDIENTES;
LOOP
FETCH ASCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMAscendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN ASCENDIENTES%NOTFOUND;

OPEN SPASCENDIENTES;
LOOP
FETCH SPASCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPASCENDIENTES%NOTFOUND;


OPEN ASCENDIENTESANT;
LOOP
FETCH ASCENDIENTESANT INTO vIdSGMMTitularANT,vIdExtSGMMDescendienteANT,vFAntAseguradoANT,
vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,vFMovAseguradoANT;
EXIT WHEN ASCENDIENTESANT%NOTFOUND;

OPEN SPASCENDIENTESANT;
LOOP
FETCH SPASCENDIENTESANT INTO vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT;
EXIT WHEN SPASCENDIENTESANT%NOTFOUND;
INSERT INTO TMPPromocionesSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,
vFNTitular,vRFCTitular,vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vNivTabTitularANT,vPercepTitularANT,vEventualANT,vFBajaTitularANT,vAPAsegurado,
vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vFAntAseguradoANT,vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,
vFMovAseguradoANT,vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vNivTabTitularANT||' '||
vPercepTitular||' '||vPercepTitularANT||' '||vEventual||' '||vFBajaTitularANT||' '||vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||
vTipoAseguradoANT||' '||vFColAseguradoANT||' '||vAltaBajaANT||' '||vFMovAseguradoANT||' '||vSumaAsegBasicaANT||' '||vPrimaBasicaANT||' '||
vSumaAsegPotenciadaANT||' '||vPrimaPotenciadaANT);
END LOOP;
CLOSE SPASCENDIENTESANT;


END LOOP;
CLOSE ASCENDIENTESANT;

END LOOP;
CLOSE SPASCENDIENTES;

END LOOP;
CLOSE ASCENDIENTES;


END LOOP;
CLOSE TITULARANT;

END LOOP;
CLOSE IDEMP;

END LOOP;
CLOSE TITULAR;

END LOOP;
CLOSE NTITULAR;

OPEN REPORTE FOR
SELECT 
vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,
vFNTitular,vRFCTitular,vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vNivTabTitularANT,vPercepTitularANT,vEventualANT,vFBajaTitularANT,vAPAsegurado,
vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vFAntAseguradoANT,vTipoAseguradoANT,vFColAseguradoANT,vAltaBajaANT,
vFMovAseguradoANT,vSumaAsegBasicaANT,vPrimaBasicaANT,vSumaAsegPotenciadaANT,vPrimaPotenciadaANT FROM TMPPromocionesSGMM;


END REPORTEPROMOCIONESSGMM;

/
--------------------------------------------------------
--  DDL for Procedure REPORTES_CANCELACHEQUE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTES_CANCELACHEQUE" (REPORTE OUT SYS_REFCURSOR)
IS 

BEGIN

OPEN REPORTE FOR

SELECT (SELECT EXTRACT (DAY FROM SYSDATE)FROM DUAL) DIA,
       (SELECT EXTRACT (MONTH FROM SYSDATE)FROM DUAL)MES,
       (SELECT EXTRACT (YEAR FROM SYSDATE)FROM DUAL)A�O,
       ROWNUM NUM,DESCRIPCION_CICLO,NO_DE_CHEQUE,NOMBRE_DEL_TRABAJADOR,BASE,CONFIANZA,SINDICATO,GOB_FED_NO_CAPITALIZABLE,
      ROUND((NVL(BASE,0)+NVL(CONFIANZA,0)+NVL(GOB_FED_NO_CAPITALIZABLE,0)) * NVL(RENDIMIENTOS,0),2) AS RENDIMIENTO,
      (NVL(BASE,0)+NVL(CONFIANZA,0)+NVL(GOB_FED_NO_CAPITALIZABLE,0))+
      ROUND(((NVL(BASE,0)+NVL(CONFIANZA,0)+NVL(GOB_FED_NO_CAPITALIZABLE,0))* NVL(RENDIMIENTOS,0)),2) IMPORTE_CHEQUE,
      2 MOTIVO
FROM
(
SELECT DISTINCT FC.DESCRIPCION_CICLO ,'' NO_DE_CHEQUE ,UPPER(DT.NOMBRE||' '||DT.A_PATERNO ||' '||DT.A_MATERNO)  NOMBRE_DEL_TRABAJADOR
      ,CASE WHEN PL.ID_TIPO_NOMBRAM_FK = 1 THEN AP.APORTACION_EMPLEADO END BASE
      ,CASE WHEN PL.ID_TIPO_NOMBRAM_FK = 2 OR PL.ID_TIPO_NOMBRAM_FK = 3 THEN AP.APORTACION_EMPLEADO END CONFIANZA 
      ,'' SINDICATO      ,AP.APORTACION_GOB_FED_NO_CAP GOB_FED_NO_CAPITALIZABLE 
      ----------CASE PERIOSO ORDINARIO O EXTRAORDINARIO-----------
      ,CASE WHEN EP.FECHA_INSCRIPCION_ALTA BETWEEN FC.VIGENCIA_INICIO  AND FC.VIGENCIA_FIN THEN 
      (SELECT RF.PORC_REND_ORD_CICLO_ACTUAL FROM RH_NOMN_FONAC_RENDIMIENTOS  RF  
        WHERE  RF.ID_EJERCICIO          = (SELECT ID_EJERCICIO  FROM RH_NOMN_CAT_EJERCICIOS 
                                                    WHERE VALOR =(SELECT EXTRACT (YEAR FROM SYSDATE)FROM DUAL))
        AND    RF.ID_MES                = (SELECT EXTRACT (MONTH FROM SYSDATE) FROM DUAL)
        AND    RF.ESTATUS ='A') 
        ELSE 
        (SELECT RF.PORC_REND_EXTRA_CICLO_ACTUAL  FROM RH_NOMN_FONAC_RENDIMIENTOS  RF  
        WHERE  RF.ID_EJERCICIO          = (SELECT ID_EJERCICIO  FROM RH_NOMN_CAT_EJERCICIOS 
                                                    WHERE VALOR =(SELECT EXTRACT (YEAR FROM SYSDATE)FROM DUAL))
        AND    RF.ID_MES                = (SELECT EXTRACT (MONTH FROM SYSDATE) FROM DUAL)
        AND    RF.ESTATUS ='A')
      END RENDIMIENTOS
FROM   RH_NOMN_DATOS_PERSONALES       DT,
       RH_NOMN_EMPLEADOS              EM,
       RH_NOMN_PLAZAS                 PL,
       RH_NOMN_FONAC_APORTACIONES     AP,
       RH_NOMN_FONAC_EMPLEADOS_PADRON EP,
       RH_NOMN_FONAC_RENDIMIENTOS     RE,
       RH_NOMN_FONAC_CICLOS           FC
WHERE EM.ID_DATOS_PERSONALES   = DT.ID_DATOS_PERSONALES
  AND EM.ID_PLAZA              = PL.ID_PLAZA
  AND EM.ID_EMPLEADO           = EP.ID_EMPLEADO
  AND EP.ID_BAJA_FONAC IS NULL
  AND AP.ID_FONAC_APORTACION   = EP.ID_FONAC_APORTACION
  AND FC.ID_ESTATUS_FONAC_FK =2
);

END REPORTES_CANCELACHEQUE;

/
--------------------------------------------------------
--  DDL for Procedure REPORTES_LIC_MEDICAS_DES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTES_LIC_MEDICAS_DES" (IDEMPLEADO IN NUMBER,COBROS OUT SYS_REFCURSOR) IS
BEGIN

OPEN COBROS FOR
                SELECT SERIE,
                       FECHA_INICIO,
                       FECHA_FIN,
                       DIAS_OTORGADOS,
                       ACUMULADO,
                       DIAS100,
                       QUINCENA,
                       DIAS50,
                       DIAS0
                FROM(
                            SELECT  DISTINCT
                                    LIC.SERIE,
                                    TO_CHAR(LIC.FECHA_INICIO,'DD/MM/YY') FECHA_INICIO,
                                    TO_CHAR(LIC.FECHA_FIN,'DD/MM/YY') FECHA_FIN,
                                    LIC.DIAS_OTORGADOS,
                                    LICANT.ACUMULADO,
                                    DECODE(DES.PORCENTAJE,100,DES.NUMERO_DIAS,null ) DIAS100,
                                    QUIN.DESCRIPCION_QUINCENA QUINCENA,
                                    DECODE(DES.PORCENTAJE,50,DES.NUMERO_DIAS,null ) DIAS50,
                                    DECODE(DES.PORCENTAJE,0,DES.NUMERO_DIAS,null ) DIAS0
                                    
                            FROM RH_NOMN_LICENCIAS_MEDICAS LIC
                            LEFT JOIN RH_NOMN_LICENCIA_ANTIGUEDAD LICANT ON LICANT.ID_LICENCIA_MEDICA_FK = LIC.ID_LICENCIA_MEDICA
                            LEFT JOIN RH_NOMN_DESCUENTOS_LIC_MED DES ON DES.ID_LICENCIA_ANTIGUEDAD_FK = LICANT.ID_LICENCIA_ANTIGUEDAD
                            LEFT JOIN RH_NOMN_CAT_QUINCENAS QUIN ON QUIN.ID_QUINCENA = DES.ID_QUINCENA_FK 
                            WHERE LIC.ID_EMPLEADO_FK IN (SELECT ID_EMPLEADO FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = IDEMPLEADO)
                            AND LIC.ACTIVO = 'A'
                            ORDER BY FECHA_INICIO
                        )
                ;


END REPORTES_LIC_MEDICAS_DES;

/
--------------------------------------------------------
--  DDL for Procedure REPORTES_QUERYS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTES_QUERYS" (numEmpleado IN INTEGER,
                                             REPORTE OUT SYS_REFCURSOR)
IS 



BEGIN


OPEN REPORTE FOR

--NOMBRE,TRABAJADOR,DIAINGRESO_IFT,MESINGRESO_IFT,A�OINGRESO_IFT
--UNIDAD_ADMINISTRATIVA
--DENOMINACION_DEL_PUESTO
--NIVEL_SALARIAL
--ZONA_ECONOMICA
--ESTADO
--WITH ENTIDADFED AS(
--SELECT (F.DESCRIPCION_ENTIDAD_FEDERATIVA) AS ESTADO ,
--UPPER(B.NOMBRE || ' ' ||B.A_PATERNO ||' '||B.A_MATERNO) AS NOMBRE ,A.NUMERO_EMPLEADO AS TRABAJADOR
--,EXTRACT(DAY  FROM A.FECHA_DE_INGRESO) AS DIAINGRESO_IFT 
--,EXTRACT(MONTH FROM A.FECHA_DE_INGRESO) AS MESINGRESO_IFT 
--,EXTRACT(YEAR FROM A.FECHA_DE_INGRESO) AS A�OINGRESO_IFT 
--,(B.CURP) ,(B.RFC) ,
--(SELECT (A.DESCRIPCION_ESTADO_CIVIL) FROM RH_NOMN_CAT_ESTADOS_CIVIL A WHERE A.ID_ESTADO_CIVIL =   B.ID_ESTADO_CIVIL) ESTADO_CIVIL
--,(SELECT (A.DESCRIPCION_GENERO)  FROM RH_NOMN_CAT_GENEROS A WHERE A.ID_GENERO = B.ID_GENERO ) SEXO
--,(SELECT (A.DESCRIPCION_NACIONALIDAD) FROM RH_NOMN_CAT_NACIONALIDADES A WHERE A.ID_NACIONALIDAD = B.ID_NACIONALIDAD )NACIONALIDAD
--,TRUNC((to_number(to_date(sysdate,'dd/mm/rr')-to_date(B.FECHA_NACIMIENTO,'dd/mm/rr'))/365)) as  EDAD
--,C.CALLE||' '||C.NUM_EXTERIOR||' '||C.NUM_INTERIOR AS CALLENUM
--,D.DESCRIPCION_COLONIA AS COLONIA
--,E.DESCRIPCION_MUNICIPIO AS MUNICIPIO
--,(SELECT A.DESCRIPCION_CODIGO_POSTAL FROM RH_NOMN_CAT_CODIGO_POSTAL A WHERE A.ID_CODIGO_POSTAL = D.ID_CODIGO_POSTAL) CODIGO_POSTAL
--FROM RH_NOMN_EMPLEADOS  A
--JOIN RH_NOMN_DATOS_PERSONALES B
--ON (A.ID_DATOS_PERSONALES= B.ID_DATOS_PERSONALES)
--JOIN RH_NOMN_DIRECCIONES C
--ON (B.ID_DIRECCION= C.ID_DIRECCION)
--JOIN RH_NOMN_CAT_COLONIAS D
--ON(C.ID_COLONIA=D.ID_COLONIA)
--JOIN RH_NOMN_CAT_MUNICIPIOS E
--ON (D.ID_MUNICIPIO=E.ID_MUNICIPIO)
--JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA  F
--ON (E.ID_ENTIDAD_FEDERATIVA=F.ID_ENTIDAD_FEDERATIVA)
--WHERE A.ID_EMPLEADO= V_IDEMPLEADO)
--select X.NOMBRE,X.TRABAJADOR,X.DIAINGRESO_IFT,X.MESINGRESO_IFT,X.A�OINGRESO_IFT,X.CURP ,X.RFC ,UPPER(X.ESTADO_CIVIL)AS ESTADO_CIVIL,X.SEXO,X.NACIONALIDAD
--        ,X.ESTADO,X.EDAD,UPPER(X.CALLENUM) AS CALLENUM,UPPER(X.COLONIA) AS COLONIA ,UPPER(X.MUNICIPIO) AS MUNICIPIO,X.CODIGO_POSTAL
--       ,A.ID_PLAZA,B.ID_EMPLEADO,C.ID_UNIADM_FK ,
--      (SELECT UPPER (A.DESCRIPCION) FROM RH_NOMN_CAT_UNIDADES_ADMINS A WHERE A.ID_UNIADM = C.ID_UNIADM_FK) UNIDAD_ADMINISTRATIVA  
--      ,D.ID_PUESTO,(SELECT UPPER(A.DESCRIPCION) FROM RH_NOMN_CAT_PUESTOS A WHERE A.ID_PUESTO = D.ID_PUESTO) DENOMINACION_DEL_PUESTO
--      , (SELECT A.NIVEL  FROM RH_NOMN_CAT_NIVELES_TABULADOR  A WHERE A.ID_NIVEL_TABULADOR  = E.ID_NIVEL_TABULADOR_FK)NIVEL_SALARIAL
--      ,1 ZONA_ECONOMICA
--      ,E.COMPENSACION_GARANTIZADA,E.SUELDO_BASE
--     from ENTIDADFED X,RH_NOMN_PLAZAS A
--     JOIN  RH_NOMN_EMPLEADOS B
--ON (A.ID_PLAZA= B.ID_PLAZA)
--JOIN RH_NOMN_ADSCRIPCIONES C
--ON (A.ID_ADSCRIPCION_FK= C.ID_ADSCRIPCION)
--JOIN RH_NOMN_CAT_PUESTOS D
--ON(A.ID_PUESTO_FK = D.ID_PUESTO)
--JOIN RH_NOMN_TABULADORES E
--ON(A.ID_TABULADOR_FK = E.ID_TABULADOR)
--WHERE B.ID_EMPLEADO = V_IDEMPLEADO;




WITH ENTIDADFED AS (SELECT F.DESCRIPCION_ENTIDAD_FEDERATIVA AS ESTADO,
                    (select a.DESCRIPCION_ENTIDAD_FEDERATIVA from RH_NOMN_CAT_ENTIDAD_FEDERATIVA a
                    where  B1.ID_ENTIDAD_NACIMIENTO = a.ID_ENTIDAD_FEDERATIVA )  AS ESTADONACIMIENTO,
                           UPPER(B1.NOMBRE || ' ' || B1.A_PATERNO || ' ' || B1.A_MATERNO) AS NOMBRE, 
                           UPPER(B1.NOMBRE) AS NOMBRE2,
                           B1.TELEFONO_MOVIL,
                           UPPER(B1.A_PATERNO) as APATERNO,
                           UPPER(B1.A_MATERNO) AS AMATERNO,
                           A1.NUMERO_EMPLEADO AS TRABAJADOR, 
                           EXTRACT(DAY FROM A1.FECHA_DE_INGRESO) AS DIAINGRESO_IFT, 
                           EXTRACT(MONTH FROM A1.FECHA_DE_INGRESO) AS MESINGRESO_IFT, 
                           EXTRACT(YEAR FROM A1.FECHA_DE_INGRESO) AS A�OINGRESO_IFT, 
                           B1.CURP, 
                           B1.RFC,
                          (SELECT A.DESCRIPCION_ESTADO_CIVIL 
                              FROM RH_NOMN_CAT_ESTADOS_CIVIL A 
                             WHERE A.ID_ESTADO_CIVIL = B1.ID_ESTADO_CIVIL) ESTADO_CIVIL, 
                              CASE WHEN B1.ID_ESTADO_CIVIL = 2 THEN 'CASADO(A)' ELSE 'SOLTERO(A)' END AS ESTADO_CIVIL2,
                                   CASE WHEN (LENGTH(B1.RFC)= 13) THEN SUBSTR(B1.RFC,1,10) ELSE  B1.RFC END AS RFC2,
                           (SELECT A.DESCRIPCION_GENERO 
                              FROM RH_NOMN_CAT_GENEROS A 
                             WHERE A.ID_GENERO = B1.ID_GENERO) SEXO, 
                            CASE WHEN (B1.ID_GENERO = 1) THEN 'X' ELSE NULL END AS SEXO1,
                            CASE WHEN (B1.ID_GENERO = 2) THEN 'X' ELSE NULL END AS SEXO2,
                           (SELECT A.DESCRIPCION_NACIONALIDAD 
                              FROM RH_NOMN_CAT_NACIONALIDADES A 
                             WHERE A.ID_NACIONALIDAD = B1.ID_NACIONALIDAD) NACIONALIDAD, 
                           TRUNC(TO_NUMBER(TO_DATE(SYSDATE, 'dd/mm/rr') - TO_DATE(B1.FECHA_NACIMIENTO, 'dd/mm/rr')) / 365) AS EDAD, 
                           C1.CALLE || ' ' || C1.NUM_EXTERIOR || ' ' || C1.NUM_INTERIOR AS CALLENUM, 
                           C1.CALLE AS CALLE,C1.NUM_EXTERIOR AS CNUME,C1.NUM_INTERIOR AS CNUMI,
                           D1.DESCRIPCION_COLONIA AS COLONIA, 
                           E1.DESCRIPCION_MUNICIPIO AS MUNICIPIO, 
                           (SELECT A.DESCRIPCION_CODIGO_POSTAL 
                              FROM RH_NOMN_CAT_CODIGO_POSTAL A 
                             WHERE A.ID_CODIGO_POSTAL = D1.ID_CODIGO_POSTAL) CODIGO_POSTAL 
                      FROM RH_NOMN_EMPLEADOS A1, 
                           RH_NOMN_DATOS_PERSONALES B1, 
                           RH_NOMN_DIRECCIONES C1, 
                           RH_NOMN_CAT_COLONIAS D1, 
                           RH_NOMN_CAT_MUNICIPIOS E1, 
                           RH_NOMN_CAT_ENTIDAD_FEDERATIVA F 
                     WHERE A1.NUMERO_EMPLEADO = numEmpleado
                       AND E1.ID_ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA 
                       AND D1.ID_MUNICIPIO = E1.ID_MUNICIPIO 
                       AND C1.ID_COLONIA = D1.ID_COLONIA 
                       AND B1.ID_DIRECCION = C1.ID_DIRECCION 
                       AND A1.ID_DATOS_PERSONALES = B1.ID_DATOS_PERSONALES) 
SELECT ''|| EXTRACT(DAY FROM SYSDATE) ||' '||'DE '||RTRIM(UPPER(TO_CHAR(sysdate, 'Month')))||' DEL '||EXTRACT(YEAR FROM SYSDATE) LUGAR_FECHA
       ,X.NOMBRE,
       X.NOMBRE2,
       X.TELEFONO_MOVIL,
       X.APATERNO,
       X.AMATERNO, 
       X.TRABAJADOR, 
       X.DIAINGRESO_IFT, 
       X.MESINGRESO_IFT, 
       X.A�OINGRESO_IFT, 
       X.CURP, 
       X.RFC, 
       UPPER(X.ESTADO_CIVIL) AS ESTADO_CIVIL, 
       UPPER(X.ESTADO_CIVIL2) AS ESTADO_CIVIL2,
       X.RFC2,
       X.SEXO, 
       X.SEXO1,
       X.SEXO2,
       X.NACIONALIDAD, 
       X.ESTADO,
       X.ESTADONACIMIENTO,
       X.EDAD, 
       X.CALLE ,X.CNUME||X.CNUMI AS NUMEROSDCALLE,
       UPPER(X.CALLENUM) AS CALLENUM, 
       UPPER(X.COLONIA) AS COLONIA, 
       UPPER(X.MUNICIPIO) AS MUNICIPIO, 
       X.CODIGO_POSTAL, 
       A2.ID_PLAZA, 
       upper(case when (SELECT A.DESCRIPCION FROM RH_NOMN_CAT_TIPOS_NOMBRAMIENTO A WHERE A.ID_TIPO_NOMBRAMIENTO = A2.ID_TIPO_NOMBRAM_FK) = 'Estructura' then 'Confianza' end) NOMBRAMIENTO,
       B2.ID_EMPLEADO,
       B2.NSS, 
       C2.ID_UNIADM_FK, 
       (SELECT UPPER(A.DESCRIPCION) 
          FROM RH_NOMN_CAT_UNIDADES_ADMINS A 
         WHERE A.ID_UNIADM = C2.ID_UNIADM_FK) UNIDAD_ADMINISTRATIVA, 
       D2.ID_PUESTO, 
       (SELECT UPPER(A.DESCRIPCION) 
          FROM RH_NOMN_CAT_PUESTOS A 
         WHERE A.ID_PUESTO = A2.ID_PUESTO_FK) DENOMINACION_DEL_PUESTO, 
       (SELECT A.NIVEL 
          FROM RH_NOMN_CAT_NIVELES_TABULADOR A 
         WHERE A.ID_NIVEL_TABULADOR = E2.ID_NIVEL_TABULADOR_FK) NIVEL_SALARIAL, 
       1 ZONA_ECONOMICA, 
       E2.COMPENSACION_GARANTIZADA, 
       E2.SUELDO_BASE 
  FROM ENTIDADFED X, 
       RH_NOMN_PLAZAS A2, 
       RH_NOMN_EMPLEADOS B2, 
       RH_NOMN_ADSCRIPCIONES C2, 
       RH_NOMN_CAT_PUESTOS D2, 
       RH_NOMN_TABULADORES E2 
 WHERE B2.NUMERO_EMPLEADO = numEmpleado 
   AND A2.ID_TABULADOR_FK = E2.ID_TABULADOR 
   AND A2.ID_PUESTO_FK = D2.ID_PUESTO 
   AND A2.ID_ADSCRIPCION_FK = C2.ID_ADSCRIPCION 
   AND A2.ID_PLAZA = B2.ID_PLAZA;


END REPORTES_QUERYS;

/
--------------------------------------------------------
--  DDL for Procedure REPORTES_QUERYSALTAIMSS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTES_QUERYSALTAIMSS" (V_IDEMPLEADO IN INTEGER,
                                             REPORTE OUT SYS_REFCURSOR)
IS 



BEGIN


OPEN REPORTE FOR

--NOMBRE,TRABAJADOR,DIAINGRESO_IFT,MESINGRESO_IFT,A�OINGRESO_IFT
--UNIDAD_ADMINISTRATIVA
--DENOMINACION_DEL_PUESTO
--NIVEL_SALARIAL
--ZONA_ECONOMICA
--ESTADO
--WITH ENTIDADFED AS(
--SELECT (F.DESCRIPCION_ENTIDAD_FEDERATIVA) AS ESTADO ,
--UPPER(B.NOMBRE || ' ' ||B.A_PATERNO ||' '||B.A_MATERNO) AS NOMBRE ,A.NUMERO_EMPLEADO AS TRABAJADOR
--,EXTRACT(DAY  FROM A.FECHA_DE_INGRESO) AS DIAINGRESO_IFT 
--,EXTRACT(MONTH FROM A.FECHA_DE_INGRESO) AS MESINGRESO_IFT 
--,EXTRACT(YEAR FROM A.FECHA_DE_INGRESO) AS A�OINGRESO_IFT 
--,(B.CURP) ,(B.RFC) ,
--(SELECT (A.DESCRIPCION_ESTADO_CIVIL) FROM RH_NOMN_CAT_ESTADOS_CIVIL A WHERE A.ID_ESTADO_CIVIL =   B.ID_ESTADO_CIVIL) ESTADO_CIVIL
--,(SELECT (A.DESCRIPCION_GENERO)  FROM RH_NOMN_CAT_GENEROS A WHERE A.ID_GENERO = B.ID_GENERO ) SEXO
--,(SELECT (A.DESCRIPCION_NACIONALIDAD) FROM RH_NOMN_CAT_NACIONALIDADES A WHERE A.ID_NACIONALIDAD = B.ID_NACIONALIDAD )NACIONALIDAD
--,TRUNC((to_number(to_date(sysdate,'dd/mm/rr')-to_date(B.FECHA_NACIMIENTO,'dd/mm/rr'))/365)) as  EDAD
--,C.CALLE||' '||C.NUM_EXTERIOR||' '||C.NUM_INTERIOR AS CALLENUM
--,D.DESCRIPCION_COLONIA AS COLONIA
--,E.DESCRIPCION_MUNICIPIO AS MUNICIPIO
--,(SELECT A.DESCRIPCION_CODIGO_POSTAL FROM RH_NOMN_CAT_CODIGO_POSTAL A WHERE A.ID_CODIGO_POSTAL = D.ID_CODIGO_POSTAL) CODIGO_POSTAL
--FROM RH_NOMN_EMPLEADOS  A
--JOIN RH_NOMN_DATOS_PERSONALES B
--ON (A.ID_DATOS_PERSONALES= B.ID_DATOS_PERSONALES)
--JOIN RH_NOMN_DIRECCIONES C
--ON (B.ID_DIRECCION= C.ID_DIRECCION)
--JOIN RH_NOMN_CAT_COLONIAS D
--ON(C.ID_COLONIA=D.ID_COLONIA)
--JOIN RH_NOMN_CAT_MUNICIPIOS E
--ON (D.ID_MUNICIPIO=E.ID_MUNICIPIO)
--JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA  F
--ON (E.ID_ENTIDAD_FEDERATIVA=F.ID_ENTIDAD_FEDERATIVA)
--WHERE A.ID_EMPLEADO= V_IDEMPLEADO)
--select X.NOMBRE,X.TRABAJADOR,X.DIAINGRESO_IFT,X.MESINGRESO_IFT,X.A�OINGRESO_IFT,X.CURP ,X.RFC ,UPPER(X.ESTADO_CIVIL)AS ESTADO_CIVIL,X.SEXO,X.NACIONALIDAD
--        ,X.ESTADO,X.EDAD,UPPER(X.CALLENUM) AS CALLENUM,UPPER(X.COLONIA) AS COLONIA ,UPPER(X.MUNICIPIO) AS MUNICIPIO,X.CODIGO_POSTAL
--       ,A.ID_PLAZA,B.ID_EMPLEADO,C.ID_UNIADM_FK ,
--      (SELECT UPPER (A.DESCRIPCION) FROM RH_NOMN_CAT_UNIDADES_ADMINS A WHERE A.ID_UNIADM = C.ID_UNIADM_FK) UNIDAD_ADMINISTRATIVA  
--      ,D.ID_PUESTO,(SELECT UPPER(A.DESCRIPCION) FROM RH_NOMN_CAT_PUESTOS A WHERE A.ID_PUESTO = D.ID_PUESTO) DENOMINACION_DEL_PUESTO
--      , (SELECT A.NIVEL  FROM RH_NOMN_CAT_NIVELES_TABULADOR  A WHERE A.ID_NIVEL_TABULADOR  = E.ID_NIVEL_TABULADOR_FK)NIVEL_SALARIAL
--      ,1 ZONA_ECONOMICA
--      ,E.COMPENSACION_GARANTIZADA,E.SUELDO_BASE
--     from ENTIDADFED X,RH_NOMN_PLAZAS A
--     JOIN  RH_NOMN_EMPLEADOS B
--ON (A.ID_PLAZA= B.ID_PLAZA)
--JOIN RH_NOMN_ADSCRIPCIONES C
--ON (A.ID_ADSCRIPCION_FK= C.ID_ADSCRIPCION)
--JOIN RH_NOMN_CAT_PUESTOS D
--ON(A.ID_PUESTO_FK = D.ID_PUESTO)
--JOIN RH_NOMN_TABULADORES E
--ON(A.ID_TABULADOR_FK = E.ID_TABULADOR)
--WHERE B.ID_EMPLEADO = V_IDEMPLEADO;





WITH ENTIDADFED AS (SELECT F.DESCRIPCION_ENTIDAD_FEDERATIVA AS ESTADO, 
                           UPPER(B1.NOMBRE || ' ' || B1.A_PATERNO || ' ' || B1.A_MATERNO) AS NOMBRE, 
                           UPPER(B1.A_PATERNO) as APATERNO,
                           UPPER(B1.A_MATERNO) AS AMATERNO,
                           A1.NUMERO_EMPLEADO AS TRABAJADOR, 
                           EXTRACT(DAY FROM A1.FECHA_DE_INGRESO) AS DIAINGRESO_IFT, 
                           EXTRACT(MONTH FROM A1.FECHA_DE_INGRESO) AS MESINGRESO_IFT, 
                           EXTRACT(YEAR FROM A1.FECHA_DE_INGRESO) AS A�OINGRESO_IFT, 
                           B1.CURP, 
                           CASE WHEN (LENGTH(B1.RFC)= 13) THEN SUBSTR(B1.RFC,1,10) ELSE  B1.RFC END AS RFC,
                           CASE WHEN B1.ID_ESTADO_CIVIL = 2 THEN 'CASADO(A)' ELSE 'SOLTERO(A)' END AS ESTADO_CIVIL,
--                           (SELECT A.DESCRIPCION_ESTADO_CIVIL 
--                              FROM RH_NOMN_CAT_ESTADOS_CIVIL A 
--                             WHERE A.ID_ESTADO_CIVIL = B1.ID_ESTADO_CIVIL) ESTADO_CIVIL, 
                           (SELECT A.DESCRIPCION_GENERO 
                              FROM RH_NOMN_CAT_GENEROS A 
                             WHERE A.ID_GENERO = B1.ID_GENERO) SEXO, 
                           (SELECT A.DESCRIPCION_NACIONALIDAD 
                              FROM RH_NOMN_CAT_NACIONALIDADES A 
                             WHERE A.ID_NACIONALIDAD = B1.ID_NACIONALIDAD) NACIONALIDAD, 
                           TRUNC(TO_NUMBER(TO_DATE(SYSDATE, 'dd/mm/rr') - TO_DATE(B1.FECHA_NACIMIENTO, 'dd/mm/rr')) / 365) AS EDAD, 
                           C1.CALLE || ' ' || C1.NUM_EXTERIOR || ' ' || C1.NUM_INTERIOR AS CALLENUM, 
                           D1.DESCRIPCION_COLONIA AS COLONIA, 
                           E1.DESCRIPCION_MUNICIPIO AS MUNICIPIO, 
                           (SELECT A.DESCRIPCION_CODIGO_POSTAL 
                              FROM RH_NOMN_CAT_CODIGO_POSTAL A 
                             WHERE A.ID_CODIGO_POSTAL = D1.ID_CODIGO_POSTAL) CODIGO_POSTAL 
                      FROM RH_NOMN_EMPLEADOS A1, 
                           RH_NOMN_DATOS_PERSONALES B1, 
                           RH_NOMN_DIRECCIONES C1, 
                           RH_NOMN_CAT_COLONIAS D1, 
                           RH_NOMN_CAT_MUNICIPIOS E1, 
                           RH_NOMN_CAT_ENTIDAD_FEDERATIVA F 
                     WHERE A1.ID_EMPLEADO = V_IDEMPLEADO 
                       AND E1.ID_ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA 
                       AND D1.ID_MUNICIPIO = E1.ID_MUNICIPIO 
                       AND C1.ID_COLONIA = D1.ID_COLONIA 
                       AND B1.ID_DIRECCION = C1.ID_DIRECCION 
                       AND A1.ID_DATOS_PERSONALES = B1.ID_DATOS_PERSONALES) 
SELECT ''|| EXTRACT(DAY FROM SYSDATE) ||' '||'DE '||RTRIM(UPPER(TO_CHAR(sysdate, 'Month')))||' DEL '||EXTRACT(YEAR FROM SYSDATE) LUGAR_FECHA
       ,X.NOMBRE,
       X.APATERNO,
       X.AMATERNO, 
       X.TRABAJADOR, 
       X.DIAINGRESO_IFT, 
       X.MESINGRESO_IFT, 
       X.A�OINGRESO_IFT, 
       X.CURP, 
       X.RFC, 
       UPPER(X.ESTADO_CIVIL) AS ESTADO_CIVIL, 
       X.SEXO, 
       X.NACIONALIDAD, 
       X.ESTADO, 
       X.EDAD, 
       UPPER(X.CALLENUM) AS CALLENUM, 
       UPPER(X.COLONIA) AS COLONIA, 
       UPPER(X.MUNICIPIO) AS MUNICIPIO, 
       X.CODIGO_POSTAL, 
       A2.ID_PLAZA, 
       B2.ID_EMPLEADO,
       B2.NSS, 
       C2.ID_UNIADM_FK, 
       (SELECT UPPER(A.DESCRIPCION) 
          FROM RH_NOMN_CAT_UNIDADES_ADMINS A 
         WHERE A.ID_UNIADM = C2.ID_UNIADM_FK) UNIDAD_ADMINISTRATIVA, 
       D2.ID_PUESTO, 
       (SELECT UPPER(A.DESCRIPCION) 
          FROM RH_NOMN_CAT_PUESTOS A 
         WHERE A.ID_PUESTO = D2.ID_PUESTO) DENOMINACION_DEL_PUESTO, 
       (SELECT A.NIVEL 
          FROM RH_NOMN_CAT_NIVELES_TABULADOR A 
         WHERE A.ID_NIVEL_TABULADOR = E2.ID_NIVEL_TABULADOR_FK) NIVEL_SALARIAL, 
       1 ZONA_ECONOMICA, 
       E2.COMPENSACION_GARANTIZADA, 
       E2.SUELDO_BASE 
  FROM ENTIDADFED X, 
       RH_NOMN_PLAZAS A2, 
       RH_NOMN_EMPLEADOS B2, 
       RH_NOMN_ADSCRIPCIONES C2, 
       RH_NOMN_CAT_PUESTOS D2, 
       RH_NOMN_TABULADORES E2 
 WHERE B2.ID_EMPLEADO = V_IDEMPLEADO 
   AND A2.ID_TABULADOR_FK = E2.ID_TABULADOR 
   AND A2.ID_PUESTO_FK = D2.ID_PUESTO 
   AND A2.ID_ADSCRIPCION_FK = C2.ID_ADSCRIPCION 
   AND A2.ID_PLAZA = B2.ID_PLAZA;



END REPORTES_QUERYSALTAIMSS;

/
--------------------------------------------------------
--  DDL for Procedure REPORTETRIMAJUSTESSGMM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."REPORTETRIMAJUSTESSGMM" (fechaInicio DATE, fechaFin DATE,
                                             REPORTE OUT SYS_REFCURSOR)
IS 

vIdSGMM   NUMBER;
vIdSGMMTitular   NUMBER;
vIdExtSGMMDescendiente  NUMBER;
vIdExtSGMMAscendiente  NUMBER;

vAPTitular  VARCHAR2(40);
vAMTitular  VARCHAR2(40);
vNOMTitular  VARCHAR2(40);
vFNTitular  DATE;
vRFCTitular VARCHAR2(20);
vCURPTitular  VARCHAR2(20);
vSexoTitular  VARCHAR2(3);
vCodEntFedTitular NUMBER;
vCodMunTitular  NUMBER;
vNivTabTitular  VARCHAR2(3);
vPercepTitular  NUMBER;
vEventual VARCHAR2(3);

vAPAsegurado  VARCHAR2(40);
vAMAsegurado VARCHAR2(40);
vNOMAsegurado  VARCHAR2(40);
vFNAsegurado  DATE;
vCURPAsegurado  VARCHAR2(20);
vSexoAsegurado  VARCHAR2(3);
vFAntAsegurado  DATE;
vTipoAsegurado  VARCHAR2(2);
vFColAsegurado  DATE;
vAltaBaja VARCHAR2(5);
vFMovAsegurado  DATE;

vSumaAsegBasica NUMBER;
vPrimaBasica NUMBER;
vSumaAsegPotenciada NUMBER;
vPrimaPotenciada NUMBER;


vPrimaPotenciadaTotal  NUMBER;
vIdGrupJer  NUMBER;
vIdSumAseg  NUMBER;
vIdTipoAsegurado  NUMBER;
vExisteSumA   NUMBER;

Tabla VARCHAR2(10000);
EXISTETABLA NUMBER;



CURSOR SGMM IS 
SELECT SGMM.ID_SGMM  FROM RH_NOMN_SGMM SGMM 
WHERE 
 SGMM.FECHA_ALTA_IFT 
BETWEEN fechaInicio AND fechaFin 
AND SGMM.COBERTURA='B'
ORDER BY SGMM.FECHA_ALTA_IFT;

CURSOR SGMM1 IS 
SELECT SGMM.ID_SGMM  FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
WHERE 
 SGMM.FECHA_ALTA_IFT 
BETWEEN fechaInicio AND fechaFin 
AND  EXTSGMM.ID_BENEFICIARIO_POLIZA=4
ORDER BY SGMM.FECHA_ALTA_IFT;


CURSOR TITULAR IS
SELECT SGMM.ID_SGMM, DP.A_PATERNO , DP.A_MATERNO , 
DP.NOMBRE,DP.FECHA_NACIMIENTO ,DP.RFC ,
DP.CURP ,DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),ENTFED.ID_ENTIDAD_FEDERATIVA ,
MUN.ID_MUNICIPIO,NTAB.NIVEL ,
TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA ,
DECODE(PL.ID_TIPO_NOMBRAM_FK,2,'SI','NO')
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_PLAZAS PL
ON EMP.ID_PLAZA = PL.ID_PLAZA
INNER JOIN RH_NOMN_TABULADORES TAB
ON PL.ID_TABULADOR_FK = TAB.ID_TABULADOR
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NTAB
ON TAB.ID_NIVEL_TABULADOR_FK = NTAB.ID_NIVEL_TABULADOR
INNER JOIN RH_NOMN_DIRECCIONES DIR
ON DP.ID_DIRECCION = DIR.ID_DIRECCION
INNER JOIN RH_NOMN_CAT_COLONIAS COLN
ON DIR.ID_COLONIA = COLN.ID_COLONIA
INNER JOIN RH_NOMN_CAT_MUNICIPIOS MUN
ON COLN.ID_MUNICIPIO = MUN.ID_MUNICIPIO
INNER JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED
ON MUN.ID_ENTIDAD_FEDERATIVA = ENTFED.ID_ENTIDAD_FEDERATIVA
WHERE SGMM.ID_SGMM =vIdSGMM;

CURSOR DESCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA ,BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR ASCENDIENTES IS 
SELECT SGMM.ID_SGMM,EXTSGMM.ID_EXT_SGMM,DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
EXTSGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA , BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(EXTSGMM.ACTIVO,1,'ALTA',0,'BAJA'),
EXTSGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR TASEGURADO IS 
SELECT DP.A_PATERNO,
DP.A_MATERNO, DP.NOMBRE, 
DP.FECHA_NACIMIENTO,DP.CURP, 
DECODE(RPAD(SUBSTR(CGEN.DESCRIPCION_GENERO,1,1),1),'M','H','F','M'),
SGMM.FECHA_ANTIGUEDAD,BPOL.ID_BENEFICIARIO_POLIZA ,BPOL.ESTRUCTURA_SGMM,
SGMM.FECHA_COLECTIVIDAD,
DECODE(SEG.ACTIVO,'A','ALTA','I','BAJA'),
SGMM.FECHA_ANTIGUEDAD
FROM RH_NOMN_SGMM SGMM
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EMP.ID_DATOS_PERSONALES = DP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_GENEROS CGEN
ON DP.ID_GENERO = CGEN.ID_GENERO
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BPOL.ID_BENEFICIARIO_POLIZA 
WHERE SGMM.ID_SGMM =vIdSGMMTitular;

CURSOR SPTITULAR IS
SELECT 
DECODE(SGMM.COBERTURA,'B',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular),'P',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular)),
DECODE(SGMM.COBERTURA,'B',BP.MONTO_BASICO,'P',BP.MONTO_BASICO),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',SUMASG.DESCRIPCION_SUMA_ASEGURADA),
DECODE(SGMM.COBERTURA,'P',BP.MONTO_POTENCIADO,'B',0),
BP.ID_GRPOS_JER_FK
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular;

CURSOR SPDESCENDIENTES IS 
SELECT
DECODE(SGMM.COBERTURA,'B',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular),'P',(SELECT MIN(TO_NUMBER(SUMASG1.DESCRIPCION_SUMA_ASEGURADA)) FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EMPLEADOS EMP
ON SEG.ID_EMPLEADO_FK =EMP.ID_EMPLEADO
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1
ON SGMM.ID_BENEFICIARIO_POLIZA_FK = BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GRP
ON BP1.ID_GRPOS_JER_FK = GRP.ID_GRUPO_JERARQUICO
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG1
ON BP1.ID_SUMA_ASEGURADA_SGMM = SUMASG1.ID_SUMA_ASEGURADA_SGMM
WHERE SGMM.ID_SGMM = vIdSGMMTitular)),
DECODE(SGMM.COBERTURA,'B',BP1.MONTO_BASICO,'P',BP1.MONTO_BASICO),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',BP.MONTO_POTENCIADO,'B',0)
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP1 
ON EXTSGMM.ID_BENEFICIARIO_POLIZA =  BP1.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_AGRUPACION_GRP_JER GJ
ON BP.ID_GRPOS_JER_FK = GJ.ID_GRPOS_JER
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
WHERE
BP1.ID_BENEFICIARIO_POLIZA=EXTSGMM.ID_BENEFICIARIO_POLIZA
AND BP1.ID_SUMA_ASEGURADA_SGMM=SUMASG.ID_SUMA_ASEGURADA_SGMM
AND BP1.ID_GRPOS_JER_FK=GJ.ID_GRPOS_JER
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMDescendiente;

CURSOR SPASCENDIENTES IS 
SELECT 
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'B',0,'P',0),
DECODE(SGMM.COBERTURA,'P',SUMASG.DESCRIPCION_SUMA_ASEGURADA,'B',0),
DECODE(SGMM.COBERTURA,'P',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,MASC.MONTO,0),'B',DECODE(EXTSGMM.ID_BENEFICIARIO_POLIZA,4,MASC.MONTO,0))
FROM RH_NOMN_SGMM SGMM 
INNER JOIN RH_NOMN_SEGUROS SEG 
ON SGMM.ID_SEGURO = SEG.ID_SEGURO
INNER JOIN RH_NOMN_EXTENSION_SGMM EXTSGMM
ON SGMM.ID_SGMM = EXTSGMM.ID_SGMM
INNER JOIN RH_NOMN_BASICA_POTENCIADO BP
ON SGMM.ID_BASICA_POT_FK = BP.ID_BASICA_POT
INNER JOIN RH_NOMN_BENEFICIARIO_POLIZA BPOL
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = BPOL.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_SUMA_ASEGURADA_SGMM SUMASG
ON BP.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
INNER JOIN RH_NOMN_BASICA_POT_EDADES RBP
ON EXTSGMM.ID_BENEFICIARIO_POLIZA = RBP.ID_BENEFICIARIO_POLIZA
INNER JOIN RH_NOMN_MONTO_ASC MASC
ON RBP.ID_BASICA_POT_EDAD = MASC.ID_BASICA_POT_EDAD
INNER JOIN RH_NOMN_DATOS_PERSONALES DP
ON EXTSGMM.ID_DATOS_PERSONALES_FK = DP.ID_DATOS_PERSONALES
WHERE
RBP.ID_BENEFICIARIO_POLIZA = EXTSGMM.ID_BENEFICIARIO_POLIZA
AND RBP.ACTIVO=1
AND(SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) >= RBP.EDAD_MINIMA
AND (SELECT TRUNC(((TO_CHAR(SYSDATE,'YYYYMMDD')-TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'))/10000),0)FROM DUAL) <= RBP.EDAD_MAXIMA
AND MASC.ID_SUMA_ASEGURADA_SGMM = SUMASG.ID_SUMA_ASEGURADA_SGMM
AND MASC.ID_BASICA_POT_EDAD = RBP.ID_BASICA_POT_EDAD
AND SGMM.ID_SGMM=vIdSGMMTitular
AND EXTSGMM.ID_EXT_SGMM=vIdExtSGMMAscendiente;


BEGIN 

 EXECUTE IMMEDIATE 'TRUNCATE TABLE TMPTRIMAJUSTESSGMM';    


OPEN SGMM;
LOOP
FETCH SGMM INTO vIdSGMM;
EXIT WHEN SGMM%NOTFOUND;

OPEN TITULAR;
LOOP
FETCH TITULAR INTO vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual;
EXIT WHEN TITULAR%NOTFOUND;

OPEN TASEGURADO;
LOOP
FETCH TASEGURADO INTO vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN TASEGURADO%NOTFOUND;

OPEN SPTITULAR;
LOOP
FETCH SPTITULAR INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vIdGrupJer;
EXIT WHEN SPTITULAR%NOTFOUND; 


DBMS_OUTPUT.PUT_LINE('vIdSGMMTitular '||vIdSGMMTitular);
DBMS_OUTPUT.PUT_LINE('vIdGrupJer '||vIdGrupJer);
DBMS_OUTPUT.PUT_LINE('vSumaAsegBasica '||vSumaAsegBasica);



vPrimaPotenciadaTotal:=(vPrimaBasica*6);

DBMS_OUTPUT.PUT_LINE('vPrimaBasica '||vPrimaBasica);
DBMS_OUTPUT.PUT_LINE('vPrimaPotenciadaTotal '||vPrimaPotenciadaTotal);

INSERT INTO TMPTRIMAJUSTESSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPTITULAR;

END LOOP;
CLOSE TASEGURADO;

OPEN DESCENDIENTES;
LOOP
FETCH DESCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMDescendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN DESCENDIENTES%NOTFOUND;

OPEN SPDESCENDIENTES;
LOOP
FETCH SPDESCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPDESCENDIENTES%NOTFOUND;

vPrimaPotenciadaTotal:=(vPrimaBasica*6);

INSERT INTO TMPTRIMAJUSTESSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE(vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPDESCENDIENTES;



END LOOP;
CLOSE DESCENDIENTES;


OPEN ASCENDIENTES;
LOOP
FETCH ASCENDIENTES INTO vIdSGMMTitular,vIdExtSGMMAscendiente,vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vIdTipoAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado;
EXIT WHEN ASCENDIENTES%NOTFOUND;

OPEN SPASCENDIENTES;
LOOP
FETCH SPASCENDIENTES INTO vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada;
EXIT WHEN SPASCENDIENTES%NOTFOUND;

vPrimaPotenciadaTotal:=(vPrimaBasica*6);

INSERT INTO TMPTRIMAJUSTESSGMM VALUES(vIdSGMMTitular,vAPTitular,vAMTitular,
vNOMTitular,vFNTitular,vRFCTitular,vCURPTitular,
vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,
vPercepTitular,vEventual,vAPAsegurado,vAMAsegurado,
vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,
vFAntAsegurado,vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,
vSumaAsegBasica,vPrimaBasica,vSumaAsegPotenciada,vPrimaPotenciada,vPrimaPotenciadaTotal);
COMMIT;
DBMS_OUTPUT.PUT_LINE('ASCENDIENTE '||vIdSGMMTitular||' '||vAPTitular||' '||vAMTitular||' '||vNOMTitular||' '||vFNTitular||' '||vRFCTitular||' '||
vCURPTitular||' '||vSexoTitular||' '||vCodEntFedTitular||' '||vCodMunTitular||' '||vNivTabTitular||' '||vPercepTitular||' '||vEventual||' '||
vAPAsegurado||' '||vAMAsegurado||' '||vNOMAsegurado||' '||vFNAsegurado||' '||vCURPAsegurado||' '||vSexoAsegurado||' '||vFAntAsegurado||' '||
vTipoAsegurado||' '||vFColAsegurado||' '||vAltaBaja||' '||vFMovAsegurado||' '||vSumaAsegBasica||' '||vPrimaBasica||' '||
vSumaAsegPotenciada||' '||vPrimaPotenciada||' '||vPrimaPotenciadaTotal);

END LOOP;
CLOSE SPASCENDIENTES;

END LOOP;
CLOSE ASCENDIENTES;


END LOOP;
CLOSE TITULAR;
END LOOP;
CLOSE SGMM;



OPEN REPORTE FOR
SELECT 
vIdSGMMTitular,vAPTitular,vAMTitular,vNOMTitular,vFNTitular,vRFCTitular,
vCURPTitular,vSexoTitular,vCodEntFedTitular,vCodMunTitular,vNivTabTitular,vPercepTitular,vEventual,
vAPAsegurado,vAMAsegurado,vNOMAsegurado,vFNAsegurado,vCURPAsegurado,vSexoAsegurado,vFAntAsegurado,
vTipoAsegurado,vFColAsegurado,vAltaBaja,vFMovAsegurado,vSumaAsegBasica,vPrimaBasica,
vSumaAsegPotenciada,vPrimaPotenciada,vPrimaPotenciadaTotal FROM TMPTRIMAJUSTESSGMM;




END REPORTETRIMAJUSTESSGMM;

/
--------------------------------------------------------
--  DDL for Procedure SP_ACTUALIZA_REPORTE_SERICA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_ACTUALIZA_REPORTE_SERICA" (IDNOMINA IN NUMBER, INARCHIVO IN BLOB)IS

BEGIN

    UPDATE RH_NOMN_ARCHIVOSERICA SET ARCHIVO = INARCHIVO
    WHERE ID_NOMINA_FK = IDNOMINA;


EXCEPTION

  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);

END SP_ACTUALIZA_REPORTE_SERICA;

/
--------------------------------------------------------
--  DDL for Procedure SP_ACTUALIZA_XML_MANUALES_TER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_ACTUALIZA_XML_MANUALES_TER" (prmIdManualesTerceros in Number, 
                                                                prmXML in VARCHAR2,
                                                                p_respuesta OUT INTEGER )
--RETURN Integer 
IS
  vRetorno Integer:=0;
  EXC_ERROR EXCEPTION;
P_ERRORCODE INTEGER;
P_ERRORDESC  VARCHAR2(500);
V_resp INTEGER := 1;

BEGIN 
  --vRetorno;
  UPDATE RH_NOMN_MANUALES_TERCEROS SET XML= prmXML WHERE ID_MANUALES_TERCEROS= prmIdManualesTerceros;
  commit;

  p_respuesta:= V_resp;   

  EXCEPTION
WHEN NO_DATA_FOUND THEN NULL;
    --return 98; 
WHEN EXC_ERROR THEN
    ROLLBACK;
    P_ERRORDESC := 'ERROR: ' || SUBSTR(SQLERRM,1,200);
    P_ERRORCODE := SQLCODE;
    SPDERRORES('SP_ACTUALIZA_XML_MANUALES_TER',P_ERRORCODE,P_ERRORDESC,'') ;
WHEN OTHERS THEN
    ROLLBACK;
    P_ERRORCODE := SQLCODE;
    P_ERRORDESC := 'ERROR: ' || SUBSTR(SQLERRM,1,200);
    SPDERRORES('SP_ACTUALIZA_XML_MANUALES_TER',P_ERRORCODE,P_ERRORDESC,'') ;
    --return 99;
    --raise_application_error(-20001,'ERROR - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_ACTUALIZAR_CONTADOR_PCP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_ACTUALIZAR_CONTADOR_PCP" 
(
  SP_ID_NOMINA      IN NUMBER,
  SP_TIPO_NOMINA    IN NUMBER
) 
AS

  vIDMANUALESTERCEROS         NUMBER;

  CURSOR C1
  IS
    SELECT
      MANTER.ID_MANUALES_TERCEROS
    FROM
      RH_NOMN_MANUALES_TERCEROS MANTER
      INNER JOIN RH_NOMN_CAT_CONCEPTOS CON ON CON.ID_CONCEPTO = MANTER.ID_CONCEPTO  
    WHERE
      CON.CLAVE_CONCEPTO = DECODE(SP_TIPO_NOMINA, 1, '03', 2, '03E')
      AND MANTER.ACTIVO = 'A'
    ;

BEGIN

  IF SP_TIPO_NOMINA = 1 OR
     SP_TIPO_NOMINA = 2 THEN
    
    OPEN C1;
      LOOP  
        FETCH C1 INTO vIDMANUALESTERCEROS;
      EXIT
        WHEN C1%NOTFOUND; 
        
        DBMS_OUTPUT.PUT_LINE('ACTUALIZACION DEL CONTADOR AL MANUAL Y TERCERO: ' || vIDMANUALESTERCEROS);
        
        -- ACTUALIZAR EL CONTADOR DEL PRESTAMO A CORTO PLAZO, ASI COMO INDICAR EN QUE NOM�NA SE ESTA ACTUALIZANDO.
        UPDATE 
          RH_NOMN_MANUALES_TERCEROS
        SET
          CONTADOR_PCP  = DECODE(CONTADOR_PCP, NULL, 1, CONTADOR_PCP + 1),
          NOMINA_PCP    = SP_ID_NOMINA
        WHERE
          ID_MANUALES_TERCEROS = vIDMANUALESTERCEROS
        ;
        
        COMMIT;        
        
      END LOOP;
    CLOSE C1;
    
  END IF;  

END SP_ACTUALIZAR_CONTADOR_PCP;

/
--------------------------------------------------------
--  DDL for Procedure SP_ACUMULADOS_ANUAL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_ACUMULADOS_ANUAL" (EJERCICIO IN NUMBER,TIPONOMINA IN NUMBER, DATOS OUT SYS_REFCURSOR) IS

vQUERY VARCHAR2(20000);
BEGIN



IF EJERCICIO IS NOT NULL THEN 


vQUERY := '
SELECT  NUMERO_EMPLEADO,
        NOMBRE,
        CLAVE_UNIADM,
        ID_TIPO_CONCEPTO,
        TIPO_CONCEPTO,
        CLAVE_CONCEPTO,
        NVL(Q01_ORDINARIA,0) Q01_ORD, NVL(Q01_EXTRAORDINARIA,0) Q01_EXT, NVL(Q01_RETROACTIVA,0) Q01_RET, NVL(Q01_REVERSA,0) Q01_REV, NVL(Q01_AGUINALDO,0) Q01_AGU, NVL(Q01_TOTAL,0)Q01_TOT,
        NVL(Q02_ORDINARIA,0) Q02_ORD, NVL(Q02_EXTRAORDINARIA,0) Q02_EXT, NVL(Q02_RETROACTIVA,0) Q02_RET, NVL(Q02_REVERSA,0) Q02_REV, NVL(Q02_AGUINALDO,0) Q02_AGU, NVL(Q02_TOTAL,0)Q02_TOT,
        NVL(Q03_ORDINARIA,0) Q03_ORD, NVL(Q03_EXTRAORDINARIA,0) Q03_EXT, NVL(Q03_RETROACTIVA,0) Q03_RET, NVL(Q03_REVERSA,0) Q03_REV, NVL(Q03_AGUINALDO,0) Q03_AGU, NVL(Q03_TOTAL,0)Q03_TOT,
        NVL(Q04_ORDINARIA,0) Q04_ORD, NVL(Q04_EXTRAORDINARIA,0) Q04_EXT, NVL(Q04_RETROACTIVA,0) Q04_RET, NVL(Q04_REVERSA,0) Q04_REV, NVL(Q04_AGUINALDO,0) Q04_AGU, NVL(Q04_TOTAL,0)Q04_TOT,
        NVL(Q05_ORDINARIA,0) Q05_ORD, NVL(Q05_EXTRAORDINARIA,0) Q05_EXT, NVL(Q05_RETROACTIVA,0) Q05_RET, NVL(Q05_REVERSA,0) Q05_REV, NVL(Q05_AGUINALDO,0) Q05_AGU, NVL(Q05_TOTAL,0)Q05_TOT,
        NVL(Q06_ORDINARIA,0) Q06_ORD, NVL(Q06_EXTRAORDINARIA,0) Q06_EXT, NVL(Q06_RETROACTIVA,0) Q06_RET, NVL(Q06_REVERSA,0) Q06_REV, NVL(Q06_AGUINALDO,0) Q06_AGU, NVL(Q06_TOTAL,0)Q06_TOT,
        NVL(Q07_ORDINARIA,0) Q07_ORD, NVL(Q07_EXTRAORDINARIA,0) Q07_EXT, NVL(Q07_RETROACTIVA,0) Q07_RET, NVL(Q07_REVERSA,0) Q07_REV, NVL(Q07_AGUINALDO,0) Q07_AGU, NVL(Q07_TOTAL,0)Q07_TOT,
        NVL(Q08_ORDINARIA,0) Q08_ORD, NVL(Q08_EXTRAORDINARIA,0) Q08_EXT, NVL(Q08_RETROACTIVA,0) Q08_RET, NVL(Q08_REVERSA,0) Q08_REV, NVL(Q08_AGUINALDO,0) Q08_AGU, NVL(Q08_TOTAL,0)Q08_TOT,
        NVL(Q09_ORDINARIA,0) Q09_ORD, NVL(Q09_EXTRAORDINARIA,0) Q09_EXT, NVL(Q09_RETROACTIVA,0) Q09_RET, NVL(Q09_REVERSA,0) Q09_REV, NVL(Q09_AGUINALDO,0) Q09_AGU, NVL(Q09_TOTAL,0)Q09_TOT,
        NVL(Q10_ORDINARIA,0) Q10_ORD, NVL(Q10_EXTRAORDINARIA,0) Q10_EXT, NVL(Q10_RETROACTIVA,0) Q10_RET, NVL(Q10_REVERSA,0) Q10_REV, NVL(Q10_AGUINALDO,0) Q10_AGU, NVL(Q10_TOTAL,0)Q10_TOT,
        NVL(Q11_ORDINARIA,0) Q11_ORD, NVL(Q11_EXTRAORDINARIA,0) Q11_EXT, NVL(Q11_RETROACTIVA,0) Q11_RET, NVL(Q11_REVERSA,0) Q11_REV, NVL(Q11_AGUINALDO,0) Q11_AGU, NVL(Q11_TOTAL,0)Q11_TOT,
        NVL(Q12_ORDINARIA,0) Q12_ORD, NVL(Q12_EXTRAORDINARIA,0) Q12_EXT, NVL(Q12_RETROACTIVA,0) Q12_RET, NVL(Q12_REVERSA,0) Q12_REV, NVL(Q12_AGUINALDO,0) Q12_AGU, NVL(Q12_TOTAL,0)Q12_TOT,
        NVL(Q13_ORDINARIA,0) Q13_ORD, NVL(Q13_EXTRAORDINARIA,0) Q13_EXT, NVL(Q13_RETROACTIVA,0) Q13_RET, NVL(Q13_REVERSA,0) Q13_REV, NVL(Q13_AGUINALDO,0) Q13_AGU, NVL(Q13_TOTAL,0)Q13_TOT,
        NVL(Q14_ORDINARIA,0) Q14_ORD, NVL(Q14_EXTRAORDINARIA,0) Q14_EXT, NVL(Q14_RETROACTIVA,0) Q14_RET, NVL(Q14_REVERSA,0) Q14_REV, NVL(Q14_AGUINALDO,0) Q14_AGU, NVL(Q14_TOTAL,0)Q14_TOT,
        NVL(Q15_ORDINARIA,0) Q15_ORD, NVL(Q15_EXTRAORDINARIA,0) Q15_EXT, NVL(Q15_RETROACTIVA,0) Q15_RET, NVL(Q15_REVERSA,0) Q15_REV, NVL(Q15_AGUINALDO,0) Q15_AGU, NVL(Q15_TOTAL,0)Q15_TOT,
        NVL(Q16_ORDINARIA,0) Q16_ORD, NVL(Q16_EXTRAORDINARIA,0) Q16_EXT, NVL(Q16_RETROACTIVA,0) Q16_RET, NVL(Q16_REVERSA,0) Q16_REV, NVL(Q16_AGUINALDO,0) Q16_AGU, NVL(Q16_TOTAL,0)Q16_TOT,
        NVL(Q17_ORDINARIA,0) Q17_ORD, NVL(Q17_EXTRAORDINARIA,0) Q17_EXT, NVL(Q17_RETROACTIVA,0) Q17_RET, NVL(Q17_REVERSA,0) Q17_REV, NVL(Q17_AGUINALDO,0) Q17_AGU, NVL(Q17_TOTAL,0)Q17_TOT,
        NVL(Q18_ORDINARIA,0) Q18_ORD, NVL(Q18_EXTRAORDINARIA,0) Q18_EXT, NVL(Q18_RETROACTIVA,0) Q18_RET, NVL(Q18_REVERSA,0) Q18_REV, NVL(Q18_AGUINALDO,0) Q18_AGU, NVL(Q18_TOTAL,0)Q18_TOT,
        NVL(Q19_ORDINARIA,0) Q19_ORD, NVL(Q19_EXTRAORDINARIA,0) Q19_EXT, NVL(Q19_RETROACTIVA,0) Q19_RET, NVL(Q19_REVERSA,0) Q19_REV, NVL(Q19_AGUINALDO,0) Q19_AGU, NVL(Q19_TOTAL,0)Q19_TOT,
        NVL(Q20_ORDINARIA,0) Q20_ORD, NVL(Q20_EXTRAORDINARIA,0) Q20_EXT, NVL(Q20_RETROACTIVA,0) Q20_RET, NVL(Q20_REVERSA,0) Q20_REV, NVL(Q20_AGUINALDO,0) Q20_AGU, NVL(Q20_TOTAL,0)Q20_TOT,
        NVL(Q21_ORDINARIA,0) Q21_ORD, NVL(Q21_EXTRAORDINARIA,0) Q21_EXT, NVL(Q21_RETROACTIVA,0) Q21_RET, NVL(Q21_REVERSA,0) Q21_REV, NVL(Q21_AGUINALDO,0) Q21_AGU, NVL(Q21_TOTAL,0)Q21_TOT,
        NVL(Q22_ORDINARIA,0) Q22_ORD, NVL(Q22_EXTRAORDINARIA,0) Q22_EXT, NVL(Q22_RETROACTIVA,0) Q22_RET, NVL(Q22_REVERSA,0) Q22_REV, NVL(Q22_AGUINALDO,0) Q22_AGU, NVL(Q22_TOTAL,0)Q22_TOT,
        NVL(Q23_ORDINARIA,0) Q23_ORD, NVL(Q23_EXTRAORDINARIA,0) Q23_EXT, NVL(Q23_RETROACTIVA,0) Q23_RET, NVL(Q23_REVERSA,0) Q23_REV, NVL(Q23_AGUINALDO,0) Q23_AGU, NVL(Q23_TOTAL,0)Q23_TOT,
        NVL(Q24_ORDINARIA,0) Q24_ORD, NVL(Q24_EXTRAORDINARIA,0) Q24_EXT, NVL(Q24_RETROACTIVA,0) Q24_RET, NVL(Q24_REVERSA,0) Q24_REV, NVL(Q24_AGUINALDO,0) Q24_AGU, NVL(Q24_TOTAL,0)Q24_TOT

FROM 
    (

                SELECT  DECODE(CABNOM.ID_NOMINA_PADRE,NULL,QUIN.NUMERO_QUINCENA,(SELECT NUMERO_QUINCENA 
                                                                                 FROM RH_NOMN_CAT_QUINCENAS 
                                                                                 WHERE ID_QUINCENA = (SELECT ID_QUINCENA_FK 
                                                                                                      FROM RH_NOMN_CABECERAS_NOMINAS 
                                                                                                      WHERE ID_NOMINA = CABNOM.ID_NOMINA_PADRE) 
                                                                                )
                               ) NUMERO_QUINCENA,
                        EM1.NUMERO_EMPLEADO NUMERO_EMPLEADO,
                        DAT1.NOMBRE||'' ''||DAT1.A_PATERNO||'' ''||DAT1.A_MATERNO NOMBRE 
                        ,UNIADM.CLAVE_UNIADM
                        ,TIPOCPTO.ID_TIPO_CONCEPTO
                        ,TIPOCPTO.TIPO_CONCEPTO
                        ,CPTOS.CLAVE_CONCEPTO
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 1 OR CABNOM.ID_TIPO_NOMINA_FK = 2 OR CABNOM.ID_TIPO_NOMINA_FK = 3 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0    
                        END ORDINARIA
                        ,CASE WHEN (CABNOM.ID_TIPO_NOMINA_FK = 16 OR CABNOM.ID_TIPO_NOMINA_FK = 17 OR CABNOM.ID_TIPO_NOMINA_FK = 18) AND (SELECT INSTR(UPPER(CABNOM.NOMBRE_NOMINA),''EXTRA'') FROM DUAL) > 0 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                              ELSE 
                                                0
                        END EXTRAORDINARIA
                        ,CASE WHEN (CABNOM.ID_TIPO_NOMINA_FK = 16 OR CABNOM.ID_TIPO_NOMINA_FK = 17 OR CABNOM.ID_TIPO_NOMINA_FK = 18) AND (SELECT INSTR(UPPER(CABNOM.NOMBRE_NOMINA),''RETROACTIVA'') FROM DUAL) > 0 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END RETROACTIVA
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 19 OR CABNOM.ID_TIPO_NOMINA_FK = 20 OR CABNOM.ID_TIPO_NOMINA_FK = 21 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END REVERSA
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 6 THEN --OR CABNOM.ID_TIPO_NOMINA_FK =  OR CABNOM.ID_TIPO_NOMINA_FK = 21 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END AGUINALDO

                FROM RH_NOMN_CABECERAS_NOMINAS             CABNOM
                INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA     EMPLZ      ON EMPLZ.ID_NOMINA_FK = CABNOM.ID_NOMINA
                INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA     CPT        ON CPT.ID_NOMEMPPLA_FK =EMPLZ.ID_NOMEMPPLA
                INNER JOIN RH_NOMN_CAT_CONCEPTOS           CPTOS      ON CPTOS.ID_CONCEPTO = CPT.ID_CONCEPTO_FK
                INNER JOIN RH_NOMN_CAT_TIPOS_CONCEPTO      TIPOCPTO   ON TIPOCPTO.ID_TIPO_CONCEPTO = CPTOS.ID_TIPO_CONCEPTO_FK
                INNER JOIN RH_NOMN_EMPLEADOS               EM1        ON EM1.ID_EMPLEADO = EMPLZ.ID_EMPLEADO_FK
                INNER JOIN RH_NOMN_PLAZAS                  PLZ        ON PLZ.ID_PLAZA = EM1.ID_PLAZA
                INNER JOIN RH_NOMN_DATOS_PERSONALES        DAT1       ON DAT1.ID_DATOS_PERSONALES = EM1.ID_DATOS_PERSONALES
                INNER JOIN RH_NOMN_ADSCRIPCIONES           ADSC       ON ADSC.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
                INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM     ON UNIADM.ID_UNIADM = ADSC.ID_UNIADM_FK
                INNER JOIN RH_NOMN_CAT_TIPOS_NOMINA        TIPNOM     ON TIPNOM.ID_TIPO_NOMINA = CABNOM.ID_TIPO_NOMINA_FK
                INNER JOIN RH_NOMN_CAT_QUINCENAS           QUIN       ON QUIN.ID_QUINCENA = CABNOM.ID_QUINCENA_FK
                WHERE QUIN.ID_EJERCICIO_FK = '||EJERCICIO;

                IF TIPONOMINA = 1 THEN
                 vQUERY := vQUERY||'   
                    AND  CABNOM.ID_TIPO_NOMINA_FK IN (1,16,19,6,7)';
                ELSIF TIPONOMINA = 2 THEN
                 vQUERY := vQUERY||'
                    AND  CABNOM.ID_TIPO_NOMINA_FK IN (2,17,20,10,11)';
                ELSIF TIPONOMINA = 3 THEN
                 vQUERY := vQUERY||'
                    AND  CABNOM.ID_TIPO_NOMINA_FK IN (3,18,21,14,15)';

                END IF;
                vQUERY := vQUERY||'
    )PIVOT (
        SUM(ORDINARIA) ORDINARIA,
        SUM(EXTRAORDINARIA)EXTRAORDINARIA,
        SUM(RETROACTIVA) RETROACTIVA,
        SUM(REVERSA) REVERSA,
        SUM(AGUINALDO) AGUINALDO,
        SUM(ORDINARIA+EXTRAORDINARIA+RETROACTIVA+REVERSA+AGUINALDO) TOTAL
        FOR NUMERO_QUINCENA IN (1 Q01,2 Q02,3 Q03,4 Q04,5 Q05,6 Q06,7 Q07,8 Q08,9 Q09,10 Q10,11 Q11,12 Q12,13 Q13,14 Q14,15 Q15,16 Q16,17 Q17,18 Q18,19 Q19,20 Q20,21 Q21,22 Q22,23 Q23,24 Q24)
    )
    ORDER BY NUMERO_EMPLEADO,ID_TIPO_CONCEPTO'
;

OPEN DATOS FOR vQUERY;
ELSE

OPEN DATOS FOR        
 SELECT        
        0     NUMERO_EMPLEADO,
        'N/A' NOMBRE,
        'N/A' CLAVE_UNIADM,
        0     ID_TIPO_CONCEPTO,
        'N/A' TIPO_CONCEPTO,
        'N/A' CLAVE_CONCEPTO,
        0 Q01_ORD, 0 Q01_EXT, 0 Q01_RET, 0 Q01_REV, 0 Q01_AGU, 0 Q01_TOT,
        0 Q02_ORD, 0 Q02_EXT, 0 Q02_RET, 0 Q02_REV, 0 Q02_AGU, 0 Q02_TOT,
        0 Q03_ORD, 0 Q03_EXT, 0 Q03_RET, 0 Q03_REV, 0 Q03_AGU, 0 Q03_TOT,
        0 Q04_ORD, 0 Q04_EXT, 0 Q04_RET, 0 Q04_REV, 0 Q04_AGU, 0 Q04_TOT,
        0 Q05_ORD, 0 Q05_EXT, 0 Q05_RET, 0 Q05_REV, 0 Q05_AGU, 0 Q05_TOT,
        0 Q06_ORD, 0 Q06_EXT, 0 Q06_RET, 0 Q06_REV, 0 Q06_AGU, 0 Q06_TOT,
        0 Q07_ORD, 0 Q07_EXT, 0 Q07_RET, 0 Q07_REV, 0 Q07_AGU, 0 Q07_TOT,
        0 Q08_ORD, 0 Q08_EXT, 0 Q08_RET, 0 Q08_REV, 0 Q08_AGU, 0 Q08_TOT,
        0 Q09_ORD, 0 Q09_EXT, 0 Q09_RET, 0 Q09_REV, 0 Q09_AGU, 0 Q09_TOT,
        0 Q10_ORD, 0 Q10_EXT, 0 Q10_RET, 0 Q10_REV, 0 Q10_AGU, 0 Q10_TOT,
        0 Q11_ORD, 0 Q11_EXT, 0 Q11_RET, 0 Q11_REV, 0 Q11_AGU, 0 Q11_TOT,
        0 Q12_ORD, 0 Q12_EXT, 0 Q12_RET, 0 Q12_REV, 0 Q12_AGU, 0 Q12_TOT,
        0 Q13_ORD, 0 Q13_EXT, 0 Q13_RET, 0 Q13_REV, 0 Q13_AGU, 0 Q13_TOT,
        0 Q14_ORD, 0 Q14_EXT, 0 Q14_RET, 0 Q14_REV, 0 Q14_AGU, 0 Q14_TOT,
        0 Q15_ORD, 0 Q15_EXT, 0 Q15_RET, 0 Q15_REV, 0 Q15_AGU, 0 Q15_TOT,
        0 Q16_ORD, 0 Q16_EXT, 0 Q16_RET, 0 Q16_REV, 0 Q16_AGU, 0 Q16_TOT,
        0 Q17_ORD, 0 Q17_EXT, 0 Q17_RET, 0 Q17_REV, 0 Q17_AGU, 0 Q17_TOT,
        0 Q18_ORD, 0 Q18_EXT, 0 Q18_RET, 0 Q18_REV, 0 Q18_AGU, 0 Q18_TOT,
        0 Q19_ORD, 0 Q19_EXT, 0 Q19_RET, 0 Q19_REV, 0 Q19_AGU, 0 Q19_TOT,
        0 Q20_ORD, 0 Q20_EXT, 0 Q20_RET, 0 Q20_REV, 0 Q20_AGU, 0 Q20_TOT,
        0 Q21_ORD, 0 Q21_EXT, 0 Q21_RET, 0 Q21_REV, 0 Q21_AGU, 0 Q21_TOT,
        0 Q22_ORD, 0 Q22_EXT, 0 Q22_RET, 0 Q22_REV, 0 Q22_AGU, 0 Q22_TOT,
        0 Q23_ORD, 0 Q23_EXT, 0 Q23_RET, 0 Q23_REV, 0 Q23_AGU, 0 Q23_TOT,
        0 Q24_ORD, 0 Q24_EXT, 0 Q24_RET, 0 Q24_REV, 0 Q24_AGU, 0 Q24_TOT
        FROM DUAL;
END IF;



END SP_ACUMULADOS_ANUAL;

/
--------------------------------------------------------
--  DDL for Procedure SP_APORTACION_CAMBIO_ESTATUS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_APORTACION_CAMBIO_ESTATUS" AS 

v varchar2(100);
fechaFin date;
fechaIni date;
fecha_retro date;
camCiclo NUMBER;
ERR_CODE    INTEGER;
ERR_MSG     VARCHAR2(250);

BEGIN

------------------------------------------------
/*Validamos que haya cambio de ciclo */
------------------------------------------------
  SELECT COUNT(1) INTO  camCiclo
  FROM  RH_NOMN_FONAC_CICLOS
  WHERE trunc(to_date(VIGENCIA_FIN,'dd/MM/yy')) <= trunc(TO_DATE(SYSDATE,'dd/MM/yy')) AND ID_ESTATUS_FONAC_FK = 2;
  
  DBMS_OUTPUT.PUT_LINE('  camCiclo-->, ' || camCiclo);
  
  IF camCiclo = 1 THEN
       
      UPDATE  RH_NOMN_FONAC_CICLOS  SET ID_ESTATUS_FONAC_FK = 1
      WHERE   ID_ESTATUS_FONAC_FK = 2 
      AND to_date(VIGENCIA_FIN,'dd/MM/yy') <= TO_DATE(SYSDATE,'dd/MM/yy');
      
      COMMIT;
  END IF;
  
  UPDATE  RH_NOMN_FONAC_CICLOS  SET ID_ESTATUS_FONAC_FK = 2
      WHERE   ID_ESTATUS_FONAC_FK = 3
      AND to_date(VIGENCIA_INICIO,'dd/MM/yy') <= TO_DATE(SYSDATE,'dd/MM/yy');
      commit;
      
------------------------------------------------
--  A P O R T A C I O N E S   E S T A T U S
------------------------------------------------

    -- ACTIVO A INACTIVO
    
       select to_date(VIGENCIA_FIN, 'dd/MM/yy') 
       into fechaFin 
       from RH_NOMN_FONAC_APORTACIONES 
       where ID_ESTATUS_FONAC_FK = 2;
      
      DBMS_OUTPUT.PUT_LINE('fechaFin stat 2-->, ' || fechaFin);
    
      update RH_NOMN_FONAC_APORTACIONES AP 
      set AP.ID_ESTATUS_FONAC_FK = 1
      where AP.ID_ESTATUS_FONAC_FK = 2
      AND trunc(fechaFin) <= trunc(SYSDATE);
      COMMIT;

             
  -- BUSCAMOS CAMBIO DE ESTATUS DE PEND A ACTIVO
      SELECT A.VIGENCIA_INICIO 
      INTO fechaIni
      FROM RH_NOMN_FONAC_APORTACIONES A
      WHERE A.ID_ESTATUS_FONAC_FK = 3
      AND to_date(VIGENCIA_INICIO,'dd/MM/yy') <= TO_DATE(SYSDATE,'dd/MM/yy');        
 
  /*cambiamos de estatus las aportaciones */
  IF TRUNC(fechaIni) <= trunc(SYSDATE) THEN 
  
       -- PENDIENTE A ACTIVO
        update RH_NOMN_FONAC_APORTACIONES AP set AP.ID_ESTATUS_FONAC_FK = 2
        where AP.ID_ESTATUS_FONAC_FK = 3
        AND to_date(VIGENCIA_INICIO,'dd/MM/yy')  <= TO_DATE(SYSDATE,'dd/MM/yy');
        COMMIT;
        
      /*Actualizamos la tabla de seguros con el nuevo monto a descontar */
        DBMS_OUTPUT.PUT_LINE('Actualizamos la tabla de seguros con el nuevo monto, ');
        update RH_NOMN_SEGUROS  SET IMPORTE = NVL((SELECT AP.APORTACION_EMPLEADO
                                                             FROM RH_NOMN_FONAC_APORTACIONES AP
                                                             WHERE AP.ID_ESTATUS_FONAC_FK = 2),0)
        where ID_SEGURO IN (select S.ID_SEGURO 
                            from RH_NOMN_SEGUROS S
                            left join RH_NOMN_CAT_PRESTACIONES P ON P.ID_PRESTACION = S.ID_PRESTACION_FK
                            where S.ACTIVO = 'A'
                            and P.DESC_PRESTACION = 'FONAC' );
      
        DBMS_OUTPUT.PUT_LINE('Finaliza cambio de aportaci�n en seguros, ');
  
  
      /*Aplicamos retroactividad si es que existe */
       DBMS_OUTPUT.PUT_LINE('Validamos si hay retroactividad ');
       SELECT RE.FECHA_INICIO INTO fecha_retro
       FROM RH_NOMN_FONAC_APORTACIONES AP,
       RH_NOMN_FONAC_APORTA_RETRO RE 
       WHERE RE.ID_FONAC_APORTACION = AP.ID_FONAC_APORTACION
       AND AP.ID_ESTATUS_FONAC_FK = 2;
       
       DBMS_OUTPUT.PUT_LINE('Fecha: '||fecha_retro);
       
      IF fecha_retro IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE('Aplicamos la retroactividad, '||fecha_retro);
        v:= FN_APP_FONAC_RETROACTIVIDAD(fecha_retro);
        DBMS_OUTPUT.PUT_LINE('Finaliza Retroactividad, '||v);
      END IF;
      IF v = 'EXITO' THEN
        COMMIT;
      ELSE
        ROLLBACK;
      END IF;
  ELSE
    DBMS_OUTPUT.PUT_LINE('No se cumple la fecha para realizar los cambios, ');
  END IF;
  
EXCEPTION
WHEN NO_DATA_FOUND THEN  
  ROLLBACK;

WHEN OTHERS THEN  
ROLLBACK;
 err_code := SQLCODE;
 err_msg := SUBSTR(SQLERRM, 1, 200);
 DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);

END SP_APORTACION_CAMBIO_ESTATUS;

/
--------------------------------------------------------
--  DDL for Procedure SP_BAJA_INTERINATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_BAJA_INTERINATO" (IDEMPLEADO IN NUMBER , 
                                                TIPOEMPLEADO IN NUMBER , 
                                                MENSAJESALIDA OUT VARCHAR2) 
IS
PRAGMA AUTONOMOUS_TRANSACTION;

vID_INTERINATO              NUMBER; 
vID_PLAZA_FK_INICIAL        NUMBER; 
vID_PLAZA_FK_INTERINATO     NUMBER; 
vID_EMPLEADO_FK_INICIAL     NUMBER;
vID_EMPLEADO_FK_INTERINATO  NUMBER;
vNUMERO_EMPLEADO            NUMBER;
vNUMERO_PLAZA               NUMBER;
vNUMERO_PLAZA_LICENCIA      NUMBER;        
vPROPIETARIO_PLAZA          NUMBER;
vFECHA_INI_INTERINATO       DATE;
vFECHA_FIN_INTERINATO       DATE; 
vFECHA_REGISTRO             DATE;
vMENSAJE                    VARCHAR2(250) := 'Fall� la ejecuci�n del SP: SP_BAJA_INTERINATO';
vESTATUS                    VARCHAR2(1);
VMAXHMOVS                   NUMBER;
VMAXHCAMP                   NUMBER;
VMAXMOVE                    NUMBER;
VMAXMOVP                    NUMBER;
VIDANIO                     NUMBER;
VIDQUI                      NUMBER;
FECHA_INI_QUIN              DATE;
FECHA_FIN_QUIN              DATE;
PFECHA                      VARCHAR2(250);
vcal                        FLOAT;
DIF_DIAS                    NUMBER;

ERR_CODE                    NUMBER;
ERR_MSG                     VARCHAR2(250);

-- SEGUROS DEL EMPLEADO CON INTERINATO.
CURSOR C_SEGUROS IS  
  SELECT 
    SEGURO.ID_SEGURO,
    SEGURO.ID_EMPLEADO_FK,
    SEGURO.ID_CONCEPTO_FK,
    SEGURO.IMPORTE,
    SEGURO.IMPORTE_PRIMER_PAGO,
    SEGURO.IMPORTE_PAGO
  FROM 
    RH_NOMN_SEGUROS SEGURO
    INNER JOIN RH_NOMN_INTERINATOS INTERINATO ON INTERINATO.ID_EMPLEADO_FK_INTERINATO = SEGURO.ID_EMPLEADO_FK
  WHERE     
    INTERINATO.ESTATUS = 'A' 
    AND SEGURO.ACTIVO = 'A'
    AND SEGURO.ID_EMPLEADO_FK = IDEMPLEADO
  ;

BEGIN

  MENSAJESALIDA := vMENSAJE;
  
  -- Obtiene la informaci�n de la quincena y ejercicio actual.
  SELECT 
    ID_EJERCICIO_FK ,
    ID_QUINCENA,
    FECHA_INICIO_QUINCENA,
    FECHA_FIN_QUINCENA  
  INTO 
    VIDANIO, 
    VIDQUI,
    FECHA_INI_QUIN,
    FECHA_FIN_QUIN
  FROM 
    RH_NOMN_CAT_QUINCENAS
  WHERE 
    ACTIVO=1 
    AND TO_DATE(SYSDATE, 'DD/MM/YYYY') BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YYYY') 
    AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YYYY')
  ;
  
  DBMS_OUTPUT.put_line('EJERCICIO: ' || VIDANIO || ' FECHA QUINCENA INICIO: ' || FECHA_INI_QUIN || ' FECHA QUINCENA FIN: ' || FECHA_FIN_QUIN);
    
  -- Diferencia de d�as con respecto a la fecha inicial de la quincena.  
  SELECT 
    (TRUNC(TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'))) - TRUNC(TO_DATE(TO_CHAR(FECHA_INI_QUIN,'DD/MM/YYYY'),'DD/MM/YYYY'))+1 
  INTO 
    DIF_DIAS
  FROM 
    DUAL
  ;
    
    DBMS_OUTPUT.put_line('DIFERENCIA DE DIAS TODAY - FECHA FINAL DE QUINCENA: ' || DIF_DIAS);  

  -- ACTUALIZAR LOS SEGUROS QUE TENGA EL EMPLEADO CON INTERINATO.
  FOR LR2 IN C_SEGUROS LOOP 
      
    VCAL := FN_PROPO_IMP_SEG_POR_DIAS(DIF_DIAS, LR2.IMPORTE, 1);
    
    UPDATE 
      RH_NOMN_SEGUROS 
    SET 
      IMPORTE_PAGO = VCAL 
    WHERE  
      ID_SEGURO = LR2.ID_SEGURO
    ;

    DBMS_OUTPUT.put_line('PROCESO DE PROPORCIONALIDAD DE SEGUROS : '|| LR2.ID_SEGURO );
  END LOOP;
    
  COMMIT;

  -- VALIDAR SI ES UN EMPLEADO DE NUEVO INGRESO O CON PLAZA CONGELADA.
  IF TIPOEMPLEADO = 0 THEN
  
    DBMS_OUTPUT.put_line('EMPLEADO CON PLAZA CONGELADA');
    
    -- OBTENER LOS DATOS DEL INTERINATO DEL EMPLEADO.
    SELECT 
      INTERINATO.ID_INTERINATO, 
      INTERINATO.ID_PLAZA_FK_INICIAL, 
      INTERINATO.ID_PLAZA_FK_INTERINATO, 
      INTERINATO.ID_EMPLEADO_FK_INICIAL, 
      INTERINATO.ID_EMPLEADO_FK_INTERINATO, 
      (
        SELECT 
          NUMERO_EMPLEADO 
        FROM 
          RH_NOMN_EMPLEADOS 
        WHERE 
          ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
      ) NUMERO_EMPLEADO, 
      (
        SELECT 
          NUMERO_PLAZA 
        FROM 
          RH_NOMN_EMPLEADOS EMP
          INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_EMPLEADO
        WHERE 
          ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INICIAL
      ) NUMERO_PLAZA,       
      (
        SELECT 
          NUMERO_PLAZA 
        FROM 
          RH_NOMN_EMPLEADOS EMP
          INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_EMPLEADO
        WHERE 
          ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
      ) NUMERO_PLAZA_LICENCIA,        
      (
        SELECT 
          PROPIETARIO 
        FROM 
          RH_NOMN_PLAZAS      
        WHERE 
          ID_PLAZA = INTERINATO.ID_PLAZA_FK_INTERINATO
      ) PROPIETARIO_PLAZA,  
      FECHA_INI_INTERINATO, 
      FECHA_FIN_INTERINATO, 
      FECHA_REGISTRO, 
      ESTATUS
    INTO
      vID_INTERINATO, 
      vID_PLAZA_FK_INICIAL,
      vID_PLAZA_FK_INTERINATO,
      vID_EMPLEADO_FK_INICIAL,
      vID_EMPLEADO_FK_INTERINATO,
      vNUMERO_EMPLEADO,
      vNUMERO_PLAZA,
      vNUMERO_PLAZA_LICENCIA,
      vPROPIETARIO_PLAZA,
      vFECHA_INI_INTERINATO,
      vFECHA_FIN_INTERINATO,
      vFECHA_REGISTRO,
      vESTATUS
    FROM 
      RH_NOMN_INTERINATOS INTERINATO 
    WHERE 
      ESTATUS='A'  
      AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
    ; 
    
    -- Actualiza la informacion del empleado COMO UNA BAJA.
    UPDATE
      RH_NOMN_EMPLEADOS
    SET
      ID_ESTATUS_EMPLEADO = 2,
      ID_CAUSA_BAJA       = 4,
      FECHA_BAJA          = vFECHA_FIN_INTERINATO, 
      FECHA_MODIFICACION  = vFECHA_FIN_INTERINATO
    WHERE
      ID_EMPLEADO         = vID_EMPLEADO_FK_INTERINATO
    ;
    
    -- Actualiza el estatus de la plaza de interinato a TEMPORAL DE NUEVA CUENTA.
    UPDATE 
      RH_NOMN_PLAZAS  
    SET 
      ID_ESTATUS_PLAZA_FK     = 5,
      FECHA_DE_MODIFICACION   = SYSDATE
    WHERE 
      ID_PLAZA                = vID_PLAZA_FK_INTERINATO
    ;  
    
    -- Actualiza el estatus de la plaza congelada a VACANTE y LIMPIAR PROPIETARIO.
    UPDATE 
      RH_NOMN_PLAZAS  
    SET 
      ID_ESTATUS_PLAZA_FK     = 1,
      FECHA_DE_MODIFICACION   = SYSDATE,
      PROPIETARIO             = NULL
    WHERE 
      ID_PLAZA                = vID_PLAZA_FK_INICIAL
    ;  
    
    -- Actualizar el cierre de las PENSIONES PARA EL EMPLEADO en caso de tener.
    UPDATE  
      RH_NOMN_PENSIONES 
    SET 
      ID_ANIO_FIN         = VIDANIO ,
      ID_QUINCENA_FIN     = VIDQUI
    WHERE 
      ID_EMPLEADO         = vID_EMPLEADO_FK_INTERINATO
      AND ESTATUS         = 'A'
    ;      
    
    -- Actualizar el cierre MANUALES Y TERCEROS PARA EL EMPLEADO (INCLUYE APOYO VEHICULAR) en caso de tener.
    UPDATE  
      RH_NOMN_MANUALES_TERCEROS  
    SET 
      FECHA_FIN             = SYSDATE,                                          
      FECHA_MODIFICACION    = SYSDATE
    WHERE 
      ID_EMPLEADO           = vID_EMPLEADO_FK_INTERINATO
      AND ACTIVO            = 'A'
    ;    
    
    -- Actualizar el INTERINATO, SE DEBE INACTIVAR.
    UPDATE
      RH_NOMN_INTERINATOS
    SET
      ESTATUS = 'I'
    WHERE
      ID_INTERINATO = vID_INTERINATO
    ; 
    
    /********************************************************/
    /*HISTORICO MOVIMIENTOS*/
    
    -- EMPLEADO: Guarda registro MOVIMIENTO TERMINO DE INTERINATO.
    SELECT 
      (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
    INTO 
      VMAXHMOVS 
    FROM 
      RH_NOMN_BIT_HISTORICOS_MOVS
    ;        
            
    INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
    (
      ID_HISTORICO_MOVIMIENTO, 
      ID_ACCION_FK, 
      ID_EJERCICIO_FK,
      FECHA, 
      ID_USUARIO_FK, 
      TABLA
    )
    VALUES 
    ( 
      VMAXHMOVS, 
      16, 
      VIDANIO, 
      SYSDATE, 
      1, 
      ''
    )
    ;     
    
    /*HISTORICO CAMPOS EMPLEADO*/   
    SELECT 
      TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
    INTO 
      PFECHA 
    FROM 
      DUAL
    ;
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO )+1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;        
    
    -- MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
    SELECT  
      NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
    INTO 
      VMAXMOVE 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    WHERE  
      ID_REGISTRO_ENTIDAD = vID_EMPLEADO_FK_INTERINATO
    ;
        
    -- EMPLEADO CAMBIA A ESTATUS DE BAJA.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      10,
      2,
      vNUMERO_EMPLEADO
    )
    ;    
        
    -- REGISTRAR LA FECHA DE BAJA.
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;        
        
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      25,
      PFECHA,
      vNUMERO_EMPLEADO
    )
    ;                
    
    -- MOVIENDO A LA PLAZA CONGELADA QUEDA CON ESTATUS VACANTE.
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;    
                        
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INICIAL,
      VMAXMOVE,
      5,
      1,
      vNUMERO_PLAZA
    )
    ; 
    
    -- MOVIENDO A LA PLAZA OCUPADA QUEDA CON ESTATUS TEMPORAl.
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
                        
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      5,
      5,
      vNUMERO_PLAZA_LICENCIA
    )
    ;    
        
    -- EMPLEADO: Guarda registro MOVIMIENTO BAJA DE EMPLEADO.    
    SELECT 
      (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
    INTO 
      VMAXHMOVS 
    FROM 
      RH_NOMN_BIT_HISTORICOS_MOVS
    ;
            
    INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
    (
      ID_HISTORICO_MOVIMIENTO, 
      ID_ACCION_FK, 
      ID_EJERCICIO_FK,
      FECHA, 
      ID_USUARIO_FK, 
      TABLA
    )
    VALUES 
    ( 
      VMAXHMOVS, 
      11, 
      VIDANIO, 
      SYSDATE, 
      1, 
      ''
    )
    ;    
    
    /*HISTORICO CAMPOS EMPLEADO*/   
    SELECT 
      TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
    INTO 
      PFECHA 
    FROM 
      DUAL
    ;
        
    SELECT 
      (MAX(ID_HISTORICO_CAMPO )+1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
    
    -- MOVIMIENTO ANTERIOR.
    SELECT  
      NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
    INTO 
      VMAXMOVE 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    WHERE  
      ID_REGISTRO_ENTIDAD = vID_EMPLEADO_FK_INTERINATO
    ;
        
    -- BAJA DE EMPLEADO: CAUSA DE LA BAJA POR TERMINO DE INTERINATO.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      24,
      4,
      vNUMERO_EMPLEADO
    )
    ;    
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
        
    -- BAJA DE EMPLEADO: FECHA DE LA BAJA.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      25,
      PFECHA,
      vNUMERO_EMPLEADO
    )
    ;
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: ID DE LA PLAZA DEL EMPLEADO QUE SE REGRESA A TEMPORAL.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      9,
      vID_PLAZA_FK_INTERINATO,
      vNUMERO_EMPLEADO
    )
    ; 
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: ID DEL ESTATUS CON QUE SE QUEDA EL EMPLEADO QUE SE DA DE BAJA.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      10,
      2,
      vNUMERO_EMPLEADO
    )
    ;    
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;      
         
    -- BAJA DE EMPLEADO: ID DEL ESTATUS DE LA PLAZA DE INTERINATO DEL EMPLEADO QUE SE DA DE BAJA, 
    --  EN ESTE CASO ES TEMPORAL.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      5,
      5,
      vNUMERO_PLAZA_LICENCIA
    )
    ; 
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: PROPIETARIO DE LA PLAZA TEMPORAL.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      6,
      vPROPIETARIO_PLAZA,
      vNUMERO_PLAZA_LICENCIA
    )
    ;
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
    
    -- BAJA DE EMPLEADO: ID DEL ESTATUS DE LA PLAZA CONGELADA DEL EMPLEADO QUE SE DA DE BAJA, 
    --  EN ESTE CASO PASA A SER VACANTE.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INICIAL,
      VMAXMOVE,
      5,
      1,
      vNUMERO_PLAZA
    )
    ; 
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: PROPIETARIO DE LA PLAZA TEMPORAL.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INICIAL,
      VMAXMOVE,
      6,
      ' ',
      vNUMERO_PLAZA
    )
    ;    
    
    DBMS_OUTPUT.PUT_LINE('PROCESO DE BAJA DE EMPLEADO EN TERMINO INTERINATO: '|| vID_EMPLEADO_FK_INTERINATO
    || ' PLAZA INTERINATO: ' || vID_PLAZA_FK_INTERINATO || ' PLAZA CONGELADA: ' || vID_PLAZA_FK_INICIAL);    
    
    COMMIT;
    
    vMENSAJE := 'EXITO';    
    
  ELSE
    
    DBMS_OUTPUT.put_line('EMPLEADO DE NUEVO INGRESO');        
        
    -- OBTENER LOS DATOS DEL INTERINATO DEL EMPLEADO.
    SELECT 
      INTERINATO.ID_INTERINATO, 
      INTERINATO.ID_PLAZA_FK_INICIAL, 
      INTERINATO.ID_PLAZA_FK_INTERINATO, 
      INTERINATO.ID_EMPLEADO_FK_INICIAL, 
      INTERINATO.ID_EMPLEADO_FK_INTERINATO, 
      (
        SELECT 
          NUMERO_EMPLEADO 
        FROM 
          RH_NOMN_EMPLEADOS 
        WHERE 
          ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
      ) NUMERO_EMPLEADO, 
      (
        SELECT 
          NUMERO_PLAZA 
        FROM 
          RH_NOMN_EMPLEADOS EMP
          INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_EMPLEADO
        WHERE 
          ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
      ) NUMERO_PLAZA_LICENCIA,        
      (
        SELECT 
          PROPIETARIO 
        FROM 
          RH_NOMN_PLAZAS      
        WHERE 
          ID_PLAZA = INTERINATO.ID_PLAZA_FK_INTERINATO
      ) PROPIETARIO_PLAZA,  
      FECHA_INI_INTERINATO, 
      FECHA_FIN_INTERINATO, 
      FECHA_REGISTRO, 
      ESTATUS
    INTO
      vID_INTERINATO, 
      vID_PLAZA_FK_INICIAL,
      vID_PLAZA_FK_INTERINATO,
      vID_EMPLEADO_FK_INICIAL,
      vID_EMPLEADO_FK_INTERINATO,
      vNUMERO_EMPLEADO,
      vNUMERO_PLAZA_LICENCIA,
      vPROPIETARIO_PLAZA,
      vFECHA_INI_INTERINATO,
      vFECHA_FIN_INTERINATO,
      vFECHA_REGISTRO,
      vESTATUS
    FROM 
      RH_NOMN_INTERINATOS INTERINATO 
    WHERE 
      ESTATUS='A'  
      AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
    ;
    
    -- Actualiza la informacion del empleado COMO UNA BAJA.
    UPDATE
      RH_NOMN_EMPLEADOS
    SET
      ID_ESTATUS_EMPLEADO = 2,
      ID_CAUSA_BAJA       = 4,
      FECHA_BAJA          = vFECHA_FIN_INTERINATO, 
      FECHA_MODIFICACION  = vFECHA_FIN_INTERINATO
    WHERE
      ID_EMPLEADO         = vID_EMPLEADO_FK_INTERINATO
    ;
    
    -- Actualiza el estatus de la plaza de interinato a TEMPORAL DE NUEVA CUENTA.
    UPDATE 
      RH_NOMN_PLAZAS  
    SET 
      ID_ESTATUS_PLAZA_FK     = 5,
      FECHA_DE_MODIFICACION   = SYSDATE
    WHERE 
      ID_PLAZA                = vID_PLAZA_FK_INTERINATO
    ;
    
    -- Actualizar el cierre de las PENSIONES PARA EL EMPLEADO en caso de tener.
    UPDATE  
      RH_NOMN_PENSIONES 
    SET 
      ID_ANIO_FIN         = VIDANIO ,
      ID_QUINCENA_FIN     = VIDQUI
    WHERE 
      ID_EMPLEADO         = vID_EMPLEADO_FK_INTERINATO
      AND ESTATUS         = 'A'
    ;      
    
    -- Actualizar el cierre MANUALES Y TERCEROS PARA EL EMPLEADO (INCLUYE APOYO VEHICULAR) en caso de tener.
    UPDATE  
      RH_NOMN_MANUALES_TERCEROS  
    SET 
      FECHA_FIN             = SYSDATE,                                          
      FECHA_MODIFICACION    = SYSDATE
    WHERE 
      ID_EMPLEADO           = vID_EMPLEADO_FK_INTERINATO
      AND ACTIVO            = 'A'
    ;    
    
    -- Actualizar el INTERINATO, SE DEBE INACTIVAR.
    UPDATE
      RH_NOMN_INTERINATOS
    SET
      ESTATUS = 'I'
    WHERE
      ID_INTERINATO = vID_INTERINATO
    ;    
        
    /********************************************************/
    /*HISTORICO MOVIMIENTOS*/
    
    -- EMPLEADO: Guarda registro MOVIMIENTO TERMINO DE INTERINATO.
    SELECT 
      (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
    INTO 
      VMAXHMOVS 
    FROM 
      RH_NOMN_BIT_HISTORICOS_MOVS
    ;
            
    INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
    (
      ID_HISTORICO_MOVIMIENTO, 
      ID_ACCION_FK, 
      ID_EJERCICIO_FK,
      FECHA, 
      ID_USUARIO_FK, 
      TABLA
    )
    VALUES 
    ( 
      VMAXHMOVS, 
      16, 
      VIDANIO, 
      SYSDATE, 
      1, 
      ''
    )
    ;     
    
    /*HISTORICO CAMPOS EMPLEADO*/   
    SELECT 
      TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
    INTO 
      PFECHA 
    FROM 
      DUAL
    ;
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO )+1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;    
    
    -- MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
    SELECT  
      NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
    INTO 
      VMAXMOVE 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    WHERE  
      ID_REGISTRO_ENTIDAD = vID_EMPLEADO_FK_INTERINATO
    ;
        
    -- EMPLEADO CAMBIA A ESTATUS DE BAJA.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      10,
      2,
      vNUMERO_EMPLEADO
    )
    ;    
        
    -- REGISTRAR LA FECHA DE BAJA.
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
        
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      25,
      PFECHA,
      vNUMERO_EMPLEADO
    )
    ;    
    
    -- MOVIENTO A LA PLAZA OCUPADA QUEDA CON ESTATUS TEMPORAL.
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
                        
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      5,
      5,
      vNUMERO_PLAZA_LICENCIA
    )
    ;   
        
    -- EMPLEADO: Guarda registro MOVIMIENTO BAJA DE EMPLEADO.    
    SELECT 
      (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
    INTO 
      VMAXHMOVS 
    FROM 
      RH_NOMN_BIT_HISTORICOS_MOVS
    ;
            
    INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
    (
      ID_HISTORICO_MOVIMIENTO, 
      ID_ACCION_FK, 
      ID_EJERCICIO_FK,
      FECHA, 
      ID_USUARIO_FK, 
      TABLA
    )
    VALUES 
    ( 
      VMAXHMOVS, 
      11, 
      VIDANIO, 
      SYSDATE, 
      1, 
      ''
    )
    ;    
    
    /*HISTORICO CAMPOS EMPLEADO*/   
    SELECT 
      TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
    INTO 
      PFECHA 
    FROM 
      DUAL
    ;
        
    SELECT 
      (MAX(ID_HISTORICO_CAMPO )+1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
    
    -- MOVIMIENTO ANTERIOR.
    SELECT  
      NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
    INTO 
      VMAXMOVE 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    WHERE  
      ID_REGISTRO_ENTIDAD = vID_EMPLEADO_FK_INTERINATO
    ;
        
    -- BAJA DE EMPLEADO: CAUSA DE LA BAJA POR TERMINO DE INTERINATO.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      24,
      4,
      vNUMERO_EMPLEADO
    )
    ;    
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
        
    -- BAJA DE EMPLEADO: FECHA DE LA BAJA.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    ( 
      VMAXHCAMP ,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      25,
      PFECHA,
      vNUMERO_EMPLEADO
    )
    ;
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: ID DE LA PLAZA DEL EMPLEADO QUE SE REGRESA A TEMPORAL.
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      9,
      vID_PLAZA_FK_INTERINATO,
      vNUMERO_EMPLEADO
    )
    ; 
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: ID DEL ESTATUS CON QUE SE QUEDA EL EMPLEADO QUE SE DA DE BAJA.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_EMPLEADO_FK_INTERINATO,
      VMAXMOVE,
      10,
      2,
      vNUMERO_EMPLEADO
    )
    ;    
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: ID DEL ESTATUS DE LA PLAZA DEL EMPLEADO QUE SE DA DE BAJA, EN ESTE CASO ES TEMPORAL YA QUE VIENE DE UN INTERINATO POR LICENCIA.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      5,
      5,
      vNUMERO_PLAZA_LICENCIA
    )
    ; 
    
    SELECT 
      (MAX(ID_HISTORICO_CAMPO ) + 1) 
    INTO 
      VMAXHCAMP 
    FROM 
      RH_NOMN_BIT_HISTORICOS_CAMPOS
    ;
         
    -- BAJA DE EMPLEADO: PROPIETARIO DE LA PLAZA TEMPORAL.               
    INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
    (
      ID_HISTORICO_CAMPO, 
      ID_HISTORICO_MOVIMIENTO_FK, 
      ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, 
      ID_CAMPO_AFECTADO_FK, 
      VALOR_ACTUAL, 
      VALOR_BUSQUEDA
    ) 
    VALUES 
    (
      VMAXHCAMP,
      VMAXHMOVS,
      vID_PLAZA_FK_INTERINATO,
      VMAXMOVE,
      6,
      vPROPIETARIO_PLAZA,
      vNUMERO_PLAZA_LICENCIA
    )
    ;    
    
    DBMS_OUTPUT.PUT_LINE('PROCESO DE BAJA DE EMPLEADO EN TERMINO INTERINATO: '|| vID_EMPLEADO_FK_INTERINATO
    || ' PLAZA INTERINATO: ' || vID_PLAZA_FK_INTERINATO);    
    
    COMMIT;
    
    vMENSAJE := 'EXITO';
  END IF;
  
  MENSAJESALIDA := vMENSAJE;
  
  DBMS_OUTPUT.put_line ('MENSAJESALIDA: ' || MENSAJESALIDA);
  
  EXCEPTION  
    WHEN NO_DATA_FOUND THEN  
      NULL;
    WHEN OTHERS THEN   
      ROLLBACK;
      ERR_CODE  := SQLCODE;
      ERR_MSG   := SUBSTR(SQLERRM, 1, 200);      
      
      DBMS_OUTPUT.put_line ('MENSAJESALIDA: ' || MENSAJESALIDA);
      DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
      DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);        

END SP_BAJA_INTERINATO;

/
--------------------------------------------------------
--  DDL for Procedure SP_CALCULA_ACUMULADOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CALCULA_ACUMULADOS" (IDQUINCENA IN NUMBER,NUMQUINCENA IN NUMBER, TIPONOMINA IN NUMBER,DATOS OUT SYS_REFCURSOR)IS
vQUERY VARCHAR2(10000);
TIPOCONSULTA NUMBER;

BEGIN
IF IDQUINCENA IS NOT NULL AND  NUMQUINCENA IS NOT NULL THEN 


    SELECT DISTINCT CASE WHEN PLZ.ID_NOMINA_FK IS NOT NULL THEN 
                                  1   ---Para ir a la tabla de nominanas cerradas
                               WHEN PLZ1.ID_NOMINA01_FK  IS NOT NULL THEN
                                  2   ---Para ir a las tabla de nominas pendientes
                          END
                          INTO TIPOCONSULTA
          FROM RH_NOMN_CABECERAS_NOMINAS NOMN
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA PLZ ON PLZ.ID_NOMINA_FK = NOMN.ID_NOMINA
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 PLZ1 ON PLZ1.ID_NOMINA01_FK = NOMN.ID_NOMINA
          WHERE NOMN.ID_QUINCENA_FK = IDQUINCENA
          AND NOMN.ID_TIPO_NOMINA_FK = TIPONOMINA;


    vQUERY := 'SELECT * FROM 
    (
                SELECT  DECODE(CABNOM.ID_NOMINA_PADRE,NULL,QUIN.NUMERO_QUINCENA,(SELECT NUMERO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS WHERE ID_QUINCENA = (SELECT ID_QUINCENA_FK FROM RH_NOMN_CABECERAS_NOMINAS WHERE ID_NOMINA = CABNOM.ID_NOMINA_PADRE) )) ID_QUINCENA,
                        TO_NUMBER(EM1.NUMERO_EMPLEADO) NUMERO_EMPLEADO,
                        DAT1.NOMBRE||'' ''||DAT1.A_PATERNO||'' ''||DAT1.A_MATERNO NOMBRE 
                        ,UNIADM.CLAVE_UNIADM
                        ,TIPOCPTO.ID_TIPO_CONCEPTO
                        ,TIPOCPTO.TIPO_CONCEPTO
                        ,CPTOS.CLAVE_CONCEPTO
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 1 OR CABNOM.ID_TIPO_NOMINA_FK = 2 OR CABNOM.ID_TIPO_NOMINA_FK = 3 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0    
                        END ORDINARIA
                        ,CASE WHEN (CABNOM.ID_TIPO_NOMINA_FK = 16 OR CABNOM.ID_TIPO_NOMINA_FK = 17 OR CABNOM.ID_TIPO_NOMINA_FK = 18) AND (SELECT INSTR(UPPER(CABNOM.NOMBRE_NOMINA),''EXTRA'') FROM DUAL) > 0 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                              ELSE 
                                                0
                        END EXTRAORDINARIA
                        ,CASE WHEN (CABNOM.ID_TIPO_NOMINA_FK = 16 OR CABNOM.ID_TIPO_NOMINA_FK = 17 OR CABNOM.ID_TIPO_NOMINA_FK = 18) AND (SELECT INSTR(UPPER(CABNOM.NOMBRE_NOMINA),''RETROACTIVA'') FROM DUAL) > 0 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END RETROACTIVA
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 19 OR CABNOM.ID_TIPO_NOMINA_FK = 20 OR CABNOM.ID_TIPO_NOMINA_FK = 21 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END REVERSA
                        ,CASE WHEN CABNOM.ID_TIPO_NOMINA_FK = 6 THEN --OR CABNOM.ID_TIPO_NOMINA_FK =  OR CABNOM.ID_TIPO_NOMINA_FK = 21 THEN
                                                TO_NUMBER(NVL(CPT.IMPORTE_CONCEPTO,0),''999999999999.99'')
                                            ELSE 
                                                0
                        END AGUINALDO

                FROM RH_NOMN_CABECERAS_NOMINAS             CABNOM';

                IF TIPOCONSULTA = 1 THEN
                    vQUERY := vQUERY||' 
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA      EMPLZ      ON EMPLZ.ID_NOMINA_FK = CABNOM.ID_NOMINA
                    INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA      CPT        ON CPT.ID_NOMEMPPLA_FK =EMPLZ.ID_NOMEMPPLA
                    INNER JOIN RH_NOMN_CAT_CONCEPTOS            CPTOS      ON CPTOS.ID_CONCEPTO = CPT.ID_CONCEPTO_FK';
                ELSE
                    vQUERY := vQUERY||' 
                    INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01     EMPLZ      ON EMPLZ.ID_NOMINA01_FK = CABNOM.ID_NOMINA
                    INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA_01     CPT        ON CPT.ID_NOMEMPPLA01_FK =EMPLZ.ID_NOMEMPPLA01
                    INNER JOIN RH_NOMN_CAT_CONCEPTOS              CPTOS      ON CPTOS.ID_CONCEPTO = CPT.ID_CONCEPTO01_FK';
                END IF;

                vQUERY := vQUERY||'
                INNER JOIN RH_NOMN_CAT_TIPOS_CONCEPTO      TIPOCPTO   ON TIPOCPTO.ID_TIPO_CONCEPTO = CPTOS.ID_TIPO_CONCEPTO_FK
                INNER JOIN RH_NOMN_EMPLEADOS               EM1        ON EM1.ID_EMPLEADO = EMPLZ.ID_EMPLEADO_FK
                INNER JOIN RH_NOMN_PLAZAS                  PLZ        ON PLZ.ID_PLAZA = EM1.ID_PLAZA
                INNER JOIN RH_NOMN_DATOS_PERSONALES        DAT1       ON DAT1.ID_DATOS_PERSONALES = EM1.ID_DATOS_PERSONALES
                INNER JOIN RH_NOMN_ADSCRIPCIONES           ADSC       ON ADSC.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
                INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM     ON UNIADM.ID_UNIADM = ADSC.ID_UNIADM_FK
                INNER JOIN RH_NOMN_CAT_TIPOS_NOMINA        TIPNOM     ON TIPNOM.ID_TIPO_NOMINA = CABNOM.ID_TIPO_NOMINA_FK
                INNER JOIN RH_NOMN_CAT_QUINCENAS           QUIN       ON QUIN.ID_QUINCENA = CABNOM.ID_QUINCENA_FK
                WHERE CABNOM.ID_NOMINA IN (SELECT ID_NOMINA
                                            FROM  RH_NOMN_CABECERAS_NOMINAS 
                                            WHERE ID_NOMINA_PADRE IN (
                                            
                                                  SELECT ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS
                                                  WHERE ID_QUINCENA_FK = '||IDQUINCENA||'
                                                  )
                                            UNION ALL
                                            SELECT ID_NOMINA FROM RH_NOMN_CABECERAS_NOMINAS
                                                  WHERE ID_QUINCENA_FK = '||IDQUINCENA||') ';
                IF TIPONOMINA = 1 THEN
                    vQUERY := vQUERY ||' AND  CABNOM.ID_TIPO_NOMINA_FK IN (1,16,19,6,7) ';
                ELSIF TIPONOMINA = 2 THEN
                    vQUERY := vQUERY ||' AND  CABNOM.ID_TIPO_NOMINA_FK IN (2,17,20,10,11) ';
                ELSIF TIPONOMINA = 3 THEN
                    vQUERY := vQUERY ||' AND  CABNOM.ID_TIPO_NOMINA_FK IN (3,18,21,14,15) ';
                END IF;

                vQUERY := vQUERY||' 

    )PIVOT (
        SUM(ORDINARIA) ORDINARIA,
        SUM(EXTRAORDINARIA)EXTRAORDINARIA,
        SUM(RETROACTIVA) RETROACTIVA,
        SUM(REVERSA) REVERSA,
        SUM(AGUINALDO) AGUINALDO,
        SUM(ORDINARIA+EXTRAORDINARIA+RETROACTIVA+REVERSA+AGUINALDO) TOTAL
        FOR ID_QUINCENA IN ('||NUMQUINCENA||' Q)
    )
    ORDER BY NUMERO_EMPLEADO,ID_TIPO_CONCEPTO';

    --DBMS_OUTPUT.PUT_LINE(vQUERY);

    OPEN DATOS FOR vQUERY;

ELSE
    OPEN DATOS FOR
    SELECT  0 NUMERO_EMPLEADO,
            0 NOMBRE,
            0 CLAVE_UNIADM,
            0 ID_TIPO_CONCEPTO,
            0 TIPO_CONCEPTO,
            0 CLAVE_CONCEPTO,
            0 Q_ORDINARIA,
            0 Q_EXTRAORDINARIA,
            0 Q_RETROACTIVA,
            0 Q_REVERSA,
            0 Q_AGUINALDO,
            0 Q_TOTAL
    FROM DUAL;
END IF;


END SP_CALCULA_ACUMULADOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_CALCULA_IMPUESTOS_NOMINA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CALCULA_IMPUESTOS_NOMINA" (EJERCICIO IN NUMBER, MES IN VARCHAR2, DATOS OUT SYS_REFCURSOR)IS

BEGIN

OPEN DATOS FOR 
SELECT 
 CLAVE_CONCEPTO
        ,CLAVE_PRESUPUESTAL
        ,NOMBRE_CONCEPTO
        ,Q1_ESTRUCTURA
        ,Q1_EVENTUAL
        ,Q1_HONORARIOS
        ,TOTAL1
        ,Q2_ESTRUCTURA
        ,Q2_EVENTUAL
        ,Q2_HONORARIOS
        ,TOTAL2
        ,(TOTAL1 + TOTAL2)PGRAVADOS
        ,((TOTAL1 + TOTAL2)*.3) PSOBRENOM
FROM 
(
SELECT   CLAVE_CONCEPTO
        ,CLAVE_PRESUPUESTAL
        ,NOMBRE_CONCEPTO
        ,Q1_ESTRUCTURA
        ,Q1_EVENTUAL
        ,Q1_HONORARIOS
        ,(Q1_ESTRUCTURA + Q1_EVENTUAL + Q1_HONORARIOS) TOTAL1
        ,Q2_ESTRUCTURA
        ,Q2_EVENTUAL
        ,Q2_HONORARIOS
        ,(Q2_ESTRUCTURA + Q2_EVENTUAL + Q2_HONORARIOS) TOTAL2

FROM
(

SELECT   CPTS.CLAVE_CONCEPTO 
        ,PART.CLAVE_PRESUPUESTAL
        ,CPTS.NOMBRE_CONCEPTO 
        ,DECODE(NOMN.ID_TIPO_NOMINA_FK,1,CPTEM.IMPORTE_CONCEPTO,0) ESTRUCTURA
        ,DECODE(NOMN.ID_TIPO_NOMINA_FK,2,CPTEM.IMPORTE_CONCEPTO,0) EVENTUAL
        ,DECODE(NOMN.ID_TIPO_NOMINA_FK,3,CPTEM.IMPORTE_CONCEPTO,0) HONORARIOS
        ,CASE WHEN Quin.Numero_Quincena = 1 OR Quin.Numero_Quincena = 3 OR Quin.Numero_Quincena = 5 OR 
                   Quin.Numero_Quincena = 7 OR Quin.Numero_Quincena = 9 OR Quin.Numero_Quincena = 11 THEN
                'QUINCENA 1'
              WHEN Quin.Numero_Quincena = 2 OR Quin.Numero_Quincena = 4 OR Quin.Numero_Quincena = 6 OR 
                   Quin.Numero_Quincena = 8 OR Quin.Numero_Quincena = 10 OR Quin.Numero_Quincena = 12 THEN  
                'QUINCENA 2'
        END
        DESCRIPCION_QUINCENA
FROM RH_NOMN_CABECERAS_NOMINAS NOMN
LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA EMPLZ ON EMPLZ.ID_NOMINA_FK     = NOMN.ID_NOMINA
LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CPTEM ON CPTEM.ID_NOMEMPPLA_FK  = EMPLZ.ID_NOMEMPPLA
LEFT JOIN RH_NOMN_CAT_CONCEPTOS       CPTS  ON CPTS.ID_CONCEPTO       = CPTEM.ID_CONCEPTO_FK
LEFT JOIN RH_NOMN_CAT_PARTIDAS_PRESUP PART  ON PART.ID_PARTIDA_PRESUP = CPTS.ID_PARTIDA_PRESUP_FK
LEFT JOIN RH_NOMN_CAT_QUINCENAS       QUIN  ON QUIN.ID_QUINCENA       = NOMN.ID_QUINCENA_FK 
LEFT JOIN RH_NOMN_CAT_EJERCICIOS      EJER  ON EJER.ID_EJERCICIO      = QUIN.ID_EJERCICIO_FK
WHERE UPPER(CPTS.APLICA_ISN) = 'S'
AND EJER.ID_EJERCICIO = EJERCICIO
AND QUIN.ID_QUINCENA IN (SELECT QUIN1.ID_QUINCENA 
                         FROM RH_NOMN_CAT_QUINCENAS QUIN1
                         WHERE TO_CHAR(QUIN1.FECHA_INICIO_QUINCENA,'MM') = MES
                         AND QUIN1.ID_EJERCICIO_FK = EJERCICIO
                        )
)
PIVOT(
         SUM(ESTRUCTURA) ESTRUCTURA
        ,SUM(EVENTUAL)   EVENTUAL
        ,SUM(HONORARIOS) HONORARIOS
        FOR(DESCRIPCION_QUINCENA) IN ('QUINCENA 1' Q1,'QUINCENA 2' Q2)
)
)


;


END SP_CALCULA_IMPUESTOS_NOMINA;

/
--------------------------------------------------------
--  DDL for Procedure SP_CARGA_MASIVA_MYT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CARGA_MASIVA_MYT" 
    (
     datos       IN STRING_ARRAY,      
     V_CURSOR OUT SYS_REFCURSOR) 
IS
   C1 SYS_REFCURSOR;
   qryTmp varchar(3000);
   cntregTemp integer;
  qry1 varchar(3000);
  qry2 varchar(3000);
  
  c1_ID_REGISTRO NUMBER;
  c1_ID_EMPLEADO NUMBER; 
  c1_CLAVE_CONCEPTO VARCHAR2(10);
  c1_FECHA_INICIO DATE;
  c1_FECHA_FIN DATE;
  c1_PORCENTAJE NUMBER(3,0);
  c1_IMPORTE NUMBER(8,2);
  c1_CANTIDAD NUMBER(8,0); 
  c1_COMENTARIOS  VARCHAR2(400);

  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
  
  tablatmp        VARCHAR2(300) ; --Para generar el nombre de la tabla temporar para crear
  existTable      integer := 0;

  vlRetCursor     SYS_REFCURSOR;     
  vCadenaSQL      varchar2(4000);  
  
  tmpVal          TYPE_STRING; --Para leer la variable TYPE_STRING
  tmpRegistro     VARCHAR2(3000):= ''; --Para generar el registro a insertar
  
  contTotRegistros    NUMERIC; -- cuenta el total de registros leidos y se usa para el id de la tabla temp
  contTotAceptados    NUMERIC; -- cuenta el total de registros que cumplen con las validaciones
  conttotRechazado    NUMERIC; -- cuenta el total de los registros que han sido rchazados
      
  -----------------------------------------------------------------------------
  
  varError varchar2(4000) := '0,';
  idTipoNombEmp number;
  cntConcepto number;
  
  idConcepto number; varEstatus VARCHAR2(1 BYTE); idTipoNombConc number;          idClasificador number;
  
  cntIdEmp number; cntIdMyT number; cntQuincenas number; cntIdNomina number;
  
  vCadenaSQL varchar2(4000); 
  apoyoAnteojos number;
  exaMedGen number;
BEGIN 
SELECT VALOR_CONSTANTE INTO apoyoAnteojos FROM RH_NOMN_CAT_ARGUMENTOS WHERE CLAVE_ARGUMENTO=':VALORMAXANTEOJOS';
SELECT VALOR_CONSTANTE INTO exaMedGen FROM RH_NOMN_CAT_ARGUMENTOS WHERE CLAVE_ARGUMENTO=':EXAMEDGEN';
  
  tablatmp  := 'TMP_MYT' || '_'|| TO_CHAR(SYSDATE,'ddmmyy_hhmmss');

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp
        AND OWNER = 'SERP';
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
    
    ----------------------------------------------------------------------
    --  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
    ----------------------------------------------------------------------
     
       EXECUTE IMMEDIATE 'CREATE TABLE '||tablatmp||' 
       (  
          ID_REGISTRO NUMBER,
          ID_EMPLEADO NUMBER, 
          CLAVE_CONCEPTO VARCHAR2(10),
          FECHA_INICIO DATE, 
          FECHA_FIN DATE, 
          PORCENTAJE NUMBER(3,0), 
          IMPORTE NUMBER(8,2), 
          CANTIDAD NUMBER(8,0), 
          COMENTARIOS VARCHAR2(400),
          ESTATUS number,
          MSJ_ERROR VARCHAR2(4000)
       )';  
          
    ---------------------------------------------------------------------------                                                          
    -- R E C O R R E M O S   E L   A R R E G L O   P A R A   I N S E R T A R  
    -- L O S   V A L O R E S   E N   L A   T A B L A                                                         
    --------------------------------------------------------------------------- 
      contTotRegistros := 1;
      FOR idxj IN datos.first()..datos.last() 
        LOOP
          tmpVal  := datos(idxj);        
          SELECT TRANSLATE(tmpVal.REGISTRO,'����������', 'aeiouAEIOU') INTO tmpRegistro FROM DUAL;
          EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' || contTotRegistros || ','|| tmpRegistro || ', 0, null )';         
          contTotRegistros := contTotRegistros + 1;
        END LOOP;
  
       
  ---------------------------------------------------------------------------
  -- R E C O R R E M O S   L O S   R E G I S T R O S   D E   L A   T A B L A
  ---------------------------------------------------------------------------
    qry1 := 'SELECT 
          ID_REGISTRO , ID_EMPLEADO , CLAVE_CONCEPTO, FECHA_INICIO , FECHA_FIN , PORCENTAJE , IMPORTE , CANTIDAD , COMENTARIOS 
       FROM    
        ' || tablatmp ;
    
    OPEN C1 FOR qry1;
      LOOP  
        FETCH C1 INTO 
          c1_ID_REGISTRO ,
          c1_ID_EMPLEADO , 
          c1_CLAVE_CONCEPTO ,
          c1_FECHA_INICIO , 
          c1_FECHA_FIN , 
          c1_PORCENTAJE , 
          c1_IMPORTE , 
          c1_CANTIDAD , 
          c1_COMENTARIOS;
        EXIT
            WHEN C1%NOTFOUND;  
            
          varError := '';  
        
         
          -- 1  id empleado no existe
              select count(ID_EMPLEADO) INTO cntIdEmp FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = c1_ID_EMPLEADO ;
                if(cntIdEmp < 1)then
                    varError := varError ||  '1,';
                else
                    varError := '0,';
                    -- 2  empleado no esta activo
                        select count(ID_EMPLEADO) INTO cntIdEmp FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = c1_ID_EMPLEADO AND ID_ESTATUS_EMPLEADO = 1 ;
                        DBMS_OUTPUT.PUT_LINE('    ---- cntIdEmp ----> ' || cntIdEmp);
                        if(cntIdEmp < 1)then
                            varError := varError || '2,';
                        else
                            -- 3  tipo nombramiento debe ser estructura (1) o eventual (2)
                                SELECT  PLA.ID_TIPO_NOMBRAM_FK 
                                INTO    idTipoNombEmp  
                                FROM    RH_NOMN_EMPLEADOS emp
                                INNER JOIN RH_NOMN_PLAZAS PLA ON emp.ID_PLAZA = PLA.ID_PLAZA
                                INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TINOM ON PLA.ID_TIPO_NOMBRAM_FK = TINOM.ID_TIPO_NOMBRAMIENTO
                                WHERE   NUMERO_EMPLEADO = c1_ID_EMPLEADO AND ID_ESTATUS_EMPLEADO = 1 ;
                                
                                if(idTipoNombEmp = 3 OR idTipoNombEmp = 0 )then
                                    varError := varError || '3,';
                                else
                                    -- 4A Concepto: no existe
                                    -- 4A Concepto: estatus inactivo
                                      select  count(ID_CONCEPTO)
                                      into    cntConcepto
                                      from    RH_NOMN_CAT_CONCEPTOS
                                      where   CLAVE_CONCEPTO =  c1_CLAVE_CONCEPTO AND ESTATUS = 'A';
                                      
                                      if(cntConcepto < 1)then
                                         varError := varError || '4A,';                                      
                                      else                                          
                                          -- 4C Concepto: no corresponde al tipo de nombramiento del empleado
                                          -- 4D Concepto: clasificador del concepto no corresponde a manual (2) o tercero(3)                                          
                                            select  ID_CONCEPTO, ESTATUS,   ID_TIPO_NOMBRAMIENTO_FK, ID_CLASIFICADOR_CONCEPTO_FK
                                            into    idConcepto, varEstatus, idTipoNombConc,          idClasificador
                                            from    RH_NOMN_CAT_CONCEPTOS
                                            where   CLAVE_CONCEPTO =  c1_CLAVE_CONCEPTO AND ESTATUS = 'A';
                                            
                                            if(c1_CLAVE_CONCEPTO='30D' OR c1_CLAVE_CONCEPTO='3DE'   ) THEN 
                                              
                                              if(c1_IMPORTE>apoyoAnteojos)then
                                                varError:=varError||'4F,';
                                              end if;

                                            end if;
                                            
                                            
                                            
                                              
                                            if(  c1_CLAVE_CONCEPTO='74' OR c1_CLAVE_CONCEPTO='74E') THEN 
                                              
                                              if(c1_IMPORTE>exaMedGen)then
                                                varError:=varError||'4F,';
                                              end if;

                                            end if;
                                            
                                            
                                            
                                            if(idTipoNombConc != idTipoNombEmp)then
                                                varError := varError || '4C,' || idTipoNombConc || '--'|| idTipoNombEmp;  
                                            end if;
                                            
                                            if(idClasificador != 2 AND idClasificador != 3 AND idClasificador != 6)then
                                                varError := varError || '4D,';  
                                            end if;
                                            
                                         -- 4E Concepto: ya existe un concepto con esta vigencia  
                                         
                                             if(c1_FECHA_FIN is not null)then
                                                select count(ID_MANUALES_TERCEROS) into cntIdMyT 
                                                from RH_NOMN_MANUALES_TERCEROS 
                                                where ID_EMPLEADO = (
                                                                      SELECT ID_EMPLEADO 
                                                                      FROM RH_NOMN_EMPLEADOS 
                                                                      WHERE ID_ESTATUS_EMPLEADO = 1 AND NUMERO_EMPLEADO = c1_ID_EMPLEADO
                                                                    ) 
                                                AND ID_CONCEPTO = idConcepto 
                                                AND ACTIVO = 'A' 
                                                AND ( ( trunc(c1_FECHA_INICIO) BETWEEN trunc(FECHA_INICIO) AND trunc(FECHA_FIN) ) OR (trunc(c1_FECHA_FIN) BETWEEN trunc(FECHA_INICIO) AND trunc(FECHA_FIN) ) );
                                             else
                                                select count(ID_MANUALES_TERCEROS) into cntIdMyT 
                                                from RH_NOMN_MANUALES_TERCEROS 
                                                where ID_EMPLEADO = (SELECT ID_EMPLEADO FROM RH_NOMN_EMPLEADOS WHERE ID_ESTATUS_EMPLEADO = 1 AND NUMERO_EMPLEADO = c1_ID_EMPLEADO) AND ID_CONCEPTO = idConcepto AND ACTIVO = 'A' 
                                                AND trunc(c1_FECHA_INICIO) >= trunc(FECHA_INICIO)  ;
                                            end if;
                                        
                                            if( cntIdMyT > 0 )then
                                              varError := varError || '4E,'; 
                                            end if;
                                            
                                            
                                      end if; -- 4
                                      
                                      
                                    -- Validamos si ya hay un registro identico guardado en la tabla temp
                                      cntregTemp := 0;
                                      
                                      qryTmp := '
                                      select count(ID_REGISTRO) 
                                      
                                      from ' || tablatmp  || '
                                      where 
                                          ID_EMPLEADO     = ' || c1_ID_EMPLEADO   || '
                                      AND CLAVE_CONCEPTO  = ''' ||c1_CLAVE_CONCEPTO || '''
                                      AND FECHA_INICIO    =  ''' ||c1_FECHA_INICIO || '''
                                      ';
                                      
                                      if(c1_FECHA_FIN is not null)then
                                        qryTmp := qryTmp || ' AND FECHA_FIN      = ''' || c1_FECHA_FIN   || '''' ;
                                      end if; 
                                      
                                      if(c1_PORCENTAJE is not null)then
                                        qryTmp := qryTmp || ' AND PORCENTAJE      = ' || c1_PORCENTAJE    ;
                                      end if;  
                                      
                                      if( c1_IMPORTE is not null)then
                                        qryTmp := qryTmp || ' AND IMPORTE      = ' || c1_IMPORTE      ;
                                      end if;
                                      
                                      if(c1_CANTIDAD is not null)then
                                        qryTmp := qryTmp || ' AND CANTIDAD     = ' || c1_CANTIDAD     ;
                                      end if;
                                      
                                      
                                      EXECUTE IMMEDIATE qryTmp                                     
                                      into cntregTemp;
                                     
                                      
                                      if(cntregTemp > 1)then
                                        varError := varError || '4E,'; 
                                      end if;
                                  -----------------------------------------------
                                  -- F E C H A S   I N I C I O   Y   F I N
                                  -----------------------------------------------
                                    -- 5A Fechas: Corresponden con alguna quincena pendiente de procesar.
                                    
                                      cntQuincenas := 0;
                                      
                                      if( idTipoNombEmp = 1)then 
                                          
                                          select count(cn.ID_QUINCENA_FK) 
                                          into  cntQuincenas 
                                          from  RH_NOMN_CABECERAS_NOMINAS cn
                                          where cn.ID_ESTATUS_NOMINA_FK = 3 
                                            and cn.ID_QUINCENA_FK in (
                                                                      select  ID_QUINCENA
                                                                      from    RH_NOMN_CAT_QUINCENAS cq
                                                                      where   
                                                                              TRUNC(c1_FECHA_INICIO) = TRUNC(cq.FECHA_INICIO_QUINCENA)
                                                                        OR    TRUNC(c1_FECHA_FIN)    = TRUNC(cq.FECHA_FIN_QUINCENA)      
                                                                       
                                                                  )
                                                                  
                                            AND cn.ID_TIPO_NOMINA_FK IN ( 1,16 );
                                       else
                                       
                                        select count(cn.ID_QUINCENA_FK) 
                                          into  cntQuincenas 
                                          from  RH_NOMN_CABECERAS_NOMINAS cn
                                          where cn.ID_ESTATUS_NOMINA_FK = 3 
                                            and cn.ID_QUINCENA_FK in (
                                                                      select  ID_QUINCENA
                                                                      from    RH_NOMN_CAT_QUINCENAS cq
                                                                      where   
                                                                              TRUNC(c1_FECHA_INICIO) = TRUNC(cq.FECHA_INICIO_QUINCENA)
                                                                        OR    TRUNC(c1_FECHA_FIN)    = TRUNC(cq.FECHA_FIN_QUINCENA)      
                                                                       
                                                                  )
                                                                  
                                            AND cn.ID_TIPO_NOMINA_FK IN ( 2,17 );
                                            
                                       end if;
                                       
                                    
                                      if(cntQuincenas > 0)then
                                        varError := varError || '5A,' ; 
                                      end if;                               
                                    
                                    -- 5B Fechas: No se encuentren dentro un periodo Ejercicio Fiscal � Quincena definida en el sistema. 
                                    -- 5C Fechas: No corresponder con la definici�n de inicio y fin de Quincenas. 
                                    
                                      select count(ID_QUINCENA) 
                                      into cntQuincenas 
                                      from RH_NOMN_CAT_QUINCENAS 
                                      --where ACTIVO = 1 AND 
                                      where TRUNC(c1_FECHA_INICIO) = TRUNC(FECHA_INICIO_QUINCENA);
                                      
                                      if(cntQuincenas = 0)then
                                        varError := varError || '5C,'; 
                                      end if;
                                      
                                      if(c1_FECHA_FIN is not null )then
                                        select count(ID_QUINCENA) 
                                        into cntQuincenas 
                                        from RH_NOMN_CAT_QUINCENAS 
                                        --where ACTIVO = 1 AND 
                                        where TRUNC(c1_FECHA_FIN) = TRUNC(FECHA_FIN_QUINCENA);
                                        
                                        if(cntQuincenas = 0)then
                                          varError := varError || '5C,'; 
                                        end if;
                                      
                                      end if;
                                      
                                  -----------------------------------------------
                                  -- F E C H A S   I N I C I O   Y   F I N
                                  -----------------------------------------------
                                     if(cntQuincenas > 1)then
                                        if(c1_FECHA_FIN is not null )then
                                            SELECT COUNT(ID_NOMINA) 
                                            INTO cntIdNomina 
                                            FROM RH_NOMN_CABECERAS_NOMINAS 
                                            WHERE  ID_ESTATUS_NOMINA_FK = 3 AND  
                                                 ID_QUINCENA_FK IN 
                                                    ( select ID_QUINCENA  
                                                      from RH_NOMN_CAT_QUINCENAS 
                                                      --where ACTIVO = 1 AND  
                                                      where TRUNC(FECHA_INICIO_QUINCENA) >= TRUNC(c1_FECHA_INICIO) 
                                                      AND TRUNC(FECHA_FIN_QUINCENA) <= TRUNC(c1_FECHA_FIN)  )  ;
                                        else
                                            SELECT COUNT(ID_NOMINA) 
                                            INTO cntIdNomina
                                            FROM RH_NOMN_CABECERAS_NOMINAS 
                                            WHERE  ID_ESTATUS_NOMINA_FK = 3 
                                            AND  ID_QUINCENA_FK IN (select ID_QUINCENA  
                                                                    from RH_NOMN_CAT_QUINCENAS 
                                                                    --where ACTIVO = 1 AND  
                                                                    where TRUNC(FECHA_INICIO_QUINCENA) >= TRUNC(c1_FECHA_INICIO)  )  ;
                                        end if;
                                        
                                        
                                          
                                                    
                                     end if;
                                                    
                                end if; --3                                
                        end if; -- 2
                end if; -- 1  
                
                if(  varError  != '0,' )then
                    EXECUTE IMMEDIATE ' update '|| tablatmp || ' SET estatus = 1, MSJ_ERROR = ''' || varError || ''' where ID_REGISTRO = '|| c1_ID_REGISTRO;
                end if;
         
          
        
      END LOOP;
      CLOSE C1;
  
       qry2 := 'SELECT 
          ID_REGISTRO , ID_EMPLEADO , CLAVE_CONCEPTO, FECHA_INICIO , FECHA_FIN , NVL(PORCENTAJE, 0.0) AS dblPORCENTAJE , NVL(IMPORTE, 0.0) as dblIMPORTE , NVL(CANTIDAD, 0.0) as dblCANTIDAD , COMENTARIOS, ESTATUS, MSJ_ERROR
       FROM    
        ' || tablatmp || '  ORDER BY  ESTATUS ASC, ID_EMPLEADO ASC, CLAVE_CONCEPTO ASC ';
        
        OPEN V_CURSOR FOR qry2;
      
  
  
 
  EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
  --CLOSE V_CURSOR;
  
  EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp 
      AND OWNER = 'SERP';
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
  V_CURSOR := null;  
  
END SP_CARGA_MASIVA_MyT ;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_ARCHIVO_CANCELACION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_ARCHIVO_CANCELACION" (UUIDS IN STRING_ARRAY, IDTIMBRADO IN NUMBER)IS

CONT NUMBER;
UUID TYPE_STRING;
AGREGAR_UUIDS CLOB;
XMLFINAL XMLTYPE;

ERR_CODE NUMBER;
ERR_MSG VARCHAR2(200);

CABECEROXML CLOB:= '<?xml version="1.0" encoding="iso-8859-1" ?>
                    <Cancelacion xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                     xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                                     Fecha="'||TO_CHAR(SYSDATE,'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE,'HH24:MI:SS')||'"
                                     RfcEmisor="IFD130924CX1"
                                     xmlns="http://cancelacfd.sat.gob.mx">';

PIEXML CLOB:='              
              <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                 <SignedInfo>
                    <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
                    <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
                    <Reference URI="">
                        <Transforms>
                            <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
                        </Transforms>
                        <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
                        <DigestValue></DigestValue>
                    </Reference>
                 </SignedInfo>
                 <SignatureValue></SignatureValue>
                 <KeyInfo>
                    <X509Data>
                        <X509IssuerSerial>
                            <X509IssuerName>OID.1.2.840.113549.1.9.2=Responsable: Claudia Covarrubias Ochoa, OID.2.5.4.45=SAT970701NN3, L=Cuauht�moc, S=Distrito Federal, C=MX, PostalCode=06300, STREET='||chr(34)||'Av. Hidalgo 77, Col. Guerrero'||chr(34)||', E=acods@sat.gob.mx, OU=Administraci�n de Seguridad de la Informaci�n, O=Servicio de Administraci�n Tributaria, CN=A.C. del Servicio de Administraci�n Tributaria</X509IssuerName>
                            <X509SerialNumber></X509SerialNumber>
                        </X509IssuerSerial>
                        <X509Certificate></X509Certificate>
                    </X509Data>
                 </KeyInfo>
               </Signature>
        </Cancelacion>';


BEGIN 

    CONT := 0;
    AGREGAR_UUIDS :='';
    FOR IDX IN UUIDS.first()..UUIDS.last()
    LOOP
        IF CONT = 500 THEN 
          CONT := 1;  

          SELECT XMLTYPE(CABECEROXML||AGREGAR_UUIDS||PIEXML) INTO XMLFINAL
          FROM DUAL;

          INSERT INTO RH_NOMN_CFDI_CANCELACIONES(ID_CFDI_CANCELADO,ARCHIVO_CANCELADO,ID_CFDI_TIMBRADO_FK,ARCHIVO_PENDIENTE)
          VALUES((SELECT NVL(MAX(ID_CFDI_CANCELADO),0)+1 
                  FROM RH_NOMN_CFDI_CANCELACIONES ),
                  NULL,
                  IDTIMBRADO,
                  XMLFINAL
                  );

        ELSE
            UUID := UUIDS(IDX);
            AGREGAR_UUIDS := AGREGAR_UUIDS||'<Folios><UUID>'||UUID.REGISTRO||'</UUID></Folios>';
        END IF;
        CONT := CONT+1;
    END LOOP;

    IF CONT < 500 THEN
        SELECT XMLTYPE(CABECEROXML||AGREGAR_UUIDS||PIEXML) INTO XMLFINAL
          FROM DUAL;
        INSERT INTO RH_NOMN_CFDI_CANCELACIONES(ID_CFDI_CANCELADO,ARCHIVO_CANCELADO,ID_CFDI_TIMBRADO_FK,ARCHIVO_PENDIENTE)
          VALUES((SELECT NVL(MAX(ID_CFDI_CANCELADO),0)+1 
                  FROM RH_NOMN_CFDI_CANCELACIONES ),
                  NULL,
                  IDTIMBRADO,
                  XMLFINAL
                  );  
    END IF;


EXCEPTION 
    WHEN OTHERS THEN 
    ERR_CODE := SQLCODE;
    ERR_MSG := SUBSTR(SQLERRM, 1, 200);
    SPDERRORES('SP_CFDI_ARCHIVO_CANCELACION',err_code,err_msg,''); 

END SP_CFDI_ARCHIVO_CANCELACION;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_INSERTAR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_INSERTAR" (idCFDI IN  NUMBER, idTimbrado IN NUMBER,idCancelado IN NUMBER, accion IN NUMBER ,archTimbrado IN BLOB, archPendiente IN BLOB, archCancelado IN BLOB,msg OUT VARCHAR2)IS
VALIDOIDS VARCHAR2(200);

FUNCTION VALIDARIDS(IDCFDI IN NUMBER,archTimbrado IN BLOB) RETURN VARCHAR2 IS
    ID1 VARCHAR2(10);
    ID2 VARCHAR2(10);
    NUMUUID NUMBER;
    RESPUESTA  VARCHAR2(100) := 'EXITO';
BEGIN
    SELECT EXTRACTVALUE(XMLTYPE(CFDI.XML,2), '//PaqueteNomina/@id','xmlns="http://www.sat.gob.mx/Nomina/Paquete"') ID1,
           EXTRACTVALUE(XMLTYPE(archTimbrado,null), '//PaqueteNomina/@id','xmlns="http://www.sat.gob.mx/Nomina/Paquete"') ID2 
           INTO ID1,ID2
    FROM RH_NOMN_CFDI CFDI
    WHERE CFDI.ID_CFDI = idCFDI;        
IF ID1 = ID2 THEN 

   SELECT COUNT(XML2.UUID) INTO NUMUUID
   FROM XMLTABLE(XMLNAMESPACES(DEFAULT 'http://www.sat.gob.mx/Nomina/Paquete'),'/PaqueteNomina/ControlComprobante/xmlComp'
                       PASSING XMLTYPE(archTimbrado,NULL)
                       COLUMNS DATOS XMLTYPE PATH '//text()'
                       )XML1
   ,XMLTABLE(XMLNAMESPACES('http://www.sat.gob.mx/cfd/3' AS "cfdi"
                                    ,'http://www.sat.gob.mx/TimbreFiscalDigital' AS "tfd" ),'/cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital'
                       PASSING XMLTYPE(REPLACE(REPLACE(XML1.DATOS.GETCLOBVAL(),'<![CDATA[',''),']]>',''))
                       COLUMNS UUID VARCHAR2(50) PATH '//@UUID'
                       )XML2
    ;
    IF NUMUUID = 0 THEN
        RESPUESTA := 'El archivo no esta timbrado';
    END IF;
ELSE
    RESPUESTA := 'El folio es incorrecto';
END IF;
RESPUESTA := 'EXITO';

RETURN RESPUESTA;    

END VALIDARIDS;    


BEGIN         
        IF ACCION = 1 AND archTimbrado IS NOT NULL THEN ----- INSERTAMOS EN LA TABLA RH_NOMN_CFDI_TIMBRADOS

            IF idTimbrado IS NULL THEN
                VALIDOIDS := VALIDARIDS(idCFDI,archTimbrado);
                --VALIDOIDS := 1;
                IF VALIDOIDS = 'EXITO' THEN
                    INSERT INTO RH_NOMN_CFDI_TIMBRADOS VALUES ( (SELECT NVL(MAX(ID_CFDI_TIMBRADO),0)+1 FROM RH_NOMN_CFDI_TIMBRADOS),
                                                                idCFDI,
                                                                archTimbrado,
                                                                (SELECT NVL(MAX(VERSION),0)+1 FROM RH_NOMN_CFDI_TIMBRADOS WHERE ID_CFDI_FK = idCFDI)
                                                              );
                    msg := 'EXITO';
                ELSE
                    msg := VALIDOIDS;
                END IF;
            ELSE
                UPDATE RH_NOMN_CFDI_TIMBRADOS SET ARCHIVOTIMBRADO = archTimbrado
                WHERE ID_CFDI_TIMBRADO = idTimbrado;
                msg := 'EXITO';
            END IF;

        ELSIF ACCION = 2  THEN

            IF  idTimbrado IS NOT NULL AND archPendiente IS NOT NULL THEN ---- GUARDAMOS EN LA TABLA RH_NOMN_CFDI_CANCELACIONES
                INSERT INTO RH_NOMN_CFDI_CANCELACIONES VALUES ( (SELECT NVL(MAX(ID_CFDI_CANCELADO),0)+1 FROM RH_NOMN_CFDI_CANCELACIONES),
                                                                 archPendiente,
                                                                 idTimbrado,
                                                                 NULL
                                                              );
                msg := 'EXITO';                                  
            ELSIF idCancelado IS NOT NULL AND archCancelado IS NOT NULL THEN
                UPDATE RH_NOMN_CFDI_CANCELACIONES SET ARCHIVO_CANCELADO = archCancelado
                WHERE ID_CFDI_CANCELADO = idCancelado;
                msg := 'EXITO';
            END IF;
    END IF;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);
    msg := 'ERROR '||SQLERRM;

END SP_CFDI_INSERTAR;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_OBTN_ARCHIVO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_OBTN_ARCHIVO" (IDCFDI IN NUMBER, ARCHIVO OUT BLOB)IS
BEGIN

        SELECT
              XML
        INTO ARCHIVO      
        FROM RH_NOMN_CFDI
        WHERE ID_CFDI = IDCFDI;

END SP_CFDI_OBTN_ARCHIVO;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_OBTN_ARCHIVO_CANCELADO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_OBTN_ARCHIVO_CANCELADO" (IDCFDICANCELADO IN NUMBER, ARCHIVO OUT BLOB) IS

BEGIN


SELECT FN_UTILS_CLOB_TO_BLOB(REPLACE(xmltype(C.ARCHIVO_PENDIENTE.GETCLOBVAL(),null).getclobval(),'WINDOWS-1252','UTF-8'))
INTO ARCHIVO
FROM RH_NOMN_CFDI_CANCELACIONES C
WHERE C.ID_CFDI_CANCELADO = IDCFDICANCELADO;

END SP_CFDI_OBTN_ARCHIVO_CANCELADO;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_OBTN_UUID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_OBTN_UUID" (TIPOCONSULTA IN NUMBER, IDNOMINA IN NUMBER, DATOS OUT SYS_REFCURSOR) IS

BEGIN


        IF TIPOCONSULTA = 1 THEN
            OPEN DATOS FOR
                    SELECT TMPXML1.NUMEMP, TMPXML.UUID 
                    FROM 
                    (
                    SELECT XML1.UUID
                          FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
                          CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                               PASSING CANCEL.ARCHIVO_PENDIENTE
                                               COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                             )XML1
                          WHERE CANCEL.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO_FK 
                                                               FROM RH_NOMN_CFDI_TIMBRADOS
                                                               WHERE ID_CFDI_FK IN (SELECT ID_CFDI 
                                                                                    FROM RH_NOMN_CFDI  
                                                                                    WHERE ID_NOMINA_FK = IDNOMINA)
                                                              )                  
                          AND CANCEL.ARCHIVO_PENDIENTE IS NOT NULL
                          MINUS
                          SELECT DISTINCT(XML2.UUID)
                          FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
                          CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'//../Folios'
                                               PASSING XMLTYPE(CANCEL.ARCHIVO_CANCELADO,2)
                                               COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                             )XML2
                          WHERE CANCEL.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO_FK 
                                                               FROM RH_NOMN_CFDI_TIMBRADOS
                                                               WHERE ID_CFDI_FK IN (SELECT ID_CFDI 
                                                                                    FROM RH_NOMN_CFDI  
                                                                                    WHERE ID_NOMINA_FK = IDNOMINA)
                                                              )
                          AND CANCEL.ARCHIVO_CANCELADO IS NOT NULL
                         )TMPXML
                         CROSS JOIN (

                                    SELECT  XML.UUID,
                                            XML.NUMEMP
                                    FROM  RH_NOMN_CFDI_TIMBRADOS CFDI,
                                    XMLTABLE( XMLNAMESPACES( 'http://www.sat.gob.mx/nomina12' AS "nomina12"
                                                            ,'http://www.sat.gob.mx/cfd/3' AS "cfdi"
                                                            ,'http://www.sat.gob.mx/TimbreFiscalDigital' AS "tfd"
                                                            ,DEFAULT 'http://www.sat.gob.mx/Nomina/Paquete'
                                                           ),'/PaqueteNomina/ControlComprobante/xmlComp'
                                              PASSING XMLTYPE( (select replace(
                                                                           replace(
                                                                             replace(XMLTYPE(CFDI.ARCHIVOTIMBRADO ,null).getclobval(),'WINDOWS-1252','ISO-8859-1')
                                                                                 ,'<![CDATA[','')
                                                                            ,']]>','') FROM DUAL)
                                                             )                 
                                            COLUMNS UUID     VARCHAR2(150) PATH '//cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital/@UUID',
                                                    NUMEMP   NUMBER        PATH '//cfdi:Comprobante/cfdi:Complemento/nomina12:Nomina/nomina12:Receptor/@NumEmpleado'

                                           )XML
                                     WHERE CFDI.ARCHIVOTIMBRADO IS NOT NULL
                                     AND   CFDI.ID_CFDI_FK IN (SELECT ID_CFDI FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = IDNOMINA)
                                )TMPXML1 
                                WHERE TMPXML1.UUID = TMPXML.UUID
              ;


        ELSIF TIPOCONSULTA = 2 THEN
            OPEN DATOS FOR

              SELECT TMPXML1.NUMEMP, TMPXML.UUID 
              FROM 
              (
                  SELECT DECODE(CANCELADOS.UUID,NULL,PENDIENTES.UUID,CANCELADOS.UUID) UUID
                  FROM
                  (SELECT DISTINCT(XML2.UUID)
                  FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
                  CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'//../Folios'
                                       PASSING XMLTYPE(CANCEL.ARCHIVO_CANCELADO,2)
                                       COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                     )XML2
                  WHERE CANCEL.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO_FK 
                                                       FROM RH_NOMN_CFDI_TIMBRADOS
                                                       WHERE ID_CFDI_FK IN (SELECT ID_CFDI 
                                                                            FROM RH_NOMN_CFDI  
                                                                            WHERE ID_NOMINA_FK = IDNOMINA)
                                                      )
                  AND CANCEL.ARCHIVO_CANCELADO IS NOT NULL
                  )CANCELADOS
                  INNER JOIN
                  (
                  SELECT DISTINCT(XML1.UUID)
                  FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
                  CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                       PASSING CANCEL.ARCHIVO_PENDIENTE
                                       COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                     )XML1
                  WHERE CANCEL.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO_FK 
                                                       FROM RH_NOMN_CFDI_TIMBRADOS
                                                       WHERE ID_CFDI_FK IN (SELECT ID_CFDI 
                                                                            FROM RH_NOMN_CFDI  
                                                                            WHERE ID_NOMINA_FK = IDNOMINA)
                                                      )
                  AND CANCEL.ARCHIVO_PENDIENTE IS NOT NULL
                  )PENDIENTES ON CANCELADOS.UUID = PENDIENTES.UUID
              )TMPXML
              CROSS JOIN (
                          SELECT  XML.UUID,
                                  XML.NUMEMP
                          FROM  RH_NOMN_CFDI_TIMBRADOS CFDI,
                                    XMLTABLE( XMLNAMESPACES( 'http://www.sat.gob.mx/nomina12' AS "nomina12"
                                                            ,'http://www.sat.gob.mx/cfd/3' AS "cfdi"
                                                            ,'http://www.sat.gob.mx/TimbreFiscalDigital' AS "tfd"
                                                            ,DEFAULT 'http://www.sat.gob.mx/Nomina/Paquete'
                                                           ),'/PaqueteNomina/ControlComprobante/xmlComp'
                                              PASSING XMLTYPE( (select replace(
                                                                           replace(
                                                                             replace(XMLTYPE(CFDI.ARCHIVOTIMBRADO ,null).getclobval(),'WINDOWS-1252','ISO-8859-1')
                                                                                 ,'<![CDATA[','')
                                                                            ,']]>','') FROM DUAL)
                                                             )                 
                                            COLUMNS UUID     VARCHAR2(150) PATH '//cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital/@UUID',
                                                    NUMEMP   NUMBER        PATH '//cfdi:Comprobante/cfdi:Complemento/nomina12:Nomina/nomina12:Receptor/@NumEmpleado'

                                           )XML
                                     WHERE CFDI.ARCHIVOTIMBRADO IS NOT NULL
                                     AND   CFDI.ID_CFDI_FK IN (SELECT ID_CFDI FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = IDNOMINA)
                                )TMPXML1 
                                WHERE TMPXML1.UUID = TMPXML.UUID

              ;

        END IF;
EXCEPTION 
WHEN OTHERS THEN 
    OPEN DATOS FOR SELECT 'NO HAY DATOS PARA MOSTRAR' X FROM DUAL;
END SP_CFDI_OBTN_UUID;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_QUITAR_UUID_PREV
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_QUITAR_UUID_PREV" (INUUID IN VARCHAR2) IS 
ID_CANCELADO  NUMBER;
DISPONIBLE    NUMBER;
NUMREGISTROS  NUMBER;
BEGIN

    SELECT COUNT(CANCEL.ID_CFDI_CANCELADO)
    INTO DISPONIBLE
    FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
    CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                         PASSING CANCEL.ARCHIVO_PENDIENTE
                         COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                       )XML1
    WHERE XML1.UUID = INUUID
    --AND CANCEL.ARCHIVO_CANCELADO IS NULL
    ;

    IF DISPONIBLE > 0 THEN
        SELECT CANCEL.ID_CFDI_CANCELADO
        INTO ID_CANCELADO 
        FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
        CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                             PASSING CANCEL.ARCHIVO_PENDIENTE
                             COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                           )XML1
        WHERE XML1.UUID = INUUID;

        SELECT COUNT(XML1.UUID)
        INTO NUMREGISTROS 
        FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
        CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                             PASSING CANCEL.ARCHIVO_PENDIENTE
                             COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                           )XML1
        WHERE CANCEL.ID_CFDI_CANCELADO = ID_CANCELADO;

        IF NUMREGISTROS = 1 THEN
            --DBMS_OUTPUT.PUT_LINE('elimina archivo');
            DELETE FROM RH_NOMN_CFDI_CANCELACIONES WHERE  ID_CFDI_CANCELADO = ID_CANCELADO;
        ELSE
            --DBMS_OUTPUT.PUT_LINE('actualiza ');
            UPDATE RH_NOMN_CFDI_CANCELACIONES SET ARCHIVO_PENDIENTE = UPDATEXML(ARCHIVO_PENDIENTE,'/*/*/UUID[text()="'||INUUID||'"]','x','xmlns="http://cancelacfd.sat.gob.mx"');
            UPDATE RH_NOMN_CFDI_CANCELACIONES SET ARCHIVO_PENDIENTE = DELETEXML(ARCHIVO_PENDIENTE,'/*/Folios[text()="x"]','xmlns="http://cancelacfd.sat.gob.mx"');
            commit;
        END IF;

    END IF;

END SP_CFDI_QUITAR_UUID_PREV;

/
--------------------------------------------------------
--  DDL for Procedure SP_CFDI_SEGUIMIENTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CFDI_SEGUIMIENTO" (accion IN NUMBER,idArchivo IN NUMBER, idNomina IN NUMBER, DATOS OUT SYS_REFCURSOR) IS

IDCFDI NUMBER;
VERSION NUMBER;
EXISTE NUMBER;
XMLS SYS_REFCURSOR;
TOTAL_REG     NUMBER;
FECHA_ARCHIVO DATE;

RESP NUMBER;

PROCEDURE OBTCABECERO (IDNOMINA IN NUMBER, DATOS OUT SYS_REFCURSOR ) IS

BEGIN

OPEN DATOS FOR
SELECT  DISTINCT
        CFDI.ID_CFDI ID_CFDI,
        TIMB.ID_CFDI_TIMBRADO ID_CFDI_TIMB,
        CFDI.FECHA_GENERACION FECHA_PROCESO,
        EXTRACTVALUE(XMLTYPE(CFDI.XML,2), '//PaqueteNomina/@totalregs','xmlns="http://www.sat.gob.mx/Nomina/Paquete"') EMITIDOS,
        DECODE(TIMB.ID_CFDI_TIMBRADO,NULL,0,EXTRACTVALUE(XMLTYPE(TIMB.ARCHIVOTIMBRADO,NULL),'//PaqueteNomina/@totalregs','xmlns="http://www.sat.gob.mx/Nomina/Paquete"')) TIMBRADOS,
        (SELECT COUNT(UUID)
         FROM(
              SELECT DISTINCT(XML1.UUID)
              FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
              CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                   PASSING ARCHIVO_PENDIENTE
                                   COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                 )XML1
              WHERE CANCEL.ID_CFDI_TIMBRADO_FK = TIMB.ID_CFDI_TIMBRADO
              AND CANCEL.ARCHIVO_PENDIENTE IS NOT NULL
              MINUS
              SELECT DISTINCT(XML2.UUID)
              FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
              CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'//../Folios'
                                   PASSING XMLTYPE(CANCEL.ARCHIVO_CANCELADO,2)
                                   COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                 )XML2
              WHERE CANCEL.ID_CFDI_TIMBRADO_FK = TIMB.ID_CFDI_TIMBRADO                 
              AND CANCEL.ARCHIVO_CANCELADO IS NOT NULL
            )
        )PENDIENTES,
        (SELECT COUNT(UUID)
         FROM(
              SELECT DECODE(A.UUID,NULL,B.UUID,A.UUID) UUID
              FROM
              (SELECT DISTINCT(XML2.UUID)
              FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
              CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'//../Folios'
                                   PASSING XMLTYPE(CANCEL.ARCHIVO_CANCELADO,2)
                                   COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                 )XML2
              WHERE CANCEL.ID_CFDI_TIMBRADO_FK = TIMB.ID_CFDI_TIMBRADO
              AND CANCEL.ARCHIVO_CANCELADO IS NOT NULL
              )A
              INNER JOIN
              (
              SELECT DISTINCT(XML1.UUID)
              FROM RH_NOMN_CFDI_CANCELACIONES CANCEL
              CROSS JOIN XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                   PASSING ARCHIVO_PENDIENTE
                                   COLUMNS UUID VARCHAR2(100) PATH '//UUID'
                                 )XML1
              WHERE CANCEL.ID_CFDI_TIMBRADO_FK = TIMB.ID_CFDI_TIMBRADO
              AND CANCEL.ARCHIVO_PENDIENTE IS NOT NULL
              )B ON A.UUID = B.UUID
            )
        )CANCELADOS
FROM  RH_NOMN_CFDI CFDI
LEFT JOIN RH_NOMN_CFDI_TIMBRADOS TIMB ON TIMB.ID_CFDI_FK = CFDI.ID_CFDI        
WHERE CFDI.ID_NOMINA_FK = IDNOMINA
ORDER BY ID_CFDI;
END OBTCABECERO;

FUNCTION OBTENERDATOS(idNomina IN NUMBER) RETURN SYS_REFCURSOR IS 

DATOS SYS_REFCURSOR;
    BEGIN
        OPEN DATOS FOR
            SELECT UUID,
                   RFC,
                   FOLIO, 
                   NUMEMP
            FROM(            

                SELECT  DISTINCT 
                        XML.UUID,
                        XML.RFC,
                        XML.FOLIO, 
                        XML.NUMEMP
                FROM  RH_NOMN_CFDI_TIMBRADOS CFDI,
                      XMLTABLE( XMLNAMESPACES( 'http://www.sat.gob.mx/nomina12' AS "nomina12"
                                                      ,'http://www.sat.gob.mx/cfd/3' AS "cfdi"
                                                      ,'http://www.sat.gob.mx/TimbreFiscalDigital' AS "tfd"
                                                      ,DEFAULT 'http://www.sat.gob.mx/Nomina/Paquete'
                                             ),'/PaqueteNomina/ControlComprobante/xmlComp'
                                PASSING XMLTYPE( (select replace(
                                                                 replace(
                                                                         replace(XMLTYPE(CFDI.ARCHIVOTIMBRADO ,null).getclobval(),'WINDOWS-1252','ISO-8859-1')
                                                                         ,'<![CDATA[','')
                                                                 ,']]>','') from dual)
                                                )                 
                                COLUMNS UUID     VARCHAR2(150) PATH '//cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital/@UUID',
                                        RFC      VARCHAR2(15)  PATH '//cfdi:Comprobante/cfdi:Receptor/@Rfc',
                                        FOLIO    NUMBER        PATH '//cfdi:Comprobante/@folio',
                                        NUMEMP   NUMBER        PATH '//cfdi:Comprobante/cfdi:Complemento/nomina12:Nomina/nomina12:Receptor/@NumEmpleado'

                              )XML
                WHERE CFDI.ARCHIVOTIMBRADO IS NOT NULL
                AND   CFDI.ID_CFDI_FK IN (SELECT ID_CFDI FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = idNomina)
               )WHERE   UUID NOT IN(
                                        SELECT XML1.UUID
                                        FROM RH_NOMN_CFDI_CANCELACIONES CFDI1,
                                             XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                                       PASSING CFDI1.ARCHIVO_PENDIENTE
                                                       COLUMNS UUID VARCHAR2(150) PATH 'UUID'
                                                     )XML1
                                        WHERE CFDI1.ARCHIVO_PENDIENTE IS NOT NULL
                                        AND   CFDI1.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO FROM RH_NOMN_CFDI_TIMBRADOS WHERE ID_CFDI_FK IN (SELECT ID_CFDI FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = idNomina))
                                        UNION
                                        SELECT  XML2.UUID
                                        FROM RH_NOMN_CFDI_CANCELACIONES CFDI2,
                                             XMLTABLE( XMLNAMESPACES(DEFAULT 'http://cancelacfd.sat.gob.mx'),'/Cancelacion/Folios'
                                                       PASSING XMLTYPE(CFDI2.ARCHIVO_CANCELADO,2)
                                                       COLUMNS UUID VARCHAR2(150) PATH 'UUID'
                                                     )XML2
                                        WHERE CFDI2.ARCHIVO_CANCELADO IS NOT NULL
                                        AND   CFDI2.ID_CFDI_TIMBRADO_FK IN (SELECT ID_CFDI_TIMBRADO FROM RH_NOMN_CFDI_TIMBRADOS WHERE ID_CFDI_FK IN (SELECT ID_CFDI FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = idNomina))
                                      )             
             ORDER BY FOLIO
        ;

    RETURN DATOS;
END OBTENERDATOS;

BEGIN
           IF accion = 1  THEN 

           SELECT COUNT(1) INTO EXISTE
           FROM RH_NOMN_CFDI WHERE ID_NOMINA_FK = IDNOMINA;

            IF EXISTE >= 1 THEN 


                OBTCABECERO(IDNOMINA,DATOS);



            ELSE

                OPEN DATOS FOR
                    SELECT 1 FROM DUAL;
            END IF;                

           ELSIF accion = 2 THEN

                --SELECT ID_CFDI  INTO IDCFDI
                --FROM   RH_NOMN_CFDI
                --WHERE  ID_NOMINA_FK = idNomina
                --;
               DATOS := OBTENERDATOS(idNomina);
           END IF; 



EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DATOS := NULL;
    DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS');
  
  WHEN OTHERS THEN
    DATOS := NULL;
    DBMS_OUTPUT.PUT_LINE(SQLERRM);


END SP_CFDI_SEGUIMIENTO;

/
--------------------------------------------------------
--  DDL for Procedure SP_COMP_QUINCENA_ANT_ACT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_COMP_QUINCENA_ANT_ACT" (idQuincena IN NUMBER,idTipoNomina IN NUMBER, diferencias OUT SYS_REFCURSOR)
                                         
IS
vESTATUSANT  NUMBER; 
vESTATUSACT  NUMBER;
EJERCICIO    NUMBER;
NUMQUINCENA  NUMBER;
ERR_CODE       INTEGER;
ERR_MSG        VARCHAR2(250);
BEGIN

DBMS_OUTPUT.PUT_LINE('INGRESA');


SELECT Q.ID_EJERCICIO_FK, Q.NUMERO_QUINCENA INTO EJERCICIO,NUMQUINCENA
FROM RH_NOMN_CAT_QUINCENAS  Q
WHERE ID_QUINCENA = idQuincena;

DBMS_OUTPUT.PUT_LINE('NUMQUINCENA'||NUMQUINCENA);

SELECT COUNT(1) INTO vESTATUSACT  
FROM RH_NOMN_CABECERAS_NOMINAS N
WHERE N.ID_QUINCENA_FK = idQuincena
AND N.ID_ESTATUS_NOMINA_FK = 2;


SELECT COUNT (1) INTO vESTATUSANT  
FROM RH_NOMN_CABECERAS_NOMINAS N
WHERE N.ID_QUINCENA_FK IN(SELECT ID_QUINCENA 
                          FROM RH_NOMN_CAT_QUINCENAS Q 
                          WHERE Q.NUMERO_QUINCENA = DECODE((NUMQUINCENA-1),0,24,(NUMQUINCENA-1)) 
                          AND   Q.ID_EJERCICIO_FK = DECODE((NUMQUINCENA-1),0,(EJERCICIO-1),EJERCICIO ) )
AND N.ID_ESTATUS_NOMINA_FK = 3;

IF   vESTATUSACT >= 1  AND   vESTATUSANT >= 1 THEN                    

OPEN diferencias FOR
            SELECT 
                DISTINCT 
                ID_EMPLEADO,
                NOMBRE,
                CLAVE_UNIADM UNIDAD,
                TIPO_NOMINA TIPO_DE_NOMINA,
                TIPO_CONCEPTO TIPO_DE_MOVIMIENTO,
                CLAVE_CONCEPTO CONCEPTO,
                DESCRIPCION,
                NVL(Q1,0) IMPORTE_ANTERIOR,
                NVL(Q2,0) IMPORTE_ACTUAL,
                ABS(NVL(Q1,0) - NVL(Q2,0) ) DIFERENCIA


FROM (

              SELECT EM1.NUMERO_EMPLEADO ID_EMPLEADO
                      ,DAT1.NOMBRE||' '||DAT1.A_PATERNO||' '||DAT1.A_MATERNO NOMBRE 
                      ,UNIADM.CLAVE_UNIADM
                      ,TIPONOM.TIPO_NOMINA
                      ,TIPOCPTO.TIPO_CONCEPTO
                      ,CPTOS.CLAVE_CONCEPTO
                      ,CPTOS.NOMBRE_CONCEPTO DESCRIPCION
                      ,NVL(CPT.IMPORTE_CONCEPTO,0) IMPORTE_CONCEPTO
                      ,1 QUINCENA
               FROM RH_NOMN_CABECERAS_NOMINAS            CABNOM
               LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA     EMPLZ      ON EMPLZ.ID_NOMINA_FK = CABNOM.ID_NOMINA
               LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA     CPT        ON CPT.ID_NOMEMPPLA_FK =EMPLZ.ID_NOMEMPPLA
               LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTOS      ON CPTOS.ID_CONCEPTO = CPT.ID_CONCEPTO_FK
               LEFT JOIN RH_NOMN_CAT_TIPOS_CONCEPTO      TIPOCPTO   ON TIPOCPTO.ID_TIPO_CONCEPTO = CPTOS.ID_TIPO_CONCEPTO_FK
               LEFT  JOIN RH_NOMN_CAT_TIPOS_NOMINA       TIPONOM    ON TIPONOM.ID_TIPO_NOMINA = CPTOS.ID_TIPO_NOMINA
               LEFT JOIN RH_NOMN_EMPLEADOS               EM1        ON EM1.ID_EMPLEADO = EMPLZ.ID_EMPLEADO_FK
               LEFT JOIN RH_NOMN_PLAZAS                  PLZ        ON PLZ.ID_PLAZA = EM1.ID_PLAZA
               LEFT JOIN RH_NOMN_DATOS_PERSONALES        DAT1       ON DAT1.ID_DATOS_PERSONALES = EM1.ID_DATOS_PERSONALES
               LEFT JOIN RH_NOMN_ADSCRIPCIONES           ADSC       ON ADSC.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
               LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM     ON UNIADM.ID_UNIADM = ADSC.ID_UNIADM_FK
               WHERE CABNOM.ID_QUINCENA_FK IN(SELECT ID_QUINCENA 
                                              FROM RH_NOMN_CAT_QUINCENAS Q 
                                              WHERE Q.NUMERO_QUINCENA = DECODE((NUMQUINCENA-1),0,24,(NUMQUINCENA-1)) 
                                              AND   Q.ID_EJERCICIO_FK = DECODE((NUMQUINCENA-1),0,(EJERCICIO-1),EJERCICIO ) )
               AND CABNOM.ID_ESTATUS_NOMINA_FK = 3
               AND CABNOM.ID_TIPO_NOMINA_FK = idTipoNomina

               UNION

              SELECT EM1.NUMERO_EMPLEADO ID_EMPLEADO
                      ,DAT1.NOMBRE||' '||DAT1.A_PATERNO||' '||DAT1.A_MATERNO NOMBRE 
                      ,UNIADM.CLAVE_UNIADM
                      ,TIPONOM.TIPO_NOMINA
                      ,TIPOCPTO.TIPO_CONCEPTO
                      ,CPTOS.CLAVE_CONCEPTO
                      ,CPTOS.NOMBRE_CONCEPTO DESCRIPCION
                      ,NVL(CPT.IMPORTE_CONCEPTO,0)IMPORTE_CONCEPTO
                      ,2 QUINCENA
               FROM RH_NOMN_CABECERAS_NOMINAS            CABNOM
               LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01  EMPLZ      ON EMPLZ.ID_NOMINA01_FK = CABNOM.ID_NOMINA
               LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA_01  CPT        ON CPT.ID_NOMEMPPLA01_FK =EMPLZ.ID_NOMEMPPLA01
               LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTOS      ON CPTOS.ID_CONCEPTO = CPT.ID_CONCEPTO01_FK
               LEFT JOIN RH_NOMN_CAT_TIPOS_CONCEPTO      TIPOCPTO   ON TIPOCPTO.ID_TIPO_CONCEPTO = CPTOS.ID_TIPO_CONCEPTO_FK
               LEFT  JOIN RH_NOMN_CAT_TIPOS_NOMINA       TIPONOM    ON TIPONOM.ID_TIPO_NOMINA = CPTOS.ID_TIPO_NOMINA
               LEFT JOIN RH_NOMN_EMPLEADOS               EM1        ON EM1.ID_EMPLEADO = EMPLZ.ID_EMPLEADO_FK
               LEFT JOIN RH_NOMN_PLAZAS                  PLZ        ON PLZ.ID_PLAZA = EM1.ID_PLAZA
               LEFT JOIN RH_NOMN_DATOS_PERSONALES        DAT1       ON DAT1.ID_DATOS_PERSONALES = EM1.ID_DATOS_PERSONALES
               LEFT JOIN RH_NOMN_ADSCRIPCIONES           ADSC       ON ADSC.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
               LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM     ON UNIADM.ID_UNIADM = ADSC.ID_UNIADM_FK
               WHERE CABNOM.ID_QUINCENA_FK = idQuincena
               AND CABNOM.ID_ESTATUS_NOMINA_FK = 2
               AND CABNOM.ID_TIPO_NOMINA_FK = idTipoNomina



)PIVOT 
(
    SUM(IMPORTE_CONCEPTO) FOR QUINCENA IN (1 Q1,2 Q2) 
)
ORDER BY ID_EMPLEADO,DECODE (TIPO_CONCEPTO,'Percepci�n',1,'Deducci�n',2,'Provisi�n',3)ASC;

  END IF;




EXCEPTION
WHEN OTHERS THEN  
 err_code := SQLCODE;
 err_msg := SUBSTR(SQLERRM, 1, 200);
 DBMS_OUTPUT.PUT_LINE(SQLERRM);
  SPDERRORES('SP_COMP_QUINCENA_ANT_ACT',err_code,err_msg,''); 

END SP_COMP_QUINCENA_ANT_ACT;

/
--------------------------------------------------------
--  DDL for Procedure SP_COMPARAR_ARCHIVO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_COMPARAR_ARCHIVO" (nomTabla IN VARCHAR2,campos IN STRING_ARRAY, registros IN STRING_ARRAY,idQuincena IN NUMBER,tipoComparacion IN NUMBER,selectCampos IN VARCHAR2,decodeConceptos IN VARCHAR2,conceptos IN VARCHAR2, diferencias OUT SYS_REFCURSOR) IS
tablatmp          VARCHAR2(300); --Para generar el nombre de la tabla temporar para crear
tmpCampos         VARCHAR2(3000):=''; -- Se concatenan todos los valores de las columnas a crear
tmpCamposSelect   VARCHAR2(3000):=''; -- Se concatenan todos los valores de las columnas a crear
tmpRegistro       VARCHAR2(3000):= ''; --Para generar el registro a insertar
tmpVal             TYPE_STRING; --Para leer la variable TYPE_STRING

ERR_CODE          INTEGER;
ERR_MSG           VARCHAR2(250);
existTable        NUMBER;
queryComparativo VARCHAR2(30000);

NUMROW NUMBER;


PROCEDURE COMPARAR_PRESTACIONES(DIFERENCIAS OUT SYS_REFCURSOR) IS

BEGIN

          QUERYCOMPARATIVO := ' 
                               SELECT '||SELECTCAMPOS||'
                               FROM
                               (
     
     
     
                                   SELECT 
                                        TO_NUMBER(ID_EMPLEADO)     ID_EMPLEADO0000
                                       ,TRIM(NOMBRE)                     NOMBRE0001
                                       ,TRIM(ADSCRIPCION)                ADSCRIPCION0002
                                       ,TRIM(UNIDAD)                     UNIDAD0003
                                       ,TRIM(TIPO)                       TIPO0004
                                       ,TRIM(NIVEL)                      NIVEL0005
                                       ,TO_NUMBER(IMPORTE_ARCHIVO) IMPORTE_ARCHIVO0006
                                       ,TO_NUMBER(IMPORTE_SISTEMA) IMPORTE_SISTEMA0007
                                       ,CONCEPTO                   CONCEPTO
                                   FROM '||tablatmp||'

                                   UNION 

                                   SELECT 
                                             ID_EMPLEADO           ID_EMPLEADO0000
                                            ,TRIM(NOMBRE)                NOMBRE0001     
                                            ,TRIM(ADSCRIPCION)           ADSCRIPCION0002
                                            ,TRIM(UNIDAD_ADMINISTRATIVA) UNIDAD0003
                                            ,TRIM(TIPO_DE_NOMINA)        TIPO0004
                                            ,NIVEL                 NIVEL0005
                                            ,0                     IMPORTE_ARCHIVO0006
                                            ,IMPORTE_SISTEMA       IMPORTE_SISTEMA0007
                                            ,DECODE(CLAVE_DE_CONCEPTO,'||DECODECONCEPTOS||'CLAVE_DE_CONCEPTO)     CONCEPTO
                                      FROM
                                      (
                                       SELECT 
                                               TO_NUMBER(EM.NUMERO_EMPLEADO)ID_EMPLEADO
                                              ,TRANSLATE(UPPER(TO_CHAR(DAT.A_PATERNO||'' ''||A_MATERNO||'' ''||DAT.NOMBRE)),''����������'', ''aeiouAEIOU'') NOMBRE
                                              ,ADSC.DESCRIPCION ADSCRIPCION
                                              ,TO_CHAR(UNIADM.CLAVE_UNIADM) UNIDAD_ADMINISTRATIVA
                                              ,TRANSLATE(UPPER(TO_CHAR(TIPONOM.TIPO_NOMINA)),''����������'', ''aeiouAEIOU'') TIPO_DE_NOMINA
                                              ,GRUPO.CLAVE_JERARQUICA||GRADO.GRADO||NIVEL.NIVEL NIVEL
                                              ,CPTEMP01.IMPORTE_CONCEPTO IMPORTE_SISTEMA
                                              ,DECODE(CPTO.CLAVE_CONCEPTO,''A1'',''QUIN'',''A2'',''QUIN'',''A3'',''QUIN'',''A4'',''QUIN'',''A5'',''QUIN''
                                                                         ,''A1E'',''QUIN'',''A2E'',''QUIN'',''A3E'',''QUIN'',''A4E'',''QUIN'',''A5E'',''QUIN''
                                                                         ,CPTO.CLAVE_CONCEPTO) CLAVE_DE_CONCEPTO
                                       FROM   RH_NOMN_CABECERAS_NOMINAS NOMN
                                       LEFT   JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 EMPLZ01  ON  EMPLZ01.ID_NOMINA01_FK      = NOMN.ID_NOMINA
                                       LEFT   JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA_01 CPTEMP01 ON  CPTEMP01.ID_NOMEMPPLA01_FK  = EMPLZ01.ID_NOMEMPPLA01
                                       LEFT   JOIN RH_NOMN_EMPLEADOS              EM       ON  EM.ID_EMPLEADO              = EMPLZ01.ID_EMPLEADO_FK
                                       LEFT   JOIN RH_NOMN_DATOS_PERSONALES       DAT      ON  DAT.ID_DATOS_PERSONALES     = EM.ID_DATOS_PERSONALES
                                       LEFT   JOIN RH_NOMN_PLAZAS                 PLZ      ON  PLZ.ID_PLAZA                = EM.ID_PLAZA
                                       LEFT   JOIN RH_NOMN_ADSCRIPCIONES          ADSC     ON  ADSC.ID_ADSCRIPCION         = PLZ.ID_ADSCRIPCION_FK
                                       LEFT   JOIN RH_NOMN_CAT_UNIDADES_ADMINS    UNIADM   ON  UNIADM.ID_UNIADM            = ADSC.ID_UNIADM_FK
                                       LEFT   JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNOM   ON  TIPNOM.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
                                       LEFT   JOIN RH_NOMN_CAT_CONCEPTOS          CPTO     ON  CPTO.ID_CONCEPTO            = CPTEMP01.ID_CONCEPTO01_FK
                                       LEFT   JOIN RH_NOMN_CAT_TIPOS_CONCEPTO     TIPOCPTO ON  TIPOCPTO.ID_TIPO_CONCEPTO   = CPTO.ID_TIPO_CONCEPTO_FK
                                       LEFT   JOIN RH_NOMN_CAT_TIPOS_NOMINA       TIPONOM  ON  TIPONOM.ID_TIPO_NOMINA      = CPTO.ID_TIPO_NOMINA
                                       LEFT   JOIN RH_NOMN_CAT_QUINCENAS          QUIN     ON  QUIN.ID_QUINCENA            = NOMN.ID_QUINCENA_FK
                                       LEFT   JOIN RH_NOMN_TABULADORES            TAB      ON  TAB.ID_TABULADOR            = PLZ.ID_TABULADOR_FK
                                       LEFT   JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRUPO    ON  GRUPO.ID_GRUPO_JERARQUICO   = TAB.ID_GRUPO_JERARQUICO_FK
                                       LEFT   JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRADO    ON  GRADO.ID_GRADO_JERARQUICO   = TAB.ID_GRADO_JERARQUICO_FK
                                       LEFT   JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NIVEL    ON  NIVEL.ID_NIVEL_TABULADOR    = TAB.ID_NIVEL_TABULADOR_FK
                                       WHERE  QUIN.ID_QUINCENA =  '||idQuincena||'
                                      )
                                 )PIVOT(
                                    SUM(IMPORTE_ARCHIVO0006)ARC, SUM(IMPORTE_SISTEMA0007) SIS  
                                    FOR(CONCEPTO) IN ('||conceptos||' )
                                 )     
                            '
                            ;
        OPEN DIFERENCIAS FOR QUERYCOMPARATIVO;

END;

PROCEDURE COMPARAR_MOVIMIENTOS(IDQUINCENA IN NUMBER, DIFERENCIAS OUT SYS_REFCURSOR)IS  

AID               NUMBER;
ARFC              VARCHAR2(13);
ANOMBRE           VARCHAR2(150);
AUNIDAD           VARCHAR2(10);
ADESCRIPCION      VARCHAR2(200);
ATIPONOMBRAMIENTO VARCHAR2(50);
AMOVIMIENTO       VARCHAR2(30);
ANIVEL            VARCHAR2(10);
AFECHA_MOVIMIENTO VARCHAR2(15);
DATOS_ARCHIVO     SYS_REFCURSOR;

BRFC              VARCHAR2(13);
BNOMBRE           VARCHAR2(150);
BUNIDAD           VARCHAR2(10);
BDESCRIPCION      VARCHAR2(200);
BTIPONOMBRAMIENTO VARCHAR2(50);
BMOVIMIENTO       VARCHAR2(30);
BNIVEL            VARCHAR2(10);
BFECHA_MOVIMIENTO VARCHAR2(15);
DATOS_BASE        SYS_REFCURSOR;

EXISTE_INFORMACION NUMBER;
EXISTE_RFC         NUMBER; 

V_DETALLE VARCHAR2(3000);

BEGIN 


        OPEN  DATOS_ARCHIVO FOR
            'SELECT  ID,
                    TRANSLATE(RFC0000,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(NOMBRE0001,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(UNIDAD_ADMINISTRATIVA0002,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(DESCRIPCION0003,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(TIPO_DE_NOMBRAMIENTO0004,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(MOVIMIENTO0005,''����������'', ''aeiouAEIOU''),
                    TRANSLATE(NIVEL_TABULAR0006,''����������'', ''aeiouAEIOU''),
                    FECHA_DE_MOVIMIENTO0007
            FROM '||TABLATMP;

        LOOP
            FETCH DATOS_ARCHIVO INTO AID, ARFC, ANOMBRE, AUNIDAD, ADESCRIPCION, ATIPONOMBRAMIENTO, AMOVIMIENTO, ANIVEL, AFECHA_MOVIMIENTO;
                EXIT WHEN DATOS_ARCHIVO%NOTFOUND;
                V_DETALLE := '';
                
                    SELECT   COUNT (DAT.RFC) INTO EXISTE_INFORMACION
                    FROM RH_NOMN_EMPLEADOS EM
                    LEFT  JOIN RH_NOMN_DATOS_PERSONALES        DAT     ON DAT.ID_DATOS_PERSONALES      = EM.ID_DATOS_PERSONALES
                    LEFT  JOIN RH_NOMN_PLAZAS                  PLZ     ON PLZ.ID_PLAZA                 = EM.ID_PLAZA
                    LEFT  JOIN RH_NOMN_ADSCRIPCIONES           ADSC    ON ADSC.ID_ADSCRIPCION          = PLZ.ID_ADSCRIPCION_FK
                    LEFT  JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM  ON UNIADM.ID_UNIADM             = ADSC.ID_UNIADM_FK
                    LEFT  JOIN RH_NOMN_TABULADORES             TAB     ON TAB.ID_TABULADOR             = PLZ.ID_TABULADOR_FK 
                    LEFT  JOIN RH_NOMN_CAT_NIVELES_TABULADOR   NIVTAB  ON NIVTAB.ID_NIVEL_TABULADOR    = TAB.ID_NIVEL_TABULADOR_FK
                    LEFT  JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS  GRA     ON GRA.ID_GRADO_JERARQUICO      = TAB.ID_GRADO_JERARQUICO_FK
                    LEFT  JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS  GRU     ON TAB.ID_GRUPO_JERARQUICO_FK   = GRU.ID_GRUPO_JERARQUICO
                    LEFT  JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS   CAMP    ON CAMP.ID_REGISTRO_ENTIDAD     = EM.ID_EMPLEADO
                    LEFT  JOIN RH_NOMN_BIT_HISTORICOS_MOVS     MOV     ON MOV.ID_HISTORICO_MOVIMIENTO  = CAMP.ID_HISTORICO_MOVIMIENTO_FK
                    LEFT  JOIN RH_NOMN_CAT_ACCIONES            ACC     ON ACC.ID_ACCION                = MOV.ID_ACCION_FK
                    LEFT  JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO  NOMB    ON NOMB.ID_TIPO_NOMBRAMIENTO    = PLZ.ID_TIPO_NOMBRAM_FK
                    WHERE MOV.FECHA_REGISTRO BETWEEN (SELECT FECHA_INICIO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS Q1 WHERE Q1.ID_QUINCENA = IDQUINCENA) 
                                             AND     (SELECT FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS Q1 WHERE Q1.ID_QUINCENA = IDQUINCENA)
                    AND UPPER(ACC.DESCRIPCION) IN ('ALTA EMPLEADO','BAJA EMPLEADO','CAMBIAR PUESTO EMPLEADO')
                    AND DAT.RFC = ARFC
                    AND TRANSLATE(UPPER(TO_CHAR(ACC.NOMBRE_CORTO)),'����������', 'aeiouAEIOU') = AMOVIMIENTO
                    AND UNIADM.CLAVE_UNIADM <> 1;
                    --AND TO_DATE(MOV.FECHA,'DD/MM/YY') =  TO_DATE(AFECHA_MOVIMIENTO,'DD/MM/YY');
                                
        
                    IF EXISTE_INFORMACION >= 1 THEN
        
                                SELECT   DISTINCT
                                       DAT.RFC                                                                                               BRFC
                                      ,TRANSLATE(UPPER(TO_CHAR(DAT.A_PATERNO||' '||A_MATERNO||' '||DAT.NOMBRE)),'����������', 'aeiouAEIOU')  BNOMBRE
                                      ,TO_CHAR(UNIADM.CLAVE_UNIADM)                                                                          BUNIDAD
                                      ,TRANSLATE(UPPER(TO_CHAR(UNIADM.DESCRIPCION)),'����������', 'aeiouAEIOU')                              BDESCRIPCION
                                      ,TRANSLATE(UPPER(TO_CHAR(NOMB.DESCRIPCION)),'����������', 'aeiouAEIOU')                                BTIPONOMBRAMIENTO
                                      ,TRANSLATE(UPPER(TO_CHAR(ACC.NOMBRE_CORTO)),'����������', 'aeiouAEIOU')                                BMOVIMIENTO
                                      ,UPPER(TO_CHAR(GRU.CLAVE_JERARQUICA||GRA.GRADO||NIVTAB.NIVEL))                                         BNIVEL
                                      ,TO_CHAR(MOV.FECHA,'DD/MM/YY')                                                                         BFECHA_MOVIMIENTO                  
                                      INTO BRFC, BNOMBRE, BUNIDAD, BDESCRIPCION, BTIPONOMBRAMIENTO, BMOVIMIENTO, BNIVEL, BFECHA_MOVIMIENTO
                                FROM RH_NOMN_EMPLEADOS EM
                                LEFT  JOIN RH_NOMN_DATOS_PERSONALES        DAT     ON DAT.ID_DATOS_PERSONALES      = EM.ID_DATOS_PERSONALES
                                LEFT  JOIN RH_NOMN_PLAZAS                  PLZ     ON PLZ.ID_PLAZA                 = EM.ID_PLAZA
                                LEFT  JOIN RH_NOMN_ADSCRIPCIONES           ADSC    ON ADSC.ID_ADSCRIPCION          = PLZ.ID_ADSCRIPCION_FK
                                LEFT  JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM  ON UNIADM.ID_UNIADM             = ADSC.ID_UNIADM_FK
                                LEFT  JOIN RH_NOMN_TABULADORES             TAB     ON TAB.ID_TABULADOR             = PLZ.ID_TABULADOR_FK 
                                LEFT  JOIN RH_NOMN_CAT_NIVELES_TABULADOR   NIVTAB  ON NIVTAB.ID_NIVEL_TABULADOR    = TAB.ID_NIVEL_TABULADOR_FK
                                LEFT  JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS  GRA     ON GRA.ID_GRADO_JERARQUICO      = TAB.ID_GRADO_JERARQUICO_FK
                                LEFT  JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS  GRU     ON TAB.ID_GRUPO_JERARQUICO_FK   = GRU.ID_GRUPO_JERARQUICO
                                LEFT  JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS   CAMP    ON CAMP.ID_REGISTRO_ENTIDAD     = EM.ID_EMPLEADO
                                LEFT  JOIN RH_NOMN_BIT_HISTORICOS_MOVS     MOV     ON MOV.ID_HISTORICO_MOVIMIENTO  = CAMP.ID_HISTORICO_MOVIMIENTO_FK
                                LEFT  JOIN RH_NOMN_CAT_ACCIONES            ACC     ON ACC.ID_ACCION                = MOV.ID_ACCION_FK
                                LEFT  JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO  NOMB    ON NOMB.ID_TIPO_NOMBRAMIENTO    = PLZ.ID_TIPO_NOMBRAM_FK
                                WHERE MOV.FECHA_REGISTRO BETWEEN (SELECT FECHA_INICIO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS Q1 WHERE Q1.ID_QUINCENA = idQuincena) 
                                                         AND     (SELECT FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS Q1 WHERE Q1.ID_QUINCENA = idQuincena)
                                AND UPPER(ACC.DESCRIPCION) IN ('ALTA EMPLEADO','BAJA EMPLEADO','CAMBIAR PUESTO EMPLEADO')
                                AND DAT.RFC = ARFC
                                AND TRANSLATE(UPPER(TO_CHAR(ACC.NOMBRE_CORTO)),'����������', 'aeiouAEIOU') = AMOVIMIENTO
                                AND UNIADM.CLAVE_UNIADM <> 1
                                AND ROWNUM < 2;
                                --ORDER BY MOV.FECHA DESC;
                                --AND TO_DATE(MOV.FECHA,'DD/MM/YY') =  TO_DATE(AFECHA_MOVIMIENTO,'DD/MM/YY');
                                
                                
                        IF  TRIM(UPPER(BRFC))              = TRIM(UPPER(ARFC))               AND  
                            TRIM(UPPER(BNOMBRE))           = TRIM(UPPER(ANOMBRE))            AND 
                            TRIM(UPPER(BUNIDAD))           = TRIM(UPPER(AUNIDAD))            AND  
                            TRIM(UPPER(BDESCRIPCION))      = TRIM(UPPER(ADESCRIPCION))       AND 
                            TRIM(UPPER(BTIPONOMBRAMIENTO)) = TRIM(UPPER(ATIPONOMBRAMIENTO))  AND  
                            TRIM(UPPER(BMOVIMIENTO))       = TRIM(UPPER(AMOVIMIENTO))        AND 
                            TRIM(UPPER(BNIVEL))            = TRIM(UPPER(ANIVEL))             AND
                            TRIM(UPPER(BFECHA_MOVIMIENTO)) = TRIM(UPPER(AFECHA_MOVIMIENTO))  THEN
                            V_DETALLE := 'Registro exitoso,';
                            
                        ELSE    
                             IF TRIM(UPPER(BNOMBRE)) != TRIM(UPPER(ANOMBRE)) THEN
                                V_DETALLE := V_DETALLE||'El nombre es diferente, ';
                             END IF;
                             IF TRIM(UPPER(BUNIDAD)) != TRIM(UPPER(AUNIDAD)) THEN
                                V_DETALLE := V_DETALLE||'La clave de unidad administrativa es diferente, ';
                             END IF;
                             IF TRIM(UPPER(BDESCRIPCION)) != TRIM(UPPER(ADESCRIPCION)) THEN
                                V_DETALLE := V_DETALLE||'La descripcion de la unidad administrativa es diferente, ';
                             END IF;
                             IF TRIM(UPPER(BTIPONOMBRAMIENTO)) != TRIM(UPPER(ATIPONOMBRAMIENTO)) THEN
                                V_DETALLE := V_DETALLE||'El tipo de nombramiento es diferente, ';
                             END IF;   
                             IF TRIM(UPPER(BMOVIMIENTO)) != TRIM(UPPER(AMOVIMIENTO)) THEN
                                V_DETALLE := V_DETALLE||'El movimiento es diferente, ';
                             END IF;
                             IF TRIM(UPPER(BNIVEL)) != TRIM(UPPER(ANIVEL)) THEN
                                V_DETALLE := V_DETALLE||'El nivel es diferente, ';
                             END IF;
                             IF TRIM(UPPER(BFECHA_MOVIMIENTO)) != TRIM(UPPER(AFECHA_MOVIMIENTO)) THEN
                                V_DETALLE := V_DETALLE||'La fecha de movimiento es diferente, ';
                             END IF;
                         
                        END IF;
                   ELSE
                        SELECT COUNT (RFC) INTO EXISTE_RFC
                        FROM RH_NOMN_DATOS_PERSONALES
                        WHERE RFC = ARFC;
                    
                        IF EXISTE_RFC >= 1 THEN
                            V_DETALLE := V_DETALLE||'El RFC no tiene movimientos en esta quincena, ';
                        ELSE
                            SELECT LENGTH(ARFC) INTO EXISTE_RFC 
                            FROM DUAL;
                            IF EXISTE_RFC = 13 THEN  
                                V_DETALLE := V_DETALLE||'El RFC no existe en base de datos, ';
                            ELSE
                                V_DETALLE := V_DETALLE||'Validar longitud del RFC, ';
                            END IF;
                        END IF;
                   END IF;
                   
                   V_DETALLE := SUBSTR(V_DETALLE, 0, LENGTH(V_DETALLE)-2);
                
                EXECUTE IMMEDIATE 'UPDATE '||TABLATMP||' SET DETALLE = '''||V_DETALLE||'''
                                   WHERE ID = '||AID;
                
                                   
        END LOOP;
        
        OPEN DIFERENCIAS FOR
            'SELECT  
                    RFC0000,
                    NOMBRE0001,
                    UNIDAD_ADMINISTRATIVA0002,
                    DESCRIPCION0003,
                    TIPO_DE_NOMBRAMIENTO0004,
                    MOVIMIENTO0005,
                    NIVEL_TABULAR0006,
                    FECHA_DE_MOVIMIENTO0007,
                    DETALLE DETALLE0008
            FROM '||TABLATMP;

END;


  BEGIN

  
    IF nomTabla is not null then --Validamos que los datos no vengan en null
      tablatmp := 'TMP_'||nomTabla||'_'||TO_CHAR(SYSDATE,'dd_mm_yy_hh_mm_ss');

      
      --Verificamos si existe la tabla temporal la borramos
      SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp; 
      

      IF existTable > 0 THEN 
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;        

      --Fin de borrado de tabla si existe

      tmpCampos:='';
      tmpCamposSelect :='';
      FOR idx IN campos.first()..campos.last() 
          LOOP
            TMPVAL  := CAMPOS(IDX);
            tmpCampos:=tmpCampos||tmpVal.REGISTRO||' VARCHAR2(200) ,';
            tmpCamposSelect := tmpCamposSelect||tmpVal.REGISTRO||' ,';
           END LOOP;

      SELECT SUBSTR(tmpCampos,1,(SELECT LENGTH(tmpCampos)-1 FROM DUAL )) INTO tmpCampos FROM DUAL;
      SELECT SUBSTR(tmpCamposSelect,1,(SELECT LENGTH(tmpCamposSelect)-1 FROM DUAL )) INTO tmpCamposSelect FROM DUAL;

      
      EXECUTE IMMEDIATE 'CREATE TABLE '||TABLATMP||' ( ID NUMBER,'||TMPCAMPOS|| ', DETALLE VARCHAR2(3000))';  
      

      -- INSERTA VALORES;
      NUMROW :=0;
      FOR idxj IN registros.first()..registros.last() 
        LOOP
          tmpVal  := registros(idxj);
          
          SELECT TRANSLATE(TMPVAL.REGISTRO,'����������', 'aeiouAEIOU') INTO TMPREGISTRO FROM DUAL;
          EXECUTE IMMEDIATE 'INSERT INTO '||TABLATMP||' VALUES ( '||NUMROW||','||TMPREGISTRO|| ',null)'; 
          
      
        NUMROW := NUMROW+1;
        END LOOP;
        

      --TERMINO DE INSERTAR;
      IF TIPOCOMPARACION = 1 THEN   -- MOVIMIENTOS QUINCENAS
        --------------------   Validamos prestaciones --------------------------------------------
          COMPARAR_PRESTACIONES(DIFERENCIAS);

      ELSIF TIPOCOMPARACION = 2 THEN --MOVIMIENTOS EMPLEADOS
      
        --------------------   Validamos movimientos --------------------------------------------
        COMPARAR_MOVIMIENTOS(IDQUINCENA,DIFERENCIAS);
      END IF;      

      IF tipoComparacion = 3 THEN   ---ARCHIVO DISPERCION

          queryComparativo := 'SELECT      TO_NUMBER(DB.ID_EMPLEADO) ID_EMPLEADO_BASE
                                           ,DB.NUMERO_DE_CUENTA NUM_CUENTA_BASE
                                           ,TO_CHAR(DB.IMPORTE,''9,999,999,999,999.99'') IMPORTE_BASE
                                           ,TO_NUMBER(AR.ID_EMPLEADO) ID_EMPLEADO_ARCHIVO
                                           ,AR.NUMERO_DE_CUENTA NUM_CUENTA_ARCHIVO
                                           ,TO_CHAR(AR.IMPORTE,''9,999,999,999,999.99'') IMPORTE_ARCHIVO
                                           ,CASE WHEN DB.ID_EMPLEADO IS NULL THEN 
                                                    ''NO EXISTE EN BASE''
                                                 WHEN AR.ID_EMPLEADO IS NULL THEN
                                                    ''NO EXISTE EN ARCHIVO''
                                                 WHEN DB.NUMERO_DE_CUENTA = AR.NUMERO_DE_CUENTA THEN
                                                    ''CUENTAS IGUALES''
                                                 WHEN DB.NUMERO_DE_CUENTA IS NULL THEN
                                                    ''NO EXISTE CUENTA EN BASE''
                                                 WHEN DB.NUMERO_DE_CUENTA <> AR.NUMERO_DE_CUENTA THEN
                                                    ''CUENTAS DIFERENTES''
                                            END ESTATUS  
                                    FROM 
                                    (
                                        (
                                          SELECT  LPAD(EM.NUMERO_EMPLEADO,10,0) ID_EMPLEADO
                                                 ,LPAD(NVL(CASE WHEN UPPER(BANCO.DESCRIPCION_BANCO) = ''BANORTE'' THEN 
                                                                 DATBAN.NUMERO_CUENTA 
                                                                ELSE 
                                                                 DATBAN.CLABE
                                                           END,0),18,0) NUMERO_DE_CUENTA
                                                 ,DECODE(EMPLAZ.IMPORTE_TOTAL,NULL,EMPLAZ01.IMPORTE_TOTAL,EMPLAZ01.IMPORTE_TOTAL,NULL,EMPLAZ.IMPORTE_TOTAL) IMPORTE
                                          FROM RH_NOMN_CABECERAS_NOMINAS NOMN
                                          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA    EMPLAZ   ON EMPLAZ.ID_NOMINA_FK = NOMN.ID_NOMINA
                                          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 EMPLAZ01 ON EMPLAZ01.ID_NOMINA01_FK = NOMN.ID_NOMINA
                                          LEFT JOIN RH_NOMN_CAT_QUINCENAS          CATQUIN  ON CATQUIN.ID_QUINCENA = NOMN.ID_QUINCENA_FK
                                          LEFT JOIN RH_NOMN_EMPLEADOS           EM      ON EM.ID_EMPLEADO = EMPLAZ.ID_EMPLEADO_FK OR EM.ID_EMPLEADO = EMPLAZ01.ID_EMPLEADO_FK
                                          LEFT JOIN RH_NOMN_DATOS_BANCARIOS     DATBAN  ON DATBAN.ID_DATOS_BANCARIOS = EM.ID_DATOS_BANCARIOS
                                          LEFT JOIN RH_NOMN_CAT_BANCOS          BANCO   ON BANCO.ID_BANCO = DATBAN.ID_BANCO
                                          WHERE CATQUIN.ID_QUINCENA = '||idQuincena||'
                                          AND EM.ID_TIPO_PAGO = 2
                                        ) DB
                                        FULL OUTER JOIN
                                        (SELECT 
                                           ID_EMPLEADO
                                          ,NUMERO_DE_CUENTA
                                          ,TO_NUMBER(SUBSTR(IMPORTE,0,13)||''.''||SUBSTR(IMPORTE,14,15)) IMPORTE
                                          FROM '||tablatmp||' )AR ON DB.ID_EMPLEADO = AR.ID_EMPLEADO
                                    )ORDER BY DB.ID_EMPLEADO';

                                    
          OPEN diferencias FOR queryComparativo;
      END IF;

      EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;

      COMMIT;  

    END IF;

  EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    ERR_MSG := SUBSTR(SQLERRM, 1, 200);
    
    ROLLBACK;
    SELECT COUNT(1) INTO EXISTTABLE
      FROM ALL_TABLES 
      WHERE TABLE_NAME = TABLATMP 
      ;

      IF EXISTTABLE > 0 THEN 
      
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;        
    --DBMS_OUTPUT.PUT_LINE(1/N);    
    SPDERRORES('SP_COMPARAR_ARCHIVO',err_code,err_msg,''); 

  END SP_COMPARAR_ARCHIVO;

/
--------------------------------------------------------
--  DDL for Procedure SP_CONFIG_LICENCIAS_MEDICAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CONFIG_LICENCIAS_MEDICAS" (NUMEMPLEADO IN NUMBER, CABECERO OUT SYS_REFCURSOR ) IS

FECHAS_ANT           SYS_REFCURSOR;
FECHAS_ANI           SYS_REFCURSOR;

FECHA_INICIO_ANT     DATE := NULL;
FECHA_INICIO_ANI     DATE := NULL;
TMP_FECHA_INICIO_ANT DATE;
TMP_FECHA_INICIO_ANI DATE;
TMP_FECHA_BAJA_ANT   DATE;
TMP_FECHA_BAJA_ANI   DATE;
FECHA_BAJA_ANT       DATE;
FECHA_BAJA_ANI       DATE;

MESES NUMBER;
ANIOS NUMBER;
DIAS_100 NUMBER;
DIAS_50 NUMBER;

BEGIN

---Obtenemos fechas del IFT y GOB_FEDERAL
OPEN FECHAS_ANT FOR
        SELECT FECHA_INGRESO, FECHA_BAJA
        FROM 
        (
            SELECT FECHA_DE_INGRESO FECHA_INGRESO, FECHA_BAJA
            FROM RH_NOMN_EMPLEADOS
            WHERE NUMERO_EMPLEADO = NUMEMPLEADO
            UNION
            SELECT FECHA_INGRESO, FECHA_EGRESO FECHA_BAJA
            FROM RH_NOMN_MOV_GOB_FEDERAL
            WHERE ID_EMPLEADO IN (SELECT ID_EMPLEADO
                                  FROM RH_NOMN_EMPLEADOS
                                  WHERE NUMERO_EMPLEADO = NUMEMPLEADO)
        )
        ORDER BY FECHA_INGRESO
;

------Validamos cual fecha se toma para contar su antig�edad
LOOP
    FETCH FECHAS_ANT INTO TMP_FECHA_INICIO_ANT , TMP_FECHA_BAJA_ANT;
        EXIT WHEN FECHAS_ANT%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('TMP FECHA INICIO '||TMP_FECHA_INICIO_ANT||' TMP FECHA_FIN '||TMP_FECHA_BAJA_ANT);
        IF FECHA_INICIO_ANT IS NULL THEN
            FECHA_INICIO_ANT := TMP_FECHA_INICIO_ANT;
            FECHA_BAJA_ANT := TMP_FECHA_BAJA_ANT;
        ELSE
            SELECT ABS(MONTHS_BETWEEN(FECHA_BAJA_ANT,TMP_FECHA_BAJA_ANT))
            INTO MESES
            FROM DUAL;
            DBMS_OUTPUT.PUT_LINE('MESES ENTRE FECHAS '||MESES);
            IF MESES >= 6 THEN 
                FECHA_INICIO_ANT := TMP_FECHA_INICIO_ANT;
            END IF;
        END IF;
        DBMS_OUTPUT.PUT_LINE('FECHA INICIO '||FECHA_INICIO_ANT);
END LOOP;

CLOSE FECHAS_ANT;

-------------------------------------- A�os de antig�edad
SELECT FLOOR(ABS(MONTHS_BETWEEN(FECHA_INICIO_ANT,SYSDATE))/12)
INTO ANIOS
FROM DUAL;

DBMS_OUTPUT.PUT_LINE('A�OS '||ANIOS);

SELECT DIAS_100, DIAS_50
INTO DIAS_100,DIAS_50
FROM RH_NOMN_GOCE_SUELDO_ANTIGUEDAD
WHERE ANIOS BETWEEN ANIO_DESDE AND NVL(ANIO_HASTA,ANIOS)
;

---Obtenemos fechas del IFT para el aniversario
OPEN FECHAS_ANI FOR
            SELECT FECHA_DE_INGRESO FECHA_INGRESO, FECHA_BAJA
            FROM RH_NOMN_EMPLEADOS
            WHERE NUMERO_EMPLEADO = NUMEMPLEADO
            ORDER BY FECHA_DE_INGRESO
;

------Validamos fecha de aniversario
LOOP
    FETCH FECHAS_ANI INTO TMP_FECHA_INICIO_ANI, TMP_FECHA_BAJA_ANI;
        EXIT WHEN FECHAS_ANI%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('TMP FECHA INICIO '||TMP_FECHA_INICIO_ANI||' TMP FECHA_FIN '||TMP_FECHA_BAJA_ANI);
        IF FECHA_INICIO_ANI IS NULL THEN
            FECHA_INICIO_ANI := TMP_FECHA_INICIO_ANI;
            FECHA_BAJA_ANI := TMP_FECHA_BAJA_ANI;
        ELSE
            SELECT ABS(MONTHS_BETWEEN(FECHA_BAJA_ANI,TMP_FECHA_BAJA_ANI))
            INTO MESES
            FROM DUAL;
            DBMS_OUTPUT.PUT_LINE('MESES ENTRE FECHAS '||MESES);
            IF MESES >= 6 THEN 
                FECHA_INICIO_ANT := TMP_FECHA_INICIO_ANI;
            END IF;
        END IF;
        DBMS_OUTPUT.PUT_LINE('FECHA INICIO ANIVERSARIO '||FECHA_INICIO_ANI);
END LOOP;

CLOSE FECHAS_ANI;







OPEN CABECERO FOR
SELECT  FECHA_INICIO_ANT FECHA_INICIO_ANT, 
        ANIOS ANIOS,
        FECHA_INICIO_ANI FECHA_INICIO_ANI,
        DIAS_100 DIAS_100,
        DIAS_50 DIAS_50
FROM DUAL;




--CLOSE CABECERO;

END SP_CONFIG_LICENCIAS_MEDICAS;

/
--------------------------------------------------------
--  DDL for Procedure SP_CONSULTAR_AGUINALDOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_CONSULTAR_AGUINALDOS" (NUMEMPLEADO IN NUMBER,FQUINCENA IN DATE, AGUINALDOS OUT SYS_REFCURSOR) IS
    IDEJERCICIO NUMBER;
BEGIN

    --- Obtenemos el ejercicio
    SELECT ID_EJERCICIO
    INTO IDEJERCICIO
    FROM RH_NOMN_CAT_EJERCICIOS
    WHERE VALOR = (SELECT TO_NUMBER(TO_CHAR(FQUINCENA,'YYYY')) 
                   FROM DUAL);


    OPEN AGUINALDOS FOR 
    SELECT QUINCENA.ID_QUINCENA,
           TRUNC(AGUINALDO.IMPORTE_QUINCENAL,6) IMPORTE_QUINCENAL,
           DECODE(AGUINALDO.TIPO_AGUINALDO,1,'COMPENSACION',0,'SUELDO') TIPO_AGUINALDO,
           QUINCENA.DESCRIPCION_QUINCENA QUINCENA,
           PUESTO.DESCRIPCION PUESTO,
           AGUINALDO.DIAS_AGUINALDO DIAS_AGUINALDO
    FROM RH_NOMN_AGUINALDOS AGUINALDO
    LEFT JOIN RH_NOMN_EMPLEADOS         EMPLEADO    ON EMPLEADO.ID_EMPLEADO = AGUINALDO.ID_EMPLEADO_FK
    LEFT JOIN RH_NOMN_CAT_QUINCENAS     QUINCENA    ON QUINCENA.ID_QUINCENA = AGUINALDO.ID_QUINCENA_FK
    LEFT JOIN RH_NOMN_PLAZAS            PLAZA       ON PLAZA.ID_PLAZA       = EMPLEADO.ID_PLAZA
    LEFT JOIN RH_NOMN_CAT_PUESTOS       PUESTO      ON PUESTO.ID_PUESTO     = PLAZA.ID_PUESTO_FK
    WHERE QUINCENA.ID_EJERCICIO_FK = IDEJERCICIO
    AND QUINCENA.FECHA_INICIO_QUINCENA <= FQUINCENA
    AND EMPLEADO.NUMERO_EMPLEADO = NUMEMPLEADO 
    ORDER BY QUINCENA.ID_QUINCENA
    ;

/*  LOOP
    FETCH AGUINALDOS INTO IMPORTE,TIPOAGUINALDO,NOMBREQUINCENA,PUESTO;
        EXIT WHEN AGUINALDOS%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('IMPORTE: '||IMPORTE||', TIPO AGUINALDO: '||TIPOAGUINALDO||', NOMBRE QUINCENA: '||NOMBREQUINCENA||', PUESTO: '||PUESTO);
  END LOOP;
  */
EXCEPTION

    WHEN NO_DATA_FOUND THEN    
        DBMS_OUTPUT.PUT_LINE('NO HAY DATOS EN LA CONSULTA');

    WHEN OTHERS THEN 
        DBMS_OUTPUT.PUT_LINE('PROBLEMAS EN LA SINTAXIS');    
END SP_CONSULTAR_AGUINALDOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_DATOS_FINANCIERO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_DATOS_FINANCIERO" 
(
  SP_EJERCICIO IN NUMBER 
, SP_NUMERO_QUINCENA IN NUMBER
, SP_MENSAJE_SALIDA OUT VARCHAR2
) 
AS

  vID_NOMINA                NUMBER;
  vID_TIPO_NOMINA_FK        NUMBER;
  vRETROACTIVIDAD           NUMBER;
  vID_NOMINA_PADRE          NUMBER;
  
  vCLAVE_UNIADM             NUMBER;
  
  --vID_EMPLEADO              NUMBER;
  vNUMERO_EMPLEADO          NUMBER;  
  vRFC                      VARCHAR2(50);
  
  vCLAVE_CONCEPTO           VARCHAR2(50);
  vIMPORTE_CONCEPTO         NUMBER;
  
  vID_EMPLEADO_2            NUMBER;
  vANIO                     NUMBER;
  vQUINCENA                 NUMBER;
  vID_TIPO_NOMINA           NUMBER;
  vID_CONCEPTO              VARCHAR2(20);
  vTIPO_CONC                NUMBER;
  vCONSECUTIVO              NUMBER;
  vIMPORTE                  NUMBER(8,2);
  vFILIACION                VARCHAR2(20);
  vNIVEL                    VARCHAR2(20);
  vUNIDAD_ADM               VARCHAR2(20);
  vID_SUBUNIDAD             VARCHAR2(20);
  vID_TIPO_NOMBRAM          NUMBER;
  vID_BANCO                 NUMBER;
  vCUENTA                   VARCHAR2(20);
  vCENTRO_DISTRIB           VARCHAR2(20);
  vANIO_FIN                 NUMBER;
  vQUINCENA_FIN             NUMBER;
  vID_PLAZA_SUC             NUMBER;
  vCVE_PRES_RH              NUMBER;
  vESTADO                   NUMBER;
  vID_PUESTO                VARCHAR2(20);
  vID_REGION                NUMBER;
  vID_ESCALA                NUMBER;
  vDIF_RET                  NUMBER;
  vREF                      VARCHAR2(20);
  vUNIDADES                 NUMBER(7,2);
  vANTECEDENTE              VARCHAR2(20);
  vFOLIOMANUAL              NUMBER;
  vNUM_PCP                  NUMBER;
  vANIOMANUAL               VARCHAR2(20);
  vCLABE                    VARCHAR2(20);
  vF_ELAB                   VARCHAR2(20);
  vHORA_ELAB                VARCHAR2(20);
  vID_USUARIO               VARCHAR2(20);
    
  vLETRANOMINA              VARCHAR2(1);
  vCONSECUTIVOEMPLEADO      NUMBER := 0;  
  vCONTADORNOMINAS          NUMBER := 0;  
  vMESINCIDENCIA            NUMBER := 0;
  vIDDATOSFINANCIEROS       NUMBER := 0;
  vCANTREGISTRONOMINA       NUMBER := 0;  
  vSUMA_IMPORTE_LIC_SB      NUMBER := 0;
  vSUMA_IMPORTE_LIC_CG      NUMBER := 0;
  vHAY_LICENCIAS            BOOLEAN := FALSE;
  
  vTABLATMP                 VARCHAR2(300);    

  ERR_CODE                  INTEGER;
  ERR_MSG                   VARCHAR2(250);

  CURSOR C1 
  IS
    SELECT   
      CAB.ID_NOMINA           ID_NOMINA,
      CAB.ID_TIPO_NOMINA_FK   ID_TIPO_NOMINA_FK,
      CAB.RETROACTIVIDAD      RETROACTIVIDAD,
      CAB.ID_NOMINA_PADRE     ID_NOMINA_PADRE
    FROM   
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK  
    WHERE
      EJE.VALOR               = SP_EJERCICIO
      AND QUI.NUMERO_QUINCENA = SP_NUMERO_QUINCENA
      AND CAB.ID_TIPO_NOMINA_FK NOT IN (4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 22, 23)
    ORDER BY
      CAB.ID_NOMINA
    ;
    
  CURSOR C2
  IS    
    SELECT 
      DISTINCT UNI.CLAVE_UNIADM     CLAVE_UNIADM
    FROM   
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOM ON NOM.ID_NOMINA_FK = CAB.ID_NOMINA
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK   
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOM.ID_EMPLEADO_FK
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = PLA.ID_ADSCRIPCION_FK
      INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNI ON UNI.ID_UNIADM = ADS.ID_UNIADM_FK
    WHERE
      EJE.VALOR               = SP_EJERCICIO
      AND QUI.NUMERO_QUINCENA = SP_NUMERO_QUINCENA
      AND CAB.ID_NOMINA       = vID_NOMINA
      AND UNI.CLAVE_UNIADM    != 1
    ORDER BY
       UNI.CLAVE_UNIADM
    ; 
    
  CURSOR C3
  IS      
    SELECT       
      EMP.NUMERO_EMPLEADO     NUMERO_EMPLEADO,
      DATPER.RFC              RFC
    FROM   
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOM ON NOM.ID_NOMINA_FK = CAB.ID_NOMINA
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK   
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOM.ID_EMPLEADO_FK
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = PLA.ID_ADSCRIPCION_FK
      INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNI ON UNI.ID_UNIADM = ADS.ID_UNIADM_FK 
      INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
    WHERE
      EJE.VALOR               = SP_EJERCICIO
      AND QUI.NUMERO_QUINCENA = SP_NUMERO_QUINCENA
      AND CAB.ID_NOMINA       = vID_NOMINA
      AND UNI.CLAVE_UNIADM    = vCLAVE_UNIADM  
      -- ES PARA QUINCENA 22 DEL 2017 POR FALLA DE EMPLEADO CON IMPORTE NEGATIVO
      AND EMP.NUMERO_EMPLEADO != 2955
    GROUP BY
      EMP.NUMERO_EMPLEADO,
      DATPER.RFC
    ORDER BY
      DATPER.RFC
    ;  
    
  CURSOR C4
  IS      
    SELECT             
      CONC.CLAVE_CONCEPTO     CLAVE_CONCEPTO,
      CON.IMPORTE_CONCEPTO    IMPORTE_CONCEPTO           
    FROM
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOM  ON NOM.ID_NOMINA_FK = CAB.ID_NOMINA
      INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA  CON ON CON.ID_NOMEMPPLA_FK = NOM.ID_NOMEMPPLA
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOM.ID_EMPLEADO_FK
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
      INNER JOIN RH_NOMN_CAT_CONCEPTOS CONC ON CONC.ID_CONCEPTO = CON.ID_CONCEPTO_FK
      INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
      LEFT JOIN RH_NOMN_DATOS_BANCARIOS DATBAN ON DATBAN.ID_DATOS_BANCARIOS = EMP.ID_DATOS_BANCARIOS
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
      INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK
      INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = PLA.ID_ADSCRIPCION_FK
      INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNIAD ON UNIAD.ID_UNIADM = ADS.ID_UNIADM_FK
      INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNO ON TIPNO.ID_TIPO_NOMBRAMIENTO = PLA.ID_TIPO_NOMBRAM_FK
      LEFT JOIN RH_NOMN_CAT_BANCOS BAN ON BAN.ID_BANCO = DATBAN.ID_BANCO
      LEFT JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED ON ENTFED.ID_ENTIDAD_FEDERATIVA = DATPER.ID_ENTIDAD_NACIMIENTO
    WHERE
      EJE.VALOR               = SP_EJERCICIO
      AND QUI.NUMERO_QUINCENA = SP_NUMERO_QUINCENA
      AND CAB.ID_NOMINA       = vID_NOMINA
      AND UNIAD.CLAVE_UNIADM  = vCLAVE_UNIADM    
      AND DATPER.RFC          = vRFC
      AND CONC.CLAVE_CONCEPTO IN
        (
          'LMSBE',
          'LMSCE',
          'LSSBE',
          'LSSCE',
          'LMSB',
          'LMSC',
          'LSSB',
          'LSSC'
        )        
    ORDER BY
      DATPER.RFC
  ;   
    
  CURSOR C5
  IS
    SELECT
      ID_EMPLEADO,
      ANIO,
      QUINCENA,
      ID_TIPO_NOMINA,
      ID_CONCEPTO,
      TIPO_CONC,
      CONSECUTIVO,
      SUM(IMPORTE),
      FILIACION,
      NIVEL,
      UNIDAD_ADM,
      ID_SUBUNIDAD,
      ID_TIPO_NOMBRAM,
      ID_BANCO,
      CUENTA,
      CENTRO_DISTRIB,
      ANIO_FIN,
      QUINCENA_FIN,
      ID_PLAZA_SUC,
      CVE_PRES_RH,
      ESTADO,
      ID_PUESTO,
      ID_REGION,
      ID_ESCALA,
      DIF_RET,
      REF,
      UNIDADES,
      ANTECEDENTE,
      FOLIOMANUAL,
      NUM_PCP,
      ANIOMANUAL,
      CLABE,
      F_ELAB,
      HORA_ELAB,
      ID_USUARIO
    FROM
    (  
      SELECT      
        EMP.NUMERO_EMPLEADO                                         ID_EMPLEADO,
        EJE.VALOR                                                   ANIO,
        QUI.NUMERO_QUINCENA                                         QUINCENA,
        DECODE(CAB.ID_TIPO_NOMINA_FK, 1, 1, 2, 14, 3, 13, 8)        ID_TIPO_NOMINA,
        CONC.CLAVE_CONCEPTO                                         ID_CONCEPTO,
        DECODE(CONC.ID_TIPO_CONCEPTO_FK, 1, 1, 2, 0)                TIPO_CONC,
        1                                                           CONSECUTIVO,
        CON.IMPORTE_CONCEPTO                                        IMPORTE,
        DATPER.RFC                                                  FILIACION,
        GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL              NIVEL,
        UNIAD.CLAVE_UNIADM                                          UNIDAD_ADM,
        UNIAD.CLAVE_UNIADM                                          ID_SUBUNIDAD,
        TIPNO.ID_TIPO_NOMBRAMIENTO                                  ID_TIPO_NOMBRAM,
        BAN.CLAVE_BANCO                                             ID_BANCO,
        DATBAN.NUMERO_CUENTA                                        CUENTA,
        UNIAD.CLAVE_UNIADM || 3                                     CENTRO_DISTRIB,
        DECODE(CONC.CLAVE_CONCEPTO,
              '03', 
              (SELECT NVL(TO_CHAR(FECHA_FIN,'YYYY'), NULL) FROM RH_NOMN_MANUALES_TERCEROS WHERE ID_EMPLEADO = EMP.ID_EMPLEADO AND ID_CONCEPTO = 50 AND ACTIVO = 'A'),
              '03E',
              (SELECT NVL(TO_CHAR(FECHA_FIN,'YYYY'), NULL) FROM RH_NOMN_MANUALES_TERCEROS WHERE ID_EMPLEADO =  EMP.ID_EMPLEADO AND ID_CONCEPTO = 121 AND ACTIVO = 'A'), 
              NULL)                                                 ANIO_FIN,
        DECODE(CONC.CLAVE_CONCEPTO,
              '03',
              (SELECT NVL(QUI.NUMERO_QUINCENA, NULL)
              FROM RH_NOMN_MANUALES_TERCEROS MANT, RH_NOMN_CAT_QUINCENAS QUI
              WHERE MANT.ID_EMPLEADO = EMP.ID_EMPLEADO AND MANT.ID_CONCEPTO = 50 AND MANT.ACTIVO = 'A' AND MANT.FECHA_FIN BETWEEN QUI.FECHA_INICIO_QUINCENA AND QUI.FECHA_FIN_QUINCENA),
              '03E',
              (SELECT NVL(QUI.NUMERO_QUINCENA, NULL)
              FROM RH_NOMN_MANUALES_TERCEROS MANT, RH_NOMN_CAT_QUINCENAS QUI
              WHERE MANT.ID_EMPLEADO = EMP.ID_EMPLEADO AND MANT.ID_CONCEPTO = 121 AND MANT.ACTIVO = 'A' AND MANT.FECHA_FIN BETWEEN QUI.FECHA_INICIO_QUINCENA AND QUI.FECHA_FIN_QUINCENA),        
              NULL)                                                 QUINCENA_FIN,
        DATBAN.SUCURSAL                                             ID_PLAZA_SUC,
        NULL                                                        CVE_PRES_RH,
        ENTFED.ID_ENTIDAD_FEDERATIVA                                ESTADO,
        GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL              ID_PUESTO,
        1                                                           ID_REGION,
        1                                                           ID_ESCALA,
        DECODE(CAB.ID_TIPO_NOMINA_FK, 1, 0, 2, 0, 3, 0, 19, 1, 20, 1, 21, 
              16, DECODE(RETROACTIVIDAD,1, 2, 0, 0), 
              17, DECODE(RETROACTIVIDAD,1, 2, 0, 0), 
              18, DECODE(RETROACTIVIDAD,1, 2, 0, 0))                DIF_RET,
        NULL                                                        REF,
        1                                                           UNIDADES,
        NULL                                                        ANTECEDENTE,
        NULL                                                        FOLIOMANUAL,
        NULL                                                        NUM_PCP,
        NULL                                                        ANIOMANUAL,
        DATBAN.CLABE                                                CLABE,
        (SELECT
          DECODE(TO_CHAR(CAB.FECHA_CIERRE, 'YYYY'), NULL, TO_CHAR(QUI.FECHA_FIN_QUINCENA,'YYYY'))
         FROM
          RH_NOMN_CABECERAS_NOMINAS CAB
          INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
         WHERE
          CAB.ID_NOMINA = vID_NOMINA)
                                                                    F_ELAB,
        (SELECT
          DECODE(TO_CHAR(CAB.FECHA_CIERRE, 'HH24:mm'), NULL, TO_CHAR(QUI.FECHA_FIN_QUINCENA, 'HH24:mm')) HORA_ELAB
         FROM
          RH_NOMN_CABECERAS_NOMINAS CAB
          INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
         WHERE
          CAB.ID_NOMINA = vID_NOMINA)        
                                                                    HORA_ELAB,
        'admin'                                                     ID_USUARIO  
      FROM
        RH_NOMN_CABECERAS_NOMINAS CAB
        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOM  ON NOM.ID_NOMINA_FK = CAB.ID_NOMINA
        INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA  CON ON CON.ID_NOMEMPPLA_FK = NOM.ID_NOMEMPPLA
        INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOM.ID_EMPLEADO_FK
        INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK
        INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
        INNER JOIN RH_NOMN_CAT_CONCEPTOS CONC ON CONC.ID_CONCEPTO = CON.ID_CONCEPTO_FK
        INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
        LEFT JOIN RH_NOMN_DATOS_BANCARIOS DATBAN ON DATBAN.ID_DATOS_BANCARIOS = EMP.ID_DATOS_BANCARIOS
        INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
        INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
        INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
        INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
        INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK
        INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = PLA.ID_ADSCRIPCION_FK
        INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNIAD ON UNIAD.ID_UNIADM = ADS.ID_UNIADM_FK
        INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNO ON TIPNO.ID_TIPO_NOMBRAMIENTO = PLA.ID_TIPO_NOMBRAM_FK
        LEFT JOIN RH_NOMN_CAT_BANCOS BAN ON BAN.ID_BANCO = DATBAN.ID_BANCO
        LEFT JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA ENTFED ON ENTFED.ID_ENTIDAD_FEDERATIVA = DATPER.ID_ENTIDAD_NACIMIENTO
      WHERE
        EJE.VALOR               = SP_EJERCICIO
        AND QUI.NUMERO_QUINCENA = SP_NUMERO_QUINCENA
        AND CAB.ID_NOMINA       = vID_NOMINA
        AND UNIAD.CLAVE_UNIADM  = vCLAVE_UNIADM    
        AND DATPER.RFC          = vRFC 
        -- ES TEMPORAL, YA QUE FALTA SABER SI DEBEN O NO APRECER EN EL ARCHIVO DE SALIDAD DE LOS DATOS FINANCIEROS.
        -- LAS QUE SON DEFINITIVAS SON LAS LICENCIAS, ESAS SINO DEBEN APARECER YA.
        AND CONC.CLAVE_CONCEPTO NOT IN
            (
              'ASP',
              'ASPE',
              '107A',
              '107AE',
              '107B',
              '107BE',
              '107C',
              '107CE',
              '107D',
              '107DE',
              '107E',
              '107EE',
              'SARP',
              'SARPE',
              '102P',
              '102PE',
              'FOVP',
              'FOVPE',
              'LMSBE',
              'LMSCE',
              'LSSBE',
              'LSSCE',
              'LMSB',
              'LMSC',
              'LSSB',
              'LSSC'              
            )        
      ORDER BY
        DATPER.RFC
    )
    GROUP BY 
      ID_EMPLEADO, 
      ANIO, 
      QUINCENA, 
      ID_TIPO_NOMINA, 
      ID_CONCEPTO, 
      TIPO_CONC, 
      CONSECUTIVO, 
      FILIACION, 
      NIVEL, 
      UNIDAD_ADM, 
      ID_SUBUNIDAD, 
      ID_TIPO_NOMBRAM, 
      ID_BANCO, 
      CUENTA, 
      CENTRO_DISTRIB, 
      ANIO_FIN, 
      QUINCENA_FIN, 
      ID_PLAZA_SUC, 
      CVE_PRES_RH, 
      ESTADO, 
      ID_PUESTO, 
      ID_REGION, 
      ID_ESCALA, 
      DIF_RET, 
      REF, 
      UNIDADES, 
      ANTECEDENTE, 
      FOLIOMANUAL, 
      NUM_PCP, 
      ANIOMANUAL, 
      CLABE, 
      F_ELAB, 
      HORA_ELAB, 
      ID_USUARIO
    ;

BEGIN

  -- VALIDACION PARA QUE SOLO SE PUEDA EJECUTAR APARTIR DE LA FECHA EN LA QUE SE REALIZAR LA MIGRACI�N DE DATOS.
  
  IF SP_EJERCICIO >= 2017 AND 
     SP_NUMERO_QUINCENA >= 20 THEN  

    DBMS_OUTPUT.PUT_LINE('DATOS FINANCIEROS DE EJERCICIO: ' || SP_EJERCICIO || ' CON NUMERO DE QUINCENA: ' || SP_NUMERO_QUINCENA);
  
    -- TABALA PARA GUARDAR LOS DATOS DEL FINANCIERO.
    vTABLATMP := 'RH_NOMN_DATOS_FINANCIEROS';
    
    -- DELETE DE LA TABLA PARA AGREGAR LOS NUEVOS DATOS FINANCIEROS.
    EXECUTE IMMEDIATE 'DELETE FROM '||vTABLATMP;  
    
    -- CICLO PARA LAS NOMINAS.
    OPEN C1;
      LOOP  
        FETCH C1 INTO vID_NOMINA, vID_TIPO_NOMINA_FK, vRETROACTIVIDAD, vID_NOMINA_PADRE;
      EXIT
        WHEN C1%NOTFOUND;              
        
      DBMS_OUTPUT.PUT_LINE('DATOS DE LA NOMINA: ' || vID_NOMINA || ' TIPO DE NOMINA:' || vID_TIPO_NOMINA_FK ||
                          ' RETROACTIVIDAD: ' || vRETROACTIVIDAD || ' Y ID PADRE EN CASO DE QUE APLIQUE: ' || 
                          vID_NOMINA_PADRE);        
        
      IF  vID_TIPO_NOMINA_FK = 16 OR
          vID_TIPO_NOMINA_FK = 17 OR
          vID_TIPO_NOMINA_FK = 18 OR
          vID_TIPO_NOMINA_FK = 19 OR
          vID_TIPO_NOMINA_FK = 20 OR
          vID_TIPO_NOMINA_FK = 21 THEN
          
        vCONTADORNOMINAS := vCONTADORNOMINAS + 1;
        
        -- OBTENER LA LETRA QUE LE CORRESPONDA A LA NOMINA SIEMPRE Y CUANDO SEA NO ORDINARIA
        IF vCONTADORNOMINAS = 1 THEN
          
          vLETRANOMINA := 'A';
          
        ELSIF vCONTADORNOMINAS = 2 THEN
        
          vLETRANOMINA := 'B';
        
        ELSIF vCONTADORNOMINAS = 3 THEN
        
          vLETRANOMINA := 'C';
        
        ELSIF vCONTADORNOMINAS = 4 THEN
        
          vLETRANOMINA := 'D';
        
        ELSIF vCONTADORNOMINAS = 5 THEN
        
          vLETRANOMINA := 'E';
        
        ELSIF vCONTADORNOMINAS = 6 THEN
        
          vLETRANOMINA := 'F';
        
        ELSIF vCONTADORNOMINAS = 7 THEN
        
          vLETRANOMINA := 'G';
        
        ELSIF vCONTADORNOMINAS = 8 THEN
        
          vLETRANOMINA := 'H';
        
        ELSIF vCONTADORNOMINAS = 9 THEN
        
          vLETRANOMINA := 'I';
        
        ELSIF vCONTADORNOMINAS = 10 THEN
          
          vLETRANOMINA := 'J';      
        END IF;  
        
        -- OBTENR EL MES AFECTADO DE LA NOMINA EXTRAORDINARIA.
        SELECT 
          TO_NUMBER(TO_CHAR(QUI.FECHA_FIN_QUINCENA, 'MM'))
        INTO
          vMESINCIDENCIA
        FROM 
          RH_NOMN_CABECERAS_NOMINAS CAB
          INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
        WHERE
          CAB.ID_NOMINA_PADRE = vID_NOMINA_PADRE
          AND CAB.ID_NOMINA = vID_NOMINA
        ;
        
        DBMS_OUTPUT.PUT_LINE('ES UNA NOMINA EXTRAORDINARIA CON DATOS; LETRA DE LA NOMINA: ' || vLETRANOMINA || 
                             ' MES DE APLICACION: ' || vMESINCIDENCIA);      
  
      ELSE
        vLETRANOMINA    := NULL;
        vMESINCIDENCIA  := NULL;
      END IF;
  
      -- CICLO PARA LAS UNIDADES ADMINISTRATIVAS.
      OPEN C2;
        LOOP     
          FETCH C2 INTO vCLAVE_UNIADM;
        EXIT
          WHEN C2%NOTFOUND; 
          
          DBMS_OUTPUT.PUT_LINE('LA UNIDAD A EJECUTAR ES: ' || vCLAVE_UNIADM || ' DE LA NOMINA: ' || vID_NOMINA);         
      
        -- CICLO PARA LOS EMPLEADOS.
        OPEN C3;
          LOOP       
              FETCH C3 INTO vNUMERO_EMPLEADO, vRFC;
          EXIT
            WHEN C3%NOTFOUND;     
        
          vCONSECUTIVOEMPLEADO := vCONSECUTIVOEMPLEADO + 1;
          
          /*
          DBMS_OUTPUT.PUT_LINE('EL EMPLEADO CON NUMERO DE EMPLEADO: ' || vNUMERO_EMPLEADO ||
                               ' , RFC: ' || vRFC || ' Y CONSECUTIVO: ' || vCONSECUTIVOEMPLEADO);
          */                                                      
          
          
          -- CICLO PARA CONTABILIZAR LAS LICENCIAS Y PODER OBTENER EL NETO EN EL SIGUIENTE CURSOR.
          OPEN C4;
            LOOP
              FETCH C4 INTO vCLAVE_CONCEPTO, vIMPORTE_CONCEPTO;
            EXIT
              WHEN C4%NOTFOUND;                          
              
            IF vCLAVE_CONCEPTO = 'LMSB' OR vCLAVE_CONCEPTO = 'LSSB' OR vCLAVE_CONCEPTO = 'LMSBE' OR vCLAVE_CONCEPTO = 'LSSBE' THEN
              
              vSUMA_IMPORTE_LIC_SB  := vSUMA_IMPORTE_LIC_SB + vIMPORTE_CONCEPTO;              
              vHAY_LICENCIAS        := TRUE;                            
              
            ELSIF vCLAVE_CONCEPTO = 'LMSC' OR vCLAVE_CONCEPTO = 'LSSC' OR vCLAVE_CONCEPTO = 'LMSCE' OR vCLAVE_CONCEPTO = 'LSSCE' THEN
              
              vSUMA_IMPORTE_LIC_CG  := vSUMA_IMPORTE_LIC_CG + vIMPORTE_CONCEPTO;              
              vHAY_LICENCIAS        := TRUE;
              
            END IF;             
            
          END LOOP;
          CLOSE C4;                        
          
          -- CICLO PARA REALIZAR LA INSERT DE CADA REGISTRO POR EMPLEADO CON LOS DATOS QE SE REQUIEREN PARA FINANCIERO.
          OPEN C5;
            LOOP       
              FETCH C5 INTO vID_EMPLEADO_2, vANIO, vQUINCENA, vID_TIPO_NOMINA, vID_CONCEPTO, vTIPO_CONC, vCONSECUTIVO, 
                            vIMPORTE, vFILIACION, vNIVEL, vUNIDAD_ADM, vID_SUBUNIDAD, vID_TIPO_NOMBRAM, vID_BANCO, vCUENTA, 
                            vCENTRO_DISTRIB, vANIO_FIN, vQUINCENA_FIN, vID_PLAZA_SUC, vCVE_PRES_RH, vESTADO, vID_PUESTO, 
                            vID_REGION, vID_ESCALA, vDIF_RET, vREF, vUNIDADES, vANTECEDENTE, vFOLIOMANUAL, vNUM_PCP, 
                            vANIOMANUAL, vCLABE, vF_ELAB, vHORA_ELAB, vID_USUARIO;
            EXIT
              WHEN C5%NOTFOUND;
          
            vIDDATOSFINANCIEROS := vIDDATOSFINANCIEROS + 1;
            
            vCANTREGISTRONOMINA := vCANTREGISTRONOMINA + 1;
                        
            -- VALIDAR SI HAY LICENCIAS QUE RESTAR TANTO DEL SUELDO BASE COMO DE LA COMPENSACION GARANTIZADA.
            IF vHAY_LICENCIAS THEN                                        
              
              -- PARA SUELDO COMPACTADO
              IF vID_CONCEPTO = '07' AND vSUMA_IMPORTE_LIC_SB > 0 THEN
              
                DBMS_OUTPUT.PUT_LINE('EMPLEADO: ' || vNUMERO_EMPLEADO || ' ENTRO A LICENCIA SB CON SUMA: ' || vSUMA_IMPORTE_LIC_SB);
                DBMS_OUTPUT.PUT_LINE('IMPORTE SB INICIAL: ' || vIMPORTE);
              
                vIMPORTE := vIMPORTE - vSUMA_IMPORTE_LIC_SB;
                                
                DBMS_OUTPUT.PUT_LINE('IMPORTE SB FINAL: ' || vIMPORTE);
                
              -- PARA COMPENSACION GARANTIZADA  
              ELSIF vID_CONCEPTO = '06' AND vSUMA_IMPORTE_LIC_CG > 0 THEN
              
                DBMS_OUTPUT.PUT_LINE('EMPLEADO: ' || vNUMERO_EMPLEADO || ' ENTRO A LICENCIA CG CON SUMA: ' || vSUMA_IMPORTE_LIC_CG);
                DBMS_OUTPUT.PUT_LINE('IMPORTE CG INICIAL: ' || vIMPORTE);
              
                vIMPORTE := vIMPORTE - vSUMA_IMPORTE_LIC_CG;
              
                DBMS_OUTPUT.PUT_LINE('IMPORTE CG FINAL: ' || vIMPORTE);
              
              END IF;                             
            END IF;                        
          
            /*
            DBMS_OUTPUT.PUT_LINE(
              'INSERT INTO ' || vTablatmp ||
              ' VALUES 
              ( ' 
              || vIDDATOSFINANCIEROS || ', '            
              || vID_EMPLEADO_2 || ', '
              || vANIO || ', '
              || vQUINCENA || ', '
              || vID_TIPO_NOMINA || ', '
              || '''' || NVL(vID_CONCEPTO, NULL) || ''', '            
              || vTIPO_CONC || ', '
              || vCONSECUTIVO || ', '
              || vIMPORTE || ', '
              || '''' || NVL(vFILIACION, NULL) || ''', '
              || '''' || NVL(vNIVEL, NULL) || ''', '
              || '''' || NVL(vUNIDAD_ADM, NULL) || ''', '
              || '''' || NVL(vID_SUBUNIDAD, NULL) || ''', ' 
              || vID_TIPO_NOMBRAM || ', '
              || vID_BANCO || ', '
              || '''' || NVL(vCUENTA, NULL) || ''', '
              || '''' || NVL(vCENTRO_DISTRIB, NULL) || ''', '
              || vANIO_FIN || ', '
              || vQUINCENA_FIN || ', '
              || vCONSECUTIVOEMPLEADO || ', '
              || vID_PLAZA_SUC || ', '
              || vCVE_PRES_RH || ', '
              || vESTADO || ', '
              || '''' || NVL(vID_PUESTO, NULL) || ''', '
              || vID_REGION || ', '
              || vID_ESCALA || ', '
              || vDIF_RET || ', '
              || '''' || NVL(vREF, NULL) || ''', '
              || '''' || NVL(vLETRANOMINA, NULL) || ''', '
              || vUNIDADES || ', '
              || vMESINCIDENCIA || ', '
              || '''' || NVL(vANTECEDENTE, NULL) || ''', '
              || vFOLIOMANUAL || ', '
              || vNUM_PCP || ', '
              || '''' || NVL(vANIOMANUAL, NULL) || ''', '
              || '''' || NVL(vCLABE, NULL) || ''', '
              || '''' || NVL(vF_ELAB, NULL) || ''', '
              || '''' || NVL(vHORA_ELAB, NULL) || ''', '
              || '''' || NVL(vID_USUARIO, NULL) || ''''                        
              || ')');
            */         
          
            INSERT INTO RH_NOMN_DATOS_FINANCIEROS
            VALUES
            (
              vIDDATOSFINANCIEROS,
              DECODE(vID_EMPLEADO_2, NULL, NULL, vID_EMPLEADO_2),
              DECODE(vANIO, NULL, NULL, vANIO),
              DECODE(vQUINCENA, NULL, NULL, vQUINCENA),
              DECODE(vID_TIPO_NOMINA, NULL, NULL, vID_TIPO_NOMINA),
              DECODE(vID_CONCEPTO,NULL, NULL, vID_CONCEPTO),
              DECODE(vTIPO_CONC, NULL, NULL, vTIPO_CONC),
              DECODE(vCONSECUTIVO, NULL, NULL, vCONSECUTIVO),
              DECODE(vIMPORTE, NULL, NULL, vIMPORTE),
              DECODE(vFILIACION,NULL, NULL, vFILIACION),
              DECODE(vNIVEL,NULL, NULL, vNIVEL),
              DECODE(vUNIDAD_ADM,NULL, NULL, vUNIDAD_ADM),
              DECODE(vID_SUBUNIDAD,NULL, NULL, vID_SUBUNIDAD),
              DECODE(vID_TIPO_NOMBRAM, NULL, NULL, vID_TIPO_NOMBRAM),
              DECODE(vID_BANCO, NULL, NULL, vID_BANCO),
              DECODE(vCUENTA,NULL, NULL, vCUENTA),
              DECODE(vCENTRO_DISTRIB,NULL, NULL, vCENTRO_DISTRIB),
              DECODE(vANIO_FIN, NULL, NULL, vANIO_FIN),
              DECODE(vQUINCENA_FIN, NULL, NULL, vQUINCENA_FIN),
              DECODE(vCONSECUTIVOEMPLEADO, NULL, NULL, vCONSECUTIVOEMPLEADO),
              DECODE(vID_PLAZA_SUC, NULL, NULL, vID_PLAZA_SUC),
              DECODE(vCVE_PRES_RH, NULL, NULL, vCVE_PRES_RH),
              DECODE(vESTADO, NULL, NULL, vESTADO),
              DECODE(vID_PUESTO,NULL, NULL, vID_PUESTO),
              DECODE(vID_REGION, NULL, NULL, vID_REGION),
              DECODE(vID_ESCALA, NULL, NULL, vID_ESCALA),
              DECODE(vDIF_RET, NULL, NULL, vDIF_RET),
              DECODE(vREF,NULL, NULL, vREF),
              DECODE(vLETRANOMINA,NULL, NULL, vLETRANOMINA),
              DECODE(vUNIDADES, NULL, NULL, vUNIDADES),
              DECODE(vMESINCIDENCIA, NULL, NULL, vMESINCIDENCIA),
              DECODE(vANTECEDENTE,NULL, NULL, vANTECEDENTE),
              DECODE(vFOLIOMANUAL, NULL, NULL, vFOLIOMANUAL),
              DECODE(vNUM_PCP, NULL, NULL, vNUM_PCP),
              DECODE(vANIOMANUAL,NULL, NULL, vANIOMANUAL),
              DECODE(vCLABE,NULL, NULL, vCLABE),
              DECODE(vF_ELAB,NULL, NULL, vF_ELAB),
              DECODE(vHORA_ELAB, NULL, NULL, vHORA_ELAB),
              DECODE(vID_USUARIO,NULL, NULL, vID_USUARIO)
            )
            ;
            
            --DBMS_OUTPUT.PUT_LINE('INSERTO EMPLEADO: ' || vID_EMPLEADO_2);                        
                    
          END LOOP;
          CLOSE C5;            
        
          -- REINICAR LAS VARIABLES PARA EL SIGUIENTE EMPLEADO.
          vSUMA_IMPORTE_LIC_SB  := 0;
          vSUMA_IMPORTE_LIC_CG  := 0;
          vHAY_LICENCIAS        := FALSE;        
        
        END LOOP;
        CLOSE C3;
        
        -- REINICIO DEL CONSECUTIVO POR UNIDAD ADMINISTRATIVA.
        vCONSECUTIVOEMPLEADO := 0;
      
      END LOOP;
      CLOSE C2;
      
      DBMS_OUTPUT.PUT_LINE('REGISTROS DE LA NOMINA: ' || vCANTREGISTRONOMINA);
      
      vCANTREGISTRONOMINA := 0;    
      COMMIT;
      
    END LOOP;
    CLOSE C1;
  
    COMMIT;
        
    SP_MENSAJE_SALIDA := 'EXITO DE EJECUCI�N';
        
  ELSE
  
    SP_MENSAJE_SALIDA := 'NO SE PUEDE EJECUTAR LA QUINCENA, YA QUE ES MENOR A LA CREACI�N DE LA MIGRACI�N DE DATOS.';
    
  END IF; 
  
  DBMS_OUTPUT.PUT_LINE('MENSAJE DE SALIDA: ' || SP_MENSAJE_SALIDA);

  EXCEPTION
  WHEN OTHERS THEN
  
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK - err_msg: ' || err_msg);
    
    SP_MENSAJE_SALIDA := 'ERROR EN EL SP_DATOS_FINANCIERO, FAVOR DE REVISARLO.';
    
    ROLLBACK;  

END SP_DATOS_FINANCIERO;

/
--------------------------------------------------------
--  DDL for Procedure SP_DIRECCION_DEPENDENCIA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_DIRECCION_DEPENDENCIA" (DATOS OUT SYS_REFCURSOR)IS
NOMBRE_DEPENDENCIA  VARCHAR2(100) := 'Instituto Federal de Telecomunicaciones ';
RAMO                NUMBER := 43;
PAGADURIA           NUMBER := 99900;
TELEFONO            VARCHAR(15):='50154000';
CALLE               VARCHAR2(250) := 'Insurgentes Sur';
NUMERO              VARCHAR(10) := '1143';
COLONIA             VARCHAR2(200) := 'Nochebuena';
MUNICIPIO           VARCHAR2(250) := 'Benito Ju�rez' ;
ENTIDAD             VARCHAR(30) := 'Ciudad de M�xico';
CODIGO_POSTAL       NUMBER := 03720;
BEGIN

OPEN DATOS FOR
SELECT 
        NOMBRE_DEPENDENCIA NOMBRE_DEPENDENCIA,
        RAMO               RAMO,
        PAGADURIA          PAGADURIA,
        TELEFONO           TELEFONO,
        CALLE              CALLE,
        NUMERO             NUMERO,
        COLONIA            COLONIA,
        MUNICIPIO          MUNICIPIO,
        ENTIDAD            ENTIDAD,
        CODIGO_POSTAL      CP 
FROM DUAL;

END SP_DIRECCION_DEPENDENCIA;

/
--------------------------------------------------------
--  DDL for Procedure SP_ELIMINA_REGISTRO_PLAZA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_ELIMINA_REGISTRO_PLAZA" (IDPLAZA IN NUMBER, MESSAGE OUT VARCHAR2) IS

ERR_CODE       INTEGER;
ERR_MSG        VARCHAR2(250);
IDMOVIMIENTO NUMBER;
IDMOVIMIENTOS SYS_REFCURSOR;
NUMPLAZA  NUMBER;


BEGIN 

--DBMS_OUTPUT.PUT_LINE('CONSULTAMOS LOS MOVIMIENTOS DE LA PLAZA');
    OPEN IDMOVIMIENTOS FOR 
        SELECT DISTINCT MOV.ID_HISTORICO_MOVIMIENTO 
        FROM RH_NOMN_BIT_HISTORICOS_MOVS MOV
        LEFT JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS CAMP ON CAMP.ID_HISTORICO_MOVIMIENTO_FK = MOV.ID_HISTORICO_MOVIMIENTO
        LEFT JOIN RH_NOMN_CAT_ACCIONES ACC ON ACC.ID_ACCION = MOV.ID_ACCION_FK
        LEFT JOIN RH_NOMN_CAT_TIPOS_MOVIMIENTO TIPMOV ON TIPMOV.ID_TIPOS_MOVIMIENTO = ACC.ID_TIPO_MOVIMIENTO
        LEFT JOIN RH_NOMN_CAT_CAMPOS CATCAMP ON CATCAMP.ID_CAMPO = CAMP.ID_CAMPO_AFECTADO_FK
        WHERE TIPMOV.ID_TIPOS_MOVIMIENTO = 1
        AND CAMP.VALOR_BUSQUEDA = TO_CHAR(IDPLAZA);
--DBMS_OUTPUT.PUT_LINE('TERMINA DE CONSULTAR');

DELETE FROM RH_NOMN_BIT_HISTORICOS_CAMPOS
WHERE ID_HISTORICO_CAMPO IN (SELECT  CAMP.ID_HISTORICO_CAMPO
                             FROM RH_NOMN_BIT_HISTORICOS_MOVS MOV
                             LEFT JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS CAMP ON CAMP.ID_HISTORICO_MOVIMIENTO_FK = MOV.ID_HISTORICO_MOVIMIENTO
                             LEFT JOIN RH_NOMN_CAT_ACCIONES ACC ON ACC.ID_ACCION = MOV.ID_ACCION_FK
                             LEFT JOIN RH_NOMN_CAT_TIPOS_MOVIMIENTO TIPMOV ON TIPMOV.ID_TIPOS_MOVIMIENTO = ACC.ID_TIPO_MOVIMIENTO
                             LEFT JOIN RH_NOMN_CAT_CAMPOS CATCAMP ON CATCAMP.ID_CAMPO = CAMP.ID_CAMPO_AFECTADO_FK
                             WHERE TIPMOV.ID_TIPOS_MOVIMIENTO = 1
                             AND CAMP.VALOR_BUSQUEDA = TO_CHAR(IDPLAZA)
                            )
;
--DBMS_OUTPUT.PUT_LINE('YA BORRO EL HOSTORICO DE CAMPOS');                            

LOOP
    FETCH IDMOVIMIENTOS INTO IDMOVIMIENTO;
        EXIT WHEN IDMOVIMIENTOS%NOTFOUND;
        DELETE FROM RH_NOMN_BIT_HISTORICOS_MOVS
        WHERE ID_HISTORICO_MOVIMIENTO = IDMOVIMIENTO;
    DBMS_OUTPUT.PUT_LINE('BORRA MOVIMIENTO '||IDMOVIMIENTO);
END LOOP;                            

--DBMS_OUTPUT.PUT_LINE('YA BORRO EL HOSTORICO DE MOVIMIENTOS');

SELECT NUMERO_PLAZA INTO NUMPLAZA
FROM RH_NOMN_PLAZAS
WHERE ID_PLAZA = IDPLAZA;

DELETE FROM RH_NOMN_PLAZAS
WHERE ID_PLAZA = IDPLAZA;
DBMS_OUTPUT.PUT_LINE('YA BORRO LA PLAZAS');

MESSAGE := 'LA ELIMINACION DE LA PLAZA '||NUMPLAZA||' SE REALIZO EXITOSAMENTE';
COMMIT;
--ROLLBACK;
EXCEPTION 
WHEN OTHERS THEN
 ERR_CODE := SQLCODE;
 ERR_MSG := SUBSTR(SQLERRM, 1, 200);
 MESSAGE := 'LA ELIMINACION DE LA PLAZA '||IDPLAZA||' NO SE PUDO REALIZAR';
 SPDERRORES('SP_ELIMINA_REGISTRO_PLAZA',ERR_CODE,ERR_MSG,''); 


ROLLBACK;


END ;

/
--------------------------------------------------------
--  DDL for Procedure SP_IDCONCEPTO_SEG_RETIRO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_IDCONCEPTO_SEG_RETIRO" (OPCION IN NUMBER, IDCONCEPTO OUT NUMBER )IS


BEGIN 

  IF OPCION = 1 THEN 
    SELECT ID_CONCEPTO INTO IDCONCEPTO 
    FROM RH_NOMN_CAT_CONCEPTOS
    WHERE TRIM(NOMBRE_CONCEPTO) = 'SEGURO DE RETIRO';
  
  ELSIF OPCION = 2 THEN 
    SELECT ID_CONCEPTO INTO IDCONCEPTO
    FROM RH_NOMN_CAT_CONCEPTOS
    WHERE TRIM(NOMBRE_CONCEPTO) = 'SEGURO DE RETIRO PERSONAL EVENTUAL';
      
  END IF;
  
END SP_IDCONCEPTO_SEG_RETIRO;

/
--------------------------------------------------------
--  DDL for Procedure SP_INTERINATO_FIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_INTERINATO_FIN" (IDEMPLEADO IN NUMBER)
IS
PRAGMA AUTONOMOUS_TRANSACTION;

ERR_CODE       INTEGER;
ERR_MSG        VARCHAR2(250);

VMAXHMOVS      INTEGER;
VMAXHCAMP      INTEGER;
VMAXMOVE       INTEGER;
VMAXMOVP       INTEGER;

NUMERO_INTERINATOS INTEGER;
NUMERO_INTERINATOS_2 INTEGER;
NUMERO_SEGUROS INTEGER;

VIDANIO        INTEGER;
VIDQUI         INTEGER;
FECHA_INI_QUIN DATE;
FECHA_FIN_QUIN DATE;

VCAL           FLOAT;
DIF_DIAS       INTEGER;

VAR_EMPLEADOS RH_NOMN_EMPLEADOS%ROWTYPE;
SEC_ID_EMPLEADO INTEGER;

-- INTERINATOS DE EMPLEADOS POR CAMBIO DE PUESTO.
CURSOR C_INTERINATOS 
IS 
  SELECT 
    INTERINATO.ID_INTERINATO, 
    INTERINATO.ID_PLAZA_FK_INICIAL, 
    INTERINATO.ID_PLAZA_FK_INTERINATO, 
    INTERINATO.ID_EMPLEADO_FK_INICIAL, 
    INTERINATO.ID_EMPLEADO_FK_INTERINATO, 
    ( SELECT NUMERO_EMPLEADO 
      FROM RH_NOMN_EMPLEADOS 
      WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
    ) NUMERO_EMPLEADO, 
    ( SELECT NUMERO_PLAZA 
      FROM RH_NOMN_EMPLEADOS EMP
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
    ) NUMERO_PLAZA_ANTERIOR,     
    ( SELECT NUMERO_PLAZA 
      FROM RH_NOMN_EMPLEADOS EMP
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INICIAL
    ) NUMERO_PLAZA,    
    ( SELECT PROPIETARIO 
      FROM RH_NOMN_PLAZAS      
      WHERE ID_PLAZA = INTERINATO.ID_PLAZA_FK_INTERINATO
    ) PROPIETARIO_PLAZA_ANTERIOR,  
    FECHA_INI_INTERINATO, 
    FECHA_FIN_INTERINATO, 
    FECHA_REGISTRO, 
    ESTATUS
  FROM RH_NOMN_INTERINATOS INTERINATO 
  WHERE ESTATUS='A'  
  AND ID_PLAZA_FK_INICIAL IS NOT NULL
  AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
;


-- INTERINATOS DE EMPLEADOS DE NUEVO INGRESO.
CURSOR C_INTERINATOS_NUEVO_EMP 
IS 
  SELECT 
    INTERINATO.ID_INTERINATO, 
    INTERINATO.ID_PLAZA_FK_INICIAL, 
    INTERINATO.ID_PLAZA_FK_INTERINATO, 
    INTERINATO.ID_EMPLEADO_FK_INICIAL, 
    INTERINATO.ID_EMPLEADO_FK_INTERINATO, 
    (
      SELECT NUMERO_EMPLEADO 
      FROM RH_NOMN_EMPLEADOS 
      WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
    ) NUMERO_EMPLEADO, 
    (
      SELECT NUMERO_PLAZA 
      FROM RH_NOMN_EMPLEADOS EMP
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INTERINATO
    ) NUMERO_PLAZA, 
    (
      SELECT PROPIETARIO 
      FROM RH_NOMN_PLAZAS      
      WHERE ID_PLAZA = INTERINATO.ID_PLAZA_FK_INTERINATO
    ) PROPIETARIO_PLAZA,    
    FECHA_INI_INTERINATO, 
    FECHA_FIN_INTERINATO, 
    FECHA_REGISTRO, 
    ESTATUS
  FROM  RH_NOMN_INTERINATOS INTERINATO 
  WHERE ESTATUS='A'    
  AND ID_PLAZA_FK_INICIAL IS NULL
  AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
;


-- SEGUROS DE LOS EMPLEADOS CON INTERINATO.
CURSOR C_SEGUROS 
IS  
  SELECT 
    SEGURO.ID_SEGURO,
    SEGURO.ID_EMPLEADO_FK,
    SEGURO.ID_CONCEPTO_FK,
    SEGURO.IMPORTE,
    SEGURO.IMPORTE_PRIMER_PAGO,
    SEGURO.IMPORTE_PAGO,
    INTERINATO.FECHA_FIN_INTERINATO
  FROM 
    RH_NOMN_SEGUROS SEGURO
    INNER JOIN RH_NOMN_INTERINATOS INTERINATO ON INTERINATO.ID_EMPLEADO_FK_INTERINATO=SEGURO.ID_EMPLEADO_FK
  WHERE     
    INTERINATO.ESTATUS = 'A' 
    AND SEGURO.ACTIVO = 'A'
    AND SEGURO.ID_EMPLEADO_FK = IDEMPLEADO
  ;
  
BEGIN     

    SELECT COUNT(*) 
    INTO NUMERO_INTERINATOS 
    FROM RH_NOMN_INTERINATOS 
    WHERE ESTATUS='A'
    AND ID_PLAZA_FK_INICIAL IS NOT NULL
    AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
;
    
--    DBMS_OUTPUT.PUT_LINE(' NUMERO DE INTERINATOS POR CAMBIO DE PUESTO: ' || NUMERO_INTERINATOS);
    
    SELECT COUNT(*) 
    INTO NUMERO_INTERINATOS_2 
    FROM RH_NOMN_INTERINATOS 
    WHERE ESTATUS='A'
    AND ID_PLAZA_FK_INICIAL IS NULL
    AND ID_EMPLEADO_FK_INTERINATO = IDEMPLEADO
    ;
    
    --DBMS_OUTPUT.PUT_LINE(' NUMERO DE INTERINATOS POR EMPLEADOS NUEVOS: ' || NUMERO_INTERINATOS_2);    
    
    
    
    SELECT 
      ID_EJERCICIO_FK ,
      ID_QUINCENA,
      FECHA_INICIO_QUINCENA,
      FECHA_FIN_QUINCENA  
    INTO 
      VIDANIO, 
      VIDQUI,
      FECHA_INI_QUIN,
      FECHA_FIN_QUIN
    FROM RH_NOMN_CAT_QUINCENAS
    WHERE ACTIVO = 1 
    AND TO_DATE(SYSDATE, 'DD/MM/YYYY') BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YYYY') AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YYYY')
;

    IF NUMERO_INTERINATOS > 0 THEN
    
      FOR LR2 IN C_SEGUROS LOOP 

        --DBMS_OUTPUT.PUT_LINE('PROCESO DE FINALIZACION DE INTERINATOS');

    
    
    --DBMS_OUTPUT.PUT_LINE('EJERCICIO: ' || VIDANIO || ' FECHA QUINCENA INICIO: ' || FECHA_INI_QUIN || ' FECHA QUINCENA FIN: ' || FECHA_FIN_QUIN);

    SELECT ABS(TRUNC(TO_DATE(LR2.FECHA_FIN_INTERINATO,'DD/MM/YYYY')) - TRUNC(TO_DATE(FECHA_INI_QUIN,'DD/MM/YYYY')))+1
    INTO DIF_DIAS
    FROM DUAL
    ;
    
    --DBMS_OUTPUT.PUT_LINE('DIFERENCIA DE DIAS TODAY - FECHA FINAL DE QUINCENA: ' || DIF_DIAS);
    
          
        VCAL := FN_PROPO_IMP_SEG_POR_DIAS(DIF_DIAS, LR2.IMPORTE, 1);
        
        UPDATE RH_NOMN_SEGUROS 
        SET IMPORTE_PAGO = VCAL 
        WHERE ID_SEGURO = LR2.ID_SEGURO
        ;
    
        --DBMS_OUTPUT.PUT_LINE('PROCESO DE PROPORCIONALIDAD DE SEGUROS : '|| LR2.ID_SEGURO );
      END LOOP;
        
      COMMIT;
    
      FOR R1 IN C_INTERINATOS LOOP        
        
        --CARGA INFORMACION DEL EMPLEADO QUE DEJA EL INTERINATO
        SELECT 
          ID_EMPLEADO, 
          ID_DATOS_PERSONALES, 
          ID_DATOS_BANCARIOS,
          ID_PLAZA, 
          ID_ESTATUS_EMPLEADO, 
          ID_TIPO_PAGO, 
          NUMERO_EMPLEADO,
          FECHA_DE_INGRESO, 
          FECHA_MODIFICACION, 
          FECHA_BAJA, 
          ID_CAUSA_BAJA, 
          NSS, 
          QUINQUENIO,
          VALOR_QUINQUENIO
        INTO 
          VAR_EMPLEADOS 
        FROM RH_NOMN_EMPLEADOS 
        WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK_INTERINATO
        ;
        
        --OBTENER SECUENCIA DE ID_EMPLEADO
        SELECT MAX(ID_EMPLEADO) + 1 
        INTO SEC_ID_EMPLEADO 
        FROM RH_NOMN_EMPLEADOS
        ;
        
        --GRABAR A UN NUEVO EMPLEADO QUE ES EL QUE SE QUEDA CON EL INTERINATO.
        INSERT INTO RH_NOMN_EMPLEADOS
        (
          ID_EMPLEADO, 
          ID_DATOS_PERSONALES, 
          ID_DATOS_BANCARIOS,
          ID_PLAZA, 
          ID_ESTATUS_EMPLEADO, 
          ID_TIPO_PAGO, 
          NUMERO_EMPLEADO,
          FECHA_DE_INGRESO, 
          FECHA_MODIFICACION, 
          FECHA_BAJA, 
          ID_CAUSA_BAJA, 
          NSS,
          QUINQUENIO,
          VALOR_QUINQUENIO
        )
        VALUES
        (
          SEC_ID_EMPLEADO,
          VAR_EMPLEADOS.ID_DATOS_PERSONALES, 
          VAR_EMPLEADOS.ID_DATOS_BANCARIOS,
          R1.ID_PLAZA_FK_INICIAL, 
          1, 
          VAR_EMPLEADOS.ID_TIPO_PAGO, 
          VAR_EMPLEADOS.NUMERO_EMPLEADO,
          R1.FECHA_FIN_INTERINATO+1, 
          SYSDATE, 
          NULL, 
          NULL, 
          VAR_EMPLEADOS.NSS,
          VAR_EMPLEADOS.QUINQUENIO,
          VAR_EMPLEADOS.VALOR_QUINQUENIO
        )
        ;        
        
        --ACTUALIZA LA INFORMACION DEL EMPLEADO
        UPDATE RH_NOMN_EMPLEADOS 
        SET 
          ID_ESTATUS_EMPLEADO = 4, 
          FECHA_BAJA = R1.FECHA_FIN_INTERINATO, 
          FECHA_MODIFICACION = SYSDATE
        WHERE 
          ID_EMPLEADO = R1.ID_EMPLEADO_FK_INTERINATO
        ;
                            
        /*INTERINATO PLAZA INICIAL*/     
        UPDATE RH_NOMN_PLAZAS  
        SET 
          ID_ESTATUS_PLAZA_FK  = 2 ,
          FECHA_DE_MODIFICACION = SYSDATE
        WHERE ID_PLAZA = R1.ID_PLAZA_FK_INICIAL
        ;
        
        /*INTERINATO PLAZA INTERINATO*/
        UPDATE RH_NOMN_PLAZAS  
        SET 
          ID_ESTATUS_PLAZA_FK = 5 ,
          FECHA_DE_MODIFICACION = SYSDATE
        WHERE 
          ID_PLAZA = R1.ID_PLAZA_FK_INTERINATO
        ;        
                
        /*PENSION*/      
        UPDATE  
          RH_NOMN_PENSIONES 
        SET 
          ID_ANIO_FIN = VIDANIO ,
          ID_QUINCENA_FIN = VIDQUI
        WHERE 
          ID_EMPLEADO = R1.ID_EMPLEADO_FK_INTERINATO
          AND ESTATUS = 'A'
        ;  
        
        /*MANUALESYT*/ 
        UPDATE  
          RH_NOMN_MANUALES_TERCEROS  
        SET 
          FECHA_FIN = FECHA_FIN_QUIN,                                          
          FECHA_MODIFICACION = SYSDATE
        WHERE 
          ID_EMPLEADO = R1.ID_EMPLEADO_FK_INTERINATO
          AND ACTIVO ='A'
        ;
        
        /* INTERINATO SE DEBE INACTIVAR*/
        UPDATE
          RH_NOMN_INTERINATOS
        SET
          ESTATUS = 'I'
        WHERE
          ID_INTERINATO = R1.ID_INTERINATO
        ;
        
        /********************************************************/
        /*HISTORICO MOVIMIENTOS EMPLEADO; GUARDA REGISTRO MOVIMIENTO TERMINO INTERINATO*/   
        SELECT 
          (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
        INTO 
          VMAXHMOVS 
        FROM 
          RH_NOMN_BIT_HISTORICOS_MOVS
        ;
        
        --OK
        INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
        (
          ID_HISTORICO_MOVIMIENTO, 
          ID_ACCION_FK, 
          ID_EJERCICIO_FK,
          FECHA, 
          ID_USUARIO_FK, 
          TABLA
        )
        VALUES 
        ( 
          VMAXHMOVS, 
          16, 
          VIDANIO, 
          R1.FECHA_FIN_INTERINATO, 
          1, 
          ''
        )
        ;        
        
        /*HISTORICO CAMPOS EMPLEADO*/   
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
      
        --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
        SELECT  
          NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
        INTO 
          VMAXMOVE 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        WHERE  
          ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO_FK_INTERINATO
        ;
        
        --EMPLEADO CAMBIA A ESTATUS CAMBIO DE PUESTO
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          SEC_ID_EMPLEADO,
          VMAXMOVE,
          10,
          4,
          R1.NUMERO_EMPLEADO
        )
        ;        

        --VERIFICAR SI EL CAMPO DE LA FECHA DE BAJA DEBE SER MODIFICADO CON LA FECHA DEL INTERINATO
        --SE DEBE REGISTRAR LA FECHA DE LA BAJA
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;                                            
        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          SEC_ID_EMPLEADO,
          VMAXMOVE,
          25,
          R1.FECHA_FIN_INTERINATO,
          R1.NUMERO_EMPLEADO
        )
        ;
        
        --MOVIENTO A LA PLAZA CONGELADA QUEDA CON ESTATUS OCUPADA.
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INICIAL,
          VMAXMOVE,
          5,
          2, 
          R1.NUMERO_PLAZA
        )
        ;           
                
        --MOVIENTO A LA PLAZA OCUPADA QUEDA CON ESTATUS TEMPORAL. 
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
                        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          5,
          5,
          R1.NUMERO_PLAZA_ANTERIOR
        )
        ;               
        
        /********************************************************/
        /*HISTORICO MOVIMIENTOS EMPLEADO; GUARDA REGISTRO MOVIMIENTO CAMBIO DE PUESTO INTERINATO*/   
        SELECT 
          (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
        INTO 
          VMAXHMOVS 
        FROM 
          RH_NOMN_BIT_HISTORICOS_MOVS
        ;
        
        --OK
        INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
        (
          ID_HISTORICO_MOVIMIENTO, 
          ID_ACCION_FK, 
          ID_EJERCICIO_FK,
          FECHA, 
          ID_USUARIO_FK, 
          TABLA
        )
        VALUES 
        ( 
          VMAXHMOVS, 
          21, 
          VIDANIO, 
          R1.FECHA_FIN_INTERINATO+1, 
          1, 
          ''
        )
        ;        
        
        /*HISTORICO CAMPOS EMPLEADO*/   
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
      
        -- ID DEL MOVIMIENTO ANTERIOR AL QUE SE TIENE REGISTRO.
        SELECT  
          MAX(ID_HISTORICO_MOVIMIENTO_FK) 
        INTO 
          VMAXMOVE 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        WHERE  
          ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO_FK_INTERINATO
        ;
        
        -- CAMBIO DE PUESTO: ESTATUS DE LA PLAZA ANTERIOR.
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          5,
          5,
          R1.NUMERO_PLAZA_ANTERIOR
        )
        ;        

        -- CAMBIO DE PUESTO: PROPIETARIO DE LA PLAZA ANTERIOR.
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;                                            
        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          6,
          R1.PROPIETARIO_PLAZA_ANTERIOR,
          R1.NUMERO_PLAZA_ANTERIOR
        )
        ;
        
        -- CAMBIO DE PUESTO: ESTATUS PLAZA ACTUAL.
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INICIAL,
          VMAXMOVE,
          5,
          2, 
          R1.NUMERO_PLAZA
        )
        ;
                
        -- CAMBIO DE PUESTO: PROPIETARIO PLAZA ACTUAL.
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
                        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_PLAZA_FK_INICIAL,
          VMAXMOVE,
          6,
          R1.NUMERO_EMPLEADO,
          R1.NUMERO_PLAZA
        )
        ;
        
        -- CAMBIO DE PUESTO: ID PLAZA ACTUAL DEL EMPLEADO.
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
                        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP,
          VMAXHMOVS,
          R1.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          9,
          R1.ID_PLAZA_FK_INICIAL,
          R1.NUMERO_EMPLEADO
        )
        ;                
        
        DBMS_OUTPUT.PUT_LINE('PROCESO DE TERMINO INTERINATO PARA LOS EMPLEADOS : '|| R1.ID_EMPLEADO_FK_INICIAL
        ||' PLAZA INICIAL: ' ||R1.ID_PLAZA_FK_INICIAL||' PLAZA INTERINATO: ' ||R1.ID_PLAZA_FK_INTERINATO);   
      
        COMMIT;
      END LOOP;
      
      COMMIT;
    
    END IF;
            
    IF NUMERO_INTERINATOS_2 > 0 THEN  
      --*****************************************--
      --SE TRATA LA PARTE DE NUEVO EMPLEADO CON INTERINATO    
      FOR R2 IN C_INTERINATOS_NUEVO_EMP LOOP
        
        --ACTUALIZA LA INFORMACION DEL EMPLEADO COMO UNA BAJA
        -- NOTA: REVISAR LA CAUSA BAJA QUE SE LE DAR�, POR AHORA SER� RENUNCIA (1).
        --  SE ACTUALIZA A TERMINO DE INTERINATO LA CAUSA BAJA.
        UPDATE 
          RH_NOMN_EMPLEADOS 
        SET 
          ID_ESTATUS_EMPLEADO = 2,
          ID_CAUSA_BAJA = 4,
          FECHA_BAJA = R2.FECHA_FIN_INTERINATO+1, 
          FECHA_MODIFICACION = SYSDATE
        WHERE 
          ID_EMPLEADO = R2.ID_EMPLEADO_FK_INTERINATO
        ;
        
        /*INTERINATO PLAZA INTERINATO*/
        UPDATE 
          RH_NOMN_PLAZAS  
        SET 
          ID_ESTATUS_PLAZA_FK = 5,
          FECHA_DE_MODIFICACION = SYSDATE
        WHERE 
          ID_PLAZA = R2.ID_PLAZA_FK_INTERINATO
        ;
                        
        /*PENSION*/      
        UPDATE  
          RH_NOMN_PENSIONES 
        SET 
          ID_ANIO_FIN = VIDANIO,
          ID_QUINCENA_FIN = VIDQUI
        WHERE 
          ID_EMPLEADO = R2.ID_EMPLEADO_FK_INTERINATO
          AND ESTATUS       ='A'
        ;  
        
        /*MANUALESYT*/ 
        UPDATE  
          RH_NOMN_MANUALES_TERCEROS  
        SET 
          FECHA_FIN = FECHA_FIN_QUIN,                                          
          FECHA_MODIFICACION = SYSDATE
        WHERE 
          ID_EMPLEADO = R2.ID_EMPLEADO_FK_INTERINATO
          AND ACTIVO ='A'
        ;
        
        /* INTERINATO SE DEBE INACTIVAR*/
        UPDATE
          RH_NOMN_INTERINATOS
        SET
          ESTATUS = 'I'
        WHERE
          ID_INTERINATO = R2.ID_INTERINATO
        ;        
                  
        /********************************************************/
        /*HISTORICO MOVIMIENTOS EMPLEADO; GUARDA REGISTRO MOVIMIENTO TERMINO INTERINATO*/   
        SELECT 
          (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
        INTO 
          VMAXHMOVS 
        FROM 
          RH_NOMN_BIT_HISTORICOS_MOVS
        ;
        
        --OK
        INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
        (
          ID_HISTORICO_MOVIMIENTO, 
          ID_ACCION_FK, 
          ID_EJERCICIO_FK,
          FECHA, 
          ID_USUARIO_FK, 
          TABLA
        )
        VALUES 
        ( 
          VMAXHMOVS, 
          16, 
          VIDANIO, 
          R2.FECHA_FIN_INTERINATO+1, 
          1, 
          ''
        )
        ;
        
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO )+1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
      
        --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
        SELECT  
          NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
        INTO 
          VMAXMOVE 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        WHERE  
          ID_REGISTRO_ENTIDAD = R2.ID_EMPLEADO_FK_INTERINATO
        ;
        
        --EMPLEADO CAMBIA A ESTATUS DE BAJA.
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP ,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          10,
          2,
          R2.NUMERO_EMPLEADO
        )
        ;

        --VERIFICAR SI EL CAMPO DE LA FECHA DE BAJA DEBE SER MODIFICADO CON LA FECHA DEL INTERINATO
        --SE DEBE REGISTRAR LA FECHA DE LA BAJA
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP ,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          25,
          R2.FECHA_FIN_INTERINATO+1,
          R2.NUMERO_EMPLEADO
        )
        ;
                        
        --MOVIENTO A LA PLAZA OCUPADA QUEDA CON ESTATUS TEMPORAL.  ----OK 
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
                        
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        (
          VMAXHCAMP,
          VMAXHMOVS,
          R2.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          5,
          5,
          R2.NUMERO_PLAZA
        )
        ;
        
        /********************************************************/
        /*HISTORICO MOVIMIENTOS EMPLEADO; GUARDA REGISTRO MOVIMIENTO BAJA DE EMPLEADO*/   
        SELECT 
          (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
        INTO 
          VMAXHMOVS 
        FROM 
          RH_NOMN_BIT_HISTORICOS_MOVS
        ;
        
        --OK
        INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
        (
          ID_HISTORICO_MOVIMIENTO, 
          ID_ACCION_FK, 
          ID_EJERCICIO_FK,
          FECHA, 
          ID_USUARIO_FK, 
          TABLA
        )
        VALUES 
        ( 
          VMAXHMOVS, 
          11, 
          VIDANIO, 
          R2.FECHA_FIN_INTERINATO, 
          1, 
          ''
        )
        ;
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO )+1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
      
        -- MOVIMIENTO ANTERIOR.
        SELECT  
          NVL((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
        INTO 
          VMAXMOVE 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        WHERE  
          ID_REGISTRO_ENTIDAD = R2.ID_EMPLEADO_FK_INTERINATO
        ;
        
        -- BAJA DE EMPLEADO: CAUSA DE LA BAJA POR DEFAULT SE DEJA RENUNCIA FALTA VER CUAL CAUSA DEBE DE APLICAR.
        --  SE ACTUALIZA A TERMINO DE INTERINATO LA CAUSA BAJA.
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP ,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          24,
          4,
          R2.NUMERO_EMPLEADO
        )
        ;
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
        
        -- BAJA DE EMPLEADO: FECHA DE LA BAJA.
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        ( 
          VMAXHCAMP ,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          25,
          R2.FECHA_FIN_INTERINATO+1,
          R2.NUMERO_EMPLEADO
        )
        ;
                                
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
         
        -- BAJA DE EMPLEADO: ID DE LA PLAZA DEL EMPLEADO QUE SE DA DE BAJA.               
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        (
          VMAXHCAMP,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          9,
          R2.ID_PLAZA_FK_INTERINATO,
          R2.NUMERO_EMPLEADO
        )
        ;   
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
         
        -- BAJA DE EMPLEADO: ID DEL ESTATUS CON QUE SE QUEDA EL EMPLEADO QUE SE DA DE BAJA.               
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        (
          VMAXHCAMP,
          VMAXHMOVS,
          R2.ID_EMPLEADO_FK_INTERINATO,
          VMAXMOVE,
          10,
          2,
          R2.NUMERO_EMPLEADO
        )
        ; 
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
         
        -- BAJA DE EMPLEADO: ID DEL ESTATUS DE LA PLAZA DEL EMPLEADO QUE SE DA DE BAJA, EN ESTE CASO ES TEMPORAL YA QUE VIENE DE UN INTERINATO POR LICENCIA.               
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        (
          VMAXHCAMP,
          VMAXHMOVS,
          R2.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          5,
          5,
          R2.NUMERO_PLAZA
        )
        ; 
        
        SELECT 
          (MAX(ID_HISTORICO_CAMPO ) + 1) 
        INTO 
          VMAXHCAMP 
        FROM 
          RH_NOMN_BIT_HISTORICOS_CAMPOS
        ;
         
        -- BAJA DE EMPLEADO: PROPIETARIO DE LA PLAZA TEMPORAL.               
        INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
        (
          ID_HISTORICO_CAMPO, 
          ID_HISTORICO_MOVIMIENTO_FK, 
          ID_REGISTRO_ENTIDAD, 
          ID_MOVIMIENTO_ANTERIOR, 
          ID_CAMPO_AFECTADO_FK, 
          VALOR_ACTUAL, 
          VALOR_BUSQUEDA
        ) 
        VALUES 
        (
          VMAXHCAMP,
          VMAXHMOVS,
          R2.ID_PLAZA_FK_INTERINATO,
          VMAXMOVE,
          6,
          R2.PROPIETARIO_PLAZA,
          R2.NUMERO_PLAZA
        )
        ;         
        
        DBMS_OUTPUT.PUT_LINE('PROCESO DE TERMINO INTERINATO PARA LOS EMPLEADOS : '|| R2.ID_EMPLEADO_FK_INICIAL
        ||' PLAZA INICIAL: ' ||R2.ID_PLAZA_FK_INICIAL||' PLAZA INTERINATO: ' ||R2.ID_PLAZA_FK_INTERINATO);  
      
        COMMIT;
      END LOOP;
      
      COMMIT;
    
    END IF;
    
  EXCEPTION  
    WHEN NO_DATA_FOUND THEN  
      NULL;
    WHEN OTHERS THEN   
      ROLLBACK;
      ERR_CODE := SQLCODE;
      ERR_MSG := SUBSTR(SQLERRM, 1, 200);
    
      DBMS_OUTPUT.PUT_LINE ('CON ERROR - ROLLBACK');
      DBMS_OUTPUT.PUT_LINE ('ERROR:'||ERR_CODE||','||ERR_MSG);
    
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_INTERINATO_INICIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_INTERINATO_INICIO" 
IS
PRAGMA AUTONOMOUS_TRANSACTION;

ERR_CODE       INTEGER;
ERR_MSG        VARCHAR2(250);

VMAXHMOVS      INTEGER;
VMAXHCAMP      INTEGER;
VMAXMOVE       INTEGER;
VMAXMOVP       INTEGER;

NUMERO_INTERINATOS INTEGER;
NUMERO_SEGUROS INTEGER;

VIDANIO        INTEGER;
VIDQUI         INTEGER;
FECHA_INI_QUIN DATE;
FECHA_FIN_QUIN DATE;

PFECHA         VARCHAR2(250);
vcal           FLOAT;
DIF_DIAS       INTEGER;

var_empleados rh_nomn_empleados%ROWTYPE;
sec_id_empleado INTEGER;

importesSegRet  SYS_REFCURSOR;
IMPORTE_NON_EMP NUMBER;
IMPORTE_PAR_EMP NUMBER;
IMPORTE_NON_DEP NUMBER;
IMPORTE_PAR_DEP NUMBER;
idConceptoSegRet NUMBER;

CURSOR c_interinatoS IS 
  SELECT    INTERINATO.ID_INTERINATO,
            INTERINATO.FECHA_INI_INTERINATO,
            INTERINATO.ID_PLAZA_FK_INICIAL,
            ID_PLAZA_FK_INTERINATO,
            INTERINATO.ID_EMPLEADO_FK_INICIAL,
            (SELECT NUMERO_EMPLEADO 
             FROM RH_NOMN_EMPLEADOS 
             WHERE ID_EMPLEADO = INTERINATO.ID_EMPLEADO_FK_INICIAL) NUMERO_EMPLEADO,
            (SELECT MAX(FECHA_FIN_LICENCIA) 
             FROM RH_NOMN_LICENCIAS_SIN_GOCE
             WHERE ESTATUS = 'A' 
             AND ID_EMPLEADO_FK = EMPLEADO.ID_EMPLEADO) FECHA_FIN_INTERINATO
  FROM RH_NOMN_INTERINATOS INTERINATO
  INNER JOIN RH_NOMN_PLAZAS PLAZA ON PLAZA.ID_PLAZA=INTERINATO.ID_PLAZA_FK_INTERINATO
  INNER JOIN RH_NOMN_EMPLEADOS EMPLEADO ON EMPLEADO.ID_PLAZA=PLAZA.ID_PLAZA
  WHERE PLAZA.ID_ESTATUS_PLAZA_FK = 5 
  AND EMPLEADO.ID_ESTATUS_EMPLEADO = 3 
  AND INTERINATO.ESTATUS='P' 
  AND TRUNC(TO_DATE(INTERINATO.FECHA_INI_INTERINATO,'DD/MM/YY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YY'));

CURSOR C_SEGUROS IS  
  SELECT    SEGURO.ID_SEGURO,
            SEGURO.ID_EMPLEADO_FK,
            SEGURO.ID_CONCEPTO_FK,
            SEGURO.IMPORTE,
            SEGURO.IMPORTE_PRIMER_PAGO,
            SEGURO.IMPORTE_PAGO
  FROM RH_NOMN_SEGUROS SEGURO
  INNER JOIN RH_NOMN_INTERINATOS INTERINATO ON INTERINATO.ID_EMPLEADO_FK_INICIAL=SEGURO.ID_EMPLEADO_FK
  WHERE TRUNC(TO_DATE(INTERINATO.FECHA_INI_INTERINATO,'DD/MM/YY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YY')) 
  AND INTERINATO.ESTATUS='P' 
  AND SEGURO.ACTIVO='A';


BEGIN     

    SELECT  ID_EJERCICIO_FK,
            ID_QUINCENA,
            FECHA_INICIO_QUINCENA,
            FECHA_FIN_QUINCENA  
    INTO    VIDANIO, 
            VIDQUI,
            FECHA_INI_QUIN,
            FECHA_FIN_QUIN
    FROM RH_NOMN_CAT_QUINCENAS
    WHERE ACTIVO = 1 
    AND TO_DATE(SYSDATE, 'DD/MM/YY') BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YY') AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YY');


    SELECT (TRUNC(TO_DATE(FECHA_FIN_QUIN,'DD/MM/YY'))) - (TRUNC(TO_DATE(FECHA_INI_QUIN,'DD/MM/YY'))) 
    INTO DIF_DIAS
    FROM DUAL;


    SELECT COUNT(*) 
    INTO NUMERO_INTERINATOS 
    FROM RH_NOMN_INTERINATOS 
    WHERE TRUNC(TO_DATE(FECHA_INI_INTERINATO,'DD/MM/YY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YY')) 
    AND ESTATUS='P';


    SELECT COUNT(*) 
    INTO NUMERO_SEGUROS
    FROM RH_NOMN_SEGUROS SEGURO
    INNER JOIN RH_NOMN_INTERINATOS INTERINATO ON INTERINATO.ID_EMPLEADO_FK_INICIAL=SEGURO.ID_EMPLEADO_FK
    WHERE TRUNC(TO_DATE(INTERINATO.FECHA_INI_INTERINATO,'DD/MM/YY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YY')) 
    AND INTERINATO.ESTATUS='P' 
    AND SEGURO.ACTIVO='A';

    IF NUMERO_INTERINATOS>0 THEN


    FOR LR2 IN C_SEGUROS
        LOOP 
          VCAL:=FN_PROPO_IMP_SEG_POR_DIAS(DIF_DIAS,LR2.IMPORTE,1);
          UPDATE RH_NOMN_SEGUROS SET IMPORTE_PAGO = VCAL 
          WHERE ID_SEGURO=LR2.ID_SEGURO;
        END LOOP;
    COMMIT;

    FOR R1 IN c_interinatoS
    LOOP
      --carga informacion del empleado que va para interinato
      SELECT    ID_EMPLEADO, 
                ID_DATOS_PERSONALES, 
                ID_DATOS_BANCARIOS,
                ID_PLAZA, 
                ID_ESTATUS_EMPLEADO, 
                ID_TIPO_PAGO, 
                NUMERO_EMPLEADO,
                FECHA_DE_INGRESO, 
                FECHA_MODIFICACION, 
                FECHA_BAJA, 
                ID_CAUSA_BAJA, 
                NSS, 
                QUINQUENIO, 
                VALOR_QUINQUENIO 
      INTO var_empleados 
      FROM RH_NOMN_EMPLEADOS 
      WHERE ID_EMPLEADO=r1.id_empleado_fk_inicial;      
      --obtener secuencia de id_empleado
      SELECT MAX(ID_EMPLEADO)+1 INTO  sec_id_empleado 
      FROM RH_NOMN_EMPLEADOS;
      --grabar a un nuevo empleado que es el que se queda con el interinato.
      INSERT INTO RH_NOMN_EMPLEADOS(ID_EMPLEADO, ID_DATOS_PERSONALES, ID_DATOS_BANCARIOS,ID_PLAZA, ID_ESTATUS_EMPLEADO, ID_TIPO_PAGO, NUMERO_EMPLEADO,FECHA_DE_INGRESO, FECHA_MODIFICACION, FECHA_BAJA, ID_CAUSA_BAJA, NSS)
      VALUES(sec_id_empleado,var_empleados.ID_DATOS_PERSONALES, var_empleados.ID_DATOS_BANCARIOS,r1.id_plaza_fk_interinato, 1, var_empleados.ID_TIPO_PAGO, var_empleados.NUMERO_EMPLEADO,r1.fecha_ini_interinato, null, null, null, var_empleados.NSS);      

      ---Obtenemos los importes del seguro de retiro
      importesSegRet := FN_ARGUMENTOS_SEG_RETIRO;
      LOOP
        FETCH importesSegRet INTO IMPORTE_NON_EMP, IMPORTE_PAR_EMP, IMPORTE_NON_DEP,IMPORTE_PAR_DEP;
            EXIT WHEN importesSegRet%NOTFOUND;
      END LOOP;

      --Obtenemos el ID_CONCEPTO
      SELECT DECODE(ID_TIPO_NOMBRAMIENTO,1,39,2,79) INTO idConceptoSegRet
      FROM RH_NOMN_CAT_TIPOS_NOMBRAMIENTO
      WHERE ID_TIPO_NOMBRAMIENTO = (SELECT ID_TIPO_NOMBRAM_FK 
                                    FROM RH_NOMN_PLAZAS
                                    WHERE ID_PLAZA = r1.id_plaza_fk_interinato
                                   );

      --Creamos el seguro de retiro, ya que este seguro nace cuando se da de alta el empleado
        INSERT INTO RH_NOMN_SEGURO_RETIRO 
        VALUES( (SELECT NVL(MAX(ID_SEGURO_RETIRO),0)+1 FROM RH_NOMN_SEGURO_RETIRO),
                IMPORTE_NON_EMP,
                IMPORTE_PAR_EMP,
                IMPORTE_NON_DEP,
                IMPORTE_PAR_DEP,
                TO_DATE(SYSDATE,'DD/MM/YY'),
                'A',
                sec_id_empleado,
                idConceptoSegRet
              );


      --cambia estatus,fecha_fin_interinato,id_empleado_fk_interinato
      UPDATE RH_NOMN_INTERINATOS 
      SET ESTATUS='A',
          FECHA_FIN_INTERINATO = r1.fecha_fin_interinato,
          ID_EMPLEADO_FK_INTERINATO = sec_id_empleado 
      WHERE ID_INTERINATO=R1.ID_INTERINATO;


      --actualiza la informacion del empleado
      UPDATE RH_NOMN_EMPLEADOS 
      SET ID_ESTATUS_EMPLEADO = 4,
          FECHA_BAJA = r1.fecha_ini_interinato-1,
          FECHA_MODIFICACION = r1.fecha_ini_interinato 
      WHERE ID_EMPLEADO = r1.id_empleado_fk_inicial;

      --Actualizamos la informacion del seguro de retiro para el idempleadoAterior
      UPDATE RH_NOMN_SEGURO_RETIRO SET ACTIVO = 'I'
      WHERE ID_EMPLEADO_FK = r1.id_empleado_fk_inicial;

      /*interinato PLAZA inicial*/     
      UPDATE RH_NOMN_PLAZAS  
      SET ID_ESTATUS_PLAZA_FK    = 4,
          FECHA_DE_MODIFICACION = SYSDATE
      WHERE ID_PLAZA = R1.id_plaza_fk_inicial;
      /*interinato PLAZA interinato*/
      UPDATE RH_NOMN_PLAZAS  SET ID_ESTATUS_PLAZA_FK    = 2 ,
      FECHA_DE_MODIFICACION = SYSDATE
      WHERE ID_PLAZA = R1.id_plaza_fk_interinato;



      /*PENSION*/      
      UPDATE  RH_NOMN_PENSIONES 
        SET ID_ANIO_FIN = VIDANIO ,
            ID_QUINCENA_FIN   = VIDQUI
      WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK_INICIAL
      AND ESTATUS       ='A';  

      /*MANUALESYT*/   -- LOS MANUALES Y TERCEROS TAMBIEN SE INACTIVAN?Arnol: queda activo.
      UPDATE  RH_NOMN_MANUALES_TERCEROS  
        SET FECHA_FIN= FECHA_FIN_QUIN,                                          
            FECHA_MODIFICACION   = SYSDATE
      WHERE ID_EMPLEADO    = R1.ID_EMPLEADO_FK_INICIAL
      AND    ACTIVO ='A';

      /********************************************************/
      /*HISTORICO MOVIMIENTOS EMPLEADO; guarda registro movimiento interinato*/   
      SELECT (max(ID_HISTORICO_MOVIMIENTO)+1) INTO VMAXHMOVS FROM RH_NOMN_BIT_HISTORICOS_MOVS;
      --OK
      INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS (ID_HISTORICO_MOVIMIENTO, ID_ACCION_FK, ID_EJERCICIO_FK,FECHA, ID_USUARIO_FK, TABLA) 
      VALUES ( VMAXHMOVS , 15, VIDANIO, r1.fecha_ini_interinato, 1, '');   

      /*HISTORICO CAMPOS EMPLEADO*/   
      SELECT TO_CHAR((TO_DATE(SYSDATE,'DD/MM/YY')),'DD/MM/YY') INTO PFECHA FROM DUAL;
      SELECT (max(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP FROM RH_NOMN_BIT_HISTORICOS_CAMPOS; 


      --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
      SELECT  nvl((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) INTO VMAXMOVE FROM RH_NOMN_BIT_HISTORICOS_CAMPOS 
      WHERE  ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO_FK_INICIAL ;      

      --valores para insertar en 2

      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
      ID_HISTORICO_CAMPO, ID_HISTORICO_MOVIMIENTO_FK, ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, ID_CAMPO_AFECTADO_FK, VALOR_ACTUAL, 
      VALOR_BUSQUEDA) 
      VALUES ( VMAXHCAMP ,
              VMAXHMOVS,
              R1.ID_EMPLEADO_FK_INICIAL,
              VMAXMOVE,
              10,
              4,
              R1.NUMERO_EMPLEADO);


      --VERIFICAR SI EL CAMPO DE LA FECHA DE BAJA DEBE SER MODIFICADO CON LA FECHA del interinato  ??????                              
      --se debe registrar la fecha de la baja
      SELECT (max(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP 
      FROM RH_NOMN_BIT_HISTORICOS_CAMPOS;                                                 

      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
      ID_HISTORICO_CAMPO, ID_HISTORICO_MOVIMIENTO_FK, ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, ID_CAMPO_AFECTADO_FK, VALOR_ACTUAL, 
      VALOR_BUSQUEDA) 
      VALUES ( VMAXHCAMP ,
              VMAXHMOVS,
              R1.ID_EMPLEADO_FK_INICIAL,
              VMAXMOVE,
              25,
              PFECHA,
              R1.NUMERO_EMPLEADO);


      --MOVIENTO A LA PLAZA QUEDA CON ESTATUS congelada.  ----OK      
      SELECT (max(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP FROM RH_NOMN_BIT_HISTORICOS_CAMPOS;

      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
      ID_HISTORICO_CAMPO, ID_HISTORICO_MOVIMIENTO_FK, ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, ID_CAMPO_AFECTADO_FK, VALOR_ACTUAL, 
      VALOR_BUSQUEDA) 
                     VALUES ( VMAXHCAMP ,
                              VMAXHMOVS,
                              R1.ID_PLAZA_fk_inicial,
                              VMAXMOVE,
                              5,
                              4,
                              R1.ID_PLAZA_fk_inicial);

      --MOVIENTO A LA PLAZA QUEDA CON ESTATUS ocupada.  ----OK      
      SELECT (max(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP FROM RH_NOMN_BIT_HISTORICOS_CAMPOS;

      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
      ID_HISTORICO_CAMPO, ID_HISTORICO_MOVIMIENTO_FK, ID_REGISTRO_ENTIDAD, 
      ID_MOVIMIENTO_ANTERIOR, ID_CAMPO_AFECTADO_FK, VALOR_ACTUAL, 
      VALOR_BUSQUEDA) 
                     VALUES ( VMAXHCAMP ,
                              VMAXHMOVS,
                              R1.ID_PLAZA_fk_interinato,
                              VMAXMOVE,
                              5,
                              2,
                              R1.ID_PLAZA_fk_interinato);                              


      DBMS_OUTPUT.PUT_LINE('PROCESO DE INTERINATO PARA LOS EMPLEADOS : '|| R1.ID_EMPLEADO_FK_INICIAL
            ||' PLAZA INICIAL: ' ||R1.ID_PLAZA_fk_inicial||' PLAZA INTERINATO: ' ||R1.id_plaza_fk_interinato);   
    END LOOP;
    COMMIT;

    END IF;
    EXCEPTION  
    WHEN NO_DATA_FOUND THEN  
      NULL;
    WHEN OTHERS THEN   
    ROLLBACK;
      err_code := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 200);
      DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
      DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_LIC_MEDICAS_ANTIGUEDAD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_LIC_MEDICAS_ANTIGUEDAD" (NUMEMPLEADO IN INTEGER,DATOS OUT SYS_REFCURSOR )IS
BEGIN

    OPEN DATOS FOR
    SELECT ID_EMPLEADO, INSTITUCION, PUESTO,FECHA_INGRESO, FECHA_BAJA
FROM 
(
    SELECT  
            EM.ID_EMPLEADO,
            'IFT' INSTITUCION,
            PST.DESCRIPCION PUESTO,
            TO_CHAR(EM.FECHA_DE_INGRESO,'DD/MM/YY') FECHA_INGRESO, 
            TO_CHAR(EM.FECHA_BAJA,'DD/MM/YY') FECHA_BAJA
    FROM RH_NOMN_EMPLEADOS EM
    LEFT JOIN RH_NOMN_PLAZAS           PLZ ON PLZ.ID_PLAZA            = EM.ID_PLAZA
    LEFT JOIN RH_NOMN_CAT_PUESTOS      PST ON PST.ID_PUESTO           = PLZ.ID_PUESTO_FK
    WHERE ID_EMPLEADO IN (SELECT ID_EMPLEADO FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = NUMEMPLEADO)
    
    UNION
    
    SELECT 
        ID_EMPLEADO,
        INSTITUCION, 
        PUESTO, 
        TO_CHAR(FECHA_INGRESO,'DD/MM/YY') FECHA_INGRESO,
        TO_CHAR(FECHA_EGRESO,'DD/MM/YY') FECHA_BAJA
    FROM RH_NOMN_MOV_GOB_FEDERAL
    WHERE ID_EMPLEADO IN (SELECT ID_EMPLEADO FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = NUMEMPLEADO)
)
ORDER BY FECHA_INGRESO
;
END SP_LIC_MEDICAS_ANTIGUEDAD;

/
--------------------------------------------------------
--  DDL for Procedure SP_LICENCIA_SIN_GOCE_FIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_LICENCIA_SIN_GOCE_FIN" (idEmpleado IN NUMBER) ---Si el idEmpleado llega como -1 es ejecutado por el job, si es diferente se ejecuta desde la aplicacion
IS
PRAGMA AUTONOMOUS_TRANSACTION;

  ERR_CODE              INTEGER;
  ERR_MSG               VARCHAR2(250);
  
  NUMERO_LICENCIAS      INTEGER;
  VIDANIO               INTEGER;
  VMAXHMOVS             INTEGER;
  VMAXHCAMP             INTEGER;
  VMAXMOVE              INTEGER;
  VMAXMOVP              INTEGER;
  
  vIDEMPINTPLAZACONG    INTEGER;
  vIDEMPINTEMPNVO       INTEGER;
  vSALIDAFNINTERINATO   INTEGER := 0;
  
  sec_id_empleado       INTEGER;
  vID_EMPLEADO          INTEGER; 
  vID_DATOS_PERSONALES  INTEGER;
  vID_DATOS_BANCARIOS   INTEGER;
  vID_PLAZA             INTEGER;
  vID_ESTATUS_EMPLEADO  INTEGER;
  vID_TIPO_PAGO         INTEGER;
  vNUMERO_EMPLEADO      INTEGER;
  vFECHA_DE_INGRESO     DATE;
  vFECHA_MODIFICACION   DATE;
  vFECHA_BAJA           DATE;
  vID_CAUSA_BAJA        INTEGER;
  vNSS                  VARCHAR(30);
  vQUINQUENIO           INTEGER;
  vVALOR_QUINQUENIO     INTEGER;
  
  CURSOR C_LICENCIAS_APLICADAS 
  IS
    SELECT 
      LSG.ID_LICENCIA_SIN_GOCE, 
      ID_EMPLEADO_FK,
      PLAZA.ID_PLAZA,
      LSG.FECHA_INICIO_LICENCIA,
      LSG.FECHA_FIN_LICENCIA,
      EMP.NUMERO_EMPLEADO
    FROM 
      RH_NOMN_LICENCIAS_SIN_GOCE LSG
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = LSG.ID_EMPLEADO_FK  
      INNER JOIN RH_NOMN_PLAZAS PLAZA ON PLAZA.ID_PLAZA   = EMP.ID_PLAZA
    WHERE 
      TRUNC(TO_DATE(LSG.FECHA_FIN_LICENCIA,'DD/MM/YYYY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YYYY'))
      AND LSG.ESTATUS = 'A'
      AND EMP.ID_EMPLEADO = DECODE(idEmpleado,-1, EMP.ID_EMPLEADO,idEmpleado)
    ;   
  
BEGIN
        
    -- OBTENER EL EJERCICIO ACTUAL.
    SELECT ID_EJERCICIO_FK 
    INTO VIDANIO
    FROM  RH_NOMN_CAT_QUINCENAS
    WHERE  ACTIVO = 1 
    AND TO_DATE(SYSDATE, 'DD/MM/YYYY') BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YYYY') AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YYYY')
    ;        
      
    -- CANTIDAD DE LICENCIAS A PROCESAR.            
    SELECT COUNT(*) 
    INTO NUMERO_LICENCIAS 
    FROM RH_NOMN_LICENCIAS_SIN_GOCE LSG
    INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO=LSG.ID_EMPLEADO_FK  
    INNER JOIN RH_NOMN_PLAZAS PLAZA ON PLAZA.ID_PLAZA=EMP.ID_PLAZA
    WHERE TRUNC(TO_DATE(LSG.FECHA_FIN_LICENCIA,'DD/MM/YYYY')) <= TRUNC(TO_DATE(SYSDATE,'DD/MM/YYYY'))
    AND LSG.ESTATUS = 'A'
    AND EMP.ID_EMPLEADO = DECODE(idEmpleado,-1, EMP.ID_EMPLEADO,idEmpleado)
    ;
    
    --DBMS_OUTPUT.put_line(' LICENCIAS A EJECUTAR: ' || NUMERO_LICENCIAS|| ' ');
  
    --EXISTEN LICENCIAS SIN GOCE que han concluido y hay que hacer el reingreso del empleado.
    IF NUMERO_LICENCIAS > 0 THEN            
            
      FOR R1 IN C_LICENCIAS_APLICADAS LOOP              

        ---INACTIVAMOS LOS INTERINATOS PENDIENTES A FECHAS FUTURAS 
          
          UPDATE RH_NOMN_INTERINATOS SET ESTATUS = 'I'
          WHERE ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
          AND ESTATUS = 'P';
          
      
        -- VALIDAR SI LA PLAZA ESTA POR INTERINATO CON UN EMPLEADO DE PLAZA CONGELADA.
        SELECT COUNT(ID_EMPLEADO_FK_INTERINATO)
        INTO vIDEMPINTPLAZACONG 
        FROM RH_NOMN_INTERINATOS 
        WHERE ESTATUS = 'A'
        AND ID_PLAZA_FK_INICIAL IS NOT NULL
        AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
        ;                
    
        -- VALIDAR SI LA PLAZA ESTA POR INTERINATO CON UN EMPLEADO DE NUEVO INGRESO.
        SELECT COUNT(ID_EMPLEADO_FK_INTERINATO)
        INTO vIDEMPINTEMPNVO 
        FROM RH_NOMN_INTERINATOS 
        WHERE ESTATUS = 'A'
        AND ID_PLAZA_FK_INICIAL IS NULL
        AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
        ;                   
            
        /***** VALIDAR POR QUE PROCESO SE HAR� EL T�RMINO DE LICENCIA. *****/        
        -- PROCESO POR INTERINATO CON EMPLEADO NUEVO.
        IF vIDEMPINTEMPNVO <> 0 THEN
        
          --DBMS_OUTPUT.put_line(' ENTRO POR INTERINATO DE NUEVO EMPLEADO, ID_EMLEADO: ' || vIDEMPINTEMPNVO);
          
          SELECT ID_EMPLEADO_FK_INTERINATO
          INTO vIDEMPINTEMPNVO 
          FROM RH_NOMN_INTERINATOS 
          WHERE ESTATUS = 'A'
          AND ID_PLAZA_FK_INICIAL IS NULL
          AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
          ;          
        
          UPDATE RH_NOMN_INTERINATOS 
          SET FECHA_FIN_INTERINATO = R1.FECHA_FIN_LICENCIA-1
          WHERE ESTATUS = 'A'
          AND ID_PLAZA_FK_INICIAL IS NULL
          AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
          ;           
        COMMIT;
        
          -- EJECUTAR SP PARA BAJA DE INTERINATO AL EMPLEADO DE NUEVO INGRESO.
          vSALIDAFNINTERINATO := FN_EJE_SP_INTERINATOS_FIN(vIDEMPINTEMPNVO);
          COMMIT;
        
        -- PROCESO POR INTERINATO CON EMPLEADO CON PLAZA CONGELADA.
        ELSIF vIDEMPINTPLAZACONG <> 0 THEN
        
          --DBMS_OUTPUT.put_line(' ENTRO POR INTERINATO EMPLEADO CON PLAZA CONGELADA, ID_EMLEADO: ' || vIDEMPINTPLAZACONG);  
          
          
          
          SELECT ID_EMPLEADO_FK_INTERINATO
          INTO vIDEMPINTPLAZACONG 
          FROM RH_NOMN_INTERINATOS 
          WHERE ESTATUS = 'A'
          AND ID_PLAZA_FK_INICIAL IS NOT NULL
          AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA
          ;          
        
          --ACTUALIZAMOS LA FECHA DE TERMINO DEL INTERINATO
          UPDATE RH_NOMN_INTERINATOS 
          SET FECHA_FIN_INTERINATO = R1.FECHA_FIN_LICENCIA
          WHERE ESTATUS = 'A'
          AND ID_PLAZA_FK_INICIAL IS NOT NULL
          AND ID_PLAZA_FK_INTERINATO = R1.ID_PLAZA;
        COMMIT;
        
        
          -- EJECUTAR SP PARA CAMBIO DE PUESTO DEL EMPLEADO CON PLAZA CONGELADA.
          vSALIDAFNINTERINATO := FN_EJE_SP_INTERINATOS_FIN(vIDEMPINTPLAZACONG);   
          COMMIT;
        
        -- PLAZA TEMPORAL SIN INTERINATO.
        ELSE
        
          --DBMS_OUTPUT.put_line(' ENTRO PLAZA TEMPORAL SIN INTERINATO.');  
          vSALIDAFNINTERINATO := 1;
          
        END IF;

        
        IF vSALIDAFNINTERINATO = 1 THEN
        
          /***** EJECUTAR EL PROCESO DE REINGRESO DEL EMPLEADO CON LICENCIA *****/ 
          -- Este proceso aplica para todos los procesos con y sin interinato.
          
          --carga informacion del empleado QUE REINGRESA A SU PLAZA POR LICENCIA SIN GOCE DE SUELDO.
          SELECT 
            ID_EMPLEADO, 
            ID_DATOS_PERSONALES, 
            ID_DATOS_BANCARIOS,
            ID_PLAZA, 
            ID_ESTATUS_EMPLEADO, 
            ID_TIPO_PAGO, 
            NUMERO_EMPLEADO,
            FECHA_DE_INGRESO, 
            FECHA_MODIFICACION, 
            FECHA_BAJA, 
            ID_CAUSA_BAJA, 
            NSS, 
            QUINQUENIO,
            VALOR_QUINQUENIO
          INTO 
            vID_EMPLEADO,
            vID_DATOS_PERSONALES,
            vID_DATOS_BANCARIOS,
            vID_PLAZA,
            vID_ESTATUS_EMPLEADO,
            vID_TIPO_PAGO,
            vNUMERO_EMPLEADO,
            vFECHA_DE_INGRESO,
            vFECHA_MODIFICACION,
            vFECHA_BAJA,
            vID_CAUSA_BAJA,
            vNSS,
            vQUINQUENIO,
            vVALOR_QUINQUENIO
          FROM 
            RH_NOMN_EMPLEADOS 
          WHERE 
            ID_EMPLEADO = R1.ID_EMPLEADO_FK
          ;     
        
          --obtener secuencia de id_empleado
          SELECT MAX(ID_EMPLEADO) + 1 
          INTO SEC_ID_EMPLEADO 
          FROM RH_NOMN_EMPLEADOS
          ;  
        
          --grabar un nuevo empleado que ES EL REGRESO DEL EMPLEADO QUE TENIA LICENCIA SIN GOCE DE SUELDO.
          INSERT INTO RH_NOMN_EMPLEADOS
          (
            ID_EMPLEADO, 
            ID_DATOS_PERSONALES, 
            ID_DATOS_BANCARIOS,
            ID_PLAZA, 
            ID_ESTATUS_EMPLEADO, 
            ID_TIPO_PAGO, 
            NUMERO_EMPLEADO,
            FECHA_DE_INGRESO, 
            FECHA_MODIFICACION, 
            FECHA_BAJA, 
            ID_CAUSA_BAJA, 
            NSS,
            QUINQUENIO,
            VALOR_QUINQUENIO
          )
          VALUES
          (
            SEC_ID_EMPLEADO,
            vID_DATOS_PERSONALES, 
            vID_DATOS_BANCARIOS,
            vID_PLAZA, 
            1, 
            vID_TIPO_PAGO, 
            vNUMERO_EMPLEADO,
            R1.FECHA_FIN_LICENCIA+1,
            SYSDATE, 
            null, 
            null, 
            vNSS,
            vQUINQUENIO,
            vVALOR_QUINQUENIO
            
          )
          ;         
    
          -- Actualiza al empleado con Licencia como activo y su fecha de ingreso (reingreso).
          UPDATE RH_NOMN_EMPLEADOS  
          SET ID_ESTATUS_EMPLEADO   = 4,
              FECHA_MODIFICACION    = SYSDATE                        
          WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK
          ;
    
          -- Actualiza el estatus de la plaza a ocupada.
          UPDATE RH_NOMN_PLAZAS  
          SET ID_ESTATUS_PLAZA_FK     = 2 ,
              FECHA_DE_MODIFICACION   = SYSDATE                               
          WHERE ID_PLAZA                = R1.ID_PLAZA
          ;    

          /* LICENCIA SIN GOCE DE SUELDO SE DEBE INACTIVAR */
              UPDATE RH_NOMN_LICENCIAS_SIN_GOCE
              SET ESTATUS = 'I'
              WHERE ID_LICENCIA_SIN_GOCE = R1.ID_LICENCIA_SIN_GOCE
              ;
      
            ---- SI EL SE EJECUTA POR JOB EL IDEMPLEADO ES -1 DE LO CONTRARIO EL SEGURO DE RETIRO SE DA DE ALTA POR APLICATIVO 
            IF idEmpleado = -1 THEN
                INSERT INTO RH_NOMN_SEGURO_RETIRO
                SELECT (SELECT NVL(MAX(ID_SEGURO_RETIRO),0)+1 FROM RH_NOMN_SEGURO_RETIRO ) ID_SEGURO,
                    IMPORTE_NON_EMP,
                    IMPORTE_PAR_EMP,
                    IMPORTE_NON_DEP,
                    IMPORTE_PAR_DEP,
                    SYSDATE,
                    'A',
                    SEC_ID_EMPLEADO,
                    DECODE( (SELECT UPPER(NOMN.DESCRIPCION)
                             FROM RH_NOMN_PLAZAS PLZ
                             INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOMN ON NOMN.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
                             WHERE ID_PLAZA = vID_PLAZA)
                           ,'ESTRUCTURA', (SELECT ID_CONCEPTO
                                           FROM RH_NOMN_CAT_CONCEPTOS
                                           WHERE TRIM(NOMBRE_CONCEPTO) = 'SEGURO DE RETIRO')
                           ,'EVENTUAL',   (SELECT ID_CONCEPTO 
                                           FROM RH_NOMN_CAT_CONCEPTOS
                                           WHERE TRIM(NOMBRE_CONCEPTO) = 'SEGURO DE RETIRO PERSONAL EVENTUAL')) CONCEPTO
                FROM 
                (
                  SELECT CLAVE_ARGUMENTO,VALOR_CONSTANTE
                  FROM RH_NOMN_CAT_ARGUMENTOS
                  WHERE CLAVE_ARGUMENTO IN (':SEGRETNONEMP',':SEGRETPAREMP',':SEGRETNONDEP',':SEGRETPARDEP')
                
                )
                PIVOT 
                (
                  SUM(VALOR_CONSTANTE)
                  FOR CLAVE_ARGUMENTO IN (':SEGRETNONEMP' IMPORTE_NON_EMP,':SEGRETPAREMP' IMPORTE_PAR_EMP,':SEGRETNONDEP' IMPORTE_NON_DEP,':SEGRETPARDEP' IMPORTE_PAR_DEP)
                );
            
            END IF;
      
      
      
          /*HISTORICO MOVIMIENTOS EMPLADO*/   
          SELECT 
            (MAX(ID_HISTORICO_MOVIMIENTO) + 1) 
          INTO 
            VMAXHMOVS 
          FROM 
            RH_NOMN_BIT_HISTORICOS_MOVS
          ;
          
          --REGISTRO DE REINGRESO DE LICENCIA
          INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
          (
            ID_HISTORICO_MOVIMIENTO, 
            ID_ACCION_FK, 
            ID_EJERCICIO_FK,
            FECHA, 
            ID_USUARIO_FK, 
            TABLA
          ) 
          VALUES 
          ( 
            VMAXHMOVS, 
            14, 
            VIDANIO,
            R1.FECHA_FIN_LICENCIA+1,
            1, 
            ''
          )
          ;
                 
          /*HISTORICO CAMPOS EMPLEADO*/    
          SELECT 
            (MAX(ID_HISTORICO_CAMPO ) + 1) 
          INTO 
            VMAXHCAMP 
          FROM 
            RH_NOMN_BIT_HISTORICOS_CAMPOS
          ;
          
          --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
          SELECT  
            (MAX(ID_HISTORICO_MOVIMIENTO_FK )) 
          INTO 
            VMAXMOVE 
          FROM 
            RH_NOMN_BIT_HISTORICOS_CAMPOS 
          WHERE  
            ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO_FK 
          ;
        
          INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
          (
            ID_HISTORICO_CAMPO, 
            ID_HISTORICO_MOVIMIENTO_FK, 
            ID_REGISTRO_ENTIDAD, 
            ID_MOVIMIENTO_ANTERIOR, 
            ID_CAMPO_AFECTADO_FK, 
            VALOR_ACTUAL, 
            VALOR_BUSQUEDA
          ) 
          VALUES 
          ( 
            VMAXHCAMP,
            VMAXHMOVS,
            SEC_ID_EMPLEADO,
            VMAXMOVE,
            10,
            1,
            R1.NUMERO_EMPLEADO
          )
          ;
      
          --LIMPIAR EL CAMPO DE LA FECHA DE LA BAJA      
          SELECT 
            (MAX(ID_HISTORICO_CAMPO ) + 1) 
          INTO 
            VMAXHCAMP 
          FROM 
            RH_NOMN_BIT_HISTORICOS_CAMPOS
          ;
          
          INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
          (
            ID_HISTORICO_CAMPO, 
            ID_HISTORICO_MOVIMIENTO_FK, 
            ID_REGISTRO_ENTIDAD, 
            ID_MOVIMIENTO_ANTERIOR, 
            ID_CAMPO_AFECTADO_FK, 
            VALOR_ACTUAL, 
            VALOR_BUSQUEDA
          ) 
          VALUES 
          ( 
            VMAXHCAMP ,
            VMAXHMOVS,
            SEC_ID_EMPLEADO,
            VMAXMOVE,
            25,
            ' ',
            R1.NUMERO_EMPLEADO
          )
          ;
        
          --MOVIMIENTO A LA PLAZA QUEDA CON ESTATUS ocupada.       
          SELECT 
            (MAX(ID_HISTORICO_CAMPO ) + 1) 
          INTO 
            VMAXHCAMP 
          FROM 
            RH_NOMN_BIT_HISTORICOS_CAMPOS
          ;                          
          
          INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
          (
            ID_HISTORICO_CAMPO, 
            ID_HISTORICO_MOVIMIENTO_FK, 
            ID_REGISTRO_ENTIDAD, 
            ID_MOVIMIENTO_ANTERIOR, 
            ID_CAMPO_AFECTADO_FK, 
            VALOR_ACTUAL, 
            VALOR_BUSQUEDA
          ) 
          VALUES 
          ( 
            VMAXHCAMP,
            VMAXHMOVS,
            R1.ID_PLAZA,
            VMAXMOVE,
            5,
            2,
            R1.ID_PLAZA
          )
          ;
        
          COMMIT;
          
          --DBMS_OUTPUT.PUT_LINE('PROCESO REINGRESO DE LICENCIA PARA EL EMPLEADO : '|| R1.ID_EMPLEADO_FK
          --||' PLAZA: ' ||R1.ID_PLAZA||' CON FECHA DE FIN DE LICENCIA: ' ||R1.FECHA_INICIO_LICENCIA);
        
        ELSE
        
          DBMS_OUTPUT.PUT_LINE(' HUBO UN ERROR EN EL SP DE INTERINATO FIN CON ID_EMPLEADO: ' || R1.ID_EMPLEADO_FK);
        END IF;
    
      END LOOP;
      COMMIT;
  
    --ELSE
    
      DBMS_OUTPUT.PUT_LINE('NO HAY LICENIAS QUE PROCESAR.');
    END IF;
  
  EXCEPTION  
  WHEN NO_DATA_FOUND THEN  
    NULL;
  WHEN OTHERS THEN   
  ROLLBACK;
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
    DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_LICENCIA_SIN_GOCE_INICIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_LICENCIA_SIN_GOCE_INICIO" 
IS
PRAGMA AUTONOMOUS_TRANSACTION;

  ERR_CODE       INTEGER;
  ERR_MSG        VARCHAR2(250);
  NUMERO_LICENCIAS INTEGER;
  NUMERO_SEGUROS INTEGER;
  VIDANIO        INTEGER;
  VIDQUI         INTEGER;
  FECHA_INI_QUIN DATE;
  FECHA_FIN_QUIN DATE;
  VMAXHMOVS      INTEGER;
  VMAXHCAMP      INTEGER;
  VMAXMOVE       INTEGER;
  VMAXMOVP       INTEGER;
  PFECHA         VARCHAR2(250);
  vcal           FLOAT;
  DIF_DIAS       INTEGER;
  
  CURSOR C_LICENCIAS_PENDIENTES IS
  SELECT LSG.ID_LICENCIA_SIN_GOCE, ID_EMPLEADO_FK,EMP.NUMERO_EMPLEADO,PLAZA.ID_PLAZA,LSG.FECHA_INICIO_LICENCIA
  FROM RH_NOMN_LICENCIAS_SIN_GOCE LSG
  INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO=LSG.ID_EMPLEADO_FK  
  INNER JOIN RH_NOMN_PLAZAS PLAZA ON PLAZA.ID_PLAZA=EMP.ID_PLAZA
  WHERE TO_DATE(LSG.FECHA_INICIO_LICENCIA,'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY')
  AND LSG.ESTATUS='P';
  
  CURSOR C_SEGUROS IS
  SELECT SEGURO.ID_SEGURO,
         SEGURO.ID_EMPLEADO_FK,
         SEGURO.ID_CONCEPTO_FK,
         SEGURO.IMPORTE,
         SEGURO.IMPORTE_PRIMER_PAGO,
         SEGURO.IMPORTE_PAGO,
         LSG.FECHA_INICIO_LICENCIA
  FROM RH_NOMN_SEGUROS SEGURO
  INNER JOIN RH_NOMN_LICENCIAS_SIN_GOCE LSG ON LSG.ID_EMPLEADO_FK=SEGURO.ID_EMPLEADO_FK
  WHERE TO_DATE(LSG.FECHA_INICIO_LICENCIA,'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY') AND LSG.ESTATUS='P' AND SEGURO.ACTIVO='A';

    
BEGIN

    SELECT  ID_EJERCICIO_FK ,
            ID_QUINCENA,
            FECHA_INICIO_QUINCENA,
            FECHA_FIN_QUINCENA  
            INTO VIDANIO , VIDQUI,FECHA_INI_QUIN,FECHA_FIN_QUIN
    FROM RH_NOMN_CAT_QUINCENAS
    WHERE ACTIVO=1 
    AND TO_DATE(SYSDATE, 'DD/MM/YYYY') BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YYYY') AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YYYY');
    
    SELECT  COUNT(*) INTO NUMERO_LICENCIAS 
    FROM RH_NOMN_LICENCIAS_SIN_GOCE 
    WHERE TO_DATE(FECHA_INICIO_LICENCIA,'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY')
    AND ESTATUS='P';
    
    SELECT COUNT(*) INTO NUMERO_SEGUROS
    FROM RH_NOMN_SEGUROS SEGURO
    INNER JOIN RH_NOMN_LICENCIAS_SIN_GOCE LSG ON LSG.ID_EMPLEADO_FK = SEGURO.ID_EMPLEADO_FK
    WHERE TO_DATE(LSG.FECHA_INICIO_LICENCIA,'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY') 
    AND LSG.ESTATUS='P' 
    AND SEGURO.ACTIVO='A';
  
  IF NUMERO_LICENCIAS > 0 THEN  --EXISTEN LICENCIAS SIN GOCE POR APLICAR
  
    --DBMS_OUTPUT.put_line ('entra flujo');
    --PROPORCIONALIDAD DE DEDUCCION DE SEGUROS.    
    FOR LR2 IN C_SEGUROS
    LOOP
        SELECT TRUNC(TO_DATE(LR2.FECHA_INICIO_LICENCIA,'DD/MM/YYYY')) - TRUNC(TO_DATE(FECHA_INI_QUIN,'DD/MM/YYYY'))+1 
        INTO DIF_DIAS
        FROM DUAL;
    
    
      VCAL:=FN_PROPO_IMP_SEG_POR_DIAS(DIF_DIAS,LR2.IMPORTE,1);
      UPDATE RH_NOMN_SEGUROS SET IMPORTE_PAGO=VCAL WHERE ID_SEGURO=LR2.ID_SEGURO;
      --DBMS_OUTPUT.put_line('PROCESO DE PROPORCIONALIDAD DE SEGUROS : '|| LR2.ID_EMPLEADO_FK );
    END LOOP;
    COMMIT;
    
    FOR R1 IN C_LICENCIAS_PENDIENTES
    LOOP
    /*ACTUALIZA EL ESTATUS DE LICENCIA SIN GOCE A 'APLICADO'*/
    UPDATE RH_NOMN_LICENCIAS_SIN_GOCE SET ESTATUS='A' WHERE ID_LICENCIA_SIN_GOCE=R1.ID_LICENCIA_SIN_GOCE;
    
    /*LICENCIA EMPELADO*/     -- SE DEBE DE ACTUALIZAR EL VALOR DE LA FECHA DE BAJA CON EL VALOR DE LA FECHA DEL INICIO DE LICENCIA????
    --se actualiza la fecha de baja
    UPDATE RH_NOMN_EMPLEADOS  SET ID_ESTATUS_EMPLEADO  = 3,
                              FECHA_MODIFICACION   = SYSDATE,
                              ID_CAUSA_BAJA=3,
                              FECHA_BAJA=R1.FECHA_INICIO_LICENCIA-1
    WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK;

    
    /*LICENCIA PLAZA*/        
    UPDATE RH_NOMN_PLAZAS  SET ID_ESTATUS_PLAZA_FK    = 5 ,
                               FECHA_DE_MODIFICACION = SYSDATE
    WHERE ID_PLAZA = R1.ID_PLAZA;    
    
    /*LICENCIA PENSION*/      -- LAS PENSIONES SI SE INACTIVAN.? Arnol: queda activo.
    UPDATE  RH_NOMN_PENSIONES SET ID_ANIO_FIN       = VIDANIO ,
                                  ID_QUINCENA_FIN   = VIDQUI                                   
    WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK
    AND ESTATUS = 'A';      
    
    /*LICENCIA MANUALESYT*/   -- LOS MANUALES Y TERCEROS TAMBIEN SE INACTIVAN?Arnol: queda activo.
    UPDATE  RH_NOMN_MANUALES_TERCEROS  SET FECHA_FIN = (SELECT FECHA_FIN_QUINCENA 
                                                        FROM RH_NOMN_CAT_QUINCENAS 
                                                        WHERE TO_DATE(R1.FECHA_INICIO_LICENCIA,'DD/MM/YY') 
                                                              BETWEEN TO_DATE(FECHA_INICIO_QUINCENA,'DD/MM/YY') AND TO_DATE(FECHA_FIN_QUINCENA,'DD/MM/YY') ),
                                           FECHA_MODIFICACION   = SYSDATE
    WHERE ID_EMPLEADO = R1.ID_EMPLEADO_FK
    AND    ACTIVO = 'A';
                                    
    /*HISTORICO MOVIMIENTOS EMPLADO*/   
    SELECT (MAX(ID_HISTORICO_MOVIMIENTO)+1) 
    INTO VMAXHMOVS 
    FROM RH_NOMN_BIT_HISTORICOS_MOVS;
      --OK
    INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS (
                                             ID_HISTORICO_MOVIMIENTO, 
                                             ID_ACCION_FK, 
                                             ID_EJERCICIO_FK,
                                             FECHA, 
                                             ID_USUARIO_FK, 
                                             TABLA) 
    VALUES ( VMAXHMOVS, 
             13, 
             VIDANIO, 
             R1.FECHA_INICIO_LICENCIA, 
             1, 
             '');   
                                                          
      
    
  /*HISTORICO CAMPOS EMPLEADO*/   
    --SELECT TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') INTO PFECHA FROM DUAL;
    SELECT FECHA_FIN_LICENCIA INTO PFECHA
    FROM RH_NOMN_LICENCIAS_SIN_GOCE 
    WHERE ID_EMPLEADO_FK = R1.ID_EMPLEADO_FK;
    
    SELECT (MAX(ID_HISTORICO_CAMPO )+1) 
    INTO VMAXHCAMP 
    FROM RH_NOMN_BIT_HISTORICOS_CAMPOS; 
    
    
    --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
    SELECT  (MAX(ID_HISTORICO_MOVIMIENTO_FK )) 
    INTO VMAXMOVE 
    FROM RH_NOMN_BIT_HISTORICOS_CAMPOS 
    WHERE  ID_REGISTRO_ENTIDAD =  R1.ID_EMPLEADO_FK ;
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
                                                  ID_HISTORICO_CAMPO, 
                                                  ID_HISTORICO_MOVIMIENTO_FK, 
                                                  ID_REGISTRO_ENTIDAD, 
                                                  ID_MOVIMIENTO_ANTERIOR, 
                                                  ID_CAMPO_AFECTADO_FK, 
                                                  VALOR_ACTUAL, 
                                                  VALOR_BUSQUEDA) 
      VALUES ( VMAXHCAMP ,
              VMAXHMOVS,
              R1.ID_EMPLEADO_FK,
              VMAXMOVE,
              10,
              3,
              R1.NUMERO_EMPLEADO);
                                
      --VERIFICAR SI EL CAMPO DE LA FECHA DE BAJA DEBE SER MODIFICADO CON LA FECHA DE LA LICENCIA  ??????                              
      --se debe registrar la fecha de la baja
      SELECT (MAX(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP 
      FROM RH_NOMN_BIT_HISTORICOS_CAMPOS;                                                 
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
                                                ID_HISTORICO_CAMPO, 
                                                ID_HISTORICO_MOVIMIENTO_FK, 
                                                ID_REGISTRO_ENTIDAD, 
                                                ID_MOVIMIENTO_ANTERIOR, 
                                                ID_CAMPO_AFECTADO_FK, 
                                                VALOR_ACTUAL, 
                                                VALOR_BUSQUEDA) 
      VALUES ( VMAXHCAMP ,
              VMAXHMOVS,
              R1.ID_EMPLEADO_FK,
              VMAXMOVE,
              25,
              PFECHA,
              R1.NUMERO_EMPLEADO);
              
      --MOVIENTO A LA PLAZA QUEDA CON ESTATUS TEMPORAL.  ----OK      
      SELECT (MAX(ID_HISTORICO_CAMPO )+1) INTO VMAXHCAMP 
      FROM RH_NOMN_BIT_HISTORICOS_CAMPOS;                          
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS (
                                                  ID_HISTORICO_CAMPO, 
                                                  ID_HISTORICO_MOVIMIENTO_FK, 
                                                  ID_REGISTRO_ENTIDAD, 
                                                  ID_MOVIMIENTO_ANTERIOR, 
                                                  ID_CAMPO_AFECTADO_FK, 
                                                  VALOR_ACTUAL, 
                                                  VALOR_BUSQUEDA) 
                     VALUES ( VMAXHCAMP ,
                              VMAXHMOVS,
                              R1.ID_PLAZA,
                              VMAXMOVE,
                              5,
                              5,
                              R1.ID_PLAZA);
                              
      
    
       --DBMS_OUTPUT.PUT_LINE('PROCESO DE LICENCIA PARA LOS EMPLEADOS : '|| R1.ID_EMPLEADO_FK
         --   ||' PLAZA: ' ||R1.ID_PLAZA||' CON FECHA DE INICIO DE LICENCIA: ' ||R1.FECHA_INICIO_LICENCIA);                                                               
    
    END LOOP;
    COMMIT;
    
  END IF;
  
  EXCEPTION  
    WHEN NO_DATA_FOUND THEN  
      NULL;
    WHEN OTHERS THEN   
    ROLLBACK;
      err_code := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 200);
      DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
      DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_OBTEN_QUINCENAS_SERICA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_OBTEN_QUINCENAS_SERICA" (IDEJERCICIO IN NUMBER, IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR)IS 

BEGIN

OPEN DATOS FOR
        SELECT MAX(ID_NOMINA)ID_NOMINA,QUINCENA,EJERCICIO,TIPO_NOMINA
        FROM
        (
            SELECT   
            N.ID_NOMINA
            ,UPPER(Q.DESCRIPCION_QUINCENA) QUINCENA
            ,E.VALOR EJERCICIO
            ,'ORDINARIA' TIPO_NOMINA
            FROM RH_NOMN_CABECERAS_NOMINAS N
            LEFT JOIN RH_NOMN_CAT_EJERCICIOS E ON E.ID_EJERCICIO = N.ID_EJERCICIO_FK
            LEFT JOIN RH_NOMN_CAT_QUINCENAS Q ON Q.ID_QUINCENA = N.ID_QUINCENA_FK
            LEFT JOIN RH_NOMN_CAT_TIPOS_NOMINA T ON T.ID_TIPO_NOMINA = N.ID_TIPO_NOMINA_FK
            WHERE N.ID_ESTATUS_NOMINA_FK IN (3)
            AND   E.ID_EJERCICIO =  IDEJERCICIO
            AND   Q.ID_QUINCENA = IDQUINCENA
            AND N.ID_TIPO_NOMINA_FK IN (1,2)
            AND N.ID_NOMINA_PADRE IS NULL

        )GROUP BY QUINCENA,EJERCICIO,TIPO_NOMINA

        UNION 

        SELECT   
                    N.ID_NOMINA
                    ,UPPER(Q.DESCRIPCION_QUINCENA) QUINCENA
                    ,E.VALOR EJERCICIO
                    ,'EXTRAORDINARIA' TIPO_NOMINA
                    FROM RH_NOMN_CABECERAS_NOMINAS N
                    LEFT JOIN RH_NOMN_CAT_EJERCICIOS E ON E.ID_EJERCICIO = N.ID_EJERCICIO_FK
                    LEFT JOIN RH_NOMN_CAT_QUINCENAS Q ON Q.ID_QUINCENA = N.ID_QUINCENA_FK
                    LEFT JOIN RH_NOMN_CAT_TIPOS_NOMINA T ON T.ID_TIPO_NOMINA = N.ID_TIPO_NOMINA_FK
                    WHERE N.ID_ESTATUS_NOMINA_FK IN (3)
                    AND N.RETROACTIVIDAD = 1
                    AND N.ID_NOMINA_PADRE IN (
                                                SELECT   
                                                N.ID_NOMINA
                                                FROM RH_NOMN_CABECERAS_NOMINAS N
                                                LEFT JOIN RH_NOMN_CAT_EJERCICIOS E ON E.ID_EJERCICIO = N.ID_EJERCICIO_FK
                                                LEFT JOIN RH_NOMN_CAT_QUINCENAS Q ON Q.ID_QUINCENA = N.ID_QUINCENA_FK
                                                LEFT JOIN RH_NOMN_CAT_TIPOS_NOMINA T ON T.ID_TIPO_NOMINA = N.ID_TIPO_NOMINA_FK
                                                WHERE N.ID_ESTATUS_NOMINA_FK IN (3)
                                                AND   E.ID_EJERCICIO =  ID_EJERCICIO
                                                AND   Q.ID_QUINCENA = IDQUINCENA
                                                AND   N.ID_TIPO_NOMINA_FK IN (1,2)
                                                AND   N.ID_NOMINA_PADRE IS NULL 

                                                );
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);        

END SP_OBTEN_QUINCENAS_SERICA;

/
--------------------------------------------------------
--  DDL for Procedure SP_OBTN_IMPORTE_NEGATIVOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_OBTN_IMPORTE_NEGATIVOS" (IDQUINCENA IN NUMBER, IDTIPONOMINA IN NUMBER, ESTATUSNOMINA IN NUMBER, DATOS OUT SYS_REFCURSOR) IS
v_QUERY VARCHAR2(3000);
BEGIN


v_QUERY := '
    
    SELECT  EM.NUMERO_EMPLEADO,
            DAT.NOMBRE ||'' ''||DAT.A_PATERNO||'' ''||DAT.A_MATERNO NOMBRE,
            UNI.CLAVE_UNIADM CLAVE_UNIDAD,
            EMPLZ.IMPORTE_TOTAL TOTAL_NETO
    FROM RH_NOMN_CABECERAS_NOMINAS NOMN ';
IF ESTATUSNOMINA = 1 OR ESTATUSNOMINA = 2 THEN
    v_QUERY := v_QUERY||'
    LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 EMPLZ ON EMPLZ.ID_NOMINA01_FK    = NOMN.ID_NOMINA
';
ELSE 
    v_QUERY := v_QUERY||'
    LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA EMPLZ ON EMPLZ.ID_NOMINA_FK    = NOMN.ID_NOMINA
    ';
END IF;
    v_QUERY := v_QUERY||'
    LEFT JOIN RH_NOMN_EMPLEADOS              EM    ON EM.ID_EMPLEADO          = EMPLZ.ID_EMPLEADO_FK
    LEFT JOIN RH_NOMN_DATOS_PERSONALES       DAT   ON DAT.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
    LEFT JOIN RH_NOMN_PLAZAS                 PLZ   ON PLZ.ID_PLAZA            = EM.ID_PLAZA
    LEFT JOIN RH_NOMN_ADSCRIPCIONES          ADS   ON ADS.ID_ADSCRIPCION      = PLZ.ID_ADSCRIPCION_FK
    LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS    UNI   ON UNI.ID_UNIADM           = ADS.ID_UNIADM_FK
    LEFT JOIN RH_NOMN_CAT_QUINCENAS          QUIN  ON QUIN.ID_QUINCENA        = NOMN.ID_QUINCENA_FK
    WHERE QUIN.ID_QUINCENA = '||IDQUINCENA||'
    AND   NOMN.ID_TIPO_NOMINA_FK = '||IDTIPONOMINA||'
    AND EMPLZ.IMPORTE_TOTAL <= 0';
 --DBMS_OUTPUT.PUT_LINE(v_QUERY);
 OPEN DATOS FOR v_QUERY;   
    
END SP_OBTN_IMPORTE_NEGATIVOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_PARAMETROSFORMULA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_PARAMETROSFORMULA" (prmIdConcepto in VARCHAR2,
                                                   p_Cursor OUT TYPES.CURSOR_TYPE)
--RETURN SYS_REFCURSOR 
IS  
  --v_Cursor SYS_REFCURSOR;
  
v_formula VARCHAR2(150);
v_idformula integer;
--vqcursor varchar2(8000);

BEGIN
       
    v_formula :='';
    v_idformula := 0;
    --vqcursor:='';
    
    Select CF.FORMULA  into v_formula from RH_NOMN_CAT_CONCEPTOS CC 
    INNER JOIN RH_NOMN_CAT_FORMULAS CF ON CC.ID_FORMULA_FK= CF.ID_FORMULA
    WHERE CC.CLAVE_CONCEPTO=prmIdConcepto;
    

  SELECT ID_formula into v_idformula FROM RH_NOMN_CAT_FORMULAS WHERE FORMULA = v_formula;
     
  delete from T_PASO;
   commit;
  delete from T_PASO2
  commit;
 
  FOR R1 in (SELECT ID_ARGUMENTO FROM  RH_NOMN_CAT_ARG_FORMULAS where ID_formula = v_idformula)
  loop
    INSERT INTO T_PASO ( SELECT FUNCION_ORACLE FROM RH_NOMN_CAT_ARGUMENTOS WHERE ID_ARGUMENTO = R1.ID_ARGUMENTO);
    
  END LOOP;
  commit;

  FOR R2 IN (SELECT VALOR FROM t_PASO WHERE  VALOR IS NOT NULL)
   LOOP
    INSERT INTO T_PASO2 ( select distinct argument_name , position from  all_arguments 
                     where object_id in ( select object_id from all_objects where object_name = R2.VALOR) 
                     and   argument_name is not null
                     );
    END LOOP;
   commit;
  
   -- vqcursor:= 'select VALOR FROM(SELECT DISTINCT VALOR,valor2 FROM  T_PASO2  where valor is not null order by 2 asc)';
    open p_Cursor for  select VALOR FROM(SELECT DISTINCT VALOR,valor2 FROM  T_PASO2  where valor is not null order by 2 asc);--vqcursor;
    
     --return v_Cursor;
     
 EXCEPTION  
       WHEN NO_DATA_FOUND THEN NULL;  
       WHEN OTHERS THEN ROLLBACK;
    
 end SP_PARAMETROSFORMULA ;

/
--------------------------------------------------------
--  DDL for Procedure SP_PUESTOS_DE_PLAZAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_PUESTOS_DE_PLAZAS" 
(
  INidAdscripcion IN NUMBER 
, FECHADISPONIBLE IN DATE 
, V_CURSOR OUT SYS_REFCURSOR
) 
AS 
err_code NUMBER;
err_msg VARCHAR(300);

BEGIN 

  OPEN V_CURSOR FOR
SELECT
      ID_PLAZA,
      ID_TABULADOR_FK,
      ID_ADSCRIPCION_FK,
      ID_PUESTO_FK,
      ID_TIPO_NOMBRAM_FK,
      ID_ESTRUCTURA_FK,
      ID_ESTATUS_PLAZA_FK,
      ID_EJERCICIO_FK,
      CLAVE_PLAZA,
      PEF,
      MAX(VIGENCIA_DE_INICIO)VIGENCIA_DE_INICIO,
      MAX(VIGENCIA_FINAL) VIGENCIA_FINAL,
      FECHA_DE_CREACION,
      FECHA_DE_MODIFICACION,
      FECHA_DE_ELIMINACION,
      COMENTARIO,
      NUMERO_PLAZA,
      PROPIETARIO,
      CLAVE_PUESTO CLAVE_PUESTO_PUE,
      DESCRIPCION DESCRIPCION_PUE,
      DIRECCION_GENERAL DIRECCION_GENERAL_PUE,
      CLAVE_ADSCR_PUESTO CLAVE_ADSCR_PUESTO_PUE,
      ID_TABULADOR ID_TABULADOR,
      SUELDO_BASE,
      COMPENSACION_GARANTIZADA,
      DESC_UNI DESCRIPCION_UNI,
      CLAVE_UNIADM CLAVE_UNIADM_UNI,
      CLAVE_JERARQUICA CLAVE_JERARQUICA_GRUP,
      GRADO GRADO_GRAD,
      NIVEL NIVEL_NIV,
      ID_ESTRUCTURA ID_ESTRUCTURA_EST,
      ID_ADSCRIPCION ID_ADSCRIPCION_ADS,
      CLAVE_ADSCRIPCION CLAVE_ADSCRIPCIONN_ADS,
      DESCRIPCION_ADS DESCRIPCIONN_ADS,
      ID_TIPO_NOMBRAMIENTO ID_TIPO_NOMBRAMIENTO_TIPNOM,      
      DECODE(ID_ESTATUS_PLAZA_FK, 1, 'Vacante', 2, 'Ocupada', 5, 'Temporal') DESCRIPCION_ESPLA,
      NOMBRAMIENTO DESCRIPCION_TIPNOM
FROM
(

        ---plazas que nunca se han ocupado 
  SELECT
        PLA.ID_PLAZA,
        PLA.ID_TABULADOR_FK,
        PLA.ID_ADSCRIPCION_FK,
        PLA.ID_PUESTO_FK,
        PLA.ID_TIPO_NOMBRAM_FK,
        PLA.ID_ESTRUCTURA_FK,
        PLA.CLAVE_PLAZA,
        PLA.PEF,
        PLA.ID_EJERCICIO_FK,
        PLA.VIGENCIA_DE_INICIO,
        PLA.VIGENCIA_FINAL,
        PLA.FECHA_DE_CREACION,
        PLA.FECHA_DE_MODIFICACION,
        PLA.FECHA_DE_ELIMINACION,  
        PLA.COMENTARIO,
        PLA.ID_ESTATUS_PLAZA_FK,
        PLA.NUMERO_PLAZA,
        PLA.PROPIETARIO,
        PUE.CLAVE_PUESTO,
        PUE.DESCRIPCION,
        PUE.DIRECCION_GENERAL, 
        PUE.CLAVE_ADSCR_PUESTO,
        TAB.ID_TABULADOR,
        TAB.SUELDO_BASE,
        TAB.COMPENSACION_GARANTIZADA,
        UNAD.DESCRIPCION DESC_UNI,
        GPO.CLAVE_JERARQUICA,
        GRAD.GRADO,
        NIV.NIVEL,
        EST.ID_ESTRUCTURA,
        UNAD.CLAVE_UNIADM,       
        ADS.ID_ADSCRIPCION,
        ADS.CLAVE_ADSCRIPCION,
        ADS.DESCRIPCION DESCRIPCION_ADS,
        TIPNOM.ID_TIPO_NOMBRAMIENTO,
        TIPNOM.DESCRIPCION NOMBRAMIENTO
  FROM RH_NOMN_PLAZAS PLA
  INNER JOIN  RH_NOMN_CAT_ESTATUS_PLAZA ESTP          ON PLA.ID_ESTATUS_PLAZA_FK      = ESTP.ID_ESTATUS_PLAZA
  INNER JOIN  RH_NOMN_CAT_PUESTOS PUE                ON PLA.ID_PUESTO_FK             = PUE.ID_PUESTO
  INNER JOIN  RH_NOMN_TABULADORES TAB                 ON PLA.ID_TABULADOR_FK          = TAB.ID_TABULADOR
  INNER JOIN  RH_NOMN_CAT_GRUPOS_JERARQUICOS GPO      ON TAB.ID_GRUPO_JERARQUICO_FK   = GPO.ID_GRUPO_JERARQUICO
  INNER JOIN  RH_NOMN_CAT_GRADOS_JERARQUICOS GRAD     ON TAB.ID_GRADO_JERARQUICO_FK   = GRAD.ID_GRADO_JERARQUICO
  INNER JOIN  RH_NOMN_CAT_NIVELES_TABULADOR NIV       ON TAB.ID_NIVEL_TABULADOR_FK    = NIV.ID_NIVEL_TABULADOR
  INNER JOIN  RH_NOMN_CAT_ESTRUCTURAS EST             ON PLA.ID_ESTRUCTURA_FK         = EST.ID_ESTRUCTURA
  INNER JOIN  RH_NOMN_ADSCRIPCIONES ADS               ON PLA.ID_ADSCRIPCION_FK        = ADS.ID_ADSCRIPCION
  INNER JOIN  RH_NOMN_CAT_UNIDADES_ADMINS UNAD        ON ADS.ID_UNIADM_FK             = UNAD.ID_UNIADM
  INNER JOIN  RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNOM   ON PLA.ID_TIPO_NOMBRAM_FK       = TIPNOM.ID_TIPO_NOMBRAMIENTO  
  WHERE ADS.ID_ADSCRIPCION = DECODE(INidAdscripcion,NULL,ADS.ID_ADSCRIPCION,INidAdscripcion)
  AND PLA.ID_PLAZA NOT IN (SELECT ID_PLAZA FROM RH_NOMN_EMPLEADOS)
  AND PLA.ID_ESTATUS_PLAZA_FK = 1
  AND PLA.VIGENCIA_DE_INICIO <= FECHADISPONIBLE
  AND (PLA.VIGENCIA_FINAL > FECHADISPONIBLE OR PLA.VIGENCIA_FINAL IS NULL)

UNION
            ---  plazas que se estan ocupadas pero quedan libres 
  SELECT   ID_PLAZA,
           ID_TABULADOR_FK,
           ID_ADSCRIPCION_FK,
           ID_PUESTO_FK,
           ID_TIPO_NOMBRAM_FK,
           ID_ESTRUCTURA_FK,
           CLAVE_PLAZA,
           PEF,
           ID_EJERCICIO_FK,
           VIGENCIA_DE_INICIO,
           VIGENCIA_FINAL,
           FECHA_DE_CREACION,
           FECHA_DE_MODIFICACION,
           FECHA_DE_ELIMINACION,  
           COMENTARIO,
           ID_ESTATUS_PLAZA_FK,
           NUMERO_PLAZA,
           PROPIETARIO,
           CLAVE_PUESTO,
           DESCRIPCION,
           DIRECCION_GENERAL, 
           CLAVE_ADSCR_PUESTO,
           ID_TABULADOR,
           SUELDO_BASE,
           COMPENSACION_GARANTIZADA,
           DESCRIPCION DESC_UNI,
           CLAVE_JERARQUICA,
           GRADO,
           NIVEL,
           ID_ESTRUCTURA,
           CLAVE_UNIADM,       
           ID_ADSCRIPCION,
           CLAVE_ADSCRIPCION,
           DESCRIPCION DESCRIPCION_ADS,
           ID_TIPO_NOMBRAMIENTO,
           NOMBRAMIENTO
  FROM(    
       SELECT   PLA.ID_PLAZA,
                PLA.ID_TABULADOR_FK,
                PLA.ID_ADSCRIPCION_FK,
                PLA.ID_PUESTO_FK,
                PLA.ID_TIPO_NOMBRAM_FK,
                PLA.ID_ESTRUCTURA_FK,
                PLA.CLAVE_PLAZA,
                PLA.PEF,
                PLA.ID_EJERCICIO_FK,
                (SELECT MAX(DECODE(EMP1.FECHA_BAJA,NULL,(DECODE( (SELECT MAX(FECHA_INICIO_LICENCIA) 
                                                                  FROM RH_NOMN_LICENCIAS_SIN_GOCE
                                                                  WHERE ID_EMPLEADO_FK = EMP1.ID_EMPLEADO
                                                                  AND ESTATUS = 'P'),NULL,FECHADISPONIBLE+1
                                                                                         ,(SELECT MAX(FECHA_INICIO_LICENCIA) 
                                                                                           FROM RH_NOMN_LICENCIAS_SIN_GOCE
                                                                                           WHERE ID_EMPLEADO_FK = EMP1.ID_EMPLEADO
                                                                                           AND ESTATUS = 'P')
                                                               ) 
                                                        ) 
                                                       ,EMP1.FECHA_BAJA+1)
                             )                       
                 FROM RH_NOMN_EMPLEADOS EMP1
                 WHERE EMP1.ID_PLAZA = PLA.ID_PLAZA
                 ) VIGENCIA_DE_INICIO,
                PLA.VIGENCIA_FINAL,
                PLA.FECHA_DE_CREACION,
                PLA.FECHA_DE_MODIFICACION,
                PLA.FECHA_DE_ELIMINACION,  
                PLA.COMENTARIO,
                DECODE((SELECT COUNT(1) 
                        FROM RH_NOMN_LICENCIAS_SIN_GOCE
                        WHERE ESTATUS = 'P'
                        AND ID_EMPLEADO_FK IN (SELECT ID_EMPLEADO 
                                           FROM RH_NOMN_EMPLEADOS 
                                           WHERE ID_PLAZA = PLA.ID_PLAZA 
                                           AND ID_ESTATUS_EMPLEADO = 1)),0,PLA.ID_ESTATUS_PLAZA_FK,5) ID_ESTATUS_PLAZA_FK,
                PLA.NUMERO_PLAZA,
                PLA.PROPIETARIO,
                PUE.CLAVE_PUESTO,
                PUE.DESCRIPCION,
                PUE.DIRECCION_GENERAL, 
                PUE.CLAVE_ADSCR_PUESTO,
                TAB.ID_TABULADOR,
                TAB.SUELDO_BASE,
                TAB.COMPENSACION_GARANTIZADA,
                UNAD.DESCRIPCION DESC_UNI,
                GPO.CLAVE_JERARQUICA,
                GRAD.GRADO,
                NIV.NIVEL,
                EST.ID_ESTRUCTURA,
                UNAD.CLAVE_UNIADM,       
                ADS.ID_ADSCRIPCION,
                ADS.CLAVE_ADSCRIPCION,
                ADS.DESCRIPCION DESCRIPCION_ADS,
                TIPNOM.ID_TIPO_NOMBRAMIENTO,
                TIPNOM.DESCRIPCION NOMBRAMIENTO
       FROM RH_NOMN_PLAZAS PLA
       INNER JOIN  RH_NOMN_CAT_ESTATUS_PLAZA ESTP          ON PLA.ID_ESTATUS_PLAZA_FK      = ESTP.ID_ESTATUS_PLAZA
       INNER JOIN  RH_NOMN_CAT_PUESTOS PUE                 ON PLA.ID_PUESTO_FK             = PUE.ID_PUESTO
       INNER JOIN  RH_NOMN_TABULADORES TAB                 ON PLA.ID_TABULADOR_FK          = TAB.ID_TABULADOR
       INNER JOIN  RH_NOMN_CAT_GRUPOS_JERARQUICOS GPO      ON TAB.ID_GRUPO_JERARQUICO_FK   = GPO.ID_GRUPO_JERARQUICO
       INNER JOIN  RH_NOMN_CAT_GRADOS_JERARQUICOS GRAD     ON TAB.ID_GRADO_JERARQUICO_FK   = GRAD.ID_GRADO_JERARQUICO
       INNER JOIN  RH_NOMN_CAT_NIVELES_TABULADOR NIV       ON TAB.ID_NIVEL_TABULADOR_FK    = NIV.ID_NIVEL_TABULADOR
       INNER JOIN  RH_NOMN_CAT_ESTRUCTURAS EST             ON PLA.ID_ESTRUCTURA_FK         = EST.ID_ESTRUCTURA
       INNER JOIN  RH_NOMN_ADSCRIPCIONES ADS               ON PLA.ID_ADSCRIPCION_FK        = ADS.ID_ADSCRIPCION
       INNER JOIN  RH_NOMN_CAT_UNIDADES_ADMINS UNAD        ON ADS.ID_UNIADM_FK             = UNAD.ID_UNIADM
       INNER JOIN  RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNOM   ON PLA.ID_TIPO_NOMBRAM_FK       = TIPNOM.ID_TIPO_NOMBRAMIENTO
       WHERE ADS.ID_ADSCRIPCION = DECODE(INidAdscripcion,NULL,ADS.ID_ADSCRIPCION,INidAdscripcion)
       AND PLA.ID_PLAZA IN (SELECT ID_PLAZA FROM RH_NOMN_EMPLEADOS)
       AND PLA.ID_ESTATUS_PLAZA_FK IN (1,2)
       AND PLA.ID_PLAZA NOT IN (SELECT ID_PLAZA_FK_INTERINATO FROM RH_NOMN_INTERINATOS WHERE ESTATUS='P')
      )WHERE VIGENCIA_DE_INICIO IS NOT NULL
       AND  FECHADISPONIBLE >= VIGENCIA_DE_INICIO
       AND (VIGENCIA_FINAL > FECHADISPONIBLE OR VIGENCIA_FINAL IS NULL)


UNION 
        --- 
  SELECT  ID_PLAZA,
          ID_TABULADOR_FK,
          ID_ADSCRIPCION_FK,
          ID_PUESTO_FK,
          ID_TIPO_NOMBRAM_FK,
          ID_ESTRUCTURA_FK,
          CLAVE_PLAZA,
          PEF,
          ID_EJERCICIO_FK,
          VIGENCIA_DE_INICIO,
          VIGENCIA_FINAL,
          FECHA_DE_CREACION,
          FECHA_DE_MODIFICACION,
          FECHA_DE_ELIMINACION,  
          COMENTARIO,
          ID_ESTATUS_PLAZA_FK,
          NUMERO_PLAZA,
          PROPIETARIO,
          CLAVE_PUESTO,
          DESCRIPCION,
          DIRECCION_GENERAL, 
          CLAVE_ADSCR_PUESTO,
          ID_TABULADOR,
          SUELDO_BASE,
          COMPENSACION_GARANTIZADA,
          DESCRIPCION DESC_UNI,
          CLAVE_JERARQUICA,
          GRADO,
          NIVEL,
          ID_ESTRUCTURA,
          CLAVE_UNIADM,       
          ID_ADSCRIPCION,
          CLAVE_ADSCRIPCION,
          DESCRIPCION DESCRIPCION_ADS,
          ID_TIPO_NOMBRAMIENTO,
          NOMBRAMIENTO
  FROM(    
          SELECT
            PLA.ID_PLAZA,
            PLA.ID_TABULADOR_FK,
            PLA.ID_ADSCRIPCION_FK,
            PLA.ID_PUESTO_FK,
            PLA.ID_TIPO_NOMBRAM_FK,
            PLA.ID_ESTRUCTURA_FK,
            PLA.CLAVE_PLAZA,
            PLA.PEF,
            PLA.ID_EJERCICIO_FK,
            DECODE( (SELECT MAX(FECHA_FIN_INTERINATO) 
                     FROM RH_NOMN_INTERINATOS INPLZ
                     WHERE INPLZ.ID_PLAZA_FK_INTERINATO = PLA.ID_PLAZA),NULL,LIC.FECHA_INICIO_LICENCIA,(SELECT MAX(FECHA_FIN_INTERINATO)+1 
                                                                                                        FROM RH_NOMN_INTERINATOS INPLZ
                                                                                                        WHERE INPLZ.ID_PLAZA_FK_INTERINATO = PLA.ID_PLAZA)
                  ) VIGENCIA_DE_INICIO,
            LIC.FECHA_FIN_LICENCIA VIGENCIA_FINAL,
            PLA.FECHA_DE_CREACION,
            PLA.FECHA_DE_MODIFICACION,
            PLA.FECHA_DE_ELIMINACION,  
            PLA.COMENTARIO,
            PLA.ID_ESTATUS_PLAZA_FK,
            PLA.NUMERO_PLAZA,
            PLA.PROPIETARIO,
            PUE.CLAVE_PUESTO,
            PUE.DESCRIPCION,
            PUE.DIRECCION_GENERAL, 
            PUE.CLAVE_ADSCR_PUESTO,
            TAB.ID_TABULADOR,
            TAB.SUELDO_BASE,
            TAB.COMPENSACION_GARANTIZADA,
            UNAD.DESCRIPCION DESC_UNI,
            GPO.CLAVE_JERARQUICA,
            GRAD.GRADO,
            NIV.NIVEL,
            EST.ID_ESTRUCTURA,
            UNAD.CLAVE_UNIADM,      
            ADS.ID_ADSCRIPCION,
            ADS.CLAVE_ADSCRIPCION,
            ADS.DESCRIPCION DESCRIPCION_ADS,
            TIPNOM.ID_TIPO_NOMBRAMIENTO,
            TIPNOM.DESCRIPCION NOMBRAMIENTO
          FROM RH_NOMN_PLAZAS PLA
          INNER JOIN  RH_NOMN_CAT_ESTATUS_PLAZA ESTP          ON PLA.ID_ESTATUS_PLAZA_FK      = ESTP.ID_ESTATUS_PLAZA
          INNER JOIN  RH_NOMN_CAT_PUESTOS PUE                 ON PLA.ID_PUESTO_FK             = PUE.ID_PUESTO
          INNER JOIN  RH_NOMN_TABULADORES TAB                 on PLA.ID_TABULADOR_FK          = TAB.ID_TABULADOR
          INNER JOIN  RH_NOMN_CAT_GRUPOS_JERARQUICOS GPO      on TAB.ID_GRUPO_JERARQUICO_FK   = GPO.ID_GRUPO_JERARQUICO
          INNER JOIN  RH_NOMN_CAT_GRADOS_JERARQUICOS GRAD     on TAB.ID_GRADO_JERARQUICO_FK   = GRAD.ID_GRADO_JERARQUICO
          INNER JOIN  RH_NOMN_CAT_NIVELES_TABULADOR NIV       ON TAB.ID_NIVEL_TABULADOR_FK    = NIV.ID_NIVEL_TABULADOR
          INNER JOIN  RH_NOMN_CAT_ESTRUCTURAS EST             ON PLA.ID_ESTRUCTURA_FK         = EST.ID_ESTRUCTURA
          INNER JOIN  RH_NOMN_ADSCRIPCIONES ADS               ON PLA.ID_ADSCRIPCION_FK        = ADS.ID_ADSCRIPCION
          INNER JOIN  RH_NOMN_CAT_UNIDADES_ADMINS UNAD        ON ADS.ID_UNIADM_FK             = UNAD.ID_UNIADM
          INNER JOIN  RH_NOMN_CAT_TIPOS_NOMBRAMIENTO TIPNOM   ON PLA.ID_TIPO_NOMBRAM_FK       = TIPNOM.ID_TIPO_NOMBRAMIENTO
          INNER JOIN  RH_NOMN_EMPLEADOS EM                    ON EM.ID_PLAZA                  = PLA.ID_PLAZA
          INNER JOIN  RH_NOMN_LICENCIAS_SIN_GOCE LIC          ON LIC.ID_EMPLEADO_FK           = EM.ID_EMPLEADO
          WHERE PLA.ID_ESTATUS_PLAZA_FK = 5        
          AND ADS.ID_ADSCRIPCION = DECODE(INidAdscripcion,NULL,ADS.ID_ADSCRIPCION,INidAdscripcion)
          AND LIC.ESTATUS = 'A'
          AND PLA.ID_PLAZA NOT IN (SELECT ID_PLAZA_FK_INTERINATO FROM RH_NOMN_INTERINATOS WHERE ESTATUS='P')
)
 WHERE VIGENCIA_DE_INICIO IS NOT NULL
 AND  FECHADISPONIBLE >= VIGENCIA_DE_INICIO
 AND (VIGENCIA_FINAL > FECHADISPONIBLE OR VIGENCIA_FINAL IS NULL)    
 )
GROUP BY ID_PLAZA,
      ID_TABULADOR_FK,
      ID_ADSCRIPCION_FK,
      ID_PUESTO_FK,
      ID_TIPO_NOMBRAM_FK,
      ID_ESTRUCTURA_FK,
      CLAVE_PLAZA,
      PEF,
      ID_EJERCICIO_FK,      
      FECHA_DE_CREACION,
      FECHA_DE_MODIFICACION,
      FECHA_DE_ELIMINACION,
      COMENTARIO,
      ID_ESTATUS_PLAZA_FK,
      NUMERO_PLAZA,
      PROPIETARIO,
      CLAVE_PUESTO,
      DESCRIPCION,
      DIRECCION_GENERAL,
      CLAVE_ADSCR_PUESTO,
      ID_TABULADOR,
      SUELDO_BASE,
      COMPENSACION_GARANTIZADA,
      DESC_UNI,
      CLAVE_UNIADM,
      CLAVE_JERARQUICA,
      GRADO,
      NIVEL,
      ID_ESTRUCTURA,
      ID_ADSCRIPCION,
      CLAVE_ADSCRIPCION,
      DESCRIPCION_ADS,
      ID_TIPO_NOMBRAMIENTO,      
      DECODE(ID_ESTATUS_PLAZA_FK, 1, 'Vacante', 2, 'Ocupada', 5, 'Temporal'),
      NOMBRAMIENTO
;


 EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK - err_msg: ' || err_msg);

    ROLLBACK;



END SP_PUESTOS_DE_PLAZAS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REP_MOVPER_APLAZA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REP_MOVPER_APLAZA" (IDEMPLEADO IN NUMBER,DATOSP OUT SYS_REFCURSOR) IS

BEGIN


OPEN DATOSP  FOR
SELECT 
         DISTINCT   
         DAT.NOMBRE||' '||DAT.A_PATERNO||' '||DAT.A_MATERNO NOMBRE
        ,EMP.NUMERO_EMPLEADO
        ,DAT.RFC RFC
        ,DAT.CURP
        ,ADS.DESCRIPCION ADSCRIPCION
        ,PUE.DESCRIPCION PUESTO
        ,GRU.CLAVE_JERARQUICA||GRA.GRADO||NVL.NIVEL NIVEL
        ,NOM.DESCRIPCION REGIMEN
FROM RH_NOMN_PLAZAS              PLZ
LEFT  JOIN RH_NOMN_EMPLEADOS              EMP ON EMP.NUMERO_EMPLEADO = PLZ.PROPIETARIO
LEFT  JOIN RH_NOMN_DATOS_PERSONALES       DAT ON DAT.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_PUESTOS            PUE ON PUE.ID_PUESTO   = PLZ.ID_PUESTO_FK
INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOM ON NOM.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
INNER JOIN RH_NOMN_ADSCRIPCIONES          ADS ON ADS.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
INNER JOIN RH_NOMN_TABULADORES            TAB ON TAB.ID_TABULADOR = PLZ.ID_TABULADOR_FK
INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NVL ON NVL.ID_NIVEL_TABULADOR  = TAB.ID_NIVEL_TABULADOR_FK
WHERE EMP.ID_EMPLEADO = IDEMPLEADO
;

END SP_REP_MOVPER_APLAZA;

/
--------------------------------------------------------
--  DDL for Procedure SP_REP_MOVPER_NPLAZA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REP_MOVPER_NPLAZA" (IDPLAZA IN NUMBER,DATOSP OUT SYS_REFCURSOR) IS

BEGIN


OPEN DATOSP  FOR
SELECT 
         DISTINCT   
         DAT.NOMBRE||' '||DAT.A_PATERNO||' '||DAT.A_MATERNO NOMBRE
        ,ADS.DESCRIPCION ADSCRIPCION
        ,PUE.DESCRIPCION PUESTO
        ,GRU.CLAVE_JERARQUICA||GRA.GRADO||NVL.NIVEL NIVEL
        ,NOM.DESCRIPCION REGIMEN
FROM RH_NOMN_PLAZAS              PLZ
LEFT  JOIN RH_NOMN_EMPLEADOS              EMP ON EMP.NUMERO_EMPLEADO = PLZ.PROPIETARIO
LEFT  JOIN RH_NOMN_DATOS_PERSONALES       DAT ON DAT.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
INNER JOIN RH_NOMN_CAT_PUESTOS            PUE ON PUE.ID_PUESTO   = PLZ.ID_PUESTO_FK
INNER JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOM ON NOM.ID_TIPO_NOMBRAMIENTO = PLZ.ID_TIPO_NOMBRAM_FK
INNER JOIN RH_NOMN_ADSCRIPCIONES          ADS ON ADS.ID_ADSCRIPCION = PLZ.ID_ADSCRIPCION_FK
INNER JOIN RH_NOMN_TABULADORES            TAB ON TAB.ID_TABULADOR = PLZ.ID_TABULADOR_FK
INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NVL ON NVL.ID_NIVEL_TABULADOR  = TAB.ID_NIVEL_TABULADOR_FK
WHERE PLZ.ID_PLAZA  = IDPLAZA
;

END SP_REP_MOVPER_NPLAZA;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORT_NOMBRAMIENTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORT_NOMBRAMIENTO" (IDEMPLEADO IN NUMBER, DATOS OUT SYS_REFCURSOR)IS
ZONA_ECONOMICA NUMBER := 1;
ESTADO         VARCHAR2(2) := '09';
DESPENSA       NUMBER :=1200.00;
AYUDA_DESPENSA NUMBER :=365.00;
BEGIN

OPEN DATOS FOR    
        SELECT   DAT.NOMBRE||' '||DAT.A_PATERNO||' '||DAT.A_MATERNO NOMBRE
                ,EM.NUMERO_EMPLEADO  NUM_EMPLEADO
                ,DAT.RFC
                ,DAT.CURP
                ,UPPER(ESTCIV.DESCRIPCION_ESTADO_CIVIL) ESTADO_CIVIL
                ,UPPER(GEN.DESCRIPCION_GENERO) SEXO
                ,ROUND(ABS(MONTHS_BETWEEN( TRUNC(DAT.FECHA_NACIMIENTO),
                                 TRUNC(TO_DATE(SYSDATE,'DD/MM/YY')) )/12))||' A�OS' EDAD
                ,NAC.DESCRIPCION_NACIONALIDAD NACIONALIDAD
                ,DIR.CALLE||' '||DIR.NUM_EXTERIOR||' '||DIR.NUM_INTERIOR DOMICILIO
                ,UPPER(COL.DESCRIPCION_COLONIA) COLONIA
                ,UPPER(MUN.DESCRIPCION_MUNICIPIO) MUNICIPIO
                ,CP.DESCRIPCION_CODIGO_POSTAL CODIGO_POSTAL
                ,TO_CHAR(EM.FECHA_DE_INGRESO,'DD') DIA
                ,TO_CHAR(EM.FECHA_DE_INGRESO,'MM') MES
                ,TO_CHAR(EM.FECHA_DE_INGRESO,'YY') ANIO
                ,UADM.CLAVE_UNIADM   UNIDAD_ADMIN
                ,PUES.DESCRIPCION   PUESTO
                ,GRUPO.CLAVE_JERARQUICA||GRADO.GRADO||NIVEL.NIVEL NIVEL_SALARIAL
                ,TAB.SUELDO_BASE 
                ,TAB.COMPENSACION_GARANTIZADA
                ,AYUDA_DESPENSA AYUDA_DESPENSA
                ,DESPENSA VALES_DESPENSA
                ,ZONA_ECONOMICA ZONA_ECONOMICA
                ,ESTADO ESTADO
        FROM RH_NOMN_EMPLEADOS                     EM 
        LEFT JOIN RH_NOMN_DATOS_PERSONALES       DAT    ON DAT.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
        LEFT JOIN RH_NOMN_CAT_GENEROS            GEN    ON GEN.ID_GENERO = DAT.ID_GENERO
        LEFT JOIN RH_NOMN_CAT_ESTADOS_CIVIL      ESTCIV ON ESTCIV.ID_ESTADO_CIVIL = DAT.ID_ESTADO_CIVIL
        LEFT JOIN RH_NOMN_DIRECCIONES            DIR    ON DIR.ID_DIRECCION = DAT.ID_DIRECCION 
        LEFT JOIN RH_NOMN_CAT_COLONIAS           COL    ON COL.ID_COLONIA = DIR.ID_COLONIA
        LEFT JOIN RH_NOMN_CAT_MUNICIPIOS         MUN    ON MUN.ID_MUNICIPIO = COL.ID_MUNICIPIO
        LEFT JOIN RH_NOMN_CAT_CODIGO_POSTAL      CP     ON CP.ID_CODIGO_POSTAL = COL.ID_CODIGO_POSTAL
        LEFT JOIN RH_NOMN_CAT_NACIONALIDADES     NAC    ON NAC.ID_NACIONALIDAD = DAT.ID_NACIONALIDAD
        LEFT JOIN RH_NOMN_PLAZAS                 PLA    ON PLA.ID_PLAZA            = EM.ID_PLAZA
        LEFT JOIN RH_NOMN_ADSCRIPCIONES          ADS    ON ADS.ID_ADSCRIPCION      = PLA.ID_ADSCRIPCION_FK
        LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS    UADM   ON UADM.ID_UNIADM          = ADS.ID_UNIADM_FK
        LEFT JOIN RH_NOMN_CAT_PUESTOS            PUES   ON PUES.ID_PUESTO          = PLA.ID_PUESTO_FK
        LEFT JOIN RH_NOMN_TABULADORES            TAB    ON TAB.ID_TABULADOR        = PLA.ID_TABULADOR_FK
        LEFT JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRUPO  ON GRUPO.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRADO  ON GRADO.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NIVEL  ON NIVEL.ID_NIVEL_TABULADOR  = TAB.ID_NIVEL_TABULADOR_FK
        WHERE EM.ID_EMPLEADO = IDEMPLEADO;



END SP_REPORT_NOMBRAMIENTO;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORT_NOMBRAMIENTO_PLZANT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORT_NOMBRAMIENTO_PLZANT" (IDEMPLEADO IN NUMBER, PLAZAANT OUT SYS_REFCURSOR)IS

MOVANT NUMBER;
PLZANT NUMBER;
DESPENSA       NUMBER :=1200.00;
AYUDA_DESPENSA NUMBER :=365.00;

BEGIN 


SELECT DISTINCT DECODE(HC.ID_MOVIMIENTO_ANTERIOR,0,HC.ID_HISTORICO_MOVIMIENTO_FK,HC.ID_MOVIMIENTO_ANTERIOR) INTO MOVANT
            FROM RH_NOMN_BIT_HISTORICOS_CAMPOS HC
            INNER JOIN RH_NOMN_CAT_CAMPOS CMP ON CMP.ID_CAMPO = HC.ID_CAMPO_AFECTADO_FK
            WHERE ID_HISTORICO_MOVIMIENTO_FK = (
                                                SELECT MAX(HISMOVS.ID_HISTORICO_MOVIMIENTO)
                                                FROM RH_NOMN_BIT_HISTORICOS_MOVS HISMOVS 
                                                INNER JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS HISCAMPS ON hismovs.id_historico_movimiento= hiscamps.id_historico_movimiento_fk
                                                INNER JOIN RH_NOMN_CAT_CAMPOS CATCAMPS ON hiscamps.id_campo_afectado_fk = catcamps.id_campo
                                                WHERE HISCAMPS.VALOR_BUSQUEDA = (SELECT EM.NUMERO_EMPLEADO FROM RH_NOMN_EMPLEADOS EM  WHERE EM.ID_EMPLEADO = IDEMPLEADO )
                                                AND   catcamps.id_tipos_movimiento = 2
                                               )

            ;

DBMS_OUTPUT.PUT_LINE('MOVIMIENTO ANTERIOR '||MOVANT);


SELECT ID_REGISTRO_ENTIDAD INTO PLZANT
FROM RH_NOMN_BIT_HISTORICOS_CAMPOS
WHERE ID_HISTORICO_CAMPO = MOVANT;


OPEN PLAZAANT FOR
SELECT 
                 UADM.CLAVE_UNIADM   UNIDAD_ADMIN
                ,PUES.DESCRIPCION   PUESTO
                ,GRUPO.CLAVE_JERARQUICA||GRADO.GRADO||NIVEL.NIVEL NIVEL_SALARIAL
                ,TAB.SUELDO_BASE 
                ,TAB.COMPENSACION_GARANTIZADA
                ,AYUDA_DESPENSA AYUDA_DESPENSA
                ,DESPENSA VALES_DESPENSA

        FROM RH_NOMN_PLAZAS                 PLA    
        LEFT JOIN RH_NOMN_ADSCRIPCIONES          ADS    ON ADS.ID_ADSCRIPCION      = PLA.ID_ADSCRIPCION_FK
        LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS    UADM   ON UADM.ID_UNIADM          = ADS.ID_UNIADM_FK
        LEFT JOIN RH_NOMN_CAT_PUESTOS            PUES   ON PUES.ID_PUESTO          = PLA.ID_PUESTO_FK
        LEFT JOIN RH_NOMN_TABULADORES            TAB    ON TAB.ID_TABULADOR        = PLA.ID_TABULADOR_FK
        LEFT JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRUPO  ON GRUPO.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRADO  ON GRADO.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
        LEFT JOIN RH_NOMN_CAT_NIVELES_TABULADOR  NIVEL  ON NIVEL.ID_NIVEL_TABULADOR  = TAB.ID_NIVEL_TABULADOR_FK
        WHERE PLA.ID_PLAZA = PLZANT;

END SP_REPORT_NOMBRAMIENTO_PLZANT;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_ARCHIVO_DISPERSION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_ARCHIVO_DISPERSION" (IDNOMINA IN NUMBER,ARCHIVO OUT BLOB)
IS
TIPOCONSULTA  NUMBER;
EXISTE        NUMBER;
CONSECUTIVO   NUMBER;


  PROCEDURE GENERAR_ARCHIVO(IDNOMINA IN NUMBER, TIPOCONSULTA IN NUMBER, ARCHIVO OUT BLOB) IS
    VQUERY        VARCHAR2(3000);
    CABECERA      VARCHAR2(200);
    DATOS         SYS_REFCURSOR;
    DETALLE       VARCHAR2(200);
    CADENA        CLOB;
    CAB           VARCHAR2(2):= 'H';
    CLAVESERVICIO VARCHAR2(3) := 'NE';
    EMISORA       NUMBER := 83712;
    --CONSECUTIVO   NUMBER;

    BEGIN


    SELECT NVL(MAX(D.VERSION),-1)+1 INTO CONSECUTIVO
    FROM RH_NOMN_ARCHIVO_DISPERSION D
    WHERE ID_NOMINA_FK = IDNOMINA
    ; 

    vQUERY := '
    SELECT  '''||CAB||'''
           ||'''||CLAVESERVICIO||'''
           ||'''||EMISORA||'''
           ||FECHA_APLICACION
           ||LPAD('||CONSECUTIVO||',2,0)
           ||LPAD(COUNT(1),6,0)
           ||LPAD(REPLACE(SUM(IMPORTES),''.'',''''),15,0)
           ||LPAD(NVL(NULL,0),49,0)
           ||LPAD('' '',77,'' '')

    FROM
    (
      SELECT  EM.NUMERO_EMPLEADO
              ,TO_CHAR(CATQUIN.FECHA_PAGO_QUINCENA,''YYYYMMDD'') FECHA_APLICACION
              ,SUM(EMPLAZ.IMPORTE_TOTAL) IMPORTES
      FROM RH_NOMN_CABECERAS_NOMINAS NOMN
      ';
    IF TIPOCONSULTA = 2 THEN 
      vQUERY := vQUERY||'
      LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA EMPLAZ  ON EMPLAZ.ID_NOMINA_FK = NOMN.ID_NOMINA
      ';
    ELSIF TIPOCONSULTA = 1 THEN 
      vQUERY := vQUERY||'
      LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 EMPLAZ  ON EMPLAZ.ID_NOMINA01_FK = NOMN.ID_NOMINA
      ';
    END IF;  
    vQUERY := vQUERY||'
      LEFT JOIN RH_NOMN_EMPLEADOS           EM      ON EM.ID_EMPLEADO = EMPLAZ.ID_EMPLEADO_FK
      LEFT JOIN RH_NOMN_CAT_QUINCENAS       CATQUIN ON CATQUIN.ID_QUINCENA = NOMN.ID_QUINCENA_FK
      WHERE NOMN.ID_NOMINA = '||IDNOMINA||'
      AND EM.ID_TIPO_PAGO = 2
      AND EMPLAZ.IMPORTE_TOTAL > 0
      GROUP BY EM.NUMERO_EMPLEADO,CATQUIN.FECHA_PAGO_QUINCENA
      --AND   EM.ID_ESTATUS_EMPLEADO = 1
    )GROUP BY FECHA_APLICACION';

    --DBMS_OUTPUT.PUT_LINE(vQUERY);

    EXECUTE IMMEDIATE vQUERY INTO CABECERA;



    vQUERY := '
    SELECT 

           ''D''
           ||FECHA_APLICACION
           ||IDEMPLEADO
           ||REFSERV
           ||REFELEYENDA
           ||LPAD(TO_NUMBER(IMPORTE),15,0)
           ||BANCO_RECEPTOR
           ||TIPO_CUENTA
           ||NUMERO_CUENTA
           ||TIPO_MOVIMIENTO
           ||ACCION
           ||IMPORTE_IVA
           ||LPAD('' '',18,'' '')
    FROM
    (
      SELECT 
               TO_CHAR(CATQUIN.FECHA_PAGO_QUINCENA,''YYYYMMDD'')FECHA_APLICACION 
              ,LPAD(EM.NUMERO_EMPLEADO,10,0) IDEMPLEADO
              ,LPAD(NVL(NULL,'' ''),40,'' '')REFSERV
              ,LPAD(NVL(NULL,'' ''),40,'' '')REFELEYENDA
              ,REPLACE(TO_CHAR(SUM(EMPLAZ.IMPORTE_TOTAL),''9999999999999.99''),''.'','''') IMPORTE
              ,LPAD(NVL(BANCO.CLAVE_BANCO,0),3,0) BANCO_RECEPTOR
              ,LPAD(NVL(CASE WHEN UPPER(BANCO.DESCRIPCION_BANCO) = ''BANORTE'' THEN 
                           ''01''
                         ELSE 
                           ''40''
               END,0),2,0) TIPO_CUENTA
               ,LPAD(NVL(CASE WHEN UPPER(BANCO.DESCRIPCION_BANCO) = ''BANORTE'' THEN 
                            DATBAN.NUMERO_CUENTA 
                          ELSE 
                            DATBAN.CLABE
                     END,0),18,0) NUMERO_CUENTA
              ,NVL(NULL,0)TIPO_MOVIMIENTO 
              ,NVL(NULL,'' '')ACCION
              ,LPAD(NVL(NULL,0),8,0)IMPORTE_IVA
      FROM RH_NOMN_CABECERAS_NOMINAS NOMN
      ';
    IF TIPOCONSULTA = 2 THEN 
      vQUERY := vQUERY||'  
      LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA EMPLAZ  ON EMPLAZ.ID_NOMINA_FK = NOMN.ID_NOMINA
      ';
    ELSIF TIPOCONSULTA = 1 THEN 
      vQUERY := vQUERY||'  
      LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 EMPLAZ  ON EMPLAZ.ID_NOMINA01_FK = NOMN.ID_NOMINA
      ';  
    END IF;  
      vQUERY := vQUERY||'
      LEFT JOIN RH_NOMN_CAT_QUINCENAS       CATQUIN ON CATQUIN.ID_QUINCENA = NOMN.ID_QUINCENA_FK
      LEFT JOIN RH_NOMN_EMPLEADOS           EM      ON EM.ID_EMPLEADO = EMPLAZ.ID_EMPLEADO_FK
      LEFT JOIN RH_NOMN_DATOS_BANCARIOS     DATBAN  ON DATBAN.ID_DATOS_BANCARIOS = EM.ID_DATOS_BANCARIOS
      LEFT JOIN RH_NOMN_CAT_BANCOS          BANCO   ON BANCO.ID_BANCO = DATBAN.ID_BANCO
      WHERE NOMN.ID_NOMINA = '||IDNOMINA||'
      AND EM.ID_TIPO_PAGO = 2
      AND EMPLAZ.IMPORTE_TOTAL > 0
      GROUP BY CATQUIN.FECHA_PAGO_QUINCENA,EM.NUMERO_EMPLEADO,BANCO.CLAVE_BANCO
              ,CASE WHEN UPPER(BANCO.DESCRIPCION_BANCO) = ''BANORTE'' THEN 
                           ''01''
                         ELSE 
                           ''40''
               END
              ,CASE WHEN UPPER(BANCO.DESCRIPCION_BANCO) = ''BANORTE'' THEN 
                            DATBAN.NUMERO_CUENTA 
                          ELSE 
                            DATBAN.CLABE
              END
      )ORDER BY IDEMPLEADO';

    --DBMS_OUTPUT.PUT_LINE(vQUERY);

    OPEN DATOS FOR vQUERY;

    DBMS_LOB.CREATETEMPORARY(CADENA,TRUE);
    DBMS_LOB.OPEN (CADENA, DBMS_LOB.LOB_READWRITE);
    DBMS_LOB.WRITEAPPEND(CADENA,length(CABECERA),CABECERA);
    LOOP
      FETCH DATOS INTO DETALLE;
        EXIT WHEN DATOS%NOTFOUND;
      DETALLE := CHR(13)||CHR(10)||DETALLE;
      DBMS_LOB.WRITEAPPEND(CADENA,length(DETALLE),DETALLE);
    END LOOP;

    ARCHIVO := FN_UTILS_CLOB_TO_BLOB_OTRO(CADENA);

END GENERAR_ARCHIVO;

BEGIN


      SELECT DISTINCT CASE WHEN PLZ.ID_NOMINA_FK IS NOT NULL THEN 
                                  2   ---Para ir a la tabla de nominanas cerradas
                               WHEN PLZ1.ID_NOMINA01_FK  IS NOT NULL THEN
                                  1   ---Para ir a las tabla de nominas pendientes
                          END INTO TIPOCONSULTA     
          FROM RH_NOMN_CABECERAS_NOMINAS NOMN
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA PLZ ON PLZ.ID_NOMINA_FK = NOMN.ID_NOMINA
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 PLZ1 ON PLZ1.ID_NOMINA01_FK = NOMN.ID_NOMINA
          WHERE NOMN.ID_NOMINA = IDNOMINA;


          IF TIPOCONSULTA IS NOT NULL THEN

            SELECT COUNT(1) INTO EXISTE 
            FROM RH_NOMN_ARCHIVO_DISPERSION
            WHERE ID_NOMINA_FK = IDNOMINA;

              IF EXISTE = 0 AND TIPOCONSULTA = 1 THEN    
                --DBMS_OUTPUT.PUT_LINE('Solo crea archivo');
                GENERAR_ARCHIVO(IDNOMINA,TIPOCONSULTA,ARCHIVO);


              ELSIF EXISTE = 0 AND TIPOCONSULTA = 2 THEN 
                --Se genera el archivo de la nomina cerrada y se guarda
                --DBMS_OUTPUT.PUT_LINE('Se genera el archivo');
                GENERAR_ARCHIVO(IDNOMINA,TIPOCONSULTA,ARCHIVO);
                INSERT INTO RH_NOMN_ARCHIVO_DISPERSION VALUES ( (SELECT NVL(MAX(D.ID_ARCHIVO_DISPERSION),1) + 1
                                                                 FROM RH_NOMN_ARCHIVO_DISPERSION D), IDNOMINA,ARCHIVO,CONSECUTIVO);
                COMMIT;
              ELSIF EXISTE = 1 AND TIPOCONSULTA = 2 THEN 
              --Se vuelve genera el archivo de la nomina cerrada y se actualiza
                GENERAR_ARCHIVO(IDNOMINA,TIPOCONSULTA,ARCHIVO);

                UPDATE RH_NOMN_ARCHIVO_DISPERSION SET ARCHIVO = ARCHIVO, VERSION = CONSECUTIVO
                WHERE ID_NOMINA_FK = IDNOMINA;                                                                                    
                COMMIT;
              END IF;  
          ELSE
            DBMS_OUTPUT.PUT_LINE('LA QUINCENA NO EXISTE');
          END IF;

COMMIT;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);

END SP_REPORTE_ARCHIVO_DISPERSION;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SEGRET_EST_PAG_PRIM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SEGRET_EST_PAG_PRIM" (MES IN VARCHAR2, DATOS OUT SYS_REFCURSOR) AS 

  vIdConceptoSegRet    NUMBER;

BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil estructura.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegRet
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '77'
  ;

  OPEN DATOS FOR
    SELECT
      ID_EMPLEADO_FK,
      A_PATERNO,
      A_MATERNO,
      NOMBRE,
      RFC,
      CURP,
      SEXO,
      IMPORTE_TRABAJADOR,
      IMPORTE_DEPENDENCIA,
      IMPORTE_TRABAJADOR + IMPORTE_DEPENDENCIA IMPORTE_PAGAR
    FROM
    (
      SELECT
        NOMEMPPLA.ID_EMPLEADO_FK,
        DATPER.A_PATERNO A_PATERNO,
        DATPER.A_MATERNO A_MATERNO,
        DATPER.NOMBRE NOMBRE,
        DATPER.RFC RFC,
        DATPER.CURP CURP,
        DECODE(CATGEN.DESCRIPCION_GENERO,'MASCULINO','H','FEMENINO','M') SEXO,
        SUM(CONEMPPLA.IMPORTE_CONCEPTO) IMPORTE_TRABAJADOR,
        NVL(SUM(DECODE((SELECT MOD(QUI2.NUMERO_QUINCENA,2) FROM DUAL),0, SEGRET.IMPORTE_PAR_DEP)), 0) +
        NVL(SUM(DECODE((SELECT MOD(QUI2.NUMERO_QUINCENA,2) FROM DUAL),1, SEGRET.IMPORTE_NON_DEP)), 0) IMPORTE_DEPENDENCIA    
      FROM 
        RH_NOMN_CABECERAS_NOMINAS CAB
        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON NOMEMPPLA.ID_NOMINA_FK = CAB.ID_NOMINA
        INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
        INNER JOIN RH_NOMN_CAT_QUINCENAS QUI2 ON QUI2.ID_QUINCENA = CAB.ID_QUINCENA_FK
        INNER JOIN RH_NOMN_SEGURO_RETIRO SEGRET ON SEGRET.ID_EMPLEADO_FK = NOMEMPPLA.ID_EMPLEADO_FK
        INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
        INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
        INNER JOIN RH_NOMN_CAT_GENEROS CATGEN ON CATGEN.ID_GENERO = DATPER.ID_GENERO  
      WHERE
        CAB.ID_QUINCENA_FK IN
        (
          SELECT 
            ID_QUINCENA
          FROM 
            RH_NOMN_CAT_QUINCENAS QUI
            INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
          WHERE 
            TO_CHAR(QUI.FECHA_INICIO_QUINCENA,'MM') = MES
            AND QUI.ID_EJERCICIO_FK = 
            (
              SELECT
                ID_EJERCICIO
              FROM
                RH_NOMN_CAT_EJERCICIOS
              WHERE
                VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
            )
        )
        AND CAB.ID_TIPO_NOMINA_FK = 1
        AND CAB.ID_ESTATUS_NOMINA_FK = 3  
        AND CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegRet
    GROUP BY 
      NOMEMPPLA.ID_EMPLEADO_FK,
      DATPER.A_PATERNO,
      DATPER.A_MATERNO,
      DATPER.NOMBRE,
      DATPER.RFC,
      DATPER.CURP,
      DECODE(CATGEN.DESCRIPCION_GENERO,'MASCULINO','H','FEMENINO','M')
    )
    ORDER BY
      A_PATERNO,
      A_MATERNO,
      NOMBRE
    ;   
    
END SP_REPORTE_SEGRET_EST_PAG_PRIM;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SEGRET_EVE_PAG_PRIM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SEGRET_EVE_PAG_PRIM" (MES IN VARCHAR2, DATOS OUT SYS_REFCURSOR) AS 

  vIdConceptoSegRet    NUMBER;

BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil eventual.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegRet
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '77E'
  ;

  OPEN DATOS FOR
    SELECT
      ID_EMPLEADO_FK,
      A_PATERNO,
      A_MATERNO,
      NOMBRE,
      RFC,
      CURP,
      SEXO,
      IMPORTE_TRABAJADOR,
      IMPORTE_DEPENDENCIA,
      IMPORTE_TRABAJADOR + IMPORTE_DEPENDENCIA IMPORTE_PAGAR
    FROM
    (
      SELECT
        NOMEMPPLA.ID_EMPLEADO_FK,
        DATPER.A_PATERNO A_PATERNO,
        DATPER.A_MATERNO A_MATERNO,
        DATPER.NOMBRE NOMBRE,
        DATPER.RFC RFC,
        DATPER.CURP CURP,
        DECODE(CATGEN.DESCRIPCION_GENERO,'MASCULINO','H','FEMENINO','M') SEXO,
        SUM(CONEMPPLA.IMPORTE_CONCEPTO) IMPORTE_TRABAJADOR,
        NVL(SUM(DECODE((SELECT MOD(QUI2.NUMERO_QUINCENA,2) FROM DUAL),0, SEGRET.IMPORTE_PAR_DEP)), 0) +
        NVL(SUM(DECODE((SELECT MOD(QUI2.NUMERO_QUINCENA,2) FROM DUAL),1, SEGRET.IMPORTE_NON_DEP)), 0) IMPORTE_DEPENDENCIA    
      FROM 
        RH_NOMN_CABECERAS_NOMINAS CAB
        INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON NOMEMPPLA.ID_NOMINA_FK = CAB.ID_NOMINA
        INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
        INNER JOIN RH_NOMN_CAT_QUINCENAS QUI2 ON QUI2.ID_QUINCENA = CAB.ID_QUINCENA_FK
        INNER JOIN RH_NOMN_SEGURO_RETIRO SEGRET ON SEGRET.ID_EMPLEADO_FK = NOMEMPPLA.ID_EMPLEADO_FK
        INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
        INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
        INNER JOIN RH_NOMN_CAT_GENEROS CATGEN ON CATGEN.ID_GENERO = DATPER.ID_GENERO  
      WHERE
        CAB.ID_QUINCENA_FK IN
        (
          SELECT 
            ID_QUINCENA
          FROM 
            RH_NOMN_CAT_QUINCENAS QUI
            INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
          WHERE 
            TO_CHAR(QUI.FECHA_INICIO_QUINCENA,'MM') = MES
            AND QUI.ID_EJERCICIO_FK = 
            (
              SELECT
                ID_EJERCICIO
              FROM
                RH_NOMN_CAT_EJERCICIOS
              WHERE
                VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
            )
        )
        AND CAB.ID_TIPO_NOMINA_FK = 2
        AND CAB.ID_ESTATUS_NOMINA_FK = 3  
        AND CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegRet
    GROUP BY 
      NOMEMPPLA.ID_EMPLEADO_FK,
      DATPER.A_PATERNO,
      DATPER.A_MATERNO,
      DATPER.NOMBRE,
      DATPER.RFC,
      DATPER.CURP,
      DECODE(CATGEN.DESCRIPCION_GENERO,'MASCULINO','H','FEMENINO','M')
    )
    ORDER BY
      A_PATERNO,
      A_MATERNO,
      NOMBRE
    ;

END SP_REPORTE_SEGRET_EVE_PAG_PRIM;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SEGRET_MOVIMIENTOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SEGRET_MOVIMIENTOS" (MES IN VARCHAR2, DATOS OUT SYS_REFCURSOR) AS 

  vFechaInicio          DATE;
  vFechaFin             DATE;

BEGIN
  
  -- Obtener las fechas de rango por mes.
  SELECT
    MIN(FECHA_INICIO_QUINCENA),
    MAX(FECHA_FIN_QUINCENA)
  INTO
    vFechaInicio,
    vFechaFin
  FROM
  (
    SELECT 
      QUI.FECHA_INICIO_QUINCENA,
      QUI.FECHA_FIN_QUINCENA
    FROM 
      RH_NOMN_CAT_QUINCENAS QUI
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
    WHERE 
      TO_CHAR(QUI.FECHA_INICIO_QUINCENA,'MM') = MES
      AND QUI.ID_EJERCICIO_FK = 
      (
        SELECT
          ID_EJERCICIO
        FROM
          RH_NOMN_CAT_EJERCICIOS
        WHERE
          VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
      )
  )
  ;  
    
  OPEN DATOS FOR
    SELECT
      ID_EMPLEADO,
      A_PATERNO,
      A_MATERNO,
      NOMBRE,
      RFC,
      CURP,
      SEXO,
      FECHA,
      ACCION,
      ENTIDAD_FEDERATIVA,
      LICENCIA_MEDICA_INICIO,
      LICENCIA_MEDICA_FIN
    FROM
    (
      SELECT
        EMP.ID_EMPLEADO ID_EMPLEADO,
        DATPER.A_PATERNO A_PATERNO,
        DATPER.A_MATERNO A_MATERNO,
        DATPER.NOMBRE NOMBRE,
        DATPER.RFC RFC,
        DATPER.CURP CURP,
        DECODE(CATGEN.DESCRIPCION_GENERO,'HOMBRE','H','MUJER','M') SEXO,
        BHM.FECHA FECHA,
        CA.NOMBRE_CORTO ACCION,
        'CIUDAD DE M�XICO' ENTIDAD_FEDERATIVA,
        NULL LICENCIA_MEDICA_INICIO,
        NULL LICENCIA_MEDICA_FIN
      FROM 
        RH_NOMN_BIT_HISTORICOS_MOVS BHM
        INNER JOIN RH_NOMN_CAT_ACCIONES CA ON CA.ID_ACCION = BHM.ID_ACCION_FK
        INNER JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS BHC ON BHC.ID_HISTORICO_MOVIMIENTO_FK = BHM.ID_HISTORICO_MOVIMIENTO
        INNER JOIN RH_NOMN_CAT_CAMPOS CC ON CC.ID_CAMPO = BHC.ID_CAMPO_AFECTADO_FK
        INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = BHC.ID_REGISTRO_ENTIDAD
        INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
        INNER JOIN RH_NOMN_CAT_GENEROS CATGEN ON CATGEN.ID_GENERO = DATPER.ID_GENERO  
      WHERE
        (CA.DESCRIPCION LIKE '%Alta Empleado%' 
        OR CA.DESCRIPCION LIKE '%Baja Empleado%')   
        AND FECHA BETWEEN TRUNC(vFechaInicio) AND TRUNC(vFechaFin)
      GROUP BY
        EMP.ID_EMPLEADO,
        DATPER.A_PATERNO,
        DATPER.A_MATERNO,
        DATPER.NOMBRE,
        DATPER.RFC,
        DATPER.CURP,
        DECODE(CATGEN.DESCRIPCION_GENERO,'HOMBRE','H','MUJER','M'),
        BHM.FECHA,
        CA.NOMBRE_CORTO ,
        'CIUDAD DE M�XICO',
        NULL,
        NULL
    )
    ORDER BY
      A_PATERNO,
      A_MATERNO,
      NOMBRE,
      FECHA
    ;  

END SP_REPORTE_SEGRET_MOVIMIENTOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SEGVID_MOVS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SEGVID_MOVS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) AS 

  vQuincenaReporta    VARCHAR2(100);
  vQuincenafechaIni date;
  vQuincenafechaFin date;

BEGIN



  -- Quincena en la que se hace el reporte.
  SELECT
    MAX(DECODE((SELECT QUI2.NUMERO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI2 WHERE QUI2.ID_QUINCENA = IDQUINCENA), 
    1, 'PRIMERA QNA DE ENERO',
    2, 'SEGUNDA QNA DE ENERO',
    3, 'PRIMERA QNA DE FEBRERO',
    4, 'SEGUNDA QNA DE FEBRERO',
    5, 'PRIMERA QNA DE MARZO',
    6, 'SEGUNDA QNA DE MARZO',
    7, 'PRIMERA QNA DE ABRIL',
    8, 'SEGUNDA QNA DE ABRIL',
    9, 'PRIMERA QNA DE MAYO',
    10, 'SEGUNDA QNA DE MAYO',
    11, 'PRIMERA QNA DE JUNIO',
    12, 'SEGUNDA QNA DE JUNIO',
    13, 'PRIMERA QNA DE JULIO',
    14, 'SEGUNDA QNA DE JULIO',
    15, 'PRIMERA QNA DE AGOSTO',
    16, 'SEGUNDA QNA DE AGOSTO',
    17, 'PRIMERA QNA DE SEPTIEMBRE',
    18, 'SEGUNDA QNA DE SEPTIEMBRE',
    19, 'PRIMERA QNA DE OCTUBRE',
    20, 'SEGUNDA QNA DE OCTUBRE',
    21, 'PRIMERA QNA DE NOVIEMBRE',
    22, 'SEGUNDA QNA DE NOVIEMBRE',
    23, 'PRIMERA QNA DE DICIEMBRE',
    24, 'SEGUNDA QNA DE DICIEMBRE')) QUINCENA_REPORTA, 
      FECHA_INICIO_QUINCENA,
      FECHA_FIN_QUINCENA
  INTO
    vQuincenaReporta, vQuincenaFechaIni, vQuincenaFechaFin
  FROM
    RH_NOMN_CAT_QUINCENAS
  ;  

    OPEN DATOS FOR
      SELECT 
            
        er.A_PATERNO, 
        er.A_MATERNO, 
        er.NOMBRE, 
        er.RFC, 
        er.CURP,
        er.FECHA_NACIMIENTO, 
        DECODE(er.DESCRIPCION_GENERO, 'MASCULINO', 'H', 'FEMENINO', 'F') SEXO, 
        er.CLAVE_JERARQUICA || er.GRADO || er.NIVEL as NIVEL_TABULAR, 
        er.SGMM AS NIVEL_TABULAR_HOMOLOGADO,
        UPPER(er.DESCRIPCION) UNIDAD_ADMINISTRATIVA,
        'CIUDAD DE MEXICO' ENTIDAD_FEDERATIVA,
        er.SUELDO_BASE,   
        er.percepcion_bruta,
        er.COMPENSACION_GARANTIZADA,    
        0 PRIMA_RIESGO,
        er.percepcion_bruta PERCEPCION_BRUTA_MENSUAL,
        1.46 PORCENTAJE_PRIMA_BASICA,
        ROUND((er.percepcion_bruta*0.0146),2) AS IMPORTE_MENSUAL_PRIMA_BASICA,
        ROUND(((er.percepcion_bruta*0.0146)*3),2) AS IMP_TRIMESTRAL_PRIMA_BASICA, 
        NVL(cep.MESES_POTENCIAR,0) AS MESES_POTENCIAR, 
        NVL(cep.PORCENTAJE_POTENCIACION,0) AS PORCENTAJE_POTENCIACION,
        NVL(cep.IMPORTE_CONCEPTO_MENSUAL,0) AS IMPORTE_CONCEPTO_MENSUAL,
        NVL(cep.IMPORTE_CONCEPTO_QUINCENAL,0) AS IMPORTE_CONCEPTO_QUINCENAL,
        vQuincenaReporta QUINCENA_REPORTA,
        er.sgmm
     ----1   
      FROM
        ( 
          SELECT 
            n.ID_NOMINA, 
            n.ID_QUINCENA_FK, 
            n.ID_TIPO_NOMINA_FK, 
            ne.ID_EMPLEADO_FK, 
            ne.ID_PLAZA_FK, 
            ne.ID_NOMEMPPLA 
          
          FROM
                (  
                  SELECT 
                      * 
                    FROM 
                      RH_NOMN_CABECERAS_NOMINAS 
                    WHERE 
                      ID_QUINCENA_FK = IDQUINCENA
                      AND ID_TIPO_NOMINA_FK IN (1,2) 
                      AND ID_ESTATUS_NOMINA_FK = 3
                 ) n
                      
          INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA ne ON n.ID_NOMINA = ne.ID_NOMINA_FK
        ) cab
       ----------   
          
          INNER JOIN
            ( Select 
                e.ID_EMPLEADO, 
                e.NUMERO_EMPLEADO, 
                p.ID_TIPO_NOMBRAM_FK, 
                p.id_plaza,
                d.A_PATERNO, 
                d.A_MATERNO, 
                d.NOMBRE, 
                d.RFC, 
                d.CURP, 
                d.FECHA_NACIMIENTO, 
                g.DESCRIPCION_GENERO,
                t.SUELDO_BASE, 
                t.COMPENSACION_GARANTIZADA, 
                (NVL(t.SUELDO_BASE,0) + NVL(t.COMPENSACION_GARANTIZADA,0)) AS percepcion_bruta,
                gj.CLAVE_JERARQUICA,          
                gj.SGMM,
                GRA.GRADO,
                NIV.NIVEL,
                UNADM.DESCRIPCION
              from 
                    RH_NOMN_EMPLEADOS  e
                    
              inner join (SELECT * FROM RH_NOMN_PLAZAS WHERE ID_TIPO_NOMBRAM_FK IN (1,2)) p on e.id_plaza=p.id_plaza
              
              INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = p.ID_ADSCRIPCION_FK
              
              INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNADM ON UNADM.ID_UNIADM = ADS.ID_UNIADM_FK
              
              inner join RH_NOMN_tabuladores t on p.id_tabulador_fk=t.id_tabulador
              inner join RH_NOMN_datos_personales d on e.id_datos_personales=d.id_datos_personales
              INNER JOIN RH_NOMN_CAT_GENEROS g ON d.ID_GENERO=g.ID_GENERO
              INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS gj ON t.ID_GRUPO_JERARQUICO_FK=gj.ID_GRUPO_JERARQUICO
              INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = t.ID_GRADO_JERARQUICO_FK
              INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = t.ID_NIVEL_TABULADOR_FK
            ) er
          ON cab.ID_EMPLEADO_FK= er.ID_EMPLEADO
      LEFT OUTER JOIN
        (
        SELECT 
          ep.ID_CONEMPPLA, 
          ep.ID_NOMEMPPLA_FK, 
          NVL(ep.ID_CONCEPTO_FK,0) AS ID_CONCEPTO_FK, 
          NVL(ep.IMPORTE_CONCEPTO,0) AS IMPORTE_CONCEPTO_QUINCENAL, 
          (NVL(ep.IMPORTE_CONCEPTO,0)*2) AS IMPORTE_CONCEPTO_MENSUAL,
          NVL(sv.IMPORTE,0) AS IMPORTE_SEGURO_VIDA, 
          NVL(sv.MESES_POTENCIAR,0) AS MESES_POTENCIAR, 
          NVL(sv.PORCENTAJE_POTENCIACION,0) AS PORCENTAJE_POTENCIACION,
          sv.ID_EMPLEADO_FK
        FROM
          (SELECT * FROM RH_NOMN_CONPTOS_EMPLE_PLAZA WHERE ID_CONCEPTO_FK IN(40, 63)) ep
          INNER JOIN
            (
            SELECT 
              s.ID_EMPLEADO_FK, 
              s.ID_SEGURO, 
              s.ID_CONCEPTO_FK, 
              s.IMPORTE, 
              c.MESES_POTENCIAR, 
              c.PORCENTAJE_POTENCIACION 
            FROM
              (SELECT * FROM RH_NOMN_SEGUROS WHERE ID_PRESTACION_FK=6 AND ACTIVO = 'A') s
            INNER JOIN RH_NOMN_SEGURO_VIDA v ON s.ID_SEGURO=v.ID_SEGURO
            INNER JOIN RH_NOMN_CAT_POTEN_SEG_VIDA c ON v.ID_POTEN_SEG_VIDA=c.ID_POTEN_SEG_VIDA
            ) sv 
        ON ep.ID_CONCEPTO_FK=sv.ID_CONCEPTO_FK
        ) cep
      
      ON cab.ID_NOMEMPPLA = cep.ID_NOMEMPPLA_FK
      AND cab.ID_EMPLEADO_FK= cep.ID_EMPLEADO_FK
      ORDER BY 
        cab.ID_EMPLEADO_FK, 
        cab.ID_PLAZA_FK
      ;      
  
END SP_REPORTE_SEGVID_MOVS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SEGVID_PAGO_PRIMAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SEGVID_PAGO_PRIMAS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) AS 

  vQuincenaReporta    VARCHAR2(100);
  vNumeroPoliza       VARCHAR2(50);

BEGIN

  -- Obtener el n�mero de poliza actual, de la prestaci�n de Seguro de Vida.
  SELECT 
    POL.NUMERO_POLIZA
  INTO
    vNumeroPoliza
  FROM 
    RH_NOMN_POLIZAS POL
    INNER JOIN RH_NOMN_CAT_PRESTACIONES PRE ON PRE.ID_PRESTACION = POL.ID_PRESTACION
  WHERE 
    PRE.DESC_PRESTACION LIKE '%VIDA%'
    AND POL.ACTIVO = 'A'
  ;  

  -- Quincena en la que se hace el reporte.
  SELECT
    MAX(DECODE((SELECT QUI2.NUMERO_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI2 WHERE QUI2.ID_QUINCENA = IDQUINCENA), 
    1, 'PRIMERA QNA DE ENERO',
    2, 'SEGUNDA QNA DE ENERO',
    3, 'PRIMERA QNA DE FEBRERO',
    4, 'SEGUNDA QNA DE FEBRERO',
    5, 'PRIMERA QNA DE MARZO',
    6, 'SEGUNDA QNA DE MARZO',
    7, 'PRIMERA QNA DE ABRIL',
    8, 'SEGUNDA QNA DE ABRIL',
    9, 'PRIMERA QNA DE MAYO',
    10, 'SEGUNDA QNA DE MAYO',
    11, 'PRIMERA QNA DE JUNIO',
    12, 'SEGUNDA QNA DE JUNIO',
    13, 'PRIMERA QNA DE JULIO',
    14, 'SEGUNDA QNA DE JULIO',
    15, 'PRIMERA QNA DE AGOSTO',
    16, 'SEGUNDA QNA DE AGOSTO',
    17, 'PRIMERA QNA DE SEPTIEMBRE',
    18, 'SEGUNDA QNA DE SEPTIEMBRE',
    19, 'PRIMERA QNA DE OCTUBRE',
    20, 'SEGUNDA QNA DE OCTUBRE',
    21, 'PRIMERA QNA DE NOVIEMBRE',
    22, 'SEGUNDA QNA DE NOVIEMBRE',
    23, 'PRIMERA QNA DE DICIEMBRE',
    24, 'SEGUNDA QNA DE DICIEMBRE')) QUINCENA_REPORTA
  INTO
    vQuincenaReporta
  FROM
    RH_NOMN_CAT_QUINCENAS
  ;  

    OPEN DATOS FOR
      SELECT 
      /*
        cab.ID_NOMINA,  
        cab.ID_QUINCENA_FK,
        cab.ID_TIPO_NOMINA_FK, 
        cab.ID_EMPLEADO_FK, 
        cab.ID_PLAZA_FK, 
        cab.ID_NOMEMPPLA,
        er.ID_EMPLEADO AS ID_EMPLEADO_EMP, 
        er.NUMERO_EMPLEADO, 
        er.ID_TIPO_NOMBRAM_FK, 
        er.id_plaza, 
        NVL(cep.ID_CONEMPPLA,0) AS ID_CONEMPPLA, 
        NVL(cep.ID_NOMEMPPLA_FK,0) AS ID_NOMEMPPLA_FK, 
        NVL(cep.ID_CONCEPTO_FK,0) AS ID_CONCEPTO_FK, 
        NVL(cep.IMPORTE_CONCEPTO_QUINCENAL,0) AS IMPORTE_CONCEPTO_QUINCENAL,
        NVL(cep.IMPORTE_CONCEPTO_MENSUAL,0) AS IMPORTE_CONCEPTO_MENSUAL,
        NVL(cep.IMPORTE_SEGURO_VIDA,0) AS IMPORTE_SEGURO_VIDA,
      */        
        er.A_PATERNO, 
        er.A_MATERNO, 
        er.NOMBRE, 
        er.RFC, 
        er.CURP,
        er.FECHA_NACIMIENTO, 
        DECODE(er.DESCRIPCION_GENERO, 'HOMBRE', 'M', 'MUJER', 'F') SEXO, 
        er.CLAVE_JERARQUICA || er.GRADO || er.NIVEL as NIVEL_TABULAR, 
        er.SGMM AS NIVEL_TABULAR_HOMOLOGADO,
        UPPER(er.DESCRIPCION) UNIDAD_ADMINISTRATIVA,
        'CIUDAD DE MEXICO' ENTIDAD_FEDERATIVA,
        er.SUELDO_BASE,   
        er.percepcion_bruta,
        er.COMPENSACION_GARANTIZADA,    
        0 PRIMA_RIESGO,
        er.percepcion_bruta PERCEPCION_BRUTA_MENSUAL,
        1.46 PORCENTAJE_PRIMA_BASICA,
        ROUND((er.percepcion_bruta*0.0146),2) AS IMPORTE_MENSUAL_PRIMA_BASICA,
        ROUND(((er.percepcion_bruta*0.0146)*3),2) AS IMP_TRIMESTRAL_PRIMA_BASICA, 
        NVL(cep.MESES_POTENCIAR,0) AS MESES_POTENCIAR, 
        NVL(cep.PORCENTAJE_POTENCIACION,0) AS PORCENTAJE_POTENCIACION,
        NVL(cep.IMPORTE_CONCEPTO_MENSUAL,0) AS IMPORTE_CONCEPTO_MENSUAL,
        NVL(cep.IMPORTE_CONCEPTO_QUINCENAL,0) AS IMPORTE_CONCEPTO_QUINCENAL,
        vNumeroPoliza NUMERO_POLIZA,
        vQuincenaReporta QUINCENA_REPORTA
      FROM
        ( SELECT 
            n.ID_NOMINA, 
            n.ID_QUINCENA_FK, 
            n.ID_TIPO_NOMINA_FK, 
            ne.ID_EMPLEADO_FK, 
            ne.ID_PLAZA_FK, 
            ne.ID_NOMEMPPLA 
          FROM
            ( SELECT 
                * 
              FROM 
                RH_NOMN_CABECERAS_NOMINAS 
              WHERE 
                ID_QUINCENA_FK = IDQUINCENA
                AND ID_TIPO_NOMINA_FK IN (1,2) 
                AND ID_ESTATUS_NOMINA_FK = 3) n
          INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA ne ON n.ID_NOMINA=ne.ID_NOMINA_FK) cab
          INNER JOIN
            ( Select 
                e.ID_EMPLEADO, 
                e.NUMERO_EMPLEADO, 
                p.ID_TIPO_NOMBRAM_FK, 
                p.id_plaza,
                d.A_PATERNO, 
                d.A_MATERNO, 
                d.NOMBRE, 
                d.RFC, 
                d.CURP, 
                d.FECHA_NACIMIENTO, 
                g.DESCRIPCION_GENERO,
                t.SUELDO_BASE, 
                t.COMPENSACION_GARANTIZADA, 
                (NVL(t.SUELDO_BASE,0) + NVL(t.COMPENSACION_GARANTIZADA,0)) AS percepcion_bruta,
                gj.CLAVE_JERARQUICA,          
                gj.SGMM,
                GRA.GRADO,
                NIV.NIVEL,
                UNADM.DESCRIPCION
              from
                ( Select 
                    * 
                  from 
                    RH_NOMN_EMPLEADOS 
                  where 
                    ID_ESTATUS_EMPLEADO = 1
                  ) e
              inner join (SELECT * FROM RH_NOMN_PLAZAS WHERE ID_TIPO_NOMBRAM_FK IN (1,2)) p on e.id_plaza=p.id_plaza
              INNER JOIN RH_NOMN_ADSCRIPCIONES ADS ON ADS.ID_ADSCRIPCION = p.ID_ADSCRIPCION_FK
              INNER JOIN RH_NOMN_CAT_UNIDADES_ADMINS UNADM ON UNADM.ID_UNIADM = ADS.ID_UNIADM_FK
              inner join RH_NOMN_tabuladores t on p.id_tabulador_fk=t.id_tabulador
              inner join RH_NOMN_datos_personales d on e.id_datos_personales=d.id_datos_personales
              INNER JOIN RH_NOMN_CAT_GENEROS g ON d.ID_GENERO=g.ID_GENERO
              INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS gj ON t.ID_GRUPO_JERARQUICO_FK=gj.ID_GRUPO_JERARQUICO
              INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = t.ID_GRADO_JERARQUICO_FK
              INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = t.ID_NIVEL_TABULADOR_FK
            ) er
          ON cab.ID_EMPLEADO_FK= er.ID_EMPLEADO
      LEFT OUTER JOIN
        (
        SELECT 
          ep.ID_CONEMPPLA, 
          ep.ID_NOMEMPPLA_FK, 
          NVL(ep.ID_CONCEPTO_FK,0) AS ID_CONCEPTO_FK, 
          NVL(ep.IMPORTE_CONCEPTO,0) AS IMPORTE_CONCEPTO_QUINCENAL, 
          (NVL(ep.IMPORTE_CONCEPTO,0)*2) AS IMPORTE_CONCEPTO_MENSUAL,
          NVL(sv.IMPORTE,0) AS IMPORTE_SEGURO_VIDA, 
          NVL(sv.MESES_POTENCIAR,0) AS MESES_POTENCIAR, 
          NVL(sv.PORCENTAJE_POTENCIACION,0) AS PORCENTAJE_POTENCIACION,          
          sv.ID_EMPLEADO_FK
        FROM
          (SELECT * FROM RH_NOMN_CONPTOS_EMPLE_PLAZA WHERE ID_CONCEPTO_FK IN(40, 63)) ep
          INNER JOIN
            (
            SELECT 
              s.ID_EMPLEADO_FK, 
              s.ID_SEGURO, 
              s.ID_CONCEPTO_FK, 
              s.IMPORTE, 
              c.MESES_POTENCIAR, 
              c.PORCENTAJE_POTENCIACION              
            FROM
              (SELECT * FROM RH_NOMN_SEGUROS WHERE ID_PRESTACION_FK=6 AND ACTIVO = 'A') s
            INNER JOIN RH_NOMN_SEGURO_VIDA v ON s.ID_SEGURO=v.ID_SEGURO
            INNER JOIN RH_NOMN_CAT_POTEN_SEG_VIDA c ON v.ID_POTEN_SEG_VIDA=c.ID_POTEN_SEG_VIDA            
            ) sv 
        ON ep.ID_CONCEPTO_FK=sv.ID_CONCEPTO_FK
        ) cep
      
      ON cab.ID_NOMEMPPLA = cep.ID_NOMEMPPLA_FK
      AND cab.ID_EMPLEADO_FK= cep.ID_EMPLEADO_FK
      ORDER BY 
        cab.ID_EMPLEADO_FK, 
        cab.ID_PLAZA_FK
      ;      
  
END SP_REPORTE_SEGVID_PAGO_PRIMAS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SERICA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SERICA" (IDNOMINA IN NUMBER,ARCHIVO OUT BLOB)
IS

vCABECERA    VARCHAR2(500);
TIPOCONSULTA NUMBER;
TIPONOMINA NUMBER;
EXISTE NUMBER;
IDNOMARCHSERICA NUMBER;
IDQUINCENA NUMBER;

  PROCEDURE CABECERA(IDQUINCENA IN NUMBER, TIPOCONCULTA IN NUMBER,TIPONOMINA IN NUMBER,IDNOMINA IN NUMBER, CAB OUT VARCHAR2)
  IS 
    vQUERY VARCHAR2(25000);
    DATOS VARCHAR2(3000);
    IMPORTES VARCHAR2(15000);
    RAMO        NUMBER := 43;
    CLAVE_SERICA VARCHAR2(10) := '09509001';

    BEGIN   
          vQUERY :='
                    SELECT ''C|''||'''||CLAVE_SERICA||'''||''Q''||(SELECT EJE.VALOR || LPAD(QUIN.NUMERO_QUINCENA,2,0)||DECODE(CABECERA.ID_TIPO_NOMINA_FK,1,''O'',2,''O'',16,''E'',17,''E'',''O'')||(SELECT 1  FROM DUAL)||
                                                                   ''|''||TO_CHAR(QUIN.FECHA_DISPERCION_QUINCENA,''YYYYMMDD'')||''|'' 
                                                               FROM RH_NOMN_CABECERAS_NOMINAS CABECERA
                                                               LEFT JOIN RH_NOMN_CAT_QUINCENAS QUIN ON QUIN.ID_QUINCENA = CABECERA.ID_QUINCENA_FK
                                                               LEFT JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUIN.ID_EJERCICIO_FK
                                                               WHERE ID_NOMINA = '||IDNOMINA||')||'
                    
                           ||RAMO||'||''|''||
                           TOTAL ||''|''||
                           NUMEMPLEADOS ||''|''||
                           PLAZAS ||''|''||
                           P11301_I ||''|''||
                           P12201_I ||''|''||
                           P12301_I ||''|''||
                           P13101_I ||''|''||
                           P13102_I ||''|''||
                           P13401_I ||''|''||
                           P13402_I ||''|''||
                           P13407_I ||''|''||
                           P13408_I ||''|''||
                           P13411_I ||''|''||
                           P15403_I ||''|''||
                           P15402_I ||''|''||
                           P10002_I ||''|''||
                           D20002_I ||''|''||
                           D20003_I ||''|''||
                           D20004_I ||''|''||
                           D20005_I ||''|''||
                           D20006_I ||''|''||
                           D20007_I ||''|''||
                           D20008_I ||''|''||
                           (P11301_I+P12201_I+P12301_I+P13101_I+P13102_I+P13401_I+P13402_I+P13407_I+P13408_I+P13411_I+P15403_I+P15402_I+P10001_I+P10002_I) ||''|''||
                           (D20001_I+D20002_I+D20003_I+D20004_I+D20005_I+D20006_I+D20008_I) ||''|''||
                           ((P11301_I+P12201_I+P12301_I+P13101_I+P13102_I+P13401_I+P13402_I+P13407_I+P13408_I+P13411_I+P15403_I+P15402_I+P10001_I+P10002_I)-(D20001_I+D20002_I+D20003_I+D20004_I+D20005_I+D20006_I+D20008_I))
                    FROM    
                    (
                            SELECT 
                            COUNT(TOTAL) TOTAL,
                            COUNT(NUMEMPLEADO) NUMEMPLEADOS,
                            COUNT(TOTAL) PLAZAS,
                            SUM(P11301_I) P11301_I,
                            SUM(P12201_I) P12201_I,
                            SUM(P12301_I) P12301_I,
                            SUM(P13101_I) P13101_I,
                            SUM(P13102_I) P13102_I,
                            SUM(P13401_I) P13401_I,
                            SUM(P13402_I) P13402_I,
                            SUM(P13407_I) P13407_I,
                            SUM(P13408_I) P13408_I,
                            SUM(P13411_I) P13411_I,
                            SUM(P15403_I) P15403_I,
                            SUM(P15402_I) P15402_I,
                            SUM(P10001_I) P10001_I,
                            SUM(P10002_I) P10002_I,
                            SUM(D20001_I) D20001_I,
                            SUM(D20002_I) D20002_I,
                            SUM(D20003_I) D20003_I,
                            SUM(D20004_I) D20004_I,
                            SUM(D20005_I) D20005_I,
                            SUM(D20006_I) D20006_I,
                            SUM(D20007_I) D20007_I,
                            SUM(D20008_I) D20008_I
                            FROM(
                                   SELECT
                                        ROWNUM TOTAL,
                                        NUMEMPLEADO NUMEMPLEADO,
                                       NVL(P11301_I,0) P11301_I,
                                       NVL(P12201_I,0) P12201_I,
                                       NVL(P12301_I,0) P12301_I,
                                       NVL(P13101_I,0) P13101_I,
                                       NVL(P13102_I,0) P13102_I,
                                       NVL(P13401_I,0) P13401_I,
                                       NVL(P13402_I,0) P13402_I,
                                       NVL(P13407_I,0) P13407_I,
                                       NVL(P13408_I,0) P13408_I,
                                       NVL(P13411_I,0) P13411_I,
                                       NVL(P15403_I,0) P15403_I,
                                       NVL(P15402_I,0) P15402_I,
                                       NVL(P10001_I,0) P10001_I,
                                       NVL(P10002_I,0) P10002_I,
                                       NVL(D20001_I,0) D20001_I,
                                       NVL(D20002_I,0) D20002_I,
                                       NVL(D20003_I,0) D20003_I,
                                       NVL(D20004_I,0) D20004_I,
                                       NVL(D20005_I,0) D20005_I,
                                       NVL(D20006_I,0) D20006_I,
                                       NVL(D20007_I,0) D20007_I,
                                       NVL(D20008_I,0) D20008_I
                            FROM 
                            (
                            SELECT  DISTINCT 
                                            
                                           EM.NUMERO_EMPLEADO NUMEMPLEADO,
                                           NVL(DECODE(UPPER(NOMB.DESCRIPCION),''ESTRUCTURA'',2,''EVENTUAL'',5,''HONORARIOS'',7),0) TCONTRATO,
                                           CASE  WHEN  CPTCAT.CLAVE_CONCEPTO = ''07'' THEN
                                                          11301 
                                                  WHEN CPTCAT.CLAVE_CONCEPTO = ''07E''  OR CPTCAT.CLAVE_CONCEPTO = ''80E'' THEN   
                                                          12201
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''A1'' OR CPTCAT.CLAVE_CONCEPTO = ''A2'' OR CPTCAT.CLAVE_CONCEPTO = ''A3'' OR CPTCAT.CLAVE_CONCEPTO = ''A4'' OR CPTCAT.CLAVE_CONCEPTO = ''A5'' OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''A1E'' OR CPTCAT.CLAVE_CONCEPTO = ''A2E'' OR CPTCAT.CLAVE_CONCEPTO = ''A3E'' OR CPTCAT.CLAVE_CONCEPTO = ''A4E'' OR CPTCAT.CLAVE_CONCEPTO = ''A5E'' THEN   
                                                          13101
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''06'' OR CPTCAT.CLAVE_CONCEPTO = ''06E'' THEN   
                                                          15402
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''38'' OR CPTCAT.CLAVE_CONCEPTO = ''38E'' THEN      
                                                          10002
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''03'' OR CPTCAT.CLAVE_CONCEPTO = ''03E'' THEN      
                                                          20002   
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''64'' OR CPTCAT.CLAVE_CONCEPTO = ''64E'' THEN      
                                                          20005
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''62'' OR CPTCAT.CLAVE_CONCEPTO = ''62P'' OR CPTCAT.CLAVE_CONCEPTO = ''62A'' OR 
                                                              CPTCAT.CLAVE_CONCEPTO = ''6AE'' OR CPTCAT.CLAVE_CONCEPTO = ''62E'' OR
                                                              CPTCAT.CLAVE_CONCEPTO = ''62E'' OR CPTCAT.CLAVE_CONCEPTO = ''6PV'' OR CPTCAT.CLAVE_CONCEPTO = ''62AE'' OR 
                                                              CPTCAT.CLAVE_CONCEPTO = ''6AEE'' THEN
                                                          20006
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''17'' OR CPTCAT.CLAVE_CONCEPTO = ''18'' OR CPTCAT.CLAVE_CONCEPTO = ''17A'' OR 
                                                              CPTCAT.CLAVE_CONCEPTO = ''18A'' OR
                                                              CPTCAT.CLAVE_CONCEPTO = ''17D'' OR CPTCAT.CLAVE_CONCEPTO = ''17E'' THEN
                                                          20007  
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''32'' OR CPTCAT.CLAVE_CONCEPTO = ''24SB'' OR CPTCAT.CLAVE_CONCEPTO = ''24CG''    OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''76'' OR CPTCAT.CLAVE_CONCEPTO = ''30D''  OR CPTCAT.CLAVE_CONCEPTO = ''43''      OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''74'' OR CPTCAT.CLAVE_CONCEPTO = ''30B''  OR CPTCAT.CLAVE_CONCEPTO = ''08''      OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''78'' OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''32E'' OR CPTCAT.CLAVE_CONCEPTO = ''24SBE'' OR CPTCAT.CLAVE_CONCEPTO = ''24CGE'' OR 
                                                        CPTCAT.CLAVE_CONCEPTO = ''76E'' OR CPTCAT.CLAVE_CONCEPTO = ''3DE''  OR CPTCAT.CLAVE_CONCEPTO = ''43E''    OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''74E'' OR CPTCAT.CLAVE_CONCEPTO = ''800''    THEN 
                                                               10001
                                                  WHEN  CPTCAT.CLAVE_CONCEPTO = ''01''  OR CPTCAT.CLAVE_CONCEPTO = ''102'' OR CPTCAT.CLAVE_CONCEPTO = ''140''   OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''199'' OR CPTCAT.CLAVE_CONCEPTO = ''42A''  OR CPTCAT.CLAVE_CONCEPTO = ''42P''  OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''56''  OR CPTCAT.CLAVE_CONCEPTO = ''56L''  OR CPTCAT.CLAVE_CONCEPTO = ''21''   OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''AS''  OR CPTCAT.CLAVE_CONCEPTO = ''77''   OR CPTCAT.CLAVE_CONCEPTO = ''50''   OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''51''  OR CPTCAT.CLAVE_CONCEPTO = ''57''   OR CPTCAT.CLAVE_CONCEPTO = ''73''   OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''75''  OR CPTCAT.CLAVE_CONCEPTO = ''79''   OR CPTCAT.CLAVE_CONCEPTO = ''83''   OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''85''  OR CPTCAT.CLAVE_CONCEPTO = ''78''   OR CPTCAT.CLAVE_CONCEPTO = ''100''  OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''01P''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01SBA'' OR	CPTCAT.CLAVE_CONCEPTO = ''65''        OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''01CGA''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01SBP'' OR	CPTCAT.CLAVE_CONCEPTO = ''01CGP'' OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''01S''	 OR	CPTCAT.CLAVE_CONCEPTO = ''81''	  OR	CPTCAT.CLAVE_CONCEPTO = ''01H''	      OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''82''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01T''	  OR	CPTCAT.CLAVE_CONCEPTO = ''01F''       OR
                            
                                                        CPTCAT.CLAVE_CONCEPTO = ''011''  OR CPTCAT.CLAVE_CONCEPTO = ''012'' OR CPTCAT.CLAVE_CONCEPTO = ''013''      OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''1PE'' OR CPTCAT.CLAVE_CONCEPTO = ''05SBA''  OR CPTCAT.CLAVE_CONCEPTO = ''015CG''  OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''15SBP''  OR CPTCAT.CLAVE_CONCEPTO = ''15CGP''  OR CPTCAT.CLAVE_CONCEPTO = ''10E'' OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''14E''  OR CPTCAT.CLAVE_CONCEPTO = ''19E''   OR CPTCAT.CLAVE_CONCEPTO = ''4AE''    OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''4PE''  OR CPTCAT.CLAVE_CONCEPTO = ''81E''   OR CPTCAT.CLAVE_CONCEPTO = ''82E''    OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''65E''  OR CPTCAT.CLAVE_CONCEPTO = ''21E''   OR CPTCAT.CLAVE_CONCEPTO = ''ASE''    OR	
                                                        CPTCAT.CLAVE_CONCEPTO = ''77E''  OR CPTCAT.CLAVE_CONCEPTO = ''50E''   OR CPTCAT.CLAVE_CONCEPTO = ''51E''    OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''57E''  OR CPTCAT.CLAVE_CONCEPTO = ''73E''   OR CPTCAT.CLAVE_CONCEPTO = ''75E''    OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''83E''  OR CPTCAT.CLAVE_CONCEPTO = ''85E''   OR CPTCAT.CLAVE_CONCEPTO = ''78E''    OR
                                                        CPTCAT.CLAVE_CONCEPTO = ''ASE''  OR CPTCAT.CLAVE_CONCEPTO = ''100E''   
                                                        THEN 
                                                                20001
                                                  END CLAVE_PRESUPUESTAL
                                           ,CPTCAT.CLAVE_CONCEPTO
                                           ,CPTSEM.IMPORTE_CONCEPTO
                            
                                    FROM RH_NOMN_CABECERAS_NOMINAS NOMN ';
                            
                                    IF TIPOCONSULTA = 2 THEN
                                        vQUERY := vQUERY||' 
                                        LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01  EMPLAZ ON EMPLAZ.ID_NOMINA01_FK = NOMN.ID_NOMINA
                                        LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA_01  CPTSEM ON CPTSEM.ID_NOMEMPPLA01_FK = EMPLAZ.ID_NOMEMPPLA01
                                        LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTCAT ON CPTCAT.ID_CONCEPTO = CPTSEM.ID_CONCEPTO01_FK ';
                                    ELSE
                                        vQUERY := vQUERY ||' 
                                        LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA     EMPLAZ ON EMPLAZ.ID_NOMINA_FK = NOMN.ID_NOMINA
                                        LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA     CPTSEM ON CPTSEM.ID_NOMEMPPLA_FK = EMPLAZ.ID_NOMEMPPLA
                                        LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTCAT ON CPTCAT.ID_CONCEPTO = CPTSEM.ID_CONCEPTO_FK ';
                                    END IF;
                            
                                    vQUERY := vQUERY||' 
                                    LEFT JOIN RH_NOMN_EMPLEADOS               EM     ON EM.ID_EMPLEADO = EMPLAZ.ID_EMPLEADO_FK
                                    LEFT JOIN RH_NOMN_DATOS_PERSONALES        DATOS  ON DATOS.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
                                    LEFT JOIN RH_NOMN_CAT_GENEROS             GENERO ON GENERO.ID_GENERO = DATOS.ID_GENERO
                                    LEFT JOIN RH_NOMN_PLAZAS                 PLAZ   ON PLAZ.ID_PLAZA = EMPLAZ.ID_PLAZA_FK
                                    LEFT JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOMB   ON NOMB.ID_TIPO_NOMBRAMIENTO = PLAZ.ID_TIPO_NOMBRAM_FK
                                    LEFT JOIN RH_NOMN_CAT_PARTIDAS_PRESUP     PRES   ON PRES.ID_PARTIDA_PRESUP = CPTCAT.ID_PARTIDA_PRESUP_FK
                                    WHERE CPTCAT.ID_TIPO_CONCEPTO_FK <>3 
                                    AND EMPLAZ.IMPORTE_TOTAL > 0
                                    AND NOMN.ID_NOMINA IN (SELECT ID_NOMINA
                                                                                   FROM RH_NOMN_CABECERAS_NOMINAS
                                                                                   WHERE ID_QUINCENA_FK = '||IDQUINCENA;
                            
                                                                                   IF TIPONOMINA = 1 THEN
                                                                                                        vQUERY := vQUERY||'
                                                                                                        AND ID_TIPO_NOMINA_FK IN (1,2) ';
                            
                                                                                                   ELSE
                                                                                                        vQUERY := vQUERY||'
                                                                                                        AND ID_TIPO_NOMINA_FK IN (16,17,19,20) 
                                                                                                        AND ID_NOMINA = '||IDNOMINA;
                            
                                                                                                   END IF;
                                                                    vQUERY := vQUERY||'    
                                                                                   )
                                    )PIVOT (
                                        MAX(CLAVE_PRESUPUESTAL)CL ,MAX(CLAVE_CONCEPTO)C ,SUM(IMPORTE_CONCEPTO)I FOR (CLAVE_PRESUPUESTAL) IN (11301 P11301 ,12201 P12201,12301 P12301,13101 P13101,13102 P13102,13401 P13401,13402 P13402,13407 P13407,13408 P13408,13411 P13411,15403 P15403,15402 P15402,10001 P10001,10002 P10002,20001 D20001,20002 D20002,20003 D20003,20004 D20004,20005 D20005,20006 D20006,20007 D20007,20008 D20008)
                                    )
                            )
                       )
                ' ;

          --DBMS_OUTPUT.PUT_LINE(vQUERY);
          EXECUTE IMMEDIATE vQUERY INTO IMPORTES;
          CAB := IMPORTES;
          --DBMS_OUTPUT.PUT_LINE(CAB);

  END CABECERA;

  PROCEDURE DETALLE (IDQUINCENA IN NUMBER, TIPOCONSULTA IN NUMBER, TIPONOMINA IN NUMBER,IDNOMINA IN NUMBER, ARCHIVO OUT BLOB)
  IS
    PAGADURIA    NUMBER := 99900;
    NUMRECIBO    NUMBER := 0;
    vQUERY       CLOB;
    DATOS        SYS_REFCURSOR;
    CAB          VARCHAR2(20000);
    ESTATUS      NUMBER;

    ARCHIVOTMP CLOB;
    FILA  VARCHAR2(15000);

    BEGIN

    --DBMS_OUTPUT.PUT_LINE(TIPONOMINA);

        vQUERY := '
      SELECT DETALLE||''|''||NSS||''|''||NOMBRE||''|''||APATERNO||''|''||AMATERNO||''|''||RFC||''|''||CURP||''|''||SEXO||''|''||PAGADURIA||''|''||NUMEMPLEADO||''|''||ROWNUM||''|''||REGIMEN||''|''||TCONTRATO||''|''||';
       
            IF TIPONOMINA <> 1 THEN
                vQUERY := vQUERY||'
               FINICIO||''|''||FFINAL||''|''||';
            END IF;
            vQUERY := vQUERY||'
               PERCP||''|''||DEDUC||''|''||
               P11301_CL||''|''||P11301_C||''|''||P11301_I||''|''||P12201_CL||''|''||P12201_C||''|''||P12201_I||''|''||P12301_CL||''|''||P12301_C||''|''||P12301_I||''|''||P13101_CL||''|''||P13101_C||''|''||P13101_I||''|''||
               P13102_CL||''|''||P13102_C||''|''||P13102_I||''|''||P13401_CL||''|''||P13401_C||''|''||P13401_I||''|''||P13402_CL||''|''||P13402_C||''|''||P13402_I||''|''||P13407_CL||''|''||P13407_C||''|''||P13407_I||''|''||
               P13408_CL||''|''||P13408_C||''|''||P13408_I||''|''||P13411_CL||''|''||P13411_C||''|''||P13411_I||''|''||P15403_CL||''|''||P15403_C||''|''||P15403_I||''|''||P15402_CL||''|''||P15402_C||''|''||P15402_I||''|''||
               P10001_CL||''|''||P10001_C||''|''||P10001_I||''|''||P10002_CL||''|''||P10002_C||''|''||P10002_I||''|''||D20001_CL||''|''||D20001_C||''|''||D20001_I||''|''||D20002_CL||''|''||D20002_C||''|''||D20002_I||''|''||
               D20003_CL||''|''||D20003_C||''|''||D20003_I||''|''||D20004_CL||''|''||D20004_C||''|''||D20004_I||''|''||D20005_CL||''|''||D20005_C||''|''||D20005_I||''|''||D20006_CL||''|''||D20006_C||''|''||D20006_I||''|''||
               D20007_CL||''|''||D20007_C||''|''||D20007_I||''|''||D20008_CL||''|''||D20008_C||''|''||D20008_I
       FROM 
       (
       
       SELECT ''D'' DETALLE,
       NSS,
       NOMBRE,
       APATERNO,
       AMATERNO,
       RFC,
       CURP,
       SEXO,
       '||PAGADURIA||' PAGADURIA,
       NUMEMPLEADO,
       REGIMEN ,
       TCONTRATO,';

       IF TIPONOMINA <> 1 THEN
        vQUERY := vQUERY||'
         (SELECT TO_CHAR(FECHA_INICIO_QUINCENA,''YYYYMMDD'') FROM RH_NOMN_CAT_QUINCENAS WHERE ID_QUINCENA = '||IDQUINCENA||')FINICIO,
         (SELECT TO_CHAR(FECHA_FIN_QUINCENA,''YYYYMMDD'') FROM RH_NOMN_CAT_QUINCENAS WHERE ID_QUINCENA = '||IDQUINCENA||')FFINAL,
        ';

       END IF;

       vQUERY := vQUERY||
       '(NVL(P11301_I,0)+NVL(P12201_I,0)+NVL(P12301_I,0)+NVL(P13101_I,0)+NVL(P13102_I,0)+NVL(P13401_I,0)+NVL(P13402_I,0)+NVL(P13407_I,0)+NVL(P13408_I,0)+NVL(P13411_I,0)+NVL(P15403_I,0)+NVL(P15402_I,0)+NVL(P10001_I,0)+NVL(P10002_I,0)) PERCP,
       (NVL(D20001_I,0)+NVL(D20002_I,0)+NVL(D20003_I,0)+NVL(D20004_I,0)+NVL(D20005_I,0)+NVL(D20006_I,0)+NVL(D20008_I,0)) DEDUC,
       DECODE(P11301_CL,NULL,11301,P11301_CL) P11301_CL,
       P11301_C P11301_C,
       NVL(P11301_I,0) P11301_I,

       DECODE(P12201_CL,NULL,12201,P12201_CL) P12201_CL,
       P12201_C P12201_C,
       NVL(P12201_I,0) P12201_I,

       DECODE(P12301_CL,NULL,12301,P12301_CL) P12301_CL,
       P12301_C P12301_C,
       NVL(P12301_I,0) P12301_I,

       DECODE(P13101_CL,NULL,13101,P13101_CL) P13101_CL, 
       P13101_C P13101_C,
       NVL(P13101_I,0) P13101_I,

       DECODE(P13102_CL,NULL,13102,P13102_CL) P13102_CL,
       P13102_C P13102_C,
       NVL(P13102_I,0) P13102_I,

       DECODE(P13401_CL,NULL,13401,P13401_CL) P13401_CL,
       P13401_C P13401_C,
       NVL(P13401_I,0) P13401_I,

       DECODE(P13402_CL,NULL,13402,P13402_CL) P13402_CL,
       P13402_C P13402_C,
       NVL(P13402_I,0) P13402_I,

       DECODE(P13407_CL,NULL,13407,P13407_CL) P13407_CL,
       P13407_C P13407_C,  
       NVL(P13407_I,0) P13407_I,

       DECODE(P13408_CL,NULL,13408,P13408_CL) P13408_CL,
       P13408_C P13408_C,
       NVL(P13408_I,0) P13408_I,

       DECODE(P13411_CL,NULL,13411,P13411_CL) P13411_CL,
       P13411_C P13411_C,
       NVL(P13411_I,0)P13411_I,

       DECODE(P15403_CL,NULL,15403,P13411_CL) P15403_CL,
       P15403_C P15403_C,
       NVL(P15403_I,0)P15403_I,

       DECODE(P15402_CL,NULL,15402,P13411_CL) P15402_CL,
       P15402_C P15402_C,
       NVL(P15402_I,0) P15402_I,

       DECODE(P10001_CL,NULL,10001,P13411_CL) P10001_CL,
       DECODE(P10001_C,NULL,''PERCP'',''PERCP'')P10001_C ,
       NVL(P10001_I,0) P10001_I,

       DECODE(P10002_CL,NULL,10002,P10002_CL) P10002_CL, 
       P10002_C P10002_C,
       NVL(P10002_I,0)P10002_I,

       DECODE(D20001_CL,NULL,20001,D20001_CL) D20001_CL,
       DECODE(D20001_C,NULL,''DEDUC'',''DEDUC'')D20001_C,
       NVL(D20001_I,0)D20001_I,

       DECODE(D20002_CL,NULL,20002,D20002_CL) D20002_CL,
       D20002_C D20002_C,
       NVL(D20002_I,0)D20002_I,

       DECODE(D20003_CL,NULL,20003,D20003_CL) D20003_CL,
       D20003_C D20003_C,
       NVL(D20003_I,0)D20003_I,

       DECODE(D20004_CL,NULL,20004,D20004_CL) D20004_CL,
       D20004_C D20004_C,
       NVL(D20004_I,0) D20004_I,

       DECODE(D20005_CL,NULL,20005,D20005_CL) D20005_CL,
       D20005_C D20005_C,
       NVL(D20005_I,0) D20005_I,

       DECODE(D20006_CL,NULL,20006,D20006_CL) D20006_CL,
       D20006_C D20006_C,
       NVL(D20006_I,0) D20006_I,

       DECODE(D20007_CL,NULL,20007,D20007_CL) D20007_CL,
       D20007_C D20007_C,
       NVL(D20007_I,0)D20007_I,

       DECODE(D20008_CL,NULL,20008,D20008_CL) D20008_CL,
       D20008_C D20008_C,
       NVL(D20008_I,0)D20008_I
FROM 
(
SELECT  DISTINCT 
                EM.NSS NSS
               ,DATOS.NOMBRE NOMBRE
               ,DATOS.A_PATERNO APATERNO
               ,DATOS.A_MATERNO AMATERNO
               ,REPLACE(DATOS.RFC,''-'','''') RFC
               ,DATOS.CURP CURP
               ,DECODE(GENERO.DESCRIPCION_GENERO,''HOMBRE'',''M'',''MUJER'',''F'') SEXO
               ,TO_NUMBER(EM.NUMERO_EMPLEADO) NUMEMPLEADO
               ,NVL((SELECT AH.REGIMEN_REPARTO + 1
                     FROM RH_NOMN_AHORRO_SOLIDARIO AH
                     WHERE AH.ID_EMPLEADO = EM.ID_EMPLEADO
                     AND AH.FECHA_MODIFICACION IS NULL
                     AND ROWNUM < 2),0) REGIMEN
               ,NVL(DECODE(UPPER(NOMB.DESCRIPCION),''ESTRUCTURA'',2,''EVENTUAL'',5,''HONORARIOS'',7),0) TCONTRATO,
               CASE  WHEN  CPTCAT.CLAVE_CONCEPTO = ''07'' THEN
                              11301 
                      WHEN CPTCAT.CLAVE_CONCEPTO = ''07E''  OR CPTCAT.CLAVE_CONCEPTO = ''80E'' THEN   
                              12201
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''A1'' OR CPTCAT.CLAVE_CONCEPTO = ''A2'' OR CPTCAT.CLAVE_CONCEPTO = ''A3'' OR CPTCAT.CLAVE_CONCEPTO = ''A4'' OR CPTCAT.CLAVE_CONCEPTO = ''A5'' OR
                            CPTCAT.CLAVE_CONCEPTO = ''A1E'' OR CPTCAT.CLAVE_CONCEPTO = ''A2E'' OR CPTCAT.CLAVE_CONCEPTO = ''A3E'' OR CPTCAT.CLAVE_CONCEPTO = ''A4E'' OR CPTCAT.CLAVE_CONCEPTO = ''A5E'' THEN   
                              13101
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''06'' OR CPTCAT.CLAVE_CONCEPTO = ''06E'' THEN   
                              15402
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''38'' OR CPTCAT.CLAVE_CONCEPTO = ''38E'' THEN      
                              10002
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''03'' OR CPTCAT.CLAVE_CONCEPTO = ''03E'' THEN      
                              20002   
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''64'' OR CPTCAT.CLAVE_CONCEPTO = ''64E'' THEN      
                              20005
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''62'' OR CPTCAT.CLAVE_CONCEPTO = ''62P'' OR CPTCAT.CLAVE_CONCEPTO = ''62A'' OR 
                                  CPTCAT.CLAVE_CONCEPTO = ''6AE'' OR CPTCAT.CLAVE_CONCEPTO = ''62E'' OR
                                  CPTCAT.CLAVE_CONCEPTO = ''62E'' OR CPTCAT.CLAVE_CONCEPTO = ''6PV'' OR CPTCAT.CLAVE_CONCEPTO = ''62AE'' OR 
                                  CPTCAT.CLAVE_CONCEPTO = ''6AEE'' THEN
                              20006
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''17'' OR CPTCAT.CLAVE_CONCEPTO = ''18'' OR CPTCAT.CLAVE_CONCEPTO = ''17A'' OR 
                                  CPTCAT.CLAVE_CONCEPTO = ''18A'' OR
                                  CPTCAT.CLAVE_CONCEPTO = ''17D'' OR CPTCAT.CLAVE_CONCEPTO = ''17E'' THEN
                              20007  
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''32'' OR CPTCAT.CLAVE_CONCEPTO = ''24SB'' OR CPTCAT.CLAVE_CONCEPTO = ''24CG''    OR
                            CPTCAT.CLAVE_CONCEPTO = ''76'' OR CPTCAT.CLAVE_CONCEPTO = ''30D''  OR CPTCAT.CLAVE_CONCEPTO = ''43''      OR
                            CPTCAT.CLAVE_CONCEPTO = ''74'' OR CPTCAT.CLAVE_CONCEPTO = ''30B''  OR CPTCAT.CLAVE_CONCEPTO = ''08''      OR
                            CPTCAT.CLAVE_CONCEPTO = ''78'' OR
                            CPTCAT.CLAVE_CONCEPTO = ''32E'' OR CPTCAT.CLAVE_CONCEPTO = ''24SBE'' OR CPTCAT.CLAVE_CONCEPTO = ''24CGE'' OR 
                            CPTCAT.CLAVE_CONCEPTO = ''76E'' OR CPTCAT.CLAVE_CONCEPTO = ''3DE''  OR CPTCAT.CLAVE_CONCEPTO = ''43E''    OR
                            CPTCAT.CLAVE_CONCEPTO = ''74E'' OR CPTCAT.CLAVE_CONCEPTO = ''800''    THEN 
                                   10001
                      WHEN  CPTCAT.CLAVE_CONCEPTO = ''01''  OR CPTCAT.CLAVE_CONCEPTO = ''102'' OR CPTCAT.CLAVE_CONCEPTO = ''140''   OR
                            CPTCAT.CLAVE_CONCEPTO = ''199'' OR CPTCAT.CLAVE_CONCEPTO = ''42A''  OR CPTCAT.CLAVE_CONCEPTO = ''42P''  OR
                            CPTCAT.CLAVE_CONCEPTO = ''56''  OR CPTCAT.CLAVE_CONCEPTO = ''56L''  OR CPTCAT.CLAVE_CONCEPTO = ''21''   OR
                            CPTCAT.CLAVE_CONCEPTO = ''AS''  OR CPTCAT.CLAVE_CONCEPTO = ''77''   OR CPTCAT.CLAVE_CONCEPTO = ''50''   OR	
                            CPTCAT.CLAVE_CONCEPTO = ''51''  OR CPTCAT.CLAVE_CONCEPTO = ''57''   OR CPTCAT.CLAVE_CONCEPTO = ''73''   OR	
                            CPTCAT.CLAVE_CONCEPTO = ''75''  OR CPTCAT.CLAVE_CONCEPTO = ''79''   OR CPTCAT.CLAVE_CONCEPTO = ''83''   OR	
                            CPTCAT.CLAVE_CONCEPTO = ''85''  OR CPTCAT.CLAVE_CONCEPTO = ''78''   OR CPTCAT.CLAVE_CONCEPTO = ''100''  OR	
                            CPTCAT.CLAVE_CONCEPTO = ''01P''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01SBA'' OR	CPTCAT.CLAVE_CONCEPTO = ''65''        OR
                            CPTCAT.CLAVE_CONCEPTO = ''01CGA''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01SBP'' OR	CPTCAT.CLAVE_CONCEPTO = ''01CGP'' OR	
                            CPTCAT.CLAVE_CONCEPTO = ''01S''	 OR	CPTCAT.CLAVE_CONCEPTO = ''81''	  OR	CPTCAT.CLAVE_CONCEPTO = ''01H''	      OR	
                            CPTCAT.CLAVE_CONCEPTO = ''82''	 OR	CPTCAT.CLAVE_CONCEPTO = ''01T''	  OR	CPTCAT.CLAVE_CONCEPTO = ''01F''       OR

                            CPTCAT.CLAVE_CONCEPTO = ''011''  OR CPTCAT.CLAVE_CONCEPTO = ''012'' OR CPTCAT.CLAVE_CONCEPTO = ''013''      OR
                            CPTCAT.CLAVE_CONCEPTO = ''1PE'' OR CPTCAT.CLAVE_CONCEPTO = ''05SBA''  OR CPTCAT.CLAVE_CONCEPTO = ''015CG''  OR
                            CPTCAT.CLAVE_CONCEPTO = ''15SBP''  OR CPTCAT.CLAVE_CONCEPTO = ''15CGP''  OR CPTCAT.CLAVE_CONCEPTO = ''10E'' OR
                            CPTCAT.CLAVE_CONCEPTO = ''14E''  OR CPTCAT.CLAVE_CONCEPTO = ''19E''   OR CPTCAT.CLAVE_CONCEPTO = ''4AE''    OR	
                            CPTCAT.CLAVE_CONCEPTO = ''4PE''  OR CPTCAT.CLAVE_CONCEPTO = ''81E''   OR CPTCAT.CLAVE_CONCEPTO = ''82E''    OR	
                            CPTCAT.CLAVE_CONCEPTO = ''65E''  OR CPTCAT.CLAVE_CONCEPTO = ''21E''   OR CPTCAT.CLAVE_CONCEPTO = ''ASE''    OR	
                            CPTCAT.CLAVE_CONCEPTO = ''77E''  OR CPTCAT.CLAVE_CONCEPTO = ''50E''   OR CPTCAT.CLAVE_CONCEPTO = ''51E''    OR
                            CPTCAT.CLAVE_CONCEPTO = ''57E''  OR CPTCAT.CLAVE_CONCEPTO = ''73E''   OR CPTCAT.CLAVE_CONCEPTO = ''75E''    OR
                            CPTCAT.CLAVE_CONCEPTO = ''83E''  OR CPTCAT.CLAVE_CONCEPTO = ''85E''   OR CPTCAT.CLAVE_CONCEPTO = ''78E''    OR
                            CPTCAT.CLAVE_CONCEPTO = ''ASE''  OR CPTCAT.CLAVE_CONCEPTO = ''100E''   
                            THEN 
                                    20001
                      END CLAVE_PRESUPUESTAL
               ,CPTCAT.CLAVE_CONCEPTO
               ,CPTSEM.IMPORTE_CONCEPTO

        FROM RH_NOMN_CABECERAS_NOMINAS NOMN ';

        IF TIPOCONSULTA = 2 THEN
            vQUERY := vQUERY||' 
            LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01  EMPLAZ ON EMPLAZ.ID_NOMINA01_FK = NOMN.ID_NOMINA
            LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA_01  CPTSEM ON CPTSEM.ID_NOMEMPPLA01_FK = EMPLAZ.ID_NOMEMPPLA01
            LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTCAT ON CPTCAT.ID_CONCEPTO = CPTSEM.ID_CONCEPTO01_FK ';
        ELSE
            vQUERY := vQUERY ||' 
            LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA     EMPLAZ ON EMPLAZ.ID_NOMINA_FK = NOMN.ID_NOMINA
            LEFT JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA     CPTSEM ON CPTSEM.ID_NOMEMPPLA_FK = EMPLAZ.ID_NOMEMPPLA
            LEFT JOIN RH_NOMN_CAT_CONCEPTOS           CPTCAT ON CPTCAT.ID_CONCEPTO = CPTSEM.ID_CONCEPTO_FK ';
        END IF;

        vQUERY := vQUERY||' 
        LEFT JOIN RH_NOMN_EMPLEADOS               EM     ON EM.ID_EMPLEADO = EMPLAZ.ID_EMPLEADO_FK
        LEFT JOIN RH_NOMN_DATOS_PERSONALES        DATOS  ON DATOS.ID_DATOS_PERSONALES = EM.ID_DATOS_PERSONALES
        LEFT JOIN RH_NOMN_CAT_GENEROS             GENERO ON GENERO.ID_GENERO = DATOS.ID_GENERO
        LEFT JOIN RH_NOMN_PLAZAS                 PLAZ   ON PLAZ.ID_PLAZA = EMPLAZ.ID_PLAZA_FK
        LEFT JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO NOMB   ON NOMB.ID_TIPO_NOMBRAMIENTO = PLAZ.ID_TIPO_NOMBRAM_FK
        LEFT JOIN RH_NOMN_CAT_PARTIDAS_PRESUP     PRES   ON PRES.ID_PARTIDA_PRESUP = CPTCAT.ID_PARTIDA_PRESUP_FK
        WHERE CPTCAT.ID_TIPO_CONCEPTO_FK <>3 
        AND EMPLAZ.IMPORTE_TOTAL > 0
        AND NOMN.ID_NOMINA IN (SELECT ID_NOMINA
                                                       FROM RH_NOMN_CABECERAS_NOMINAS
                                                       WHERE ID_QUINCENA_FK = '||IDQUINCENA;

                                                       IF TIPONOMINA = 1 THEN
                                                                            vQUERY := vQUERY||'
                                                                            AND ID_TIPO_NOMINA_FK IN (1,2) ';

                                                                       ELSE
                                                                            vQUERY := vQUERY||'
                                                                            AND ID_TIPO_NOMINA_FK IN (16,17,19,20) 
                                                                            AND ID_NOMINA = '||IDNOMINA;

                                                                       END IF;
                                        vQUERY := vQUERY||'    
                                                       )
        )PIVOT (
            MAX(CLAVE_PRESUPUESTAL)CL ,MAX(CLAVE_CONCEPTO)C ,SUM(IMPORTE_CONCEPTO)I FOR (CLAVE_PRESUPUESTAL) IN (11301 P11301 ,12201 P12201,12301 P12301,13101 P13101,13102 P13102,13401 P13401,13402 P13402,13407 P13407,13408 P13408,13411 P13411,15403 P15403,15402 P15402,10001 P10001,10002 P10002,20001 D20001,20002 D20002,20003 D20003,20004 D20004,20005 D20005,20006 D20006,20007 D20007,20008 D20008)
        )
        ORDER BY  TCONTRATO,NUMEMPLEADO
)';

    --DBMS_OUTPUT.PUT_LINE(vQUERY);
    OPEN DATOS FOR vQUERY;
    DBMS_LOB.CREATETEMPORARY(ARCHIVOTMP,TRUE);
    DBMS_LOB.OPEN (ARCHIVOTMP, DBMS_LOB.LOB_READWRITE);

    CABECERA(IDQUINCENA,TIPOCONSULTA,TIPONOMINA,IDNOMINA,CAB);

    CAB := CAB;
      --DBMS_OUTPUT.PUT_LINE(CAB);
    DBMS_LOB.WRITEAPPEND(ARCHIVOTMP,length(CAB),CAB);

    LOOP
     FETCH DATOS INTO FILA;
        EXIT WHEN DATOS%NOTFOUND;
         FILA := CHR(13)||CHR(10)||FILA;
         DBMS_LOB.WRITEAPPEND(ARCHIVOTMP,length(FILA),FILA);
    END LOOP;

    ARCHIVO:=FN_UTILS_CLOB_TO_BLOB_OTRO(ARCHIVOTMP);

    SELECT ID_ESTATUS_NOMINA_FK INTO ESTATUS
    FROM RH_NOMN_CABECERAS_NOMINAS
    WHERE ID_NOMINA = IDNOMINA;

    IF ESTATUS = 3 THEN
        SELECT NVL(MAX(ID_ARCHIVOSERICA),0)+1 INTO IDNOMARCHSERICA
        FROM RH_NOMN_ARCHIVOSERICA;

        INSERT INTO RH_NOMN_ARCHIVOSERICA VALUES ( IDNOMARCHSERICA,IDNOMINA,ARCHIVO);
    END IF;


    --DBMS_OUTPUT.PUT_LINE('TERMINO');
    DBMS_LOB.CLOSE (ARCHIVOTMP);
    DBMS_LOB.FREETEMPORARY(ARCHIVOTMP);        
    CLOSE DATOS;

    COMMIT;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN 
        DBMS_OUTPUT.PUT_LINE('NOT FOUND');  
    WHEN OTHERS THEN   
       DBMS_OUTPUT.PUT_LINE('OTHES '||SQLERRM);
END DETALLE;

  BEGIN  
      SELECT COUNT (1) INTO EXISTE 
      FROM RH_NOMN_ARCHIVOSERICA
      WHERE ID_NOMINA_FK = IDNOMINA;


      IF EXISTE = 0 THEN

          SELECT DISTINCT CASE WHEN PLZ.ID_NOMINA_FK IS NOT NULL THEN 
                                  1   ---Para ir a la tabla de nominanas cerradas
                               WHEN PLZ1.ID_NOMINA01_FK  IS NOT NULL THEN
                                  2   ---Para ir a las tabla de nominas pendientes
                          END,
                          CASE WHEN NOMN.ID_TIPO_NOMINA_FK = 1 OR NOMN.ID_TIPO_NOMINA_FK = 2 THEN
                                 1
                               WHEN NOMN.ID_TIPO_NOMINA_FK = 16 OR NOMN.ID_TIPO_NOMINA_FK = 17 OR NOMN.ID_TIPO_NOMINA_FK = 19 OR NOMN.ID_TIPO_NOMINA_FK = 20 THEN
                                 2  
                          END 
                          INTO TIPOCONSULTA, TIPONOMINA 
          FROM RH_NOMN_CABECERAS_NOMINAS NOMN
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA PLZ ON PLZ.ID_NOMINA_FK = NOMN.ID_NOMINA
          LEFT JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA_01 PLZ1 ON PLZ1.ID_NOMINA01_FK = NOMN.ID_NOMINA
          WHERE NOMN.ID_NOMINA = IDNOMINA;


          IF TIPOCONSULTA IS NOT NULL THEN


             SELECT DISTINCT ID_QUINCENA_FK INTO IDQUINCENA
             FROM RH_NOMN_CABECERAS_NOMINAS
             WHERE ID_NOMINA = IDNOMINA;

             DETALLE(IDQUINCENA,TIPOCONSULTA,TIPONOMINA,IDNOMINA,ARCHIVO);


          ELSE
            DBMS_OUTPUT.PUT_LINE('LA NOMINA NO EXISTE');
          END IF;
      ELSE
        SELECT ARCHIVO INTO ARCHIVO 
        FROM RH_NOMN_ARCHIVOSERICA
        WHERE ID_NOMINA_FK = IDNOMINA;
      END IF;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);

END SP_REPORTE_SERICA;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SIRI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SIRI" (fechaIni in Date, fechaFin in Date,numeroBimestre in NUMBER, archivo out Blob)
is 

---PreArchivo
SiriFinal CLOB;
vEjercicio  NUMBER;
vExisteArchivo  NUMBER;

--Variables cabecero
vTipoRegistro VARCHAR2(2);
vIdServicio VARCHAR2(2);
vIdOperacion  NUMBER;
vTipoEntidadOrigen  VARCHAR2(2);
vClaveEntidadOrigen VARCHAR2(7);
vTipoEntidadDestino VARCHAR2(2);
vClaceEntidadDestino  VARCHAR2(7);
vFechaTransmision VARCHAR2(9);
vRFC  VARCHAR2(12);
vNombreInstitucion VARCHAR2(130);
vIdCentroPago NUMBER;
vClavePago  NUMBER;
vClavePagaduria NUMBER;
vDomicilio  VARCHAR2(40);
vColonia  VARCHAR2(25);
vDelegacion VARCHAR(25);
vCP NUMBER;
vEntidadFederativa  VARCHAR2(23);
vTelefono NUMBER;
vAltas  NUMBER:=0;
vModificaciones NUMBER:=0;
vBajas  NUMBER:=0;
vTotalRegistros NUMBER:=0;
vFiller VARCHAR2(264);
vAceptado NUMBER; 
vRechazo1 NUMBER;
vRechazo2 NUMBER;
vRechazo3 NUMBER;
Cabecero CLOB;

CURSOR C0 IS
SELECT RPAD('01',2) AS TIPO_REGISTRO, RPAD('09',2) AS ID_SERVICIO, RPAD(96,2) AS ID_OPERACION, RPAD('06',2) AS TIPO_ENTIDAD_ORIGEN,
RPAD('6091352',7) AS CLAVE_ENTIDAD_ORIGEN,RPAD('03',2) AS TIPO_ENTIDAD_DESTINO,RPAD('01',7) AS CLAVE_ENTIDAD_DESTINO, 
RPAD(TO_CHAR(SYSDATE,'YYYYMMDD'),8) AS FECHA_TRANSMISION,RPAD('IFD130924CX1',12) AS RFC_DEPENDENCIA,
RPAD('INSTITUTO FEDERAL DE TELECOMUNICACIONES',130) AS NOMBRE_DEPENDENCIA,RPAD(6091352,7) AS ID_CENTRO_PAGO,
RPAD(43,5) AS CLAVE_RAMO, RPAD(99900,5) AS CLAVE_PAGADURIA,RPAD('INSURGENTES SUR 1143',40) AS DOMICILIO, 
RPAD('NOCHEBUENA',25) AS COLONIA,RPAD('BENITO JUAREZ',25) AS DELEGACION,RPAD(3720,5) AS CP,RPAD('DISTRITO FEDERAL',23) AS ENTIDAD_FEDERATIVA,
RPAD(5550154640,10) AS TELEFONO FROM DUAL;

CURSOR C1 IS
SELECT RPAD(vAltas,9) AS ALTAS, RPAD(vModificaciones,9) AS MODIFICACIONES,RPAD(vBajas,9) AS BAJAS, RPAD(vTotalRegistros,9) AS TOTAL_REGISTROS,
RPAD(' ',264) AS FILLER,RPAD(0,2) AS ACEPTADO, RPAD(0,3) AS RECHAZO_1, RPAD(0,3) AS RECHAZO_2, RPAD(0,3) AS RECHAZO_3 FROM DUAL;


--Variables Anexo A altas
vIdEmpleadoAlta   NUMBER;
vIdPlazaAlta      NUMBER;
vNumeroQuincenaAlta   NUMBER;
vIdQuincenaAlta   NUMBER;
vIdNominaAlta   NUMBER;

vTipoMovimientoAlta VARCHAR2(2);
vRFCAlta  VARCHAR2(14);
vCURPAlta VARCHAR2(19);
vNSSAlta  VARCHAR2(12);
vApellidoPAlta  VARCHAR2(41);
vApellidoMAlta  VARCHAR2(41);
vNombreAlta VARCHAR2(41);
vClavePagAlta NUMBER;
vClaveRepAlta VARCHAR2(21);
vFechaNacimientoAlta  VARCHAR2(9)  ;
vEntidadNacimientoAlta  NUMBER;
vSexoAlta VARCHAR2(5);
vEstadoCivilAlta NUMBER;
vDomicilioAlta  VARCHAR2(61);
vColoniaAlta  VARCHAR2(31);
vMunicipioAlta  VARCHAR2(31);
vCodigoPostalAlta  NUMBER;
vEntidadFederativaAlta  NUMBER;
vNombramientoAlta NUMBER;
vNumeroDeEmpleadoAlta NUMBER;

vClaveERAlta  NUMBER;
vAforeAlta  NUMBER;
vFechaIngresoAlta VARCHAR2(9);
vFechaISSSTEAlta  VARCHAR2(9);
vCreditoFovAlta NUMBER;
 

vDiasQuincenaAlta   NUMBER;
vAcumuladoDiasAlta  NUMBER:=0;
vDiasLicenciaAlta   NUMBER;
vAcumuladoLicenciasAlta   NUMBER:=0;
vDiasFaltasAlta   NUMBER;
vAcumuladoFaltasAlta  NUMBER:=0;
vSueldoAlta   NUMBER;
vAcumuladoSueldoAlta  NUMBER:=0;
vImporteAhorroAlta  NUMBER;
vExisteAhorroAlta   NUMBER;

vUsoFuturoAlta  VARCHAR2(24);
vIndicadorExcepcionAlta VARCHAR2(1);
vFillerAlta VARCHAR2(187);
vResultadoAlta  NUMBER;
vRechazo1Alta NUMBER;
vRechazo2Alta NUMBER;
vRechazo3Alta NUMBER;

vContadorAltas  NUMBER:=0;

DatosAlta VARCHAR2(8000);
SiriAlta CLOB;

CURSOR C0Alta IS 
SELECT ID_NOMINA,ID_QUINCENA_FK,NUMERO_QUINCENA FROM RH_NOMN_CABECERAS_NOMINAS CAB 
INNER JOIN RH_NOMN_CAT_QUINCENAS CQ
ON CAB.ID_QUINCENA_FK = CQ.ID_QUINCENA  
WHERE CQ.FECHA_INICIO_QUINCENA BETWEEN fechaIni AND fechaFin
AND  CQ.FECHA_FIN_QUINCENA BETWEEN fechaIni AND fechaFin
AND (CAB.ID_TIPO_NOMINA_FK=1 OR CAB.ID_TIPO_NOMINA_FK=2);

CURSOR C1Alta IS 
SELECT ID_EMPLEADO, ID_PLAZA FROM RH_NOMN_EMPLEADOS
WHERE FECHA_DE_INGRESO BETWEEN fechaIni AND fechaFin;

CURSOR C2Alta IS
SELECT RPAD(FN_CALCULA_DIAS_LABORADOS(0,vIdEmpleadoAlta,vIdPlazaAlta,vNumeroQuincenaAlta),3) FROM DUAL;
  
CURSOR C3Alta IS
SELECT RPAD(FN_OBTEN_DIAS_LICENCIA(vIdEmpleadoAlta,vIdQuincenaAlta),3) FROM DUAL;
  
CURSOR C4Alta IS
SELECT RPAD(FN_OBTEN_NUMERO_FALTAS(vIdEmpleadoAlta,vIdQuincenaAlta),3) FROM DUAL;
  
CURSOR C5Alta IS
SELECT RPAD(IMPORTE_CONCEPTO,5) FROM RH_NOMN_CONPTOS_EMPLE_PLAZA CEP INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NEP
ON CEP.ID_NOMEMPPLA_FK = NEP.ID_NOMEMPPLA WHERE NEP.ID_EMPLEADO_FK=vIdEmpleadoAlta AND NEP.ID_NOMINA_FK=vIdNominaAlta;
   
CURSOR C6Alta IS
SELECT  RPAD('A',1) AS A, RPAD(DP.RFC,13),RPAD(DP.CURP,18),RPAD(NE.NSS,11),RPAD(DP.A_PATERNO,40),RPAD(DP.A_MATERNO,40),RPAD(DP.NOMBRE,40),
RPAD(99900,5) AS CV_PAG, RPAD(CONCAT(CUA.CLAVE_UNIADM,DP.RFC),20) AS CV_REP,
RPAD(TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'),8),RPAD(DP.ID_ENTIDAD_NACIMIENTO,2),RPAD(SUBSTR(CG.DESCRIPCION_GENERO,1,1),1),
RPAD(DECODE(CEC.ID_ESTADO_CIVIL,2,1,5,1,0),1),
RPAD(CONCAT(DIR.CALLE,CONCAT(' EXT ',CONCAT(DIR.NUM_EXTERIOR,CONCAT(' INT ',DIR.NUM_INTERIOR)))),60),
RPAD(CCOL.DESCRIPCION_COLONIA,30),RPAD(CMUN.DESCRIPCION_MUNICIPIO,30),RPAD(CP.DESCRIPCION_CODIGO_POSTAL,55),
RPAD(CEF.ID_ENTIDAD_FEDERATIVA,2),RPAD(DECODE(NP.ID_TIPO_NOMBRAM_FK,1,2,2,3),1),RPAD(NE.NUMERO_EMPLEADO,10)
FROM RH_NOMN_EMPLEADOS NE
LEFT JOIN RH_NOMN_DATOS_PERSONALES DP ON NE.ID_DATOS_PERSONALES=DP.ID_DATOS_PERSONALES
LEFT JOIN RH_NOMN_PLAZAS NP ON NP.ID_PLAZA = NE.ID_PLAZA
LEFT JOIN RH_NOMN_ADSCRIPCIONES NAD ON NAD.ID_ADSCRIPCION=NP.ID_ADSCRIPCION_FK
LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS CUA ON CUA.ID_UNIADM = NAD.ID_UNIADM_FK
LEFT JOIN RH_NOMN_CAT_GENEROS CG ON CG.ID_GENERO=DP.ID_GENERO
LEFT JOIN RH_NOMN_CAT_ESTADOS_CIVIL CEC ON CEC.ID_ESTADO_CIVIL = DP.ID_ESTADO_CIVIL
LEFT JOIN RH_NOMN_DIRECCIONES DIR ON DIR.ID_DIRECCION = DP.ID_DIRECCION
LEFT JOIN RH_NOMN_CAT_COLONIAS CCOL ON CCOL.ID_COLONIA = DIR.ID_COLONIA
LEFT JOIN RH_NOMN_CAT_MUNICIPIOS CMUN ON CMUN.ID_MUNICIPIO = CCOL.ID_COLONIA
LEFT JOIN RH_NOMN_CAT_CODIGO_POSTAL CP ON CCOL.ID_CODIGO_POSTAL =CP.ID_CODIGO_POSTAL
LEFT JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA CEF ON CEF.ID_ENTIDAD_FEDERATIVA = CMUN.ID_ENTIDAD_FEDERATIVA
WHERE NE.ID_EMPLEADO=vIdEmpleadoAlta;
  
CURSOR C7Alta IS 
SELECT RPAD(2,3) AS CV_ENT_REC, RPAD(0,2) AS AFORE, RPAD(TO_CHAR(NE.FECHA_DE_INGRESO,'YYYYMMDD'),8), RPAD(( SELECT TO_CHAR(MIN(MGF.FECHA_INGRESO),'YYYYMMDD') 
FROM RH_NOMN_MOV_GOB_FEDERAL MGF 
WHERE MGF.ID_EMPLEADO=vIdEmpleadoAlta ),8) AS FECHA_ISSSTE,
RPAD((SELECT CASE WHEN COUNT(ID_MANUALES_TERCEROS)>=1 THEN 1 ELSE 0 END   
FROM RH_NOMN_MANUALES_TERCEROS MT LEFT JOIN RH_NOMN_EMPLEADOS NE ON MT.ID_EMPLEADO= NE.ID_EMPLEADO WHERE (ID_CONCEPTO=52 OR ID_CONCEPTO=51 
OR ID_CONCEPTO=50 OR ID_CONCEPTO=55 OR ID_CONCEPTO=49 OR ID_CONCEPTO=96 OR ID_CONCEPTO=122) 
AND NE.ID_EMPLEADO=vIdEmpleadoAlta),1) AS CRED_FOVISSTE
FROM RH_NOMN_EMPLEADOS NE
WHERE NE.ID_EMPLEADO=vIdEmpleadoAlta;  
  
CURSOR C8Alta IS
SELECT RPAD(' ',24) AS USO_FUTURO, RPAD(0,1) AS INDICADOR_EXCEPCION,RPAD(' ',187) AS FILLER,
RPAD(0,2) AS RESULTADO,RPAD(0,3) AS RECHAZO_1,RPAD(0,3) AS RECHAZO_2,RPAD(0,3) AS RECHAZO_3 FROM DUAL; 

----Variables Anexo A Bajas
vIdEmpleadoBaja   NUMBER;
vIdPlazaBaja      NUMBER;
vNumeroQuincenaBaja   NUMBER;
vIdQuincenaBaja   NUMBER;
vIdNominaBaja   NUMBER;

vTipoMovimientoBaja VARCHAR2(2);
vRFCBaja  VARCHAR2(14);
vCURPBaja VARCHAR2(19);
vNSSBaja  VARCHAR2(12);
vApellidoPBaja  VARCHAR2(41);
vApellidoMBaja  VARCHAR2(41);
vNombreBaja VARCHAR2(41);
vClavePagBaja NUMBER;
vClaveRepBaja VARCHAR2(21);
vFechaNacimientoBaja  VARCHAR2(9)  ;
vEntidadNacimientoBaja  NUMBER;
vSexoBaja VARCHAR2(5);
vEstadoCivilBaja NUMBER;
vDomicilioBaja  VARCHAR2(61);
vColoniaBaja  VARCHAR2(31);
vMunicipioBaja  VARCHAR2(31);
vCodigoPostalBaja  NUMBER;
vEntidadFederativaBaja  NUMBER;
vNombramientoBaja NUMBER;
vNumeroDeEmpleadoBaja NUMBER;

vFechaIngresoBaja VARCHAR2(9);
vFechaBajaBaja  VARCHAR2(9);
vCausaBajaBaja NUMBER;
 
vUsoFuturoBaja  VARCHAR2(24);
vIndicadorExcepcionBaja VARCHAR2(1);
vFillerBaja VARCHAR2(187);
vResultadoBaja  NUMBER;
vRechazo1Baja NUMBER;
vRechazo2Baja NUMBER;
vRechazo3Baja NUMBER;

vContadorBajas  NUMBER:=0;

DatosBaja VARCHAR2(8000);
SiriBaja CLOB;

CURSOR C0Baja IS 
SELECT ID_EMPLEADO, ID_PLAZA FROM RH_NOMN_EMPLEADOS
WHERE FECHA_DE_INGRESO BETWEEN fechaIni AND fechaFin;

  
CURSOR C1Baja IS
SELECT  RPAD('B',1) AS A, RPAD(DP.RFC,13),RPAD(DP.CURP,18),RPAD(NE.NSS,11),RPAD(DP.A_PATERNO,40),RPAD(DP.A_MATERNO,40),RPAD(DP.NOMBRE,40),
RPAD(99900,5) AS CV_PAG, RPAD(CONCAT(CUA.CLAVE_UNIADM,DP.RFC),20) AS CV_REP,
RPAD(TO_CHAR(DP.FECHA_NACIMIENTO,'YYYYMMDD'),8),RPAD(DP.ID_ENTIDAD_NACIMIENTO,2),RPAD(SUBSTR(CG.DESCRIPCION_GENERO,1,1),1),
RPAD(DECODE(CEC.ID_ESTADO_CIVIL,2,1,5,1,0),1),
RPAD(CONCAT(DIR.CALLE,CONCAT(' EXT ',CONCAT(DIR.NUM_EXTERIOR,CONCAT(' INT ',DIR.NUM_INTERIOR)))),60),
RPAD(CCOL.DESCRIPCION_COLONIA,30),RPAD(CMUN.DESCRIPCION_MUNICIPIO,30),RPAD(CP.DESCRIPCION_CODIGO_POSTAL,55),
RPAD(CEF.ID_ENTIDAD_FEDERATIVA,2),RPAD(DECODE(NP.ID_TIPO_NOMBRAM_FK,1,2,2,3),1),RPAD(NE.NUMERO_EMPLEADO,10)
FROM RH_NOMN_EMPLEADOS NE
LEFT JOIN RH_NOMN_DATOS_PERSONALES DP ON NE.ID_DATOS_PERSONALES=DP.ID_DATOS_PERSONALES
LEFT JOIN RH_NOMN_PLAZAS NP ON NP.ID_PLAZA = NE.ID_PLAZA
LEFT JOIN RH_NOMN_ADSCRIPCIONES NAD ON NAD.ID_ADSCRIPCION=NP.ID_ADSCRIPCION_FK
LEFT JOIN RH_NOMN_CAT_UNIDADES_ADMINS CUA ON CUA.ID_UNIADM = NAD.ID_UNIADM_FK
LEFT JOIN RH_NOMN_CAT_GENEROS CG ON CG.ID_GENERO=DP.ID_GENERO
LEFT JOIN RH_NOMN_CAT_ESTADOS_CIVIL CEC ON CEC.ID_ESTADO_CIVIL = DP.ID_ESTADO_CIVIL
LEFT JOIN RH_NOMN_DIRECCIONES DIR ON DIR.ID_DIRECCION = DP.ID_DIRECCION
LEFT JOIN RH_NOMN_CAT_COLONIAS CCOL ON CCOL.ID_COLONIA = DIR.ID_COLONIA
LEFT JOIN RH_NOMN_CAT_MUNICIPIOS CMUN ON CMUN.ID_MUNICIPIO = CCOL.ID_COLONIA
LEFT JOIN RH_NOMN_CAT_CODIGO_POSTAL CP ON CCOL.ID_CODIGO_POSTAL =CP.ID_CODIGO_POSTAL
LEFT JOIN RH_NOMN_CAT_ENTIDAD_FEDERATIVA CEF ON CEF.ID_ENTIDAD_FEDERATIVA = CMUN.ID_ENTIDAD_FEDERATIVA
WHERE NE.ID_EMPLEADO=vIdEmpleadoBaja;
  
CURSOR C2Baja IS 
SELECT  RPAD(TO_CHAR(NE.FECHA_DE_INGRESO,'YYYYMMDD'),8),RPAD(TO_CHAR(NE.FECHA_BAJA,'YYYYMMDD'),8) ,
RPAD(DECODE(NE.ID_CAUSA_BAJA,1,5,2,3),1)
FROM RH_NOMN_EMPLEADOS NE
WHERE NE.ID_EMPLEADO=vIdEmpleadoBaja;  
  
CURSOR C3Baja IS
SELECT RPAD(' ',24) AS USO_FUTURO, RPAD(0,1) AS INDICADOR_EXCEPCION,RPAD(' ',187) AS FILLER,
RPAD(0,2) AS RESULTADO,RPAD(0,3) AS RECHAZO_1,RPAD(0,3) AS RECHAZO_2,RPAD(0,3) AS RECHAZO_3 FROM DUAL;

BEGIN

SELECT ID_EJERCICIO INTO vEjercicio FROM RH_NOMN_CAT_EJERCICIOS WHERE VALOR= (SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL) AND ACTIVO=1;
---Proceso Cabecero
OPEN C0;
LOOP
FETCH C0 INTO 
vTipoRegistro, vIdServicio, vIdOperacion, vTipoEntidadOrigen, vClaveEntidadOrigen, vTipoEntidadDestino, 
vClaceEntidadDestino, vFechaTransmision, vRFC, vNombreInstitucion, vIdCentroPago, vClavePago,  
vClavePagaduria, vDomicilio, vColonia, vDelegacion, vCP, vEntidadFederativa, vTelefono ;
EXIT WHEN C0%NOTFOUND;
Cabecero:=vTipoRegistro||vIdServicio||vIdOperacion||
vTipoEntidadOrigen||vClaveEntidadOrigen||vTipoEntidadDestino||
vClaceEntidadDestino||vFechaTransmision||vRFC||vNombreInstitucion
||vIdCentroPago||vClavePago||vClavePagaduria||
vDomicilio||vColonia||vDelegacion||vCP||
vEntidadFederativa||vTelefono||chr(13);
END LOOP;
CLOSE C0;
--DBMS_OUTPUT.PUT_LINE(Cabecero);

---Proceso Anexo A Altas
OPEN C1Alta;
LOOP 
FETCH C1Alta INTO vIdEmpleadoAlta,vIdPlazaAlta;
EXIT WHEN C1Alta%NOTFOUND;

OPEN C6Alta;
LOOP
FETCH C6Alta INTO vTipoMovimientoAlta,vRFCAlta,vCURPAlta,vNSSAlta,vApellidoPAlta,vApellidoMAlta,vNombreAlta,
vClavePagAlta,vClaveRepAlta,vFechaNacimientoAlta,vEntidadNacimientoAlta,vSexoAlta,vEstadoCivilAlta,vDomicilioAlta,
vColoniaAlta,vMunicipioAlta,vCodigoPostalAlta,vEntidadFederativaAlta,vNombramientoAlta,vNumeroDeEmpleadoAlta;
EXIT WHEN C6Alta%NOTFOUND;
DatosAlta:=vTipoMovimientoAlta||vRFCAlta||vCURPAlta||vNSSAlta||
vApellidoPAlta||vApellidoMAlta||vNombreAlta||vClavePagAlta||
vClaveRepAlta||vFechaNacimientoAlta||vEntidadNacimientoAlta||vSexoAlta||
vEstadoCivilAlta||vDomicilioAlta||vColoniaAlta||vMunicipioAlta||
vCodigoPostalAlta||vEntidadFederativaAlta||vNombramientoAlta||vNumeroDeEmpleadoAlta;
END LOOP;
CLOSE C6Alta;

OPEN C7Alta;
LOOP
FETCH C7Alta INTO vClaveERAlta, vAforeAlta ,vFechaIngresoAlta ,vFechaISSSTEAlta  ,vCreditoFovAlta ;
EXIT WHEN C7Alta%NOTFOUND;
IF(vFechaISSSTEAlta IS NULL)THEN
vFechaISSSTEAlta:=vFechaIngresoAlta;
END IF;
DatosAlta:=DatosAlta||vClaveERAlta|| vAforeAlta||vFechaIngresoAlta||vFechaISSSTEAlta||vCreditoFovAlta;
END LOOP;
CLOSE C7Alta;

OPEN C0Alta;
LOOP
FETCH C0Alta INTO vIdNominaAlta,vIdQuincenaAlta, vNumeroQuincenaAlta ;
EXIT WHEN C0Alta%NOTFOUND;

OPEN C2Alta;
LOOP
FETCH C2Alta INTO vDiasQuincenaAlta;
EXIT WHEN C2Alta%NOTFOUND;
vAcumuladoDiasAlta:=vDiasQuincenaAlta;
END LOOP;
CLOSE C2Alta;

DatosAlta:=DatosAlta||vAcumuladoDiasAlta;

OPEN C3Alta;
LOOP
FETCH C3Alta INTO vDiasLicenciaAlta;
EXIT WHEN C3Alta%NOTFOUND;
vAcumuladoLicenciasAlta:=vDiasLicenciaAlta;
END LOOP;
CLOSE C3Alta; 

DatosAlta:=DatosAlta||vAcumuladoLicenciasAlta;

OPEN C4Alta;
LOOP
FETCH C4Alta INTO vDiasFaltasAlta;
EXIT WHEN C4Alta%NOTFOUND;
vAcumuladoFaltasAlta:=vDiasFaltasAlta;
END LOOP;
CLOSE C4Alta; 

DatosAlta:=DatosAlta|| vAcumuladoFaltasAlta;

OPEN C5Alta;
LOOP
FETCH C5Alta INTO vSueldoAlta;
EXIT WHEN C5Alta%NOTFOUND;
vAcumuladoSueldoAlta:=vSueldoAlta;
END LOOP;
CLOSE C5Alta; 

DatosAlta:=DatosAlta|| vAcumuladoSueldoAlta;

END LOOP;
CLOSE C0Alta;

SELECT RPAD(CASE  WHEN COUNT(ID_AHORRO_SOLIDARIO)>=1 THEN 1 ELSE 0 END,1) INTO vExisteAhorroAlta 
FROM RH_NOMN_AHORRO_SOLIDARIO WHERE ID_EMPLEADO=vIdEmpleadoAlta AND ACTIVO=1;
IF(vExisteAhorroAlta>0)THEN
SELECT RPAD(IMPORTE,5) INTO vImporteAhorroAlta  FROM RH_NOMN_AHORRO_SOLIDARIO WHERE ID_EMPLEADO=vIdEmpleadoAlta AND ACTIVO=1;
END IF;

DatosAlta:=DatosAlta||vExisteAhorroAlta||vImporteAhorroAlta;

OPEN C8Alta;
LOOP
FETCH C8Alta INTO vUsoFuturoAlta, vIndicadorExcepcionAlta, vFillerAlta, vResultadoAlta, vRechazo1Alta, vRechazo2Alta, vRechazo3Alta;
EXIT WHEN C8Alta%NOTFOUND;

END LOOP;
CLOSE C8Alta;

DatosAlta:=DatosAlta||vUsoFuturoAlta||vIndicadorExcepcionAlta||vFillerAlta||vResultadoAlta||vRechazo1Alta||vRechazo2Alta||vRechazo3Alta||chr(13) ;

SiriAlta:=SiriAlta||DatosAlta;
vContadorAltas:=vContadorAltas+1;

END LOOP;
CLOSE C1Alta;
--DBMS_OUTPUT.PUT_LINE(SiriAlta);

---Proceso Anexo A Bajas

OPEN C0Baja;
LOOP 
FETCH C0Baja INTO vIdEmpleadoBaja,vIdPlazaBaja;
EXIT WHEN C0Baja%NOTFOUND;

OPEN C1Baja;
LOOP
FETCH C1Baja INTO vTipoMovimientoBaja,vRFCBaja,vCURPBaja,vNSSBaja,vApellidoPBaja,vApellidoMBaja,vNombreBaja,
vClavePagBaja,vClaveRepBaja,vFechaNacimientoBaja,vEntidadNacimientoBaja,vSexoBaja,vEstadoCivilBaja,vDomicilioBaja,
vColoniaBaja,vMunicipioBaja,vCodigoPostalBaja,vEntidadFederativaBaja,vNombramientoBaja,vNumeroDeEmpleadoBaja;
EXIT WHEN C1Baja%NOTFOUND;
DatosBaja:=vTipoMovimientoBaja||vRFCBaja||vCURPBaja||vNSSBaja||
vApellidoPBaja||vApellidoMBaja||vNombreBaja||vClavePagBaja||
vClaveRepBaja||vFechaNacimientoBaja||vEntidadNacimientoBaja||vSexoBaja||
vEstadoCivilBaja||vDomicilioBaja||vColoniaBaja||vMunicipioBaja||
vCodigoPostalBaja||vEntidadFederativaBaja||vNombramientoBaja||vNumeroDeEmpleadoBaja;
END LOOP;
CLOSE C1Baja;

OPEN C2Baja;
LOOP
FETCH C2Baja INTO vFechaIngresoBaja ,vFechaBajaBaja  ,vCausaBajaBaja ;
EXIT WHEN C2Baja%NOTFOUND;
DatosBaja:=DatosBaja||vFechaIngresoBaja ||vFechaBajaBaja||vCausaBajaBaja ;
END LOOP;
CLOSE C2Baja;

OPEN C3Baja;
LOOP
FETCH C3Baja INTO vUsoFuturoBaja, vIndicadorExcepcionBaja, vFillerBaja, vResultadoBaja, vRechazo1Baja, vRechazo2Baja, vRechazo3Baja;
EXIT WHEN C3Baja%NOTFOUND;
END LOOP;
CLOSE C3Baja;

DatosBaja:=DatosBaja||vUsoFuturoBaja||vIndicadorExcepcionBaja||vFillerBaja||vResultadoBaja||vRechazo1Baja||vRechazo2Baja||vRechazo3Baja||chr(13) ;
   
SiriBaja:=SiriBaja||DatosBaja;
vContadorBajas:=vContadorBajas+1;
END LOOP;
CLOSE C0Baja;

--DBMS_OUTPUT.PUT_LINE(SiriBaja);
vAltas:=vContadorAltas;
vBajas:=vContadorBajas;
vTotalRegistros:=vAltas+vBajas;

DBMS_OUTPUT.PUT_LINE(vAltas);
DBMS_OUTPUT.PUT_LINE(vBajas);
DBMS_OUTPUT.PUT_LINE(vTotalRegistros);
OPEN C1;
LOOP
FETCH C1 INTO vAltas,vModificaciones,
vBajas,vTotalRegistros,vFiller,vAceptado,
vRechazo1,vRechazo2,vRechazo3;
EXIT WHEN C1%NOTFOUND;
Cabecero:=Cabecero||vAltas||vModificaciones||
vBajas||vTotalRegistros||vFiller||vAceptado||
vRechazo1||vRechazo2||vRechazo3||chr(13);
END LOOP;
CLOSE C1;

---Se juntan los resultados
SiriFinal:=Cabecero||chr(13)||SiriAlta||chr(13)||SiriBaja;
--DBMS_OUTPUT.PUT_LINE(SiriFinal);

archivo := FN_UTILS_CLOB_TO_BLOB(SiriFinal);
SELECT COUNT(ID_ARCHIVOSIRI) INTO vExisteArchivo FROM RH_NOMN_ARCHIVOSIRI WHERE ID_BIMESTRE=numeroBimestre AND ID_EJERCICIO=vEjercicio;
IF(vExisteArchivo=0)THEN
INSERT INTO RH_NOMN_ARCHIVOSIRI (ID_ARCHIVOSIRI,ID_BIMESTRE,ID_EJERCICIO,ARCHIVO) VALUES (sec_reporte_siri.nextval,numeroBimestre,vEjercicio,archivo);
COMMIT;
ELSE
UPDATE RH_NOMN_ARCHIVOSIRI SET ARCHIVO=archivo WHERE ID_BIMESTRE=numeroBimestre AND ID_EJERCICIO=vEjercicio;
COMMIT;
END IF;

end SP_REPORTE_SIRI;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SRC_ESTRUC_CORRIDA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SRC_ESTRUC_CORRIDA" (DATOS OUT SYS_REFCURSOR) AS 
  
  vIdQuincenaActual       NUMBER;
  vIdConceptoSegResCiv    NUMBER;
  vIdEjercicioActual      NUMBER;
  vCantidadRegistros      NUMBER;

BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil estructura.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegResCiv
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '85'
  ;    
  
  -- Validar que por lo menos haya una n�mina cerrada, para realizar la busqueda.
  SELECT 
    COUNT(CAB.ID_QUINCENA_FK)
  INTO
    vCantidadRegistros
  FROM 
    RH_NOMN_CABECERAS_NOMINAS CAB
    INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK    
  WHERE
    CAB.ID_TIPO_NOMINA_FK = 1
    AND CAB.ID_ESTATUS_NOMINA_FK = 3
    AND CAB.ID_EJERCICIO_FK = 
    (
      SELECT
        ID_EJERCICIO
      FROM
        RH_NOMN_CAT_EJERCICIOS
      WHERE
        VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
    )    
  ORDER BY
    CAB.ID_QUINCENA_FK DESC
  ;
  
  IF vCantidadRegistros > 0 THEN
  
    -- Obtiene el ID de la quincena de la n�mina estructura cerrada m�s reciente del ejercicio actual.
    SELECT
      ID_QUINCENA,
      ID_EJERCICIO
    INTO
      vIdQuincenaActual,
      vIdEjercicioActual  
    FROM
    (
      SELECT 
        CAB.ID_QUINCENA_FK ID_QUINCENA,
        CAB.ID_EJERCICIO_FK ID_EJERCICIO
      FROM 
        RH_NOMN_CABECERAS_NOMINAS CAB
        INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK    
      WHERE
        CAB.ID_TIPO_NOMINA_FK = 1
        AND CAB.ID_ESTATUS_NOMINA_FK = 3
        AND CAB.ID_EJERCICIO_FK = 
        (
          SELECT
            ID_EJERCICIO
          FROM
            RH_NOMN_CAT_EJERCICIOS
          WHERE
            VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
        )    
      ORDER BY
        CAB.ID_QUINCENA_FK DESC
    )
    WHERE 
      ROWNUM = 1 
    ; 
    
    OPEN DATOS FOR  
      SELECT  
          ROWNUM NUMERO_REGISTRO,
          ID_EMPLEADO_FK ID_EMPLEADO_FK,
          NOMBRE || ' ' || NVL(A_PATERNO,' ') || ' ' || NVL(A_MATERNO,' ') NOMBRE1,
          CLAVE_JERARQUICA || GRADO || NIVEL NIVEL1,
          SUBSTR(ID_CONF_PERIODOS_SRC,2,4) PERIODO1,
          DESCRIPCION_NIVEL_POTENCIA_SRC RIESGO1,
          ROUND((PRIMA_ANUAL_NIVEL_POTENCIA_SRC * ((100 - PORCENTAJE_DESCUENTO)/ 100)), 2) COSTO1,
          QUI1,
          QUI2,
          QUI3,
          QUI4,
          QUI5,
          QUI6,
          QUI7,
          QUI8,
          QUI9,
          QUI10,
          QUI11,
          QUI12,
          QUI13,
          QUI14,
          QUI15,
          QUI16,
          QUI17,
          QUI18,
          QUI19,
          QUI20,
          QUI21,
          QUI22,
          QUI23,
          QUI24
      FROM
      (
        SELECT     
          NOMEMPPLA.ID_EMPLEADO_FK AS ID_EMPLEADO_FK,
          QUI.NUMERO_QUINCENA AS NUMERO_QUINCENA,
          CONEMPPLA.IMPORTE_CONCEPTO AS IMPORTE_CONCEPTO,
          DATPER.NOMBRE AS NOMBRE,
          DATPER.A_PATERNO AS A_PATERNO,
          DATPER.A_MATERNO AS A_MATERNO,
          GRU.CLAVE_JERARQUICA AS CLAVE_JERARQUICA,
          GRA.GRADO AS GRADO,
          NIV.NIVEL AS NIVEL,
          CONPE.ID_CONF_PERIODOS_SRC AS ID_CONF_PERIODOS_SRC,
          NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC AS DESCRIPCION_NIVEL_POTENCIA_SRC,
          NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC AS PRIMA_ANUAL_NIVEL_POTENCIA_SRC,
          CONPE.PORCENTAJE_DESCUENTO AS PORCENTAJE_DESCUENTO
        FROM
          RH_NOMN_CABECERAS_NOMINAS CAB
          INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
          INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON CAB.ID_NOMINA = NOMEMPPLA.ID_NOMINA_FK
          INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
          INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
          INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
          INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
          INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
          INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
          INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
          INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK    
          INNER JOIN RH_NOMN_SEGUROS SEG ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
          INNER JOIN RH_NOMN_SEG_RESP_CIVI SEGRES ON SEGRES.ID_SEGURO = SEG.ID_SEGURO
          INNER JOIN RH_NOMN_CONF_PERIODOS_SRC CONPE ON CONPE.ID_CONF_PERIODOS_SRC = SEGRES.ID_CONF_PERIODOS_SRC
          INNER JOIN RH_NOMN_CAT_PERIODOS_SRC CATPE ON CATPE.ID_PERIODO_SRC = CONPE.ID_PERIODO_SRC
          INNER JOIN RH_NOMN_NIVELES_POTENCIA_SRC NIVPO ON NIVPO.ID_NIVEL_POTENCIA_SRC = SEGRES.ID_NIVEL_POTENCIA_SRC
        WHERE  
          CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegResCiv        
          AND CAB.ID_EJERCICIO_FK = vIdEjercicioActual
          AND SEGRES.FECHA_INICIO <= (SELECT QUI.FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI WHERE QUI.ID_QUINCENA = vIdQuincenaActual)
          AND (SEGRES.FECHA_FIN IS NULL OR SEGRES.FECHA_FIN >= (SELECT QUI.FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI WHERE QUI.ID_QUINCENA = vIdQuincenaActual))        
        GROUP BY
          CAB.ID_NOMINA, 
          NOMEMPPLA.ID_EMPLEADO_FK, 
          QUI.NUMERO_QUINCENA, 
          CONEMPPLA.IMPORTE_CONCEPTO,
          DATPER.NOMBRE,
          DATPER.A_PATERNO,
          DATPER.A_MATERNO,
          GRU.CLAVE_JERARQUICA,
          GRA.GRADO,
          GRA.GRADO,
          NIV.NIVEL,
          CONPE.ID_CONF_PERIODOS_SRC,
          NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC,
          NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC,
          CONPE.PORCENTAJE_DESCUENTO
        ORDER BY
          CAB.ID_NOMINA    
      )
      PIVOT
      (
        MAX(IMPORTE_CONCEPTO)
      FOR
        NUMERO_QUINCENA IN (1 QUI1, 2 QUI2, 3 QUI3, 4 QUI4, 5 QUI5, 6 QUI6, 7 QUI7, 8 QUI8,
                            9 QUI9, 10 QUI10, 11 QUI11, 12 QUI12, 13 QUI13, 14 QUI14, 15 QUI15,
                            16 QUI16, 17 QUI17, 18 QUI18, 19 QUI19, 20 QUI20, 21 QUI21, 22 QUI22,
                            23 QUI23, 24 QUI24)
      )
      ; 
     
  ELSE
  
    OPEN DATOS FOR  
      SELECT  
          0 NUMERO_REGISTRO,
          0 ID_EMPLEADO_FK,
          '0' NOMBRE1,
          '0' NIVEL1,
          '0' PERIODO1,
          '0' RIESGO1,
          0 COSTO1,
          0 QUI1,
          0 QUI2,
          0 QUI3,
          0 QUI4,
          0 QUI5,
          0 QUI6,
          0 QUI7,
          0 QUI8,
          0 QUI9,
          0 QUI10,
          0 QUI11,
          0 QUI12,
          0 QUI13,
          0 QUI14,
          0 QUI15,
          0 QUI16,
          0 QUI17,
          0 QUI18,
          0 QUI19,
          0 QUI20,
          0 QUI21,
          0 QUI22,
          0 QUI23,
          0 QUI24
      FROM 
        DUAL;
        
  END IF;    
    
END SP_REPORTE_SRC_ESTRUC_CORRIDA;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SRC_ESTRUCT_PAGOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SRC_ESTRUCT_PAGOS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) AS 

  vIdConceptoSegResCiv    NUMBER;
  
BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil estructura.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegResCiv
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '85'
  ;
  
  OPEN DATOS FOR
    SELECT
      ROWNUM NUMERO_REGISTRO,
      EMP.ID_EMPLEADO ID_EMPLEADO,      
      DATPER.NOMBRE || ' ' || NVL(DATPER.A_PATERNO, ' ') || ' ' || NVL(DATPER.A_MATERNO, ' ') NOMBRE,
      GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL NIVEL,
      SUBSTR(CONPE.ID_CONF_PERIODOS_SRC,2,4) PERIODO,
      NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC RIESGO,
      ROUND((NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC * ((100 - CONPE.PORCENTAJE_DESCUENTO)/ 100)), 2) COSTO,
      CONEMPPLA.IMPORTE_CONCEPTO DES_QUIN,
      'Base de datos Qna. ' || QUI.NUMERO_QUINCENA || '/' || EJE.VALOR CABECERO
    FROM
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON CAB.ID_NOMINA = NOMEMPPLA.ID_NOMINA_FK
      INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
      INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
      INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK    
      INNER JOIN RH_NOMN_SEGUROS SEG ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
      INNER JOIN RH_NOMN_SEG_RESP_CIVI SEGRES ON SEGRES.ID_SEGURO = SEG.ID_SEGURO
      INNER JOIN RH_NOMN_CONF_PERIODOS_SRC CONPE ON CONPE.ID_CONF_PERIODOS_SRC = SEGRES.ID_CONF_PERIODOS_SRC
      INNER JOIN RH_NOMN_CAT_PERIODOS_SRC CATPE ON CATPE.ID_PERIODO_SRC = CONPE.ID_PERIODO_SRC
      INNER JOIN RH_NOMN_NIVELES_POTENCIA_SRC NIVPO ON NIVPO.ID_NIVEL_POTENCIA_SRC = SEGRES.ID_NIVEL_POTENCIA_SRC
    WHERE
      CAB.ID_QUINCENA_FK = IDQUINCENA
      AND CAB.ID_TIPO_NOMINA_FK = 1
      AND CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegResCiv
      AND SEG.ACTIVO = 'A'
    ;  

END SP_REPORTE_SRC_ESTRUCT_PAGOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SRC_EVENT_CORRIDA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SRC_EVENT_CORRIDA" (DATOS OUT SYS_REFCURSOR) IS

  vIdQuincenaActual       NUMBER;
  vIdConceptoSegResCiv    NUMBER;
  vIdEjercicioActual      NUMBER;
  vCantidadRegistros      NUMBER;

BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil eventual.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegResCiv
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '85E'
  ;
  
  -- Validar que por lo menos haya una n�mina cerrada, para realizar la busqueda.
  SELECT 
    COUNT(CAB.ID_QUINCENA_FK)
  INTO
    vCantidadRegistros
  FROM 
    RH_NOMN_CABECERAS_NOMINAS CAB
    INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK    
  WHERE
    CAB.ID_TIPO_NOMINA_FK = 2
    AND CAB.ID_ESTATUS_NOMINA_FK = 3
    AND CAB.ID_EJERCICIO_FK = 
    (
      SELECT
        ID_EJERCICIO
      FROM
        RH_NOMN_CAT_EJERCICIOS
      WHERE
        VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
    )    
  ORDER BY
    CAB.ID_QUINCENA_FK DESC
  ;
  
  IF vCantidadRegistros > 0 THEN  
  
    -- Obtiene el ID de la quincena de la n�mina eventual cerrada m�s reciente del ejercicio actual.
    SELECT
      ID_QUINCENA,
      ID_EJERCICIO
    INTO
      vIdQuincenaActual,
      vIdEjercicioActual  
    FROM
    (
      SELECT 
        CAB.ID_QUINCENA_FK ID_QUINCENA,
        CAB.ID_EJERCICIO_FK ID_EJERCICIO
      FROM 
        RH_NOMN_CABECERAS_NOMINAS CAB
        INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = CAB.ID_EJERCICIO_FK    
      WHERE
        CAB.ID_TIPO_NOMINA_FK = 2
        AND CAB.ID_ESTATUS_NOMINA_FK = 3
        AND CAB.ID_EJERCICIO_FK = 
        (
          SELECT
            ID_EJERCICIO
          FROM
            RH_NOMN_CAT_EJERCICIOS
          WHERE
            VALOR = ( SELECT TO_CHAR(SYSDATE,'YYYY') FROM DUAL)
        )    
      ORDER BY
        CAB.ID_QUINCENA_FK DESC
    )
    WHERE 
      ROWNUM = 1 
    ;   
    
    OPEN DATOS FOR  
      SELECT  
          ROWNUM NUMERO_REGISTRO,
          ID_EMPLEADO_FK ID_EMPLEADO_FK,
          NOMBRE || ' ' || NVL(A_PATERNO,' ') || ' ' || NVL(A_MATERNO,' ') NOMBRE1,
          CLAVE_JERARQUICA || GRADO || NIVEL NIVEL1,
          SUBSTR(ID_CONF_PERIODOS_SRC,2,4) PERIODO1,
          DESCRIPCION_NIVEL_POTENCIA_SRC RIESGO1,
          ROUND((PRIMA_ANUAL_NIVEL_POTENCIA_SRC * ((100 - PORCENTAJE_DESCUENTO)/ 100)), 2) COSTO1,
          QUI1,
          QUI2,
          QUI3,
          QUI4,
          QUI5,
          QUI6,
          QUI7,
          QUI8,
          QUI9,
          QUI10,
          QUI11,
          QUI12,
          QUI13,
          QUI14,
          QUI15,
          QUI16,
          QUI17,
          QUI18,
          QUI19,
          QUI20,
          QUI21,
          QUI22,
          QUI23,
          QUI24
      FROM
      (
        SELECT     
          NOMEMPPLA.ID_EMPLEADO_FK AS ID_EMPLEADO_FK,
          QUI.NUMERO_QUINCENA AS NUMERO_QUINCENA,
          CONEMPPLA.IMPORTE_CONCEPTO AS IMPORTE_CONCEPTO,
          DATPER.NOMBRE AS NOMBRE,
          DATPER.A_PATERNO AS A_PATERNO,
          DATPER.A_MATERNO AS A_MATERNO,
          GRU.CLAVE_JERARQUICA AS CLAVE_JERARQUICA,
          GRA.GRADO AS GRADO,
          NIV.NIVEL AS NIVEL,
          CONPE.ID_CONF_PERIODOS_SRC AS ID_CONF_PERIODOS_SRC,
          NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC AS DESCRIPCION_NIVEL_POTENCIA_SRC,
          NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC AS PRIMA_ANUAL_NIVEL_POTENCIA_SRC,
          CONPE.PORCENTAJE_DESCUENTO AS PORCENTAJE_DESCUENTO
        FROM
          RH_NOMN_CABECERAS_NOMINAS CAB
          INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
          INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON CAB.ID_NOMINA = NOMEMPPLA.ID_NOMINA_FK
          INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
          INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
          INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
          INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
          INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
          INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
          INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
          INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK    
          INNER JOIN RH_NOMN_SEGUROS SEG ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
          INNER JOIN RH_NOMN_SEG_RESP_CIVI SEGRES ON SEGRES.ID_SEGURO = SEG.ID_SEGURO
          INNER JOIN RH_NOMN_CONF_PERIODOS_SRC CONPE ON CONPE.ID_CONF_PERIODOS_SRC = SEGRES.ID_CONF_PERIODOS_SRC
          INNER JOIN RH_NOMN_CAT_PERIODOS_SRC CATPE ON CATPE.ID_PERIODO_SRC = CONPE.ID_PERIODO_SRC
          INNER JOIN RH_NOMN_NIVELES_POTENCIA_SRC NIVPO ON NIVPO.ID_NIVEL_POTENCIA_SRC = SEGRES.ID_NIVEL_POTENCIA_SRC
        WHERE  
          CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegResCiv        
          AND CAB.ID_EJERCICIO_FK = vIdEjercicioActual
          AND SEGRES.FECHA_INICIO <= (SELECT QUI.FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI WHERE QUI.ID_QUINCENA = vIdQuincenaActual)
          AND (SEGRES.FECHA_FIN IS NULL OR SEGRES.FECHA_FIN >= (SELECT QUI.FECHA_FIN_QUINCENA FROM RH_NOMN_CAT_QUINCENAS QUI WHERE QUI.ID_QUINCENA = vIdQuincenaActual))        
        GROUP BY
          CAB.ID_NOMINA, 
          NOMEMPPLA.ID_EMPLEADO_FK, 
          QUI.NUMERO_QUINCENA, 
          CONEMPPLA.IMPORTE_CONCEPTO,
          DATPER.NOMBRE,
          DATPER.A_PATERNO,
          DATPER.A_MATERNO,
          GRU.CLAVE_JERARQUICA,
          GRA.GRADO,
          GRA.GRADO,
          NIV.NIVEL,
          CONPE.ID_CONF_PERIODOS_SRC,
          NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC,
          NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC,
          CONPE.PORCENTAJE_DESCUENTO
        ORDER BY
          CAB.ID_NOMINA    
      )
      PIVOT
      (
        MAX(IMPORTE_CONCEPTO)
      FOR
        NUMERO_QUINCENA IN (1 QUI1, 2 QUI2, 3 QUI3, 4 QUI4, 5 QUI5, 6 QUI6, 7 QUI7, 8 QUI8,
                            9 QUI9, 10 QUI10, 11 QUI11, 12 QUI12, 13 QUI13, 14 QUI14, 15 QUI15,
                            16 QUI16, 17 QUI17, 18 QUI18, 19 QUI19, 20 QUI20, 21 QUI21, 22 QUI22,
                            23 QUI23, 24 QUI24)
      )
      ;    
      
  ELSE
  
    OPEN DATOS FOR  
      SELECT  
          0 NUMERO_REGISTRO,
          0 ID_EMPLEADO_FK,
          '0' NOMBRE1,
          '0' NIVEL1,
          '0' PERIODO1,
          '0' RIESGO1,
          0 COSTO1,
          0 QUI1,
          0 QUI2,
          0 QUI3,
          0 QUI4,
          0 QUI5,
          0 QUI6,
          0 QUI7,
          0 QUI8,
          0 QUI9,
          0 QUI10,
          0 QUI11,
          0 QUI12,
          0 QUI13,
          0 QUI14,
          0 QUI15,
          0 QUI16,
          0 QUI17,
          0 QUI18,
          0 QUI19,
          0 QUI20,
          0 QUI21,
          0 QUI22,
          0 QUI23,
          0 QUI24
      FROM 
        DUAL;
        
  END IF;       
  
END SP_REPORTE_SRC_EVENT_CORRIDA;

/
--------------------------------------------------------
--  DDL for Procedure SP_REPORTE_SRC_EVENT_PAGOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REPORTE_SRC_EVENT_PAGOS" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) AS 

  vIdConceptoSegResCiv    NUMBER;
  
BEGIN

  -- Obtiene el ID del concepto de seguro de responsabilidad civil eventual.
  SELECT
    ID_CONCEPTO
  INTO
    vIdConceptoSegResCiv
  FROM
    RH_NOMN_CAT_CONCEPTOS
  WHERE
    CLAVE_CONCEPTO = '85E'
  ;
  
  OPEN DATOS FOR
    SELECT
      ROWNUM NUMERO_REGISTRO,
      EMP.ID_EMPLEADO ID_EMPLEADO,
      DATPER.NOMBRE || ' ' || NVL(DATPER.A_PATERNO, ' ') || ' ' || NVL(DATPER.A_MATERNO, ' ') NOMBRE,
      GRU.CLAVE_JERARQUICA || GRA.GRADO || NIV.NIVEL NIVEL,
      SUBSTR(CONPE.ID_CONF_PERIODOS_SRC,2,4) PERIODO,
      NIVPO.DESCRIPCION_NIVEL_POTENCIA_SRC RIESGO,
      ROUND((NIVPO.PRIMA_ANUAL_NIVEL_POTENCIA_SRC * ((100 - CONPE.PORCENTAJE_DESCUENTO)/ 100)), 2) COSTO,
      CONEMPPLA.IMPORTE_CONCEPTO DES_QUIN,
      'Base de datos Qna. ' || QUI.NUMERO_QUINCENA || '/' || EJE.VALOR CABECERO
    FROM
      RH_NOMN_CABECERAS_NOMINAS CAB
      INNER JOIN RH_NOMN_CAT_QUINCENAS QUI ON QUI.ID_QUINCENA = CAB.ID_QUINCENA_FK
      INNER JOIN RH_NOMN_CAT_EJERCICIOS EJE ON EJE.ID_EJERCICIO = QUI.ID_EJERCICIO_FK
      INNER JOIN RH_NOMN_NOMINAS_EMPLE_PLAZA NOMEMPPLA ON CAB.ID_NOMINA = NOMEMPPLA.ID_NOMINA_FK
      INNER JOIN RH_NOMN_CONPTOS_EMPLE_PLAZA CONEMPPLA ON CONEMPPLA.ID_NOMEMPPLA_FK = NOMEMPPLA.ID_NOMEMPPLA
      INNER JOIN RH_NOMN_EMPLEADOS EMP ON EMP.ID_EMPLEADO = NOMEMPPLA.ID_EMPLEADO_FK
      INNER JOIN RH_NOMN_DATOS_PERSONALES DATPER ON DATPER.ID_DATOS_PERSONALES = EMP.ID_DATOS_PERSONALES
      INNER JOIN RH_NOMN_PLAZAS PLA ON PLA.ID_PLAZA = EMP.ID_PLAZA
      INNER JOIN RH_NOMN_TABULADORES TAB ON TAB.ID_TABULADOR = PLA.ID_TABULADOR_FK
      INNER JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS GRU ON GRU.ID_GRUPO_JERARQUICO = TAB.ID_GRUPO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS GRA ON GRA.ID_GRADO_JERARQUICO = TAB.ID_GRADO_JERARQUICO_FK
      INNER JOIN RH_NOMN_CAT_NIVELES_TABULADOR NIV ON NIV.ID_NIVEL_TABULADOR = TAB.ID_NIVEL_TABULADOR_FK    
      INNER JOIN RH_NOMN_SEGUROS SEG ON SEG.ID_EMPLEADO_FK = EMP.ID_EMPLEADO
      INNER JOIN RH_NOMN_SEG_RESP_CIVI SEGRES ON SEGRES.ID_SEGURO = SEG.ID_SEGURO
      INNER JOIN RH_NOMN_CONF_PERIODOS_SRC CONPE ON CONPE.ID_CONF_PERIODOS_SRC = SEGRES.ID_CONF_PERIODOS_SRC
      INNER JOIN RH_NOMN_CAT_PERIODOS_SRC CATPE ON CATPE.ID_PERIODO_SRC = CONPE.ID_PERIODO_SRC
      INNER JOIN RH_NOMN_NIVELES_POTENCIA_SRC NIVPO ON NIVPO.ID_NIVEL_POTENCIA_SRC = SEGRES.ID_NIVEL_POTENCIA_SRC
    WHERE
      CAB.ID_QUINCENA_FK = IDQUINCENA
      AND CAB.ID_TIPO_NOMINA_FK = 2
      AND CONEMPPLA.ID_CONCEPTO_FK = vIdConceptoSegResCiv
      AND SEG.ACTIVO = 'A'
    ; 
    
END SP_REPORTE_SRC_EVENT_PAGOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_REVERTIR_PLAZAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_REVERTIR_PLAZAS" (IDPLAZA IN NUMBER,MESSAGE OUT VARCHAR2 ) IS

ERR_CODE       INTEGER;
ERR_MSG        VARCHAR2(250);
IDMOVIMIENTO NUMBER;
IDMOVIMIENTOS SYS_REFCURSOR;
PLAZAS SYS_REFCURSOR;
PLAZA NUMBER;
NUMPLAZA NUMBER;

BEGIN

--Obtenemos las plazas hijas
OPEN PLAZAS FOR SELECT ID_PLAZA FROM RH_NOMN_PLAZAS WHERE ID_PLAZA_PADRE = IDPLAZA;


--Obtenemos los movimientos de las plazas hijas
    OPEN IDMOVIMIENTOS FOR 
        SELECT DISTINCT MOV.ID_HISTORICO_MOVIMIENTO 
        FROM RH_NOMN_BIT_HISTORICOS_MOVS MOV
        LEFT JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS CAMP ON CAMP.ID_HISTORICO_MOVIMIENTO_FK = MOV.ID_HISTORICO_MOVIMIENTO
        LEFT JOIN RH_NOMN_CAT_ACCIONES ACC ON ACC.ID_ACCION = MOV.ID_ACCION_FK
        LEFT JOIN RH_NOMN_CAT_TIPOS_MOVIMIENTO TIPMOV ON TIPMOV.ID_TIPOS_MOVIMIENTO = ACC.ID_TIPO_MOVIMIENTO
        LEFT JOIN RH_NOMN_CAT_CAMPOS CATCAMP ON CATCAMP.ID_CAMPO = CAMP.ID_CAMPO_AFECTADO_FK
        WHERE TIPMOV.ID_TIPOS_MOVIMIENTO = 1
        AND CAMP.VALOR_BUSQUEDA IN (SELECT TO_CHAR(ID_PLAZA) FROM RH_NOMN_PLAZAS WHERE ID_PLAZA_PADRE = IDPLAZA);


-- Borramos los la bitacora de campos de las plazas hijas
DELETE FROM RH_NOMN_BIT_HISTORICOS_CAMPOS
WHERE ID_HISTORICO_MOVIMIENTO_FK IN (SELECT  MOV.ID_HISTORICO_MOVIMIENTO
                             FROM RH_NOMN_BIT_HISTORICOS_MOVS MOV
                             LEFT JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS CAMP ON CAMP.ID_HISTORICO_MOVIMIENTO_FK = MOV.ID_HISTORICO_MOVIMIENTO
                             LEFT JOIN RH_NOMN_CAT_ACCIONES ACC ON ACC.ID_ACCION = MOV.ID_ACCION_FK
                             LEFT JOIN RH_NOMN_CAT_TIPOS_MOVIMIENTO TIPMOV ON TIPMOV.ID_TIPOS_MOVIMIENTO = ACC.ID_TIPO_MOVIMIENTO
                             LEFT JOIN RH_NOMN_CAT_CAMPOS CATCAMP ON CATCAMP.ID_CAMPO = CAMP.ID_CAMPO_AFECTADO_FK
                             WHERE TIPMOV.ID_TIPOS_MOVIMIENTO = 1
                             AND CAMP.VALOR_BUSQUEDA IN (SELECT TO_CHAR(ID_PLAZA) FROM RH_NOMN_PLAZAS WHERE ID_PLAZA_PADRE = IDPLAZA)

                            )
;

--Borramos los movimientos de las plazas hijas
LOOP
    FETCH IDMOVIMIENTOS INTO IDMOVIMIENTO;
        EXIT WHEN IDMOVIMIENTOS%NOTFOUND;
        DELETE FROM RH_NOMN_BIT_HISTORICOS_MOVS
        WHERE ID_HISTORICO_MOVIMIENTO = IDMOVIMIENTO;
END LOOP; 


--Borramos las plazas
LOOP
    FETCH PLAZAS INTO PLAZA;
        EXIT WHEN PLAZAS%NOTFOUND;
        DELETE FROM RH_NOMN_PLAZAS
        WHERE ID_PLAZA = PLAZA;
END LOOP;

SELECT NUMERO_PLAZA INTO NUMPLAZA 
FROM RH_NOMN_PLAZAS
WHERE ID_PLAZA = IDPLAZA;

--Actualizamos la plaza padre
UPDATE RH_NOMN_PLAZAS SET ID_ESTATUS_PLAZA_FK = 1,
                          VIGENCIA_FINAL = (SELECT  DECODE(VALOR_ACTUAL,'N/A',NULL,TO_DATE(VALOR_ACTUAL,'DD/MM/YY'))
                                            FROM RH_NOMN_BIT_HISTORICOS_CAMPOS
                                            WHERE ID_HISTORICO_CAMPO = (SELECT MAX(CAMP.ID_HISTORICO_CAMPO)
                                                                        FROM RH_NOMN_BIT_HISTORICOS_MOVS MOV
                                                                        LEFT JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS CAMP ON CAMP.ID_HISTORICO_MOVIMIENTO_FK = MOV.ID_HISTORICO_MOVIMIENTO
                                                                        LEFT JOIN RH_NOMN_CAT_ACCIONES ACC ON ACC.ID_ACCION = MOV.ID_ACCION_FK
                                                                        LEFT JOIN RH_NOMN_CAT_TIPOS_MOVIMIENTO TIPMOV ON TIPMOV.ID_TIPOS_MOVIMIENTO = ACC.ID_TIPO_MOVIMIENTO
                                                                        LEFT JOIN RH_NOMN_CAT_CAMPOS CATCAMP ON CATCAMP.ID_CAMPO = CAMP.ID_CAMPO_AFECTADO_FK
                                                                        WHERE TIPMOV.ID_TIPOS_MOVIMIENTO = 1
                                                                        AND CAMP.VALOR_BUSQUEDA = TO_CHAR(IDPLAZA)
                                                                        AND CAMP.ID_CAMPO_AFECTADO_FK = 7
                                                                        )
                                            )
WHERE ID_PLAZA = IDPLAZA;
MESSAGE := 'La reversi�n de la plaza '||IDPLAZA||' termin� exitosamente';

COMMIT;

EXCEPTION 
WHEN OTHERS THEN
 ERR_CODE := SQLCODE;
 ERR_MSG := SUBSTR(SQLERRM, 1, 200);
 MESSAGE := 'La reversi�n de la plaza '||IDPLAZA||' no se pudo realizar';
 SPDERRORES('SP_REVERTIR_PLAZAS',ERR_CODE,ERR_MSG,''); 
ROLLBACK;
END SP_REVERTIR_PLAZAS;

/
--------------------------------------------------------
--  DDL for Procedure SP_RPT_LISTA_CONCEPTOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_RPT_LISTA_CONCEPTOS" (prmGravado VARCHAR2, e_recordser OUT SYS_REFCURSOR) 
AS  
  --vlRetCursor SYS_REFCURSOR;
  vCadenaSQL varchar2(8000);
  VLongitudCadena NUMBER;
  vRealizoValidacion Integer;
  prmIdConcepto NUMBER; 
  --prmGravado VARCHAR2(1);
BEGIN 
  prmIdConcepto:=0; 
  --prmGravado:='G';

  VLongitudCadena:=0;
  VLongitudCadena:=nvl(LENGTH(TRIM(prmGravado)),0);
  vRealizoValidacion:=0;
  vCadenaSQL:='';

  IF prmIdConcepto>0 AND VLongitudCadena=0 AND vRealizoValidacion=0 then
    vCadenaSQL:='Select ID_CONCEPTO,CLAVE_CONCEPTO, ESTATUS, GRAVADO, NOMBRE_CONCEPTO from RH_NOMN_CAT_CONCEPTOS where ID_Concepto= ' || TO_CHAR(prmIdConcepto);
    vRealizoValidacion:=1;
  END IF;
  IF prmIdConcepto>0 AND VLongitudCadena>0 AND vRealizoValidacion=0 then
    vCadenaSQL:='Select ID_CONCEPTO,CLAVE_CONCEPTO, ESTATUS, GRAVADO, NOMBRE_CONCEPTO from RH_NOMN_CAT_CONCEPTOS where GRAVADO= ''' || prmGravado || ''' AND ID_Concepto= ' || TO_CHAR(prmIdConcepto);
    --vCadenaSQL:='Select ID_CONCEPTO,CLAVE_CONCEPTO, ESTATUS, GRAVADO, NOMBRE_CONCEPTO from RH_NOMN_CAT_CONCEPTOS where GRAVADO= ''' || prmGravado || '' ;
    vRealizoValidacion:=2;
  END IF;
  IF prmIdConcepto=0 AND VLongitudCadena>0 AND vRealizoValidacion=0 then
    vCadenaSQL:='Select ID_CONCEPTO,CLAVE_CONCEPTO, ESTATUS, GRAVADO, NOMBRE_CONCEPTO from RH_NOMN_CAT_CONCEPTOS where GRAVADO= ''' || prmGravado || '''';
    vRealizoValidacion:=3;
  END IF;
 
  ---vCadenaSQL:= 'SELECT ''' || vCadenaSQL || ''' AS QRY FROM DUAL';
  --vCadenaSQL:= 'SELECT ''' || TO_CHAR(vRealizoValidacion) || ''' AS VDACION, ''' || vCadenaSQL || ''' AS QRYE FROM DUAL';
  --vCadenaSQL:='SELECT ''' || TO_CHAR(vRealizoValidacion) || ''' AS VDACION, ''' || TO_CHAR(prmIdConcepto) || ''' AS prmIdConcepto, ''' || vCadenaSQL || ''' AS QRYE FROM DUAL';
  --vCadenaSQL:='SELECT ''' || TO_CHAR(vRealizoValidacion) || ''' AS VDACION, ''' || TO_CHAR(prmIdConcepto) || ''' AS prmIdConcepto, ''' || prmGravado || ''' AS prmGravado, ''' || vCadenaSQL || ''' AS QRYE FROM DUAL';
  DBMS_OUTPUT.PUT_LINE(vCadenaSQL);
  OPEN e_recordser FOR vCadenaSQL;

  EXCEPTION  
     WHEN NO_DATA_FOUND THEN  
       DBMS_OUTPUT.PUT_LINE('No hay registros ... ') ;  
     WHEN OTHERS THEN  
       DBMS_OUTPUT.PUT_LINE('No hay registros ... ') ;  
 
END SP_RPT_LISTA_CONCEPTOS;

/
--------------------------------------------------------
--  DDL for Procedure SP_SEGURO_SEP_INDV_HIST2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SEGURO_SEP_INDV_HIST2" 
  ( 
--    EJERCICIOINI  IN  NUMBER,
--    EJERCICIOFIN  IN  NUMBER,
--    QUINCENAINI   IN  NUMBER,
--    QUINCENAFIN   IN  NUMBER,
    REPORTE       OUT SYS_REFCURSOR
   )
IS 

BEGIN


OPEN REPORTE FOR

SELECT 
              ns.ID_CONCEPTO_FK
            , (select a.DESCRIPCION from RH_NOMN_CAT_PUESTOS a where pla.ID_PUESTO_FK =a.ID_PUESTO) puesto 
            , ad.DESCRIPCION ADSCRIPCION 
            , ns.ID_PRESTACION_FK
            , cab_n.ID_EJERCICIO_FK
            , cab_n.ID_QUINCENA_FK
            , dp.curp  curp 
            , dp.rfc  rfc 
            , ne.numero_empleado  num_empleado 
            , dp.a_paterno 	a_paterno 
            , dp.a_materno 	a_materno 
            , dp.nombre  nombre 
      --      , di.calle || ' num. ' || nvl(di.num_exterior,'s/n ') || ' int. ' || nvl(di.num_interior, ' s/i' )  direccion 
     --       , cc.descripcion_colonia  colonia
     --       , cm.descripcion_municipio 	del_municipio
     --       , cp.descripcion_codigo_postal 	cp
     --       , cef.id_entidad_federativa  edo
     --       , nvl(dp.telefono_particular, dp.telefono_movil) tel
            , tab.sueldo_base  sueldo
       --     , tab.compensacion_garantizada  compensacion_g
       --     , '0000000.00' 	compensacion_e
       --     , '0000000.00' 	carrera_mag
        --    , cnt.nivel  n_tab 
        ---    , '1'  zona_eco
        --    , '1'  tipo_reg
        --    , 'm'  tipo_mov
        --    , 'p'  tipo_tr
        --    , substr(cee.descripcios_estatus_empleado, 1,1)  sit_serv
        --    , ne.fecha_de_ingreso  fecha_ingre
       --     , cq.fecha_pago_quincena  fecha_pago
       --     , dp.fecha_nacimiento  fecha_nac 
       --     , '0111' ramo
       --     , '009'  unidad
      --      , conc.clave_concepto codigo
            , con_emp.importe_concepto 	imp_codigo
       --     , gen.descripcion_genero  sexo
       --     , ec.descripcion_estado_civil  edo_civil
       --     , dp.id_entidad_nacimiento  edo_nac
            , ssep.descripcion_seguro_separacion  seg_separa 
      --      , dban.clabe  clabe
            , ctnom.tipo_nomina  tipo_nomina
            , cq.numero_quincena  num_quincena
       FROM   rh_nomn_datos_personales DP            , rh_nomn_empleados NE
            , rh_nomn_direcciones DI                 , rh_nomn_cat_colonias CC 
            , rh_nomn_cat_municipios CM              , rh_nomn_cat_entidad_federativa CEF
            , rh_nomn_cat_codigo_postal cp           , rh_nomn_plazas pla
            , rh_nomn_tabuladores tab                , rh_nomn_cat_niveles_tabulador cnt
            , rh_nomn_cat_estatus_empleado cee       , rh_nomn_nominas_emple_plaza_01 emp_p
            , rh_nomn_conptos_emple_plaza_01 con_emp , rh_nomn_cat_conceptos conc
            , rh_nomn_cat_estados_civil ec           , RH_NOMN_SEGUROS ns
            , rh_nomn_seg_separa_idividual sep_ind   , rh_nomn_cat_seguro_separacion ssep
            , rh_nomn_datos_bancarios DBAN           , rh_nomn_cat_generos GEN
            , rh_nomn_cabeceras_nominas cab_n        , rh_nomn_cat_quincenas cq
            , rh_nomn_cat_tipos_nomina ctnom         , rh_nomn_cat_ejercicios cejer
            , RH_NOMN_ADSCRIPCIONES ad
       WHERE    dp.id_datos_personales          =  ne.id_datos_personales
            AND dp.id_direccion                 =  di.id_direccion
            AND di.id_colonia                   =  cc.id_colonia
            AND cc.id_municipio                 =  cm.id_municipio
            AND cm.id_entidad_federativa        =  cef.id_entidad_federativa
            AND cc.id_codigo_postal             =  cp.id_codigo_postal
            AND ne.id_plaza                     =  pla.id_plaza
            AND pla.id_tabulador_fk             =  tab.id_tabulador
            AND pla.ID_ADSCRIPCION_FK           =  ad.ID_ADSCRIPCION
            AND tab.id_nivel_tabulador_fk       =  cnt.id_nivel_tabulador
            AND ne.id_estatus_empleado          =  cee.id_estatus_empleado
            AND ne.id_empleado                  =  emp_p.id_empleado_fk
            AND pla.id_plaza                    =  emp_p.id_plaza_fk
            AND emp_p.id_nomemppla01            =  con_emp.id_nomemppla01_fk
            AND con_emp.id_concepto01_fk        =  conc.id_concepto
            AND dp.id_estado_civil              =  ec.id_estado_civil
            AND dp.id_genero                    = gen.id_genero
            AND ne.id_empleado                  = ns.id_empleado_fk
            AND ns.id_seguro                    = sep_ind.id_seguro_fk(+)
            AND sep_ind.id_seguro_separacion_fk = ssep.id_seguro_separacion(+)
            AND ne.id_datos_bancarios           = dban.id_datos_bancarios(+)
            AND cab_n.id_nomina                 = emp_p.id_nomina01_fk
            AND cab_n.id_quincena_fk            = cq.id_quincena
            AND cab_n.id_tipo_nomina_fk         = ctnom.id_tipo_nomina
            AND cq.id_ejercicio_fk              = cejer.id_ejercicio
           AND ns.ID_PRESTACION_FK             = 7
           AND ns.ID_CONCEPTO_FK               in (53,62);


END SP_SEGURO_SEP_INDV_HIST2;

/
--------------------------------------------------------
--  DDL for Procedure SP_SEGURO_SEPARACION_INDV_HIST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SEGURO_SEPARACION_INDV_HIST" 
  ( 
    EJERCICIOINI  IN  NUMBER,
    EJERCICIOFIN  IN  NUMBER,
    QUINCENAINI   IN  NUMBER,
    QUINCENAFIN   IN  NUMBER,
    REPORTE       OUT SYS_REFCURSOR
   )
IS 

BEGIN


OPEN REPORTE FOR
SELECT 
EMPLEADO, 
NOMBRE,
APATERNO,
AMATERNO,
RFC                  , 
PUESTO               ,
ADSCRIPCION          ,
SUELDOBASE           ,
IMPORTE              ,
IDEMPLEADO           ,
IDCONCEPTO           ,
IDPRESTACION         ,
ACTIVO               ,
IDEJERCICIO          ,
FECHA_MOVIMIENTO     ,
QUINCENA_INICIO      ,   
QUINCENA_FIN
FROM
(
SELECT e.NUMERO_EMPLEADO    EMPLEADO, 
       d.NOMBRE             NOMBRE,
       d.A_PATERNO          APATERNO,
       d.A_MATERNO          AMATERNO,
       d.RFC                RFC
       ,(select a.DESCRIPCION from RH_NOMN_CAT_PUESTOS a where p.ID_PUESTO_FK =a.ID_PUESTO) PUESTO 
       ,ad.DESCRIPCION      ADSCRIPCION 
       ,T.SUELDO_BASE       SUELDOBASE
       ,R.IMPORTE
       ,R.ID_EMPLEADO_FK    IDEMPLEADO
       ,R.ID_CONCEPTO_FK    IDCONCEPTO
       ,R.ID_PRESTACION_FK  IDPRESTACION
       ,R.ACTIVO            ACTIVO
       ,BHM.ID_EJERCICIO_FK IDEJERCICIO
       ,BHM.FECHA           FECHA_MOVIMIENTO,
        (SELECT NUMERO_QUINCENA
           FROM RH_NOMN_CAT_QUINCENAS A
           WHERE A.FECHA_INICIO_QUINCENA = Q.FECHA_INICIO_QUINCENA)
        AS QUINCENA_INICIO,
        Q.FECHA_INICIO_QUINCENA,
        (SELECT NUMERO_QUINCENA
         FROM   RH_NOMN_CAT_QUINCENAS A
         WHERE  A.FECHA_FIN_QUINCENA = Q.FECHA_FIN_QUINCENA)
        AS QUINCENA_FIN
       FROM RH_NOMN_SEGUROS R,
            RH_NOMN_EMPLEADOS e,
            RH_NOMN_DATOS_PERSONALES d ,
            RH_NOMN_PLAZAS p,
            RH_NOMN_ADSCRIPCIONES ad,
            RH_NOMN_TABULADORES T ,
            RH_NOMN_BIT_HISTORICOS_MOVS BHM,
            RH_NOMN_BIT_HISTORICOS_CAMPOS BHC,
            RH_NOMN_CAT_QUINCENAS Q
 where R.ID_PRESTACION_FK= 7
and R.ID_CONCEPTO_FK in (53,62)
and R.ID_EMPLEADO_FK=e.ID_EMPLEADO
and e.ID_DATOS_PERSONALES = d.ID_DATOS_PERSONALES
and e.ID_PLAZA = p.ID_PLAZA
AND p.ID_ADSCRIPCION_FK = ad.ID_ADSCRIPCION
AND P.ID_TABULADOR_FK = T.ID_TABULADOR
AND BHM.ID_HISTORICO_MOVIMIENTO = BHC.ID_HISTORICO_MOVIMIENTO_FK
AND BHM.ID_EJERCICIO_FK = Q.ID_EJERCICIO_FK
AND BHM.FECHA BETWEEN FECHA_INICIO_QUINCENA AND FECHA_FIN_QUINCENA
)
WHERE (   IDEJERCICIO    >= EJERCICIOINI 
       OR IDEJERCICIO    <= EJERCICIOFIN 
       OR QUINCENA_INICIO >= QUINCENAINI 
       OR QUINCENA_FIN    <= QUINCENAFIN)
ORDER BY NOMBRE DESC;

END SP_SEGURO_SEPARACION_INDV_HIST;

/
--------------------------------------------------------
--  DDL for Procedure SP_SIGEVI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SIGEVI" (P_EJERCICIO    IN INTEGER, P_QUINCENA    IN INTEGER, V_CURSOR OUT SYS_REFCURSOR)
is 

cadenaClob clob;
qryInsDet clob;
cntReg integer;
--toBlob blob;
poscArroba numeric;
ctaDominio varchar2(50);
nameFile varchar2(20);


  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
  

newID number :=0;

fechaIni date;
fechaFin date;
v_fi date;
v_ff date;
contReg NUMERIC;
strQuinAnt varchar2(600);
qry varchar2(4000);
strTbl varchar2(50);
cntX numeric;
cntExiste numeric;

V_ID_EMPLEADO NUMBER;
V_ID_DATOS_PERSONALES NUMBER;
V_ID_DATOS_BANCARIOS NUMBER;
V_ID_PLAZA NUMBER;
V_ID_ESTATUS_EMPLEADO NUMBER;
V_NUMERO_EMPLEADO VARCHAR2(30 BYTE);
V_FECHA_DATO DATE; 
v_RFC VARCHAR2(20 BYTE);
v_NOMBRE VARCHAR2(100 BYTE);
v_A_PATERNO VARCHAR2(80 BYTE);
v_A_MATERNO VARCHAR2(80 BYTE);
v_CORREO_ELECTRONICO VARCHAR2(100 BYTE);
v_DESCRIPCION_BANCO VARCHAR2(100 BYTE);
v_NUMERO_CUENTA VARCHAR2(17 BYTE);
v_CLABE VARCHAR2(20 BYTE);
v_ID_UNIADM number;
v_ID_ADSCRIPCION number;
v_CLAVE_UNIADM NUMBER(3,0);
v_ID_PUESTO_FK number;
v_puesto VARCHAR2(255 BYTE);
V_ID_HIST_MOV number;

C2_ID_EJER NUMBER;
C2_ID_QUIN NUMBER;
C2_FECHA_INI_Q DATE;
C2_FECHA_FIN_Q DATE;

C1 SYS_REFCURSOR;
C2 SYS_REFCURSOR;
Q1 SYS_REFCURSOR;
CB1 SYS_REFCURSOR;


existTable      integer := 0;
BEGIN 

  ---------------------------------------------------------------------  
    -- N U E V O   I D   D E   L A   F I L A 
  ---------------------------------------------------------------------   
    select max(id_sigevi) into newID from rh_nomn_sigevi;
    
      if(newID is null)then
        newID := 1;
      else
        newID := newID + 1;
      end if;
      
  ---------------------------------------------------------------------
    -- N O M B R E   D E L   A R C H I V O
  ---------------------------------------------------------------------    
  nameFile := 'SIGEVI_' || to_char(sysdate, 'yy MM dd') ;  
   
         
      
---------------------------------------------------------------------  
    -- C R E A M O S   U N A   N U E V A   F I L A 
  ---------------------------------------------------------------------   
     
    insert into RH_NOMN_SIGEVI 
    (
      ID_SIGEVI,
      ID_EJERCICIO_FK,
      ID_QUINCENA_FK,
      NOMBRE_ARCHIVO
    )
    values
      (
        newID,
        P_EJERCICIO,
        P_QUINCENA,        
        nameFile        
      );      

strTbl := 'TMP_SIGEVI';

-- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = strTbl;
        --AND OWNER = 'SERP';
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||strTbl;
      
      END IF; 

  -- obtenemos las fechas de la quincena
  select FECHA_INICIO_QUINCENA, FECHA_FIN_QUINCENA 
  into fechaIni, fechaFin
  from rh_nomn_cat_quincenas 
  where  ID_EJERCICIO_FK = P_EJERCICIO and id_quincena = P_QUINCENA;

--DBMS_OUTPUT.PUT_LINE(' existTable->' || fechaIni || '//' || fechaFin);

---------------------------------------
--- O B T E N I E N D O   A L T A S  
---------------------------------------
    
    --DBMS_OUTPUT.PUT_LINE(' --- O B T E N I E N D O   A L T A S  -------'  );
    
    OPEN C1 FOR 
      SELECT p.ID_PUESTO_FK, a.ID_UNIADM_FK, p.ID_ADSCRIPCION_FK, e.ID_EMPLEADO, e.ID_DATOS_PERSONALES, e.ID_DATOS_BANCARIOS, e.ID_PLAZA, e.ID_ESTATUS_EMPLEADO, e.NUMERO_EMPLEADO, e.FECHA_DE_INGRESO 
      FROM RH_NOMN_EMPLEADOS e
      inner join rh_nomn_plazas p on e.id_plaza = p.id_plaza
      inner join rh_nomn_adscripciones a on a.ID_ADSCRIPCION = p.ID_ADSCRIPCION_fk
      WHERE e.FECHA_DE_INGRESO BETWEEN fechaIni AND fechaFin
      ORDER BY e.FECHA_DE_INGRESO ASC;

      LOOP  
        FETCH C1 INTO 
          v_ID_PUESTO_FK,
          v_ID_UNIADM,
          v_ID_ADSCRIPCION,
          V_ID_EMPLEADO,
          V_ID_DATOS_PERSONALES, 
          V_ID_DATOS_BANCARIOS, 
          V_ID_PLAZA, 
          V_ID_ESTATUS_EMPLEADO, 
          V_NUMERO_EMPLEADO, 
          V_FECHA_DATO ;
        EXIT WHEN C1%NOTFOUND;    
        --DBMS_OUTPUT.PUT_LINE('-----------1---------' );
          --DBMS_OUTPUT.PUT_LINE('    V_ID_EMPLEADO->' || V_ID_EMPLEADO || '--' || V_FECHA_DATO );
          
          select RFC, NOMBRE, A_PATERNO, A_MATERNO , CORREO_ELECTRONICO
          into v_RFC, v_NOMBRE, v_A_PATERNO, v_A_MATERNO, v_CORREO_ELECTRONICO
          from rh_nomn_datos_personales 
          where ID_DATOS_PERSONALES = V_ID_DATOS_PERSONALES;
          
          poscArroba := 0;
          ctaDominio := '';
          
          IF(v_CORREO_ELECTRONICO IS NOT NULL)THEN
             select instr(v_CORREO_ELECTRONICO,'@') INTO poscArroba from dual  ;
             
             if(poscArroba > 0)then
                select substr(v_CORREO_ELECTRONICO,1,(poscArroba - 1)) INTO ctaDominio from dual;          
             end if;
              
           END IF;                    
          
          IF(V_ID_DATOS_BANCARIOS is not null and V_ID_DATOS_BANCARIOS > 0 )THEN
            select cb.DESCRIPCION_BANCO, db.NUMERO_CUENTA, db.CLABE
            into   v_DESCRIPCION_BANCO, v_NUMERO_CUENTA, v_CLABE
            from rh_nomn_datos_bancarios db
            inner join rh_nomn_cat_bancos cb on db.ID_BANCO = cb.ID_BANCO
            where ID_DATOS_BANCARIOS = V_ID_DATOS_BANCARIOS;
          ELSE
            v_DESCRIPCION_BANCO := ' '; v_NUMERO_CUENTA := ' '; v_CLABE := ' ';
          END IF;
          
          
          select CLAVE_UNIADM
          into v_CLAVE_UNIADM
          from rh_nomn_cat_unidades_admins
          where ID_UNIADM = v_ID_UNIADM;
                   
          
          
          SELECT 
                DECODE(NVL(CLAVE_PUESTO, 'noInfo'), '', 'noInfo') 
          INTO  v_puesto FROM RH_NOMN_CAT_PUESTOS 
          WHERE ID_PUESTO  = v_ID_PUESTO_FK;
          
          
          cadenaClob := cadenaClob || V_NUMERO_EMPLEADO || ';' || v_RFC || ';' || v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE
                            || ';' || v_CLAVE_UNIADM || ';' || v_puesto || ';' || v_NUMERO_CUENTA || ';' ||  v_CLABE    || ';' || v_DESCRIPCION_BANCO  
                            || ';' || ctaDominio     || ';' || v_CORREO_ELECTRONICO || ';' || 'ALTA' || ';' || '' || ';' || '' || ';' || '' || CHR(13)  ;      
          
         qryInsDet := 'insert into RH_NOMN_SIGEVI_DATOS 
                                      (
                                        ID_SIGEVI_DATO,
                                        ID_SIGEVI_FK,
                                        NUMERO_EMPLEADO,  
                                        RFC,
                                        NOMBRE,
                                        CLAVE_UNIADM,
                                        CLAVE_PUESTO,
                                        BANCO,
                                        NUM_CUENTA,
                                        CLABE,
                                        CORREO,
                                        OPERACION,
                                        DOMINIO
                                      ) values
                                      (
                                        (select (nvl(max(ID_SIGEVI_DATO), 0))+1 from rh_nomn_sigevi_datos ) ,
                                        ' || newID || ',' || 
                                        V_NUMERO_EMPLEADO || ',''' ||   
                                        V_RFC || ''',''' || 
                                        v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE || ''', ' || 
                                        V_CLAVE_UNIADM || ',''' || 
                                        v_puesto || ''',''' || 
                                        v_DESCRIPCION_BANCO || ''',''' || 
                                        v_NUMERO_CUENTA || ''',''' || 
                                        V_CLABE || ''',''' || 
                                        v_CORREO_ELECTRONICO || ''',' || 
                                        '''ALTA''' || ', ''' || 
                                        ctaDominio || '''
                                      ) 
                                      ';
                         --DBMS_OUTPUT.PUT_LINE('    qryInsDet1->' || qryInsDet );                
                      execute immediate qryInsDet;             
          
        END LOOP;
        close c1;
        
---------------------------------------
--- O B T E N I E N D O   B A J A S 
---------------------------------------    

--DBMS_OUTPUT.PUT_LINE(' --------------------------------------------'  );
--DBMS_OUTPUT.PUT_LINE(' --- O B T E N I E N D O   B A J A S -------'  );

    OPEN C1 FOR 
      SELECT p.ID_PUESTO_FK, a.ID_UNIADM_FK, p.ID_ADSCRIPCION_FK, e.ID_EMPLEADO, e.ID_DATOS_PERSONALES, e.ID_DATOS_BANCARIOS, e.ID_PLAZA, e.ID_ESTATUS_EMPLEADO, e.NUMERO_EMPLEADO, e.FECHA_DE_INGRESO 
      FROM RH_NOMN_EMPLEADOS e
      inner join rh_nomn_plazas p on e.id_plaza = p.id_plaza
      inner join rh_nomn_adscripciones a on a.ID_ADSCRIPCION = p.ID_ADSCRIPCION_fk
      WHERE e.FECHA_BAJA IS NOT NULL AND e.FECHA_BAJA BETWEEN fechaIni AND fechaFin
      ORDER BY e.FECHA_BAJA ASC;

      LOOP  
        FETCH C1 INTO 
          v_ID_PUESTO_FK,
          v_ID_UNIADM,
          v_ID_ADSCRIPCION,
          V_ID_EMPLEADO,
          V_ID_DATOS_PERSONALES, 
          V_ID_DATOS_BANCARIOS, 
          V_ID_PLAZA, 
          V_ID_ESTATUS_EMPLEADO, 
          V_NUMERO_EMPLEADO, 
          V_FECHA_DATO ;
        EXIT WHEN C1%NOTFOUND;    
        --DBMS_OUTPUT.PUT_LINE('-----------2---------' );
          --DBMS_OUTPUT.PUT_LINE(' V_ID_EMPLEADO->' || V_ID_EMPLEADO || '--' || V_FECHA_DATO );
          
          select RFC, NOMBRE, A_PATERNO, A_MATERNO , CORREO_ELECTRONICO
          into v_RFC, v_NOMBRE, v_A_PATERNO, v_A_MATERNO, v_CORREO_ELECTRONICO
          from rh_nomn_datos_personales 
          where ID_DATOS_PERSONALES = V_ID_DATOS_PERSONALES;
          
          poscArroba := 0;
          ctaDominio := '';
          
          IF(v_CORREO_ELECTRONICO IS NOT NULL)THEN
             select instr(v_CORREO_ELECTRONICO,'@') INTO poscArroba from dual  ;
             
             if(poscArroba > 0)then
                select substr(v_CORREO_ELECTRONICO,1,(poscArroba - 1)) INTO ctaDominio from dual;          
             end if;
              
           END IF;                    
          
          IF(V_ID_DATOS_BANCARIOS is not null )THEN
            select cb.DESCRIPCION_BANCO, db.NUMERO_CUENTA, db.CLABE
            into   v_DESCRIPCION_BANCO, v_NUMERO_CUENTA, v_CLABE
            from rh_nomn_datos_bancarios db
            inner join rh_nomn_cat_bancos cb on db.ID_BANCO = cb.ID_BANCO
            where ID_DATOS_BANCARIOS = V_ID_DATOS_BANCARIOS;
          ELSE
            v_DESCRIPCION_BANCO := ''; v_NUMERO_CUENTA := ''; v_CLABE := '';
          END IF;
          
          
          select CLAVE_UNIADM
          into v_CLAVE_UNIADM
          from rh_nomn_cat_unidades_admins
          where ID_UNIADM = v_ID_UNIADM;
                   
          
          
          SELECT 
                DECODE(NVL(CLAVE_PUESTO, 'noInfo'), '', 'noInfo') 
          INTO  v_puesto FROM RH_NOMN_CAT_PUESTOS 
          WHERE ID_PUESTO  = v_ID_PUESTO_FK;
          
          
          cadenaClob := cadenaClob || V_NUMERO_EMPLEADO || ';' || v_RFC || ';' || v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE
                            || ';' || v_CLAVE_UNIADM || ';' || v_puesto || ';' || v_NUMERO_CUENTA || ';' ||  v_CLABE    || ';' || v_DESCRIPCION_BANCO  
                            || ';' || ctaDominio     || ';' || v_CORREO_ELECTRONICO || ';' || 'BAJA' || ';' || '' || ';' || '' || ';' || '' || CHR(13)  ;      
          
          qryInsDet := 'insert into RH_NOMN_SIGEVI_DATOS 
                                      (
                                        ID_SIGEVI_DATO,
                                        ID_SIGEVI_FK,
                                        NUMERO_EMPLEADO,  
                                        RFC,
                                        NOMBRE,
                                        CLAVE_UNIADM,
                                        CLAVE_PUESTO,
                                        BANCO,
                                        NUM_CUENTA,
                                        CLABE,
                                        CORREO,
                                        OPERACION,
                                        DOMINIO
                                      ) values
                                      (
                                        (select (nvl(max(ID_SIGEVI_DATO), 0))+1 from rh_nomn_sigevi_datos ) ,
                                        ' || newID || ',' || 
                                        V_NUMERO_EMPLEADO || ',''' ||   
                                        V_RFC || ''',''' || 
                                        v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE || ''', ' || 
                                        V_CLAVE_UNIADM || ',''' || 
                                        v_puesto || ''',''' || 
                                        v_DESCRIPCION_BANCO || ''',''' || 
                                        v_NUMERO_CUENTA || ''',''' || 
                                        V_CLABE || ''',''' || 
                                        v_CORREO_ELECTRONICO || ''',' || 
                                        '''BAJA''' || ', ''' || 
                                        ctaDominio || '''
                                      ) 
                                      ';
                          --DBMS_OUTPUT.PUT_LINE('    qryInsDet2->' || qryInsDet );                
                      execute immediate qryInsDet;  
          
          
        
        END LOOP;
        close c1;
  
---------------------------------------------------------------------
--- O B T E N I E N D O   C A M B I O   D E   P U E S T O S
---------------------------------------------------------------------
  
--DBMS_OUTPUT.PUT_LINE(' --------------------------------------------'  );
DBMS_OUTPUT.PUT_LINE(' --- O B T E N I E N D O   P U E S T O S -------' || fechaIni || ' ' || fechaFin );    


     OPEN C1 FOR 
      SELECT 
        distinct (HM.ID_HISTORICO_MOVIMIENTO),
        p.ID_PUESTO_FK, a.ID_UNIADM_FK, p.ID_ADSCRIPCION_FK, e.ID_EMPLEADO, e.ID_DATOS_PERSONALES, e.ID_DATOS_BANCARIOS, e.ID_PLAZA, 
        e.ID_ESTATUS_EMPLEADO, e.NUMERO_EMPLEADO, HM.FECHA 
      FROM 
        RH_NOMN_BIT_HISTORICOS_MOVS HM
      
      INNER JOIN 
        RH_NOMN_BIT_HISTORICOS_CAMPOS HC ON HM.ID_HISTORICO_MOVIMIENTO = HC.ID_HISTORICO_MOVIMIENTO_FK 
      
      INNER JOIN 
        RH_NOMN_EMPLEADOS e ON E.NUMERO_EMPLEADO = HC.VALOR_BUSQUEDA AND  E.ID_PLAZA =  cast(HC.VALOR_ACTUAL as NUMBER(8,0))
      
      INNER JOIN 
        rh_nomn_plazas p on e.id_plaza = p.id_plaza
      
      INNER JOIN 
        rh_nomn_adscripciones a on a.ID_ADSCRIPCION = p.ID_ADSCRIPCION_fk
        
      INNER JOIN 
        RH_NOMN_CAT_ACCIONES ACC ON HM.id_accion_fk = acc.id_accion
      
      WHERE HM.ID_ACCION_FK = 10 AND to_char(HM.FECHA, 'dd/MM/YY') BETWEEN  to_char(fechaIni, 'dd/MM/YY') AND to_char(fechaFin , 'dd/MM/YY');
    LOOP  
        FETCH C1 INTO 
          V_ID_HIST_MOV,
          v_ID_PUESTO_FK,
          v_ID_UNIADM,
          v_ID_ADSCRIPCION,
          V_ID_EMPLEADO,
          V_ID_DATOS_PERSONALES, 
          V_ID_DATOS_BANCARIOS, 
          V_ID_PLAZA, 
          V_ID_ESTATUS_EMPLEADO, 
          V_NUMERO_EMPLEADO, 
          V_FECHA_DATO ;
        EXIT WHEN C1%NOTFOUND;    
        DBMS_OUTPUT.PUT_LINE('-----------3-----------' );
          --DBMS_OUTPUT.PUT_LINE(' V_ID_EMPLEADO->' || V_ID_EMPLEADO || '--' || V_FECHA_DATO );
          
          select RFC, NOMBRE, A_PATERNO, A_MATERNO , CORREO_ELECTRONICO
          into v_RFC, v_NOMBRE, v_A_PATERNO, v_A_MATERNO, v_CORREO_ELECTRONICO
          from rh_nomn_datos_personales 
          where ID_DATOS_PERSONALES = V_ID_DATOS_PERSONALES;
          
          poscArroba := 0;
          ctaDominio := '';
          
          IF(v_CORREO_ELECTRONICO IS NOT NULL)THEN
             select instr(v_CORREO_ELECTRONICO,'@') INTO poscArroba from dual  ;
             
             if(poscArroba > 0)then
                select substr(v_CORREO_ELECTRONICO,1,(poscArroba - 1)) INTO ctaDominio from dual;          
             end if;
              
           END IF;                    
          
          IF(V_ID_DATOS_BANCARIOS is not null )THEN
            select cb.DESCRIPCION_BANCO, db.NUMERO_CUENTA, db.CLABE
            into   v_DESCRIPCION_BANCO, v_NUMERO_CUENTA, v_CLABE
            from rh_nomn_datos_bancarios db
            inner join rh_nomn_cat_bancos cb on db.ID_BANCO = cb.ID_BANCO
            where ID_DATOS_BANCARIOS = V_ID_DATOS_BANCARIOS;
          ELSE
            v_DESCRIPCION_BANCO := ''; v_NUMERO_CUENTA := ''; v_CLABE := '';
          END IF;
          
          
          select CLAVE_UNIADM
          into v_CLAVE_UNIADM
          from rh_nomn_cat_unidades_admins
          where ID_UNIADM = v_ID_UNIADM;
                   
          
          
          SELECT 
                DECODE(NVL(CLAVE_PUESTO, 'noInfo'), '', 'noInfo') 
          INTO  v_puesto FROM RH_NOMN_CAT_PUESTOS 
          WHERE ID_PUESTO  = v_ID_PUESTO_FK;
          
          
          cadenaClob := cadenaClob || V_NUMERO_EMPLEADO || ';' || v_RFC || ';' || v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE
                            || ';' || v_CLAVE_UNIADM || ';' || v_puesto || ';' || v_NUMERO_CUENTA || ';' ||  v_CLABE    || ';' || v_DESCRIPCION_BANCO  
                            || ';' || ctaDominio     || ';' || v_CORREO_ELECTRONICO || ';' || 'ACTUALIZA' || ';' || '' || ';' || '' || ';' || '' || CHR(13)  ;      
          
         qryInsDet := 'insert into RH_NOMN_SIGEVI_DATOS 
                                      (
                                        ID_SIGEVI_DATO,
                                        ID_SIGEVI_FK,
                                        NUMERO_EMPLEADO,  
                                        RFC,
                                        NOMBRE,
                                        CLAVE_UNIADM,
                                        CLAVE_PUESTO,
                                        BANCO,
                                        NUM_CUENTA,
                                        CLABE,
                                        CORREO,
                                        OPERACION,
                                        DOMINIO
                                      ) values
                                      (
                                        (select (nvl(max(ID_SIGEVI_DATO), 0))+1 from rh_nomn_sigevi_datos ) ,
                                        ' || newID || ',' || 
                                        V_NUMERO_EMPLEADO || ',''' ||   
                                        V_RFC || ''',''' || 
                                        v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE || ''', ' || 
                                        V_CLAVE_UNIADM || ',''' || 
                                        v_puesto || ''',''' || 
                                        v_DESCRIPCION_BANCO || ''',''' || 
                                        v_NUMERO_CUENTA || ''',''' || 
                                        V_CLABE || ''',''' || 
                                        v_CORREO_ELECTRONICO || ''',' || 
                                        '''ACTUALIZA''' || ', ''' || 
                                        ctaDominio || '''
                                      ) 
                                      ';
                          --DBMS_OUTPUT.PUT_LINE('    qryInsDet3->' || qryInsDet );                
                      execute immediate qryInsDet;  
          
        
        END LOOP;
        close c1;
        
        
---------------------------------------------------------------------
--- O B T E N I E N D O   A L T A   R E T R O A C T I V O
---------------------------------------------------------------------
  
--DBMS_OUTPUT.PUT_LINE(' --------------------------------------------'  );
--DBMS_OUTPUT.PUT_LINE(' --- A L T A   R E T R O A C T I V O -------'  );    
    


    EXECUTE IMMEDIATE 'CREATE TABLE ' || strTbl || '
       (  
          ID_REGISTRO NUMBER,
          ID_EJERCICIO NUMBER, 
          ID_QUINCENA NUMBER,
          FECHA_INICIO DATE, 
          FECHA_FIN DATE          
       )';   
       
       contReg := 1;
       
    ------------------------------------------------------
    -- OBTENEMOS 3 QUINCENAS ANTERIORES Y LAS GUARDAMOS
    ------------------------------------------------------
    cntX := 1;

     OPEN C2 FOR      
      SELECT ID_EJERCICIO_FK AS ID_EJER, ID_QUINCENA AS ID_QUIN, FECHA_INICIO_QUINCENA AS FECHA_INI, FECHA_FIN_QUINCENA AS FECHA_F
      FROM
      (
         SELECT 
            Q.ID_EJERCICIO_FK, Q.ID_QUINCENA, Q.FECHA_INICIO_QUINCENA, Q.FECHA_FIN_QUINCENA
          FROM
            RH_NOMN_CAT_QUINCENAS Q                            
          WHERE 
             Q.ID_QUINCENA < P_QUINCENA AND Q.ACTIVO = 1  AND  ROWNUM <= 3                            
         ORDER BY Q.ID_QUINCENA  DESC
      )
      ORDER BY ID_QUINCENA ASC  
       ;
    LOOP  
        FETCH C2 INTO 
          C2_ID_EJER, 
          C2_ID_QUIN, 
          C2_FECHA_INI_Q,
          C2_FECHA_FIN_Q
          ;
          
        EXIT WHEN C2%NOTFOUND;    
        --DBMS_OUTPUT.PUT_LINE('-----------4-------');
        --DBMS_OUTPUT.PUT_LINE('-------QUINCENA-------' || C2_ID_EJER || '~' || C2_ID_QUIN ||  ' // ' || C2_FECHA_INI_Q ||  ' - ' || C2_FECHA_FIN_Q  );
        
        EXECUTE IMMEDIATE 'INSERT INTO TMP_SIGEVI VALUES (' || contReg || ','|| C2_ID_EJER || ',' || C2_ID_QUIN || ',''' || C2_FECHA_INI_Q || ''',''' || C2_FECHA_FIN_Q || ''' )';         
        
        if (cntX = 1 ) then
          strQuinAnt := strQuinAnt || ' ( ID_EJERCICIO_FK = '|| C2_ID_EJER || ' AND ID_QUINCENA_FK = ' || C2_ID_QUIN || ')';
        else
          strQuinAnt := strQuinAnt || ' OR ( ID_EJERCICIO_FK = '|| C2_ID_EJER || ' AND ID_QUINCENA_FK = ' || C2_ID_QUIN || ')';
        end if;
        
        contReg := contReg + 1;
        cntX := cntX + 1;
        
        END LOOP;
        close C2;  
        
        
        
        --DBMS_OUTPUT.PUT_LINE('  xxxxxxxxxxxx strQuinAnt xxxxxxxxxxxx ' || strQuinAnt);

      ------------------------------------------------------------
      -- CON LOS DATOS DE LAS QUINCENAS OBTENEMOS LAS FECHAS INI
      -- Y FIN PARA BUSCAR ALTAS DE EMPLEADOS EN QUINC ANTERIORES
      ------------------------------------------------------------ 
      
  
      
      EXECUTE IMMEDIATE 'select FECHA_INICIO  from TMP_SIGEVI WHERE ID_REGISTRO = 1' INTO v_fi;          --select FECHA_INICIO INTO v_fi from TMP_SIGEVI WHERE ID_REGISTRO = 1;
      EXECUTE IMMEDIATE 'select FECHA_FIN     from TMP_SIGEVI WHERE ID_REGISTRO = (SELECT MAX(ID_REGISTRO) FROM TMP_SIGEVI)' INTO  v_ff;         --select FECHA_FIN    INTO v_ff from TMP_SIGEVI WHERE ID_REGISTRO = (SELECT MAX(ID_REGISTRO) FROM TMP_SIGEVI);
      
      
      --DBMS_OUTPUT.PUT_LINE('      -->' || v_fi || '~' || v_ff);
      
      
      OPEN C1 FOR 
        SELECT p.ID_PUESTO_FK, a.ID_UNIADM_FK, p.ID_ADSCRIPCION_FK, e.ID_EMPLEADO, e.ID_DATOS_PERSONALES, e.ID_DATOS_BANCARIOS, e.ID_PLAZA, e.ID_ESTATUS_EMPLEADO, e.NUMERO_EMPLEADO, e.FECHA_DE_INGRESO 
        FROM RH_NOMN_EMPLEADOS e
        inner join rh_nomn_plazas p on e.id_plaza = p.id_plaza
        inner join rh_nomn_adscripciones a on a.ID_ADSCRIPCION = p.ID_ADSCRIPCION_fk
        WHERE e.FECHA_DE_INGRESO BETWEEN V_FI AND V_FF
        ORDER BY e.FECHA_DE_INGRESO ASC;

      LOOP  
        FETCH C1 INTO 
          v_ID_PUESTO_FK,
          v_ID_UNIADM,
          v_ID_ADSCRIPCION,
          V_ID_EMPLEADO,
          V_ID_DATOS_PERSONALES, 
          V_ID_DATOS_BANCARIOS, 
          V_ID_PLAZA, 
          V_ID_ESTATUS_EMPLEADO, 
          V_NUMERO_EMPLEADO, 
          V_FECHA_DATO ;
        EXIT WHEN C1%NOTFOUND;    
        --DBMS_OUTPUT.PUT_LINE('--------5------------' );
          --DBMS_OUTPUT.PUT_LINE('    V_ID_EMPLEADO->' || V_ID_EMPLEADO || '--' || V_FECHA_DATO );
          
          -- POR CADA EMPLEADO ENCONTRADO VALIDAMOS SI EXISTE EN CABECERAS DE QUINCENAS ANTERIORES
          
          qry := 'select count(ID_NOMEMPPLA) 
                  from rh_nomn_nominas_emple_plaza 
                  where  id_nomina_fk in ( select id_nomina from rh_nomn_cabeceras_nominas where ' || strQuinAnt || ' )  AND ID_EMPLEADO_FK = ' || V_ID_EMPLEADO;
          
          
            --DBMS_OUTPUT.PUT_LINE('   --qry 1---> ' || qry);
          
         
          
           EXECUTE IMMEDIATE qry into cntExiste;
           
         -- DBMS_OUTPUT.PUT_LINE('   --cntExiste 1---> ' || cntExiste);
           
           if(cntExiste = 0)then
              -- buscamos si en la quincena actual aparece el empleado
                  qry := 'select count(ID_NOMEMPPLA) 
                          from rh_nomn_nominas_emple_plaza 
                          where  id_nomina_fk in ( select id_nomina from rh_nomn_cabeceras_nominas where id_ejercicio_fk = ' || P_EJERCICIO || ' and id_quincena_fk = ' || P_QUINCENA || ' )  AND ID_EMPLEADO_FK = ' || V_ID_EMPLEADO;
           
                  EXECUTE IMMEDIATE qry into cntExiste;
                  
                  --DBMS_OUTPUT.PUT_LINE('   --qry 1---> ' || qry);
           
           --       DBMS_OUTPUT.PUT_LINE('   --cntExiste2---> ' || cntExiste);
            
              if(cntExiste > 0) then
                    --DBMS_OUTPUT.PUT_LINE('--------------------' );
                    --DBMS_OUTPUT.PUT_LINE('    V_ID_EMPLEADO->' || V_ID_EMPLEADO || '--' || V_FECHA_DATO );
                    
                    select RFC, NOMBRE, A_PATERNO, A_MATERNO , CORREO_ELECTRONICO
                    into v_RFC, v_NOMBRE, v_A_PATERNO, v_A_MATERNO, v_CORREO_ELECTRONICO
                    from rh_nomn_datos_personales 
                    where ID_DATOS_PERSONALES = V_ID_DATOS_PERSONALES;
                    
                    poscArroba := 0;
                    ctaDominio := '';
                    
                    IF(v_CORREO_ELECTRONICO IS NOT NULL)THEN
                       select instr(v_CORREO_ELECTRONICO,'@') INTO poscArroba from dual  ;
                       
                       if(poscArroba > 0)then
                          select substr(v_CORREO_ELECTRONICO,1,(poscArroba - 1)) INTO ctaDominio from dual;          
                       end if;
                        
                     END IF;                    
                    
                    IF(V_ID_DATOS_BANCARIOS is not null )THEN
                      select cb.DESCRIPCION_BANCO, db.NUMERO_CUENTA, db.CLABE
                      into   v_DESCRIPCION_BANCO, v_NUMERO_CUENTA, v_CLABE
                      from rh_nomn_datos_bancarios db
                      inner join rh_nomn_cat_bancos cb on db.ID_BANCO = cb.ID_BANCO
                      where ID_DATOS_BANCARIOS = V_ID_DATOS_BANCARIOS;
                    ELSE
                      v_DESCRIPCION_BANCO := ''; v_NUMERO_CUENTA := ''; v_CLABE := '';
                    END IF;
                    
                    
                    select CLAVE_UNIADM
                    into v_CLAVE_UNIADM
                    from rh_nomn_cat_unidades_admins
                    where ID_UNIADM = v_ID_UNIADM;
                             
                    
                    
                    SELECT 
                          DECODE(NVL(CLAVE_PUESTO, 'noInfo'), '', 'noInfo') 
                    INTO  v_puesto FROM RH_NOMN_CAT_PUESTOS 
                    WHERE ID_PUESTO  = v_ID_PUESTO_FK;
                    
                    
                    cadenaClob := cadenaClob || V_NUMERO_EMPLEADO || ';' || v_RFC || ';' || v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE
                                      || ';' || v_CLAVE_UNIADM || ';' || v_puesto || ';' || v_NUMERO_CUENTA || ';' ||  v_CLABE    || ';' || v_DESCRIPCION_BANCO  
                                      || ';' || ctaDominio     || ';' || v_CORREO_ELECTRONICO || ';' || 'ALTA' || ';' || '' || ';' || '' || ';' || '' || CHR(13)  ;      
                    
                    qryInsDet := 'insert into RH_NOMN_SIGEVI_DATOS 
                                      (
                                        ID_SIGEVI_DATO,
                                        ID_SIGEVI_FK,
                                        NUMERO_EMPLEADO,  
                                        RFC,
                                        NOMBRE,
                                        CLAVE_UNIADM,
                                        CLAVE_PUESTO,
                                        BANCO,
                                        NUM_CUENTA,
                                        CLABE,
                                        CORREO,
                                        OPERACION,
                                        DOMINIO
                                      ) values
                                      (
                                        (select (nvl(max(ID_SIGEVI_DATO), 0))+1 from rh_nomn_sigevi_datos ) ,
                                        ' || newID || ',' || 
                                        V_NUMERO_EMPLEADO || ',''' ||   
                                        V_RFC || ''',''' || 
                                        v_A_PATERNO     || ' ' || v_A_MATERNO || ' ' || v_NOMBRE || ''', ' || 
                                        V_CLAVE_UNIADM || ',''' || 
                                        v_puesto || ''',''' || 
                                        v_DESCRIPCION_BANCO || ''',''' || 
                                        v_NUMERO_CUENTA || ''',''' || 
                                        V_CLABE || ''',''' || 
                                        v_CORREO_ELECTRONICO || ''',' || 
                                        '''ALTA''' || ', ''' || 
                                        ctaDominio || '''
                                      ) 
                                      ';
                        --  DBMS_OUTPUT.PUT_LINE('    INSERT_5->' || qryInsDet );                
                      execute immediate qryInsDet;   
                                     
                    
              end if;
           
           end if;
          
        END LOOP;
        close c1;

  ---------------------------------------------------------------------
    -- C O N V E R T I M O S   E L   C L O B   A   U N   B L O B
  ---------------------------------------------------------------------
    --toBlob := FN_UTILS_CLOB_TO_BLOB(cadenaClob);
 
   ---------------------------------------------------------------------
    -- A C T U A L I Z A M O S   E L   B L O B 
  ---------------------------------------------------------------------
    --update rh_nomn_sigevi set excel1 = toBlob where id_sigevi = newID;

      
      EXECUTE IMMEDIATE 'DROP TABLE TMP_SIGEVI';

  ---------------------------------------------------------------------
    -- r e g r e s a m o s  l o s   v a l o r e s  
  ---------------------------------------------------------------------
  
  select count(ID_SIGEVI_FK) into cntReg from rh_nomn_sigevi_datos where ID_SIGEVI_FK = newID; 
  
  DBMS_OUTPUT.PUT_LINE('    cntReg->' || cntReg );                
  
  if(cntReg = 0)then
    delete  from rh_nomn_sigevi where id_sigevi = newID;   
  end if;
  
   qry := 'SELECT 
              sg.ID_SIGEVI_DATO,
              sg.ID_SIGEVI_FK,
              sg.NUMERO_EMPLEADO,
              sg.RFC,
              sg.NOMBRE,
              sg.CLAVE_UNIADM,
              sg.CLAVE_PUESTO,
              sg.BANCO,
              sg.NUM_CUENTA,
              sg.CLABE,
              sg.CORREO,
              sg.OPERACION,
              sg.AUTORIZA_NACIONAL,
              sg.AUTORIZA_GENERAL,
              sg.CARGO, 
              s.NOMBRE_ARCHIVO,
              sg.DOMINIO
              
       FROM    
          rh_nomn_sigevi_datos sg 
      inner join rh_nomn_sigevi s on s.id_sigevi = sg.ID_SIGEVI_FK 
      
       where sg.ID_SIGEVI_FK = ' ||  newID;
        
        OPEN V_CURSOR FOR qry;
      
      
 
  EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = strTbl ;
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||strTbl;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
  --V_CURSOR := null;  
        
end SP_SIGEVI;

/
--------------------------------------------------------
--  DDL for Procedure SP_SIGEVI_ACTUALIZA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SIGEVI_ACTUALIZA" 
    (
     datos       IN STRING_ARRAY,
     V_CURSOR OUT SYS_REFCURSOR) 
IS
   C1 SYS_REFCURSOR;
   
   ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
   
   tablatmp        VARCHAR2(300) ; --Para generar el nombre de la tabla temporar para crear
   existTable      integer := 0;
   tmpRegistro     VARCHAR2(3000):= ''; --Para generar el registro a insertar
   tmpVal          TYPE_STRING; --Para leer la variable TYPE_STRING
   
   
   qry1 varchar(3000);
   
   c1_idSigevi numeric;
   c1_cmp0  numeric;
   c1_cmp1  numeric;
   c1_cmp2  numeric;
   c1_cmp3  VARCHAR2(500);
   
  
BEGIN 
  
  tablatmp  := 'TMP_SIGEVI_ACT';

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp
        AND OWNER = 'SERP';
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
      
    ----------------------------------------------------------------------
    --  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
    ----------------------------------------------------------------------
     
       EXECUTE IMMEDIATE 'CREATE TABLE '||tablatmp||' 
       (  
          ID_SIGEVI NUMBER,
          ID_EMPLEADO NUMBER, 
          CMP1 NUMBER,
          CMP2 NUMBER,
          CMP3 VARCHAR2(800)
       )';        
       
    ---------------------------------------------------------------------------                                                          
    -- R E C O R R E M O S   E L   A R R E G L O   P A R A   I N S E R T A R  
    -- L O S   V A L O R E S   E N   L A   T A B L A                                                         
    --------------------------------------------------------------------------- 
      
      FOR idxj IN datos.first()..datos.last() 
        LOOP
          tmpVal  := datos(idxj);        
          SELECT TRANSLATE(tmpVal.REGISTRO,'����������', 'aeiouAEIOU') INTO tmpRegistro FROM DUAL;
          EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' || tmpRegistro || ')';         
          
        END LOOP;
        
    ---------------------------------------------------------------------------                                                          
    -- r e c o r r e mo s   l o s   d a t o s   c a r g a d o s                                                     
    --------------------------------------------------------------------------- 
        
        qry1 := 'SELECT 
          ID_SIGEVI, ID_EMPLEADO, CMP1, CMP2, CMP3
       FROM    
        ' || tablatmp ;
    
        OPEN C1 FOR qry1;
          LOOP  
            FETCH C1 INTO 
              c1_idSigevi,
              c1_cmp0 ,
              c1_cmp1 , 
              c1_cmp2 ,
              c1_cmp3 ;
            EXIT
                WHEN C1%NOTFOUND;  
                
                UPDATE rh_nomn_sigevi_datos set
                AUTORIZA_NACIONAL= c1_cmp1,
                AUTORIZA_GENERAL = c1_cmp2,
                CARGO  = c1_cmp3
                where numero_empleado = c1_cmp0 and ID_SIGEVI_FK = c1_idSigevi;
                commit;
          
            
          END LOOP;
          close c1;  
          
    qry1 := 'Select * from rh_nomn_sigevi_datos where ID_SIGEVI_FK =' || c1_idSigevi;
    
    OPEN V_CURSOR FOR qry1;
    
    
    EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
    

  EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp ;
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
  V_CURSOR := null;  
      

  
END SP_SIGEVI_ACTUALIZA ;

/
--------------------------------------------------------
--  DDL for Procedure SP_SIGEVI_OBTN_REPORTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SIGEVI_OBTN_REPORTE" (IDQUINCENA IN NUMBER, DATOS OUT SYS_REFCURSOR) IS

BEGIN

OPEN DATOS FOR
SELECT DISTINCT
         EM.NUMERO_EMPLEADO                                                                                    NUM_EMPLEADO
        ,DAT.RFC                                                                                               RFC
        ,TRANSLATE(UPPER(TO_CHAR(DAT.A_PATERNO||' '||A_MATERNO||' '||DAT.NOMBRE)),'����������', 'aeiouAEIOU')  NOMBRE
        ,TO_CHAR(UNIADM.CLAVE_UNIADM)                                                                          UNIDAD
        ,UPPER(TO_CHAR(GRU.CLAVE_JERARQUICA||GRA.GRADO||NIVTAB.NIVEL))                                         PUESTO
        ,CHR(39)||DATBAN.NUMERO_CUENTA||CHR(39)                                                                CUENTA
        ,CHR(39)||DATBAN.CLABE||CHR(39)                                                                        CLABE
        ,BANCO.DESCRIPCION_BANCO                                                                               NOMBRE_BANCO
        ,RTRIM (REGEXP_SUBSTR (DAT.CORREO_ELECTRONICO, '[^,]*@', 1, 1), '@')                                   CUENTA_DOMINIO 
        ,DAT.CORREO_ELECTRONICO                                                                                CORREO_ELECTRONICO 
        ,DECODE(ACC.ID_ACCION,7,'ALTA',8,'ACTUALIZA',9,'ACTUALIZA',10,'ACTUALIZA',11,'BAJA')                   OPERACION
        ,PUE.DESCRIPCION                                                                                       CARGO 
        ,ACC.ID_ACCION
FROM RH_NOMN_EMPLEADOS EM
LEFT  JOIN RH_NOMN_DATOS_PERSONALES        DAT     ON DAT.ID_DATOS_PERSONALES      = EM.ID_DATOS_PERSONALES
LEFT  JOIN RH_NOMN_DATOS_BANCARIOS         DATBAN  ON DATBAN.ID_DATOS_BANCARIOS    = EM.ID_DATOS_BANCARIOS
LEFT  JOIN RH_NOMN_CAT_BANCOS              BANCO   ON BANCO.ID_BANCO               = DATBAN.ID_BANCO  
LEFT  JOIN RH_NOMN_PLAZAS                  PLZ     ON PLZ.ID_PLAZA                 = EM.ID_PLAZA
LEFT  JOIN RH_NOMN_CAT_PUESTOS            PUE     ON PUE.ID_PUESTO                 = PLZ.ID_PUESTO_FK     
LEFT  JOIN RH_NOMN_ADSCRIPCIONES           ADSC    ON ADSC.ID_ADSCRIPCION          = PLZ.ID_ADSCRIPCION_FK
LEFT  JOIN RH_NOMN_CAT_UNIDADES_ADMINS     UNIADM  ON UNIADM.ID_UNIADM             = ADSC.ID_UNIADM_FK
LEFT  JOIN RH_NOMN_TABULADORES             TAB     ON TAB.ID_TABULADOR             = PLZ.ID_TABULADOR_FK 
LEFT  JOIN RH_NOMN_CAT_NIVELES_TABULADOR   NIVTAB  ON NIVTAB.ID_NIVEL_TABULADOR    = TAB.ID_NIVEL_TABULADOR_FK
LEFT  JOIN RH_NOMN_CAT_GRADOS_JERARQUICOS  GRA     ON GRA.ID_GRADO_JERARQUICO      = TAB.ID_GRADO_JERARQUICO_FK
LEFT  JOIN RH_NOMN_CAT_GRUPOS_JERARQUICOS  GRU     ON TAB.ID_GRUPO_JERARQUICO_FK   = GRU.ID_GRUPO_JERARQUICO
LEFT  JOIN RH_NOMN_BIT_HISTORICOS_CAMPOS   CAMP    ON CAMP.ID_REGISTRO_ENTIDAD     = EM.ID_EMPLEADO
LEFT  JOIN RH_NOMN_BIT_HISTORICOS_MOVS     MOV     ON MOV.ID_HISTORICO_MOVIMIENTO  = CAMP.ID_HISTORICO_MOVIMIENTO_FK
LEFT  JOIN RH_NOMN_CAT_ACCIONES            ACC     ON ACC.ID_ACCION                = MOV.ID_ACCION_FK
LEFT  JOIN RH_NOMN_CAT_TIPOS_NOMBRAMIENTO  NOMB    ON NOMB.ID_TIPO_NOMBRAMIENTO    = PLZ.ID_TIPO_NOMBRAM_FK
CROSS JOIN RH_NOMN_CAT_QUINCENAS           QUIN
WHERE MOV.FECHA_REGISTRO BETWEEN QUIN.FECHA_INICIO_QUINCENA AND QUIN.FECHA_FIN_QUINCENA
AND   MOV.FECHA BETWEEN QUIN.FECHA_INICIO_QUINCENA-45 AND QUIN.FECHA_FIN_QUINCENA+500
AND   QUIN.ID_QUINCENA = IDQUINCENA
AND   UPPER(ACC.DESCRIPCION) IN ('ALTA EMPLEADO','BAJA EMPLEADO','CAMBIAR PUESTO EMPLEADO','EDITAR EMPLEADO')
AND   UNIADM.CLAVE_UNIADM != 1
ORDER BY DECODE(ACC.ID_ACCION,7,2,8,1,9,3,10,4,11,5) ASC

;




END SP_SIGEVI_OBTN_REPORTE;

/
--------------------------------------------------------
--  DDL for Procedure SP_SSI_REP_HISTORICO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_SSI_REP_HISTORICO" (DATOS OUT SYS_REFCURSOR )IS
BEGIN

    OPEN DATOS FOR
    SELECT  EM.NUMERO_EMPLEADO,
            DAT.NOMBRE||' '||DAT.A_PATERNO||' '||DAT.A_MATERNO NOMBRE,
            DAT.RFC,
            PST.CLAVE_PUESTO PUESTO,
            ADS.CLAVE_ADSCRIPCION ADSCRIPCION,
            (TAB.SUELDO_BASE+TAB.COMPENSACION_GARANTIZADA) SUELDO,
            SEG.IMPORTE PAGO_QUINCENAL
    FROM RH_NOMN_SEG_SEPARA_IDIVIDUAL  SEP
    LEFT JOIN RH_NOMN_SEGUROS          SEG ON SEG.ID_SEGURO             = SEP.ID_SEGURO_FK
    LEFT JOIN RH_NOMN_EMPLEADOS        EM  ON EM.ID_EMPLEADO            = SEG.ID_EMPLEADO_FK
    LEFT JOIN RH_NOMN_DATOS_PERSONALES DAT ON DAT.ID_DATOS_PERSONALES   = EM.ID_DATOS_PERSONALES
    LEFT JOIN RH_NOMN_PLAZAS           PLZ ON PLZ.ID_PLAZA              = EM.ID_PLAZA
    LEFT JOIN RH_NOMN_CAT_PUESTOS      PST ON PST.ID_PUESTO             = PLZ.ID_PUESTO_FK
    LEFT JOIN RH_NOMN_ADSCRIPCIONES    ADS ON ADS.ID_ADSCRIPCION        = PLZ.ID_ADSCRIPCION_FK
    LEFT JOIN RH_NOMN_TABULADORES      TAB ON TAB.ID_TABULADOR          = PLZ.ID_TABULADOR_FK
    
;
END SP_SSI_REP_HISTORICO;

/
--------------------------------------------------------
--  DDL for Procedure SP_VIATICOS_VALIDA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SP_VIATICOS_VALIDA" 
    (
     datos       IN STRING_ARRAY,      
     V_CURSOR OUT SYS_REFCURSOR) 
IS
   C1 SYS_REFCURSOR;
   
  qry1 varchar(3000);
  qry2 varchar(3000);
  
    c1_ID_REGISTRO     NUMBER;
    c1_NUM_EMPLEADO    NUMBER; 
    c1_FECHA_PAGO      DATE; 
    c1_UNIDAD_ADMIN    NUMBER(3,0);
    c1_IMPORTE         NUMBER(8,2); 
    c1_DESC            VARCHAR2(1000);
    c1_MES_CFDI        VARCHAR2(20);
    c1_CONCEPTO        VARCHAR2(10);
    c1_TRANSFERENCIA   VARCHAR2(400);
    c1_QUINCENA        VARCHAR2(400);

  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(500 BYTE);
  
  tablatmp        VARCHAR2(300) ; --Para generar el nombre de la tabla temporar para crear
  existTable      integer := 0;

  vlRetCursor     SYS_REFCURSOR;     
  vCadenaSQL      varchar2(4000);  
  
  tmpVal          TYPE_STRING; --Para leer la variable TYPE_STRING
  tmpRegistro     VARCHAR2(3000):= ''; --Para generar el registro a insertar
  
  contTotRegistros    NUMERIC; -- cuenta el total de registros leidos y se usa para el id de la tabla temp
  contTotAceptados    NUMERIC; -- cuenta el total de registros que cumplen con las validaciones
  conttotRechazado    NUMERIC; -- cuenta el total de los registros que han sido rchazados
      
  -----------------------------------------------------------------------------
  
  varError varchar2(4000) := '0,';
  idTipoNombEmp number;
  cntConcepto number;
  cntUniAdm number;
  
  idConcepto number; varEstatus VARCHAR2(1 BYTE); idTipoNombConc number;          idClasificador number;
  
  cntIdEmp number; cntIdMyT number; cntQuincenas number; cntIdNomina number;
  
  vCadenaSQL varchar2(4000); 
BEGIN 
  
  tablatmp  := 'TMP_VIATICOS'  || '_'|| TO_CHAR(SYSDATE,'ddmmyy_hhmmss');

  -- VALIDAMOS QUE SI EXISTE LA TABLA TEMP SE ELIMINE
    SELECT COUNT(1) INTO existTable
        FROM ALL_TABLES 
        WHERE TABLE_NAME = tablatmp
        AND OWNER = 'SERP';
      --DBMS_OUTPUT.PUT_LINE(' existTable->' || existTable);
      
      IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      
      END IF; 
    
    ----------------------------------------------------------------------
    --  Q R Y   P A R A   C R E A R   L A   T A B L A   T E M P
    ----------------------------------------------------------------------
     
       EXECUTE IMMEDIATE 'CREATE TABLE '||tablatmp||' 
       (  
          ID_REGISTRO     NUMBER,
          NUM_EMPLEADO    NUMBER, 
          FECHA_PAGO      DATE, 
          UNIDAD_ADMIN    NUMBER(8,0), 
          IMPORTE         NUMBER(8,2), 
          DESCRIP         VARCHAR2(200),
          MES_CFDI        VARCHAR2(20),
          CONCEPTO        VARCHAR2(10),
          TRANSFERENCIA   VARCHAR2(400),
          QUINCENA        VARCHAR2(400),
          ESTATUS         NUMBER,
          MSJ_ERROR       VARCHAR2(4000)
       )';  
         
    ---------------------------------------------------------------------------                                                          
    -- R E C O R R E M O S   E L   A R R E G L O   P A R A   I N S E R T A R  
    -- L O S   V A L O R E S   E N   L A   T A B L A                                                         
    --------------------------------------------------------------------------- 
      contTotRegistros := 1;
      FOR idxj IN datos.first()..datos.last() 
        LOOP
          tmpVal  := datos(idxj);        
          SELECT TRANSLATE(tmpVal.REGISTRO,'����������', 'aeiouAEIOU') INTO tmpRegistro FROM DUAL;
          EXECUTE IMMEDIATE 'INSERT INTO '||tablatmp||' VALUES (' || contTotRegistros || ','|| tmpRegistro || ', 0, '''' )';         
          --EXECUTE IMMEDIATE 'INSERT INTO TMP_VIATICOS VALUES ( 980, NULL ,NULL,NULL,NULL ,NULL ,NULL,NULL,NULL ,NULL, 0, null )';         
          COMMIT;
          contTotRegistros := contTotRegistros + 1;
        END LOOP;
  
  ---------------------------------------------------------------------------
  -- R E C O R R E M O S   L O S   R E G I S T R O S   D E   L A   T A B L A
  ---------------------------------------------------------------------------
    qry1 := 'SELECT 
                ID_REGISTRO     ,
                NUM_EMPLEADO    , 
                FECHA_PAGO      , 
                UNIDAD_ADMIN    , 
                IMPORTE         ,
                DESCRIP            ,
                MES_CFDI        ,
                CONCEPTO        ,
                TRANSFERENCIA   ,
                QUINCENA        
              FROM    
        ' || tablatmp ;
    
    OPEN C1 FOR qry1;
      LOOP  
        FETCH C1 INTO 
          c1_ID_REGISTRO     ,
          c1_NUM_EMPLEADO    , 
          c1_FECHA_PAGO      , 
          c1_UNIDAD_ADMIN    , 
          c1_IMPORTE         ,
          c1_DESC            ,
          c1_MES_CFDI        ,
          c1_CONCEPTO        ,
          c1_TRANSFERENCIA   ,
          c1_QUINCENA  ;
        EXIT
            WHEN C1%NOTFOUND;  
            
          varError := '0,';  
        
          DBMS_OUTPUT.PUT_LINE('---------------------------------------------------' || c1_NUM_EMPLEADO );
          
          -- 1  id empleado no existe
              select count(ID_EMPLEADO) INTO cntIdEmp FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = c1_NUM_EMPLEADO ;
                if(cntIdEmp < 1)then
                    varError := varError ||  'EL empleado no existe, ';
                else
                    varError := '0,';
                    cntIdEmp := 1;
                        -- 2  empleado no esta activo, instrucci�n de aldo de quitar esta validaci�n
                        --select count(ID_EMPLEADO) INTO cntIdEmp FROM RH_NOMN_EMPLEADOS WHERE NUMERO_EMPLEADO = c1_NUM_EMPLEADO AND ID_ESTATUS_EMPLEADO = 1 ;
                        
                        if(cntIdEmp < 1)then
                            varError := varError || 'el empleado no esta activo, ';
                        else
                            
                            --3 la unidad administrativa no es existe
                            select count(CLAVE_UNIADM) into cntUniAdm  FROM RH_NOMN_CAT_UNIDADES_ADMINS WHERE CLAVE_UNIADM = c1_UNIDAD_ADMIN;
                            if( cntUniAdm < 1 )then
                              varError := varError || 'La unidad administrativa no existe,'; 
                            
                            end if;
                            
                            
                        end if; -- 2
                end if; -- 1  
                
                if(  varError  != '0,' )then
                    EXECUTE IMMEDIATE ' update '|| tablatmp || ' SET ESTATUS = 1, MSJ_ERROR = ''' || varError || ''' where ID_REGISTRO = '|| c1_ID_REGISTRO;
                    commit;
                end if;
                  
         -- DBMS_OUTPUT.PUT_LINE('      --->' || varError  );
          
      END LOOP;
      CLOSE C1;
  
       qry2 := 'SELECT 
                  ID_REGISTRO     ,
                  NUM_EMPLEADO    , 
                  FECHA_PAGO      , 
                  UNIDAD_ADMIN    ,
                  IMPORTE         ,
                  DESCRIP           ,
                  MES_CFDI        ,
                  CONCEPTO        ,
                  TRANSFERENCIA   ,
                  QUINCENA        ,
                  ESTATUS         ,
                  MSJ_ERROR       
               FROM    
                ' || tablatmp || '  ORDER BY  ESTATUS ASC, NUM_EMPLEADO ASC ';
        
        OPEN V_CURSOR FOR qry2;
        
 
  EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;



  
  EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 500);
    DBMS_OUTPUT.PUT_LINE('ERROR SE HACE ROLLBACK');
    SPDERRORES('SP_VIATICOS_VALIDA',err_code,err_msg,'987'); 
    
    ROLLBACK;
    
    
    SELECT COUNT(1) INTO existTable
      FROM ALL_TABLES 
      WHERE TABLE_NAME = tablatmp 
      AND OWNER = 'SERP';
      
     IF existTable > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('BORRA TABLA');
        EXECUTE IMMEDIATE 'DROP TABLE '||tablatmp;
      END IF;       
    DBMS_OUTPUT.PUT_LINE('Error:'||err_code||','||err_msg);
    SPDERRORES('SP_VIATICOS_VALIDA',err_code,err_msg,'987'); 
  V_CURSOR := null; 
  
END SP_VIATICOS_VALIDA ;

/
--------------------------------------------------------
--  DDL for Procedure SPARAMETROSFUCION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SPARAMETROSFUCION" (prmIdConcepto IN INTEGER,
                                                    P_RESPUESTASP OUT TYPES.CURSOR_TYPE)
as


v_formula VARCHAR2(150);
v_idformula integer;


BEGIN
       
    v_formula :='';
    v_idformula := 0;
    
    Select CF.FORMULA  into v_formula from RH_NOMN_CAT_CONCEPTOS CC 
    INNER JOIN RH_NOMN_CAT_FORMULAS CF ON CC.ID_FORMULA_FK= CF.ID_FORMULA
    WHERE CC.ID_CONCEPTO=prmIdConcepto;
    

  SELECT ID_formula into v_idformula FROM RH_NOMN_CAT_FORMULAS WHERE FORMULA = v_formula;
     
  delete from T_PASO;
   commit;
  delete from T_PASO2
  commit;
 
  FOR R1 in (SELECT ID_ARGUMENTO FROM  RH_NOMN_CAT_ARG_FORMULAS where ID_formula = v_idformula)
  loop
    INSERT INTO T_PASO ( SELECT FUNCION_ORACLE FROM RH_NOMN_CAT_ARGUMENTOS WHERE ID_ARGUMENTO = R1.ID_ARGUMENTO);
  END LOOP;

  FOR R2 IN (SELECT VALOR FROM t_PASO WHERE  VALOR IS NOT NULL)
   LOOP
    INSERT INTO T_PASO2 ( select distinct argument_name , position from  all_arguments 
                     where object_id in ( select object_id from all_objects where object_name = R2.VALOR) 
                     and   argument_name is not null
                     );
    END LOOP;
  
    
    open P_RESPUESTASP for 
    
    select VALOR FROM(SELECT DISTINCT VALOR,valor2 FROM  T_PASO2  where valor is not null order by 2 asc);
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    
    end SPARAMETROSFUCION;

/
--------------------------------------------------------
--  DDL for Procedure SPBAJAS_EMPLEADOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SPBAJAS_EMPLEADOS" 
IS  
PRAGMA AUTONOMOUS_TRANSACTION;

  ERR_CODE        INTEGER;
  ERR_MSG         VARCHAR2(250);
  VFECHABAJA      DATE;
  VPLAZA          INTEGER;
  VIDANIO         INTEGER;
  VIDQUI          INTEGER;
  VFECHAFINQUIN   DATE;
  VC              INTEGER;
  VC2             INTEGER;   
  VMAXHMOVS       INTEGER;
  VMAXHCAMP       INTEGER;
  VMAXMOVE        INTEGER;
  VMAXMOVP        INTEGER;
  PFECHA          VARCHAR2(250);
  
  VCANTPLAZA      NUMBER;   
  VIDEMPFUT       NUMBER;
  VNUMEMPFUT      NUMBER;


  ---  se modifica la condicion AND TO_DATE(FECHA_BAJA,'DD/MM/RR') =  TO_DATE(SYSDATE,'DD/MM/RR')   a 
  --  AND TO_DATE(FECHA_BAJA,'DD/MM/RR') <=  TO_DATE(SYSDATE,'DD/MM/RR')  esto en los cursores
  CURSOR C1 
  IS
    SELECT  
      A.ID_EMPLEADO ID_EMPLEADO,
      B.ID_PLAZA ID_PLAZA,
      A.FECHA_BAJA   FECHA_BAJA,
      A.NUMERO_EMPLEADO
    FROM 
      RH_NOMN_EMPLEADOS   A, 
      RH_NOMN_PLAZAS      B
    WHERE   
      A.ID_PLAZA = B.ID_PLAZA   
      AND     TO_DATE(FECHA_BAJA,'DD/MM/RR') <=  TO_DATE(SYSDATE,'DD/MM/RR')
      --AND     (TO_DATE(FECHA_BAJA,'DD/MM/RR') >  TO_DATE(FECHA_MODIFICACION,'DD/MM/RR') OR FECHA_MODIFICACION IS NULL)
      AND     ID_ESTATUS_EMPLEADO = 1 
    ;
    
BEGIN 

  SELECT 
    ID_EJERCICIO_FK,
    ID_QUINCENA, 
    FECHA_FIN_QUINCENA
  INTO 
    VIDANIO, 
    VIDQUI,
    VFECHAFINQUIN
  FROM 
    RH_NOMN_CAT_QUINCENAS
  WHERE 
    TO_DATE(SYSDATE, 'DD/MM/RR') BETWEEN FECHA_INICIO_QUINCENA AND FECHA_FIN_QUINCENA
  ;
       
  SELECT  
    COUNT(1)  
  INTO 
    VC 
  FROM 
    RH_NOMN_EMPLEADOS
  WHERE   
    TO_DATE(FECHA_BAJA,'DD/MM/RR') <=  TO_DATE(SYSDATE,'DD/MM/RR')
    --AND     (TO_DATE(FECHA_BAJA,'DD/MM/RR') >  TO_DATE(FECHA_MODIFICACION,'DD/MM/RR') OR FECHA_MODIFICACION IS NULL)
    AND     ID_ESTATUS_EMPLEADO = 1 
  ;
 
  IF VC > 0 THEN 

    FOR R1 IN C1 LOOP

      /*BAJA EMPELADO*/
      UPDATE 
        RH_NOMN_EMPLEADOS  
      SET 
        ID_ESTATUS_EMPLEADO  = 2,
        FECHA_MODIFICACION   = SYSDATE
      WHERE 
        ID_EMPLEADO          = R1.ID_EMPLEADO
      ;  
                                                                                                            
      /*BAJA PLAZA*/                                 
      UPDATE 
        RH_NOMN_PLAZAS  
      SET 
        ID_ESTATUS_PLAZA_FK     = 1 ,
        FECHA_DE_MODIFICACION   = SYSDATE,
        PROPIETARIO             = NULL
      WHERE 
        ID_PLAZA              = R1.ID_PLAZA
      ;
                         
      /*BAJA PENSION*/                               
      UPDATE  
        RH_NOMN_PENSIONES 
      SET 
        ID_ANIO_FIN       = VIDANIO ,
        ID_QUINCENA_FIN   = VIDQUI  ,
        ESTATUS           = 'I'
      WHERE 
        ID_EMPLEADO          = R1.ID_EMPLEADO
        AND ESTATUS          = 'A'
      ;
                             
      /*BAJA MANUALESYT*/ 
      UPDATE  
        RH_NOMN_MANUALES_TERCEROS  
      SET 
        FECHA_FIN            = VFECHAFINQUIN,
        ACTIVO               = 'I',
        FECHA_MODIFICACION   = SYSDATE
      WHERE 
        ID_EMPLEADO          = R1.ID_EMPLEADO
        AND    ACTIVO        = 'A'
      ;
       
      /*HISTORICO MOVIMIENTOS EMPLADO*/   
      SELECT 
        (max(ID_HISTORICO_MOVIMIENTO)+1) 
      INTO 
        VMAXHMOVS 
      FROM 
        RH_NOMN_BIT_HISTORICOS_MOVS
      ;                        
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_MOVS 
      (
        ID_HISTORICO_MOVIMIENTO, 
        ID_ACCION_FK, 
        ID_EJERCICIO_FK, 
        FECHA, 
        ID_USUARIO_FK, 
        TABLA
      ) 
      VALUES 
      ( 
        VMAXHMOVS,
        11,
        VIDANIO,
        SYSDATE,
        1,
        ''
      )
      ;    
                                                     
                                
      SELECT 
        TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
      INTO 
        PFECHA 
      FROM 
        DUAL
      ;
      
      /*HISTORICO CAMPOS EMPLEADO*/   
      SELECT 
        (max(ID_HISTORICO_CAMPO )+1) 
      INTO 
        VMAXHCAMP 
      FROM 
        RH_NOMN_BIT_HISTORICOS_CAMPOS
      ;
      
      
      SELECT  
        (MAX(ID_HISTORICO_MOVIMIENTO_FK )) 
      INTO 
        VMAXMOVE 
      FROM 
        RH_NOMN_BIT_HISTORICOS_CAMPOS
      WHERE  
        ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO 
      ;
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
      (
        ID_HISTORICO_CAMPO, 
        ID_HISTORICO_MOVIMIENTO_FK, 
        ID_REGISTRO_ENTIDAD, 
        ID_MOVIMIENTO_ANTERIOR, 
        ID_CAMPO_AFECTADO_FK, 
        VALOR_ACTUAL, 
        VALOR_BUSQUEDA
      ) 
      VALUES 
      ( 
        VMAXHCAMP,
        VMAXHMOVS,
        R1.ID_EMPLEADO,
        VMAXMOVE,
        10,
        2,
        R1.NUMERO_EMPLEADO
      )
      ;
      
      
      SELECT 
        (max(ID_HISTORICO_CAMPO )+1) 
      INTO 
        VMAXHCAMP 
      FROM 
        RH_NOMN_BIT_HISTORICOS_CAMPOS
      ;                                                        
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
      (
        ID_HISTORICO_CAMPO, 
        ID_HISTORICO_MOVIMIENTO_FK, 
        ID_REGISTRO_ENTIDAD, 
        ID_MOVIMIENTO_ANTERIOR, 
        ID_CAMPO_AFECTADO_FK, 
        VALOR_ACTUAL, 
        VALOR_BUSQUEDA
      ) 
      VALUES 
      ( 
        VMAXHCAMP,
        VMAXHMOVS,
        R1.ID_EMPLEADO,
        VMAXMOVE,
        25,
        PFECHA,
        R1.NUMERO_EMPLEADO
      )
      ; 
      
      
      SELECT 
        (max(ID_HISTORICO_CAMPO )+1) 
      INTO 
        VMAXHCAMP 
      FROM 
        RH_NOMN_BIT_HISTORICOS_CAMPOS
      ;                          
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
      (
        ID_HISTORICO_CAMPO, 
        ID_HISTORICO_MOVIMIENTO_FK, 
        ID_REGISTRO_ENTIDAD, 
        ID_MOVIMIENTO_ANTERIOR, 
        ID_CAMPO_AFECTADO_FK, 
        VALOR_ACTUAL, 
        VALOR_BUSQUEDA
      ) 
      VALUES 
      ( 
        VMAXHCAMP,
        VMAXHMOVS,
        R1.ID_PLAZA,
        VMAXMOVE,
        5,
        1,
        R1.ID_PLAZA
      )
      ;
      
      
      SELECT 
        (max(ID_HISTORICO_CAMPO )+1) 
      INTO 
        VMAXHCAMP 
      FROM 
        RH_NOMN_BIT_HISTORICOS_CAMPOS
      ;                          
      
      INSERT INTO RH_NOMN_BIT_HISTORICOS_CAMPOS 
      (
        ID_HISTORICO_CAMPO, 
        ID_HISTORICO_MOVIMIENTO_FK, 
        ID_REGISTRO_ENTIDAD, 
        ID_MOVIMIENTO_ANTERIOR, 
        ID_CAMPO_AFECTADO_FK, 
        VALOR_ACTUAL, 
        VALOR_BUSQUEDA
      ) 
      VALUES 
      (
        VMAXHCAMP,
        VMAXHMOVS,
        R1.ID_PLAZA,
        VMAXMOVE,
        6,
        0,
        R1.ID_PLAZA
      )
      ;
                       
      DBMS_OUTPUT.PUT_LINE('PROCESO DE BAJA PARA LOS EMPLEADOS : '|| R1.ID_EMPLEADO 
      ||' PLAZA: ' ||R1.ID_PLAZA||' CON FECHA DE BAJA: ' ||R1.FECHA_BAJA); 
      
      
      -- Cantidad de veces utilizada la plaza en los empleados ACTIVOS, BAJAS Y LICENCIAS 
      --  (por el tema de movimientos futuros).      
      SELECT 
        COUNT(ID_PLAZA)
      INTO
        VCANTPLAZA
      FROM 
        RH_NOMN_EMPLEADOS  
      WHERE
        ID_PLAZA = R1.ID_PLAZA
        AND ID_ESTATUS_EMPLEADO IN (1, 2, 3)
      ;  
      
      -- Validar si tiene un movimient futuro la plaza
      IF VCANTPLAZA = 2 THEN
      
        SELECT 
          ID_EMPLEADO,
          NUMERO_EMPLEADO
        INTO
          VIDEMPFUT,
          VNUMEMPFUT
        FROM 
          RH_NOMN_EMPLEADOS  
        WHERE
          ID_PLAZA                = R1.ID_PLAZA
          AND ID_ESTATUS_EMPLEADO = 1
        ;                              
        
        /*PLAZA OCUPADA POR EMPLEADO DE MOVIMIENTO FUTURO*/                                 
        UPDATE 
          RH_NOMN_PLAZAS  
        SET 
          ID_ESTATUS_PLAZA_FK     = 2,
          FECHA_DE_MODIFICACION   = SYSDATE,
          PROPIETARIO             = VNUMEMPFUT
        WHERE 
          ID_PLAZA              = R1.ID_PLAZA
        ;
        
      END IF;
            
    END LOOP;
    COMMIT;
  
  END IF;
  COMMIT;

  IF VC = 0 THEN 

    DBMS_OUTPUT.PUT_LINE('NO SE ENCONTRARON BAJAS'); 
  END IF;
               
  EXCEPTION  
  WHEN NO_DATA_FOUND THEN  
    NULL;
  WHEN OTHERS THEN   
    ROLLBACK;
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
    DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);
    
END SPBAJAS_EMPLEADOS;

/
--------------------------------------------------------
--  DDL for Procedure SPDERRORES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SPDERRORES" (
    PAPROCESO                 IN    VARCHAR2,
    PANUMERROR                IN    INTEGER,
    PADESERROR                IN    VARCHAR2,
    PIDEMPLEADO               IN    INTEGER 
)

IS
BEGIN

INSERT INTO RH_NOMN_BIT_ERRORES_FNNOM (ID_ERROR, PROCESO, NUM_ERROR, 
                                            DESC_ERROR, FECHA, USUARIO,P_IDEMPLEADO ) 
                                    VALUES ( BITACORANOM_SEQ.NEXTVAL,
                                             PAPROCESO,
                                             PANUMERROR,
                                             PADESERROR,
                                             SYSDATE,
                                             USER,
                                             PIDEMPLEADO);
   COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END SPDERRORES;

/
--------------------------------------------------------
--  DDL for Procedure SPOBTENERSUELDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SPOBTENERSUELDO" (prmIdEmp IN integer, prmIdPlaza IN integer, retValor OUT number) 
IS
  rtSueldoBase rh_nomn_tabuladores.sueldo_base%type;
  vIdTabulador rh_nomn_plazas.id_tabulador_fk%type;
BEGIN
  select id_tabulador_fk into vIdTabulador from rh_nomn_plazas where id_plaza= prmIdPlaza;
  select sueldo_base into retValor from rh_nomn_tabuladores where id_tabulador=vIdTabulador;

END spObtenerSueldo;

/
--------------------------------------------------------
--  DDL for Procedure SPVIGENCIA_PLAZA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "USR_SERP_QA"."SPVIGENCIA_PLAZA" 
IS  
PRAGMA AUTONOMOUS_TRANSACTION;
ERR_CODE                    INTEGER;
ERR_MSG                     VARCHAR2(250);
VFECHABAJA                  DATE;
VPLAZA                      INTEGER;
VIDANIO                     INTEGER;
VIDQUI                      INTEGER;
VC                          INTEGER;
VC2                         INTEGER;   
PLA_OCUPADA_NORMAL          INTEGER;
PLA_OCUPADA_TEMPORAL        INTEGER;
PLA_OCUPADA_TEMP_CAM_PSTO   INTEGER;
ID_EMP_INTERINATO           INTEGER;
NUMERO_EMP_INTERINATO       INTEGER;
VIGENCIA_FINAL_INTERINATO   DATE;
ID_PLA_EMP_INTERINATO       INTEGER;
NUMERO_PLA_INTERINATO       INTEGER;
PROPIETA_PLA_INTERINATO     INTEGER;
VMAXHMOVS                   INTEGER;
VMAXHCAMP                   INTEGER;
VMAXMOVE                    INTEGER;
VMAXMOVP                    INTEGER;
PFECHA                      VARCHAR2(250);
          
/* Tomar solo valores de estatus, RH_NOMN_CAT_ESTATUS_PLAZA 
        1=Vacante,  
        2=Ocupada, 
        5=Temporal
*/
CURSOR C1 IS
SELECT 
	A.ID_PLAZA,
	B.ID_EMPLEADO, 
	A.VIGENCIA_FINAL,
	A.ID_ESTATUS_PLAZA_FK,
	B.NUMERO_EMPLEADO,
  A.NUMERO_PLAZA
FROM  
	RH_NOMN_PLAZAS A, 
	RH_NOMN_EMPLEADOS B
WHERE 
	A.ID_PLAZA = B.ID_PLAZA (+)  
	AND   TO_DATE(TO_CHAR( A.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE + 1 ,'yyyy/mm/dd'), 'yyyy/mm/dd')
	AND   B.ID_ESTATUS_EMPLEADO NOT IN (2,3)
	AND   A.ID_ESTATUS_PLAZA_FK IN ( 2 )
UNION
SELECT 
	A.ID_PLAZA,
	B.ID_EMPLEADO, 
	A.VIGENCIA_FINAL,
	A.ID_ESTATUS_PLAZA_FK,
	B.NUMERO_EMPLEADO,
  A.NUMERO_PLAZA
FROM  
	RH_NOMN_PLAZAS A, 
	RH_NOMN_EMPLEADOS B
WHERE 
	A.ID_PLAZA = B.ID_PLAZA (+)  
	AND   TO_DATE(TO_CHAR( A.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE + 1 ,'yyyy/mm/dd'), 'yyyy/mm/dd')  
	AND   A.ID_ESTATUS_PLAZA_FK IN ( 1,5 )
;      

CURSOR C2 IS 
SELECT  
	A.ID_EMPLEADO,
	D.ID_PLAZA,
	C.ID_PENSION, 
	D.VIGENCIA_FINAL
FROM  
	RH_NOMN_EMPLEADOS  A,
	RH_NOMN_PENSIONES  C,
	RH_NOMN_PLAZAS     D
WHERE  
	D.ID_PLAZA = A.ID_PLAZA (+) 
	AND   C.ID_EMPLEADO = A.ID_EMPLEADO(+)
	AND   A.ID_ESTATUS_EMPLEADO = 2 
	AND   C.ESTATUS = 'I'
	AND   TO_DATE(TO_CHAR( d.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd' )
	AND   D.ID_ESTATUS_PLAZA_FK IN ( 1,2,5 )
;

CURSOR C3 IS         
SELECT  
	A.ID_EMPLEADO,  
	D.ID_PLAZA,  
	C.ID_MANUALES_TERCEROS, 
	D.VIGENCIA_FINAL 
FROM  
	RH_NOMN_EMPLEADOS  A,
	RH_NOMN_MANUALES_TERCEROS   C,
	RH_NOMN_PLAZAS     D
WHERE 
	D.ID_PLAZA = A.ID_PLAZA (+) 
	AND   C.ID_EMPLEADO = A.ID_EMPLEADO(+)
	AND   A.ID_ESTATUS_EMPLEADO = 2 
	AND   C.ACTIVO  ='I'
	AND   TO_DATE(TO_CHAR( d.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd' )
	AND   D.ID_ESTATUS_PLAZA_FK in ( 1,2,5 )
;
            
BEGIN 

	SELECT 
		ID_EJERCICIO_FK,
		ID_QUINCENA  
	INTO 
		VIDANIO, 
		VIDQUI
	FROM 
		RH_NOMN_CAT_QUINCENAS
	WHERE 
		TO_DATE(SYSDATE, 'DD/MM/RRRR') BETWEEN FECHA_INICIO_QUINCENA AND FECHA_FIN_QUINCENA
	;
       
	SELECT  
		COUNT(A.ID_PLAZA) 
	INTO 
		VC
	FROM  
		RH_NOMN_PLAZAS A, 
		RH_NOMN_EMPLEADOS B
	WHERE   
		A.ID_PLAZA = B.ID_PLAZA (+)  
		AND TO_DATE(TO_CHAR( a.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd' )
		AND A.ID_ESTATUS_PLAZA_FK in ( 1,2,5 )
	;
      
	IF VC > 0 THEN 
  
    DBMS_OUTPUT.PUT_LINE(  'VC: '|| VC);   
    
		FOR R1 IN C1 LOOP
		
			-- PARA PLAZAS TEMPORALES
			IF R1.ID_ESTATUS_PLAZA_FK = 5 THEN
        
				/*BAJA EMPELADO*/
				UPDATE 
					RH_NOMN_EMPLEADOS  
				SET 
					ID_ESTATUS_EMPLEADO   = 2,
					FECHA_MODIFICACION    = R1.VIGENCIA_FINAL
          /*20171030*/
          ,FECHA_BAJA = R1.VIGENCIA_FINAL
				WHERE 
					ID_EMPLEADO = R1.ID_EMPLEADO
				;
                      
				/*BAJA PENSION*/                               
				UPDATE  
					RH_NOMN_PENSIONES 
				SET 
					ID_ANIO_FIN       = VIDANIO,
					ID_QUINCENA_FIN     = VIDQUI,
					ESTATUS             = 'I'
				WHERE 
					ID_EMPLEADO   = R1.ID_EMPLEADO 
					AND ESTATUS   ='A' 
				;
                                     
				/*BAJA MANUALESYT*/ 
				UPDATE  
					RH_NOMN_MANUALES_TERCEROS  
				SET 
					FECHA_FIN           = R1.VIGENCIA_FINAL,
					ACTIVO              = 'I' ,
					FECHA_MODIFICACION  = R1.VIGENCIA_FINAL
				WHERE 
					ID_EMPLEADO = R1.ID_EMPLEADO 
					AND  ACTIVO ='A'
				;         
        
				/*HISTORICO MOVIMIENTOS EMPLADO*/   
				SELECT 
					(max(ID_HISTORICO_MOVIMIENTO)+1) 
				INTO 
					VMAXHMOVS 
				FROM 
					RH_NOMN_BIT_HISTORICOS_MOVS
				;                        
        
				INSERT INTO 
					RH_NOMN_BIT_HISTORICOS_MOVS 
					( 
					ID_HISTORICO_MOVIMIENTO, 
					ID_ACCION_FK, 
					ID_EJERCICIO_FK, 
					FECHA, 
					ID_USUARIO_FK, 
					TABLA
					) 
				VALUES 
					( 
					VMAXHMOVS,
					12,
					VIDANIO,
					R1.VIGENCIA_FINAL,
					1,
					''
					)
				;    
                                      
				SELECT 
					TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
				INTO 
					PFECHA 
				FROM 
					DUAL
				;
          
				/*HISTORICO CAMPOS EMPLEADO*/   
				SELECT  
					nvl((max(ID_HISTORICO_CAMPO )+1),1) 
				INTO 
					VMAXHCAMP 
				FROM 
					RH_NOMN_BIT_HISTORICOS_CAMPOS
				;  
        
				SELECT  
					NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
				INTO 
					VMAXMOVE 
				FROM 
					RH_NOMN_BIT_HISTORICOS_CAMPOS
				WHERE  
					ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO 
				;
        
				INSERT INTO 
					RH_NOMN_BIT_HISTORICOS_CAMPOS 
					( 
					ID_HISTORICO_CAMPO, 
					ID_HISTORICO_MOVIMIENTO_FK, 
					ID_REGISTRO_ENTIDAD, 
					ID_MOVIMIENTO_ANTERIOR, 
					ID_CAMPO_AFECTADO_FK, 
					VALOR_ACTUAL, 
					VALOR_BUSQUEDA
					) 
				VALUES 
					( 
					VMAXHCAMP,
					VMAXHMOVS,
					R1.ID_EMPLEADO,
					VMAXMOVE,
					10,
					2,
          /*20171030*/
					--R1.ID_EMPLEADO
          R1.NUMERO_EMPLEADO
					)
				;    
            
				SELECT  
					nvl((max(ID_HISTORICO_CAMPO )+1),1) 
				INTO 
					VMAXHCAMP 
				FROM 
					RH_NOMN_BIT_HISTORICOS_CAMPOS
				;
          
				INSERT INTO 
					RH_NOMN_BIT_HISTORICOS_CAMPOS 
					( 
					ID_HISTORICO_CAMPO, 
					ID_HISTORICO_MOVIMIENTO_FK, 
					ID_REGISTRO_ENTIDAD, 
					ID_MOVIMIENTO_ANTERIOR, 
					ID_CAMPO_AFECTADO_FK, 
					VALOR_ACTUAL, 
					VALOR_BUSQUEDA
					) 
				VALUES 
					( 
					VMAXHCAMP,
					VMAXHMOVS,
					R1.ID_EMPLEADO,
					VMAXMOVE,
					25,
					PFECHA,
          /*20171030*/
          --R1.ID_EMPLEADO
          R1.NUMERO_EMPLEADO
					)
				; 
                                    
				SELECT  
					NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
				INTO 
					VMAXHCAMP 
				FROM 
					RH_NOMN_BIT_HISTORICOS_CAMPOS
				;                          
        
				INSERT INTO 
					RH_NOMN_BIT_HISTORICOS_CAMPOS 
					( 
					ID_HISTORICO_CAMPO, 
					ID_HISTORICO_MOVIMIENTO_FK, 
					ID_REGISTRO_ENTIDAD, 
					ID_MOVIMIENTO_ANTERIOR, 
					ID_CAMPO_AFECTADO_FK, 
					VALOR_ACTUAL, 
					VALOR_BUSQUEDA
					) 
				VALUES 
					( 
					VMAXHCAMP,
					VMAXHMOVS,
					R1.ID_PLAZA,
					VMAXMOVE,
					5,
					6,
          /*20171030*/
          --R1.ID_PLAZA
          R1.NUMERO_PLAZA
					)
				;                                                    
        
			-- PARA PLAZAS OCUPADAS, OCUPADAS TEMPORALES NUEVO INGRESO Y OCUPADES TEMPORALES CAMBIO DE PUESTO.
			ELSIF R1.ID_ESTATUS_PLAZA_FK = 2 THEN   
      
				SELECT
					COUNT(1)
				INTO
					PLA_OCUPADA_NORMAL
				FROM  
					RH_NOMN_PLAZAS A, 
					RH_NOMN_EMPLEADOS B
				WHERE 
					A.ID_PLAZA = B.ID_PLAZA (+)  
					AND TO_DATE(TO_CHAR( A.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd')
					AND A.ID_ESTATUS_PLAZA_FK = 2
					AND B.ID_ESTATUS_EMPLEADO = 1
					AND B.ID_EMPLEADO = R1.ID_EMPLEADO
					AND B.NUMERO_EMPLEADO = A.PROPIETARIO
				; 
          
				SELECT
					COUNT(1)
				INTO
					PLA_OCUPADA_TEMPORAL
				FROM  
					RH_NOMN_PLAZAS A, 
					RH_NOMN_EMPLEADOS B
				WHERE 
					A.ID_PLAZA = B.ID_PLAZA (+)  
					AND TO_DATE(TO_CHAR( A.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd')
					AND A.ID_ESTATUS_PLAZA_FK = 2
					AND B.ID_ESTATUS_EMPLEADO = 1
					AND B.NUMERO_EMPLEADO != A.PROPIETARIO
				;                     
              
				-- PLAZA OCUPADA NORMAL            
				IF PLA_OCUPADA_NORMAL > 0 THEN
        
					/*BAJA EMPELADO*/
					UPDATE 
						RH_NOMN_EMPLEADOS  
					SET 
						ID_ESTATUS_EMPLEADO   = 2,
						FECHA_MODIFICACION    = R1.VIGENCIA_FINAL
            /*20171030*/
            ,FECHA_BAJA = R1.VIGENCIA_FINAL
					WHERE 
						ID_EMPLEADO = R1.ID_EMPLEADO
					;
                        
					/*BAJA PENSION*/                               
					UPDATE  
						RH_NOMN_PENSIONES 
					SET 
						ID_ANIO_FIN       = VIDANIO,
						ID_QUINCENA_FIN     = VIDQUI,
						ESTATUS             = 'I'
					WHERE 
						ID_EMPLEADO   = R1.ID_EMPLEADO 
						AND ESTATUS   ='A' 
					;
                                       
					/*BAJA MANUALESYT*/ 
					UPDATE  
						RH_NOMN_MANUALES_TERCEROS  
					SET 
						FECHA_FIN           = R1.VIGENCIA_FINAL,
						ACTIVO              = 'I' ,
						FECHA_MODIFICACION  = R1.VIGENCIA_FINAL
					WHERE 
						ID_EMPLEADO = R1.ID_EMPLEADO 
						AND  ACTIVO ='A'
					;   
        			
					/*HISTORICO MOVIMIENTOS EMPLADO*/   
					SELECT 
						(max(ID_HISTORICO_MOVIMIENTO)+1) 
					INTO 
						VMAXHMOVS 
					FROM 
						RH_NOMN_BIT_HISTORICOS_MOVS
					;                        
			
					INSERT INTO 
						RH_NOMN_BIT_HISTORICOS_MOVS 
						( 
						ID_HISTORICO_MOVIMIENTO, 
						ID_ACCION_FK, 
						ID_EJERCICIO_FK, 
						FECHA, 
						ID_USUARIO_FK, 
						TABLA
						) 
					VALUES 
						( 
						VMAXHMOVS,
						12,
						VIDANIO,
						R1.VIGENCIA_FINAL,
						1,
						''
						)
					;    
										  
					SELECT 
						TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
					INTO 
						PFECHA 
					FROM 
						DUAL
					;
			  
					/*HISTORICO CAMPOS EMPLEADO*/   
					SELECT  
						nvl((max(ID_HISTORICO_CAMPO )+1),1) 
					INTO 
						VMAXHCAMP 
					FROM 
						RH_NOMN_BIT_HISTORICOS_CAMPOS
					;  
			
					SELECT  
						NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
					INTO 
						VMAXMOVE 
					FROM 
						RH_NOMN_BIT_HISTORICOS_CAMPOS
					WHERE  
						ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO 
					;
			
					INSERT INTO 
						RH_NOMN_BIT_HISTORICOS_CAMPOS 
						( 
						ID_HISTORICO_CAMPO, 
						ID_HISTORICO_MOVIMIENTO_FK, 
						ID_REGISTRO_ENTIDAD, 
						ID_MOVIMIENTO_ANTERIOR, 
						ID_CAMPO_AFECTADO_FK, 
						VALOR_ACTUAL, 
						VALOR_BUSQUEDA
						) 
					VALUES 
						( 
						VMAXHCAMP,
						VMAXHMOVS,
						R1.ID_EMPLEADO,
						VMAXMOVE,
						10,
						2,
            /*20171030*/
            --R1.ID_EMPLEADO
            R1.NUMERO_EMPLEADO
						)
					;    
				
					SELECT  
						nvl((max(ID_HISTORICO_CAMPO )+1),1) 
					INTO 
						VMAXHCAMP 
					FROM 
						RH_NOMN_BIT_HISTORICOS_CAMPOS
					;
			  
					INSERT INTO 
						RH_NOMN_BIT_HISTORICOS_CAMPOS 
						( 
						ID_HISTORICO_CAMPO, 
						ID_HISTORICO_MOVIMIENTO_FK, 
						ID_REGISTRO_ENTIDAD, 
						ID_MOVIMIENTO_ANTERIOR, 
						ID_CAMPO_AFECTADO_FK, 
						VALOR_ACTUAL, 
						VALOR_BUSQUEDA
						) 
					VALUES 
						( 
						VMAXHCAMP,
						VMAXHMOVS,
						R1.ID_EMPLEADO,
						VMAXMOVE,
						25,
						PFECHA,
            /*20171030*/
            --R1.ID_EMPLEADO
            R1.NUMERO_EMPLEADO
						)
					; 
										
					SELECT  
						NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
					INTO 
						VMAXHCAMP 
					FROM 
						RH_NOMN_BIT_HISTORICOS_CAMPOS
					;                          
			
					INSERT INTO 
						RH_NOMN_BIT_HISTORICOS_CAMPOS 
						( 
						ID_HISTORICO_CAMPO, 
						ID_HISTORICO_MOVIMIENTO_FK, 
						ID_REGISTRO_ENTIDAD, 
						ID_MOVIMIENTO_ANTERIOR, 
						ID_CAMPO_AFECTADO_FK, 
						VALOR_ACTUAL, 
						VALOR_BUSQUEDA
						) 
					VALUES 
						( 
						VMAXHCAMP,
						VMAXHMOVS,
						R1.ID_PLAZA,
						VMAXMOVE,
						5,
						6,
            /*20171030*/
            --R1.ID_PLAZA
            R1.NUMERO_PLAZA
						)
					;					
        
				-- PLAZAS OCUPADAS TEMPORALMENTE            
				ELSIF PLA_OCUPADA_TEMPORAL > 0 THEN          
       
					SELECT
						B.ID_EMPLEADO,
						B.NUMERO_EMPLEADO,
						A.VIGENCIA_FINAL
					INTO
						ID_EMP_INTERINATO,
						NUMERO_EMP_INTERINATO,
						VIGENCIA_FINAL_INTERINATO
					FROM  
						RH_NOMN_PLAZAS A, 
						RH_NOMN_EMPLEADOS B
					WHERE 
						A.ID_PLAZA = B.ID_PLAZA (+)  
						AND TO_DATE(TO_CHAR( A.VIGENCIA_FINAL ,'yyyy/mm/dd'), 'yyyy/mm/dd' ) <= TO_DATE(TO_CHAR( SYSDATE  ,'yyyy/mm/dd'), 'yyyy/mm/dd')
						AND A.ID_ESTATUS_PLAZA_FK = 2
						AND B.ID_ESTATUS_EMPLEADO = 1
						AND B.ID_EMPLEADO =  R1.ID_EMPLEADO
						AND B.NUMERO_EMPLEADO != A.PROPIETARIO
					;
            
					SELECT
						COUNT(1)
					INTO  
						PLA_OCUPADA_TEMP_CAM_PSTO
					FROM  
						RH_NOMN_PLAZAS
					WHERE 
						PROPIETARIO = NUMERO_EMP_INTERINATO
					;
					      
					-- PLAZAS OCUPADAS TEMPORALMENTE POR INTERINATO (CAMBIO DE PUESTO).
					IF PLA_OCUPADA_TEMP_CAM_PSTO > 0 THEN
          
						/*OBTENER LOS DATOS DE LA PLAZA CONGELADA DEL EMPLEADO DE INTERINATO*/
						SELECT
							ID_PLAZA,
							NUMERO_PLAZA,
							PROPIETARIO              
						INTO
							ID_PLA_EMP_INTERINATO,
							NUMERO_PLA_INTERINATO,
							PROPIETA_PLA_INTERINATO
						FROM  
							RH_NOMN_PLAZAS
						WHERE 
							PROPIETARIO = NUMERO_EMP_INTERINATO
						;    
            
						/*REGRESAR EMPLEADO DE INTERINATO A SU PLAZA CONGELADA*/
						UPDATE 
							RH_NOMN_EMPLEADOS  
						SET   
							ID_PLAZA = ID_PLA_EMP_INTERINATO,
							FECHA_MODIFICACION    = VIGENCIA_FINAL_INTERINATO
						WHERE 
							ID_EMPLEADO = ID_EMP_INTERINATO
						;  
            
						/*SE REACTIVA LA PLAZA CONGELADA A OCUPADA POR EL EMPLEADO QUE ESTABA EN INTERINATO*/
						UPDATE 
							RH_NOMN_PLAZAS  
						SET   
							ID_ESTATUS_PLAZA_FK = 2,
							FECHA_DE_MODIFICACION = VIGENCIA_FINAL_INTERINATO
						WHERE 
							ID_PLAZA = ID_PLA_EMP_INTERINATO
						;    
            
						/*REALIZAR LOS HISTORICOS PARA EL CAMBIO DE PUESTO*/               
            SELECT 
              (max(ID_HISTORICO_MOVIMIENTO)+1) 
            INTO 
              VMAXHMOVS 
            FROM 
              RH_NOMN_BIT_HISTORICOS_MOVS
            ;
            
            INSERT INTO 
              RH_NOMN_BIT_HISTORICOS_MOVS 
              (
              ID_HISTORICO_MOVIMIENTO, 
              ID_ACCION_FK, 
              ID_EJERCICIO_FK,
              FECHA, 
              ID_USUARIO_FK, 
              TABLA
              )
            VALUES 
              ( 
              VMAXHMOVS , 
              16, 
              VIDANIO, 
              SYSDATE, 
              1, 
              '')
            ;
            
            /*HISTORICO CAMPOS EMPLEADO*/   
            SELECT 
              TO_CHAR((TO_DATE(SYSDATE,'DD/MM/RRRR')),'DD/MM/RRRR') 
            INTO 
              PFECHA 
            FROM 
              DUAL
            ;
            
            SELECT 
              (max(ID_HISTORICO_CAMPO )+1) 
            INTO 
              VMAXHCAMP 
            FROM 
              RH_NOMN_BIT_HISTORICOS_CAMPOS
            ;
          
            --MODIFICA EL CAMPO ID_ESTATUS_EMPLEADO
            SELECT  
              nvl((MAX(ID_HISTORICO_MOVIMIENTO_FK )),1) 
            INTO 
              VMAXMOVE 
            FROM 
              RH_NOMN_BIT_HISTORICOS_CAMPOS
            WHERE  
              ID_REGISTRO_ENTIDAD = ID_EMP_INTERINATO
            ;
            
            --EMPLEADO CAMBIA A ESTATUS CAMBIO DE PUESTO
            INSERT INTO 
              RH_NOMN_BIT_HISTORICOS_CAMPOS 
              (
              ID_HISTORICO_CAMPO, 
              ID_HISTORICO_MOVIMIENTO_FK, 
              ID_REGISTRO_ENTIDAD, 
              ID_MOVIMIENTO_ANTERIOR, 
              ID_CAMPO_AFECTADO_FK, 
              VALOR_ACTUAL, 
              VALOR_BUSQUEDA
              ) 
            VALUES 
              ( 
              VMAXHCAMP ,
              VMAXHMOVS,
              ID_EMP_INTERINATO,
              VMAXMOVE,
              10,
              4,
              NUMERO_EMP_INTERINATO
              )
            ;
                
            --se debe registrar la fecha de la baja
            SELECT 
              (max(ID_HISTORICO_CAMPO )+1) 
            INTO 
              VMAXHCAMP 
            FROM 
              RH_NOMN_BIT_HISTORICOS_CAMPOS
            ;                                            
            
            INSERT INTO 
              RH_NOMN_BIT_HISTORICOS_CAMPOS 
              (
              ID_HISTORICO_CAMPO, 
              ID_HISTORICO_MOVIMIENTO_FK, 
              ID_REGISTRO_ENTIDAD, 
              ID_MOVIMIENTO_ANTERIOR, 
              ID_CAMPO_AFECTADO_FK, 
              VALOR_ACTUAL, 
              VALOR_BUSQUEDA
              ) 
            VALUES 
              ( 
              VMAXHCAMP ,
              VMAXHMOVS,
              ID_EMP_INTERINATO,
              VMAXMOVE,
              25,
              PFECHA,
              NUMERO_EMP_INTERINATO
              )
            ;
            
            /*HISTORICO CAMPOS PLAZA CONGELDA*/
            --MOVIENTO A LA PLAZA CONGELADA QUEDA CON ESTATUS OCUPADA.
            SELECT 
              (max(ID_HISTORICO_CAMPO )+1) 
            INTO 
              VMAXHCAMP 
            FROM 
              RH_NOMN_BIT_HISTORICOS_CAMPOS
            ;
            
            INSERT INTO 
              RH_NOMN_BIT_HISTORICOS_CAMPOS 
              (
              ID_HISTORICO_CAMPO, 
              ID_HISTORICO_MOVIMIENTO_FK, 
              ID_REGISTRO_ENTIDAD, 
              ID_MOVIMIENTO_ANTERIOR, 
              ID_CAMPO_AFECTADO_FK, 
              VALOR_ACTUAL, 
              VALOR_BUSQUEDA
              ) 
            VALUES 
              ( 
              VMAXHCAMP ,
              VMAXHMOVS,
              ID_PLA_EMP_INTERINATO,
              VMAXMOVE,
              5,
              2, 
              /*20171030*/
              --ID_PLA_EMP_INTERINATO
              NUMERO_PLA_INTERINATO
              )
            ;                                              
            
						/*BAJA EMPELADO POR LICENCIA*/
						UPDATE 
							RH_NOMN_EMPLEADOS  
						SET 
							ID_ESTATUS_EMPLEADO   = 2,
							FECHA_MODIFICACION    = R1.VIGENCIA_FINAL
						WHERE 
							ID_EMPLEADO = R1.ID_EMPLEADO
						;
                          
						/*BAJA PENSION*/                               
						UPDATE  
							RH_NOMN_PENSIONES 
						SET 
							ID_ANIO_FIN       = VIDANIO,
							ID_QUINCENA_FIN     = VIDQUI,
							ESTATUS             = 'I'
						WHERE 
							ID_EMPLEADO   = R1.ID_EMPLEADO 
							AND ESTATUS   ='A' 
						;
                                         
						/*BAJA MANUALESYT*/ 
						UPDATE  
							RH_NOMN_MANUALES_TERCEROS  
						SET 
							FECHA_FIN           = R1.VIGENCIA_FINAL,
							ACTIVO              = 'I' ,
							FECHA_MODIFICACION  = R1.VIGENCIA_FINAL
						WHERE 
							ID_EMPLEADO = R1.ID_EMPLEADO 
							AND  ACTIVO ='A'
						;               
            
						/*HISTORICO MOVIMIENTOS EMPLADO POR LICENCIA*/   
						SELECT 
							(max(ID_HISTORICO_MOVIMIENTO)+1) 
						INTO 
							VMAXHMOVS 
						FROM 
							RH_NOMN_BIT_HISTORICOS_MOVS
						;                        
            
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_MOVS 
							( 
							ID_HISTORICO_MOVIMIENTO, 
							ID_ACCION_FK, 
							ID_EJERCICIO_FK, 
							FECHA, 
							ID_USUARIO_FK, 
							TABLA
							) 
						VALUES 
							( 
							VMAXHMOVS,
							12,
							VIDANIO,
							R1.VIGENCIA_FINAL,
							1,
							''
							)
						;    
                                                  
						SELECT 
							TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
						INTO 
							PFECHA 
						FROM 
							DUAL
						;
              
						/*HISTORICO CAMPOS EMPLEADO*/   
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;  
                    
						SELECT  
							NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
						INTO 
							VMAXMOVE 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						WHERE  
							ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO 
						;
                
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_EMPLEADO,
							VMAXMOVE,
							10,
							2,
              /*20171030*/
              --R1.ID_EMPLEADO
              R1.NUMERO_EMPLEADO
							)
						;    
                      
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;
                
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_EMPLEADO,
							VMAXMOVE,
							25,
							PFECHA,
              /*20171030*/
              --R1.ID_EMPLEADO
              R1.NUMERO_EMPLEADO
							)
						; 
                                              
						SELECT  
							NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;                          
                
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_PLAZA,
							VMAXMOVE,
							5,
							6,
              /*20171030*/
              --R1.ID_PLAZA
              R1.NUMERO_PLAZA
							)
						;                                   
                      
					-- PLAZAS OCUPADAS TEMPORALMENTE POR INTERINATO (NUEVO INGRESO).
					ELSE
          
						/*BAJA EMPELADO RECIEN INGRESO*/
						UPDATE 
							RH_NOMN_EMPLEADOS  
						SET 
							ID_ESTATUS_EMPLEADO   = 2,
							FECHA_MODIFICACION    = VIGENCIA_FINAL_INTERINATO
              /*20171030*/
              ,FECHA_BAJA = R1.VIGENCIA_FINAL              
						WHERE 
							ID_EMPLEADO = ID_EMP_INTERINATO
						;
                          
						/*BAJA PENSION*/                               
						UPDATE  
							RH_NOMN_PENSIONES 
						SET 
							ID_ANIO_FIN         = VIDANIO,
							ID_QUINCENA_FIN     = VIDQUI,
							ESTATUS             = 'I'
						WHERE 
							ID_EMPLEADO   = ID_EMP_INTERINATO 
							AND ESTATUS   ='A' 
						;
                                         
						/*BAJA MANUALESYT*/ 
						UPDATE  
							RH_NOMN_MANUALES_TERCEROS  
						SET 
							FECHA_FIN           = VIGENCIA_FINAL_INTERINATO,
							ACTIVO              = 'I' ,
							FECHA_MODIFICACION  = VIGENCIA_FINAL_INTERINATO
						WHERE 
							ID_EMPLEADO =ID_EMP_INTERINATO 
							AND  ACTIVO ='A'
						;          
          
						/*BAJA EMPELADO POR LICENCIA*/
						UPDATE 
							RH_NOMN_EMPLEADOS  
						SET 
							ID_ESTATUS_EMPLEADO   = 2,
							FECHA_MODIFICACION    = R1.VIGENCIA_FINAL
              /*20171030*/
              ,FECHA_BAJA = R1.VIGENCIA_FINAL          
						WHERE 
							ID_EMPLEADO = R1.ID_EMPLEADO
						;
                          
						/*BAJA PENSION*/                               
						UPDATE  
							RH_NOMN_PENSIONES 
						SET 
							ID_ANIO_FIN       = VIDANIO,
							ID_QUINCENA_FIN     = VIDQUI,
							ESTATUS             = 'I'
						WHERE 
							ID_EMPLEADO   = R1.ID_EMPLEADO 
							AND ESTATUS   ='A' 
						;
                                         
						/*BAJA MANUALESYT*/ 
						UPDATE  
							RH_NOMN_MANUALES_TERCEROS  
						SET 
							FECHA_FIN           = R1.VIGENCIA_FINAL,
							ACTIVO              = 'I' ,
							FECHA_MODIFICACION  = R1.VIGENCIA_FINAL
						WHERE 
							ID_EMPLEADO = R1.ID_EMPLEADO 
							AND  ACTIVO ='A'
						;      
            
						/*HISTORICO MOVIMIENTOS EMPLADO RECIEN INGRESO*/   
						SELECT 
							(max(ID_HISTORICO_MOVIMIENTO)+1) 
						INTO 
							VMAXHMOVS 
						FROM 
							RH_NOMN_BIT_HISTORICOS_MOVS
						;                        
            
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_MOVS 
							( 
							ID_HISTORICO_MOVIMIENTO, 
							ID_ACCION_FK, 
							ID_EJERCICIO_FK, 
							FECHA, 
							ID_USUARIO_FK, 
							TABLA
							) 
						VALUES 
							( 
							VMAXHMOVS,
							12,
							VIDANIO,
							VIGENCIA_FINAL_INTERINATO,
							1,
							''
							)						
						;    
                                        
						SELECT 
							TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
						INTO 
							PFECHA 
						FROM 
							DUAL
						;
              
						/*HISTORICO CAMPOS EMPLEADO*/   
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;  
                  
						SELECT  
							NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
						INTO 
							VMAXMOVE 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						WHERE  
							ID_REGISTRO_ENTIDAD = ID_EMP_INTERINATO 
						;
            
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							ID_EMP_INTERINATO,
							VMAXMOVE,
							10,
							2,
              /*20171030*/
							--ID_EMP_INTERINATO
							NUMERO_EMP_INTERINATO
              )
						;    
                    
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;
              
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							ID_EMP_INTERINATO,
							VMAXMOVE,
							25,
							PFECHA,
               /*20171030*/
              --ID_EMP_INTERINATO
              NUMERO_EMP_INTERINATO      
							)
						; 
                                            
						SELECT  
							NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;                          
              
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							ID_EMP_INTERINATO,
							VMAXMOVE,
							5,
							6,
               /*20171030*/
              --ID_EMP_INTERINATO
              NUMERO_EMP_INTERINATO      
							)
						;                                
              
						/*HISTORICO MOVIMIENTOS EMPLADO POR LICENCIA*/   
						SELECT 
							(max(ID_HISTORICO_MOVIMIENTO)+1) 
						INTO 
							VMAXHMOVS 
						FROM 
							RH_NOMN_BIT_HISTORICOS_MOVS
						;                        
            
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_MOVS 
							( 
							ID_HISTORICO_MOVIMIENTO, 
							ID_ACCION_FK, 
							ID_EJERCICIO_FK, 
							FECHA, 
							ID_USUARIO_FK, 
							TABLA
							) 
						VALUES 
							( 
							VMAXHMOVS,
							12,
							VIDANIO,
							R1.VIGENCIA_FINAL,
							1,
							''
							)
						;    
                                                
						SELECT 
							TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
						INTO 
							PFECHA 
						FROM 
							DUAL;
                
						/*HISTORICO CAMPOS EMPLEADO*/   
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;  
                  
						SELECT  
							NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
						INTO 
							VMAXMOVE 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						WHERE  
							ID_REGISTRO_ENTIDAD = R1.ID_EMPLEADO 
						;
                
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_EMPLEADO,
							VMAXMOVE,
							10,
							2,
              /*20171030*/
              --R1.ID_EMPLEADO
              R1.NUMERO_EMPLEADO
							)
						;    
                  
						SELECT  
							nvl((max(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;
                    
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_EMPLEADO,
							VMAXMOVE,
							25,
							PFECHA,
              /*20171030*/
              --R1.ID_EMPLEADO
              R1.NUMERO_EMPLEADO
							)
						; 
                                              
						SELECT  
							NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
						INTO 
							VMAXHCAMP 
						FROM 
							RH_NOMN_BIT_HISTORICOS_CAMPOS
						;                          
                
						INSERT INTO 
							RH_NOMN_BIT_HISTORICOS_CAMPOS 
							( 
							ID_HISTORICO_CAMPO, 
							ID_HISTORICO_MOVIMIENTO_FK, 
							ID_REGISTRO_ENTIDAD, 
							ID_MOVIMIENTO_ANTERIOR, 
							ID_CAMPO_AFECTADO_FK, 
							VALOR_ACTUAL, 
							VALOR_BUSQUEDA
							) 
						VALUES 
							( 
							VMAXHCAMP,
							VMAXHMOVS,
							R1.ID_PLAZA,
							VMAXMOVE,
							5,
							6,
              /*20171030*/
              --R1.ID_PLAZA
              R1.NUMERO_PLAZA
							)
						;                                 
					END IF;
				END IF;                 
			END IF;			                  
			
			/*ESTE CAMBIO APLICA PARA TODAS LAS OPCIONES INCLUIDA LA DE LA PLAZA VACANTE (R1.ID_ESTATUS_PLAZA_FK = 1)*/	
			/*BAJA PLAZA*/                                 
			UPDATE 
				RH_NOMN_PLAZAS  
			SET 
				ID_ESTATUS_PLAZA_FK = 6,
				FECHA_DE_MODIFICACION = R1.VIGENCIA_FINAL,
				FECHA_DE_ELIMINACION  = R1.VIGENCIA_FINAL
			WHERE 
				ID_PLAZA  = R1.ID_PLAZA
			;
			  
			/*HISTORICO MOVIMIENTOS PLAZA*/  
			SELECT 
				(max(ID_HISTORICO_MOVIMIENTO)+1) 
			INTO 
				VMAXHMOVS 
			FROM 
				 RH_NOMN_BIT_HISTORICOS_MOVS
			; 
      
      SELECT 
        TO_CHAR((TO_DATE(R1.VIGENCIA_FINAL,'DD/MM/RRRR')),'DD/MM/RRRR') 
      INTO 
        PFECHA 
      FROM 
        DUAL;      
	
			INSERT INTO 
				RH_NOMN_BIT_HISTORICOS_MOVS 
				( 
				ID_HISTORICO_MOVIMIENTO, 
				ID_ACCION_FK, 
				ID_EJERCICIO_FK, 
				FECHA, 
				ID_USUARIO_FK, 
				TABLA
				) 
			VALUES 
				( 
				VMAXHMOVS,
				2,
				VIDANIO,
				R1.VIGENCIA_FINAL,
				1,
				''
				)
			;  
							  
			/*HISTORICO CAMPOS PLAZA*/      
			SELECT  
				NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
			INTO 
				VMAXHCAMP 
			FROM 
				RH_NOMN_BIT_HISTORICOS_CAMPOS
			;

			SELECT  
				NVL(MAX(ID_HISTORICO_MOVIMIENTO_FK ),1) 
			INTO 
				VMAXMOVP 
			FROM 
				RH_NOMN_BIT_HISTORICOS_CAMPOS
			WHERE  
				ID_REGISTRO_ENTIDAD = R1.ID_PLAZA 
			;
	
			INSERT INTO 
				RH_NOMN_BIT_HISTORICOS_CAMPOS 
				( 
				ID_HISTORICO_CAMPO, 
				ID_HISTORICO_MOVIMIENTO_FK, 
				ID_REGISTRO_ENTIDAD, 
				ID_MOVIMIENTO_ANTERIOR, 
				ID_CAMPO_AFECTADO_FK, 
				VALOR_ACTUAL, 
				VALOR_BUSQUEDA
				) 
			VALUES 
				( 
				VMAXHCAMP,
				VMAXHMOVS,
				R1.ID_PLAZA,
				VMAXMOVP,
				7,
				PFECHA,
        /*20171030*/
        --R1.ID_PLAZA
        R1.NUMERO_PLAZA
				)
			;
	
			SELECT  
				NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
			INTO 
				VMAXHCAMP 
			FROM 
				RH_NOMN_BIT_HISTORICOS_CAMPOS
			;
	
			INSERT INTO 
				RH_NOMN_BIT_HISTORICOS_CAMPOS 
				( 
				ID_HISTORICO_CAMPO, 
				ID_HISTORICO_MOVIMIENTO_FK, 
				ID_REGISTRO_ENTIDAD, 
				ID_MOVIMIENTO_ANTERIOR, 
				ID_CAMPO_AFECTADO_FK, 
				VALOR_ACTUAL, 
				VALOR_BUSQUEDA
				) 
			VALUES 
				( 
				VMAXHCAMP,
				VMAXHMOVS,
				R1.ID_PLAZA,
				VMAXMOVP,
				5,
				6,
        /*20171030*/
        --R1.ID_PLAZA
        R1.NUMERO_PLAZA
				)
			;
	
			SELECT  
				NVL((MAX(ID_HISTORICO_CAMPO )+1),1) 
			INTO 
				VMAXHCAMP 
			FROM 
				RH_NOMN_BIT_HISTORICOS_CAMPOS
			;

			INSERT INTO 
				RH_NOMN_BIT_HISTORICOS_CAMPOS 
				( 
				ID_HISTORICO_CAMPO, 
				ID_HISTORICO_MOVIMIENTO_FK, 
				ID_REGISTRO_ENTIDAD, 
				ID_MOVIMIENTO_ANTERIOR, 
				ID_CAMPO_AFECTADO_FK, 
				VALOR_ACTUAL, 
				VALOR_BUSQUEDA
				) 
			VALUES 
				( 
				VMAXHCAMP,
				VMAXHMOVS,
				R1.ID_PLAZA,
				VMAXMOVP,
				6,
				0,
        /*20171030*/
        --R1.ID_PLAZA
        R1.NUMERO_PLAZA
				)
			;
					  
			DBMS_OUTPUT.PUT_LINE
			( 'PROCESO DE BAJA POR FIN DE VIGENCIA DE PLAZA : '|| 
			R1.ID_EMPLEADO  ||
			' PLAZA: '      ||
			R1.ID_PLAZA     ||
			' CON FECHA DE BAJA: ' ||
			R1.VIGENCIA_FINAL
			);    

		END LOOP;
        
		COMMIT;
	  
		FOR R2 IN C2 LOOP
			DBMS_OUTPUT.PUT_LINE(  'EMPLEADO - PENSION: '|| R2.ID_EMPLEADO||' - '|| R2.ID_PENSION );   
		END LOOP;
	  
		FOR R3 IN C3 LOOP            
			DBMS_OUTPUT.PUT_LINE(  ' EMPLEADO - MANUALEST: '|| R3.ID_EMPLEADO||' - '|| R3.ID_MANUALES_TERCEROS );                                        
		END LOOP;				
      
    END IF;

	COMMIT;
      
	IF VC = 0 THEN 
		DBMS_OUTPUT.PUT_LINE('NO SE ENCONTRARON BAJAS');
	END IF;
                     
	EXCEPTION  
	WHEN NO_DATA_FOUND THEN  
		NULL;
	WHEN OTHERS THEN   
		ROLLBACK;
	
	err_code := SQLCODE;
	err_msg := SUBSTR(SQLERRM, 1, 200);
	
	DBMS_OUTPUT.put_line ('CON ERROR - ROLLBACK');
	DBMS_OUTPUT.put_line ('Error:'||err_code||','||err_msg);

END SPVIGENCIA_PLAZA;

/
